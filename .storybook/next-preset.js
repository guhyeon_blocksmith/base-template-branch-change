const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const path = require('path');

module.exports = {
    webpackFinal: async (baseConfig/*, options*/) => {
        const {module = {}} = baseConfig;

        const newConfig = {
            ...baseConfig,
            module: {
                ...module,
                rules: [...(module.rules || [])],
            },
        };

        // TypeScript
        newConfig.module.rules.push({
            test: /\.(ts|tsx)$/,
            include: [path.resolve(__dirname, '../components'),path.resolve(__dirname, '../pages')],
            use: [
                {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            'next/babel',
                            require.resolve('babel-preset-react-app'),
                            // require.resolve("@emotion/babel-preset-css-prop"),
                        ],
                        plugins: ['react-docgen'],
                    },
                },
            ],
        });
        newConfig.resolve.plugins = newConfig.resolve.plugins || [];
        newConfig.resolve.plugins.push(
          new TsconfigPathsPlugin({
              // extensions: ['.ts', '.tsx', '.js', '.jsx'],
              // configFile: path.resolve(__dirname, '../tsconfig.json'),
          })
        );
        newConfig.resolve.extensions.push('.ts', '.tsx');
        // newConfig.resolve.alias = {
        //   ...newConfig.resolve.alias,
        //   '@': path.resolve(__dirname, '../'),
        // };
        // SCSS
        // newConfig.module.rules.push({
        //   test: /\.(s*)css$/,
        //   loaders: ['style-loader', 'css-loader', 'sass-loader'],
        //   include: [
        //     path.resolve(__dirname, '../pages'),
        //     path.resolve(__dirname, '../components'),
        //     path.resolve(__dirname, '../assets')
        //   ],
        //   exclude: /\.module\.(s*)css$/
        // });
        //
        // newConfig.module.rules.push({
        //   test: /\.module\.(s*)css$/,
        //   use: [
        //     'style-loader',
        //     'css-modules-typescript-loader',
        //     {
        //       loader: 'css-loader',
        //       options: {
        //         modules: true,
        //       },
        //     },
        //   ],
        // });

        newConfig.module.rules.find(
          rule => rule.test.toString() === '/\\.css$/'
        ).exclude = /\.module\.css$/;

        // Then we tell webpack what to do with CSS modules
        newConfig.module.rules.push({
            test: /\.module\.css$/,
            include: [
                path.resolve(__dirname, '../storybook'),
                path.resolve(__dirname, '../components'),
                path.resolve(__dirname, '../pages')
            ],
            use: [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        modules: true
                    }
                }
            ]
        });

        // newConfig.module.rules.push({
        //     test: /\.module\.scss$/,
        //     use: [
        //         {
        //             loader: 'style-loader'
        //         },
        //         {
        //             loader: 'css-loader',
        //             options: {
        //                 importLoaders: 1,
        //                 modules: {
        //                     compileType: 'module'
        //                 }
        //             }
        //         },
        //         {
        //             loader: 'sass-loader'
        //         },
        //     ],
        // });


        // If you are using CSS Modules, check out the setup from Justin (justincy)
        // Many thanks to Justin for the inspiration
        // https://gist.github.com/justincy/b8805ae2b333ac98d5a3bd9f431e8f70#file-next-preset-js

        return newConfig;
    },
};
