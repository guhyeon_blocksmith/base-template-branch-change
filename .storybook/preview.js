import React from 'react';
import 'smith-ui-components/resources/primereact.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

// Editor css
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';

//Cropper.css
import 'cropperjs/dist/cropper.css'

////!! custom 레이아웃 사용시 아래 main.css를 주석 풀고 App.scss를 주석처리하세요
// import '../assets/layout/custom/main.css';
import '../assets/layout/app/App.scss';

// video seek slider
// import 'react-video-seek-slider/lib/ui-video-seek-slider.css';




import {withNextRouter} from "storybook-addon-next-router";
import {themeNames} from "../libs/hooks/useTheme";
import {ThemeStoryBookProvider} from "../libs/providers/ThemeStoryBookProvider";

export const globalTypes = {
    theme: {
        name: 'Theme',
        description: 'Global theme for components',
        defaultValue: 'base',
        toolbar: {
            icon: 'browser',
            // array of plain string values or MenuItem shape (see below)
            items: themeNames.map(t => t.name),
        },
    },
};

/**
 * smith ui components에 테마를 story북에 적용
 * @param Story
 * @param context
 * @return {JSX.Element}
 */
const withThemeProvider = (Story, context) => {
    const theme = context.globals.theme;
    // console.log(theme);

    return (
        <ThemeStoryBookProvider theme={theme}>
            <Story {...context} />
        </ThemeStoryBookProvider>
    )
}


// addDecorator(
//     withNextRouter({
//         path: '/', // defaults to `/`
//         asPath: '/', // defaults to `/`
//         query: {}, // defaults to `{}`
//         push() {} // defaults to using addon actions integration, can override any method in the router
//     })
// );
export const decorators = [withNextRouter(), withThemeProvider];

export const parameters = {
    actions: {argTypesRegex: "^on[A-Z].*"},
};

