const path = require('path');

module.exports = {
  stories: [
    '../storybook/**/*.stories.mdx',
    '../storybook/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials', 'storybook-css-modules-preset','@storybook/preset-scss', 'storybook-addon-mobx'],
  // Add nextjs preset
  presets: [path.resolve(__dirname, './next-preset.js')],
};
