// You can include shared interfaces/types in a separate file
// and then use them in any component by importing them. For
// example, to import the interface below do:
//
// import User from 'path/to/interfaces';
import { ElementType } from 'react';
import TreeNode from 'smith-ui-components/components/treenode/TreeNode';

// class T {}
//
// export interface BaseMenuChildren {
//   name: string;
//   to?: string;
//   meta: Array<T>;
//   href?: string;
//   badge?: string;
// }
// export interface BaseMenu {
//   name: string;
//   meta: Array<T>;
//   children: BaseMenuChildren[];
// }

export interface CrumbType {
  label: string;
  url: string;
}

export interface Environment {
  isCustomLayout?: string;
  menuMode?: string;
  baseUrl?: string;
  menuUrl?: string;
  projectCustomCss?: string;
  useThemeSelector?: string;
}

export interface MenuItemType {
  label?: string;
  icon?: string;
  url?: string;
  items?: MenuItemType[] | MenuItemType[][];
  disabled?: boolean;
  target?: string;
  separator?: boolean;
  style?: any;
  className?: string;
  command?(e: { originalEvent: Event; item: MenuItemType }): void;
}

export interface MyPageMenuItemType {
  /**
   * 메뉴명
   */
  label?: string;
  /**
   * 아이콘 사용시 아이콘 클래스 - smith-ui
   */
  icon?: string;
  /**
   * 클릭 시 이동 url
   */
  url?: string;
  /**
   * 하위 메뉴
   */
  items?: MyPageMenuItemType[] | MyPageMenuItemType[][];
  /**
   * disabled 여부
   */
  disabled?: boolean;
  target?: string;
  separator?: boolean;
  style?: any;
  className?: string;
  command?(e: { originalEvent: Event; item: MyPageMenuItemType }): void;
}

export interface SNBMenuItemType {
  /**
   * 메뉴 명
   */
  name: string;
  /**
   * 내부 링크
   */
  to?: string;
  /**
   * 외부 링크
   */
  href?: string;
  /**
   * 뱃지
   */
  badge?: string;

  children?: SNBMenuItemType[];

  meta?: string[];

  expanded?: boolean;
}

export interface ConfigHeader {
  logo?: { src: string; width: number; height: number; alt: string };
  url: string;
  type: string;
  ogImage: string;
  icon: string;
}

export interface LanguageSet {
  slogan: string;
  title: string;
  author: string;
  description: string;
}

export interface Layout {
  logoPosition: string;
}

export interface MenuType {
  snb: string;
  gnb: string;
}

export interface Multilingual {
  defaultLocale?: string;
  list: Regions[];
}

export interface LayoutConfig {
  showHeader?: boolean;
  showMenu?: boolean;
  showFooter?: boolean;
  showBreadCrumb?: boolean;
  showMenuSearch?: boolean;
  footerText?: string;
  footerTextPosition?: string;
  showMultilingual?: boolean;
  header: ConfigHeader;
  multilingual: Multilingual;
  menuType: MenuType;
  layout: Layout;
  kr?: LanguageSet;
  en?: LanguageSet;
  cn?: LanguageSet;
  env: Environment;
}

export interface LayoutCustom {
  CustomHeader?: ElementType;
  useCustomHeader?: boolean;
  CustomContent?: ElementType;
  useCustomContent?: boolean;
  CustomFooter?: ElementType;
  useCustomFooter?: boolean;
  className?: string | undefined;
}

export interface Regions {
  name: string;
  code: string;
}

/**
 * SmithUiComponent Dropdown changeEvent
 */
export interface DropdownChangeEvent {
  originalEvent: Event;
  value: any;
  target: { name: string; id: string; value: any };
}

/**
 * SmithUiComponent Check changeEvent
 */
export interface CheckChangeEvent {
  originalEvent: Event;
  value: any;
  checked: boolean;
  target: {
    type: string;
    name: string;
    id: string;
    value: any;
    checked: boolean;
  };
}

export interface SurveySecondsArrayType {
  sec: number;
}

export interface SurveySecondsCallBack {
  secArray: SurveySecondsArrayType[];
  secIndex: number;
}

interface Questionnaire {
  type: string;
  question: string;
  answers: any[];
}

export interface SurveyItemType {
  createdAt: any;
  playedSec: number;
  guid: string;
  questionnaire: Questionnaire;
  sid: string;
}

export interface SurveyListType {
  Items: SurveyItemType[];
  Count: number;
  ScannedCount: number;
}

/**
 * 트리 노드 타입 generic.
 */
export type TreeNodeType<T> = {
  data: T;
  children: TreeNodeType<T>[];
} & Omit<TreeNode, 'data'>; // TreeNode에서 data를 제외(omit)한 다음 data에 genericType을 주입

export function isTouchEvent(
  e: React.TouchEvent | React.MouseEvent
): e is React.TouchEvent {
  return e && 'touches' in e;
}

export function isMouseEvent(
  e: React.TouchEvent | React.MouseEvent
): e is React.MouseEvent {
  return e && 'screenX' in e;
}
