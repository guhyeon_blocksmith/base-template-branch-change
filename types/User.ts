export interface LoginUser {
  email: string;
  password: string;
}
export interface UserSession {
  email: string;
}
export interface User extends LoginUser {
  id: number;
  refresh_token: string;
}
