import { Matches, MinLength } from 'class-validator';
import { makeAutoObservable } from 'mobx';
import { ManagerLoginRequest } from 'apis/ApiAdmin';

/**
 * LoginModel
 * 유효성 검사 https://github.com/typestack/class-validator 참고
 * 모델에 propery들은 undefined인 경우 모든 유효성검사가 작동합니다. 유의해주세요.
 */
export class LoginModel implements ManagerLoginRequest {
  /**
   * Store에서 사용하는 Model클래스는 observable로 만들어주세요.
   */
  constructor() {
    makeAutoObservable(this);
  }

  /**
   * 로그인 ID
   */
  @MinLength(5, {
    // message: () => {
    //   // const { constraints } = args;
    //   return `최소 5자 이상 영문 소문자 및 숫자를 사용하세요`;
    // },
  })
  @Matches(/[^ㄱ-ㅎㅏ-ㅣ가-힣]/, {
    // message: () => {
    //   return '최소 5자 이상 영문 소문자 및 숫자를 사용하세요';
    // },
  })
  loginId = '';

  /**
   * 로그인 비밀번호
   */
  @Matches(/((?=.*\d)(?=.*[a-z])(?=.*[\W])(?!.*[ㄱ-ㅎㅏ-ㅣ가-힣]).{8,64})/, {
    // message: () => {
    //   return '최소 8자이상 영문 대 소문자, 숫자 및 특수문자를 함께 사용하세요.';
    // },
  })
  password = '';
}
