import { IsNotEmpty, Matches } from 'class-validator';
import { makeAutoObservable } from 'mobx';
import { IsEqualTo } from 'libs/customValidators';

/**
 * PasswordChangeModel
 * 유효성 검사 https://github.com/typestack/class-validator 참고
 * 모델에 propery들은 undefined인 경우 모든 유효성검사가 작동합니다. 유의해주세요.
 */
export class PasswordChangeModel {
  /**
   * Store에서 사용하는 Model클래스는 observable로 만들어주세요.
   */
  constructor() {
    makeAutoObservable(this);
  }

  /**
   * 현재 비밀번호
   */
  @IsNotEmpty({
    message: () => {
      return '현재 비밀번호를 입력해주세요.';
    },
  })
  currentPassword = '';

  /**
   * 새 비밀번호
   */
  @Matches(/((?=.*\d)(?=.*[a-z])(?=.*[\W])(?!.*[ㄱ-ㅎㅏ-ㅣ가-힣]).{8,64})/, {
    // pattern에 /g 없어야 작동 https://github.com/typestack/class-validator/issues/484#issuecomment-595821457
    message: () => {
      return '최소 8자이상 영문 대 소문자, 숫자 및 특수문자를 함께 사용하세요.';
    },
  })
  newPassword = '';

  /**
   * 새 비밀번호 확인
   */
  @IsEqualTo('newPassword', {
    message: () => {
      return '비밀번호가 일치하지 않습니다.';
    },
  })
  newConfirmPassword = '';
}
