import {
  IsNotEmpty,
  MaxLength,
  MinLength,
  ValidationArguments,
} from 'class-validator';
import { makeAutoObservable } from 'mobx';
import { AttachFileType } from 'types/AttachFileType';

/**
 * BoardNewFormModel
 * 게시판 새로 만들기 폼 모델. edit와 별도로 만든 이유는 실제 업무에서 다르게 나오는 경우가 더 많기 떄문입니다.
 * 유효성 검사 https://github.com/typestack/class-validator 참고
 * 모델에 propery들은 undefined인 경우 모든 유효성검사가 작동합니다. 유의해주세요.
 */
export class BoardNewFormModel {
  /**
   * Store에서 사용하는 Model클래스는 observable로 만들어주세요.
   */
  constructor() {
    makeAutoObservable(this);
  }

  @IsNotEmpty()
  id?: string;

  @MinLength(5, {
    message: (args: ValidationArguments) => {
      const { property, value, constraints } = args;
      return `${property}에 ${constraints[0]} 글자 이상 입력해주세요. 입력값 :'${value}'`;
    },
  })
  @MaxLength(50, {
    message: (args: ValidationArguments) => {
      const { property, value, constraints } = args;
      return `${property}에 ${constraints[0]}글자까지 입력할 수 있습니다. 입력값 : '${value}'`;
    },
  })
  title?: string;

  @IsNotEmpty()
  @MaxLength(5000, {
    message: (args: ValidationArguments) => {
      const { property, value, constraints } = args;
      return `${property}에 ${constraints[0]}글자까지 입력할 수 있습니다. 입력값 : '${value}'`;
    },
  })
  content?: string;

  /**
   * 첨부파일 경로 목록
   */
  attachedFiles?: AttachFileType[];

  createdAt?: number;
  editedAt?: number;
}
