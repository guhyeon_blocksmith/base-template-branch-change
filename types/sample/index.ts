import BoardDetailResponse from './board/BoardDetailResponse';

/**
 * smith ui component 기본 dropdownType
 */
export interface DropdownTypeLabelValue {
  label: string;
  value: string | number;
}

export interface DropdownType {
  name: string;
  code: string;
}

export interface GalleriaType {
  itemImageSrc: string;
  alt: string;
  thumbnailImageSrc: string;
}

export interface ProductsType {
  id: string;
  code: string;
  name: string;
  description: string;
  image: string;
  price: number;
  category: string;
  quantity: number;
  inventoryStatus: string;
  rating: number;
}

export interface CustomerType {
  id: number;
  name: string;
  country: DropdownType;
  company: string;
  date: string;
  status: string;
  activity: number;
  representative: RepresentativeType;
}

export interface RepresentativeType {
  name: string;
  image: string;
}

export type { BoardDetailResponse };
