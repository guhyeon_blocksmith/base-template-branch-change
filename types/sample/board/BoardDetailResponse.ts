import { AttachFileType } from 'types/AttachFileType';

/**
 * 게시판 상세보기 api 리턴 타입
 */
interface BoardDetailResponse {
  id?: string;
  title?: string;
  content?: string;
  attachedFiles?: AttachFileType[];
  createdAt?: number;
  editedAt?: number;
}
export default BoardDetailResponse;
