/* eslint-disable */
importScripts("https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js");
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');

const config = {
    apiKey: "AIzaSyAeOS_DuuTl3Ae1xK46TuX_FEEhZycsC4o",
    authDomain: "base-template-46ac0.firebaseapp.com",
    projectId: "base-template-46ac0",
    storageBucket: "base-template-46ac0.appspot.com",
    messagingSenderId: "198558585141",
    appId: "1:198558585141:web:e75a5724148a9564c93c7c",
    measurementId: "G-862BPQZWR0"
}
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: '/firebase-logo.png',
        click_action: payload.data.click_action,
        data: {
            click_action: payload.data.click_action,
        },
    }

    self.addEventListener('notificationclick', function(event) {
        if (!event.action) {
            self.clients.openWindow(event.notification.data.click_action, '_blank')
            event.notification.close();
        }else{
            event.notification.close();
        }
    });
    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});

