import { Api as _ApiUser } from 'apis/ApiUser';
import { Api as _ApiSocket } from 'apis/ApiSocket';
import { Api as _ApiAdmin } from 'apis/ApiAdmin';
import { alert } from 'smith-ui-components/dialog';
import { isServer } from 'libs/common';
import { AxiosResponse } from 'axios';

const proxyBaseUrl = process.env.NEXT_PUBLIC_NEXT_API_BASE ?? '';

/**
 * API 호출 공통 성공 처리 인터셉터
 * @param response
 */
const commonSuccessInterceptor = async (response: AxiosResponse<any>) => {
  if (!isServer && response) {
    if (
      process.env.NEXT_PUBLIC_SITE_TYPE === 'ADMIN' &&
      response.data?.error?.message
    ) {
      alert(response.data.error.message).then();
    }

    const code = response.data?.error?.code;
    if (code === undefined) {
      throw '응답에러에 error.code가 없습니다. 백엔드에 요청하세요.';
    }
  }
  return response;
};

/**
 * API 호출 공통 에러 처리 인터셉터. 401, 403, 404등 실패시 error 안뱉도록
 * @param error
 */
const commonFailInterceptor = async (error: any) => {
  // 401, 403권한없음, 404데이터없음, 400오류 응답은 처리하여 code로 넘겨준다.
  if (!isServer && error.response) {
    if (
      process.env.NEXT_PUBLIC_SITE_TYPE === 'ADMIN' &&
      error.response.data?.error?.message
    ) {
      alert(error.response.data.error.message).then();
    }

    const code = error.response.data?.error?.code;
    if (code === undefined) {
      throw '응답에러에 error.code가 없습니다. 백엔드에 요청하세요.';
    }

    return Promise.resolve(error.response);
  }

  // 오류 응답을 처리
  return Promise.reject(error);
};

/**
 * Proxy. 관리자 Private  Api ( 인증 필요 )
 */
export const PrivateAdminApi = new _ApiAdmin({
  baseURL: proxyBaseUrl + process.env.NEXT_PUBLIC_API_ADMIN_PRIVATE_PROXY_URL,
});

// 공통 메뉴(CRUD) 권한 에러 메시지 처리
PrivateAdminApi.instance.interceptors.response.use(
  commonSuccessInterceptor,
  commonFailInterceptor
);

/**
 * Proxy. 관리자 Public Api ( 인증 필요 X). 로그인에서 사용
 */
export const PublicAdminApi = new _ApiAdmin({
  baseURL: proxyBaseUrl + process.env.NEXT_PUBLIC_API_ADMIN_PUBLIC_PROXY_URL,
});

// 공통 메뉴(CRUD) 권한 에러 메시지 처리
PublicAdminApi.instance.interceptors.response.use(
  commonSuccessInterceptor,
  commonFailInterceptor
);

/**
 * Proxy. 사용자 Public Api ( 인증 필요 X)
 */
export const PublicUserApi = new _ApiUser({
  baseURL: proxyBaseUrl + process.env.NEXT_PUBLIC_API_USER_PUBLIC_PROXY_URL,
});
// 공통 메뉴(CRUD) 권한 에러 메시지 처리
PublicUserApi.instance.interceptors.response.use(
  commonSuccessInterceptor,
  commonFailInterceptor
);

/**
 * Proxy. 사용자 Private Api ( 인증 필요 )
 */
export const PrivateUserApi = new _ApiUser({
  baseURL: proxyBaseUrl + process.env.NEXT_PUBLIC_API_USER_PRIVATE_PROXY_URL,
});
// 공통 메뉴(CRUD) 권한 에러 메시지 처리
PrivateUserApi.instance.interceptors.response.use(
  commonSuccessInterceptor,
  commonFailInterceptor
);

/**
 * 소켓 Public Api
 */
export const SocketApi = process.env.NEXT_PUBLIC_API_SOCKET_URL
  ? new _ApiSocket({
      baseUrl: process.env.NEXT_PUBLIC_API_SOCKET_URL,
      baseApiParams: { format: 'json' },
    })
  : undefined;
