/**
 * https://github.com/techiediaries/fake-api-jwt-json-server
 * JWT 사용 fake server
 */

import fs from 'fs';
import bodyParser from 'body-parser';
import jsonServer from 'json-server';
import jwt from 'jsonwebtoken';
import { LoginUser, User, UserSession } from '../../types/User';

const server = jsonServer.create();
const router = jsonServer.router('fake-db.json');
const middlewares = jsonServer.defaults();
let port = 3001;

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(middlewares);

// process.argv.forEach(function (val, index, array) {
//   console.log(array);
//   console.log(index + ': ' + val);
// });

const portIndex =
  process.argv.findIndex((arg) => arg === '--port' || arg === '-P') + 1;

if (portIndex > -1) {
  const argPort = Number(process.argv[portIndex]);
  if (Number.isNaN(argPort)) {
    throw Error('enter port number');
  }
  port = argPort;
}

const userdb = JSON.parse(
  fs.readFileSync('./users.json', { encoding: 'utf-8' })
);

const SECRET_KEY = '123456789';

const expiresIn = '1h';

// Create a token from a payload
function createToken(payload: UserSession) {
  return jwt.sign(payload, SECRET_KEY, { expiresIn });
}

// Create a token from a payload
function createRefreshToken(payload: UserSession) {
  return jwt.sign(payload, '11112332', { expiresIn: '18d' });
}

// Verify the token
function verifyToken(token: string) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, SECRET_KEY, (err, decode) => {
      if (decode !== undefined) resolve(decode);
      reject(err);
    });
  });
}

// Check if the user exists in database
function isAuthenticated({ email, password }: LoginUser) {
  return (
    (userdb.users as User[]).findIndex(
      (user) => user.email === email && user.password === password
    ) !== -1
  );
}

// Register New User
server.post('/auth/register', (req, res) => {
  console.log('register endpoint called; request body:');
  const { email, password } = req.body as LoginUser;

  if (isAuthenticated({ email, password }) === true) {
    const status = 401;
    const message = 'Email and Password already exist';
    res.status(status).json({ status, message });
    return;
  }

  fs.readFile('./users.json', (err, buffer) => {
    if (err) {
      const status = 401;
      const message = err;
      res.status(status).json({ status, message });
      return;
    }

    // Get current users data
    const data = JSON.parse(buffer.toString());

    // Get the id of last user
    const last_item_id = data.users[data.users.length - 1].id;

    //Add new user
    data.users.push({ id: last_item_id + 1, email: email, password: password }); //add some data
    fs.writeFile('./users.json', JSON.stringify(data), (err) => {
      // WRITE
      if (err) {
        const status = 401;
        const message = err;
        res.status(status).json({ status, message });
        return;
      }
    });
  });

  // Create token for new user
  const access_token = createToken({ email });
  console.log('Access Token:' + access_token);
  res.status(200).json({ access_token });
});

// Login to one of the users from ./users.json
server.post('/auth/login', (req, res) => {
  console.log('login endpoint called; request body:');
  const { email, password } = req.body;
  if (isAuthenticated({ email, password }) === false) {
    const status = 401;
    const message = 'Incorrect email or password';
    res.status(status).json({ status, message });
    return;
  }
  const refresh_token = createRefreshToken({ email });

  fs.readFile('./users.json', (err, buffer) => {
    if (err) {
      const status = 401;
      const message = err;
      res.status(status).json({ status, message });
      return;
    }

    // Get current users data
    const data = JSON.parse(buffer.toString());

    data.users.find(
      (u: User) => u.email === email
    ).refresh_token = refresh_token;

    fs.writeFile('./users.json', JSON.stringify(data), (err) => {
      // WRITE
      if (err) {
        const status = 401;
        const message = err;
        res.status(status).json({ status, message });
        return;
      }
    });
  });
  const access_token = createToken({ email });
  console.log('Access Token:' + access_token);
  res.status(200).json({ access_token, refresh_token });
});

// Login to one of the users from ./users.json
server.post('/auth/refresh', (req, res) => {
  console.log('refresh endpoint called; request body:');
  const { refresh_token } = req.body;
  // if (isAuthenticated({ email, password }) === false) {
  //   const status = 401;
  //   const message = 'Incorrect email or password';
  //   res.status(status).json({ status, message });
  //   return;
  // }
  const user = (userdb.users as User[]).find(
    (u) => (u.refresh_token = refresh_token)
  );

  if (!user) {
    const status = 401;
    const message = 'refresh token이 없습니다.';
    res.status(status).json({ status, message });
    return;
  }

  const access_token = createToken({ email: user.email });
  res.status(200).json({ access_token, refresh_token });
});

// auth 페이지가 아닌 경우. header에 auth 체크
server.use(/^(?!\/auth).*$/, async (req, res, next) => {
  if (
    req.headers.authorization === undefined ||
    req.headers.authorization.split(' ')[0] !== 'Bearer'
  ) {
    const status = 401;
    const message = 'Error in authorization format';
    res.status(status).json({ status, message });
    return;
  }
  try {
    await verifyToken(req.headers.authorization.split(' ')[1]);
    next();
  } catch {
    const status = 401;
    const message = 'Access token not provided';
    res.status(status).json({ status, message });
    return;
  }
});

server.use(router);
server.listen(port, () => {
  console.log('JSON Server is running');
});
