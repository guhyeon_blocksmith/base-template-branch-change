/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface AFReponse {
  /** @format int32 */
  attachFileIdF?: number;
  fileExt?: string;
  fileName?: string;
  fileUrl?: string;
}

export interface AddressBookAddressInfoResponse {
  /**
   * 주소록 주소
   * @example 서울시 강남구 논현동 13-13
   */
  adres?: string;

  /**
   * 주소록 주소 ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface AddressBookAddressInsertRequest {
  /**
   * 주소록 주소
   * @example 서울시 강남구 논현동 13-13
   */
  adres?: string;
}

export interface AddressBookAddressModifyRequest {
  /**
   * 주소록 주소
   * @example 서울시 강남구 논현동 13-13
   */
  adres?: string;
}

export interface AddressBookDeleteRequest {
  /**
   * 주소록 ID
   * @example [1,2,3,4]
   */
  addressBookIds?: number[];
}

export interface AddressBookEmailInfoResponse {
  /**
   * 주소록 이메일
   * @example block@blocksmith.xyz
   */
  email?: string;

  /**
   * 주소록 이메일 ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface AddressBookEmailInsertRequest {
  /**
   * 주소록 이메일
   * @example block@blocksmith.xyz
   */
  email?: string;
}

export interface AddressBookEmailModifyRequest {
  /**
   * 주소록 이메일
   * @example block@blocksmith.xyz
   */
  email?: string;
}

export interface AddressBookInfoResponse {
  /**
   * 주소록 주소 목록
   * @example 서울시 강남구 논현동 13-13
   */
  addressList?: AddressBookAddressInfoResponse[];

  /**
   * 생년월일
   * @example 1990-01-01
   */
  brthdy?: string;

  /**
   * 소속/회사
   * @example 블록스미스
   */
  company?: string;

  /**
   * 주소록 이메일 목록
   * @example block@blocksmith.xyz
   */
  emailList?: AddressBookEmailInfoResponse[];
  error?: ErrorVo;

  /**
   * 주소록 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 국가코드ID (코드그룹ID : CM007 로 조회)
   * @example 82
   */
  nationNo?: string;

  /**
   * 국적(F:해외 / I:국내)
   * @example I
   */
  nationalty?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 포지션/직책
   * @example 개발자
   */
  position?: string;

  /**
   * 주소록 사이트/SNS URL 목록
   * @example www.blocksmith.com
   */
  siteUrlList?: AddressBookUrlInfoResponse[];

  /**
   * 전화번호
   * @example 010-1234-1234
   */
  telNo?: string;
}

export interface AddressBookInsertRequest {
  /** 주소록 주소 */
  addressList?: AddressBookAddressInsertRequest[];

  /**
   * 생년월일
   * @example 1990-01-01
   */
  brthdy?: string;

  /**
   * 소속/회사
   * @example 블록스미스
   */
  company?: string;

  /** 주소록 이메일 */
  emailList?: AddressBookEmailInsertRequest[];

  /**
   * 국가번호 (코드그룹ID : CM007로 조회)
   * @example 82
   */
  nationCodeId?: string;

  /**
   * 국적(F:해외 / I:국내)
   * @example I
   */
  nationalty?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 포지션/직책
   * @example 개발자
   */
  position?: string;

  /** 주소록 사이트/SNS URL */
  siteUrlList?: AddressBookUrlInsertRequest[];

  /**
   * 전화번호
   * @example 010-1234-1234
   */
  telNo?: string;
}

export interface AddressBookModifyRequest {
  /** 주소록 주소 목록 */
  addressList?: AddressBookAddressModifyRequest[];

  /**
   * 생년월일
   * @example 1990-01-01
   */
  brthdy?: string;

  /**
   * 소속/회사
   * @example 블록스미스
   */
  company?: string;

  /** 주소록 이메일 목록 */
  emailList?: AddressBookEmailModifyRequest[];

  /**
   * 국가번호
   * @example 82
   */
  nationCodeId?: string;

  /**
   * 국적(F:해외 / I:국내)
   * @example I
   */
  nationalty?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 포지션/직책
   * @example 개발자
   */
  position?: string;

  /** 주소록 사이트/SNS URL 목록 */
  siteUrlList?: AddressBookUrlModifyRequest[];

  /**
   * 전화번호
   * @example 010-1234-1234
   */
  telNo?: string;
}

export interface AddressBookNationCodeResponse {
  /**
   * 코드명
   * @example 대한민국
   */
  label?: string;

  /**
   * 국가코드ID
   * @example 82
   */
  value?: string;
}

export interface AddressBookSearchResponse {
  /**
   * 주소록 주소
   * @example 서울시 강남구 논현동 13-13
   */
  address?: string;

  /**
   * 생년월일
   * @example 1990-01-01
   */
  brthdy?: string;

  /**
   * 소속/회사
   * @example 블록스미스
   */
  company?: string;

  /**
   * 주소록 이메일
   * @example block@blocksmith.xyz
   */
  email?: string;

  /**
   * 주소록 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 국적(F:해외 / I:국내)
   * @example I
   */
  nationalty?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 포지션/직책
   * @example 개발자
   */
  position?: string;

  /**
   * 주소록 사이트/SNS URL
   * @example www.blocksmith.com
   */
  siteUrl?: string;

  /**
   * 전화번호
   * @example 010-1234-1234
   */
  telNo?: string;
}

export interface AddressBookUrlInfoResponse {
  /**
   * 주소록 사이트/SNS URL ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 주소록 사이트/SNS URL
   * @example www.blocksmith.com
   */
  siteUrl?: string;
}

export interface AddressBookUrlInsertRequest {
  /**
   * 주소록 사이트/SNS URL
   * @example www.blocksmith.com
   */
  siteUrl?: string;
}

export interface AddressBookUrlModifyRequest {
  /**
   * 주소록 사이트/SNS URL
   * @example www.blocksmith.com
   */
  siteUrl?: string;
}

/**
 * 환불 정보 조회 response
 */
export interface AdminCilentMemberRefundSearchResponse {
  /**
   * 담당자
   * @example 홍길동
   */
  charger?: string;

  /**
   * 쿠폰사용금액
   * @format int32
   * @example 20000
   */
  couponDscntAmount?: number;
  error?: ErrorVo;

  /**
   * 환불 처리 상태
   * @example 취소신청
   */
  processStatus?: string;

  /**
   * 환불금액
   * @format int32
   * @example 300000
   */
  refndAmount?: number;

  /**
   * 취소사유
   * @example 불법컨텐츠 입니다
   */
  reqestCanclResn?: string;

  /**
   * 의뢰 번호
   * @format int32
   * @example 245333
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example ㅇㅇㅇㅇ 의뢰평가
   */
  reqestNm?: string;

  /**
   * 의뢰상태
   * @example 심사 대기
   */
  reqestStatus?: string;

  /**
   * 신청일
   * @example 2021-03-04 12:15:06
   */
  rqstdt?: string;

  /**
   * 결제금액
   * @format int32
   * @example 300000
   */
  setleReqestAmount?: number;

  /**
   * 주문번호
   * @example 23489-34334
   */
  settlementNo?: string;

  /**
   * 처리일
   * @example 2021-03-04 12:00:00
   */
  trede?: string;
}

export interface AdminClientChartSearchRequest {
  /**
   * 차트 연령별 조건(전체: 모든 연령대 배열에 넣어주어야 함)
   * @example [10,20,30,40]
   */
  ageList?: number[];

  /**
   * 차트 필터(연령별:A 성별:S)
   * @example A
   */
  filter?: string;

  /**
   * 차트 정렬 조건(A:보기순 E:평가순)
   * @example A
   */
  orderBy?: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 요청서 설문지 질문
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 요청서 설문지 아이디
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 요청서 아이디
   * @format int32
   * @example 295
   */
  requestId?: number;

  /**
   * 차트 성별 조건(전체:A 남:M 여:F)
   * @example A
   */
  sexdstn?: string;
}

/**
 * 관리자 - 사용자 쿠폰 Insert Request VO
 */
export interface AdminClientCouponInsertRequest {
  /**
   * 쿠폰 고유번호
   * @format int32
   * @example 1
   */
  couponNo?: number;

  /**
   * 사용자ID 배열
   * @example [1,2,3,4,5]
   */
  memberIdArr?: number[];
}

/**
 * 관리자 - 사용자 쿠폰 Insert Response VO
 */
export interface AdminClientCouponInsertResponse {
  /**
   * 쿠폰 고유번호
   * @format int32
   * @example 1
   */
  couponNo?: number;
  error?: ErrorVo;

  /**
   * 사용자ID 배열
   * @example 1,2,3,4,5
   */
  memberIdArr?: number[];
}

/**
 * 관리자 - 사용자 쿠폰 Response VO
 */
export interface AdminClientCouponResponse {
  /**
   * 쿠폰 코드
   * @example ILOVEWEWILL
   */
  couponCode?: string;

  /**
   * 쿠폰 만료일
   * @example 1
   */
  couponExpireDt?: string;

  /**
   * 쿠폰 등록일
   * @example 1
   */
  couponRecptDt?: string;

  /**
   * 쿠폰 고유 번호(난수)
   * @example ABEL1X3BLEODFPAB10CKLPD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용일
   * @example 1
   */
  couponUseDt?: string;

  /**
   * 쿠폰 사용 여부(Y: 사용완료, N: 미사용, E:기간만료
   * @example Y
   */
  couponUseYn?: string;

  /**
   * 의뢰자명
   * @example 강하늘
   */
  name?: string;

  /**
   * 사용자 쿠폰 고유번호
   * @format int32
   * @example 1
   */
  no?: number;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rnum?: number;

  /**
   * 아이디
   * @example 1
   */
  userName?: string;
}

export interface AdminClientMemberCouponListResponse {
  /**
   * 쿠폰 코드
   * @example ILOVEWEWILL
   */
  couponCode?: string;

  /**
   * 쿠폰 할인 혜택
   * @example 퍼센트 할인 (40%) 50000원이상/10000원까지
   */
  couponDscnt?: string;

  /**
   * 쿠폰 고유 번호
   * @example yrd197uiwxho091yw0co1w4fy92dkj
   */
  couponEsntlNo?: string;

  /**
   * 쿠폰 만료일
   * @example 2021-04-20 11:59:59
   */
  couponExpireDt?: string;

  /**
   * 쿠폰 명
   * @example 의뢰 40% 할인 쿠폰
   */
  couponName?: string;

  /**
   * 쿠폰 등록일
   * @example 2021-04-20 11:59:59
   */
  couponRecptDt?: string;

  /**
   * 쿠폰상태(사용 가능, 사용 완료, 기간 만료)
   * @example 사용 가능
   */
  couponSttus?: string;

  /**
   * 쿠폰 유형(관리자가 회원에게 등록(AD) or 회원이 스스로 등록(CL))
   * @example AD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용일
   * @example 2021-04-20 11:59:59
   */
  couponUseDt?: string;

  /**
   * 쿠폰 고유ID
   * @format int32
   */
  no?: number;

  /**
   * 의뢰 고유ID
   * @format int32
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example 유튜브 평가 의뢰
   */
  reqestNm?: string;

  /**
   * 순번
   * @format int32
   */
  rownum?: number;
}

export interface AdminClientMemberInfoResponse {
  /**
   * 통합회원 식별 id
   * @format int32
   * @example 1
   */
  accountId?: number;

  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 123123123
   */
  acnutno?: string;

  /**
   * 광고성 정보 수신 동의 여부
   * @example test@blocksmith.xyz
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 광고성 정보 수신 동의 시간
   * @example 2000-01-01 00:00:00
   */
  advrtsProvisionChangeDt?: string;

  /**
   * 파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 환불 받은 은행 명
   * @example 신한
   */
  bankNm?: string;

  /**
   * 생년월일
   * @example 2000-01-01
   */
  birthday?: string;

  /**
   * 의뢰자 구분(P:개인 C:법인)
   * @example P
   */
  bizSection?: string;

  /**
   * 이메일
   * @example test@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 파일 url
   * @example http://test.com
   */
  fileUrl?: string;

  /**
   * 휴대폰 번호
   * @example 010-1212-1212
   */
  mbtlnum?: string;

  /**
   * 회원 식별 id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 회원 상태(N:활성, Y:탈퇴)
   * @example N
   */
  memberState?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 비밀번호 변경일
   * @example 1
   */
  passwordChangeDt?: string;

  /**
   * 최근 접속일
   * @example 2000-01-01 00:00:00
   */
  recentConectDt?: string;

  /**
   * 추천인
   * @format int32
   * @example 1
   */
  recomendrId?: number;

  /**
   * 첨부파일 첨부 일시
   * @example 1
   */
  regDt?: string;

  /**
   * 탈퇴이유
   * @example 기타
   */
  secessionContents?: string;

  /**
   * 탈퇴일
   * @example 2000-01-01 00:00:00
   */
  secessionDt?: string;

  /**
   * 탈퇴사유 ([MA004] 001:서비스 불만족, 002:고객응대 불만족, 003:사용 안 하는 서비스, 004: 기타)
   * @example 004
   */
  secessionReason?: string;

  /**
   * 구분(E:평가자, C:의뢰자)
   * @example C
   */
  section?: string;

  /**
   * 성별(M:남,F:여)
   * @example M
   */
  sexdstn?: string;

  /**
   * 가입일
   * @example 2000-01-01 00:00:00
   */
  subscribeDt?: string;

  /**
   * 회원 id
   * @example blocksmith
   */
  username?: string;
}

export interface AdminClientMemberInsertRequest {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 123123123
   */
  acnutno?: string;

  /**
   * 광고성 정보 수신 동의(Y,N)
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 환불 받은 은행 명
   * @example 신한
   */
  bankNm?: string;

  /**
   * 생년월일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 사업자 등록증 파일 id
   * @format int32
   * @example 1
   */
  bizFileId?: number;

  /**
   * 사업 구분[P:개인 , C:법인]
   * @example P
   */
  bizSection?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 비밀번호
   * @example 1234
   */
  password?: string;

  /**
   * 서비스(wewill:W fillin:F)
   * @example W
   */
  service?: string;

  /**
   * 성별(남:M, 여:F)
   * @example M
   */
  sexdstn?: string;

  /**
   * 로그인ID
   * @example test
   */
  username?: string;
}

export interface AdminClientMemberModifyRequest {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 123123123
   */
  acnutno?: string;

  /**
   * 사업자 등록증 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 은행
   * @example 123123
   */
  bankNm?: string;

  /**
   * 생년월일
   * @example 19900101
   */
  birthday?: string;

  /**
   * 의뢰자 구분(P:개인 , C:법인)
   * @example P
   */
  bizSection?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 성별
   * @example M or F
   */
  sexdstn?: string;
}

export interface AdminClientMemberReqInfoResponse {
  /**
   * 의뢰 신고 총 횟수
   * @format int32
   * @example 1
   */
  declarationTotalCnt?: number;
  error?: ErrorVo;

  /** 의뢰 내역 */
  reqPageList?: PagingInfoAdminClientMemberRequestListResponse;

  /**
   * 의뢰 총 횟수
   * @format int32
   * @example 1
   */
  reqTotalCnt?: number;
}

export interface AdminClientMemberRequestListResponse {
  /**
   * 이탈자 수
   * @format int32
   * @example 1
   */
  awayCnt?: number;

  /**
   * 카테고리 명
   * @example 키즈
   */
  categoryNm?: string;

  /**
   * 신고 건수
   * @format int32
   * @example 1
   */
  declarationCnt?: number;

  /**
   * 불성실 평가자 수
   * @format int32
   * @example 1
   */
  dishonestNCnt?: number;

  /**
   * 정상 평가자 수
   * @format int32
   * @example 1
   */
  dishonestYCnt?: number;

  /**
   * 목표 평가자 수
   * @format int32
   * @example 1
   */
  goalEvlManCnt?: number;

  /**
   * 총 설문 수
   * @format int32
   * @example 1
   */
  qestnCnt?: number;

  /**
   * 의뢰콘텐츠 ([IN002] V:영상, P:제품)
   * @example V
   */
  reqestCntnts?: string;

  /**
   * 의뢰 명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰 신청일
   * @example 2020-01-01 00:00:00
   */
  reqestReqstDt?: string;

  /**
   * 의뢰상태 ([IN001]10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 10
   */
  reqestStatus?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example B
   */
  reqestTy?: string;

  /**
   * 의뢰 고유 번호
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 견적산출 금액
   * @format int32
   * @example 1000
   */
  totSetleExpectAmount?: number;
}

/**
 * 관리자 - 사용자 쿠폰 Response VO
 */
export interface AdminClientMemberResponse {
  /**
   * 생년월일
   * @format date
   * @example 1985-01-01
   */
  birthday?: string;

  /**
   * 이메일
   * @example example@blocksmith.xyz
   */
  email?: string;

  /**
   * 의뢰자 고유번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 010-4117-2114
   */
  mbtlnum?: string;

  /**
   * 의뢰자명
   * @example 강하늘
   */
  name?: string;

  /**
   * 최종 접속일
   * @example 2021-04-20 12:00:00
   */
  recentConectDt?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;

  /**
   * 성별코드
   * @example M:남, F:여
   */
  sexdstnCd?: string;

  /**
   * 성별명
   * @example M:남, F:여
   */
  sexdstnNm?: string;

  /**
   * 가입일
   * @example 2021-04-20 12:00:00
   */
  subscribeDt?: string;

  /**
   * 로그인id
   * @example acbd123
   */
  username?: string;
}

export interface AdminClientMemberSearchResponse {
  /**
   * 생년월일
   * @example 2000-01-01
   */
  birthday?: string;

  /**
   * 의뢰자 구분(P:개인, C:사업자(법인))
   * @example 개인
   */
  bizSection?: string;

  /**
   * 이메일
   * @example test@blocksmith.xyz
   */
  email?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰 번호
   * @example 01011111111
   */
  mbtlnum?: string;

  /**
   * 상태( N: 활성, Y: 탈퇴)
   * @example N
   */
  memberState?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 최종 접속일
   * @example 2000-01-01 00:00:00
   */
  recentConectDt?: string;

  /**
   * 의뢰 횟수
   * @format int32
   * @example 10
   */
  requestCnt?: number;

  /**
   * 성별(남,여)
   * @example 남
   */
  sexdstn?: string;

  /**
   * 가입일
   * @example 2000-01-01 00:00:00
   */
  subscribeDt?: string;

  /**
   * 로그인 ID
   * @example blocksmith
   */
  username?: string;
}

export interface AdminClientMemberSettlementListResponse {
  /**
   * 결제일시
   * @example 2021-04-03 12:32:32
   */
  applyDt?: string;

  /**
   * 의뢰 카테고리
   * @example 영화,음식,도서
   */
  categoryList?: ReqCategoryResponse[];

  /**
   * 쿠폰 코드
   * @example ILOVEWEWILL
   */
  couponCode?: string;

  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 고유 번호
   * @example yrd197uiwxho091yw0co1w4fy92dkj
   */
  couponEsntlNo?: string;

  /**
   * 쿠폰 명
   * @example 의뢰 40% 할인 쿠폰
   */
  couponName?: string;

  /**
   * 쿠폰사용여부
   * @example Y
   */
  couponUseYn?: string;

  /**
   * PG사 거래번호
   * @example 444333
   */
  delngNo?: string;

  /**
   * PG사 승인번호
   * @example 23124
   */
  pgTid?: string;

  /**
   * 의뢰 콘텐츠
   * @example V:영상, P:제품
   */
  reqestCntnts?: string;

  /**
   * 의뢰 번호
   * @format int32
   * @example 6332
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example ㅇㅇㅇㅇ 의뢰평가
   */
  reqestNm?: string;

  /**
   * 결제수단
   * @example 카드결제
   */
  setleKnd?: string;

  /**
   * 결제금액
   * @format int32
   * @example 300000
   */
  setleReqestAmount?: number;

  /**
   * 주문번호
   * @example 23489-34334
   */
  settlementNo?: string;
}

export interface AdminClientReportBasicWordCloudSearchResponse {
  /** 단어 */
  text?: string;

  /**
   * 단어 수
   * @format int32
   */
  value?: number;
}

export interface AdminClientReportHighLightChartFilterRequest {
  /**
   * 연령대(전체일 경우 모든 나이 다 보내줘야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 필터(A:나이 S:성별)
   * @example A
   */
  filter?: string;

  /**
   * 의뢰서 id
   * @format int32
   * @example 335
   */
  requestId?: number;

  /**
   * 성별(A:전체 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;
}

export interface AdminClientReportInfoResponse {
  /**
   * 의뢰서에서 설정한 연령대
   * @example [10,20,30]
   */
  ageList?: number[];

  /** 평가자 연령대 차트 */
  ageRangesChart?: StackBarChart;

  /**
   * 카테고리 목록
   * @example ["TV","영화"]
   */
  categoryList?: string[];

  /**
   * 의뢰 완료 기간
   * @example 2021-01-01 ~ 2021-01-02
   */
  completeDate?: string;

  /**
   * 의뢰 콘텐츠명
   * @example 의뢰 명
   */
  contentNm?: string;
  error?: ErrorVo;

  /**
   * 평가인원 수
   * @format int32
   * @example 1000
   */
  evaluatorTotalCnt?: number;

  /** 하이라이트 차트 */
  highlightChart?: StackBarChart;

  /**
   * 영상 제목
   * @example 영상 제목
   */
  mvpSubject?: string;

  /**
   * 설문 수
   * @format int32
   * @example 3
   */
  qestnrTotalCnt?: number;

  /**
   * 영상 다시보기
   * @example 의뢰 명
   */
  replayYn?: string;

  /**
   * 의뢰 명
   * @example 의뢰 명
   */
  reqestNm?: string;

  /**
   * 의뢰 상품(B:베이직 P:프리미엄)
   * @example B
   */
  reqestTy?: string;

  /**
   * 의뢰 상품명
   * @example 베이직
   */
  requestTyNm?: string;

  /** 질문 차트(프리미엄만) */
  researchChart?: AdminClientResearchResponse;

  /** 평가자 성별 분포 */
  sexdstnChart?: StackBarChart;

  /** 별점 평가(~~어떠신가요) */
  star001Chart?: StackBarChart;

  /** 별점 평가(~~기대되시나요) */
  star002Chart?: StackBarChart;

  /**
   * 별점 평균
   * @format float
   * @example 3.5
   */
  startScoreAverage?: number;

  /**
   * 유튜브 링크
   * @example http://test.com
   */
  url?: string;

  /**
   * 영상 재생 시간(초)
   * @format int32
   * @example 1000
   */
  vidoRevivTime?: number;

  /**
   * 영상 형태
   * @example youtube 링크
   */
  vidoTypeNm?: string;

  /**
   * 조회 수
   * @format int32
   * @example 1000
   */
  viewTotalCnt?: number;

  /** 워드클라우드(장면으로 뽑은 이유) */
  wordCloud001?: AdminClientReportBasicWordCloudSearchResponse[];

  /** 워드클라우드(소감) */
  wordCloud002?: AdminClientReportBasicWordCloudSearchResponse[];
}

export interface AdminClientResearchResponse {
  /** 프리미엄 의뢰서일 경우 모든 질문의 대한 차트 리스트 */
  dataList?: object[];
}

/**
 * 관리자 - 쿠폰 상세 Response VO
 */
export interface AdminCouponInfoResponse {
  /**
   * 쿠폰 코드
   * @example ILOVEWEWILL
   */
  couponCode?: string;

  /**
   * 쿠폰 할인 정보1
   * @example 40 % 할인
   */
  couponDscntInfo1?: string;

  /**
   * 쿠폰 할인 정보2
   * @example 최대 50,000원까지 할인 가능
   */
  couponDscntInfo2?: string;

  /**
   * 쿠폰 할인 혜택(퍼센트 할인(DC) or 금액할인(FD))
   * @example DC
   */
  couponDscntKnd?: string;

  /**
   * 쿠폰 발행일
   * @example 2021-04-20 12:00:00
   */
  couponIsuDt?: string;

  /**
   * 쿠폰 명
   * @example 의뢰 40% 할인 쿠폰
   */
  couponName?: string;

  /**
   * 쿠폰 발급자
   * @example 김태리 (audit1735)
   */
  couponPblisr?: string;

  /**
   * 쿠폰 유형(관리자가 회원에게 등록(AD) or 회원이 스스로 등록(CL))
   * @example AD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용 기간 종류(PD(기간), DC(일수))
   * @example PD
   */
  couponUsePdKnd?: string;

  /**
   * 쿠폰 사용 가능 기간 정보1
   * @example 2021-04-21 12:00 ~ 2021-04-26 23:00
   */
  couponUsePeriodInfo1?: string;

  /**
   * 쿠폰 사용 가능 기간 정보2
   * @example 사용 가능일:2021-04-19 12:00 까지
   */
  couponUsePeriodInfo2?: string;

  /**
   * 쿠폰 사용 가능 명수
   * @example 3,000
   */
  couponUsePosblCnt?: string;
  error?: ErrorVo;

  /**
   * 최소 결제 금액
   * @example 50,000원 이상일 경우 결제 시 사용 가능
   */
  mummSetleAmount?: string;

  /**
   * 쿠폰 고유ID
   * @format int32
   */
  no?: number;
}

/**
 * 쿠폰 Insert Request VO
 */
export interface AdminCouponInsertRequest {
  /**
   * 쿠폰 코드
   * @example E794E74A
   */
  couponCode?: string;

  /**
   * 쿠폰 할인 금액(couponDscntKnd : 쿠폰 할인 혜택이 금액할인(FD)일때만 입력 받음
   * @format int32
   * @example 500000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 혜택(퍼센트 할인(DC) or 금액할인(FD))
   * @example DC
   */
  couponDscntKnd?: string;

  /**
   * 쿠폰 할인 한도
   * @format int32
   * @example 300000
   */
  couponDscntLmt?: number;

  /**
   * 쿠폰 할인율(couponDscntKnd : 쿠폰 할인 혜택이 퍼센트 할인(DC)일때만 입력 받음
   * @example 20
   */
  couponDscntRate?: number;

  /**
   * 쿠폰 발행 수
   * @format int32
   * @example 50000
   */
  couponIsuCount?: number;

  /**
   * 쿠폰 명
   * @example 100일 기념 이벤트 쿠폰
   */
  couponName?: string;

  /**
   * 쿠폰 유형(관리자가 회원에게 등록(AD) or 회원이 스스로 등록(CL))
   * @example AD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용 기간 시작 시간
   * @example 01
   */
  couponUseBeginTime?: string;

  /**
   * 쿠폰 사용 기간 시작일자
   * @format date
   * @example 2021-04-21
   */
  couponUseBgnde?: string;

  /**
   * 쿠폰 사용 조건 상세(코드 or 값등)
   * @example 100000
   */
  couponUseCndlDetail?: string;

  /**
   * 쿠폰 사용 조건 종류(의뢰 카테고리 또는 금액등의 기준)
   * @example AMOUNT
   */
  couponUseCndlKnd?: string;

  /**
   * 쿠폰 사용 가능 일수
   * @example 30
   */
  couponUseDaycnt?: string;

  /**
   * 쿠폰 사용 기간 종료 시간
   * @example 23
   */
  couponUseEndTime?: string;

  /**
   * 쿠폰 사용 기간 종료일자
   * @format date
   * @example 2021-05-20
   */
  couponUseEndde?: string;

  /**
   * 쿠폰 사용 기간 종류(PD(기간), DC(일수))
   * @example PD
   */
  couponUsePdKnd?: string;
}

/**
 * 쿠폰 Insert Response VO
 */
export interface AdminCouponInsertResponse {
  /**
   * 쿠폰 코드
   * @example E794E74A
   */
  couponCode?: string;

  /**
   * 쿠폰 할인 금액(couponDscntKnd : 쿠폰 할인 혜택이 금액할인(FD)일때만 입력 받음
   * @format int32
   * @example 500000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 혜택(퍼센트 할인(DC) or 금액할인(FD))
   * @example DC
   */
  couponDscntKnd?: string;

  /**
   * 쿠폰 할인 한도
   * @format int32
   * @example 300000
   */
  couponDscntLmt?: number;

  /**
   * 쿠폰 할인율(couponDscntKnd : 쿠폰 할인 혜택이 퍼센트 할인(DC)일때만 입력 받음
   * @example 20
   */
  couponDscntRate?: number;

  /**
   * 쿠폰 발행 수
   * @format int32
   * @example 50000
   */
  couponIsuCount?: number;

  /**
   * 쿠폰 발행일
   * @format date
   * @example 2021-04-20
   */
  couponIsuDt?: string;

  /**
   * 쿠폰 명
   * @example 1000
   */
  couponName?: string;

  /**
   * 쿠폰 발급자 ID
   * @format int32
   * @example 1000
   */
  couponPblisr?: number;

  /**
   * 쿠폰 유형(관리자가 회원에게 등록(AD) or 회원이 스스로 등록(CL))
   * @example AD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용 기간 시작 시간
   * @example 01
   */
  couponUseBeginTime?: string;

  /**
   * 쿠폰 사용 기간 시작일자
   * @format date
   * @example 4
   */
  couponUseBgnde?: string;

  /**
   * 쿠폰 사용 조건 상세(코드 or 값등)
   * @example 100000
   */
  couponUseCndlDetail?: string;

  /**
   * 쿠폰 사용 조건 종류(의뢰 카테고리 또는 금액등의 기준)
   * @example AMOUNT
   */
  couponUseCndlKnd?: string;

  /**
   * 쿠폰 사용 가능 일수
   * @example 30
   */
  couponUseDaycnt?: string;

  /**
   * 쿠폰 사용 기간 종료 시간
   * @example 23
   */
  couponUseEndTime?: string;

  /**
   * 쿠폰 사용 기간 종료일자
   * @format date
   * @example DFCD3454BBEA788A751A
   */
  couponUseEndde?: string;

  /**
   * 쿠폰 사용 기간 종류(PD(기간), DC(일수))
   * @example PD
   */
  couponUsePdKnd?: string;
  error?: ErrorVo;

  /**
   * 쿠폰 고유ID
   * @format int32
   */
  no?: number;
}

/**
 * 쿠폰 Response VO
 */
export interface AdminCouponResponse {
  /**
   * 쿠폰 할인 혜택
   * @example 퍼센트 할인 (40%) 50000원이상/10000원까지
   */
  couponDscnt?: string;

  /**
   * 발급명수
   * @example 3,000 명
   */
  couponIsuCount?: string;

  /**
   * 쿠폰 발행일
   * @example 2021-04-20
   */
  couponIsuDt?: string;

  /**
   * 쿠폰 명
   * @example 의뢰 40% 할인 쿠폰
   */
  couponName?: string;

  /**
   * 쿠폰 발급자
   * @example 김태희 (abjpp123)
   */
  couponPblisr?: string;

  /**
   * 쿠폰상태("":전체, A:활성, C:종료)
   * @example A
   */
  couponSttus?: string;

  /**
   * 쿠폰상태("":전체, A:활성, C:종료)
   * @example 활성
   */
  couponSttusNm?: string;

  /**
   * 쿠폰 유형(관리자가 회원에게 등록(AD) or 회원이 스스로 등록(CL))
   * @example AD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용 가능 기간
   * @example 2021-04-21 12:00 ~ 2021-04-26 23:00
   */
  couponUsePeriod?: string;

  /**
   * 쿠폰 고유ID
   * @format int32
   */
  no?: number;

  /**
   * 순번
   * @format int32
   */
  rownum?: number;
}

/**
 * 상세조회 Response Vo
 */
export interface AdminCreatorsInfoResponse {
  afList?: AFReponse[];

  /**
   * 답변내용
   * @example 답변입니다
   */
  answerCn?: string;

  /**
   * 답변제목
   * @example 답변 제목입니다.
   */
  answerSj?: string;

  /**
   * 담당자
   * @format int32
   * @example 1
   */
  charger?: number;

  /**
   * 이관팀
   * @example 영업:001
   */
  chrgProcessDept?: string;

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * 확인 날짜
   * @example 2021-02-02 10:20:30
   */
  confirmDt?: string;

  /**
   * cs카테고리대 ([csoo3] 001:의뢰자, 002:평가자, 003:비회원의뢰자, 004:비회원평가자)
   * @example 001
   */
  csCategoryLrge?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 파일 첨부 리스트
   * @example [1,2]
   */
  csfList?: CSFResponse[];

  /**
   * 미확인 상태가 3일이 지났을 경우
   * @example Y
   */
  delayflag?: string;

  /**
   * 회원 email
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /** @format int32 */
  id?: number;

  /**
   * 담당자id
   * @example mghjyoon
   */
  mgLoginId?: string;

  /**
   * 담당자이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 회원 전화번호
   * @example 01033334444
   */
  mobileNo?: string;

  /**
   * 자신이면Y,아니면N
   * @example Y
   */
  myflag?: string;

  /**
   * 회원 이름
   * @example 윤효진
   */
  name?: string;

  /**
   * 비회원 회사명
   * @example 블록스미스
   */
  nonCapnyNm?: string;

  /**
   * 비회원 부서명
   * @example 영업
   */
  nonFatherSign?: string;

  /**
   * 답변완료일시
   * @example 2021-02-02 10:20:30
   */
  processComptDt?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰상태 ([IN001]10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @format int32
   * @example 심사 대기
   */
  reqestStatus?: number;

  /**
   * role flag
   * @example 부서장:H 부서원:S
   */
  roleFlag?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;

  /**
   * 회원id
   * @example hjyoon
   */
  userId?: string;
}

export interface AdminCustomerInsertAnswerRequest {
  /**
   * 답변내용
   * @example 답변 내용입니다.
   */
  answerCn?: string;

  /**
   * 답변제목
   * @example 답변 제목입니다.
   */
  answerSj?: string;

  /**
   * 담당자
   * @format int32
   */
  charger?: number;

  /**
   * 이관팀
   * @example 001
   */
  chrgProcessDept?: string;

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * 회원 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;

  /**
   * 회원id
   * @format int32
   * @example 5
   */
  memberId?: number;

  /**
   * 저장,발송 구분값
   * @example 저장:S1/발송:S2
   */
  saveFlag?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 30
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;

  /**
   * 회원 이름
   * @example 윤효진
   */
  username?: string;
}

export interface AdminCustomerInsertAnswerResponse {
  /**
   * 답변내용
   * @example 답변 내용입니다.
   */
  answerCn?: string;

  /**
   * 답변제목
   * @example 답변 제목입니다.
   */
  answerSj?: string;

  /**
   * 담당자
   * @format int32
   * @example 1
   */
  charger?: number;

  /**
   * 이관팀
   * @example 영업:001
   */
  chrgProcessDept?: string;

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;
  error?: ErrorVo;

  /**
   * 중요도 (상중하)
   * @example 하
   */
  ipcr?: string;

  /**
   * 회원id
   * @format int32
   * @example 5
   */
  memberId?: number;

  /**
   * 처리완료일시
   * @format date-time
   * @example 2021-02-02 10:20:30
   */
  processComptDt?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 20
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;
}

/**
 * 창작자/공유자 목록조회 Response
 */
export interface AdminCustomerSearchResponse {
  /**
   * 이관팀
   * @example 영업:001
   */
  chrgProcessDept?: string;

  /**
   * cs카테고리대 ([cs003] 001:의뢰자, 002:평가자, 003:비회원의뢰자, 004:비회원평가자)
   * @example 001
   */
  csCategoryLrge?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 미확인 상태가 3일이 지났을 경우
   * @example Y
   */
  delayflag?: string;

  /**
   * 회원이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 담당자id
   * @example hjyoon
   */
  mgLoginId?: string;

  /**
   * 담당자이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 회원이름
   * @example 이태수
   */
  name?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;

  /**
   * 상태
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;

  /**
   * 회원계정명
   * @example hjyoon
   */
  username?: string;
}

export interface AdminEvaluatorBasicInsertRequest {
  /**
   * 환불 받을 계좌번호
   * @example 123-123-123
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울 강남구
   */
  address?: string;

  /**
   * 환불 받을 은행명
   * @example XX은행
   */
  bankNm?: string;

  /**
   * 생일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 상세 주소
   * @example 17-11
   */
  detailaddress?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 등급
   * @example W1
   */
  grad?: string;

  /**
   * 가입경로
   * @example 친구 추천
   */
  joinpath?: string;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 비밀번호
   * @example 1234
   */
  password?: string;

  /**
   * 등록 아이디
   * @format int32
   * @example 1
   */
  regId?: number;

  /**
   * 서비스
   * @example W
   */
  service?: string;

  /**
   * 성별
   * @example W or F
   */
  sexdstn?: string;

  /**
   * 이용약관 식별자(배열)
   * @example [1,2,3]
   */
  termsIds?: number[];

  /**
   * 계정 아이디
   * @example test
   */
  username?: string;
}

export interface AdminEvaluatorBasicInsertResponse {
  /**
   * 환불 받을 계좌번호
   * @example 123-123-123
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울 강남구
   */
  address?: string;

  /**
   * 환불 받을 은행명
   * @example XX은행
   */
  bankNm?: string;

  /**
   * 생일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 상세 주소
   * @example 17-11
   */
  detailaddress?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 등급
   * @example W1
   */
  grad?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 가입경로
   * @example 친구 추천
   */
  joinpath?: string;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 계정 아이디
   * @example test
   */
  username?: string;
}

export interface AdminEvaluatorContractDeleteRequest {
  /**
   * 계약서 아이디
   * @format int32
   * @example 1
   */
  contractId?: number;
}

export interface AdminEvaluatorContractInfoResponse {
  /**
   * 첨부파일 확장자 명
   * @example pdf
   */
  attachFileExt?: string;

  /**
   * 첨부파일 아이디
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 첨부파일 명
   * @example 첨부파일
   */
  attachFileName?: string;

  /**
   * 첨부파일 다운 URL
   * @example http://test.com
   */
  attachFileUrl?: string;

  /**
   * 계약서 아이디
   * @format int32
   * @example 1
   */
  contractId?: number;

  /**
   * 계약서 구분
   * @example 001:위촉계약서 002:비밀유지계약서 003:윤리서약서
   */
  contractSection?: string;

  /**
   * 계약서 등록일
   * @example 1990-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 계약서 구분 한글명
   * @example 1
   */
  sectionKr?: string;
}

export interface AdminEvaluatorContractInsertRequest {
  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 계약서명
   * @example 위촉계약서
   */
  contractNm?: string;

  /**
   * 계약서 구분
   * @example 001:위촉계약서 002:비밀유지계약서 003:윤리서약서
   */
  contractSection?: string;

  /**
   * 설명
   * @example 계약서 설명
   */
  description?: string;
}

export interface AdminEvaluatorDailyAnswersResponse {
  /**
   * 설문명
   * @example 설문명
   */
  dc?: string;

  /**
   * 내 답변
   * @example 답변(해시태그)
   */
  myAnswer?: string;

  /**
   * 포인트
   * @format int32
   * @example 10
   */
  point?: number;

  /**
   * 질문명
   * @example 설문명
   */
  qestnCn?: string;

  /**
   * 설문 완료일
   * @format date-time
   * @example 19990-01-01 00:00:00
   */
  regDt?: string;
}

export interface AdminEvaluatorEvaluationResponse {
  /**
   * 성실/불성실(Y,N)
   * @example Y
   */
  dishonestEvlYn?: string;

  /**
   * 평가 종료일
   * @example 1990-01-01 00:00:00
   */
  evlEndDt?: string;

  /**
   * 평가 시작일
   * @example 1990-01-01 00:00:00
   */
  evlStartDt?: string;

  /**
   * 평가 상태
   * @example 1
   */
  evlStatus?: string;

  /**
   * 평가 상태명
   * @example 1
   */
  evlStatusNm?: string;

  /**
   * 의뢰 고유번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 포인트
   * @format int32
   * @example 1
   */
  point?: number;

  /**
   * 다시보기(Y,N)
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰 명
   * @example 1
   */
  reqestNm?: string;

  /**
   * 의뢰 신청일
   * @example 1990-01-01 00:00:00
   */
  reqestReqstDt?: string;

  /** 의뢰 콘텐츠(B:베이직 P:프리미엄) */
  reqestTy?: string;
}

export interface AdminEvaluatorEvlCntResponse {
  /**
   * 불성실 답변 수
   * @format int32
   * @example 10
   */
  dishonestCnt?: number;

  /**
   * 불성실 답변율
   * @format float
   * @example 33.3
   */
  dishonestPercent?: number;

  /**
   * 불성실 답변(system) 수
   * @format int32
   * @example 10
   */
  dishonestSystemCnt?: number;

  /**
   * 불성실 답변율(system)
   * @format float
   * @example 33.3
   */
  dishonestSystemPercent?: number;
  error?: ErrorVo;

  /**
   * 성실 답변 수
   * @format int32
   * @example 10
   */
  honestCnt?: number;

  /**
   * 성실 답변율
   * @format float
   * @example 33.3
   */
  honestPercent?: number;

  /**
   * 총 카운트
   * @format int32
   * @example 30
   */
  totalCnt?: number;
}

export interface AdminEvaluatorMemberInfoResponse {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 1
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울 강남구
   */
  address?: string;

  /**
   * 광고성 정보 수신 동의 여부(Y,N)
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 광고성 정보 수신 동의 시간
   * @example 1900-01-01 00:00:00
   */
  advrtsProvisionChangeDt?: string;

  /**
   * 대리점 아이디
   * @format int32
   * @example 1
   */
  agencyId?: number;

  /**
   * 은행코드[ coudeGroup : CM010 ]
   * @example 001
   */
  bankCode?: string;

  /**
   * 은행명
   * @example 은행명
   */
  bankNm?: string;

  /**
   * 생일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 등급점수
   * @format int32
   * @example 1
   */
  cnfdncScore?: number;

  /**
   * 신고한 횟수
   * @format int32
   * @example 1
   */
  declarationCn?: number;

  /**
   * 회원상태(Y:활성 N:탈퇴)
   * @example Y
   */
  delYn?: string;

  /**
   * 상세 주소
   * @example 17-11
   */
  detailaddress?: string;

  /**
   * 불성실 답변 횟수
   * @format int32
   * @example 1
   */
  dishonestEvlCnt?: number;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 필수 영상 시청 완료 시간
   * @example 1900-01-01 00:00:00
   */
  essntlVidoCtyhllComptDt?: string;

  /**
   * 필수 영상 시청 여부(Y,N)
   * @example Y
   */
  essntlVidoCtyhllYn?: string;

  /**
   * 평가자 점수
   * @format int32
   * @example 1
   */
  evlManScore?: number;

  /**
   * 등급
   * @example W1
   */
  grad?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 변경자
   * @example 홍길동
   */
  modName?: string;

  /**
   * 변경자 아이디
   * @example 홍길동
   */
  modUsername?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 비밀번호 변경일
   * @example 1900-01-01 00:00:00
   */
  passwordChangeDt?: string;

  /**
   * 불성실 답변율
   * @format float
   * @example 1
   */
  percent?: number;

  /**
   * 플래너 아이디
   * @format int32
   * @example 1
   */
  plannerId?: number;

  /**
   * 포인트
   * @format int32
   * @example 1
   */
  point?: number;

  /**
   * 최근접속일
   * @example 1900-01-01 00:00:00
   */
  recentConectDt?: string;

  /**
   * 추천인 아이디
   * @format int32
   * @example 1
   */
  recomendrId?: number;

  /**
   * 탈퇴 사유
   * @example 홍길동
   */
  secessionContents?: string;

  /**
   * 탈퇴일
   * @example 1900-01-01 00:00:00
   */
  secessionDt?: string;

  /**
   * 성별(M:남 F:여)
   * @example F
   */
  sexdstn?: string;

  /**
   * 가입일
   * @example 1900-01-01 00:00:00
   */
  subscribeDt?: string;

  /**
   * 오늘 얻은 포인트
   * @format int32
   * @example 1
   */
  todayPoint?: number;

  /**
   * 총 평가 횟수
   * @format int32
   * @example 1
   */
  totalEvlCnt?: number;

  /**
   * 사용자 계정
   * @example blocksmith
   */
  username?: string;

  /**
   * 출금 완료 포인트
   * @format int32
   * @example 1
   */
  withdrawPoint?: number;
}

export interface AdminEvaluatorMemberListResponse {
  /**
   * 주소
   * @example 서울 강남구
   */
  address?: string;

  /**
   * 생일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 상태(Y:활성 N:탈퇴)
   * @example Y
   */
  delYn?: string;

  /**
   * 상세 주소
   * @example 17-11
   */
  detailaddress?: string;

  /**
   * 불성실 답변율(%)
   * @format float
   * @example 10
   */
  dishonestPercent?: number;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 등급점수
   * @format int32
   * @example Y
   */
  evlManScore?: number;

  /**
   * 등급
   * @example W1
   */
  grad?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 포인트
   * @format int32
   * @example 1
   */
  point?: number;

  /**
   * 최근 접속일
   * @example 20200101 00:00:00
   */
  recentConectDt?: string;

  /**
   * 성별(M:남 F:여)
   * @example M
   */
  sexdstn?: string;

  /**
   * 가입일
   * @example 20200101 00:00:00
   */
  subscribeDt?: string;

  /**
   * 사용자 계정
   * @example blocksmith
   */
  username?: string;
}

export interface AdminEvaluatorMemberModifyRequest {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 1123123
   */
  acnutno?: string;

  /**
   * 은행코드[코드그룹 : CM010 ]
   * @example 001
   */
  bankCode?: string;

  /**
   * 생일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 등급
   * @example W1
   */
  grad?: string;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 성별(M:남 F:여)
   * @example F
   */
  sexdstn?: string;
}

export interface AdminEvaluatorPointResponse {
  /**
   * 성실/불성실
   * @example 불성실
   */
  dishonestEvl?: string;
  error?: ErrorVo;

  /**
   * 처리자
   * @example 홍길동(asdf1234)
   */
  operator?: string;

  /**
   * 포인트
   * @example + 2,500
   */
  point?: string;

  /**
   * 포인트 상태
   * @example 적립 완료
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 출금 신청 완료
   */
  reason?: string;

  /**
   * 날짜
   * @example 2021.01.01 00:00:00
   */
  regDt?: string;

  /**
   * 영상/제품명
   * @example 포맨 남자 향수
   */
  reqestNm?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;
}

/**
 * 상세조회 Response Vo
 */
export interface AdminMarkersInfoResponse {
  afList?: AFReponse[];

  /**
   * 답변내용
   * @example 답변 내용 입니다
   */
  answerCn?: string;

  /**
   * 답변제목
   * @example 답변 제목 입니다
   */
  answerSj?: string;

  /**
   * 담당자
   * @format int32
   * @example 1
   */
  charger?: number;

  /**
   * 이관팀
   * @example 영업:001
   */
  chrgProcessDept?: string;

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * 확인 날짜
   * @example 2021-02-02 10:20:30
   */
  confirmDt?: string;

  /**
   * cs카테고리대 ([csoo3] 001:의뢰자, 002:평가자, 003:비회원의뢰자, 004:비회원평가자)
   * @example 001
   */
  csCategoryLrge?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 파일 첨부 리스트
   * @example [1,2]
   */
  csfList?: CSFResponse[];

  /**
   * 미확인 상태가 3일이 지났을 경우
   * @example Y
   */
  delayflag?: string;

  /**
   * 회원 email
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /** @format int32 */
  id?: number;

  /**
   * 담당자id
   * @example hjyoon
   */
  mgLoginId?: string;

  /**
   * 담당자이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 회원 전화번호
   * @example 01033334444
   */
  mobileNo?: string;

  /**
   * 자신이면Y
   * @example Y
   */
  myflag?: string;

  /**
   * 회원 이름
   * @example 윤효진
   */
  name?: string;

  /**
   * 답변완료일시
   * @example 2021-02-02 10:20:30
   */
  processComptDt?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 선택한 의뢰 제목
   * @format int32
   * @example 독립영화 평가 의뢰
   */
  reqestSj?: number;

  /**
   * role flag
   * @example 부서장:H 부서원:S
   */
  roleFlag?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;

  /**
   * 회원id
   * @example hjyoon
   */
  userId?: string;
}

export interface AdminMemberSecessionRequest {
  /**
   * id 배열
   * @example [1,2]
   */
  ids?: number[];
}

/**
 * 상세조회 Response Vo
 */
export interface AdminMonitorsInfoResponse {
  afList?: AFReponse[];

  /**
   * 답변내용
   * @example 답변입니다
   */
  answerCn?: string;

  /**
   * 답변제목
   * @example 답변 제목입니다.
   */
  answerSj?: string;

  /**
   * 담당자
   * @format int32
   * @example 1
   */
  charger?: number;

  /**
   * 이관팀
   * @example 영업:001
   */
  chrgProcessDept?: string;

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * 확인 날짜
   * @example 2021-02-02 10:20:30
   */
  confirmDt?: string;

  /**
   * 영업 확인 상태
   * @example Y
   */
  confirmYn?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /** 파일 첨부 리스트 */
  csfList?: CSFResponse[];

  /**
   * 미확인 상태가 3일이 지났을 경우
   * @example Y
   */
  delayflag?: string;

  /**
   * 회원 email
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /** @format int32 */
  id?: number;

  /**
   * 담당자id
   * @example mghjyoon
   */
  mgLoginId?: string;

  /**
   * 담당자이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 회원 전화번호
   * @example 01033334444
   */
  mobileNo?: string;

  /**
   * 자신이면Y,아니면N
   * @example Y
   */
  myflag?: string;

  /**
   * 회원 이름
   * @example 윤효진
   */
  name?: string;

  /**
   * 비회원 회사명
   * @example 블록스미스
   */
  nonCapnyNm?: string;

  /**
   * 비회원 부서명
   * @example 영업
   */
  nonFatherSign?: string;

  /**
   * 답변완료일시
   * @example 2021-02-02 10:20:30
   */
  processComptDt?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰상태 ([IN001]10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @format int32
   * @example 50:심사 대기
   */
  reqestStatus?: number;

  /**
   * 영업담당자id
   * @example hongkk
   */
  smgLoginId?: string;

  /**
   * 영업담당자이름
   * @example 홍길동
   */
  smgName?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;

  /**
   * 회원id
   * @example hjyoon
   */
  userId?: string;
}

export interface AdminMonitorsSearchResponse {
  /**
   * 담당자
   * @format int32
   * @example 12
   */
  charger?: number;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 회원이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /** @format int32 */
  id?: number;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 담당자id
   * @example hjyoon
   */
  mgLoginId?: string;

  /**
   * 담당자이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 회원전화번호
   * @example 010-3333-4444
   */
  mobileNo?: string;

  /**
   * 내 문의
   * @example Y 또는 N
   */
  myCs?: string;

  /**
   * 회원이름
   * @example 윤효진
   */
  name?: string;
  order?: string[];

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;

  /**
   * 영업담당자no
   * @format int32
   * @example 114
   */
  salesCharger?: number;

  /**
   * 영업담당자id
   * @example hongkk
   */
  smgLoginId?: string;

  /**
   * 영업담당자이름
   * @example 홍길동
   */
  smgName?: string;
  sort?: string[];

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;

  /**
   * 회원계정명
   * @example hjyoon
   */
  username?: string;
}

export interface AdminPointListResponse {
  /**
   * 성실/불성실
   * @example 불성실
   */
  dishonestEvl?: string;

  /**
   * 이름(아이디)
   * @example 고길동(asdf1342)
   */
  nameId?: string;

  /**
   * 처리자
   * @example 홍길동(asdf1234)
   */
  operator?: string;

  /**
   * 포인트 상태
   * @example 적립 완료
   */
  pointSttusNm?: string;

  /**
   * 포인트
   * @example + 2,500
   */
  pointTxt?: string;

  /**
   * 사유
   * @example 출금 신청 완료
   */
  reason?: string;

  /**
   * 날짜
   * @example 2021.01.01 00:00:00
   */
  regDt?: string;

  /**
   * 영상/제품명
   * @example 포맨 남자 향수
   */
  reqestNm?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;
}

/**
 * 포인트 관리 > 포인트 적립/차감 Insert Request VO
 */
export interface AdminPointManageInsertRequest {
  /**
   * 사용자 id
   * @example 1000
   */
  memberIdArr?: number[];

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 포인트 종류 PAY : 결제, EVL : 평가, EVT : 이벤트, DEF : 출금, ADM : 관리자, DAY: 데일리평가
   * @example ADM
   */
  pointKnd?: string;

  /**
   * 포인트 상태 A: 적립, U: 차감
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;
}

/**
 * 포인트 마스터 Insert Response VO
 */
export interface AdminPointManageResponse {
  /**
   * 출금 완료 포인트
   * @example 30,000
   */
  accmltDefPoint?: string;

  /**
   * 생년월일
   * @format date
   * @example 1985-01-01
   */
  birthday?: string;

  /**
   * 이메일
   * @example example@blocksmith.xyz
   */
  email?: string;

  /**
   * 평가자 고유번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 010-4117-2114
   */
  mbtlnum?: string;

  /**
   * 의뢰자명
   * @example 강하늘
   */
  name?: string;

  /**
   * 최종 접속일
   * @example 2021-04-20 12:00:00
   */
  recentConectDt?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;

  /**
   * 성별명
   * @example M:남, F:여
   */
  sexdstnNm?: string;

  /**
   * 가입일
   * @example 2021-04-20 12:00:00
   */
  subscribeDt?: string;

  /**
   * 오늘 얻은 포인트
   * @example 30,000
   */
  todayAccmlPoint?: string;

  /**
   * 보유 포인트
   * @example 30,000
   */
  usePosblPoint?: string;

  /**
   * 로그인id
   * @example acbd123
   */
  username?: string;
}

export interface AdminQratingModifyRequest {
  /**
   * 큐레이팅ID
   * @format int32
   * @example 1
   */
  id: number;

  /** 큐레이팅명 */
  qratingNm: string;
}

export interface AdminQratingSearchResponse {
  /**
   * 큐레이팅 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  num?: number;

  /** 큐레이팅 명 */
  qratingNm?: string;
}

export interface AdminReportAllAnswerRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 모든 답변 질문 타입(H:평가자들이 해당 구간을 가장 인상깊은 장면으로 뽑은 이유 R:영상을 본 소감)
   * @example H
   */
  type?: string;
}

export interface AdminReportAnswerRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;
}

export interface AdminReportAnswerResponse {
  /**
   * 연령
   * @example 30대
   */
  age?: string;

  /**
   * 답변 내용
   * @example 답변 내용
   */
  answerCn?: string;

  /**
   * 성별
   * @example 남성
   */
  sexdstnNm?: string;

  /**
   * 아이디
   * @example bl***
   */
  username?: string;
}

export interface AdminReportStarScoreRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 필터( 연령별 :A 성별:S
   * @example A
   */
  filter?: string;

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 별점 설문 구분값(001:전반적 평가 002:기대)
   * @example 1
   */
  starType?: string;
}

export interface AdminWordCloudChart {
  /**
   * 차트 연령별 조건(전체: 모든 연령대 배열에 넣어주어야 함)
   * @example [10,20,30,40]
   */
  ageList?: number[];

  /**
   * 질문 타입(답변유형 ([CL002] 001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지))
   * @example 001
   */
  answerTy?: string;

  /**
   * 질문 타입명
   * @example 객관식
   */
  answerTyNm?: string;
  data?: WordCloud[];
  error?: ErrorVo;

  /**
   * 평가 수
   * @format int32
   * @example 100
   */
  evlCnt?: number;

  /**
   * 설문 타입
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문 타입명
   * @example 001
   */
  evlMthdNm?: string;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 차트 성별 조건(전체:A 남:M 여:F)
   * @example A
   */
  sexdstn?: string;

  /**
   * 차트 제목
   * @example 차트 제목
   */
  title?: string;
}

export interface AdminWordCloudRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 워드 크라우드 종류(H:~~뽑은 이유 R:소감)
   * @example H
   */
  type?: string;
}

/**
 * 질문 답변 정보
 */
export interface Answer {
  /**
   * 답변내용
   * @example 기분좋음
   */
  answerCn: string;

  /**
   * 파일아이디
   * @format int32
   * @example 1000000
   */
  attachFileId?: number;

  /**
   * n문항으로 이동 설정. qestnNo 값 입력
   * @format int32
   */
  skipToQestnNo?: number;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  sn: number;

  /**
   * 함정답변여부 (Y:함정질문, N:일반질문)
   * @example N
   */
  trapAnswerYn?: string;
}

export interface AnswerEntity {
  answerCn?: string;

  /** @format int32 */
  answerId?: number;

  /** @format int32 */
  attachFileId?: number;
  delYn?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnEntity?: QestnEntity;

  /** @format int32 */
  qestnId?: number;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;

  /** @format int32 */
  skipToQestnNo?: number;

  /** @format int32 */
  sn?: number;
  trapAnswerYn?: string;
}

export interface AnswerHashResponse {
  /**
   * 답
   * @example 액션
   */
  answerCn?: string;

  /**
   * 해시태그
   * @example 판타지
   */
  hashCol?: string;

  /**
   * 답변순서
   * @example 1
   */
  sn?: string;
}

export interface ApprovalInfoResponse {
  /** 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함) */
  age?: string;

  /** 의뢰코멘트 */
  comment?: string;

  /** 영상설명 */
  dc?: string;

  /** 영상업로드 cmaf엔드포인트 */
  egressEndpointCmaf?: string;

  /** 영상업로드 dash엔드포인트 */
  egressEndpointDash?: string;

  /** 영상업로드 his엔드포인트 */
  egressEndpointHls?: string;

  /** 영상업로드 mss엔드포인트 */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /** 평가자구성 (001: 균등분포) */
  evaluatorComposition?: string;

  /** 영상업로드 첨부파일명 */
  fileName?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlCo?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /**
   * 심사비용
   * @format int32
   */
  jdgmnCtUntpc?: number;

  /** 최종평가설정 (001: 영상평가, 002: 영상출연자 평가) */
  lastEvlEstbs?: string;

  /** 영상제목 */
  mvpSubject?: string;

  /** 영상URL */
  mvpUrl?: string;

  /** 의뢰자명 */
  name?: string;

  /**
   * 포인트
   * @format int32
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   */
  prductAmount?: number;

  /** 제품카테고리 */
  prductCategorys?: ApprovalPrductCategoryResponse[];

  /** 제폼설명 */
  prductDc?: string;

  /** 제품 첨부파일 */
  prductFiles?: ApprovalPrductFileResponse[];

  /** 제품 이미지 */
  prductImages?: ApprovalPrductFileResponse[];

  /** 제폼명 */
  prductNm?: string;

  /** 제품 썸네일 */
  prductTumbnails?: ApprovalObjectImageResponse[];

  /**
   * 견적서 식별자
   * @format int32
   */
  prqudoId?: number;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 견적보조담당자 */
  reAsstnCharger?: string;

  /** 견적확인담당자 */
  reCnfirmCharger?: string;

  /** 견적필수확인담당자 */
  reEssntlCnfirmCharger?: string;

  /**
   * 견적필수확인담당자ID
   * @format int32
   */
  reEssntlCnfirmChargerId?: number;

  /** 견적필수확인담당자확인여부 */
  reEssntlCnfirmYn?: string;

  /**
   * 의뢰견적 식별자
   * @format int32
   */
  reId?: number;

  /** 견적반려사유 */
  reReturnResn?: string;

  /** 영상다시보기 */
  replayYn?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태명 */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 응답수집비용단가
   * @format int32
   */
  rspnsColctCtUntpc?: number;

  /** 응답수집비용단가 노출여부 */
  rspnsColctCtUntpcViewYn?: boolean;

  /** 구분 (LK:LINK, UL:업로드) */
  se?: string;

  /** 평가자성별 */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   */
  splpcAm?: number;

  /** 영상업로드 썸네일 S3경로. 콤마구분 */
  thumbNails?: string;

  /** 영상업로드 썸네일 웹경로. 콤마구분 */
  thumbNailsUrls?: string;

  /**
   * 응답수집비용
   * @format int32
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 로그인ID */
  username?: string;

  /**
   * 부가가치세
   * @format int32
   */
  vat?: number;

  /** 영상카테고리 */
  vidoCategorys?: ApprovalVidoCategoryResponse[];

  /**
   * 영상 식별자
   * @format int32
   */
  vidoId?: number;

  /** 영상재생길이 */
  vidoRevivTime?: string;

  /** 영상썸네일 */
  vidoTumbnails?: ApprovalObjectImageResponse[];
}

export interface ApprovalObjectImageResponse {
  /** 영상 OR 제품 파일명 */
  fileName?: string;

  /** 영상 OR 제품 썸네일URL */
  fileUrl?: string;

  /**
   * 썸네일 식별자
   * @format int32
   */
  thumbId?: number;

  /** 영상 대표 썸네일 (Y이면 대표썸네일, 이외는 대표썸네일 아님) */
  useYn?: string;
}

export interface ApprovalPrductCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 제품카테고리 식별자
   * @format int32
   */
  prductCategoryId?: number;
}

export interface ApprovalPrductFileResponse {
  /** 제품 파일명 */
  fileName?: string;

  /** 제품 파일URL */
  fileUrl?: string;
}

export interface ApprovalSaveRequest {
  /**
   * 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트 입니다.
   */
  comment?: string;

  /**
   * 영상설명
   * @example 영상설명 입니다.
   */
  dc?: string;

  /**
   * 평가자구성 (001: 균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 심사비용
   * @format int32
   * @example 10000
   */
  jdgmnCtUntpc?: number;

  /**
   * 최종평가설정 (001: 영상평가, 002: 영상출연자 평가)
   * @example 001
   */
  lastEvlEstbs?: string;

  /**
   * 영상제목
   * @example 영상제목 입니다.
   */
  mvpSubject?: string;

  /**
   * 포인트
   * @format int32
   * @example 100
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   * @example 50000
   */
  prductAmount?: number;

  /**
   * 제품카테고리ID (내가 체크한것만 받음)
   * @example [2,3,4]
   */
  prductCategorys?: ApprovalPrductCategoryResponse[];

  /**
   * 제품 식별자
   * @format int32
   * @example 1
   */
  prductId?: number;

  /**
   * 제폼명
   * @example 핫식스
   */
  prductNm?: string;

  /**
   * 선택한 제품썸네일 식별자
   * @format int32
   * @example 1
   */
  prductThumbId?: number;

  /**
   * 견적서 식별자
   * @format int32
   * @example 1
   */
  prqudoId?: number;

  /**
   * 의뢰견적 식별자
   * @format int32
   */
  reId?: number;

  /** 견적반려사유 */
  reReturnResn?: string;

  /**
   * 영상다시보기 (Y/N)
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰컨텐츠 (V: 영상, P: 제품)
   * @example V
   */
  reqestCntnts?: string;

  /**
   * 의뢰제목
   * @example 의뢰제목입니다..
   */
  reqestNm?: string;

  /**
   * 응답수집비용단가
   * @format int32
   * @example 10000
   */
  rspnsColctCtUntpc?: number;

  /**
   * 평가자성별 (M/F)
   * @example M
   */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   * @example 10000
   */
  splpcAm?: number;

  /**
   * 응답수집비용
   * @format int32
   * @example 50000
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   * @example 10000
   */
  totSetleExpectAmount?: number;

  /**
   * 부가가치세
   * @format int32
   * @example 10000
   */
  vat?: number;

  /**
   * 영상카테고리ID (내가 체크한것만 받음)
   * @example [2,3,4]
   */
  vidoCategorys?: ApprovalVidoCategoryResponse[];

  /**
   * 영상 식별자
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 선택한 영상 썸네일 식별자
   * @format int32
   * @example 1
   */
  vidoThumbId?: number;
}

export interface ApprovalSearchResponse {
  error?: ErrorVo;

  /** 견적승인요청일 */
  estmtApprovalRqestDt?: string;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 의뢰자명 */
  name?: string;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 견적보조담당자 */
  reAsstnCharger?: string;

  /** 견적확인담당자 */
  reCnfirmCharger?: string;

  /** 견적필수확인담당자 */
  reEssntlCnfirmCharger?: string;

  /** 견적필수확인담당자확인여부 */
  reEssntlCnfirmYn?: string;

  /** 견적산출지연일 */
  reLazyDt?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /** 로그인ID */
  username?: string;

  /** 영상재생길이 */
  vidoRevivTime?: string;
}

export interface ApprovalVidoCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 의뢰카테고리 식별자
   * @format int32
   */
  reqestCategoryId?: number;
}

export interface AttachFileInfResponse {
  /** @format int32 */
  attachFileIdF?: number;
  fileExt?: string;
  fileName?: string;
  fileUrl?: string;
}

/**
 * 첨부파일 상세조회 Response
 */
export interface AttachFileInfoResponse {
  /**
   * 변경 파일명
   * @example 3e09fe25-14c6-4bf4-8e40-3439e828f644.txt
   */
  fileChgName?: string;

  /**
   * 확장자
   * @example txt
   */
  fileExt?: string;

  /**
   * 파일명
   * @example test
   */
  fileName?: string;

  /**
   * 파일 물리경로
   * @example /assets/wewill/dev/upload/board/event/20210312
   */
  filePath?: string;

  /**
   * 파일사이즈
   * @format int32
   * @example 15
   */
  fileSize?: number;

  /**
   * 파일 url
   * @example https://cuspcdn.s3.ap-northeast-2.amazonaws.com/assets/wewill/dev/upload/board/20210312/3e09fe25-14c6-4bf4-8e40-3439e828f644.txt
   */
  fileUrl?: string;

  /**
   * 파일id
   * @format int32
   * @example 1
   */
  id?: number;
}

/**
 * 회계(세금계산서&현금영수증) 상세조회 response
 */
export interface BillPublicationInfoResponse {
  /**
   * 결제일시
   * @example 2021-04-03 12:32:32
   */
  applyDt?: string;

  /**
   * 의뢰자구분
   * @example 법인
   */
  bizSection?: string;

  /**
   * 사업자등록증
   * @example 세금계산서:url정보, 현금영수증:123-32233-5343
   */
  bizrno?: string;

  /**
   * 의뢰카테고리
   * @example 영화,음식,도서
   */
  categoryList?: ReqCategoryResponse[];

  /**
   * 쿠폰사용금액
   * @format int32
   * @example 20000
   */
  couponDscntAmount?: number;

  /**
   * PG사 거래번호
   * @example 444333
   */
  delngNo?: string;

  /**
   * 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 연락처
   * @example 01034343233
   */
  mobileNo?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * PG사 승인번호
   * @example 23124
   */
  pgTid?: string;

  /**
   * 의뢰콘텐츠
   * @example 영상
   */
  reqestCntnts?: string;

  /**
   * 의뢰명
   * @example ㅇㅇㅇㅇ 의뢰평가
   */
  reqestNm?: string;

  /**
   * 결제수단
   * @example 카드결제
   */
  setleKnd?: string;

  /**
   * 최종결제금액
   * @format int32
   * @example 300000
   */
  setleReqestAmount?: number;

  /**
   * 주문번호
   * @example 23489-34334
   */
  settlementNo?: string;

  /**
   * 회원구분
   * @example 의뢰자
   */
  uesrSection?: string;

  /**
   * 아이디
   * @example testid
   */
  userName?: string;
}

/**
 * 발행여부 상태 다중 수정 request
 */
export interface BillStatusModifyRequest {
  /**
   * 발행여부[MA006]
   * @example 10
   */
  pblicteStatus?: string;

  /**
   * 주문번호
   * @example 20210414-214855-1
   */
  settlementNo?: string;
}

/**
 * 회계(세금계산서&현금영수증) 목록조회 response
 */
export interface BillpublicationSearchResponse {
  /**
   * 결제일
   * @example 2021-05-05
   */
  applyDt?: string;

  /**
   * 담당자
   * @example 차명환
   */
  chager?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 발행여부[MA006]
   * @example 신청
   */
  pblicteStatus?: string;

  /**
   * 의뢰명
   * @example 의뢰명의 의뢰
   */
  reqestNm?: string;

  /**
   * 신청일
   * @example 2021-05-05
   */
  rqstdt?: string;

  /**
   * 결제금액
   * @format int32
   * @example 100000
   */
  setleReqestAmount?: number;

  /**
   * 주문번호
   * @example 2348-2239
   */
  settlementNo?: string;

  /**
   * 아이디
   * @example testid
   */
  userName?: string;
}

/**
 * 국세청 홈택스 사업자등록번호 API 조회 Response Vo
 */
export interface BiznumResponse {
  /**
   * 사업자유형
   * @example Y : 등록되어 있는 사업자, N : 등록되어 있지 않은 사업자, C: 폐업자
   */
  businessExist?: string;

  /**
   * 폐업일자
   * @example 2021-02-01
   */
  closeDt?: string;
  error?: ErrorVo;

  /**
   * 개인구분 코드
   * @example A:개인과세 사업자, B:개인면세 사업자, C:법인이 아닌 종교단체, D:아파트 관리사무소 및 다단계 판매원, E:영리법인, F:비영리법인, G:국가,지방자치단체, 지방자치단체 조합, H: 외국법인의 본지점 및 연락 사무소, I:영리법인의 지점
   */
  indvdlDivCd?: string;

  /**
   * 개인구분 명
   * @example A:개인과세 사업자, B:개인면세 사업자, C:법인이 아닌 종교단체, D:아파트 관리사무소 및 다단계 판매원, E:영리법인, F:비영리법인, G:국가,지방자치단체, 지방자치단체 조합, H: 외국법인의 본지점 및 연락 사무소, I:영리법인의 지점
   */
  indvdlDivNm?: string;

  /**
   * 홈택스로부터 응답받은 값 전체
   * @example <map id='' ><map id='resultMsg' ><detailMsg></detailMsg><msg></msg><code></code><result>S</result></map><trtEndCd>Y</trtEndCd><smpcBmanEnglTrtCntn>The business registration number is not registered(date of closure: 2018-02-28)</smpcBmanEnglTrtCntn><nrgtTxprYn>N</nrgtTxprYn><smpcBmanTrtCntn>등록되어 있지 않은 사업자등록번호 입니다. (폐업일자: 2018-02-28)</smpcBmanTrtCntn><trtCntn>폐업자 (과세유형: 부가가치세 일반과세자, 폐업일자:2018-02-28) 입니다.</trtCntn></map>
   */
  responseFullText?: string;

  /**
   * 과세유형
   * @example 1
   */
  taxType?: string;
}

export interface BlankResponse {
  error?: ErrorVo;
}

/**
 * 게시판설정 상세조회 Response
 */
export interface BoardInfoResponse {
  /**
   * 댓글유무(Y/N)
   * @example N
   */
  answerYn?: string;

  /**
   * 첨부파일유무(Y/N)
   * @example N
   */
  atchmnflYn?: string;

  /**
   * 게시판위치(001:게이트페이지(평가자), 002:게이트페이지(의뢰자), 003:평가자, 004:의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 게시판설정 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 게시일유무(Y/N)
   * @example N
   */
  openDateYn?: string;

  /**
   * 구분
   * @example N:공지사항, E:이벤트
   */
  se?: string;
}

/**
 * 게시판설정 등록 Request
 */
export interface BoardInsertRequest {
  /**
   * 댓글유무(Y/N)
   * @example N
   */
  answerYn?: string;

  /**
   * 첨부파일유무(Y/N)
   * @example N
   */
  atchmnflYn?: string;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 게시일유무(Y/N)
   * @example N
   */
  openDateYn?: string;
}

/**
 * 게시판설정 등록 Response
 */
export interface BoardInsertResponse {
  /**
   * 댓글유무(Y/N)
   * @example N
   */
  answerYn?: string;

  /**
   * 첨부파일유무(Y/N)
   * @example N
   */
  atchmnflYn?: string;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 게시판설정 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 수정일시
   * @format date-time
   */
  modDt?: string;

  /**
   * 수정자 ID
   * @format int32
   * @example 2
   */
  modId?: number;

  /**
   * 게시일유무(Y/N)
   * @example N
   */
  openDateYn?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 등록자 ID
   * @format int32
   * @example 1
   */
  regId?: number;

  /**
   * 구분
   * @example N:공지사항, E:이벤트
   */
  se?: string;
}

/**
 * 게시판설정 수정 Request
 */
export interface BoardModifyRequest {
  /**
   * 댓글유무(Y/N)
   * @example N
   */
  answerYn?: string;

  /**
   * 첨부파일유무(Y/N)
   * @example N
   */
  atchmnflYn?: string;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 게시판설정 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 게시일유무(Y/N)
   * @example N
   */
  openDateYn?: string;
}

/**
 * 게시판설정 수정 Response
 */
export interface BoardModifyResponse {
  /**
   * 댓글유무(Y/N)
   * @example N
   */
  answerYn?: string;

  /**
   * 첨부파일유무(Y/N)
   * @example N
   */
  atchmnflYn?: string;

  /**
   * 게시판위치(001:게이트페이지(평가자), 002:게이트페이지(의뢰자), 003:평가자, 004:의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 게시판설정 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-01-01 00:00:00
   */
  modDt?: string;

  /**
   * 수정자 ID
   * @format int32
   * @example 2
   */
  modId?: number;

  /**
   * 게시일유무(Y/N)
   * @example N
   */
  openDateYn?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 등록자 ID
   * @format int32
   * @example 1
   */
  regId?: number;

  /**
   * 구분
   * @example N:공지사항, E:이벤트
   */
  se?: string;
}

/**
 * 게시판설정 목록 조회 Response
 */
export interface BoardSearchResponse {
  /**
   * 댓글유무(Y/N)
   * @example N
   */
  answerYn?: string;

  /**
   * 첨부파일유무(Y/N)
   * @example Y
   */
  atchmnflYn?: string;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 게시판설정 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 게시일유무(Y/N)
   * @example N
   */
  openDateYn?: string;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example N
   */
  se?: string;
}

export interface CSFResponse {
  /** @format int32 */
  attachFileId?: number;

  /** @format int32 */
  no?: number;
  se?: string;
}

export interface CancelStatusModifyRequest {
  /**
   * 취소처리상태[MA007]
   * @example 20
   */
  processStatus?: string;

  /**
   * 반려사유
   * @example 반려사유 내용 입니다
   */
  returnResn?: string;
}

export interface ChargerInfoResponse {
  /** 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함) */
  age?: string;

  /** 의뢰코멘트 */
  comment?: string;

  /**
   * 쿠폰할인
   * @format int32
   */
  couponDscntAmount?: number;

  /** 영상설명 */
  dc?: string;

  /** 영상업로드 cmaf엔드포인트 */
  egressEndpointCmaf?: string;

  /** 영상업로드 dash엔드포인트 */
  egressEndpointDash?: string;

  /** 영상업로드 his엔드포인트 */
  egressEndpointHls?: string;

  /** 영상업로드 mss엔드포인트 */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /** 견적산출상태명 */
  estmtComputStatusNm?: string;

  /** 평가자구성 (001: 균등분포) */
  evaluatorComposition?: string;

  /** 영상업로드 첨부파일명 */
  fileName?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlCo?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /**
   * 심사비용
   * @format int32
   */
  jdgmnCtUntpc?: number;

  /** 최종평가설정 (001: 영상평가, 002: 영상출연자 평가) */
  lastEvlEstbs?: string;

  /** 영상제목 */
  mvpSubject?: string;

  /** 영상URL */
  mvpUrl?: string;

  /** 의뢰자명 */
  name?: string;

  /**
   * 포인트
   * @format int32
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   */
  prductAmount?: number;

  /** 제품카테고리 */
  prductCategorys?: ChargerPrductCategoryResponse[];
  prductDc?: string;

  /** 제품 첨부파일 */
  prductFiles?: ChargerPrductFileResponse[];

  /**
   * 제품 식별자
   * @format int32
   */
  prductId?: number;

  /** 제품 이미지 */
  prductImages?: ChargerObjectImageResponse[];

  /** 제폼명 */
  prductNm?: string;

  /** 제품 썸네일 */
  prductTumbnails?: ChargerObjectImageResponse[];

  /**
   * 견적서 식별자
   * @format int32
   */
  prqudoId?: number;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 심사보조담당자 */
  raAsstnCharger?: string;

  /** 심사확인담당자 */
  raCnfirmCharger?: string;

  /**
   * 심사확인담당자ID
   * @format int32
   */
  raCnfirmChargerId?: number;

  /** 심사확인담당자 이름 */
  raCnfirmChargerName?: string;

  /** 심사확인담당자 로그인아이디 */
  raCnfirmChargerUsername?: string;

  /** 심사필수확인담당자 */
  raEssntlCnfirmCharger?: string;

  /**
   * 심사필수확인담당자ID
   * @format int32
   */
  raEssntlCnfirmChargerId?: number;

  /** 심사필수확인담당자 이름 */
  raEssntlCnfirmChargerName?: string;

  /** 심사필수확인담당자 로그인아이디 */
  raEssntlCnfirmChargerUsername?: string;

  /** 심사필수확인담당자확인여부 */
  raEssntlCnfirmYn?: string;

  /**
   * 의뢰심사확인 식별자
   * @format int32
   */
  raId?: number;

  /** 심사반려사유 */
  raReturnResn?: string;

  /** 견적보조담당자 */
  reAsstnCharger?: string;

  /** 견적확인담당자 */
  reCnfirmCharger?: string;

  /**
   * 견적확인담당자ID
   * @format int32
   */
  reCnfirmChargerId?: number;

  /** 견적확인담당자 이름 */
  reCnfirmChargerName?: string;

  /** 견적확인담당자 로그인아이디 */
  reCnfirmChargerUsername?: string;

  /** 견적필수확인담당자 */
  reEssntlCnfirmCharger?: string;

  /**
   * 견적필수확인담당자ID
   * @format int32
   */
  reEssntlCnfirmChargerId?: number;

  /** 견적필수확인담당자 이름 */
  reEssntlCnfirmChargerName?: string;

  /** 견적필수확인담당자 로그인아이디 */
  reEssntlCnfirmChargerUsername?: string;

  /** 견적필수확인담당자확인여부 */
  reEssntlCnfirmYn?: string;

  /**
   * 의뢰견적 식별자
   * @format int32
   */
  reId?: number;

  /** 견적반려사유 */
  reReturnResn?: string;

  /** 영상다시보기 */
  replayYn?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태명 */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 응답수집비용단가
   * @format int32
   */
  rspnsColctCtUntpc?: number;

  /** 평가 보조담당자 */
  rvAsstnCharger?: string;

  /** 평가 확인담당자 */
  rvCnfirmCharger?: string;

  /**
   * 평가 확인담당자ID
   * @format int32
   */
  rvCnfirmChargerId?: number;

  /** 평가 확인담당자 이름 */
  rvCnfirmChargerName?: string;

  /** 평가 확인담당자 로그인아이디 */
  rvCnfirmChargerUsername?: string;

  /** 평가 필수확인담당자 */
  rvEssntlCnfirmCharger?: string;

  /**
   * 평가 필수확인담당자ID
   * @format int32
   */
  rvEssntlCnfirmChargerId?: number;

  /** 평가 필수확인담당자 이름 */
  rvEssntlCnfirmChargerName?: string;

  /** 평가 필수확인담당자 로그인아이디 */
  rvEssntlCnfirmChargerUsername?: string;

  /** 평가 필수확인담당자확인여부 */
  rvEssntlCnfirmYn?: string;

  /**
   * 의뢰평가 식별자
   * @format int32
   */
  rvId?: number;

  /** 구분 (LK:LINK, UL:업로드) */
  se?: string;

  /**
   * 총결제금액
   * @format int32
   */
  setleReqestAmount?: number;

  /**
   * 결제공급가액
   * @format int32
   */
  setleSplpcAm?: number;

  /**
   * 결제부가세
   * @format int32
   */
  setleVat?: number;

  /** 평가자성별 */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   */
  splpcAm?: number;

  /** 영상업로드 썸네일 S3경로. 콤마구분 */
  thumbNails?: string;

  /** 영상업로드 썸네일 웹경로. 콤마구분 */
  thumbNailsUrls?: string;

  /**
   * 응답수집비용
   * @format int32
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 로그인ID */
  username?: string;

  /**
   * 부가가치세
   * @format int32
   */
  vat?: number;

  /** 영상카테고리 */
  vidoCategorys?: ChargerVidoCategoryResponse[];

  /**
   * 영상 식별자
   * @format int32
   */
  vidoId?: number;

  /** 영상재생길이 */
  vidoRevivTime?: string;

  /** 영상 썸네일 */
  vidoTumbnails?: ChargerObjectImageResponse[];
}

export interface ChargerInspectManagerSearchResponse {
  error?: ErrorVo;

  /**
   * 관리자 식별자
   * @format int32
   */
  managerId?: number;

  /** 검수관리자 명 */
  name?: string;

  /** 검수관리자 ID */
  username?: string;
}

export interface ChargerMappingRequest {
  /**
   * 확인담당자 식별자 (관리자 식별자)
   * @format int32
   * @example 1
   */
  cnfirmChargerId?: number;
  error?: ErrorVo;

  /**
   * 필수확인담당자 식별자 (관리자 식별자)
   * @format int32
   * @example 1
   */
  essntlCnfirmChargerId?: number;
}

export interface ChargerObjectImageResponse {
  /** 영상 OR 제품 파일명 */
  fileName?: string;

  /** 영상 OR 제품 썸네일URL */
  fileUrl?: string;

  /**
   * 썸네일 식별자
   * @format int32
   */
  thumbId?: number;

  /** 영상 대표 썸네일 (Y이면 대표썸네일, 이외는 대표썸네일 아님) */
  useYn?: string;
}

export interface ChargerPrductCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 제품카테고리 식별자
   * @format int32
   */
  prductCategoryId?: number;
}

export interface ChargerPrductFileResponse {
  /** 제품 파일명 */
  fileName?: string;

  /** 제품 파일URL */
  fileUrl?: string;
}

export interface ChargerSearchResponse {
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 의뢰자명 */
  name?: string;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 심사보조담당자 */
  raAsstnCharger?: string;

  /** 심사확인담당자 */
  raCnfirmCharger?: string;

  /** 심사필수확인담당자 */
  raEssntlCnfirmCharger?: string;

  /** 심사필수확인담당자확인여부 */
  raEssntlCnfirmYn?: string;

  /** 심사지연일 */
  raLazyDt?: string;

  /** 견적보조담당자 */
  reAsstnCharger?: string;

  /** 견적확인담당자 */
  reCnfirmCharger?: string;

  /** 견적필수확인담당자 */
  reEssntlCnfirmCharger?: string;

  /** 견적필수확인담당자확인여부 */
  reEssntlCnfirmYn?: string;

  /** 견적산출지연일 */
  reLazyDt?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /** 평가보조담당자 */
  rvAsstnCharger?: string;

  /** 평가확인담당자 */
  rvCnfirmCharger?: string;

  /** 평가필수확인담당자 */
  rvEssntlCnfirmCharger?: string;

  /** 평가필수확인담당자확인여부 */
  rvEssntlCnfirmYn?: string;

  /**
   * 견적산출금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 로그인ID */
  username?: string;

  /** 영상재생길이 */
  vidoRevivTime?: string;
}

export interface ChargerVidoCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 의뢰카테고리 식별자
   * @format int32
   */
  reqestCategoryId?: number;
}

/**
 * 담당자 변경 List Response Vo
 */
export interface ChargersModifyResponse {
  chargerNm?: string;

  /** @format int32 */
  id?: number;
  loginId?: string;
}

export interface ChartData {
  datasets?: ChartDataSet[];
  labels?: object[];
}

export interface ChartDataSet {
  backgroundColor?: string[];
  data?: number[];
}

/**
 * 욕설체크 Response
 */
export interface CheckBadWordsResponse {
  /**
   * 검출 된 욕설 목록
   * @example ["신발","황세준"]
   */
  badWordsList?: string[];
  error?: ErrorVo;
}

/**
 * [의뢰자] - 서비스 소개서 관련 등록 Response
 */
export interface ClientIntroSearchResponse {
  /**
   * 활동분야 [CS008] - 001 : TV/방송 002 : 키즈/애니 003 : 음악/댄스 004 : 엔터테인먼트, 005 : 영화, 006 : 교양/정보/다큐, 007 : 예술/공연, 008 : 개인방송, 009 : 뷰티, 010 : 식품, 011 : 유아동, 012 : 펫용품, 013 : 패션잡화, 014 : 도서, 015 : 기타
   * @example 001
   */
  actRealm?: string;

  /**
   * 활동분야명
   * @example 대중음악
   */
  actRealmNm?: string;

  /**
   * 광고성정보수신약관동의여부
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 회사명
   * @example 블록스미스
   */
  cmpnyNm?: string;

  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 부서명
   * @example 마케팅 부서
   */
  fatherSign?: string;

  /**
   * 의뢰자 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 서비스 소개서 요청 일시
   * @example 2021-04-01
   */
  regDt?: string;
}

/**
 * 관리자 회원 - 의뢰자 등록 요청 Response
 */
export interface ClientMemberInsertResponse {
  /**
   * 주소
   * @example 서울시 강남구 도산대로8길
   */
  address?: string;

  /**
   * 생년월일
   * @example 19900101
   */
  birthday?: string;

  /**
   * 사업자 설명
   * @example 사업자 설명
   */
  bizDescription?: string;

  /**
   * 사업자명
   * @example (주)회사명
   */
  bizNm?: string;

  /**
   * 사업자 번호
   * @example 123-12-12345
   */
  bizNo?: string;

  /**
   * 사업 구분[개인 , 법인]
   * @example ABC
   */
  bizSection?: string;

  /**
   * 의뢰자 구분[개인회원,개인사업자...]
   * @example ABC
   */
  clientSection?: string;

  /**
   * 계약서명
   * @example 사업자등록증
   */
  contractNm?: string;

  /**
   * 계약서 구분(위촉 계약서, 비밀유지 계약서, 윤리 계약서...)
   * @example ABC
   */
  contractSection?: string;

  /**
   * 상세주소
   * @example 17-11 빌딩AN 2층
   */
  detailaddress?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 이메일 수신 동의
   * @example Y or N
   */
  emailRecptnYn?: string;
  error?: ErrorVo;

  /**
   * 사업자 등록증 파일명
   * @example (주)회사명
   */
  fileNm?: string;

  /**
   * 회원 id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 알게된경로 (가입경로)
   * @example 광고보고 가입
   */
  joinPath?: string;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 추천인 아이디
   * @format int32
   * @example 1
   */
  recomendrId?: number;

  /**
   * 등록자 id
   * @format int32
   * @example 1
   */
  regId?: number;

  /**
   * 서비스
   * @example F or W or etc...
   */
  service?: string;

  /**
   * 성별
   * @example M or F
   */
  sexdstn?: string;

  /**
   * sms 수신 동의
   * @example Y or N
   */
  smsRecptnYn?: string;

  /**
   * 이용약관 식별자(배열)
   * @example [1,2,3]
   */
  termsIds?: number[];

  /**
   * 로그인ID
   * @example test
   */
  username?: string;

  /**
   * 우편번호
   * @example 01100
   */
  zipcode?: string;
}

/**
 * 관리자 회원 - 의뢰자 수정 요청 Response
 */
export interface ClientMemberModifyResponse {
  /**
   * 사업자 등록증 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 생년월일
   * @example 19900101
   */
  birthday?: string;

  /**
   * 사업 구분[P:개인 , C:법인]
   * @example P
   */
  bizSection?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 사업자 등록증 첨부파일 확장자
   * @example jpg
   */
  fileExt?: string;

  /**
   * 사업자 등록증 첨부파일명
   * @example 등록증
   */
  fileNm?: string;

  /**
   * ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 추천인 아이디
   * @example blocksmith
   */
  recomendrId?: string;

  /**
   * 첨부파일 첨부 일시
   * @format date-time
   * @example 1
   */
  regDt?: string;

  /**
   * 성별
   * @example M or F
   */
  sexdstn?: string;

  /**
   * 로그인ID
   * @example test
   */
  username?: string;
}

export interface CodeGroupListResponse {
  /** 설명 */
  description?: string;

  /**
   * 코드그룹 명
   * @example CM001
   */
  groupNm?: string;

  /**
   * 코드그룹id
   * @example 1
   */
  id?: string;
}

export interface CodeGroupTreeResponse {
  /** 해당코드그룹 하위 코드목록 */
  codeList?: CodeListResponse[];

  /** 설명 */
  description?: string;

  /**
   * 코드그룹 명
   * @example CM001
   */
  groupNm?: string;

  /**
   * 코드그룹id
   * @example 1
   */
  id?: string;
}

export interface CodeListResponse {
  /**
   * 코드이름
   * @example 의뢰자
   */
  label?: string;

  /**
   * 코드ID
   * @example C
   */
  value?: string;
}

export interface CommonResetPasswordRequest {
  /**
   * 회원 아이디(통합회원쪽(accountId) id)
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 전송 이메일
   * @example test@test.com
   */
  to?: string;
}

export interface CommonUsernameCheckResponse {
  error?: ErrorVo;

  /**
   * 아이디 중복 여부(중복:true, 중복아님:false)
   * @example true
   */
  exist?: boolean;
}

export interface CompleteRequest {
  /**
   * 의뢰식별자
   * @example [1,2,3]
   */
  ids: number[];
}

export interface CompleteSearchResponse {
  error?: ErrorVo;

  /**
   * 불성실평가자수
   * @format int32
   */
  evlBadCnt?: number;

  /**
   * 총평가완료수
   * @format int32
   */
  evlCompleteTotalCnt?: number;

  /**
   * 이탈자수
   * @format int32
   */
  evlLeaveCnt?: number;

  /**
   * 정상평가자수
   * @format int32
   */
  evlNormalCnt?: number;

  /** 진행시작일 */
  evlProgrsBgnDt?: string;

  /**
   * 진행율
   * @format int32
   */
  evlProgrsBgnPer?: number;

  /**
   * 평가참여인원
   * @format int32
   */
  evlTotalCnt?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 의뢰자명 */
  name?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /** 의뢰자ID */
  username?: string;
}

/**
 * 콘텐츠 카드 등록 Request
 */
export interface ContentCardRequest {
  /**
   * 엔티티태그
   * @example alwasjkld214jkladklq
   */
  etag: string;

  /**
   * 파일명
   * @example video.mp4
   */
  fileName: string;
}

/**
 * 콘텐츠 카드 등록 Response
 */
export interface ContentCardResponse {
  /**
   * 컨텐츠카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;
  error?: ErrorVo;

  /**
   * 트랜스코딩 예상시간(초)
   * @format int32
   * @example 120
   */
  estimatedSeconds?: number;

  /**
   * 트랜스코딩 예상시간(타임형식)
   * @example 00:02:00
   */
  estimatedTime?: string;

  /**
   * 엔티티태그
   * @example alwasjkld214jkladklq
   */
  etag: string;

  /**
   * 파일명
   * @example video.mp4
   */
  fileName: string;

  /** 미디어워크플로우 정보 수신 웹소켓 정보 */
  websocketInfo?: WebSocketVo;
}

/**
 * 통합컨텐츠 영상 스트리밍정보 상세조회 Response
 */
export interface ContentCardVidoResponse {
  /**
   * content_card_no
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * cmaf엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/f6a738caadd041ee81b956d014cfa67c/27f9d779da5f4b85a5c0732cae6d2a54/index.m3u8
   */
  egressEndpointCmaf?: string;

  /**
   * dash엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/7c033e24f8c54d13833bd902b9d22c33/c916a709ea62459abe2b7b4fb25b9b86/index.mpd
   */
  egressEndpointDash?: string;

  /**
   * hls엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/32a0e4f73a8b402f9e84d3350da33474/a3c862e9dd854f8ea6fb18b7959fe254/index.m3u8
   */
  egressEndpointHls?: string;

  /**
   * mss엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/961e172ab1f844209b1a9afd70be9bc1/78623a75eec14c74861a821dd3022395/index.ism/Manifest
   */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /**
   * 파일명
   * @example REPCircle_Presentation17.mp4
   */
  fileNm?: string;

  /**
   * 썸네일S3경로. 콤마구분
   * @example s3://cuspvideoservice-destination-m98dshvs1el8/0f8a927c-594b-4cd6-984e-df6ae7b4b96f/thumbnails/REPCircle_Presentation16_thumb.0000007.jpg
   */
  thumbNails?: string;

  /**
   * 썸네일 웹경로. 콤마구분
   * @example https://d1dopqicmcx111.cloudfront.net/0f8a927c-594b-4cd6-984e-df6ae7b4b96f/thumbnails/REPCircle_Presentation16_thumb.0000007.jpg
   */
  thumbNailsUrls?: string;
}

/**
 * 데일리 설문 목록조회 response
 */
export interface DailyQuestionInfoResponse {
  /**
   * 답변에따른 해쉬태그
   * @example ["액션","액션"]
   */
  answerHashList?: AnswerHashResponse[];

  /**
   * 데일리 설문명
   * @example 6월 12일 데일리 설문
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 노출 날짜
   * @example 2021-06-06
   */
  exposureDt?: string;

  /**
   * 설문지id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록자 아이디
   * @example hjyoon
   */
  mgLoginId?: string;

  /**
   * 등록자 이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 포인트
   * @example 10
   */
  point?: string;

  /**
   * 질문
   * @example 여러분이 좋아하는 영화 장르는?
   */
  qestnCn?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;
}

/**
 * 데일리 설문 목록조회 response
 */
export interface DailyQuestionSearchResponse {
  /**
   * 데일리 설문명
   * @example 6월 12일 데일리 설문
   */
  dc?: string;

  /**
   * 노출 날짜
   * @example 2021-06-06
   */
  exposureDt?: string;

  /**
   * 설문지id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록자 아이디
   * @example 윤효진
   */
  mgLoginId?: string;

  /**
   * 등록자 이름
   * @example 윤효진
   */
  mgName?: string;

  /**
   * 포인트
   * @example 10
   */
  point?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;
}

export interface DeclarationInfoResponse {
  /** 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함) */
  age?: string;

  /** 의뢰코멘트 */
  comment?: string;

  /**
   * 쿠폰할인
   * @format int32
   */
  couponDscntAmount?: number;

  /** 영상설명 */
  dc?: string;

  /** 신고리스트 */
  declarations?: PagingInfoSearchDeclarationsResponse;

  /** 영상업로드 cmaf엔드포인트 */
  egressEndpointCmaf?: string;

  /** 영상업로드 dash엔드포인트 */
  egressEndpointDash?: string;

  /** 영상업로드 his엔드포인트 */
  egressEndpointHls?: string;

  /** 영상업로드 mss엔드포인트 */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /** 평가자구성 (001: 균등분포) */
  evaluatorComposition?: string;

  /** 영상업로드 첨부파일명 */
  fileName?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlCo?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /**
   * 심사비용
   * @format int32
   */
  jdgmnCtUntpc?: number;

  /** 최종평가설정 (001: 영상평가, 002: 영상출연자 평가) */
  lastEvlEstbs?: string;

  /** 영상제목 */
  mvpSubject?: string;

  /** 영상URL */
  mvpUrl?: string;

  /** 의뢰자명 */
  name?: string;

  /**
   * 포인트
   * @format int32
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   */
  prductAmount?: number;

  /** 제품카테고리 */
  prductCategorys?: DeclarationPrductCategoryResponse[];

  /** 제폼설명 */
  prductDc?: string;

  /** 제품 첨부파일 */
  prductFiles?: DeclarationPrductFileResponse[];

  /** 제품 이미지 */
  prductImages?: DeclarationObjectImageResponse[];

  /** 제폼명 */
  prductNm?: string;

  /** 제품 썸네일 */
  prductTumbnails?: DeclarationObjectImageResponse[];

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 영상다시보기 */
  replayYn?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태명 */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 응답수집비용단가
   * @format int32
   */
  rspnsColctCtUntpc?: number;

  /** 구분 (LK:LINK, UL:업로드) */
  se?: string;

  /**
   * 총결제금액
   * @format int32
   */
  setleReqestAmount?: number;

  /**
   * 결제공급가액
   * @format int32
   */
  setleSplpcAm?: number;

  /**
   * 결제부가세
   * @format int32
   */
  setleVat?: number;

  /** 평가자성별 */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   */
  splpcAm?: number;

  /** 영상업로드 썸네일 S3경로. 콤마구분 */
  thumbNails?: string;

  /** 영상업로드 썸네일 웹경로. 콤마구분 */
  thumbNailsUrls?: string;

  /**
   * 응답수집비용
   * @format int32
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 로그인ID */
  username?: string;

  /**
   * 부가가치세
   * @format int32
   */
  vat?: number;

  /** 영상카테고리 */
  vidoCategorys?: DeclarationVidoCategoryResponse[];

  /** 영상재생길이 */
  vidoRevivTime?: string;

  /** 영상 썸네일 */
  vidoTumbnails?: DeclarationObjectImageResponse[];
}

export interface DeclarationObjectImageResponse {
  /** 영상 OR 제품 파일명 */
  fileName?: string;

  /** 영상 OR 제품 썸네일URL */
  fileUrl?: string;

  /**
   * 썸네일 식별자
   * @format int32
   */
  thumbId?: number;

  /** 영상 대표 썸네일 (Y이면 대표썸네일, 이외는 대표썸네일 아님) */
  useYn?: string;
}

export interface DeclarationPrductCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 제품카테고리 식별자
   * @format int32
   */
  prductCategoryId?: number;
}

export interface DeclarationPrductFileResponse {
  /** 제품 파일명 */
  fileName?: string;

  /** 제품 파일URL */
  fileUrl?: string;
}

export interface DeclarationSearchResponse {
  /**
   * 신고 건 수
   * @format int32
   */
  declarationCnt?: number;
  error?: ErrorVo;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 의뢰자명 */
  name?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /** 로그인ID */
  username?: string;
}

export interface DeclarationVidoCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 의뢰카테고리 식별자
   * @format int32
   */
  reqestCategoryId?: number;
}

/**
 * 미디어 컨버터 오류 결과 상세
 */
export interface Detail {
  /**
   * AWS계정ID
   * @example 745109727851
   */
  accountId?: string;

  /**
   * 오류코드
   * @format int32
   * @example 1040
   */
  errorCode?: number;

  /**
   * 오류메세지
   * @example Invalid audio track specified for audio_selector [1]. Audio track [1] not found in input container.
   */
  errorMessage?: string;

  /**
   * 작업ID
   * @example 1622298028173-ccztlo
   */
  jobId?: string;

  /**
   * 큐 ARN정보
   * @example arn:aws:mediaconvert:ap-northeast-2:745109727851:queues/Default
   */
  queue?: string;

  /**
   * 상태
   * @example ERROR
   */
  status?: string;

  /**
   * timestamp
   * @format int64
   * @example 1622298032017
   */
  timestamp?: number;

  /**
   * 사용자메타데이터
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  userMetadata?: UserMetadata;
}

export interface ErrorVo {
  /**
   * 성공/에러코드
   * @example 00
   */
  code?: string;

  /** 에러의 경우 에러메시지 */
  message?: string;

  /**
   * 메세지 코드
   * @example ERROR.SYS.999
   */
  messageCode?: string;
}

export interface EstmtInfoResponse {
  /** 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함) */
  age?: string;

  /** 의뢰코멘트 */
  comment?: string;

  /**
   * 쿠폰할인
   * @format int32
   */
  couponDscntAmount?: number;

  /** 영상설명 */
  dc?: string;

  /** 영상업로드 cmaf엔드포인트 */
  egressEndpointCmaf?: string;

  /** 영상업로드 dash엔드포인트 */
  egressEndpointDash?: string;

  /** 영상업로드 his엔드포인트 */
  egressEndpointHls?: string;

  /** 영상업로드 mss엔드포인트 */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /** 견적산출상태명 */
  estmtComputStatusNm?: string;

  /** 평가자구성 (001: 균등분포) */
  evaluatorComposition?: string;

  /** 영상업로드 첨부파일명 */
  fileName?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlCo?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /**
   * 심사비용
   * @format int32
   */
  jdgmnCtUntpc?: number;

  /** 최종평가설정 (001: 영상평가, 002: 영상출연자 평가) */
  lastEvlEstbs?: string;

  /**
   * 회원 식별자
   * @format int32
   */
  memberId?: number;

  /** 영상제목 */
  mvpSubject?: string;

  /** 영상URL */
  mvpUrl?: string;

  /**
   * 내 관리자 식별자
   * @format int32
   */
  myId?: number;

  /** 의뢰자명 */
  name?: string;

  /**
   * 포인트
   * @format int32
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   */
  prductAmount?: number;

  /** 제품카테고리 */
  prductCategorys?: EstmtPrductCategoryResponse[];
  prductDc?: string;

  /** 제품 첨부파일 */
  prductFiles?: EstmtPrductFileResponse[];

  /**
   * 제품 식별자
   * @format int32
   */
  prductId?: number;

  /** 제품 이미지 */
  prductImages?: EstmtPrductFileResponse[];

  /** 제폼명 */
  prductNm?: string;

  /** 제품 썸네일 */
  prductTumbnails?: EstmtObjectImageResponse[];

  /**
   * 견적서 식별자
   * @format int32
   */
  prqudoId?: number;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 심사보조담당자 */
  raAsstnCharger?: string;

  /** 심사확인담당자 */
  raCnfirmCharger?: string;

  /** 심사필수확인담당자 */
  raEssntlCnfirmCharger?: string;

  /**
   * 심사필수확인담당자ID
   * @format int32
   */
  raEssntlCnfirmChargerId?: number;

  /** 심사필수확인담당자확인여부 */
  raEssntlCnfirmYn?: string;

  /**
   * 의뢰심사확인 식별자
   * @format int32
   */
  raId?: number;

  /** 심사반려사유 */
  raReturnResn?: string;

  /** 견적보조담당자 */
  reAsstnCharger?: string;

  /** 견적확인담당자 */
  reCnfirmCharger?: string;

  /** 견적필수확인담당자 */
  reEssntlCnfirmCharger?: string;

  /**
   * 견적필수확인담당자ID
   * @format int32
   */
  reEssntlCnfirmChargerId?: number;

  /** 견적필수확인담당자확인여부 */
  reEssntlCnfirmYn?: string;

  /**
   * 의뢰견적 식별자
   * @format int32
   */
  reId?: number;

  /** 견적반려사유 */
  reReturnResn?: string;

  /** 영상다시보기 */
  replayYn?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태명 */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 응답수집비용단가
   * @format int32
   */
  rspnsColctCtUntpc?: number;

  /** 응답수집비용단가 노출여부 */
  rspnsColctCtUntpcViewYn?: boolean;

  /** 구분 (LK:LINK, UL:업로드) */
  se?: string;

  /**
   * 총결제금액
   * @format int32
   */
  setleReqestAmount?: number;

  /**
   * 결제공급가액
   * @format int32
   */
  setleSplpcAm?: number;

  /**
   * 결제부가세
   * @format int32
   */
  setleVat?: number;

  /** 평가자성별 */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   */
  splpcAm?: number;

  /** 영상업로드 썸네일 S3경로. 콤마구분 */
  thumbNails?: string;

  /** 영상업로드 썸네일 웹경로. 콤마구분 */
  thumbNailsUrls?: string;

  /**
   * 응답수집비용
   * @format int32
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 로그인ID */
  username?: string;

  /**
   * 부가가치세
   * @format int32
   */
  vat?: number;

  /** 영상카테고리 */
  vidoCategorys?: EstmtVidoCategoryResponse[];

  /**
   * 영상 식별자
   * @format int32
   */
  vidoId?: number;

  /** 영상재생길이 */
  vidoRevivTime?: string;

  /** 영상썸네일 */
  vidoTumbnails?: EstmtObjectImageResponse[];
}

export interface EstmtObjectImageResponse {
  /** 영상 OR 제품 파일명 */
  fileName?: string;

  /** 영상 OR 제품 썸네일URL */
  fileUrl?: string;

  /**
   * 썸네일 식별자
   * @format int32
   */
  thumbId?: number;

  /** 영상 대표 썸네일 (Y이면 대표썸네일, 이외는 대표썸네일 아님) */
  useYn?: string;
}

export interface EstmtPrductCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 제품카테고리 식별자
   * @format int32
   */
  prductCategoryId?: number;
}

export interface EstmtPrductFileResponse {
  /** 제품 파일명 */
  fileName?: string;

  /** 제품 파일URL */
  fileUrl?: string;
}

export interface EstmtRejectRequest {
  /**
   * 의뢰심사확인 식별자
   * @format int32
   * @example 1
   */
  raId?: number;

  /**
   * 반려사유
   * @example 반려사유 입니다..
   */
  returnResn?: string;
}

export interface EstmtSaveRequest {
  /**
   * 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트 입니다.
   */
  comment?: string;

  /**
   * 영상설명
   * @example 영상설명 입니다.
   */
  dc?: string;

  /**
   * 평가자구성 (001: 균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 심사비용
   * @format int32
   * @example 10000
   */
  jdgmnCtUntpc?: number;

  /**
   * 최종평가설정 (001: 영상평가, 002: 영상출연자 평가)
   * @example 001
   */
  lastEvlEstbs?: string;

  /**
   * 영상제목
   * @example 영상제목 입니다.
   */
  mvpSubject?: string;

  /**
   * 포인트
   * @format int32
   * @example 100
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   * @example 50000
   */
  prductAmount?: number;

  /**
   * 제품카테고리ID (내가 체크한것만 받음)
   * @example [2,3,4]
   */
  prductCategorys?: EstmtPrductCategoryResponse[];

  /**
   * 제품 식별자
   * @format int32
   * @example 1
   */
  prductId?: number;

  /**
   * 제폼명
   * @example 핫식스
   */
  prductNm?: string;

  /**
   * 선택한 제품썸네일 식별자
   * @format int32
   * @example 1
   */
  prductThumbId?: number;

  /**
   * 견적서 식별자
   * @format int32
   * @example 1
   */
  prqudoId?: number;

  /**
   * 영상다시보기 (Y/N)
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰컨텐츠 (V: 영상, P: 제품)
   * @example V
   */
  reqestCntnts?: string;

  /**
   * 의뢰제목
   * @example 의뢰제목입니다..
   */
  reqestNm?: string;

  /**
   * 응답수집비용단가
   * @format int32
   * @example 10000
   */
  rspnsColctCtUntpc?: number;

  /**
   * 평가자성별 (M/F)
   * @example M
   */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   * @example 10000
   */
  splpcAm?: number;

  /**
   * 응답수집비용
   * @format int32
   * @example 50000
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   * @example 10000
   */
  totSetleExpectAmount?: number;

  /**
   * 부가가치세
   * @format int32
   * @example 10000
   */
  vat?: number;

  /**
   * 영상카테고리ID (내가 체크한것만 받음)
   * @example [2,3,4]
   */
  vidoCategorys?: EstmtVidoCategoryResponse[];

  /**
   * 영상 식별자
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 선택한 영상 썸네일 식별자
   * @format int32
   * @example 1
   */
  vidoThumbId?: number;
}

export interface EstmtSearchResponse {
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 의뢰자명 */
  name?: string;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 심사보조담당자 */
  raAsstnCharger?: string;

  /** 심사확인담당자 */
  raCnfirmCharger?: string;

  /** 심사필수확인담당자 */
  raEssntlCnfirmCharger?: string;

  /** 심사필수확인담당자확인여부 */
  raEssntlCnfirmYn?: string;

  /** 심사지연일 */
  raLazyDt?: string;

  /** 견적보조담당자 */
  reAsstnCharger?: string;

  /** 견적확인담당자 */
  reCnfirmCharger?: string;

  /** 견적필수확인담당자 */
  reEssntlCnfirmCharger?: string;

  /** 견적필수확인담당자확인여부 */
  reEssntlCnfirmYn?: string;

  /** 견적산출지연일 */
  reLazyDt?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 견적요청금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 로그인ID */
  username?: string;

  /** 영상재생길이 */
  vidoRevivTime?: string;
}

export interface EstmtVidoCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 의뢰카테고리 식별자
   * @format int32
   */
  reqestCategoryId?: number;
}

export interface EvaluationDishonestSaveRequest {
  /**
   * 회원평가 식별자
   * @format int32
   * @example 1
   */
  meId: number;
}

export interface EvaluationDishonestTmpSaveRequest {
  /**
   * 정상/불성실 변경처리 (Y or N)
   * @example Y
   */
  dishonestEvlTmpYn?: string;

  /**
   * 회원평가 식별자
   * @format int32
   * @example 1
   */
  meId: number;
}

export interface EvaluationInfoResponse {
  error?: ErrorVo;

  /** 평가자 평가결과리스트 */
  evaluationResultResponses?: EvaluationInfoResultResponse[];

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /**
   * 내 관리자 식별자
   * @format int32
   */
  myId?: number;

  /** 의뢰자명 */
  name?: string;

  /** 제품카테고리 */
  prductCategorys?: EvaluationPrductCategoryResponse[];

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태명 */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /** 평가 보조담당자 */
  rvAsstnCharger?: string;

  /** 평가 확인담당자 */
  rvCnfirmCharger?: string;

  /** 평가 필수확인담당자 */
  rvEssntlCnfirmCharger?: string;

  /**
   * 평가 필수확인담당자ID
   * @format int32
   */
  rvEssntlCnfirmChargerId?: number;

  /** 평가 필수확인담당자확인여부 */
  rvEssntlCnfirmYn?: string;

  /**
   * 의뢰평가 식별자
   * @format int32
   */
  rvId?: number;

  /** 로그인ID */
  username?: string;

  /** 영상카테고리 */
  vidoCategorys?: EvaluationVidoCategoryResponse[];
}

export interface EvaluationInfoResultList {
  error?: ErrorVo;

  /** 평가자 평가결과리스트 */
  evaluationResultResponses?: EvaluationInfoResultResponse[];
}

export interface EvaluationInfoResultResponse {
  /** 평가자답변 */
  answerCn?: string;

  /** 정상/불성실 변경처리 */
  dishonestEvlTmpYn?: string;

  /** 정상/불성실 상태 */
  dishonestEvlYn?: string;
  error?: ErrorVo;

  /**
   * 회원평가 식별자
   * @format int32
   */
  meId?: number;

  /** 평가자명 */
  name?: string;

  /** 주관식내용 */
  qestnCn?: string;

  /** 평가자ID */
  username?: string;
}

export interface EvaluationPrductCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 제품카테고리 식별자
   * @format int32
   */
  prductCategoryId?: number;
}

export interface EvaluationSearchResponse {
  error?: ErrorVo;

  /**
   * 불성실평가자수
   * @format int32
   */
  evlBadCnt?: number;

  /**
   * 정상평가자수
   * @format int32
   */
  evlNormalCnt?: number;

  /** 진행시작일 */
  evlProgrsBgnDt?: string;

  /**
   * 진행율
   * @format int32
   */
  evlProgrsBgnPer?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 의뢰자명 */
  name?: string;

  /**
   * 주관식 질문수
   * @format int32
   */
  qestnShortTotalCnt?: number;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /** 평가 보조담당자 */
  rvAsstnCharger?: string;

  /** 평가 확인담당자 */
  rvCnfirmCharger?: string;

  /** 평가 필수확인담당자 */
  rvEssntlCnfirmCharger?: string;

  /** 평가 필수확인담당자확인여부 */
  rvEssntlCnfirmYn?: string;

  /** 로그인ID */
  username?: string;
}

export interface EvaluationVidoCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 의뢰카테고리 식별자
   * @format int32
   */
  reqestCategoryId?: number;
}

export interface File {
  absolute?: boolean;
  absoluteFile?: File;
  absolutePath?: string;
  canonicalFile?: File;
  canonicalPath?: string;
  directory?: boolean;
  executable?: boolean;
  file?: boolean;

  /** @format int64 */
  freeSpace?: number;
  hidden?: boolean;

  /** @format int64 */
  lastModified?: number;
  name?: string;
  parent?: string;
  parentFile?: File;
  path?: string;
  readable?: boolean;

  /** @format int64 */
  totalSpace?: number;

  /** @format int64 */
  usableSpace?: number;
  writable?: boolean;
}

/**
 * Import 결제 완료 Response Vo
 */
export interface IfImportPayCompleteResponse {
  error?: ErrorVo;

  /**
   * 결제반환메시지
   * @example 일반 결제 성공
   */
  message?: string;

  /**
   * 결제반환상태
   * @example success
   */
  status?: string;
}

/**
 * Import 결제 완료 Response Vo
 */
export interface IfImportPayCompleteVbankResponse {
  /**
   * 금액 - 공급가액
   * @format int32
   * @example 1280000
   */
  amount?: number;

  /**
   * 금액 - 공급가액 포멧
   * @example 1,280,000
   */
  amountFormat?: string;

  /**
   * 현금영수증발급 여부
   * @example true
   */
  cashReceiptIssued?: boolean;

  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 금액 포멧
   * @example 8,000
   */
  couponDscntAmountFormat?: string;
  error?: ErrorVo;

  /**
   * 결제반환메시지
   * @example 일반 결제 성공
   */
  message?: string;

  /**
   * 결제(주문)번호
   * @example 20210428-212018-6
   */
  no?: string;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌, memor:수기결제
   * @example vbank
   */
  setleKnd?: string;

  /**
   * 결제종류코드명
   * @example 가상계좌
   */
  setleKndNm?: string;

  /**
   * 결제 금액 합계
   * @format int32
   * @example 1400000
   */
  setleReqestAmount?: number;

  /**
   * 결제 금액 합계 포멧
   * @example 1,400,000
   */
  setleReqestAmountFormat?: string;

  /**
   * 부가세
   * @format int32
   * @example 128000
   */
  vat?: number;

  /**
   * 부가세 포멧
   * @example 12,8000
   */
  vatFormat?: string;

  /**
   * 가상계좌 은행 표준코드 - (금융결제원기준)
   * @example 088
   */
  vbankCode?: string;

  /**
   * 입금받을 가상계좌 마감기한
   * @format date-time
   * @example 1625826618
   */
  vbankDt?: string;

  /**
   * 입금받을 가상계좌 예금주
   * @example （주）케이지이니시
   */
  vbankHolder?: string;

  /**
   * 입금받을 가상계좌 은행명
   * @example 신한(조흥)은행
   */
  vbankName?: string;

  /**
   * 입금받을 가상계좌 계좌번호
   * @example 56211102580756
   */
  vbankNum?: string;

  /**
   * 결제반환상태
   * @example success
   */
  zxc?: string;
}

/**
 * Import 결제 요청 정보 생성 Response Vo
 */
export interface IfImportPayGenerateResponse {
  error?: ErrorVo;

  /**
   * 결제아이디
   * @example f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d
   */
  merchantUid?: string;

  /**
   * 가상계좌입금기한
   * @example 20210712190000
   */
  vbankDue?: string;
}

export interface ImageAttachFileInfoResponse {
  /** @format int32 */
  iattachFileId?: number;
  ifileExt?: string;
  ifileName?: string;
  ifileUrl?: string;
}

export interface ImageFileInfoResponse {
  /** @format int32 */
  attachFileIdIF?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  prductId?: number;
}

export interface InfoResponse {
  error?: ErrorVo;

  /** 요청내용 */
  requestCn?: string;

  /** 응답내용 */
  responseCn?: string;
}

export type InputStream = object;

export interface InsertReqestEstimateResponse {
  /**
   * 자동산출여부
   * @example Y/N
   */
  atmcPrdctnYn?: string;
  error?: ErrorVo;

  /**
   * 견적서 id
   * @format int32
   * @example 1
   */
  prqudoId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;
}

export interface Legend {
  /** lebels */
  labels?: string[];
}

export interface MailVo {
  address?: string;
  message?: string;
  title?: string;
}

/**
 * [Admin] - 관리자 상세조회 Response
 */
export interface ManagerInfoResponse {
  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 관리자id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /**
   * 수정자 로그인id
   * @example 13
   */
  modLoginId?: string;

  /** 수정자 */
  modName?: string;

  /** 이름 */
  name?: string;

  /**
   * 비밀번호 변경일
   * @example 2021-01-01 00:00:00
   */
  passwordChangeDt?: string;

  /**
   * 최근접속일
   * @example 2021-01-01 00:00:00
   */
  recentConectDt?: string;

  /**
   * 등록일시
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;
}

/**
 * [Admin] - 관리자 등록 Request
 */
export interface ManagerInsertRequest {
  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /** 이름 */
  name?: string;
}

/**
 * [Admin] - 관리자 등록 Response
 */
export interface ManagerInsertResponse {
  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 관리자id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /**
   * 이름
   * @example 이호랑
   */
  name?: string;

  /**
   * 등록일시
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 2
   */
  regId?: number;

  /**
   * 등록자명
   * @example 2
   */
  regName?: string;

  /**
   * 비밀번호 초기화 여부
   * @example false
   */
  resetPw?: boolean;
}

/**
 * [Admin] - 관리자 로그인 Request
 */
export interface ManagerLoginRequest {
  /**
   * 로그인id
   * @example staff06
   */
  loginId?: string;

  /**
   * 비밀번호(Staff 생성 시 제공)
   * @example Cusp1234!@
   */
  password?: string;
}

/**
 * [Admin] - 관리자 로그인 Response
 */
export interface ManagerLoginResponse {
  /**
   * 액세스토큰
   * @example 인증서버가 발행한 access_token
   */
  accessToken?: string;
  error?: ErrorVo;
}

export interface ManagerMe {
  error?: ErrorVo;

  /** @format int32 */
  id?: number;
  loginId?: string;
  name?: string;
  roles?: Roles[];
}

/**
 * [Admin] - 관리자 수정 Response
 */
export interface ManagerModifyResponse {
  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 관리자id
   * @format int32
   * @example adminLoginId
   */
  id?: number;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /**
   * 수정자 ID
   * @format int32
   * @example 13
   */
  modId?: number;

  /** 수정자 */
  modName?: string;

  /**
   * 비밀번호변경일
   * @example 2021-01-01 00:00:00
   */
  passwordChangeDt?: string;

  /**
   * 최근접속일
   * @example 2021-01-01 00:00:00
   */
  recentConectDt?: string;

  /**
   * 등록자 ID
   * @format int32
   * @example 2
   */
  regId?: number;
}

/**
 * [Admin] - 메뉴, 기능 권한 Request
 */
export interface ManagerPrivilegeRequest {
  /** 내 권한 객체 */
  managerPrivileges?: ManagerPrivileges;

  /**
   * 권한ID
   * @example AMN010101C
   */
  privilegeId?: string;

  /**
   * 역할ID
   * @format int32
   * @example 2
   */
  roleId?: number;
}

export interface ManagerPrivilegeResponse {
  /**
   * depth
   * @format int32
   * @example 1
   */
  depth?: number;

  /** 기능 */
  managerPrivileges?: ManagerPrivileges;

  /**
   * 권한 ID
   * @example 1
   */
  privilegeId?: string;

  /**
   * 권한 명
   * @example 기간별 매출현황
   */
  privilegeNm?: string;

  /**
   * 역할 ID
   * @format int32
   * @example 1
   */
  roleId?: number;

  /**
   * 상위(부모) 권한 ID
   * @example 1
   */
  upperPrivilegeId?: string;
}

/**
 * [Admin] - 기능 목록 Request
 */
export interface ManagerPrivileges {
  c?: boolean;
  d?: boolean;
  r?: boolean;
  u?: boolean;
}

/**
 * [Admin] - 관리자 비밀번호 재설정 Request
 */
export interface ManagerResetChangePwRequest {
  /**
   * 비밀번호
   * @example ~blocksmith17
   */
  password?: string;

  /**
   * 비밀번호 변경
   * @example Cusp1234!@
   */
  pwdChange?: string;
}

/**
 * [Admin] - 관리자 마이페이지 비밀번호 설정
 */
export interface ManagerResetPwErrorVo {
  error?: ErrorVo;
}

/**
 * [Admin] - 관리자 비밀번호 초기화 Response
 */
export interface ManagerResetPwResponse {
  error?: ErrorVo;

  /**
   * 관리자id
   * @format int32
   * @example 2
   */
  id?: number;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 수정자 ID
   * @format int32
   * @example 13
   */
  modId?: number;

  /** 수정자명 */
  modName?: string;

  /**
   * 다음비밀번호변경일자
   * @example 2021-01-01 00:00:00
   */
  nextPasswordChangeDe?: string;

  /**
   * 비밀번호 변경일
   * @example 2021-01-01 00:00:00
   */
  passwordChangeDt?: string;
}

export interface ManagerRolePrivilegeResponse {
  error?: ErrorVo;

  /** 메뉴, 권한 List */
  managerPrivileges?: ManagerPrivilegeResponse[];

  /** 역할 List */
  managerRoles?: ManagerRoleResponse[];
}

/**
 * [Admin] - 관리자 역할 상세조회 Response
 */
export interface ManagerRoleResponse {
  /**
   * 역할 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 매니저 역할
   * @example false
   */
  managerRole?: boolean;

  /**
   * 역할설명
   * @example 검수운영 부서장
   */
  roleDc?: string;

  /**
   * 역할명
   * @example ROLE_INSPECT_HEAD
   */
  roleNm?: string;
}

/**
 * [Admin] - 관리자 목록 조회 Response
 */
export interface ManagerSearchResponse {
  error?: ErrorVo;

  /**
   * 관리자id
   * @format int32
   * @example 2
   */
  id?: number;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /**
   * 이름
   * @example smith
   */
  name?: string;

  /**
   * 최근접속일
   * @example 2021-01-01
   */
  recentConectDt?: string;

  /**
   * 등록일시
   * @example 2021-01-01
   */
  regDt?: string;
}

/**
 * 미디어 컨버터 오류 결과
 */
export interface MediaConvertErrorResult {
  /**
   * AWS계정ID
   * @example 745109727851
   */
  account?: string;

  /**
   * 오류 상세
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  detail?: Detail;

  /**
   * 상세타입
   * @example MediaConvert Job State Change
   */
  "detail-type"?: string;

  /**
   * id
   * @example 5a0c3d8c-c9c6-4b18-f796-16fee7540ba4
   */
  id?: string;

  /**
   * 리전
   * @example ap-northeast-2
   */
  region?: string;

  /**
   * 리소스 ARN정보
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  resources?: string[];

  /**
   * 원본
   * @example aws.mediaconvert
   */
  source?: string;

  /**
   * 일시
   * @format date-time
   * @example 2021-05-29T14:20:32Z
   */
  time?: string;

  /**
   * 미디어컨버터 버전
   * @example 0
   */
  version?: string;
}

/**
 * 미디어 워크플로우 결과
 */
export interface MediaWorkflowResultResponse {
  /**
   * acceleratedTranscoding
   * @example PREFERRED
   */
  acceleratedTranscoding?: string;

  /**
   * archiveSource
   * @example GLACIER
   */
  archiveSource?: string;

  /**
   * encodeJobId
   * @format int32
   */
  cardNo?: number;

  /**
   * cloudFront
   * @example d1dopqicmcx111.cloudfront.net
   */
  cloudFront?: string;

  /**
   * customTemplate
   * @example true
   */
  customTemplate?: boolean;

  /**
   * destBucket
   * @example cuspvideoservice-destination-m98dshvs1el8
   */
  destBucket?: string;

  /**
   * egressEndpointCmaf
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/f6a738caadd041ee81b956d014cfa67c/27f9d779da5f4b85a5c0732cae6d2a54/index.m3u8
   */
  egressEndpointCmaf?: string;

  /**
   * egressEndpointDash
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/7c033e24f8c54d13833bd902b9d22c33/c916a709ea62459abe2b7b4fb25b9b86/index.mpd
   */
  egressEndpointDash?: string;

  /**
   * egressEndpointHls
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/32a0e4f73a8b402f9e84d3350da33474/a3c862e9dd854f8ea6fb18b7959fe254/index.m3u8
   */
  egressEndpointHls?: string;

  /**
   * egressEndpointMss
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/961e172ab1f844209b1a9afd70be9bc1/78623a75eec14c74861a821dd3022395/index.ism/Manifest
   */
  egressEndpointMss?: string;

  /** enableMediaPackage */
  enableMediaPackage?: boolean;

  /**
   * enableSns
   * @example true
   */
  enableSns?: boolean;

  /**
   * enableSqs
   * @example true
   */
  enableSqs?: boolean;

  /**
   * encodeJobId
   * @example 1621924386435-648wcn
   */
  encodeJobId?: string;

  /**
   * encodingProfile
   * @format int32
   */
  encodingProfile?: number;

  /**
   * endTime
   * @example 2021-05-25T06:32:55.283Z
   */
  endTime?: string;
  error?: ErrorVo;

  /** 워크플로우 오류 정보 */
  errorResult?: MediaConvertErrorResult;

  /**
   * frameCapture
   * @example true
   */
  frameCapture?: boolean;

  /**
   * frameCaptureHeight
   * @format int32
   */
  frameCaptureHeight?: number;

  /**
   * frameCaptureWidth
   * @format int32
   * @example 1280
   */
  frameCaptureWidth?: number;

  /**
   * inputRotate
   * @example DEGREE_0
   */
  inputRotate?: string;

  /**
   * jobTemplate
   * @example cuspvideoservice_Ott_720p_Avc_Aac_16x9_mvod_no_preset
   */
  jobTemplate?: string;

  /** mediaPackageResourceId */
  mediaPackageResourceId?: string;

  /**
   * srcBucket
   * @example cuspvideoservice-source-z8k6fz4eo0e4
   */
  srcBucket?: string;

  /**
   * srcHeight
   * @format int32
   * @example 420
   */
  srcHeight?: number;

  /**
   * 원본영상 파일명
   * @example test.mp4
   */
  srcVideo?: string;

  /**
   * srcWidth
   * @format int32
   * @example 630
   */
  srcWidth?: number;

  /**
   * startTime
   * @example 2021-05-25T06:32:55.283Z
   */
  startTime?: string;

  /**
   * S3썸네일정보 (,)콤마구분
   * @example a.jpg, b.jpg
   */
  thumbNails?: string;

  /**
   * S3썸네일URL (,)콤마구분
   * @example https://cuspcdn.s3.ap-northeast-2.amazonaws.com/assets/wewill/dev/upload/customer/20210312/37f9d333-58e0-4dbb-b19c-b7a22d455039.jpg, https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/sample_do_not_delete/%EA%B0%95%EC%95%84%EC%A7%80.jpg
   */
  thumbNailsUrls?: string;

  /**
   * workflowName
   * @example cuspvideoservice
   */
  workflowName?: string;

  /**
   * workflowStatus
   * @example Compelete
   */
  workflowStatus?: string;

  /**
   * workflowTrigger
   * @example Video
   */
  workflowTrigger?: string;
}

export interface MemberidPointSumInterface {
  /** @format int32 */
  member_id?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  point?: number;

  /** @format int32 */
  save_no?: number;
}

/**
 * [Admin] - 관리자 마이페이지 상세조회 Response
 */
export interface MyPageInfoResponse {
  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 관리자id
   * @format int32
   * @example adminLoginId
   */
  id?: number;

  /**
   * 로그인id
   * @example loginId
   */
  loginId?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /** 이름 */
  name?: string;

  /**
   * 역할설명
   * @example 검수운영 부서장
   */
  roleDc?: string;
}

export interface NotificationMessage {
  /** Json 형식의 스트링 */
  data?: string;

  /**
   * 내부,외부 여부(외부 : true , 내부: false)
   * @example false
   */
  isExternal?: boolean;

  /**
   * 새창 여부(새창 : true , 아닐경우: false)
   * @example false
   */
  isNewWindow?: boolean;

  /**
   * Link URL
   * @example http://test.com
   */
  linkUrl?: string;

  /**
   * message 내용 json + uri인코딩
   * @example 메시지 내용
   */
  message?: string;

  /**
   * message Type( N: 일반, L:링크)
   * @example N
   */
  messageType?: "N" | "L";

  /**
   * message Target(A: 전체에게 전송 , U: 개인에게 전송)
   * @example U
   */
  targetType?: "A" | "U";

  /**
   * message 받을 로그인 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface Options {
  /** 차트 옵션 legend */
  legend?: Legend;
}

/**
 * 의뢰 콘텐츠 설정 (Youtube & 영상) 등록 Request
 */
export interface OrderMergeRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (Youtube & 영상) 등록 Response
 */
export interface OrderMergeResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 제품의뢰 사용 전 설문 등록 :: 저장 Request
 */
export interface OrderProductSurveyStep1InsertRequest {
  /** 사용 전 의뢰설문지 정보 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 제품의뢰 사용 중 설문 설문 등록 :: 저장 Request
 */
export interface OrderProductSurveyStep2InsertRequest {
  /** 사용 중 의뢰설문지 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 제품의뢰 사용 후 설문 등록 :: 저장 Request
 */
export interface OrderProductSurveyStep3InsertRequest {
  /** 사용 후 의뢰설문지 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 의뢰반려사유 조회 Response
 */
export interface OrderReturnResnResponse {
  error?: ErrorVo;

  /**
   * return_resn
   * @example 반려사유
   */
  returnResn?: string;
}

/**
 * 의뢰 설정 상세조회 Response
 */
export interface OrderSetInfoResponse {
  /**
   * 연령 (ex. 20,30,40)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트
   */
  comment?: string;
  error?: ErrorVo;

  /**
   * 평가자구성 ([IN005] 001:균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 성별([IN004] A: 전체, M:남성, F:여성)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 의뢰 설문 설정 (평가자설정) 수정 Request
 */
export interface OrderSetModifyRequest {
  /**
   * 연령 (ex. 20,30,40)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트
   */
  comment?: string;

  /**
   * 평가자구성 ([IN005] 001:균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 성별([IN004] A: 전체, M:남성, F:여성)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 의뢰 설문 설정 (평가자설정) 수정 Response
 */
export interface OrderSetModifyResponse {
  /**
   * 연령 (ex. 20,30,40)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트
   */
  comment?: string;
  error?: ErrorVo;

  /**
   * 평가자구성 ([IN005] 001:균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 성별([IN004] A: 전체, M:남성, F:여성)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 견적서 요청 등록 :: 저장 Request
 */
export interface OrderSurveyFinalInsertRequest {
  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId: number;
}

/**
 * 의뢰 설문 일괄 등록 :: 저장 Request
 */
export interface OrderSurveyInsertAllRequest {
  /** 의뢰 설문지 목록 */
  requestSurveyList?: ReqestQestnr[];
}

/**
 * 설문일괄 등록 :: 저장 Response
 */
export interface OrderSurveyInsertAllResponse {
  error?: ErrorVo;

  /** 생성 된 의뢰 설문지 목록 */
  orderSurveyInsertResponseList?: OrderSurveyInsertResponse[];
}

/**
 * 의뢰 사전 설문 등록 :: 저장 Response
 */
export interface OrderSurveyInsertResponse {
  error?: ErrorVo;

  /** 생성 된 설문지 정보 */
  qestnrEntity?: QestnrEntity;

  /** 생성 된 의뢰 설문지 정보 */
  reqestQestnrEntity?: ReqestQestnrEntity;
}

/**
 * 의뢰 설문 설정 (영상) 프리미엄 정책데이터 Response
 */
export interface OrderSurveyPolicyResponse {
  /**
   * 최종평가 영상 출연자 평가의 설문지 ID
   * @format int32
   * @example -2
   */
  actorQestnrId?: number;
  error?: ErrorVo;

  /**
   * 등록 가능한 질문 최대 개수
   * @format int32
   * @example 39
   */
  maxQuestionCnt?: number;

  /**
   * 질문형식 옵션 목록 (single:객관식 질문 (단일 응답), multi:객관식 질문 (복수 응답), short:주관식 질문, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지답변)
   * @example 1
   */
  questionSelectOptions?: SelectOptionsVo[];

  /**
   * 시분초 타임형식 패턴
   * @example ^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$
   */
  timePattern?: string;

  /**
   * 함정답변 목록
   * @example ["꼬부기","이상해씨","파이리"]
   */
  trapAnswerList?: string[];

  /**
   * 최종평가 영상평가의 설문지 ID
   * @format int32
   * @example -1
   */
  videoQestnrId?: number;
}

/**
 * 의뢰 사전 설문 등록 :: 저장 Request
 */
export interface OrderSurveyStep1InsertRequest {
  /** 의뢰설문지 정보 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 의뢰 실시간 설문 등록 :: 저장 Request
 */
export interface OrderSurveyStep2InsertRequest {
  /** 실시간 의뢰설문지 목록 */
  requestSurveyList?: ReqestQestnr[];
}

/**
 * 의뢰 실시간 설문목록 등록 :: 저장 Response
 */
export interface OrderSurveyStep2InsertResponse {
  error?: ErrorVo;

  /** 생성 된 실시간 설문지 목록 */
  orderSurveyInsertResponseList?: OrderSurveyInsertResponse[];
}

/**
 * 의뢰 사후 설문 등록 :: 저장 Request
 */
export interface OrderSurveyStep3InsertRequest {
  /** 의뢰설문지 정보 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 최종평가 설문 등록 :: 저장/임시저장 Request
 */
export interface OrderSurveyStep4InsertRequest {
  /**
   * 설문지id 고정값 (-1:영상평가, -2:영상출연자평가)
   * @format int32
   * @example 1
   */
  qestnrId: number;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId: number;

  /**
   * 영상id - 제품의 경우 입력 불필요
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

/**
 * 의뢰 콘텐츠 설정 상세 Response
 */
export interface OrderVidoInfoResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 업로드 완료여부
   * @example Y
   */
  uploadCompleteYn?: string;

  /** 업로드 파일명 */
  uploadFileNm?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y","fileName":"도넛","fileUrl":"https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/board/event/20210423/59de7024-c5c1-47de-9224-f72b2fa4de2a.png"},{"attachFileId":2,"useYn":"N","fileName":"도넛2","fileUrl":"https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/board/event/20210423/59de7024-c5c1-47de-9224-f72b2fa4de2a.png"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailInfo[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 등록 Request
 */
export interface OrderVidoInsertRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 등록 Response
 */
export interface OrderVidoInsertResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 수정 Request
 */
export interface OrderVidoModifyRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 수정 Response
 */
export interface OrderVidoModifyResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브) 등록 Request
 */
export interface OrderYoutubeInsertRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브) 등록 Response
 */
export interface OrderYoutubeInsertResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브)등록 Request
 */
export interface OrderYoutubeModifyRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브)수정 Response
 */
export interface OrderYoutubeModifyResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 결제 패키지 등록 Response Vo
 */
export interface PackageResponse {
  /**
   * 서비스기간 종료일
   * @example 2020-07-27
   */
  endDate?: string;
  error?: ErrorVo;

  /**
   * 주문번호
   * @example 20200627-123456-1123
   */
  paymentCode?: string;

  /**
   * 결제일
   * @format date-time
   * @example 2020-06-25 22:15:00
   */
  paymentDate?: string;

  /**
   * 결제일 포멧팅
   * @example 06.25 22:15
   */
  paymentDateStr?: string;

  /**
   * 상점 시퀀스
   * @format int32
   * @example 22
   */
  shopSeq?: number;

  /**
   * 서비스기간 시작일
   * @example 2020-06-27
   */
  startDate?: string;
}

/**
 * 결제결과 Vo
 */
export interface PaymentResult {
  /**
   * 금액
   * @format int32
   * @example 10000
   */
  amount?: number;

  /**
   * 결제아이디
   * @example f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d
   */
  merchantUid?: string;

  /**
   * 결제번호
   * @example 20200627-123456-1123
   */
  paymentCode?: string;

  /**
   * 결제일
   * @format date-time
   * @example 2020-06-25 22:15:00
   */
  paymentDate?: string;

  /**
   * 결제일 포멧팅
   * @example 06.25 22:15
   */
  paymentDateStr?: string;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌
   * @example 1
   */
  paymentMethod?: string;

  /**
   * 결제종류이름
   * @example 신용카드
   */
  paymentMethodName?: string;

  /**
   * 결제상태. ready:미결제, paid:결제완료, cancelled:결제취소, failed:결제실패 = ['ready', 'paid', 'cancelled', 'failed']
   * @example paid
   */
  paymentStatus?: string;

  /**
   * 결제상태명
   * @example 결제완료
   */
  paymentStatusName?: string;
}

/**
 * 포인트 출금 신청 상세 Response VO
 */
export interface PointDefraymentInfoResponse {
  /**
   * 계좌번호
   * @example 092-21-0222-243
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울 강남구 도산대로8길 17-11
   */
  address?: string;

  /**
   * 아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 이름
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 은행명
   * @example 국민은행
   */
  bankNm?: string;

  /**
   * 담당자
   * @example 차명환
   */
  chargerNm?: string;

  /**
   * 상태 (00:출금신청, 10:출금완료, 20:출금반려)
   * @example 00
   */
  defraySttus?: string;

  /**
   * 상태 (00:출금신청, 10:출금완료, 20:출금반려)
   * @example 출금신청
   */
  defraySttusNm?: string;

  /**
   * 상세주소
   * @example 2층 블록스미스
   */
  detailaddress?: string;

  /**
   * 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 전체 주소
   * @example (06039) 서울 강남구 도산대로8길 17-11 2층 블록스미스
   */
  fullAddress?: string;

  /**
   * 회원등급
   * @example W1
   */
  grad?: string;

  /**
   * 보유포인트
   * @example 150,000
   */
  holdPoint?: string;

  /**
   * 포인트 출금 신청 고유 번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 식별자
   * @example 891012-1231231
   */
  identifier?: string;

  /**
   * 연락처
   * @example 010-3333-4444
   */
  mbtlnum?: string;

  /**
   * 처리일
   * @format date
   * @example 1985-01-01
   */
  processDt?: string;

  /**
   * 출금신청금액
   * @example 100,000
   */
  reqstPoint?: string;

  /**
   * 반려사유
   * @example 어머니는 자장면이 싫다고 하셨어
   */
  returnResn?: string;

  /**
   * 우편번호
   * @example 06317
   */
  zipcode?: string;
}

/**
 * 포인트 출금 신청 상세 수정 Request VO
 */
export interface PointDefraymentModifyRequest {
  /**
   * 상태 (00:출금신청, 10:출금완료, 20:출금반려)
   * @example 00
   */
  defraySttus?: string;

  /**
   * 반려사유
   * @example 일일 출금신청 한도 초과
   */
  returnResn?: string;
}

/**
 * 포인트 출금 신청 목록 Response VO
 */
export interface PointDefraymentResponse {
  /**
   * 신청자아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 신청자명
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 담당자
   * @example 차명환
   */
  chargerNm?: string;

  /**
   * 상태 (출금신청, 출금완료, 출금반려)
   * @example 출금신청
   */
  defraySttus?: string;

  /**
   * 출금 신청일
   * @format date
   * @example 2020-12-06
   */
  defraymentReqstDt?: string;

  /**
   * 보유포인트
   * @example 150,000
   */
  holdPoint?: string;

  /**
   * 포인트 출금 신청 고유 번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 처리일
   * @example 1985-01-01
   */
  processDt?: string;

  /**
   * 출금신청금액
   * @example 100,000
   */
  reqstPoint?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;
}

/**
 * 포인트 상세 VO
 */
export interface PointDetail {
  /**
   * 포인트 상세 취소 순번
   * @format int32
   * @example 4
   */
  cancelNo?: number;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /** 수정일시 */
  mod_dt?: string;

  /**
   * 수정자
   * @format int32
   */
  mod_id?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 처리일자
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointProcessDt?: string;

  /**
   * 포인트 요청일자
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointReqDt?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 포인트 사용일자
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointUseDt?: string;

  /** 등록일시 */
  reg_dt?: string;

  /**
   * 등록자
   * @format int32
   */
  reg_id?: number;

  /**
   * 포인트 상세 적립 순번
   * @format int32
   * @example 1
   */
  saveNo?: number;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Insert Request VO
 */
export interface PointMasterInsertRequest {
  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류 PAY : 결제, EVL : 평가, EVT : 이벤트, DEF : 출금, ADM : 관리자, DAY: 데일리평가
   * @example PAY
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 사용 여부('Y':사용, 'N':미사용
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Insert Response VO
 */
export interface PointMasterInsertResponse {
  error?: ErrorVo;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  modDt?: string;

  /**
   * 수정자
   * @format int32
   * @example 1010
   */
  modId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 1010
   */
  regId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Modify Request VO
 */
export interface PointMasterModifyRequest {
  /**
   * 적립 예정일
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  accmlPrarnde?: string;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1000
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Modify Response VO
 */
export interface PointMasterModifyResponse {
  error?: ErrorVo;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  modDt?: string;

  /**
   * 수정자
   * @format int32
   * @example 1010
   */
  modId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 1010
   */
  regId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 VO
 */
export interface PointMasterResponse {
  /**
   * 적립 예정일
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  accmlPrarnde?: string;
  error?: ErrorVo;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  modDt?: string;

  /**
   * 수정자
   * @format int32
   * @example 1010
   */
  modId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 0
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 1010
   */
  regId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Insert Response VO
 */
export interface PointMasterSingleGroupByResponse {
  /**
   * 누적 출금 포인트
   * @format int32
   * @example 1000
   */
  accmlt_def_point?: number;
  error?: ErrorVo;

  /**
   * 오늘 적립 예정 포인트
   * @format int32
   * @example 1000
   */
  today_accml_point?: number;

  /**
   * 사용 가능한 포인트
   * @format int32
   * @example 1000
   */
  use_posbl_point?: number;
}

/**
 * 이벤트 게시글 관련 삭제 삭제 Request
 */
export interface PostEventDeleteRequest {
  /**
   * 게시글 ID
   * @example [1,2]
   */
  ids?: number[];
}

/**
 * [고객센터] - 이벤트 게시글 상세조회 Response
 */
export interface PostEventInfoResponse {
  attachFileInfoResponse?: AttachFileInfoResponse[];

  /**
   * 게시판위치(001:게이트페이지(평가자), 002:게이트페이지(의뢰자), 003:평가자, 004:의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;
  error?: ErrorVo;

  /**
   * 이벤트 상태 코드 (W:대기, P:진행, D:마감)
   * @example P
   */
  eventStatus?: string;

  /**
   * 이벤트 상태(대기, 진행, 마감)
   * @example 진행
   */
  eventStatusNm?: string;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 로그인 ID
   * @example 1
   */
  loginId?: string;

  /**
   * 작성자명
   * @example 이호랑
   */
  name?: string;

  /**
   * 등록일시
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 게시글 등록 Request
 */
export interface PostEventInsertRequest {
  /**
   * 첨부파일 ID(이벤트 썸네일용)
   * @format int32
   * @example 7515
   */
  attachFileId?: number;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example E
   */
  se?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * 고객센터 - 이벤트 게시글 등록 Response
 */
export interface PostEventInsertResponse {
  /**
   * 첨부파일 ID(이벤트 썸네일용)
   * @format int32
   * @example 7515
   */
  attachFileId?: number;

  /**
   * 게시판위치(001:게이트페이지(평가자), 002:게이트페이지(의뢰자), 003:평가자, 004:의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;
  error?: ErrorVo;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록일
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 20
   */
  regId?: number;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example E
   */
  se?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 이벤트 게시글 수정 Request
 */
export interface PostEventModifyRequest {
  /**
   * 첨부파일 ID(이벤트 썸네일용)
   * @format int32
   * @example 7515
   */
  attachFileId?: number;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example E
   */
  se?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 이벤트 게시글 수정 Response
 */
export interface PostEventModifyResponse {
  /**
   * 첨부파일 ID(이벤트 썸네일용)
   * @format int32
   * @example 7515
   */
  attachFileId?: number;

  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;
  error?: ErrorVo;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /** 수정일시 */
  modDt?: string;

  /**
   * 수정자 ID
   * @format int32
   * @example 2
   */
  modId?: number;

  /**
   * 수정자 로그인id
   * @example 13
   */
  modLoginId?: string;

  /** 수정자 */
  modName?: string;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example E
   */
  se?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 이벤트 게시글 목록 조회 Response
 */
export interface PostEventSearchResponse {
  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;
  error?: ErrorVo;

  /**
   * 이벤트 상태 코드 (W:대기, P:진행, D:마감
   * @example P
   */
  eventStatus?: string;

  /**
   * 이벤트 상태(대기, 진행, 마감)
   * @example 진행
   */
  eventStatusNm?: string;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 로그인 ID
   * @example 1
   */
  loginId?: string;

  /**
   * 작성자명
   * @example 이호랑
   */
  name?: string;

  /**
   * 등록일
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * 게시글첨부파일 등록 Request
 */
export interface PostFileInsertRequest {
  /**
   * 게시글id
   * @format int32
   * @example 1
   */
  postId?: number;
}

/**
 * 공지사항 게시글 관련 삭제 삭제 Request
 */
export interface PostNoticeDeleteRequest {
  /**
   * 게시글 ID
   * @example [1,2]
   */
  ids?: number[];
}

/**
 * [고객센터] - 공지사항 게시글 상세조회 Response
 */
export interface PostNoticeInfoResponse {
  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;
  error?: ErrorVo;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 로그인 ID
   * @example 1
   */
  loginId?: string;

  /**
   * 작성자명
   * @example 이호랑
   */
  name?: string;

  /**
   * 등록일시
   * @example 2021-01-01
   */
  regDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 공지사항 게시글 등록 Request
 */
export interface PostNoticeInsertRequest {
  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example N
   */
  se?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * 고객센터 - 공지사항 게시글 등록 Response
 */
export interface PostNoticeInsertResponse {
  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;
  error?: ErrorVo;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록일
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 20
   */
  regId?: number;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example N
   */
  se?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 공지사항 게시글 수정 Request
 */
export interface PostNoticeModifyRequest {
  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 구분(N:공지사항, E:이벤트)
   * @example N
   */
  se?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 공지사항 게시글 수정 Response
 */
export interface PostNoticeModifyResponse {
  /**
   * 게시판위치(001:게이트페이지(평가자), 002:게이트페이지(의뢰자), 003:평가자, 004:의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;
  error?: ErrorVo;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 공지사항 게시글 목록 조회 Response
 */
export interface PostNoticeSearchResponse {
  /**
   * 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자)
   * @example ["001","003"]
   */
  bbsLc?: string[];
  error?: ErrorVo;

  /**
   * 노출여부(Y : 노출 / N : 노출 안함)
   * @example Y
   */
  exposureYn?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 로그인 ID
   * @example 1
   */
  loginId?: string;

  /**
   * 작성자명
   * @example 이호랑
   */
  name?: string;

  /**
   * 등록일시
   * @example 2021-01-01
   */
  regDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

export interface ProductCategoryInfoResponse {
  /** @format int32 */
  categoryId?: number;
}

export interface ProductFileInfoResponse {
  /** @format int32 */
  attachFileId?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  prductId?: number;
}

export interface ProductInfoResponse {
  /**
   * 첨부파일 ID(첨부파일aws)
   * @example []
   */
  attachFileList?: AttachFileInfResponse[];

  /**
   * 제품설명
   * @example 질문합니다
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이미지파일 ID(이미지파일aws)
   * @example []
   */
  imageAttachFileList?: ImageAttachFileInfoResponse[];

  /**
   * 제품 이미지파일 ID(제품 이미지)
   * @example []
   */
  imageFileList?: ImageFileInfoResponse[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품카테고리
   * @example [1,2]
   */
  productCategoryList?: ProductCategoryInfoResponse[];

  /**
   * 제품 첨부파일 ID(제품 파일)
   * @example []
   */
  productFileList?: ProductFileInfoResponse[];

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;

  /**
   * 썸네일파일 ID(썸네일파일aws)
   * @example []
   */
  thumbAttachFileList?: ThumbAttachFileInfoResponse[];

  /**
   * 제품 이미지파일 썸네일 ID(제품 썸네일)
   * @example []
   */
  thumbFileList?: ProductThumbInfoResponse[];
}

export interface ProductModifyRequest {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 설명
   * @example 제품 설명
   */
  dc?: string;

  /**
   * 이미지파일 ID(제품 이미지)
   * @example [1,2]
   */
  imageFileIds?: number[];

  /**
   * 제품카테고리
   * @example [1,2]
   */
  prdtCategoryIds?: number[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품 썸네일 ID
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  productThumbnailInfoList?: ProductThumbnailMerge[];

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;
}

export interface ProductModifyResponse {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 제품설명
   * @example 질문합니다
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이미지파일 ID(제품 이미지)
   * @example [1,2]
   */
  imageFileIds?: number[];

  /**
   * 제품카테고리
   * @example [59,60]
   */
  prdtCategoryIds?: number[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품 썸네일 ID
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  productThumbnailInfoList?: ProductThumbnailMerge[];

  /**
   * 의뢰콘텐츠 (V:영상, P:제품)
   * @example P
   */
  reqestCntnts?: string;

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;
}

/**
 * 제품 썸네일 등록&수정
 */
export interface ProductThumbnailMerge {
  /**
   * 썸네일파일id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 사용여부
   * @example N
   */
  useYn?: string;
}

export interface QestnEntity {
  answerEntityList?: AnswerEntity[];
  answerTy?: string;
  delYn?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnCn?: string;

  /** @format int32 */
  qestnId?: number;

  /** @format int32 */
  qestnNo?: number;
  qestnrEntity?: QestnrEntity;

  /** @format int32 */
  qestnrId?: number;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;
  rspnsOption?: string;

  /** @format int32 */
  sn?: number;
}

export interface QestnrEntity {
  dc?: string;
  delYn?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnEntityList?: QestnEntity[];

  /** @format int32 */
  qestnrId?: number;
  qestnrTy?: string;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;
}

/**
 * 의뢰 설문지 질문 정보
 */
export interface Question {
  /**
   * 답변유형 (001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지답변)
   * @example 001
   */
  answerTy?: string;

  /** 답변목록 */
  answers?: Answer[];

  /**
   * 질문내용
   * @example 세 사람의 학자가 함께 기차를 타고 가다가 몇 분 걸려 터널을 통과했는데
   */
  qestnCn: string;

  /**
   * 질문 번호
   * @format int32
   * @example 1
   */
  qestnNo: number;

  /**
   * 응답옵션 (S:단일응답, M:중복응답)
   * @example S
   */
  rspnsOption?: string;

  /**
   * 질문 순번 (질문 번호와 동일)
   * @format int32
   * @example 1
   */
  sn: number;

  /**
   * 내부 유지 값(프론트 용)
   * @example aa
   */
  value?: string;
}

/**
 * 캡챠 Response
 */
export interface RecaptchaResponse {
  /**
   * 성공여부
   * @example submit
   */
  action?: string;

  /**
   * 호출시간
   * @example 2021-05-18T05:35:04Z
   */
  challenge_ts?: string;
  error?: ErrorVo;

  /**
   * hostname
   * @example localhost
   */
  hostname?: string;

  /**
   * 점수
   * @example 0.9
   */
  score?: string;

  /**
   * 성공여부
   * @example true
   */
  success?: boolean;
}

export interface ReqCategoryResponse {
  categoryNm?: string;
}

export interface ReqestCancelRequest {
  /**
   * 환불금액
   * @format int32
   * @example 10000
   */
  amount?: number;

  /**
   * 회원 식별자
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 취소사유
   * @example 취소사유입니다.
   */
  reqestCanclResn?: string;
}

export interface ReqestDeleteRequest {
  /**
   * 의뢰식별자
   * @example [1,2,3]
   */
  ids: number[];
}

export interface ReqestInfoResponse {
  /** 평가자연령 (data가 20,30 이런식으로 되어있음. split해서 사용하도록 함) */
  age?: string;

  /** 의뢰코멘트 */
  comment?: string;

  /**
   * 쿠폰할인
   * @format int32
   */
  couponDscntAmount?: number;

  /** 영상설명 */
  dc?: string;

  /** 영상업로드 cmaf엔드포인트 */
  egressEndpointCmaf?: string;

  /** 영상업로드 dash엔드포인트 */
  egressEndpointDash?: string;

  /** 영상업로드 his엔드포인트 */
  egressEndpointHls?: string;

  /** 영상업로드 mss엔드포인트 */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /** 견적산출상태 */
  estmtComputStatus?: string;

  /** 평가자구성 (001: 균등분포) */
  evaluatorComposition?: string;

  /**
   * 불성실평가자수
   * @format int32
   */
  evlBadCnt?: number;

  /**
   * 이탈자수
   * @format int32
   */
  evlLeaveCnt?: number;

  /**
   * 정상평가자수
   * @format int32
   */
  evlNormalCnt?: number;

  /**
   * 평가참여인원
   * @format int32
   */
  evlTotalCnt?: number;

  /** 영상업로드 첨부파일명 */
  fileName?: string;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlCo?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /**
   * 심사비용
   * @format int32
   */
  jdgmnCtUntpc?: number;

  /** 최종평가설정 (001: 영상평가, 002: 영상출연자 평가) */
  lastEvlEstbs?: string;

  /**
   * 회원 식별자
   * @format int32
   */
  memberId?: number;

  /** 영상제목 */
  mvpSubject?: string;

  /** 영상 URL */
  mvpUrl?: string;

  /** 의뢰자명 */
  name?: string;

  /**
   * 포인트
   * @format int32
   */
  point?: number;

  /**
   * 제품 금액
   * @format int32
   */
  prductAmount?: number;

  /** 제품카테고리 */
  prductCategorys?: ReqestPrductCategoryResponse[];
  prductDc?: string;

  /** 제품 첨부파일 */
  prductFiles?: ReqestPrductFileResponse[];

  /**
   * 제품 식별자
   * @format int32
   */
  prductId?: number;

  /** 제품 이미지 */
  prductImages?: ReqestPrductFileResponse[];

  /** 제폼명 */
  prductNm?: string;

  /** 제품 썸네일 */
  prductTumbnails?: ReqestObjectImageResponse[];

  /**
   * 견적서 식별자
   * @format int32
   */
  prqudoId?: number;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 영상다시보기 */
  replayYn?: string;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태명 */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 응답수집비용단가
   * @format int32
   */
  rspnsColctCtUntpc?: number;

  /** 응답수집비용단가 노출여부 */
  rspnsColctCtUntpcViewYn?: boolean;

  /** 구분 (LK:LINK, UL:업로드) */
  se?: string;

  /**
   * 총결제금액
   * @format int32
   */
  setleReqestAmount?: number;

  /**
   * 결제공급가액
   * @format int32
   */
  setleSplpcAm?: number;

  /**
   * 결제부가세
   * @format int32
   */
  setleVat?: number;

  /** 평가자성별 */
  sexdstn?: string;

  /**
   * 공급가액
   * @format int32
   */
  splpcAm?: number;

  /** 영상업로드 썸네일 S3경로. 콤마구분 */
  thumbNails?: string;

  /** 영상업로드 썸네일 웹경로. 콤마구분 */
  thumbNailsUrls?: string;

  /**
   * 응답수집비용
   * @format int32
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 의뢰자ID */
  username?: string;

  /**
   * 부가가치세
   * @format int32
   */
  vat?: number;

  /** 영상카테고리 */
  vidoCategorys?: ReqestVidoCategoryResponse[];

  /**
   * 영상 식별자
   * @format int32
   */
  vidoId?: number;

  /** 영상재생길이 */
  vidoRevivTime?: string;
}

export interface ReqestObjectImageResponse {
  /** 영상 OR 제품 파일명 */
  fileName?: string;

  /** 영상 OR 제품 썸네일URL */
  fileUrl?: string;

  /**
   * 썸네일 식별자
   * @format int32
   */
  thumbId?: number;

  /** 영상 대표 썸네일 (Y이면 대표썸네일, 이외는 대표썸네일 아님) */
  useYn?: string;
}

export interface ReqestPrductCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 제품카테고리 식별자
   * @format int32
   */
  prductCategoryId?: number;
}

export interface ReqestPrductFileResponse {
  /** 제품 파일명 */
  fileName?: string;

  /** 제품 파일URL */
  fileUrl?: string;
}

/**
 * 의뢰 설문지 정보
 */
export interface ReqestQestnr {
  /**
   * 평가방식 (001:사전설문, 002:실시간설문, 003:사후설문, 004최종평가)
   * @example 001
   */
  evlMthd: string;

  /**
   * 설문지 등장시간 - 사전설문의 경우 00:00:00, 사후설문의 경우 영상재생시간 마지막
   * @example 00:00:00
   */
  qestnTime?: string;

  /**
   * 설문지 설명 - 제품 설문에 사용. 영상은 반드시 null
   * @example 이 제품은 장기 사용 시 납중독 우려가 있음.
   */
  qestnrDc?: string;

  /** 질문목록 */
  questionList: Question[];

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId: number;

  /**
   * 영상id - 제품의 경우 입력 불필요
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

export interface ReqestQestnrEntity {
  evlMthd?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnTime?: string;

  /** @format int32 */
  qestnrId?: number;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;

  /** @format int32 */
  reqestId?: number;

  /** @format int32 */
  vidoId?: number;
}

export interface ReqestSaveRequest {
  /**
   * 심사비용
   * @format int32
   * @example 10000
   */
  jdgmnCtUntpc?: number;

  /**
   * 지급포인트
   * @format int32
   * @example 5000
   */
  point?: number;

  /**
   * 견적서 식별자
   * @format int32
   * @example 1
   */
  prqudoId?: number;

  /**
   * 응답수집비용단가
   * @format int32
   * @example 10000
   */
  rspnsColctCtUntpc?: number;

  /**
   * 공급가액
   * @format int32
   * @example 10000
   */
  splpcAm?: number;

  /**
   * 응답수집비용
   * @format int32
   * @example 50000
   */
  totRspnsColctCt?: number;

  /**
   * 견적산출금액
   * @format int32
   * @example 10000
   */
  totSetleExpectAmount?: number;

  /**
   * 부가가치세
   * @format int32
   * @example 10000
   */
  vat?: number;
}

/**
 * 의뢰신청 전체 임시저장 Response
 */
export interface ReqestSaveResponse {
  error?: ErrorVo;

  /**
   * 로컬스토리지 데이터 JSON (uri encoding)
   * @example {"a":"aaa"}
   */
  jsonDataUriEncoded?: string;

  /**
   * 의뢰ID
   * @format int32
   * @example 1
   */
  reqestId?: number;
}

export interface ReqestSearchResponse {
  /** 의뢰자구분 */
  bizSection?: string;
  error?: ErrorVo;

  /**
   * 불성실평가자수
   * @format int32
   */
  evlBadCnt?: number;

  /**
   * 이탈자수
   * @format int32
   */
  evlLeaveCnt?: number;

  /**
   * 정상평가자수
   * @format int32
   */
  evlNormalCnt?: number;

  /**
   * 평가참여인원
   * @format int32
   */
  evlTotalCnt?: number;

  /**
   * 목표평가자수
   * @format int32
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;

  /** 관리자 로그인ID */
  managerId?: string;

  /** 관리자명 */
  managerName?: string;

  /** 의뢰자명 */
  name?: string;

  /**
   * 총질문수
   * @format int32
   */
  qestnTotalCnt?: number;

  /** 의뢰컨텐츠 */
  reqestCntnts?: string;

  /** 의뢰제목 */
  reqestNm?: string;

  /** 의뢰신청일 */
  reqestReqstDt?: string;

  /** 의뢰상태 */
  reqestStatus?: string;

  /** 의뢰상태 (명칭) */
  reqestStatusNm?: string;

  /** 의뢰상품 */
  reqestTy?: string;

  /**
   * 견적요청금액
   * @format int32
   */
  totSetleExpectAmount?: number;

  /** 의뢰자ID */
  username?: string;
}

export interface ReqestVidoCategoryResponse {
  /**
   * 카테고리 식별자
   * @format int32
   */
  categoryId?: number;

  /** 카테고리 이름 */
  categoryNm?: string;

  /**
   * 의뢰카테고리 식별자
   * @format int32
   */
  vidoCategoryId?: number;
}

/**
 * 의뢰취소 상세조회 response
 */
export interface RequestCancelInfoResponse {
  /**
   * 의뢰자구분
   * @example 법인
   */
  bizSection?: string;

  /**
   * 의뢰카테고리
   * @example 영화,음식,도서
   */
  categoryList?: ReqCategoryResponse[];

  /**
   * 담당자
   * @example 홍길동
   */
  charger?: string;

  /**
   * 쿠폰사용금액
   * @format int32
   * @example 20000
   */
  couponDscntAmount?: number;

  /**
   * 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /** @format int32 */
  id?: number;

  /**
   * 연락처
   * @example 01034343233
   */
  mobileNo?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 취소신청상태(취소신청:10,취소반려:20,취소완료:30)
   * @example 10
   */
  processStatus?: string;

  /**
   * 환불계좌
   * @example 1231239675
   */
  refndAcnut?: string;

  /**
   * 환불금액
   * @format int32
   * @example 300000
   */
  refndAmount?: number;

  /**
   * 환불계좌은행명
   * @example 우리은행
   */
  refndBankNm?: string;

  /**
   * 취소사유
   * @example 불법컨텐츠 입니다
   */
  reqestCanclResn?: string;

  /**
   * 의뢰콘텐츠
   * @example 영상 또는 제품
   */
  reqestCntnts?: string;

  /**
   * 의뢰명
   * @example ㅇㅇㅇㅇ 의뢰평가
   */
  reqestNm?: string;

  /**
   * 의뢰상태
   * @example 심사 대기
   */
  reqestStatus?: string;

  /**
   * 반려사유
   * @example 반려사유 내용 입니다
   */
  returnResn?: string;

  /**
   * 신청일
   * @example 2021-03-04 12:15:06
   */
  rqstdt?: string;

  /**
   * 결제수단
   * @example 카드결제
   */
  setleKnd?: string;

  /**
   * 결제금액
   * @format int32
   * @example 300000
   */
  setleReqestAmount?: number;

  /**
   * 주문번호
   * @example 23489-34334
   */
  settlementNo?: string;

  /**
   * 처리일
   * @example 2021-03-04
   */
  trede?: string;

  /**
   * 회원구분
   * @example 의뢰자
   */
  uesrSection?: string;

  /**
   * 아이디
   * @example testid
   */
  userName?: string;
}

/**
 * 회계(세금계산서&현금영수증) 목록조회 response
 */
export interface RequestCancelSearchResponse {
  /**
   * 담당자
   * @example 차명환
   */
  chager?: string;

  /**
   * 쿠폰사용금액
   * @format int32
   * @example 20000
   */
  couponDscntAmount?: number;

  /**
   * id
   * @format int32
   */
  id?: number;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 취소처리상태(취소신청:10,취소반려:20,취소완료:30)
   * @example 취소신청
   */
  processStatus?: string;

  /**
   * 취소금액
   * @format int32
   * @example 100000
   */
  refndAmount?: number;

  /**
   * 취소사유
   * @example 불법컨텐츠
   */
  reqestCanclResn?: string;

  /**
   * 의뢰명
   * @example 의뢰명의 의뢰
   */
  reqestNm?: string;

  /**
   * 의뢰상태[IN001]
   * @example 심사대기
   */
  reqestStatus?: string;

  /**
   * 신청일
   * @example 2021-05-05
   */
  rqstdt?: string;

  /**
   * 결제금액
   * @format int32
   * @example 100000
   */
  setleReqestAmount?: number;

  /**
   * 주문번호
   * @example 23489
   */
  settlementNo?: string;

  /**
   * 처리일
   * @example 2021-05-05
   */
  trede?: string;

  /**
   * 아이디
   * @example testid
   */
  userName?: string;
}

export interface Resource {
  description?: string;
  file?: File;
  filename?: string;
  inputStream?: InputStream;
  open?: boolean;
  readable?: boolean;
  uri?: URI;
  url?: URL;
}

export interface Roles {
  dc?: string;

  /** @format int32 */
  id?: number;
  roleNm?: string;
}

export interface SearchDeclarationsResponse {
  /** 신고 내용 */
  declarationCn?: string;

  /** 신고 날짜 */
  declarationDt?: string;

  /** 신고 유형 */
  declarationTy?: string;
  error?: ErrorVo;

  /** 평가자 명 */
  name?: string;
}

export interface SearchPrductCategoryResponse {
  /** 제품 카테고리명 */
  categoryNm?: string;

  /**
   * 제품 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface SearchPrductOrderCategoryResponse {
  /** 제품 카테고리명 */
  categoryNm?: string;

  /**
   * 제품 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface SearchResponse {
  /** 액션 */
  action?: string;

  /** 접속IP */
  connectIp?: string;
  error?: ErrorVo;

  /** 메뉴명 */
  menuNm?: string;

  /**
   * 번호
   * @format int32
   */
  no?: number;

  /**
   * 처리일시
   * @format date-time
   */
  processDt?: string;

  /** 이름(아이디) */
  username?: string;
}

export interface SearchVidoCategoryResponse {
  /** 영상 카테고리명 */
  categoryNm?: string;

  /**
   * 영상 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface SearchVidoOrderCategoryResponse {
  /** 영상 카테고리명 */
  categoryNm?: string;

  /**
   * 영상 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

/**
 * SelectBox option 생성용 VO
 */
export interface SelectOptionsVo {
  /**
   * 추가정보
   * @example 연세
   */
  extra?: string;

  /**
   * 레이블
   * @example 나이
   */
  label?: string;

  /**
   * 초기선택값 여부
   * @example false
   */
  selected?: boolean;

  /**
   * 순번
   * @format int32
   * @example 0
   */
  seq?: number;

  /**
   * 선택값
   * @example age
   */
  value?: string;
}

/**
 * 결제 취소 Request
 */
export interface SettlementCancelRequest {
  /**
   * 환불 요청 금액
   * @format double
   * @example 10000
   */
  amount?: number;

  /**
   * 서비스 유형
   * @example wewill
   */
  app?: string;

  /**
   * 취소 가능한 잔액
   * @format double
   * @example 10000
   */
  checkSum?: number;

  /**
   * 아임포트결제고유ID
   * @example f87c73b7fe13b724b32836984905f2fd167fc68f
   */
  merchantUid?: string;

  /**
   * 취소사유
   * @example 중복결제로 인한 결제 취소
   */
  reason?: string;

  /**
   * 환불 수령계좌 번호
   * @example 123-45-12345
   */
  refundAccount?: string;

  /**
   * 환불 수령계좌 은행코드
   * @example 031
   */
  refundBank?: string;

  /**
   * 환불 수령계좌 예금주
   * @example 김지만
   */
  refundHolder?: string;
}

/**
 * 결제 등록 Request
 */
export interface SettlementInsertRequest {
  /**
   * 쿠폰 아이디 배열
   * @example [3,4]
   */
  clientCouponNoList?: number[];

  /**
   * 쿠폰할인금액
   * @format int32
   * @example 15000
   */
  couponDscntAmount?: number;

  /**
   * 상품명
   * @example Premium
   */
  goodsName?: string;

  /**
   * 회원 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 아임포트결제고유ID
   * @example f87c73b7fe13b724b32836984905f2fd167fc68f
   */
  merchantUid?: string;

  /**
   * 의뢰 아이디
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰 명
   * @example 의뢰명입니다.
   */
  reqestNm?: string;

  /**
   * 결제 요청 금액
   * @format int32
   * @example 200
   */
  setleReqestAmount?: number;

  /**
   * 공급가액
   * @format int32
   * @example 1280000
   */
  splpcAm?: number;

  /**
   * 부가가치세
   * @format int32
   * @example 128000
   */
  vat?: number;
}

/**
 * 결제 등록 Response
 */
export interface SettlementInsertResponse {
  /**
   * 금액 - 공급가액
   * @format int32
   * @example 1280000
   */
  amount?: number;

  /**
   * 금액 - 공급가액 포멧
   * @example 1,280,000
   */
  amountFormat?: string;

  /**
   * 현금영수증발급 사업자 등록 번호
   * @example 123-45-12345
   */
  cashReceiptBizno?: string;

  /**
   * 현금영수증발급 여부
   * @example true
   */
  cashReceiptIssued?: boolean;

  /**
   * 현금영수증발급 휴대폰 번호
   * @example 010-2323-3232
   */
  cashReceiptMbtlnum?: string;

  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 금액 포멧
   * @example 8,000
   */
  couponDscntAmountFormat?: string;
  error?: ErrorVo;

  /**
   * 상품명
   * @example Premium
   */
  goodsName?: string;

  /**
   * 회원 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 아임포트결제고유ID
   * @example 078831d6b187b2bd23157b3898a142816793fb39
   */
  merchantUid?: string;

  /**
   * 주문번호
   * @example 20210414-215520-1
   */
  no?: string;

  /**
   * 의뢰 아이디
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌, memor:수기결제
   * @example vbank
   */
  setleKnd?: string;

  /**
   * 결제종류코드명
   * @example 가상계좌
   */
  setleKndNm?: string;

  /**
   * 결제 금액 합계
   * @format int32
   * @example 1400000
   */
  setleReqestAmount?: number;

  /**
   * 결제 금액 합계 포멧
   * @example 1,400,000
   */
  setleReqestAmountFormat?: string;

  /**
   * 부가세
   * @format int32
   * @example 128000
   */
  vat?: number;

  /**
   * 부가세 포멧
   * @example 12,8000
   */
  vatFormat?: string;

  /**
   * 가상계좌 은행 표준코드 - (금융결제원기준)
   * @example 088
   */
  vbankCode?: string;

  /**
   * 입금받을 가상계좌 마감기한
   * @example 1625826618
   */
  vbankDt?: string;

  /**
   * 입금받을 가상계좌 예금주
   * @example （주）케이지이니시
   */
  vbankHolder?: string;

  /**
   * 입금받을 가상계좌 은행명
   * @example 신한(조흥)은행
   */
  vbankName?: string;

  /**
   * 입금받을 가상계좌 계좌번호
   * @example 56211102580756
   */
  vbankNum?: string;
}

/**
 * Settlement Response Vo
 */
export interface SettlementResponse {
  /** @format int32 */
  amount?: number;

  /** @format date-time */
  applyDt?: string;

  /** @format date-time */
  cancelDt?: string;
  cardCode?: string;
  cardExpirDt?: string;
  cardName?: string;
  cardNumber?: string;

  /** @format int32 */
  cardQuota?: number;
  cardType?: string;

  /** @format date-time */
  chgDt?: string;

  /** @format int32 */
  chgId?: number;
  chgIp?: string;
  delngNo?: string;

  /** @format int32 */
  memberId?: number;
  pgTid?: string;

  /** @format int32 */
  pg_fee?: number;
  receiptUrl?: string;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;
  regIp?: string;

  /** @format int32 */
  reqestId?: number;

  /** @format date-time */
  setleBgnde?: string;

  /** @format int32 */
  setleCode?: number;

  /** @format date-time */
  setleEndde?: string;
  setleKnd?: string;
  setleSttus?: string;

  /** @format int32 */
  vat?: number;
}

export interface StackBarChart {
  /**
   * 질문 타입
   * @example 001
   */
  answerTy?: string;

  /**
   * 질문 타입명
   * @example 객관식 단일
   */
  answerTyNm?: string;

  /** 차트 데이터 셋 */
  chartData?: ChartData;
  error?: ErrorVo;

  /**
   * 평가 수
   * @format int32
   * @example 1000
   */
  evlCnt?: number;

  /**
   * 설문 타입
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문 타입명
   * @example 사전 설문
   */
  evlMthdNm?: string;

  /** 차트 질문 보기목록(Option) */
  options?: Options;

  /**
   * 질문 아이디
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문 노출 시간
   * @example 00:00
   */
  qestnTime?: Time;

  /**
   * 설문지 아이디
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 별점 평균(별점차트)
   * @format float
   * @example 1
   */
  starScoreAverage?: number;

  /**
   * 질문 제목
   * @example 질문 제목
   */
  title?: string;

  /**
   * 이미지 답변 url
   * @example http://test.com
   */
  url?: string[];
}

export interface ThumbAttachFileInfoResponse {
  /** @format int32 */
  tattachFileId?: number;
  tfileExt?: string;
  tfileName?: string;
  tfileUrl?: string;
}

export interface Time {
  /** @format int32 */
  date?: number;

  /** @format int32 */
  day?: number;

  /** @format int32 */
  hours?: number;

  /** @format int32 */
  minutes?: number;

  /** @format int32 */
  month?: number;

  /** @format int32 */
  seconds?: number;

  /** @format int64 */
  time?: number;

  /** @format int32 */
  timezoneOffset?: number;

  /** @format int32 */
  year?: number;
}

export interface TokenRequest {
  /**
   * 토큰
   * @example 토큰값
   */
  token?: string;
}

export interface URI {
  absolute?: boolean;
  authority?: string;
  fragment?: string;
  host?: string;
  opaque?: boolean;
  path?: string;

  /** @format int32 */
  port?: number;
  query?: string;
  rawAuthority?: string;
  rawFragment?: string;
  rawPath?: string;
  rawQuery?: string;
  rawSchemeSpecificPart?: string;
  rawUserInfo?: string;
  scheme?: string;
  schemeSpecificPart?: string;
  userInfo?: string;
}

export interface URL {
  authority?: string;
  content?: object;

  /** @format int32 */
  defaultPort?: number;
  deserializedFields?: URLStreamHandler;
  file?: string;
  host?: string;
  path?: string;

  /** @format int32 */
  port?: number;
  protocol?: string;
  query?: string;
  ref?: string;

  /** @format int32 */
  serializedHashCode?: number;
  userInfo?: string;
}

export type URLStreamHandler = object;

export interface UploadAttachFileResponse {
  error?: ErrorVo;

  /**
   * 파일 확장자
   * @example jpg
   */
  fileExt?: string;

  /**
   * 파일명
   * @example 테스트
   */
  fileName?: string;

  /**
   * 파일 URL
   * @example https://test.com
   */
  fileUrl?: string;

  /**
   * 파일 아이디
   * @format int32
   * @example 1
   */
  id?: number;
}

/**
 * 미디어 컨버터 오류 유저 메타데이터
 */
export interface UserMetadata {
  /**
   * 고유식별자ID
   * @example 22595b2e-025d-49ca-a2b3-eb35e4b3d605
   */
  guid?: string;

  /**
   * 워크플로우명
   * @example cuspvideoservice
   */
  workflow?: string;
}

/**
 * 후원 대시보드 상세 > 평가자 최종평가 리스트 Response
 */
export interface UserReqestDashboardFinalQnAResponse {
  /**
   * 최종평가 질문 답변
   * @example 답변
   */
  answerCn?: string;

  /**
   * 최종평가 질문
   * @example 해당 구간을 가장 인상깊은 장면으로 뽑은 이유는 무엇인가요?
   */
  qestnCn?: string;
}

/**
 * 의뢰 대시보드 상세 > 평가자 설문 항목 리스트 Response
 */
export interface UserReqestDashboardQnAResponse {
  /**
   * 평가자 답변
   * @example 진돗개
   */
  answerCn?: string;

  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /** 영상 파일 리스트 */
  attachFileInfoResponse?: AttachFileInfoResponse;

  /**
   * 의뢰자 질문
   * @example 좋아하는 강아지 종은 무엇인가요?
   */
  qestnCn?: string;

  /**
   * 질문 번호
   * @example Q1
   */
  qestnNo?: string;

  /**
   * 설문지타입 (객관식 단일, 객관식 복수, 평가형, 이미지 단일, 이미지 복수, 주관식)
   * @example 객관식 단일
   */
  qestnrType?: string;

  /**
   * 행 순번
   * @format int32
   * @example 1
   */
  rowNum?: number;
}

/**
 * 의뢰 대시보드 목록 Response
 */
export interface UserReqestDashboardSearchResponse {
  /**
   * 연령층
   * @format int32
   * @example 30
   */
  age?: number;

  /**
   * 평가 소요 시간 (일)
   * @example 5
   */
  daydiff?: string;
  error?: ErrorVo;

  /**
   * 총 평가자 수
   * @format int32
   * @example 61
   */
  evlCnt?: number;

  /**
   * 평가완료일시
   * @example 2021-05-31 00:00:00
   */
  evlEndDt?: string;

  /** 최종평가 리스트 */
  evlFinalQnAList?: UserReqestDashboardFinalQnAResponse[];

  /** 설문 리스트 */
  evlResearchList?: UserReqestDashboardQnAResponse[];

  /**
   * 평가시작일시
   * @example 2021-05-31 00:00:00
   */
  evlStartDt?: string;

  /**
   * 평가 소요 시간 (시분초)
   * @example 01:15:00
   */
  hmsdiff?: string;

  /**
   * 평가자 id
   * @format int32
   * @example 115
   */
  memberId?: number;

  /**
   * 평가자
   * @example 평가자
   */
  memberNM?: string;

  /**
   * 평가자 번호
   * @format int32
   * @example 1
   */
  rowNum?: number;

  /**
   * 성별 (M / F)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 의뢰 콘텐츠 설정 (영상) 썸네일 등록 Response
 */
export interface VidoThumbnailInfo {
  /**
   * 썸네일파일id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 파일명
   * @example 도넛
   */
  fileName?: string;

  /**
   * 파일url
   * @example https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/board/event/20210423/59de7024-c5c1-47de-9224-f72b2fa4de2a.png
   */
  fileUrl?: string;

  /**
   * 사용여부
   * @example N
   */
  useYn?: string;
}

/**
 * 의뢰 콘텐츠 설정 (영상) 썸네일 등록&수정
 */
export interface VidoThumbnailMerge {
  /**
   * 썸네일파일id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 사용여부
   * @example N
   */
  useYn?: string;
}

export interface WebSocketVo {
  /**
   * 웹소켓서버 호스트 URL
   * @example https://websocket.we-will.link
   */
  host?: string;

  /**
   * 웹소켓서버 WS-STOMP URL
   * @example https://websocket.we-will.link
   */
  subUrl?: string;

  /**
   * 웹소켓서버 호스트
   * @example https://websocket.we-will.link
   */
  wsUrl?: string;
}

export interface WordCloud {
  /** 단어 */
  text?: string;

  /**
   * 단어 수
   * @format int32
   */
  value?: number;
}

export interface ProductThumbInfoResponse {
  /** @format int32 */
  attachFileIdtf?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  prductId?: number;
}

export interface ListVoAddressBookNationCodeResponse {
  content?: AddressBookNationCodeResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoAdminEvaluatorContractInfoResponse {
  content?: AdminEvaluatorContractInfoResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoAdminQratingSearchResponse {
  content?: AdminQratingSearchResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoChargersModifyResponse {
  content?: ChargersModifyResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoCodeGroupListResponse {
  content?: CodeGroupListResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoCodeGroupTreeResponse {
  content?: CodeGroupTreeResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoCodeListResponse {
  content?: CodeListResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchPrductCategoryResponse {
  content?: SearchPrductCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchPrductOrderCategoryResponse {
  content?: SearchPrductOrderCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchVidoCategoryResponse {
  content?: SearchVidoCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchVidoOrderCategoryResponse {
  content?: SearchVidoOrderCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoWordCloud {
  content?: WordCloud[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface PageInfoSettlementResponse {
  /** @format int32 */
  endRow?: number;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;
  isFirstPage?: boolean;
  isLastPage?: boolean;

  /** @format int32 */
  lastPage?: number;
  list?: SettlementResponse[];

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int64 */
  total?: number;
}

export interface PagingInfoAddressBookSearchResponse {
  content?: AddressBookSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminCilentMemberRefundSearchResponse {
  content?: AdminCilentMemberRefundSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminClientCouponResponse {
  content?: AdminClientCouponResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminClientMemberCouponListResponse {
  content?: AdminClientMemberCouponListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminClientMemberRequestListResponse {
  content?: AdminClientMemberRequestListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminClientMemberResponse {
  content?: AdminClientMemberResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminClientMemberSearchResponse {
  content?: AdminClientMemberSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminClientMemberSettlementListResponse {
  content?: AdminClientMemberSettlementListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminCouponResponse {
  content?: AdminCouponResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminCustomerSearchResponse {
  content?: AdminCustomerSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminEvaluatorDailyAnswersResponse {
  content?: AdminEvaluatorDailyAnswersResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminEvaluatorEvaluationResponse {
  content?: AdminEvaluatorEvaluationResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminEvaluatorMemberListResponse {
  content?: AdminEvaluatorMemberListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminEvaluatorPointResponse {
  content?: AdminEvaluatorPointResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminMonitorsSearchResponse {
  content?: AdminMonitorsSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminPointListResponse {
  content?: AdminPointListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminPointManageResponse {
  content?: AdminPointManageResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminReportAnswerResponse {
  content?: AdminReportAnswerResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoApprovalSearchResponse {
  content?: ApprovalSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoBillpublicationSearchResponse {
  content?: BillpublicationSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoBoardSearchResponse {
  content?: BoardSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoChargerInspectManagerSearchResponse {
  content?: ChargerInspectManagerSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoChargerSearchResponse {
  content?: ChargerSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoClientIntroSearchResponse {
  content?: ClientIntroSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoCompleteSearchResponse {
  content?: CompleteSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoDailyQuestionSearchResponse {
  content?: DailyQuestionSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoDeclarationSearchResponse {
  content?: DeclarationSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoEstmtSearchResponse {
  content?: EstmtSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoEvaluationSearchResponse {
  content?: EvaluationSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoManagerSearchResponse {
  content?: ManagerSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoMemberidPointSumInterface {
  content?: MemberidPointSumInterface[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPaymentResult {
  content?: PaymentResult[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPointDefraymentResponse {
  content?: PointDefraymentResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPointDetail {
  content?: PointDetail[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPointMasterResponse {
  content?: PointMasterResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPostEventSearchResponse {
  content?: PostEventSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPostNoticeSearchResponse {
  content?: PostNoticeSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoReqestSearchResponse {
  content?: ReqestSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoRequestCancelSearchResponse {
  content?: RequestCancelSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoSearchDeclarationsResponse {
  content?: SearchDeclarationsResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoSearchResponse {
  content?: SearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface SearchCashReceiptUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 이름 */
  name?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간시작 */
  sDate?: string;

  /** 주문번호 */
  settlementNo?: string;

  /** 아이디 */
  userName?: string;
}

export interface DownloadCashReceiptExcelUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 이름 */
  name?: string;

  /** 기간시작 */
  sDate?: string;

  /** 주문번호 */
  settlementNo?: string;

  /** 아이디 */
  userName?: string;
}

export interface SearchRequestCancelUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 이름 */
  name?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간시작 */
  sDate?: string;

  /** 주문번호 */
  settlementNo?: string;

  /** 아이디 */
  userName?: string;
}

export interface DownloadRequestCancelExcelUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 이름 */
  name?: string;

  /** 기간시작 */
  sDate?: string;

  /** 주문번호 */
  settlementNo?: string;

  /** 아이디 */
  userName?: string;
}

export interface SearchTaxBillUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 이름 */
  name?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간시작 */
  sDate?: string;

  /** 주문번호 */
  settlementNo?: string;

  /** 아이디 */
  userName?: string;
}

export interface DownloadTaxBillExcelUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 이름 */
  name?: string;

  /** 기간시작 */
  sDate?: string;

  /** 주문번호 */
  settlementNo?: string;

  /** 아이디 */
  userName?: string;
}

export interface SearchAddressBooksUsingGetParams {
  /** 검색 조건(이름: N , 소속/회사: C, 포지션/직책: P, 전화번호: T, 주소: A) */
  condition?: string;

  /** 이메일 */
  email?: string;

  /** 종료 날짜 */
  endDt?: string;

  /** 검색어 */
  keyWord?: string;

  /** 해외/국내(국내 : I , 해외 : F , 전체 : A) */
  nation?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 시작 날짜 */
  startDt?: string;
}

export interface DownloadAddressBookExcelUsingGetParams {
  /** 검색 조건(이름: N , 소속/회사: C, 포지션/직책: P, 전화번호: T, 주소: A) */
  condition?: string;

  /** 이메일 */
  email?: string;

  /** 종료 날짜 */
  endDt?: string;

  /** 검색어 */
  keyWord?: string;

  /** 해외/국내(국내 : I , 해외 : F , 전체 : A) */
  nation?: string;

  /** 시작 날짜 */
  startDt?: string;
}

export interface AuditListUsingGetParams {
  /** 액션 (C: 등록, R: 조회, U: 수정, D: 삭제) */
  action?: string;

  /** 검색조건 (NM: 이름, ID: 아이디, MN: 메뉴명, IP: 접속IP) */
  condition?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지당 리스트 수
   * @format int32
   */
  pageSize: number;

  /** 검색할 월 */
  searchMonth?: string;
}

export interface SearchBoardUsingGetParams {
  /** 댓글유무(Y/N) */
  answerYn?: string;

  /** 첨부파일유무(Y/N) */
  atchmnflYn?: string;

  /** 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자) */
  bbsLc?: string[];

  /**
   * 게시판설정 ID
   * @format int32
   */
  id?: number;

  /** 게시일유무(Y/N) */
  openDateYn?: string;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;

  /** 구분(N:공지사항, E:이벤트) */
  se?: string;
}

export interface InsertBoardUsingPostParams {
  /** bbsLs */
  bbsLs: "GateM" | "GateC" | "MARKER" | "CREATOR";

  /** se */
  se: "Notice" | "Event";
}

export interface ModifyBoardUsingPutParams {
  /** bbsLs */
  bbsLs: "GateM" | "GateC" | "MARKER" | "CREATOR";

  /** se */
  se: "Notice" | "Event";

  /**
   * id
   * @format int32
   */
  id: number;
}

export interface ClientCouponListUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface CouponDownExcelUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;
}

export interface DownExcelUsingGetParams {
  /**
   * 의뢰자 구분(전체:A, 개인:P, 법인:C)
   * @example A
   */
  bizSection?: string;

  /**
   * 기간 검색 조건(선택:A, 가입일:J, 탈퇴일:S, 최종 접속일:R)
   * @example A
   */
  dateCondition?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 종료일
   * @format date
   * @example 2021-01-01
   */
  endDt?: string;

  /**
   * 검색어
   * @example test
   */
  keyword?: string;

  /**
   * 검색어 조건(아이디:U, 이름:N, 핸드폰 번호:P)
   * @example U
   */
  searchCondition?: string;

  /**
   * 성별(전쳬:A, 남:M, 여:F)
   * @example M
   */
  sexdstn?: string;

  /**
   * 시작일
   * @format date
   * @example 1990-01-01
   */
  startDt?: string;

  /**
   * 상태 조건(전체:A, 활성:N, 탈퇴:S)
   * @example A
   */
  stateCondition?: string;
}

export interface ClientRefundListUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface RefundDownExcelUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;
}

export interface ClientRequestListUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface ClientSettlementListUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface SettlementDownExcelUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;
}

export interface SearchMemberUsingGetParams {
  /**
   * 의뢰자 구분(전체:A, 개인:P, 법인:C)
   * @example A
   */
  bizSection?: string;

  /**
   * 기간 검색 조건(선택:A, 가입일:J, 탈퇴일:S, 최종 접속일:R)
   * @example A
   */
  dateCondition?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 종료일
   * @format date
   * @example 2021-01-01
   */
  endDt?: string;

  /**
   * 검색어
   * @example test
   */
  keyword?: string;

  /**
   * 현재 페이지 번호
   * @format int32
   * @example 1
   */
  pageNum?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 검색어 조건(아이디:U, 이름:N, 핸드폰 번호:P)
   * @example U
   */
  searchCondition?: string;

  /**
   * 성별(전쳬:A, 남:M, 여:F)
   * @example M
   */
  sexdstn?: string;

  /**
   * 시작일
   * @format date
   * @example 1990-01-01
   */
  startDt?: string;

  /**
   * 상태 조건(전체:A, 활성:N, 탈퇴:S)
   * @example A
   */
  stateCondition?: string;
}

export interface SearchClientIntroUsingGetParams {
  /** 종료일(2021-05-31) */
  endDt: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface FindAllCouponUsingGetParams {
  /**
   * 검색 쿠폰생성일(시작일)
   * @example 2021-04-20
   */
  couponBgnde?: string;

  /**
   * 쿠폰 할인 혜택(퍼센트 할인(DC) or 금액할인(FD))
   * @example DC
   */
  couponDscntKnd?: string;

  /**
   * 검색 쿠폰생성일(종료일)
   * @example 2021-04-21
   */
  couponEndde?: string;

  /**
   * 쿠폰상태("":전체, A:활성, C:종료)
   * @example A
   */
  couponSttus?: string;

  /**
   * 쿠폰 유형(관리자가 회원에게 등록(AD) or 회원이 스스로 등록(CL))
   * @example AD
   */
  couponTy?: string;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;

  /**
   * 검색어 구분
   * @example 의뢰
   */
  searchKey?: string;

  /** @example COUPON_NAME */
  searchKnd?: string;
}

export interface SearchMemberUsingGet1Params {
  /**
   * email 주소
   * @example example@blocksmith.xyz
   */
  email?: string;

  /**
   * 종료일
   * @example 2021-04-21
   */
  endDt?: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize: number;

  /**
   * 기간검색(등록일:R / 최종접속일:RC)
   * @example R
   */
  periodType?: string;

  /**
   * 검색어 종류값에 따른 검색어 입력
   * @example asdf
   */
  searchKey?: string;

  /**
   * 검색어 종류(USERNAME : 아이디, NAME : 이름, BIRTHDAY : 생년월일
   * @example USERNAME
   */
  searchKnd?: string;

  /**
   * 성별('F':여성, 'M':남성, '':전체)
   * @example ''
   */
  sexdstn?: string;

  /**
   * 시작일
   * @example 2021-04-20
   */
  startDt?: string;
}

export interface FindAllClientCouponUsingGetParams {
  /**
   * @format int32
   * @example 1
   */
  no: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface RemoveClientCouponUsingDeleteParams {
  clientCouponNoArr?: number[];
}

export interface DownClientCouponExcelDownUsingGet1Params {
  /**
   * @format int32
   * @example 1
   */
  no: number;
}

export interface SearchCreatorsCustomerUsingGetParams {
  /** 문의 카테고리대(001:창작자,002:공유자) */
  CsCategoryLrge: string;

  /** 검색기준 UN:회원이름 UI:회원ID AN:담당자이름 AI:담당자ID */
  condition?: string;

  /** 문의 카테고리중 */
  csCategoryMiddl?: string;

  /** 기간종료 */
  eDate?: string;

  /** 검색어 */
  keyword?: string;

  /** 내문의(Y또는N) */
  myCs?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간시작 */
  sDate?: string;

  /** 상태값 */
  status?: string;
}

export interface SearchMarkersCustomerUsingGetParams {
  /** 문의 카테고리대(001:창작자,002:공유자) */
  CsCategoryLrge: string;

  /** 검색기준 UN:회원이름 UI:회원ID AN:담당자이름 AI:담당자ID */
  condition?: string;

  /** 문의 카테고리중 */
  csCategoryMiddl?: string;

  /** 기간종료 */
  eDate?: string;

  /** 검색어 */
  keyword?: string;

  /** 내문의(Y또는N) */
  myCs?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간시작 */
  sDate?: string;

  /** 상태값 */
  status?: string;
}

export interface SearchAdminMonitorsUsingGetParams {
  /** 기간종료 */
  eDate?: string;

  /** 내문의(Y또는N) */
  myCs?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간시작 */
  sDate?: string;

  /** 담당자이름 */
  smgName?: string;
}

export interface SearchDailyUsingGetParams {
  /** 검색기준(D:데일리 설문명, M:등록자) */
  condition?: string;

  /** 날짜검색기준(E:노출일, R:등록일) */
  dateCondition?: string;

  /** 기간종료 */
  eDate?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize?: number;

  /** 기간시작 */
  sDate?: string;
}

export interface SearchEvaluatorUsingGetParams {
  /** 기간 조건(전체:A 가입일:J 탈퇴일:S 최종접속일:R */
  dateCondition?: string;

  /** 이메일 */
  email?: string;

  /**
   * 기간 종료일
   * @format date
   */
  endDt?: string;

  /** 평가자 등급(A:전체 W1:루키 WP1:루키2 W2:올스타 SP:세미프로 SP2:세미프로2) */
  gradeCondition?: string;

  /** keyword */
  keyword?: string;

  /** 검색어 조건(U:아이디 N:이름 M:핸드폰번호 A:주소) */
  keywordCondition?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 포인트 조건(M:보유포인트 D:출금 완료 포인트) */
  pointCondition?: string;

  /**
   * 포인트(end)
   * @format int32
   */
  pointEnd?: number;

  /**
   * 포인트(start)
   * @format int32
   */
  pointStart?: number;

  /**
   * 등급점수(end)
   * @format int32
   */
  scoreEnd?: number;

  /**
   * 등급점수(start)
   * @format int32
   */
  scoreStart?: number;

  /** 성별(A:전체 M:남자 F:여자 */
  sexdstn?: string;

  /**
   * 기간 시작일
   * @format date
   */
  startDt?: string;

  /** 상태 조건(A:전체 C:활성 S:탈퇴) */
  statusCondition?: string;
}

export interface DownExcelMemberDailyAnswersUsingGetParams {
  /**
   * 회원 id
   * @format int32
   */
  memberId?: number;
}

export interface DownExcelMemberEvaluationsUsingGetParams {
  /**
   * 회원 id
   * @format int32
   */
  memberId?: number;
}

export interface DownClientCouponExcelDownUsingGetParams {
  /**
   * 회원 id
   * @format int32
   */
  memberId?: number;
}

export interface InfoMemberDailyAnswersUsingGetParams {
  /**
   * 회원 id
   * @format int32
   */
  memberId?: number;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize?: number;
}

export interface InfoMemberEvaluationsUsingGetParams {
  /**
   * 회원 id
   * @format int32
   */
  memberId?: number;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize?: number;
}

export interface InfoEvaluatorEvlCntUsingGetParams {
  /**
   * 회원 아이디
   * @format int32
   */
  memberId?: number;
}

export interface DownEvaluatorMemberExcelDownUsingGetParams {
  /** 기간 조건(전체:A 가입일:J 탈퇴일:S 최종접속일:R */
  dateCondition?: string;

  /** 이메일 */
  email?: string;

  /**
   * 기간 종료일
   * @format date
   */
  endDt?: string;

  /** 평가자 등급(A:전체 W1:루키 WP1:루키2 W2:올스타 SP:세미프로 SP2:세미프로2) */
  gradeCondition?: string;

  /** keyword */
  keyword?: string;

  /** 검색어 조건(U:아이디 N:이름 M:핸드폰번호 A:주소) */
  keywordCondition?: string;

  /** 포인트 조건(M:보유포인트 D:출금 완료 포인트) */
  pointCondition?: string;

  /**
   * 포인트(end)
   * @format int32
   */
  pointEnd?: number;

  /**
   * 포인트(start)
   * @format int32
   */
  pointStart?: number;

  /**
   * 등급점수(end)
   * @format int32
   */
  scoreEnd?: number;

  /**
   * 등급점수(start)
   * @format int32
   */
  scoreStart?: number;

  /** 성별(A:전체 M:남자 F:여자 */
  sexdstn?: string;

  /**
   * 기간 시작일
   * @format date
   */
  startDt?: string;

  /** 상태 조건(A:전체 C:활성 S:탈퇴) */
  statusCondition?: string;
}

export interface InfoMemberPointsUsingGetParams {
  /**
   * @format int32
   * @example 134
   */
  memberId?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 4
   */
  pageSize?: number;
}

export interface SearchApprovalUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, TL: 의뢰명, NM: 의뢰자명, ID: 의뢰자ID, MN: 견적산출담당자명, MI: 견적산출담당자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 견적산출상태 (30: 견적승인요청, 40: 견적산출완료, 50: 견적반려) */
  estmtComputStatus?: string[];

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 정렬조건 (RD: 의뢰신청일 Desc, RA: 의뢰신청일 Asc, ED: 견적승인요청일 Desc, EA: 견적승인요청일 Asc, LD: 견적산출지연일 Desc, LA: 견적산출지연일 Asc, VD: 영상재생시간 Desc, VA: 영상재생시간 Asc) */
  sortBy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchChargerUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, TL: 의뢰명, NM: 의뢰자명, ID: 의뢰자ID, EN: 견적산출담당자명, EI: 견적산출담당자ID, AN: 심사담당자명, AI: 심사담당자ID, VN: 평가담당자명, VI: 평가담당자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 견적산출상태 (10: 견적미산출, 20: 결적산출중, 30: 견적승인요청, 40: 견적산출완료, 50: 견적반려) */
  estmtComputStatus?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 의뢰상태 ([IN001] 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지) */
  reqestStatus?: string[];

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 정렬조건 (RD: 의뢰신청일 Desc, RA: 의뢰신청일 Asc, VD: 영상재생시간 Desc, VA: 영상재생시간 Asc, QD: 총설문수 Desc, QA: 총설문수 Asc, ED: 견적산출지연일 Desc, EA: 견적산출지연일 Asc, AD: 심사지연일 Desc, AA: 심사지연일 Asc) */
  sortBy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchInspectManagerUsingGetParams {
  /** 검색조건 (MN: 검수관리자명, MI: 검수관리자ID) */
  condition?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface SearchReqestCompleteUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, TL: 의뢰제목, NM: 의뢰자명, ID: 의뢰자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 의뢰상태 ([IN001]80:진행중, 100:완료) */
  reqestStatus?: string[];

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 정렬조건 (RD: 의뢰신청일 Desc, RA: 의뢰신청일 Asc, ED: 평가진행시작일 Desc, EA: 평가진행시작일 Asc, PD: 진행율 Desc, PA: 진행율 Asc) */
  sortBy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchDeclarationUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, NM: 의뢰자명, ID: 의뢰자ID, MN: 평가검수담당자명, MI: 평가검수담당자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 의뢰상태 ([IN001] 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지) */
  reqestStatus?: string[];

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 정렬조건 (DD: 신규신고일 Desc, DA: 신규신고일 Asc, RD: 의뢰신청일 Desc, RA: 의뢰신청일 Asc, CD: 신고건수 Desc, CA: 신고건수 Asc, GD: 목표평가자수 Desc, GA: 목표평가자수 Asc) */
  sortBy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchDeclarationsUsingGet1Params {
  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;
}

export interface SearchEstmtUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, TL: 의뢰제목, NM: 의뢰자명, ID: 의뢰자ID, MN: 수정담당자명, MI: 수정담당자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 견적산출상태 (10: 견적미산출, 20: 결적산출중, 30: 견적승인요청, 40: 견적산출완료, 50: 견적반려) */
  reqestEstmtApproval?: string;

  /** 의뢰상태 ([IN001] 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지) */
  reqestStatus?: string[];

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchEvaluationUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, TL: 의뢰제목, NM: 의뢰자명, ID: 의뢰자ID, MN: 수정담당자명, MI: 수정담당자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 의뢰상태 ([IN001] 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지) */
  reqestStatus?: string[];

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 정렬순서 (RD: 의뢰신청일 Desc, RA: 의뢰신청일 Asc, SD: 주관식설문수 Desc, SA: 주관식설문수 Asc, ED: 진행일 Desc, EA: 진행일 Asc, PD: 진행율 Desc, PA: 진행율 Asc) */
  sortBy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface RvChargerManageSaveUsingPutParams {
  /**
   * 의뢰평가 식별자
   * @format int32
   */
  rvId?: number;
}

export interface RvChargerNecessarySaveUsingPutParams {
  /** 필수확인담당자 체크여부 (Y / N) */
  checkYn?: string;

  /**
   * 의뢰평가 식별자
   * @format int32
   */
  rvId?: number;
}

export interface RvChargerRemoveSaveUsingPutParams {
  /**
   * 의뢰평가 식별자
   * @format int32
   */
  rvId?: number;
}

export interface InfoResultExcelEvaluationUsingGetParams {
  /** 정상/불성실 임시 ('': 전체, Y: 불성실평가자, N: 정상평가자) */
  dishonestEvlTmpYn?: string;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;
}

export interface InfoResultEvaluationUsingGetParams {
  /** 정상/불성실 임시 ('': 전체, Y: 불성실평가자, N: 정상평가자) */
  dishonestEvlTmpYn?: string;

  /**
   * 의뢰 식별자
   * @format int32
   */
  id?: number;
}

export interface SearchReqestUsingGetParams {
  /** 검색조건 (PK: 의뢰고유번호, TL: 의뢰제목, NM: 의뢰자명, ID: 의뢰자ID, MN: 수정담당자명, MI: 수정담당자ID) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 의뢰컨텐츠 (V: 영상, P: 제품) */
  reqestCntnts?: string;

  /** 의뢰상태 ([IN001] 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지) */
  reqestStatus?: string[];

  /** 의뢰상품 (B:basic, P:premium) */
  reqestTy?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchManagerUsingGetParams {
  /** 검색조건(L : 아이디, N : 이름, M : 핸드폰번호) */
  condition?: string;

  /** 이메일 */
  email?: string;

  /** 종료일(2021-03-31) */
  endDt?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 기간검색(등록일:R / 최종접속일:RC) */
  periodType?: string;

  /** 시작일(2021-03-01) */
  startDt?: string;
}

export interface ResetStaffPwInfoUsingPostParams {
  /**
   * 관리자 ID
   * @format int32
   */
  id: number;
}

export interface ModifyManagerUsingPutParams {
  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mobileNo?: string;

  /**
   * id
   * @format int32
   */
  id: number;
}

export interface SearchReqestDashboardUsingGetParams {
  /**
   * 평가자 번호
   * @format int32
   */
  rowNum: number;

  /**
   * reqestId
   * @format int32
   */
  reqestId: number;
}

export interface SelectReturnResnInReqestCancelUsingGetParams {
  /**
   * 의뢰ID
   * @format int32
   */
  reqestId: number;
}

export interface FindPointDefrayExcelDownUsingGetParams {
  EDate?: string;
  SDate?: string;

  /**
   * 신청자아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 신청자명
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 상태 ("":전체, 00:출금신청, 10:출금완료, 20:출금반려)
   * @example ""
   */
  defraySttus?: string;
}

export interface FindPointDefrayUsingGetParams {
  EDate?: string;
  SDate?: string;

  /**
   * 신청자아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 신청자명
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 상태 ("":전체, 00:출금신청, 10:출금완료, 20:출금반려)
   * @example ""
   */
  defraySttus?: string;

  /**
   * 현재 페이지정보
   * @format int32
   * @example 1
   */
  pageNum: number;

  /**
   * 페이지 크기
   * @format int32
   * @example 10
   */
  pageSize: number;
}

export interface FindAllPointDetailUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId?: number;

  /**
   * no
   * @format int32
   */
  no?: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface FindAllPointMasterUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId?: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface FindGroupAllPointMasterUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;
}

export interface FindGroupPointMasterUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId?: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface FindGroupAdminPointManageUsingGetParams {
  /**
   * email 주소
   * @example example@blocksmith.xyz
   */
  email?: string;

  /**
   * 종료일
   * @example 2021-04-21
   */
  endDt?: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 기간검색(등록일:R / 최종접속일:RC)
   * @example R
   */
  periodType?: string;

  /**
   * 검색어 종류값에 따른 검색어 입력
   * @example asdf
   */
  searchKey?: string;

  /**
   * 검색어 종류(USERNAME : 아이디, NAME : 이름, BIRTHDAY : 생년월일
   * @example USERNAME
   */
  searchKnd?: string;

  /**
   * 성별('F':여성, 'M':남성, '':전체)
   * @example ''
   */
  sexdstn?: string;

  /**
   * 시작일
   * @example 2021-04-20
   */
  startDt?: string;
}

export interface FindAdminPointListUsingGetParams {
  /**
   * 성실/불성실("":전체, Y:불성실, N:성실)
   * @example N
   */
  dishonestEvl?: string;

  /**
   * 종료일
   * @example 2021-12-31
   */
  endDt: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 4
   */
  pageSize: number;

  /**
   * 포인트 상태 ("":전체, W:적립 예정, A:적립 완료, U:차감, E:소멸, D:출금)
   * @example A
   */
  pointSttus?: string;

  /**
   * 검색어 구분
   * @example 홍길동
   */
  searchKey?: string;

  /**
   * 검색어 종류(NAME : 이름, USERNAME : 아이디, REQUEST_NM : 영상/제품명, POINT : 포인트, OPERATOR_NM : 처리자 이름, OPERATOR_ID : 처리자 아이디)
   * @example NAME
   */
  searchKnd?: string;

  /**
   * 시작일
   * @example 2021-01-01
   */
  startDt: string;

  /**
   * System 여부("":전체, Y:포함, N:불포함
   * @example N
   */
  systemYn?: string;
}

export interface InsertPostFileUsingPostParams {
  /**
   * attachFileId
   * @format int32
   */
  attachFileId: number;

  /**
   * postId
   * @format int32
   */
  postId: number;
}

export interface DeletePostFileUsingDeleteParams {
  /**
   * postId
   * @format int32
   */
  postId: number;

  /**
   * id
   * @format int32
   */
  id: number;
}

export interface SearchEventPostUsingGetParams {
  /** 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자) */
  bbsLc?: string[];

  /** 검색조건 S(제목)/C(내용)/SC(제목+내용)/R(작성자명) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 이벤트 상태(W : 대기, P : 진행, D : 마감) */
  eventStatus?: string[];

  /** 노출 여부(Y : 노출 / N : 노출 안함) */
  exposureYn?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 구분(N:공지사항, E:이벤트) */
  se?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SearchNoticePostUsingGetParams {
  /** 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자) */
  bbsLc?: string[];

  /** 검색조건 S(제목)/C(내용)/SC(제목+내용)/R(작성자명) */
  condition?: string;

  /** 종료일(2021-05-31) */
  endDt: string;

  /** 노출 여부(Y : 노출 / N : 노출 안함) */
  exposureYn?: string;

  /** 검색어 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;

  /** 구분(N:공지사항, E:이벤트) */
  se?: string;

  /** 시작일(2021-04-20) */
  startDt: string;
}

export interface SendNewPasswordUsingGetParams {
  /** username */
  username: string;
}

export interface UploadAttachFileUsingPostParams {
  /**
   * 등록자 아이디(회원가입시 빈값 입력)
   * @format int32
   */
  regId?: number;

  /** 업로드 경로(예시:board/attach) */
  subPath?: string;
}

export interface GetCodeNameUsingGetParams {
  /** codeGroupId */
  codeGroupId: string;

  /** codeId */
  codeId?: string;
}

export interface PayCompleteUsingPostParams {
  /** app */
  app?: string;

  /** imp_uid */
  imp_uid?: string;

  /** merchant_uid */
  merchant_uid?: string;
}

export interface PayGenerateUsingPostParams {
  /** amount */
  amount?: string;

  /** app */
  app?: string;

  /** member_id */
  member_id?: string;

  /** order_name */
  order_name?: string;

  /** product_id */
  product_id?: string;
}

export interface SearchSettlementUsingGetParams {
  /** @example 1 */
  setleCode?: string;
}

export interface PaymentResultListPagingUsingGetParams {
  /**
   * 회원시퀀스
   * @format int32
   */
  memSeq: number;

  /** 기간[A:전체 / W:1주일 / 1:1개월 / 3:3개월] */
  period: string;

  /**
   * 페이지번호
   * @format int32
   * @example 1
   */
  pageNo: number;

  /**
   * 페이지당 리스트 수
   * @format int32
   * @example 10
   */
  pageSize: number;
}

import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, ResponseType } from "axios";
import qs from "qs";

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, "data" | "params" | "url" | "responseType"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, "data" | "cancelToken"> {
  securityWorker?: (
    securityData: SecurityDataType | null,
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || "//dev1.blocksmith.xyz:28090" });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.instance.defaults.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = (format && this.format) || void 0;

    if (type === ContentType.FormData) {
      const formData = new FormData();
      requestParams.headers.common = { Accept: "*/*" };
      requestParams.headers.post = {};
      requestParams.headers.put = {};

      const fromBody = body as any;
      for (const property in fromBody) {
        formData.append(property, fromBody[property]);
      }
      body = formData;
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
      paramsSerializer: (p) => {
        return qs.stringify(p, { arrayFormat: "repeat" });
      },
    });
  };
}

/**
 * @title WEWILL 서비스 API
 * @version 0.1
 * @license
 * @baseUrl //dev1.blocksmith.xyz:28090
 *
 * WEWILL 서비스 API
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  /**
   * @description 사업자번호를 이용하여 홈택스 API 조회
   *
   * @tags [샘플코드] - Biznum API
   * @name FindByBiznumUsingGet
   * @summary 국세청 홈택스 사업자등록번호 조회
   * @request GET:/api/biznum/{code}
   * @secure
   */
  findByBiznumUsingGet = (code: string, params: RequestParams = {}) =>
    this.request<BiznumResponse, void>({
      path: `/api/biznum/${code}`,
      method: "GET",
      secure: true,
      format: "json",
      ...params,
    });

  /**
   * @description 메일발송
   *
   * @tags [랜딩페이지] - 공통 API
   * @name ExecMailUsingPost
   * @summary 메일발송
   * @request POST:/api/common/mail
   * @secure
   */
  execMailUsingPost = (mailVo: MailVo, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/api/common/mail`,
      method: "POST",
      body: mailVo,
      secure: true,
      type: ContentType.Json,
      ...params,
    });

  /**
   * @description 메일발송
   *
   * @tags [랜딩페이지] - 공통 API
   * @name ExecMail2UsingPost
   * @summary 메일발송
   * @request POST:/api/common/mail2
   * @secure
   */
  execMail2UsingPost = (mailVo: MailVo, params: RequestParams = {}) =>
    this.request<void, void>({
      path: `/api/common/mail2`,
      method: "POST",
      body: mailVo,
      secure: true,
      type: ContentType.Json,
      ...params,
    });

  accounting = {
    /**
     * @description 발행여부 상태 수정
     *
     * @tags [Admin Accounting] - 회계 API
     * @name ModifyBillStatusUsingPut
     * @summary 계산서 발행 > 세금계산서&현금영수증 목록 ::발행여부 상태 수정
     * @request PUT:/api/wewill/admin/accounting/bill/set
     * @secure
     */
    modifyBillStatusUsingPut: (request: BillStatusModifyRequest[], params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/accounting/bill/set`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 세금계산서 목록 조회
     *
     * @tags [Admin Accounting] - 회계 API
     * @name SearchCashReceiptUsingGet
     * @summary 계산서 발행 > 현금영수증 목록
     * @request GET:/api/wewill/admin/accounting/cash-receipt
     * @secure
     */
    searchCashReceiptUsingGet: (query: SearchCashReceiptUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoBillpublicationSearchResponse, void>({
        path: `/api/wewill/admin/accounting/cash-receipt`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 현금영수증 엑셀 다운로드
     *
     * @tags [Admin Accounting] - 회계 API
     * @name DownloadCashReceiptExcelUsingGet
     * @summary 계산서 발행 > 현금영수증 목록 :: 엑셀 다운로드 API
     * @request GET:/api/wewill/admin/accounting/cash-receipt/excel
     * @secure
     */
    downloadCashReceiptExcelUsingGet: (query: DownloadCashReceiptExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/accounting/cash-receipt/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰취소 목록 조회
     *
     * @tags [Admin Accounting] - 회계 API
     * @name SearchRequestCancelUsingGet
     * @summary 취소 > 취소신청 목록
     * @request GET:/api/wewill/admin/accounting/requestCancel
     * @secure
     */
    searchRequestCancelUsingGet: (query: SearchRequestCancelUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoRequestCancelSearchResponse, void>({
        path: `/api/wewill/admin/accounting/requestCancel`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰취소 엑셀 다운로드
     *
     * @tags [Admin Accounting] - 회계 API
     * @name DownloadRequestCancelExcelUsingGet
     * @summary 취소 > 취소신청 목록 :: 엑셀 다운로드 API
     * @request GET:/api/wewill/admin/accounting/requestCancel/excel
     * @secure
     */
    downloadRequestCancelExcelUsingGet: (query: DownloadRequestCancelExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/accounting/requestCancel/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰취소신청 상세 조회
     *
     * @tags [Admin Accounting] - 회계 API
     * @name SelectRequestCancelUsingGet
     * @summary 취소 > 취소신청 목록 > 상세
     * @request GET:/api/wewill/admin/accounting/requestCancel/{id}
     * @secure
     */
    selectRequestCancelUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<RequestCancelInfoResponse, void>({
        path: `/api/wewill/admin/accounting/requestCancel/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 취소 상태 수정
     *
     * @tags [Admin Accounting] - 회계 API
     * @name ModifyProcessStatusUsingPut
     * @summary 취소 > 취소신청 목록 > 상세::취소 처리 상태 수정
     * @request PUT:/api/wewill/admin/accounting/requestCancel/{id}/setStatus
     * @secure
     */
    modifyProcessStatusUsingPut: (id: number, request: CancelStatusModifyRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/accounting/requestCancel/${id}/setStatus`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 세금계산서 목록 조회
     *
     * @tags [Admin Accounting] - 회계 API
     * @name SearchTaxBillUsingGet
     * @summary 계산서 발행 > 세금계산서 목록
     * @request GET:/api/wewill/admin/accounting/tax-bill
     * @secure
     */
    searchTaxBillUsingGet: (query: SearchTaxBillUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoBillpublicationSearchResponse, void>({
        path: `/api/wewill/admin/accounting/tax-bill`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 세금계산서 엑셀 다운로드
     *
     * @tags [Admin Accounting] - 회계 API
     * @name DownloadTaxBillExcelUsingGet
     * @summary 계산서 발행 > 세금계산서 목록 :: 엑셀 다운로드 API
     * @request GET:/api/wewill/admin/accounting/tax-bill/excel
     * @secure
     */
    downloadTaxBillExcelUsingGet: (query: DownloadTaxBillExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/accounting/tax-bill/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 세금계산서&현금영수증 상세 조회
     *
     * @tags [Admin Accounting] - 회계 API
     * @name SelectTaxBillUsingGet
     * @summary 계산서 발행 > 세금계산서&현금영수증 목록 > (공통)주문번호 상세
     * @request GET:/api/wewill/admin/accounting/{settlementNo}
     * @secure
     */
    selectTaxBillUsingGet: (settlementNo?: string, params: RequestParams = {}) =>
      this.request<BillPublicationInfoResponse, void>({
        path: `/api/wewill/admin/accounting/${settlementNo}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  addressbooks = {
    /**
     * @description 주소록 목록 조회
     *
     * @tags [Master] - 주소록 API
     * @name SearchAddressBooksUsingGet
     * @summary 주소록 > 주소록 목록 조회 API
     * @request GET:/api/wewill/admin/addressbooks
     * @secure
     */
    searchAddressBooksUsingGet: (query: SearchAddressBooksUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAddressBookSearchResponse, void>({
        path: `/api/wewill/admin/addressbooks`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 주소록 생성
     *
     * @tags [Master] - 주소록 API
     * @name InsertAddressBookUsingPost
     * @summary 주소록 > 주소록 상세 정보 ::: 주소록 생성 API
     * @request POST:/api/wewill/admin/addressbooks
     * @secure
     */
    insertAddressBookUsingPost: (request: AddressBookInsertRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/addressbooks`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 주소록 삭제
     *
     * @tags [Master] - 주소록 API
     * @name DeleteAddressBookUsingDelete
     * @summary 주소록 > 주소록 삭제 API
     * @request DELETE:/api/wewill/admin/addressbooks
     * @secure
     */
    deleteAddressBookUsingDelete: (request: AddressBookDeleteRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/addressbooks`,
        method: "DELETE",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 주소록 엑셀 다운로드
     *
     * @tags [Master] - 주소록 API
     * @name DownloadAddressBookExcelUsingGet
     * @summary 주소록 > 주소록 목록 > 엑셀 다운로드 API
     * @request GET:/api/wewill/admin/addressbooks/excel
     * @secure
     */
    downloadAddressBookExcelUsingGet: (query: DownloadAddressBookExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/addressbooks/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 국가 코드 목록 조회
     *
     * @tags [Master] - 주소록 API
     * @name SearchNationCodesUsingGet
     * @summary 주소록 > 주소록 상세 정보 > 주소록 상세 조회 > 국가 코드 목록 조회 API
     * @request GET:/api/wewill/admin/addressbooks/nations
     * @secure
     */
    searchNationCodesUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoAddressBookNationCodeResponse, void>({
        path: `/api/wewill/admin/addressbooks/nations`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 주소록 상세 조회
     *
     * @tags [Master] - 주소록 API
     * @name SelectAddressBookUsingGet
     * @summary 주소록 > 주소록 상세 정보 ::: 주소록 상세 조회 API
     * @request GET:/api/wewill/admin/addressbooks/{addressBookId}
     * @secure
     */
    selectAddressBookUsingGet: (addressBookId: number, params: RequestParams = {}) =>
      this.request<AddressBookInfoResponse, void>({
        path: `/api/wewill/admin/addressbooks/${addressBookId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 주소록 수정
     *
     * @tags [Master] - 주소록 API
     * @name ModifyAddressBookUsingPut
     * @summary 주소록 > 주소록 상세 정보 ::: 주소록 수정 API
     * @request PUT:/api/wewill/admin/addressbooks/{addressBookId}
     * @secure
     */
    modifyAddressBookUsingPut: (addressBookId: number, request: AddressBookModifyRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/addressbooks/${addressBookId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  audit = {
    /**
     * @description Staff 활동로그
     *
     * @tags [Admin Master] - Staff 활동로그 API
     * @name AuditListUsingGet
     * @summary Staff 활동로그
     * @request GET:/api/wewill/admin/audit
     * @secure
     */
    auditListUsingGet: (query: AuditListUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoSearchResponse, void>({
        path: `/api/wewill/admin/audit`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description Staff 활동로그 > 요청/응답 내역
     *
     * @tags [Admin Master] - Staff 활동로그 API
     * @name AuditInfoUsingGet
     * @summary Staff 활동로그 > 요청/응답 내역
     * @request GET:/api/wewill/admin/audit/{no}
     * @secure
     */
    auditInfoUsingGet: (no?: number, params: RequestParams = {}) =>
      this.request<InfoResponse, void>({
        path: `/api/wewill/admin/audit/${no}`,
        method: "GET",
        secure: true,
        ...params,
      }),
  };
  board = {
    /**
     * @description 게시판설정 목록 조회
     *
     * @tags [Admin Master] - 게시판관리 API (화면없음)
     * @name SearchBoardUsingGet
     * @summary 게시판설정 목록 조회 API
     * @request GET:/api/wewill/admin/board
     * @secure
     */
    searchBoardUsingGet: (query: SearchBoardUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoBoardSearchResponse, void>({
        path: `/api/wewill/admin/board`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 게시판설정 등록
     *
     * @tags [Admin Master] - 게시판관리 API (화면없음)
     * @name InsertBoardUsingPost
     * @summary 게시판설정 등록 API
     * @request POST:/api/wewill/admin/board
     * @secure
     */
    insertBoardUsingPost: (
      query: InsertBoardUsingPostParams,
      request: BoardInsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<BoardInsertResponse, void>({
        path: `/api/wewill/admin/board`,
        method: "POST",
        query: query,
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 게시판설정 상세 조회
     *
     * @tags [Admin Master] - 게시판관리 API (화면없음)
     * @name SelectBoardUsingGet
     * @summary 게시판설정 상세 조회 API
     * @request GET:/api/wewill/admin/board/{id}
     * @secure
     */
    selectBoardUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<BoardInfoResponse, void>({
        path: `/api/wewill/admin/board/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 게시판설정 수정
     *
     * @tags [Admin Master] - 게시판관리 API (화면없음)
     * @name ModifyBoardUsingPut
     * @summary 게시판설정 수정 API
     * @request PUT:/api/wewill/admin/board/{id}
     * @secure
     */
    modifyBoardUsingPut: (
      { id, ...query }: ModifyBoardUsingPutParams,
      request: BoardModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<BoardModifyResponse, void>({
        path: `/api/wewill/admin/board/${id}`,
        method: "PUT",
        query: query,
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 게시판설정 삭제
     *
     * @tags [Admin Master] - 게시판관리 API (화면없음)
     * @name DeleteBoardUsingDelete
     * @summary 게시판설정 삭제 API
     * @request DELETE:/api/wewill/admin/board/{id}
     * @secure
     */
    deleteBoardUsingDelete: (id: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/board/${id}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  categories = {
    /**
     * @description 전체 제품 카테고리 목록 조회
     *
     * @tags [Admin] - 제품,영상 카테고리 조회 API
     * @name SearchRequestPrductCategoriesUsingGet
     * @summary 관리자 > 전체 제품 카테고리 목록 조회 API
     * @request GET:/api/wewill/admin/categories/products
     * @secure
     */
    searchRequestPrductCategoriesUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoSearchPrductOrderCategoryResponse, void>({
        path: `/api/wewill/admin/categories/products`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 ID값 통한 매핑된 제품 카테고리 목록 조회
     *
     * @tags [Admin] - 제품,영상 카테고리 조회 API
     * @name SearchPrductCategoriesByProductIdUsingGet
     * @summary 관리자 > 제품 카테고리 조회 API
     * @request GET:/api/wewill/admin/categories/products/{id}
     * @secure
     */
    searchPrductCategoriesByProductIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ListVoSearchPrductCategoryResponse, void>({
        path: `/api/wewill/admin/categories/products/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 관리자 전체 영상 카테고리 목록 조회
     *
     * @tags [Admin] - 제품,영상 카테고리 조회 API
     * @name SearchRequestVideoCategoriesUsingGet
     * @summary 관리자 > 전체 영상 카테고리 목록 조회 API
     * @request GET:/api/wewill/admin/categories/videos
     * @secure
     */
    searchRequestVideoCategoriesUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoSearchVidoOrderCategoryResponse, void>({
        path: `/api/wewill/admin/categories/videos`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 영상 ID값 통한 매핑된 영상 카테고리 목록 조회
     *
     * @tags [Admin] - 제품,영상 카테고리 조회 API
     * @name SearchVidoCategoriesByProductIdUsingGet
     * @summary 관리자 > 영상 카테고리 조회 API
     * @request GET:/api/wewill/admin/categories/videos/{id}
     * @secure
     */
    searchVidoCategoriesByProductIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ListVoSearchVidoCategoryResponse, void>({
        path: `/api/wewill/admin/categories/videos/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  clientMember = {
    /**
     * @description 관리자가 의뢰자 등록
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name InsertClientMemberUsingPost
     * @summary 회원 관리 > 의뢰자 > 회원 생성
     * @request POST:/api/wewill/admin/client-member
     * @secure
     */
    insertClientMemberUsingPost: (req: AdminClientMemberInsertRequest, params: RequestParams = {}) =>
      this.request<ClientMemberInsertResponse, void>({
        path: `/api/wewill/admin/client-member`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 쿠폰 목록
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name ClientCouponListUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 쿠폰 정보(탭)
     * @request GET:/api/wewill/admin/client-member/coupons
     * @secure
     */
    clientCouponListUsingGet: (query: ClientCouponListUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminClientMemberCouponListResponse, void>({
        path: `/api/wewill/admin/client-member/coupons`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description "엑셀로 내보내기" 버튼
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name CouponDownExcelUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 쿠폰 정보(탭) > 쿠폰 정보 엑셀다운로드
     * @request GET:/api/wewill/admin/client-member/coupons/excel
     * @secure
     */
    couponDownExcelUsingGet: (query: CouponDownExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/client-member/coupons/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰자 회원 엑셀 다운로드
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name DownExcelUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 검색 > 엑셀 다운로드
     * @request GET:/api/wewill/admin/client-member/excel
     * @secure
     */
    downExcelUsingGet: (query: DownExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/client-member/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 환불 내역 목록
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name ClientRefundListUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 결제/환불 정보(탭)::환불 내역
     * @request GET:/api/wewill/admin/client-member/refund
     * @secure
     */
    clientRefundListUsingGet: (query: ClientRefundListUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminCilentMemberRefundSearchResponse, void>({
        path: `/api/wewill/admin/client-member/refund`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 회원 환불 엑셀 다운로드
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name RefundDownExcelUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 결제/환불 정보(탭)::환불 내역 > 엑셀 다운로드
     * @request GET:/api/wewill/admin/client-member/refund/excel
     * @secure
     */
    refundDownExcelUsingGet: (query: RefundDownExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/client-member/refund/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 정보
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name ClientRequestListUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 의뢰 정보(탭)
     * @request GET:/api/wewill/admin/client-member/requests
     * @secure
     */
    clientRequestListUsingGet: (query: ClientRequestListUsingGetParams, params: RequestParams = {}) =>
      this.request<AdminClientMemberReqInfoResponse, void>({
        path: `/api/wewill/admin/client-member/requests`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 내역 목록
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name ClientSettlementListUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 결제/환불 정보(탭)::결제 내역
     * @request GET:/api/wewill/admin/client-member/settlement
     * @secure
     */
    clientSettlementListUsingGet: (query: ClientSettlementListUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminClientMemberSettlementListResponse, void>({
        path: `/api/wewill/admin/client-member/settlement`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 회원 결제 엑셀 다운로드
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name SettlementDownExcelUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 정보 > 결제/환불 정보(탭)::결제 내역 > 엑셀 다운로드
     * @request GET:/api/wewill/admin/client-member/settlement/excel
     * @secure
     */
    settlementDownExcelUsingGet: (query: SettlementDownExcelUsingGetParams, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/client-member/settlement/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰자 상세보기 (기본정보 tab)
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name InfoClientMemberUsingGet
     * @summary 회원 관리 > 의뢰자 > 의뢰자 회원 정보
     * @request GET:/api/wewill/admin/client-member/{id}
     * @secure
     */
    infoClientMemberUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<AdminClientMemberInfoResponse, void>({
        path: `/api/wewill/admin/client-member/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 기본 정보 수정(tab1)
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name ModifyClientMemberUsingPut
     * @summary 회원 관리 > 의뢰자 > 의뢰자 회원 정보 > 기본 정보 수정
     * @request PUT:/api/wewill/admin/client-member/{id}
     * @secure
     */
    modifyClientMemberUsingPut: (id: number, req: AdminClientMemberModifyRequest, params: RequestParams = {}) =>
      this.request<ClientMemberModifyResponse, void>({
        path: `/api/wewill/admin/client-member/${id}`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  clientMembers = {
    /**
     * @description 의뢰자 검색 조회 api
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name SearchMemberUsingGet
     * @summary 회원 관리 > 의뢰자 > 회원 검색
     * @request GET:/api/wewill/admin/client-members
     * @secure
     */
    searchMemberUsingGet: (query: SearchMemberUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminClientMemberSearchResponse, void>({
        path: `/api/wewill/admin/client-members`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  client = {
    /**
     * @description 최종평가 워드클라우드 > 모든답변 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoRequestBasicAllAnswerUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 최종평가 워드클라우드 > 모든답변 조회 API
     * @request POST:/api/wewill/admin/client/report/basic/answer
     * @secure
     */
    infoRequestBasicAllAnswerUsingPost: (req: AdminReportAllAnswerRequest, params: RequestParams = {}) =>
      this.request<PagingInfoAdminReportAnswerResponse, void>({
        path: `/api/wewill/admin/client/report/basic/answer`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 최종평가 하이라이트 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoRequestBasicChartUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 최종평가 하이라이트 조회 API
     * @request POST:/api/wewill/admin/client/report/basic/highlight
     * @secure
     */
    infoRequestBasicChartUsingPost: (req: AdminClientReportHighLightChartFilterRequest, params: RequestParams = {}) =>
      this.request<StackBarChart, void>({
        path: `/api/wewill/admin/client/report/basic/highlight`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 별점 조회 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoRequestBasicStarScoreUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 최종평가 별점 조회 API
     * @request POST:/api/wewill/admin/client/report/basic/starscore
     * @secure
     */
    infoRequestBasicStarScoreUsingPost: (req: AdminReportStarScoreRequest, params: RequestParams = {}) =>
      this.request<PagingInfoAdminReportAnswerResponse, void>({
        path: `/api/wewill/admin/client/report/basic/starscore`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 최종평가 워드클라우드 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoRequestBasicWordCloudUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 최종평가 워드클라우드 조회 API
     * @request POST:/api/wewill/admin/client/report/basic/wordcloud
     * @secure
     */
    infoRequestBasicWordCloudUsingPost: (req: AdminWordCloudRequest, params: RequestParams = {}) =>
      this.request<ListVoWordCloud, void>({
        path: `/api/wewill/admin/client/report/basic/wordcloud`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 프리미엄용 질문 전체답변 조회 API 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoRequestBasicAnswerUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 프리미엄용 질문 조회 > 전체답변 조회 API
     * @request POST:/api/wewill/admin/client/report/premium/m/answer/search
     * @secure
     */
    infoRequestBasicAnswerUsingPost: (req: AdminReportAnswerRequest, params: RequestParams = {}) =>
      this.request<AdminWordCloudChart, void>({
        path: `/api/wewill/admin/client/report/premium/m/answer/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 프리미엄용 질문(주관식만) 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoWordCloudFilterUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 프리미엄용 질문 조회(주관식용,워드클라우드) API
     * @request POST:/api/wewill/admin/client/report/premium/m/search
     * @secure
     */
    infoWordCloudFilterUsingPost: (req: AdminClientChartSearchRequest, params: RequestParams = {}) =>
      this.request<AdminWordCloudChart, void>({
        path: `/api/wewill/admin/client/report/premium/m/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 프리미엄용 질문 조회 API
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoChartFilterUsingPost
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 > 프리미엄용 질문 조회(주관식 제외) API
     * @request POST:/api/wewill/admin/client/report/premium/s/search
     * @secure
     */
    infoChartFilterUsingPost: (req: AdminClientChartSearchRequest, params: RequestParams = {}) =>
      this.request<StackBarChart, void>({
        path: `/api/wewill/admin/client/report/premium/s/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰서 보고서 상세 조회
     *
     * @tags [Admin 검수] - 의뢰자 보고서 API
     * @name InfoRequestBasicVidoInfoUsingGet
     * @summary 검수/CS > 검수 > 평가검수 > 부서장 검수관리 > 의뢰 완료 건 검수 > 보고서 상세보기 API
     * @request GET:/api/wewill/admin/client/report/{requestId}
     * @secure
     */
    infoRequestBasicVidoInfoUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<AdminClientReportInfoResponse, void>({
        path: `/api/wewill/admin/client/report/${requestId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  clientintro = {
    /**
     * @description 서비스 소개서 요청 조회
     *
     * @tags [Admin CS] 서비스 소개서 요청 API
     * @name SearchClientIntroUsingGet
     * @summary 서비스 소개서 요청 > 목록 API
     * @request GET:/api/wewill/admin/clientintro
     * @secure
     */
    searchClientIntroUsingGet: (query: SearchClientIntroUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoClientIntroSearchResponse, void>({
        path: `/api/wewill/admin/clientintro`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  coupons = {
    /**
     * @description 검색결과 영역의 "검색" 버튼. 페이징 조회
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name FindAllCouponUsingGet
     * @summary 쿠폰관리 > 쿠폰발행목록
     * @request GET:/api/wewill/admin/coupons
     * @secure
     */
    findAllCouponUsingGet: (query: FindAllCouponUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminCouponResponse, void>({
        path: `/api/wewill/admin/coupons`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description "쿠폰발행" 버튼
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name RegisterCouponUsingPost
     * @summary 쿠폰관리 > 쿠폰발행 > 쿠폰발행
     * @request POST:/api/wewill/admin/coupons
     * @secure
     */
    registerCouponUsingPost: (request: AdminCouponInsertRequest, params: RequestParams = {}) =>
      this.request<AdminCouponInsertResponse, void>({
        path: `/api/wewill/admin/coupons`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 회원 정보 검색(의뢰자)
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name SearchMemberUsingGet1
     * @summary 쿠폰 관리 > 쿠폰 목록 > 쿠폰 상세 정보 > 쿠폰 등록 (탭)
     * @request GET:/api/wewill/admin/coupons/client-members
     * @secure
     */
    searchMemberUsingGet1: (query: SearchMemberUsingGet1Params, params: RequestParams = {}) =>
      this.request<PagingInfoAdminClientMemberResponse, void>({
        path: `/api/wewill/admin/coupons/client-members`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 쿠폰 등록 내역의 목록 조회 페이징
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name FindAllClientCouponUsingGet
     * @summary 쿠폰관리 > 쿠폰목록 > 쿠폰 상세정보 > 쿠폰등록 탭 > 쿠폰 등록 내역
     * @request GET:/api/wewill/admin/coupons/clientcoupons
     * @secure
     */
    findAllClientCouponUsingGet: (query: FindAllClientCouponUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminClientCouponResponse, void>({
        path: `/api/wewill/admin/coupons/clientcoupons`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 회원 정보 검색 결과 영역의 "선택등록" 버튼. 선택 사용자(멀티건 지원)에 대한 쿠폰 등록 기능
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name RegisterClientCouponUsingPost
     * @summary 쿠폰관리 > 쿠폰목록 > 쿠폰 상세정보 > 쿠폰등록 탭 > 쿠폰등록
     * @request POST:/api/wewill/admin/coupons/clientcoupons
     * @secure
     */
    registerClientCouponUsingPost: (request: AdminClientCouponInsertRequest, params: RequestParams = {}) =>
      this.request<AdminClientCouponInsertResponse, void>({
        path: `/api/wewill/admin/coupons/clientcoupons`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 쿠폰 등록 내역 영역의 "선택삭제" 버튼. 선택 사용자(멀티건 지원)에 대한 쿠폰 삭제 기능
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name RemoveClientCouponUsingDelete
     * @summary 쿠폰관리 > 쿠폰목록 > 쿠폰 상세정보 > 쿠폰등록 탭 > 쿠폰삭제
     * @request DELETE:/api/wewill/admin/coupons/clientcoupons
     * @secure
     */
    removeClientCouponUsingDelete: (query: RemoveClientCouponUsingDeleteParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/coupons/clientcoupons`,
        method: "DELETE",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description "엑셀로 내보내기" 버튼
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name DownClientCouponExcelDownUsingGet1
     * @summary 쿠폰관리 > 쿠폰목록 > 쿠폰 상세정보 > 쿠폰등록 탭 > 쿠폰 등록 내역 엑셀다운로드
     * @request GET:/api/wewill/admin/coupons/clientcoupons/excel
     * @secure
     */
    downClientCouponExcelDownUsingGet1: (query: DownClientCouponExcelDownUsingGet1Params, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/coupons/clientcoupons/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 검색결과의 "상세보기" 버튼
     *
     * @tags [Admin Master] - 쿠폰관리 API
     * @name FindCouponUsingGet
     * @summary 쿠폰관리 > 쿠폰발행목록 > 쿠폰상세보기
     * @request GET:/api/wewill/admin/coupons/{no}
     * @secure
     */
    findCouponUsingGet: (no: number, params: RequestParams = {}) =>
      this.request<AdminCouponInfoResponse, void>({
        path: `/api/wewill/admin/coupons/${no}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  customers = {
    /**
     * @description 1:1문의 상세 영역의 CS담당자명 선택 용
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name ChargerListUsingGet
     * @summary 1:1 문의> 영업 > 상세 :: CS 담당자 List API
     * @request GET:/api/wewill/admin/customers/chargerList
     * @secure
     */
    chargerListUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoChargersModifyResponse, void>({
        path: `/api/wewill/admin/customers/chargerList`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 전체 목록 조회
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name SearchCreatorsCustomerUsingGet
     * @summary 1:1 문의> 의뢰자> 목록
     * @request GET:/api/wewill/admin/customers/creators
     * @secure
     */
    searchCreatorsCustomerUsingGet: (query: SearchCreatorsCustomerUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminCustomerSearchResponse, void>({
        path: `/api/wewill/admin/customers/creators`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 전체 목록 조회
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name SearchMarkersCustomerUsingGet
     * @summary 1:1 문의> 평가자> 목록
     * @request GET:/api/wewill/admin/customers/markers
     * @secure
     */
    searchMarkersCustomerUsingGet: (query: SearchMarkersCustomerUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminCustomerSearchResponse, void>({
        path: `/api/wewill/admin/customers/markers`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 영업 전체 목록 조회
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name SearchAdminMonitorsUsingGet
     * @summary 1:1 문의> 영업> 목록
     * @request GET:/api/wewill/admin/customers/monitors
     * @secure
     */
    searchAdminMonitorsUsingGet: (query: SearchAdminMonitorsUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminMonitorsSearchResponse, void>({
        path: `/api/wewill/admin/customers/monitors`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 문의답변 영역 "저장" 버튼. 답변 등록 (의뢰자/평가자 공통)
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name InsertAnswerUsingPost
     * @summary 1:1 문의> 의뢰자/평가자 > 상세 :: 답변등록 (공통)
     * @request POST:/api/wewill/admin/customers/{id}/answer
     * @secure
     */
    insertAnswerUsingPost: (id: number, request: AdminCustomerInsertAnswerRequest, params: RequestParams = {}) =>
      this.request<AdminCustomerInsertAnswerResponse, void>({
        path: `/api/wewill/admin/customers/${id}/answer`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 상세 조회
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name SelectCreatorsUsingGet
     * @summary 1:1 문의> 의뢰자> 상세 :: 조회
     * @request GET:/api/wewill/admin/customers/{id}/creators
     * @secure
     */
    selectCreatorsUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<AdminCreatorsInfoResponse, void>({
        path: `/api/wewill/admin/customers/${id}/creators`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세 조회
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name SelectMakersUsingGet
     * @summary 1:1 문의> 평가자> 상세 :: 조회
     * @request GET:/api/wewill/admin/customers/{id}/markers
     * @secure
     */
    selectMakersUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<AdminMarkersInfoResponse, void>({
        path: `/api/wewill/admin/customers/${id}/markers`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 문의답변 영역 "저장" 버튼. 답변 등록 (영업)
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name InsertMonitorAnswerUsingPost
     * @summary 1:1 문의> 영업 > 상세 :: 답변등록 (공통)
     * @request POST:/api/wewill/admin/customers/{id}/monitorAnswer
     * @secure
     */
    insertMonitorAnswerUsingPost: (id: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/customers/${id}/monitorAnswer`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 영업 상세 조회
     *
     * @tags [Admin 검수] - CS 1:1문의 API
     * @name SelectMonitorsUsingGet
     * @summary 1:1 문의> 영업> 상세 :: 조회
     * @request GET:/api/wewill/admin/customers/{id}/monitors
     * @secure
     */
    selectMonitorsUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<AdminMonitorsInfoResponse, void>({
        path: `/api/wewill/admin/customers/${id}/monitors`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  dailyQuestions = {
    /**
     * @description 데일리 설문 전체 List
     *
     * @tags [Admin Master] - Daily 설문 API
     * @name SearchDailyUsingGet
     * @summary 데일리 설문 관리::전체 조회
     * @request GET:/api/wewill/admin/dailyQuestions
     * @secure
     */
    searchDailyUsingGet: (query: SearchDailyUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoDailyQuestionSearchResponse, void>({
        path: `/api/wewill/admin/dailyQuestions`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 데일리 설문 상세
     *
     * @tags [Admin Master] - Daily 설문 API
     * @name SelectDailyUsingGet
     * @summary 데일리 설문 관리::상세 조회
     * @request GET:/api/wewill/admin/dailyQuestions/{id}
     * @secure
     */
    selectDailyUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<DailyQuestionInfoResponse, void>({
        path: `/api/wewill/admin/dailyQuestions/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  evaluatorMember = {
    /**
     * @description 평가자 회원 목록
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name SearchEvaluatorUsingGet
     * @summary 평가자 회원 목록 API
     * @request GET:/api/wewill/admin/evaluator-member
     * @secure
     */
    searchEvaluatorUsingGet: (query: SearchEvaluatorUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminEvaluatorMemberListResponse, void>({
        path: `/api/wewill/admin/evaluator-member`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 기본정보 등록
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InsertAdminEvaluatorUsingPost
     * @summary 평가자 기본정보 등록 API
     * @request POST:/api/wewill/admin/evaluator-member
     * @secure
     */
    insertAdminEvaluatorUsingPost: (req: AdminEvaluatorBasicInsertRequest, params: RequestParams = {}) =>
      this.request<AdminEvaluatorBasicInsertResponse, void>({
        path: `/api/wewill/admin/evaluator-member`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 평가 정보 > 데일리 설문 내역 리스트 엑셀 다운로드
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name DownExcelMemberDailyAnswersUsingGet
     * @summary 평가자 상세보기 > 평가 정보 > 데일리 설문 내역 > 엑셀 다운로드
     * @request GET:/api/wewill/admin/evaluator-member/down/excel/evaluation/daily
     * @secure
     */
    downExcelMemberDailyAnswersUsingGet: (
      query: DownExcelMemberDailyAnswersUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<PagingInfoAdminEvaluatorEvaluationResponse, void>({
        path: `/api/wewill/admin/evaluator-member/down/excel/evaluation/daily`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 평가 정보 > 평가 내역 리스트 엑셀 다운로드
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name DownExcelMemberEvaluationsUsingGet
     * @summary 평가자 상세보기 > 평가 정보 > 평가 내역 > 엑셀 다운로드
     * @request GET:/api/wewill/admin/evaluator-member/down/excel/evaluation/evl
     * @secure
     */
    downExcelMemberEvaluationsUsingGet: (query: DownExcelMemberEvaluationsUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminEvaluatorEvaluationResponse, void>({
        path: `/api/wewill/admin/evaluator-member/down/excel/evaluation/evl`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 포인트 정보 > 포인트 내역 리스트 엑셀 다운로드
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name DownClientCouponExcelDownUsingGet
     * @summary 회원 관리 >평가자 >평가자 회원 정보 ::포인트 정보 내역 엑셀다운로드
     * @request GET:/api/wewill/admin/evaluator-member/down/excel/point
     * @secure
     */
    downClientCouponExcelDownUsingGet: (query: DownClientCouponExcelDownUsingGetParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/evaluator-member/down/excel/point`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 평가 정보 > 데일리 설문 내역 리스트
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InfoMemberDailyAnswersUsingGet
     * @summary 평가자 상세보기 > 평가 정보 > 데일리 설문 내역
     * @request GET:/api/wewill/admin/evaluator-member/evaluation/daily
     * @secure
     */
    infoMemberDailyAnswersUsingGet: (query: InfoMemberDailyAnswersUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminEvaluatorDailyAnswersResponse, void>({
        path: `/api/wewill/admin/evaluator-member/evaluation/daily`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 평가 정보 > 평가 내역 리스트
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InfoMemberEvaluationsUsingGet
     * @summary 평가자 상세보기 > 평가 정보 > 평가 내역
     * @request GET:/api/wewill/admin/evaluator-member/evaluation/evl
     * @secure
     */
    infoMemberEvaluationsUsingGet: (query: InfoMemberEvaluationsUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminEvaluatorEvaluationResponse, void>({
        path: `/api/wewill/admin/evaluator-member/evaluation/evl`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 평가정보 tab > 평가정보 카운트
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InfoEvaluatorEvlCntUsingGet
     * @summary 회원 관리 >평가자 >평가자 회원 정보 > 평가정보 > 평가정보 카운트 API
     * @request GET:/api/wewill/admin/evaluator-member/evaluation/evl/cnt
     * @secure
     */
    infoEvaluatorEvlCntUsingGet: (query: InfoEvaluatorEvlCntUsingGetParams, params: RequestParams = {}) =>
      this.request<AdminEvaluatorEvlCntResponse, void>({
        path: `/api/wewill/admin/evaluator-member/evaluation/evl/cnt`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 회원 목록 엑셀 다운로드
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name DownEvaluatorMemberExcelDownUsingGet
     * @summary 평가자 회원 목록 엑셀 다운로드 API
     * @request GET:/api/wewill/admin/evaluator-member/excel
     * @secure
     */
    downEvaluatorMemberExcelDownUsingGet: (
      query: DownEvaluatorMemberExcelDownUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/evaluator-member/excel`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 상세보기 > 포인트 정보 > 포인트 내역 리스트
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InfoMemberPointsUsingGet
     * @summary 회원 관리 >평가자 >평가자 회원 정보 ::포인트 정보 API
     * @request GET:/api/wewill/admin/evaluator-member/point
     * @secure
     */
    infoMemberPointsUsingGet: (query: InfoMemberPointsUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminEvaluatorPointResponse, void>({
        path: `/api/wewill/admin/evaluator-member/point`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 회원 상세보기(기본정보)
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InfoEvaluatorUsingGet
     * @summary 평가자 회원 상세보기(기본정보) API
     * @request GET:/api/wewill/admin/evaluator-member/{memberId}
     * @secure
     */
    infoEvaluatorUsingGet: (memberId: number, params: RequestParams = {}) =>
      this.request<AdminEvaluatorMemberInfoResponse, void>({
        path: `/api/wewill/admin/evaluator-member/${memberId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 계약서 등록
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InsertEvaluatorContractUsingPost
     * @summary 평가자 계약서 등록 API
     * @request POST:/api/wewill/admin/evaluator-member/{memberId}
     * @secure
     */
    insertEvaluatorContractUsingPost: (
      memberId: number,
      req: AdminEvaluatorContractInsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/evaluator-member/${memberId}`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 회원 수정(기본정보)
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name ModifyEvaluatorUsingPut
     * @summary 평가자 회원 수정(기본정보) API
     * @request PUT:/api/wewill/admin/evaluator-member/{memberId}
     * @secure
     */
    modifyEvaluatorUsingPut: (memberId: number, req: AdminEvaluatorMemberModifyRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/evaluator-member/${memberId}`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 회원 상세보기(계약서)
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name InfoEvaluatorContractUsingGet
     * @summary 평가자 회원 상세보기(계약서) API
     * @request GET:/api/wewill/admin/evaluator-member/{memberId}/contract
     * @secure
     */
    infoEvaluatorContractUsingGet: (memberId: number, params: RequestParams = {}) =>
      this.request<ListVoAdminEvaluatorContractInfoResponse, void>({
        path: `/api/wewill/admin/evaluator-member/${memberId}/contract`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 계약서 삭제
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name DeleteEvaluatorCOntractUsingDelete
     * @summary 평가자 계약서 삭제 API
     * @request DELETE:/api/wewill/admin/evaluator-member/{memberId}/contract
     * @secure
     */
    deleteEvaluatorCOntractUsingDelete: (
      memberId: number,
      req: AdminEvaluatorContractDeleteRequest,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/evaluator-member/${memberId}/contract`,
        method: "DELETE",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  inspect = {
    /**
     * @description 의뢰 견적 승인관리 목록 조회
     *
     * @tags [Admin 검수] - 의뢰견적 승인관리 API
     * @name SearchApprovalUsingGet
     * @summary 부서장 검수관리 > 의뢰견적 승인관리
     * @request GET:/api/wewill/admin/inspect/approval
     * @secure
     */
    searchApprovalUsingGet: (query: SearchApprovalUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoApprovalSearchResponse, void>({
        path: `/api/wewill/admin/inspect/approval`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 견적 승인관리 상세 조회
     *
     * @tags [Admin 검수] - 의뢰견적 승인관리 API
     * @name InfoApprovalUsingGet
     * @summary 부서장 검수관리 > 의뢰견적 승인관리 > 상세
     * @request GET:/api/wewill/admin/inspect/approval/{id}
     * @secure
     */
    infoApprovalUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<ApprovalInfoResponse, void>({
        path: `/api/wewill/admin/inspect/approval/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 견적 승인관리 견적반려
     *
     * @tags [Admin 검수] - 의뢰견적 승인관리 API
     * @name PutRejectUsingPut
     * @summary 부서장 검수관리 > 의뢰견적 승인관리 > 상세 > 견적반려
     * @request PUT:/api/wewill/admin/inspect/approval/{id}/reject
     * @secure
     */
    putRejectUsingPut: (request: ApprovalSaveRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/approval/${id}/reject`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적 승인관리 견적승인
     *
     * @tags [Admin 검수] - 의뢰견적 승인관리 API
     * @name PutApprovalUsingPut
     * @summary 부서장 검수관리 > 의뢰견적 승인관리 > 상세 > 견적승인
     * @request PUT:/api/wewill/admin/inspect/approval/{id}/save
     * @secure
     */
    putApprovalUsingPut: (request: ApprovalSaveRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/approval/${id}/save`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 담당자 관리 목록 조회
     *
     * @tags [Admin 검수] - 담당자 관리 API
     * @name SearchChargerUsingGet
     * @summary 부서장 검수관리 > 담당자 관리
     * @request GET:/api/wewill/admin/inspect/charger
     * @secure
     */
    searchChargerUsingGet: (query: SearchChargerUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoChargerSearchResponse, void>({
        path: `/api/wewill/admin/inspect/charger`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 검수관리자 목록 조회
     *
     * @tags [Admin 검수] - 담당자 관리 API
     * @name SearchInspectManagerUsingGet
     * @summary 부서장 검수관리 > 담당자 관리 > 상세 > 담당자배정 > 검수관리자 목록
     * @request GET:/api/wewill/admin/inspect/charger/managers
     * @secure
     */
    searchInspectManagerUsingGet: (query: SearchInspectManagerUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoChargerInspectManagerSearchResponse, void>({
        path: `/api/wewill/admin/inspect/charger/managers`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 담당자 관리 상세 조회
     *
     * @tags [Admin 검수] - 담당자 관리 API
     * @name InfoChargerUsingGet
     * @summary 부서장 검수관리 > 담당자 관리 > 상세
     * @request GET:/api/wewill/admin/inspect/charger/{id}
     * @secure
     */
    infoChargerUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<ChargerInfoResponse, void>({
        path: `/api/wewill/admin/inspect/charger/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰심사담당자 배정
     *
     * @tags [Admin 검수] - 담당자 관리 API
     * @name PutAcceptanceChargerUsingPut
     * @summary 부서장 검수관리 > 담당자 관리 > 상세 > 담당자배정 > 의뢰심사담당자 배정
     * @request PUT:/api/wewill/admin/inspect/charger/{id}/ra/{raId}
     * @secure
     */
    putAcceptanceChargerUsingPut: (
      request: ChargerMappingRequest,
      id?: number,
      raId?: number,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/charger/${id}/ra/${raId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 견적산출담당자 배정
     *
     * @tags [Admin 검수] - 담당자 관리 API
     * @name PutEstmtChargerUsingPut
     * @summary 부서장 검수관리 > 담당자 관리 > 상세 > 담당자배정 > 견적산출담당자 배정
     * @request PUT:/api/wewill/admin/inspect/charger/{id}/re/{reId}
     * @secure
     */
    putEstmtChargerUsingPut: (request: ChargerMappingRequest, id?: number, reId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/charger/${id}/re/${reId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 평가검수담당자 배정
     *
     * @tags [Admin 검수] - 담당자 관리 API
     * @name PutEvaluationChargerUsingPut
     * @summary 부서장 검수관리 > 담당자 관리 > 상세 > 담당자배정 > 평가검수담당자 배정
     * @request PUT:/api/wewill/admin/inspect/charger/{id}/rv/{rvId}
     * @secure
     */
    putEvaluationChargerUsingPut: (
      request: ChargerMappingRequest,
      id?: number,
      rvId?: number,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/charger/${id}/rv/${rvId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰완료 건 검수 목록 조회
     *
     * @tags [Admin 검수] - 의뢰 완료 건 검수 API
     * @name SearchReqestCompleteUsingGet
     * @summary 부서장 검수관리 > 의뢰완료 건 검수
     * @request GET:/api/wewill/admin/inspect/complete
     * @secure
     */
    searchReqestCompleteUsingGet: (query: SearchReqestCompleteUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoCompleteSearchResponse, void>({
        path: `/api/wewill/admin/inspect/complete`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰완료 건 검수 다중 완료처리
     *
     * @tags [Admin 검수] - 의뢰 완료 건 검수 API
     * @name PutReqestCompletesUsingPut
     * @summary 부서장 검수관리 > 의뢰완료 건 검수 > 다중 완료처리
     * @request PUT:/api/wewill/admin/inspect/complete
     * @secure
     */
    putReqestCompletesUsingPut: (request: CompleteRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/complete`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 별 신고 검수 목록 조회
     *
     * @tags [Admin 검수] - 의뢰 별 신고 검수 API
     * @name SearchDeclarationUsingGet
     * @summary 신고 건 검수 > 의뢰 별 신고 검수
     * @request GET:/api/wewill/admin/inspect/declaration
     * @secure
     */
    searchDeclarationUsingGet: (query: SearchDeclarationUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoDeclarationSearchResponse, void>({
        path: `/api/wewill/admin/inspect/declaration`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 별 신고 검수 상세 조회
     *
     * @tags [Admin 검수] - 의뢰 별 신고 검수 API
     * @name InfoDeclarationUsingGet
     * @summary 신고 건 검수 > 의뢰 별 신고 검수 > 상세
     * @request GET:/api/wewill/admin/inspect/declaration/{id}
     * @secure
     */
    infoDeclarationUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<DeclarationInfoResponse, void>({
        path: `/api/wewill/admin/inspect/declaration/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 별 신고 검수 상세 내부 신고리스트 엑셀다운로드
     *
     * @tags [Admin 검수] - 의뢰 별 신고 검수 API
     * @name SearchDeclarationsUsingGet
     * @summary 의뢰 별 평가결과 검수 > 상세 > 신고리스트 엑셀다운로드
     * @request GET:/api/wewill/admin/inspect/declaration/{id}/excel
     * @secure
     */
    searchDeclarationsUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/inspect/declaration/${id}/excel`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 별 신고 검수 상세 내부 신고리스트
     *
     * @tags [Admin 검수] - 의뢰 별 신고 검수 API
     * @name SearchDeclarationsUsingGet1
     * @summary 의뢰 별 평가결과 검수 > 상세 > 신고리스트
     * @request GET:/api/wewill/admin/inspect/declaration/{id}/list
     * @secure
     */
    searchDeclarationsUsingGet1: ({ id, ...query }: SearchDeclarationsUsingGet1Params, params: RequestParams = {}) =>
      this.request<PagingInfoSearchDeclarationsResponse, void>({
        path: `/api/wewill/admin/inspect/declaration/${id}/list`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 중지
     *
     * @tags [Admin 검수] - 의뢰 별 신고 검수 API
     * @name PutStopReqestUsingPut1
     * @summary 신고 건 검수 > 의뢰 별 신고 검수 > 상세 > 의뢰 중지
     * @request PUT:/api/wewill/admin/inspect/declaration/{id}/stop
     * @secure
     */
    putStopReqestUsingPut1: (id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/declaration/${id}/stop`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 목록 조회
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name SearchEstmtUsingGet
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사
     * @request GET:/api/wewill/admin/inspect/estmt
     * @secure
     */
    searchEstmtUsingGet: (query: SearchEstmtUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoEstmtSearchResponse, void>({
        path: `/api/wewill/admin/inspect/estmt`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 의뢰심사담당자 내가 담당하기
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name RaChargerManageSaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 의뢰심사담당자 내가 담당하기
     * @request PUT:/api/wewill/admin/inspect/estmt/ra/{id}/{raId}/manage
     * @secure
     */
    raChargerManageSaveUsingPut: (id?: number, raId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/ra/${id}/${raId}/manage`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 의뢰심사담당자 담당해제
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name RaChargerRemoveSaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 의뢰심사담당자 담당해제
     * @request PUT:/api/wewill/admin/inspect/estmt/ra/{id}/{raId}/remove
     * @secure
     */
    raChargerRemoveSaveUsingPut: (id?: number, raId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/ra/${id}/${raId}/remove`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 의뢰심사담당자 필수확인담당자 확인여부
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name RaChargerNecessarySaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 의뢰심사담당자 필수확인담당자 확인여부
     * @request PUT:/api/wewill/admin/inspect/estmt/ra/{raId}/necessary/{checkYn}
     * @secure
     */
    raChargerNecessarySaveUsingPut: (checkYn?: string, raId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/ra/${raId}/necessary/${checkYn}`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 견적산출담당자 내가 담당하기
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name ReChargerManageSaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 견적산출담당자 내가 담당하기
     * @request PUT:/api/wewill/admin/inspect/estmt/re/{id}/{reId}/manage
     * @secure
     */
    reChargerManageSaveUsingPut: (id?: number, reId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/re/${id}/${reId}/manage`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 견적산출담당자 담당해제
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name ReChargerRemoveSaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 견적산출담당자 담당해제
     * @request PUT:/api/wewill/admin/inspect/estmt/re/{id}/{reId}/remove
     * @secure
     */
    reChargerRemoveSaveUsingPut: (id?: number, reId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/re/${id}/${reId}/remove`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 견적승인요청
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name ReApprovalSaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 견적승인요청
     * @request PUT:/api/wewill/admin/inspect/estmt/re/{reId}/approval
     * @secure
     */
    reApprovalSaveUsingPut: (reId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/re/${reId}/approval`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 견적산출담당자 필수확인담당자 확인여부
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name ReChargerNecessarySaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 견적산출담당자 필수확인담당자 확인여부
     * @request PUT:/api/wewill/admin/inspect/estmt/re/{reId}/necessary/{checkYn}
     * @secure
     */
    reChargerNecessarySaveUsingPut: (checkYn?: string, reId?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/re/${reId}/necessary/${checkYn}`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 상세 조회
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name InfoEstmtUsingGet
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세
     * @request GET:/api/wewill/admin/inspect/estmt/{id}
     * @secure
     */
    infoEstmtUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<EstmtInfoResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 의뢰취소
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name PutCancelUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 의뢰취소
     * @request PUT:/api/wewill/admin/inspect/estmt/{id}/cancel
     * @secure
     */
    putCancelUsingPut: (request: ReqestCancelRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/${id}/cancel`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 심사완료
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name PutConfirmUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 심사완료
     * @request PUT:/api/wewill/admin/inspect/estmt/{id}/confirm
     * @secure
     */
    putConfirmUsingPut: (id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/${id}/confirm`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 심사반려
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name PutRejectUsingPut1
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 심사반려
     * @request PUT:/api/wewill/admin/inspect/estmt/{id}/reject
     * @secure
     */
    putRejectUsingPut1: (request: EstmtRejectRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/${id}/reject`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 견적산출/심사 저장
     *
     * @tags [Admin 검수] - 의뢰 견적 산출/의뢰 심사 API
     * @name PutSaveUsingPut
     * @summary 의뢰검수 > 의뢰 견적 산출/의뢰 심사 > 상세 > 저장
     * @request PUT:/api/wewill/admin/inspect/estmt/{id}/save
     * @secure
     */
    putSaveUsingPut: (request: EstmtSaveRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/estmt/${id}/save`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 목록 조회
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name SearchEvaluationUsingGet
     * @summary 평가검수 > 의뢰 별 평가결과 검수
     * @request GET:/api/wewill/admin/inspect/evaluation
     * @secure
     */
    searchEvaluationUsingGet: (query: SearchEvaluationUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoEvaluationSearchResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 정상/불성실 변경처리 반영하기
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name DishonestSaveUsingPut
     * @summary 평가검수 > 의뢰 별 평가결과 검수 > 상세 > 정상/불성실 변경처리 반영하기
     * @request PUT:/api/wewill/admin/inspect/evaluation/dishonestSave
     * @secure
     */
    dishonestSaveUsingPut: (request: EvaluationDishonestSaveRequest[], params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation/dishonestSave`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 임시저장
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name DishonestTmpSaveUsingPut
     * @summary 평가검수 > 의뢰 별 평가결과 검수 > 상세 > 임시저장
     * @request PUT:/api/wewill/admin/inspect/evaluation/dishonestTmpSave
     * @secure
     */
    dishonestTmpSaveUsingPut: (request: EvaluationDishonestTmpSaveRequest[], params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation/dishonestTmpSave`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 내가 담당하기
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name RvChargerManageSaveUsingPut
     * @summary 평가검수 > 의뢰 별 평가결과 검수 > 상세 > 내가 담당하기
     * @request PUT:/api/wewill/admin/inspect/evaluation/rv/manage
     * @secure
     */
    rvChargerManageSaveUsingPut: (query: RvChargerManageSaveUsingPutParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation/rv/manage`,
        method: "PUT",
        query: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 필수확인담당자 확인여부
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name RvChargerNecessarySaveUsingPut
     * @summary 평가검수 > 의뢰 별 평가결과 검수 > 상세 > 필수확인담당자 확인여부
     * @request PUT:/api/wewill/admin/inspect/evaluation/rv/necessary
     * @secure
     */
    rvChargerNecessarySaveUsingPut: (query: RvChargerNecessarySaveUsingPutParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation/rv/necessary`,
        method: "PUT",
        query: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 담당해제
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name RvChargerRemoveSaveUsingPut
     * @summary 평가검수 > 의뢰 별 평가결과 검수 > 상세 > 담당해제
     * @request PUT:/api/wewill/admin/inspect/evaluation/rv/remove
     * @secure
     */
    rvChargerRemoveSaveUsingPut: (query: RvChargerRemoveSaveUsingPutParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation/rv/remove`,
        method: "PUT",
        query: query,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 상세 조회
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name InfoEvaluationUsingGet
     * @summary 평가검수 > 의뢰 별 평가결과 검수 > 상세
     * @request GET:/api/wewill/admin/inspect/evaluation/{id}
     * @secure
     */
    infoEvaluationUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<EvaluationInfoResponse, void>({
        path: `/api/wewill/admin/inspect/evaluation/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 상세 내부에 있는 평가결과 목록 엑셀다운로드
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name InfoResultExcelEvaluationUsingGet
     * @summary 의뢰 별 평가결과 검수 > 상세 > 평가자 평가결과 엑셀다운로드
     * @request GET:/api/wewill/admin/inspect/evaluation/{id}/excel
     * @secure
     */
    infoResultExcelEvaluationUsingGet: (
      { id, ...query }: InfoResultExcelEvaluationUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<void, void>({
        path: `/api/wewill/admin/inspect/evaluation/${id}/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰별 평가 결과 상세 내부에 있는 평가결과 목록 조회
     *
     * @tags [Admin 검수] - 의뢰 별 평가결과 검수 API
     * @name InfoResultEvaluationUsingGet
     * @summary 의뢰 별 평가결과 검수 > 상세 > 평가자 평가결과 정보
     * @request GET:/api/wewill/admin/inspect/evaluation/{id}/result
     * @secure
     */
    infoResultEvaluationUsingGet: ({ id, ...query }: InfoResultEvaluationUsingGetParams, params: RequestParams = {}) =>
      this.request<EvaluationInfoResultList, void>({
        path: `/api/wewill/admin/inspect/evaluation/${id}/result`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 목록 조회
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name SearchReqestUsingGet
     * @summary 의뢰검수 > 의뢰관리
     * @request GET:/api/wewill/admin/inspect/reqest
     * @secure
     */
    searchReqestUsingGet: (query: SearchReqestUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoReqestSearchResponse, void>({
        path: `/api/wewill/admin/inspect/reqest`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 다중 삭제
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name DeleteReqestsUsingDelete
     * @summary 의뢰검수 > 의뢰관리 > 의뢰다중삭제
     * @request DELETE:/api/wewill/admin/inspect/reqest
     * @secure
     */
    deleteReqestsUsingDelete: (request: ReqestDeleteRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/reqest`,
        method: "DELETE",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 상세 조회
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name InfoReqestUsingGet
     * @summary 의뢰검수 > 의뢰관리 > 상세보기
     * @request GET:/api/wewill/admin/inspect/reqest/{id}
     * @secure
     */
    infoReqestUsingGet: (id?: number, params: RequestParams = {}) =>
      this.request<ReqestInfoResponse, void>({
        path: `/api/wewill/admin/inspect/reqest/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 삭제
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name DeleteReqestUsingDelete
     * @summary 의뢰검수 > 의뢰관리 > 상세보기 > 의뢰 삭제
     * @request DELETE:/api/wewill/admin/inspect/reqest/{id}
     * @secure
     */
    deleteReqestUsingDelete: (id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/reqest/${id}`,
        method: "DELETE",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 취소
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name PutCancelReqestUsingPut
     * @summary 의뢰검수 > 의뢰관리 > 상세보기 > 의뢰 취소
     * @request PUT:/api/wewill/admin/inspect/reqest/{id}/cancel
     * @secure
     */
    putCancelReqestUsingPut: (request: ReqestCancelRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/reqest/${id}/cancel`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 재시작
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name PutRestartReqestUsingPut
     * @summary 의뢰검수 > 의뢰관리 > 상세보기 > 의뢰 재시작
     * @request PUT:/api/wewill/admin/inspect/reqest/{id}/restart
     * @secure
     */
    putRestartReqestUsingPut: (id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/reqest/${id}/restart`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 저장
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name PutSaveReqestUsingPut
     * @summary 의뢰검수 > 의뢰관리 > 상세보기 > 의뢰 저장
     * @request PUT:/api/wewill/admin/inspect/reqest/{id}/save
     * @secure
     */
    putSaveReqestUsingPut: (request: ReqestSaveRequest, id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/reqest/${id}/save`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 의뢰 중지
     *
     * @tags [Admin 검수] - 의뢰 관리 API
     * @name PutStopReqestUsingPut
     * @summary 의뢰검수 > 의뢰관리 > 상세보기 > 의뢰 중지
     * @request PUT:/api/wewill/admin/inspect/reqest/{id}/stop
     * @secure
     */
    putStopReqestUsingPut: (id?: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/inspect/reqest/${id}/stop`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  login = {
    /**
     * @description 관리자 로그인
     *
     * @tags [Admin] - 관리자 로그인 API
     * @name LoginManagerUsingPost
     * @summary 관리자 로그인 API
     * @request POST:/api/wewill/admin/login
     * @secure
     */
    loginManagerUsingPost: (request: ManagerLoginRequest, params: RequestParams = {}) =>
      this.request<ManagerLoginResponse, void>({
        path: `/api/wewill/admin/login`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  managers = {
    /**
     * @description 관리자 목록 조회
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name SearchManagerUsingGet
     * @summary Staff 계정 관리 :: 검색 영역 API
     * @request GET:/api/wewill/admin/managers
     * @secure
     */
    searchManagerUsingGet: (query: SearchManagerUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoManagerSearchResponse, void>({
        path: `/api/wewill/admin/managers`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 관리자 등록
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name InsertManagerUsingPost
     * @summary Staff 계정 관리 > Staff 상세 정보 > 기본 정보 (탭) :: 계정 생성 API
     * @request POST:/api/wewill/admin/managers
     * @secure
     */
    insertManagerUsingPost: (request: ManagerInsertRequest, params: RequestParams = {}) =>
      this.request<ManagerInsertResponse, void>({
        path: `/api/wewill/admin/managers`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 관리자 아이디 찾기 중복체크(true : 중복, false : 사용가능)
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name CheckLoginIdDuplicateUsingGet
     * @summary Staff 계정 관리 > Staff 상세 정보 > 기본 정보 (탭) > 아이디 중복체크 API
     * @request GET:/api/wewill/admin/managers/find/{loginId}/exists
     * @secure
     */
    checkLoginIdDuplicateUsingGet: (loginId: string, params: RequestParams = {}) =>
      this.request<boolean, void>({
        path: `/api/wewill/admin/managers/find/${loginId}/exists`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 발급받은 토큰에 대한 내 정보 조회
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name ManagerMeUsingGet
     * @summary Staff 계정 관리 > 내 정보 조회 API
     * @request GET:/api/wewill/admin/managers/me
     * @secure
     */
    managerMeUsingGet: (params: RequestParams = {}) =>
      this.request<ManagerMe, void>({
        path: `/api/wewill/admin/managers/me`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description Staff 관리자 비밀번호 초기화
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name ResetStaffPwInfoUsingPost
     * @summary Staff 계정 관리 > Staff 상세 정보 > 기본 정보 (탭) > 비밀번호 초기화 API
     * @request POST:/api/wewill/admin/managers/resetpw
     * @secure
     */
    resetStaffPwInfoUsingPost: (query: ResetStaffPwInfoUsingPostParams, params: RequestParams = {}) =>
      this.request<ManagerResetPwResponse, void>({
        path: `/api/wewill/admin/managers/resetpw`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 관리자 삭제 (다중 지원 - 복수 입력시 콤마(,) 구분자 사용)
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name DeleteManagerUsingDelete
     * @summary Staff 계정 관리 > 삭제 API
     * @request DELETE:/api/wewill/admin/managers/{ids}
     * @secure
     */
    deleteManagerUsingDelete: (ids: string, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/managers/${ids}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 관리자 상세 조회
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name SelectManagerUsingGet
     * @summary Staff 계정 관리 > Staff 상세 정보 > 기본 정보 (탭) API
     * @request GET:/api/wewill/admin/managers/{id}
     * @secure
     */
    selectManagerUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ManagerInfoResponse, void>({
        path: `/api/wewill/admin/managers/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 관리자 수정
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name ModifyManagerUsingPut
     * @summary Staff 계정 관리 > Staff 상세 정보 > 기본 정보 (탭) > 수정 API
     * @request PUT:/api/wewill/admin/managers/{id}
     * @secure
     */
    modifyManagerUsingPut: ({ id, ...query }: ModifyManagerUsingPutParams, params: RequestParams = {}) =>
      this.request<ManagerModifyResponse, void>({
        path: `/api/wewill/admin/managers/${id}`,
        method: "PUT",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 메뉴 권한 정보 영역의 Staff의 메뉴,기능 권한 목록
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name GetManagerPrivilegeUsingGet
     * @summary Staff 계정 관리 > Staff 상세 정보 > 메뉴 권한 정보 (탭) :: 메뉴,기능 권한 목록 API
     * @request GET:/api/wewill/admin/managers/{managerId}/privileges
     * @secure
     */
    getManagerPrivilegeUsingGet: (managerId: number, params: RequestParams = {}) =>
      this.request<ManagerRolePrivilegeResponse, void>({
        path: `/api/wewill/admin/managers/${managerId}/privileges`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 메뉴 권한 정보 영역의 "저장"버튼. 메뉴, 기능 권한 일괄 등록/수정
     *
     * @tags [Admin Master] - Staff 계정 관리 API
     * @name PutManagerPrivilegeUsingPatch
     * @summary Staff 계정 관리 > Staff 상세 정보 > 메뉴 권한 정보 (탭) :: 메뉴, 기능 권한 저장
     * @request PATCH:/api/wewill/admin/managers/{managerId}/privileges
     * @secure
     */
    putManagerPrivilegeUsingPatch: (
      managerId: number,
      request: ManagerPrivilegeRequest[],
      params: RequestParams = {},
    ) =>
      this.request<ManagerRolePrivilegeResponse, void>({
        path: `/api/wewill/admin/managers/${managerId}/privileges`,
        method: "PATCH",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  media = {
    /**
     * @description 미디어컨텐츠 카드 등록 API
     *
     * @tags [Admin] - 미디어 API
     * @name CardUsingPost
     * @summary 미디어컨텐츠 카드 등록 API
     * @request POST:/api/wewill/admin/media/cards
     * @secure
     */
    cardUsingPost: (request: ContentCardRequest, params: RequestParams = {}) =>
      this.request<ContentCardResponse, void>({
        path: `/api/wewill/admin/media/cards`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 미디어 워크플로우 상세 조회 API
     *
     * @tags [Admin] - 미디어 API
     * @name SelectCardUsingPost
     * @summary 미디어 워크플로우 상세 조회 API
     * @request POST:/api/wewill/admin/media/cards/{contentCardNo}
     * @secure
     */
    selectCardUsingPost: (contentCardNo?: number, params: RequestParams = {}) =>
      this.request<MediaWorkflowResultResponse, void>({
        path: `/api/wewill/admin/media/cards/${contentCardNo}`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  mypage = {
    /**
     * @description 의뢰 대시보드 > 평가자 개별 정보 상세
     *
     * @tags [의뢰자] - 마이페이지 의뢰 대시보드 API
     * @name SearchReqestDashboardUsingGet
     * @summary 마이페이지 > 대시 보드 > 보고서 (프리미엄) > 평가자 개별정보 상세 API
     * @request GET:/api/wewill/admin/mypage/reqestdashboard/{reqestId}
     * @secure
     */
    searchReqestDashboardUsingGet: (
      { reqestId, ...query }: SearchReqestDashboardUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<UserReqestDashboardSearchResponse, void>({
        path: `/api/wewill/admin/mypage/reqestdashboard/${reqestId}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  mypages = {
    /**
     * @description 마이페이지 비밀번호 변경
     *
     * @tags [Admin 검수] - 검수/CS 마이페이지 API
     * @name ResetPwUsingPost
     * @summary 검수/CS 검수 > 마이페이지 > 비밀번호 변경 API
     * @request POST:/api/wewill/admin/mypages/resetPw
     * @secure
     */
    resetPwUsingPost: (request: ManagerResetChangePwRequest, params: RequestParams = {}) =>
      this.request<ManagerResetPwErrorVo, void>({
        path: `/api/wewill/admin/mypages/resetPw`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 마이페이지 상세 조회
     *
     * @tags [Admin 검수] - 검수/CS 마이페이지 API
     * @name SelectMypageUsingGet
     * @summary 검수/CS 검수 > 마이페이지 API
     * @request GET:/api/wewill/admin/mypages/{id}
     * @secure
     */
    selectMypageUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<MyPageInfoResponse, void>({
        path: `/api/wewill/admin/mypages/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  order = {
    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상 업로드 일 경우 등록
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderAllUsingPost
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상 업로드 일 경우 등록 API
     * @request POST:/api/wewill/admin/order/
     * @secure
     */
    insertOrderAllUsingPost: (request: OrderMergeRequest, params: RequestParams = {}) =>
      this.request<OrderMergeResponse, void>({
        path: `/api/wewill/admin/order/`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 통합컨텐츠 영상 스트리밍정보 상세
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectContentCardVidoUsingGet
     * @summary 통합컨텐츠 영상 스트리밍정보 상세 API
     * @request GET:/api/wewill/admin/order/contentCardVido/{contentCardNo}
     * @secure
     */
    selectContentCardVidoUsingGet: (contentCardNo: number, params: RequestParams = {}) =>
      this.request<ContentCardVidoResponse, void>({
        path: `/api/wewill/admin/order/contentCardVido/${contentCardNo}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 의뢰 등록 상세
     *
     * @tags [Admin Master] - 의뢰 콘텐츠 설정 제품 API
     * @name SelectProductUsingGet
     * @summary 의뢰 신청 > 제품 > 의뢰 콘텐츠 설정 > 제품 의뢰 등록 상세 API
     * @request GET:/api/wewill/admin/order/product/{id}
     * @secure
     */
    selectProductUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ProductInfoResponse, void>({
        path: `/api/wewill/admin/order/product/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 의뢰 수정
     *
     * @tags [Admin Master] - 의뢰 콘텐츠 설정 제품 API
     * @name ModifyProductUsingPut
     * @summary 의뢰 신청 > 제품 > 의뢰 콘텐츠 설정 > 제품 의뢰 수정 API
     * @request PUT:/api/wewill/admin/order/product/{id}
     * @secure
     */
    modifyProductUsingPut: (id: number, request: ProductModifyRequest, params: RequestParams = {}) =>
      this.request<ProductModifyResponse, void>({
        path: `/api/wewill/admin/order/product/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 설문전체 일괄 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyAllUsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 설문 일괄 등록 :: 저장
     * @request PATCH:/api/wewill/admin/order/productsurvey
     * @secure
     */
    insertOrderProductSurveyAllUsingPatch: (
      orderSurveyInsertAllRequest: OrderSurveyInsertAllRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertAllResponse, void>({
        path: `/api/wewill/admin/order/productsurvey`,
        method: "PATCH",
        body: orderSurveyInsertAllRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 전 설문 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep1UsingPost
     * @summary 의뢰 신청  > 의뢰 설문 설정 > 제품 > 사용 전 설문 등록 :: 저장
     * @request POST:/api/wewill/admin/order/productsurvey/steps/1
     * @secure
     */
    insertOrderProductSurveyStep1UsingPost: (
      orderProductSurveyStep1InsertRequest: OrderProductSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/1`,
        method: "POST",
        body: orderProductSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 전 설문 수정. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderProductSurveyStep1UsingPut
     * @summary 의뢰 신청  > 의뢰 설문 설정 > 제품 > 사용 전 설문 수정 :: 저장
     * @request PUT:/api/wewill/admin/order/productsurvey/steps/1
     * @secure
     */
    deleteInsertOrderProductSurveyStep1UsingPut: (
      orderProductSurveyStep1InsertRequest: OrderProductSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/1`,
        method: "PUT",
        body: orderProductSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 중 설문 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep2UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 중 설문 등록 :: 저장
     * @request POST:/api/wewill/admin/order/productsurvey/steps/2
     * @secure
     */
    insertOrderProductSurveyStep2UsingPost: (
      orderSurveyProductStep2InsertRequest: OrderProductSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/2`,
        method: "POST",
        body: orderSurveyProductStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 중 설문 수정. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderProductSurveyStep2UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 중 설문 수정 :: 저장
     * @request PUT:/api/wewill/admin/order/productsurvey/steps/2
     * @secure
     */
    deleteInsertOrderProductSurveyStep2UsingPut: (
      orderSurveyProductStep2InsertRequest: OrderProductSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/2`,
        method: "PUT",
        body: orderSurveyProductStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 후 설문 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep3UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 후 설문 등록 :: 저장
     * @request POST:/api/wewill/admin/order/productsurvey/steps/3
     * @secure
     */
    insertOrderProductSurveyStep3UsingPost: (
      orderProductSurveyStep3InsertRequest: OrderProductSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/3`,
        method: "POST",
        body: orderProductSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 후 설문 수정. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderProductSurveyStep3UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 후 설문 수정 :: 저장
     * @request PUT:/api/wewill/admin/order/productsurvey/steps/3
     * @secure
     */
    deleteInsertOrderProductSurveyStep3UsingPut: (
      orderProductSurveyStep3InsertRequest: OrderProductSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/3`,
        method: "PUT",
        body: orderProductSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 견적 요청 등록.
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep4UsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 후 설문 :: 견적요청
     * @request PATCH:/api/wewill/admin/order/productsurvey/steps/4
     * @secure
     */
    insertOrderProductSurveyStep4UsingPatch: (
      orderSurveyFinalInsertRequest: OrderSurveyFinalInsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<InsertReqestEstimateResponse, void>({
        path: `/api/wewill/admin/order/productsurvey/steps/4`,
        method: "PATCH",
        body: orderSurveyFinalInsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 심사반려인 경우 의뢰수정 완료
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name UpdateReqestStatusEvaluateStandbyUsingGet
     * @summary 심사반려인 경우 의뢰수정 완료 API
     * @request GET:/api/wewill/admin/order/reqest/{reqestId}/evaluate/standby
     * @secure
     */
    updateReqestStatusEvaluateStandbyUsingGet: (reqestId: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/order/reqest/${reqestId}/evaluate/standby`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰반려사유 조회
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectReturnResnInReqestCancelUsingGet
     * @summary 의뢰반려사유 조회 API
     * @request GET:/api/wewill/admin/order/reqestCancel/returnResn
     * @secure
     */
    selectReturnResnInReqestCancelUsingGet: (
      query: SelectReturnResnInReqestCancelUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<OrderReturnResnResponse, void>({
        path: `/api/wewill/admin/order/reqestCancel/returnResn`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 저장한 임시데이터 조회. 디코딩 샘플 const json = JSON.parse(decodeURI(response.jsonDataUriEncoded));
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectSaveTempUsingGet
     * @summary 의뢰신청 임시저장 데이터 조회
     * @request GET:/api/wewill/admin/order/save/{reqestId}
     * @secure
     */
    selectSaveTempUsingGet: (reqestId: number, params: RequestParams = {}) =>
      this.request<ReqestSaveResponse, void>({
        path: `/api/wewill/admin/order/save/${reqestId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰신청 전체 임시저장. 재호출 시 덮어씀. 인코딩 샘플 const json = {a:1} const jsonStrURIEncoded = encodeURI(JSON.stringify(json));
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SaveTempUsingPut
     * @summary 의뢰신청 전체 임시저장
     * @request PUT:/api/wewill/admin/order/save/{reqestId}
     * @secure
     */
    saveTempUsingPut: (reqestId: number, jsonDataUriEncoded: string, params: RequestParams = {}) =>
      this.request<ReqestSaveResponse, void>({
        path: `/api/wewill/admin/order/save/${reqestId}`,
        method: "PUT",
        body: jsonDataUriEncoded,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 상세
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderSetUsingGet
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 상세 API
     * @request GET:/api/wewill/admin/order/set/{id}
     * @secure
     */
    selectOrderSetUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<OrderSetInfoResponse, void>({
        path: `/api/wewill/admin/order/set/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 등록 & 수정
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderSetUsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 등록 & 수정 API
     * @request PUT:/api/wewill/admin/order/set/{id}
     * @secure
     */
    modifyOrderSetUsingPut: (id: number, request: OrderSetModifyRequest, params: RequestParams = {}) =>
      this.request<OrderSetModifyResponse, void>({
        path: `/api/wewill/admin/order/set/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 설문전체 일괄 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyAllUsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 설문 일괄 등록
     * @request PATCH:/api/wewill/admin/order/survey
     * @secure
     */
    insertOrderSurveyAllUsingPatch: (
      orderSurveyInsertAllRequest: OrderSurveyInsertAllRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertAllResponse, void>({
        path: `/api/wewill/admin/order/survey`,
        method: "PATCH",
        body: orderSurveyInsertAllRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 프리미엄 > 사전 설문 등록용 정책데이터
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderSurveyPolicyUsingGet
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사전 설문 등록 :: 정책데이터
     * @request GET:/api/wewill/admin/order/survey/policies
     * @secure
     */
    selectOrderSurveyPolicyUsingGet: (params: RequestParams = {}) =>
      this.request<OrderSurveyPolicyResponse, void>({
        path: `/api/wewill/admin/order/survey/policies`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 사전 설문 등록용
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep1UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사전 설문 등록 :: 저장
     * @request POST:/api/wewill/admin/order/survey/steps/1
     * @secure
     */
    insertOrderSurveyStep1UsingPost: (
      orderSurveyStep1InsertRequest: OrderSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/1`,
        method: "POST",
        body: orderSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사전 설문 수정용
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderSurveyStep1UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사전 설문 수정 :: 저장
     * @request PUT:/api/wewill/admin/order/survey/steps/1
     * @secure
     */
    deleteInsertOrderSurveyStep1UsingPut: (
      orderSurveyStep1InsertRequest: OrderSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/1`,
        method: "PUT",
        body: orderSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 실시간 설문 일괄 등록용. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep2UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 실시간 설문 등록 :: 저장
     * @request POST:/api/wewill/admin/order/survey/steps/2
     * @secure
     */
    insertOrderSurveyStep2UsingPost: (
      orderSurveyStep2InsertRequest: OrderSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyStep2InsertResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/2`,
        method: "POST",
        body: orderSurveyStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 실시간 설문 일괄 수정용. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderSurveyStep2UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 실시간 설문 수정 :: 저장
     * @request PUT:/api/wewill/admin/order/survey/steps/2
     * @secure
     */
    deleteInsertOrderSurveyStep2UsingPut: (
      orderSurveyStep2InsertRequest: OrderSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyStep2InsertResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/2`,
        method: "PUT",
        body: orderSurveyStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사후 설문 등록용. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep3UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사후 설문 등록 :: 저장
     * @request POST:/api/wewill/admin/order/survey/steps/3
     * @secure
     */
    insertOrderSurveyStep3UsingPost: (
      orderSurveyStep3InsertRequest: OrderSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/3`,
        method: "POST",
        body: orderSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사후 설문 수정용. 등록/수정 동일 - delete / insert
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderSurveyStep3UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사후 설문 수정 :: 저장
     * @request PUT:/api/wewill/admin/order/survey/steps/3
     * @secure
     */
    deleteInsertOrderSurveyStep3UsingPut: (
      orderSurveyStep3InsertRequest: OrderSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/3`,
        method: "PUT",
        body: orderSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 최종평가 및 견적 요청 등록. 베이직은 step 1~3 없이 4번 즉시 등록
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep4UsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 베이직/프리미엄 > 최종평가 :: 견적요청
     * @request PATCH:/api/wewill/admin/order/survey/steps/4
     * @secure
     */
    insertOrderSurveyStep4UsingPatch: (
      orderSurveyStep4InsertRequest: OrderSurveyStep4InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<InsertReqestEstimateResponse, void>({
        path: `/api/wewill/admin/order/survey/steps/4`,
        method: "PATCH",
        body: orderSurveyStep4InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 등록
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderVidoUsingPost
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 등록 API
     * @request POST:/api/wewill/admin/order/vido
     * @secure
     */
    insertOrderVidoUsingPost: (request: OrderVidoInsertRequest, params: RequestParams = {}) =>
      this.request<OrderVidoInsertResponse, void>({
        path: `/api/wewill/admin/order/vido`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 등록
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderYoutubeUsingPost
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 등록 API
     * @request POST:/api/wewill/admin/order/youtube
     * @secure
     */
    insertOrderYoutubeUsingPost: (request: OrderYoutubeInsertRequest, params: RequestParams = {}) =>
      this.request<OrderYoutubeInsertResponse, void>({
        path: `/api/wewill/admin/order/youtube`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > (Youtube 일 경우 & 영상 업로드 일 경우) 상세
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderVidoUsingGet
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > (Youtube 일 경우 & 영상 업로드 일 경우) 상세 API
     * @request GET:/api/wewill/admin/order/{id}
     * @secure
     */
    selectOrderVidoUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<OrderVidoInfoResponse, void>({
        path: `/api/wewill/admin/order/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 수정
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderVidoUsingPut
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 수정 API
     * @request PUT:/api/wewill/admin/order/{id}/vido/{vidoId}
     * @secure
     */
    modifyOrderVidoUsingPut: (
      id: number,
      vidoId: number,
      request: OrderVidoModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderVidoModifyResponse, void>({
        path: `/api/wewill/admin/order/${id}/vido/${vidoId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상업로드 일 경우 수정
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderAllUsingPut
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상업로드 일 경우 수정 API
     * @request PUT:/api/wewill/admin/order/{id}/vido/{vidoId}/common
     * @secure
     */
    modifyOrderAllUsingPut: (id: number, vidoId: number, request: OrderMergeRequest, params: RequestParams = {}) =>
      this.request<OrderMergeResponse, void>({
        path: `/api/wewill/admin/order/${id}/vido/${vidoId}/common`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 수정
     *
     * @tags [Admin 검수] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderYoutubeUsingPut
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 수정 API
     * @request PUT:/api/wewill/admin/order/{id}/youtube/{vidoId}
     * @secure
     */
    modifyOrderYoutubeUsingPut: (
      id: number,
      vidoId: number,
      request: OrderYoutubeModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderYoutubeModifyResponse, void>({
        path: `/api/wewill/admin/order/${id}/youtube/${vidoId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  pointdefrayment = {
    /**
     * @description 포인트 출금 신청 목록 ExcelDown
     *
     * @tags [Admin] - 포인트 출금 API
     * @name FindPointDefrayExcelDownUsingGet
     * @summary 포인트 출금 > 출금신청 목록 ExcelDown
     * @request GET:/api/wewill/admin/pointdefrayment/defray/excel
     * @secure
     */
    findPointDefrayExcelDownUsingGet: (query: FindPointDefrayExcelDownUsingGetParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/pointdefrayment/defray/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 출금 신청 목록 조회
     *
     * @tags [Admin] - 포인트 출금 API
     * @name FindPointDefrayUsingGet
     * @summary 포인트 출금 > 출금신청 목록 조회
     * @request GET:/api/wewill/admin/pointdefrayment/defray/findAll
     * @secure
     */
    findPointDefrayUsingGet: (query: FindPointDefrayUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPointDefraymentResponse, void>({
        path: `/api/wewill/admin/pointdefrayment/defray/findAll`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 출금 신청 상세 조회
     *
     * @tags [Admin] - 포인트 출금 API
     * @name FindPointDefrayInfoUsingGet
     * @summary 포인트 출금 > 출금신청 상세 (팝업) > 조회
     * @request GET:/api/wewill/admin/pointdefrayment/defray/{id}
     * @secure
     */
    findPointDefrayInfoUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<PointDefraymentInfoResponse, void>({
        path: `/api/wewill/admin/pointdefrayment/defray/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 출금 신청 상세 수정
     *
     * @tags [Admin] - 포인트 출금 API
     * @name UpdatePointDefraymentInfoUsingPut
     * @summary 포인트 출금 > 출금신청 상세 (팝업) > 수정
     * @request PUT:/api/wewill/admin/pointdefrayment/defray/{id}
     * @secure
     */
    updatePointDefraymentInfoUsingPut: (
      id: number,
      request: PointDefraymentModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/pointdefrayment/defray/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  pointdetail = {
    /**
     * @description 포인트목록 조회 페이징
     *
     * @tags [Admin] - 포인트 상세 API
     * @name FindAllPointDetailUsingGet
     * @summary 포인트 전체 조회
     * @request GET:/api/wewill/admin/pointdetail
     * @secure
     */
    findAllPointDetailUsingGet: (query: FindAllPointDetailUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPointDetail, void>({
        path: `/api/wewill/admin/pointdetail`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 상품저장
     *
     * @tags [Admin] - 포인트 상세 API
     * @name RegisterPointDetailUsingPost
     * @summary 상품저장
     * @request POST:/api/wewill/admin/pointdetail
     * @secure
     */
    registerPointDetailUsingPost: (pointDetail: PointDetail, params: RequestParams = {}) =>
      this.request<PointDetail, void>({
        path: `/api/wewill/admin/pointdetail`,
        method: "POST",
        body: pointDetail,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 포인트 조회
     *
     * @tags [Admin] - 포인트 상세 API
     * @name FindPointDetailUsingGet
     * @summary 포인트 조회
     * @request GET:/api/wewill/admin/pointdetail/{no}
     * @secure
     */
    findPointDetailUsingGet: (no: number, params: RequestParams = {}) =>
      this.request<PointDetail, void>({
        path: `/api/wewill/admin/pointdetail/${no}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트목록 수정
     *
     * @tags [Admin] - 포인트 상세 API
     * @name UpdatePointDetailUsingPut
     * @summary 포인트 수정
     * @request PUT:/api/wewill/admin/pointdetail/{no}
     * @secure
     */
    updatePointDetailUsingPut: (no: number, pointDetail: PointDetail, params: RequestParams = {}) =>
      this.request<PointDetail, void>({
        path: `/api/wewill/admin/pointdetail/${no}`,
        method: "PUT",
        body: pointDetail,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  pointmaster = {
    /**
     * @description 포인트 마스터 목록 조회 페이징
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name FindAllPointMasterUsingGet
     * @summary 포인트 마스터 전체 조회
     * @request GET:/api/wewill/admin/pointmaster
     * @secure
     */
    findAllPointMasterUsingGet: (query: FindAllPointMasterUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPointMasterResponse, void>({
        path: `/api/wewill/admin/pointmaster`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 저장
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name RegisterPointMasterUsingPost
     * @summary 포인트 마스터 저장
     * @request POST:/api/wewill/admin/pointmaster
     * @secure
     */
    registerPointMasterUsingPost: (request: PointMasterInsertRequest, params: RequestParams = {}) =>
      this.request<PointMasterInsertResponse, void>({
        path: `/api/wewill/admin/pointmaster`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 포인트 마스터 Group By 목록 조회 페이징
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name FindGroupAllPointMasterUsingGet
     * @summary 포인트 마스터 Group By 개별 조회
     * @request GET:/api/wewill/admin/pointmaster/find
     * @secure
     */
    findGroupAllPointMasterUsingGet: (query: FindGroupAllPointMasterUsingGetParams, params: RequestParams = {}) =>
      this.request<PointMasterSingleGroupByResponse, void>({
        path: `/api/wewill/admin/pointmaster/find`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 Group By 목록 조회 페이징
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name FindGroupPointMasterUsingGet
     * @summary 포인트 마스터 Group By 전체 조회
     * @request GET:/api/wewill/admin/pointmaster/findAll
     * @secure
     */
    findGroupPointMasterUsingGet: (query: FindGroupPointMasterUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoMemberidPointSumInterface, void>({
        path: `/api/wewill/admin/pointmaster/findAll`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 다건의 포인트 적립 및 차감
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name RegisterAdminPointManageUsingPost
     * @summary 포인트 관리 > 포인트 적립/차감
     * @request POST:/api/wewill/admin/pointmaster/manage
     * @secure
     */
    registerAdminPointManageUsingPost: (request: AdminPointManageInsertRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/pointmaster/manage`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 포인트 관리 > 포인트 적립/차감 Group By 목록 조회 페이징
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name FindGroupAdminPointManageUsingGet
     * @summary 포인트 관리 > 포인트 적립/차감 Group By 전체 조회
     * @request GET:/api/wewill/admin/pointmaster/manage/findAll
     * @secure
     */
    findGroupAdminPointManageUsingGet: (query: FindGroupAdminPointManageUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminPointManageResponse, void>({
        path: `/api/wewill/admin/pointmaster/manage/findAll`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 관리 > 포인트 내역 > 포인트 내역 리스트
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name FindAdminPointListUsingGet
     * @summary 포인트 관리 > 포인트 내역
     * @request GET:/api/wewill/admin/pointmaster/manage/list
     * @secure
     */
    findAdminPointListUsingGet: (query: FindAdminPointListUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminPointListResponse, void>({
        path: `/api/wewill/admin/pointmaster/manage/list`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 조회
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name FindPointMasterUsingGet
     * @summary 포인트 마스터 조회
     * @request GET:/api/wewill/admin/pointmaster/{no}
     * @secure
     */
    findPointMasterUsingGet: (no: number, params: RequestParams = {}) =>
      this.request<PointMasterResponse, void>({
        path: `/api/wewill/admin/pointmaster/${no}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 목록 수정
     *
     * @tags [Admin] - 포인트 마스터 API
     * @name UpdatePointMasterUsingPut
     * @summary 포인트 마스터 수정
     * @request PUT:/api/wewill/admin/pointmaster/{no}
     * @secure
     */
    updatePointMasterUsingPut: (no: number, request: PointMasterModifyRequest, params: RequestParams = {}) =>
      this.request<PointMasterModifyResponse, void>({
        path: `/api/wewill/admin/pointmaster/${no}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  policies = {
    /**
     * @description 욕설목록
     *
     * @tags [Admin] - 정책 API
     * @name GetBadwordsPolicyUsingGet
     * @summary 욕설 정책
     * @request GET:/api/wewill/admin/policies/badwords
     * @secure
     */
    getBadwordsPolicyUsingGet: (params: RequestParams = {}) =>
      this.request<string[], void>({
        path: `/api/wewill/admin/policies/badwords`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 욕설 체크
     *
     * @tags [Admin] - 정책 API
     * @name CheckBadwordsUsingPost
     * @summary 욕설 체크
     * @request POST:/api/wewill/admin/policies/badwords
     * @secure
     */
    checkBadwordsUsingPost: (wordList: string[], params: RequestParams = {}) =>
      this.request<CheckBadWordsResponse, void>({
        path: `/api/wewill/admin/policies/badwords`,
        method: "POST",
        body: wordList,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  postfiles = {
    /**
     * @description 게시글 첨부파일 등록
     *
     * @tags [Admin Master] - 게시글 첨부파일(관계테이블) API
     * @name InsertPostFileUsingPost
     * @summary 게시글 첨부파일 등록 API
     * @request POST:/api/wewill/admin/postfiles
     * @secure
     */
    insertPostFileUsingPost: (query: InsertPostFileUsingPostParams, params: RequestParams = {}) =>
      this.request<PostFileInsertRequest, void>({
        path: `/api/wewill/admin/postfiles`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 게시글 첨부파일 삭제
     *
     * @tags [Admin Master] - 게시글 첨부파일(관계테이블) API
     * @name DeletePostFileUsingDelete
     * @summary 게시글 첨부파일 삭제 API
     * @request DELETE:/api/wewill/admin/postfiles/{id}
     * @secure
     */
    deletePostFileUsingDelete: ({ id, ...query }: DeletePostFileUsingDeleteParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/postfiles/${id}`,
        method: "DELETE",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  posts = {
    /**
     * @description 이벤트 게시글 이벤트 목록 조회
     *
     * @tags [Admin Master] - 이벤트 API
     * @name SearchEventPostUsingGet
     * @summary 페이지 기본 관리 > 게시판 관리 > 이벤트 API
     * @request GET:/api/wewill/admin/posts/event
     * @secure
     */
    searchEventPostUsingGet: (query: SearchEventPostUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPostEventSearchResponse, void>({
        path: `/api/wewill/admin/posts/event`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 이벤트 게시글 등록
     *
     * @tags [Admin Master] - 이벤트 API
     * @name InsertEventPostUsingPost
     * @summary 페이지 기본 관리 > 게시판 관리 > 이벤트 > 이벤트 생성 API
     * @request POST:/api/wewill/admin/posts/event
     * @secure
     */
    insertEventPostUsingPost: (postInsertRequest: PostEventInsertRequest, params: RequestParams = {}) =>
      this.request<PostEventInsertResponse, void>({
        path: `/api/wewill/admin/posts/event`,
        method: "POST",
        body: postInsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 이벤트 게시글 삭제 (다중 지원 - 복수 입력시 콤마(,) 구분자 사용)
     *
     * @tags [Admin Master] - 이벤트 API
     * @name DeleteEventPostsUsingDelete
     * @summary 페이지 기본 관리 > 게시판 관리 > 이벤트 ::(선택 삭제 팝업) API
     * @request DELETE:/api/wewill/admin/posts/event
     * @secure
     */
    deleteEventPostsUsingDelete: (request: PostEventDeleteRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/posts/event`,
        method: "DELETE",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 이벤트 게시글 상세 조회
     *
     * @tags [Admin Master] - 이벤트 API
     * @name SelectEventPostUsingGet
     * @summary 페이지 기본 관리 > 게시판 관리 > 이벤트 상세 정보 API
     * @request GET:/api/wewill/admin/posts/event/{id}
     * @secure
     */
    selectEventPostUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<PostEventInfoResponse, void>({
        path: `/api/wewill/admin/posts/event/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 이벤트 게시글 수정
     *
     * @tags [Admin Master] - 이벤트 API
     * @name ModifyEventPostUsingPut
     * @summary 페이지 기본 관리 > 게시판 관리 > 이벤트 > 이벤트 수정 API
     * @request PUT:/api/wewill/admin/posts/event/{id}
     * @secure
     */
    modifyEventPostUsingPut: (id: number, postModifyRequest: PostEventModifyRequest, params: RequestParams = {}) =>
      this.request<PostEventModifyResponse, void>({
        path: `/api/wewill/admin/posts/event/${id}`,
        method: "PUT",
        body: postModifyRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 공지사항 게시글 목록 조회
     *
     * @tags [Admin Master] - 공지사항 API
     * @name SearchNoticePostUsingGet
     * @summary 페이지 기본 관리 > 게시판 관리 > 공지사항 API
     * @request GET:/api/wewill/admin/posts/notice
     * @secure
     */
    searchNoticePostUsingGet: (query: SearchNoticePostUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPostNoticeSearchResponse, void>({
        path: `/api/wewill/admin/posts/notice`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 공지사항 게시글 등록
     *
     * @tags [Admin Master] - 공지사항 API
     * @name InsertNoticePostUsingPost
     * @summary 페이지 기본 관리 > 게시판 관리 > 공지사항 생성 API
     * @request POST:/api/wewill/admin/posts/notice
     * @secure
     */
    insertNoticePostUsingPost: (postInsertRequest: PostNoticeInsertRequest, params: RequestParams = {}) =>
      this.request<PostNoticeInsertResponse, void>({
        path: `/api/wewill/admin/posts/notice`,
        method: "POST",
        body: postInsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 공지사항 게시글 삭제 (다중 지원 - 복수 입력시 콤마(,) 구분자 사용)
     *
     * @tags [Admin Master] - 공지사항 API
     * @name DeleteNoticePostsUsingDelete
     * @summary 페이지 기본 관리 > 게시판 관리 > 공지사항 ::(선택 삭제 팝업) API
     * @request DELETE:/api/wewill/admin/posts/notice
     * @secure
     */
    deleteNoticePostsUsingDelete: (request: PostNoticeDeleteRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/posts/notice`,
        method: "DELETE",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 공지사항 게시글 상세 조회
     *
     * @tags [Admin Master] - 공지사항 API
     * @name SelectNoticePostUsingGet
     * @summary 페이지 기본 관리 > 게시판 관리 > 공지사항 상세 정보 API
     * @request GET:/api/wewill/admin/posts/notice/{id}
     * @secure
     */
    selectNoticePostUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<PostNoticeInfoResponse, void>({
        path: `/api/wewill/admin/posts/notice/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 공지사항 게시글 수정
     *
     * @tags [Admin Master] - 공지사항 API
     * @name ModifyNoticePostUsingPut
     * @summary 페이지 기본 관리 > 게시판 관리 > 공지사항 수정 API
     * @request PUT:/api/wewill/admin/posts/notice/{id}
     * @secure
     */
    modifyNoticePostUsingPut: (id: number, postModifyRequest: PostNoticeModifyRequest, params: RequestParams = {}) =>
      this.request<PostNoticeModifyResponse, void>({
        path: `/api/wewill/admin/posts/notice/${id}`,
        method: "PUT",
        body: postModifyRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  qratings = {
    /**
     * @description 전체 큐레이팅 목록 조회
     *
     * @tags [Admin Master] - 큐레이팅 관리 API
     * @name SearchQratingUsingGet
     * @summary 큐레이팅 관리 > 큐레이팅 목록 > 목록 조회 API
     * @request GET:/api/wewill/admin/qratings
     * @secure
     */
    searchQratingUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoAdminQratingSearchResponse, void>({
        path: `/api/wewill/admin/qratings`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 큐레이팅 수정 (다중 지원)
     *
     * @tags [Admin Master] - 큐레이팅 관리 API
     * @name ModifyQratingUsingPut
     * @summary 큐레이팅 관리 > 큐레이팅 목록 > 큐레이팅 수정 API
     * @request PUT:/api/wewill/admin/qratings
     * @secure
     */
    modifyQratingUsingPut: (requestList: AdminQratingModifyRequest[], params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/qratings`,
        method: "PUT",
        body: requestList,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 큐레이팅 삭제 (다중 지원)
     *
     * @tags [Admin Master] - 큐레이팅 관리 API
     * @name DeleteQratingUsingDelete
     * @summary 큐레이팅 관리 > 큐레이팅 목록 > 큐레이팅 삭제 API
     * @request DELETE:/api/wewill/admin/qratings
     * @secure
     */
    deleteQratingUsingDelete: (qratingIds: number[], params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/qratings`,
        method: "DELETE",
        body: qratingIds,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  recaptcha = {
    /**
     * @description 리캡챠
     *
     * @tags 리캡챠 API
     * @name RecaptchaUsingPost
     * @summary 리캡챠 API
     * @request POST:/api/wewill/admin/recaptcha
     * @secure
     */
    recaptchaUsingPost: (request: TokenRequest, params: RequestParams = {}) =>
      this.request<RecaptchaResponse, void>({
        path: `/api/wewill/admin/recaptcha`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  reportExcel = {
    /**
     * @description Admin > 의뢰자 보고서 엑셀 다운로드
     *
     * @tags [Admin 검수] - 의뢰자 보고서 엑셀 다운로드
     * @name DownloadAdminReportExcelUsingGet
     * @summary Admnin > 대시보드 > 보고서 > 엑셀 다운로드
     * @request GET:/api/wewill/admin/reportExcel/{requestId}
     * @secure
     */
    downloadAdminReportExcelUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/admin/reportExcel/${requestId}`,
        method: "GET",
        secure: true,
        ...params,
      }),
  };
  secession = {
    /**
     * @description 의뢰자 다중 탈퇴
     *
     * @tags [Master] - 의뢰자 회원 관리 API
     * @name SecessionClientMembersUsingPut
     * @summary 회원 관리 > 의뢰자 > 회원 검색 > 다중탈퇴
     * @request PUT:/api/wewill/admin/secession/client-members
     * @secure
     */
    secessionClientMembersUsingPut: (req: AdminMemberSecessionRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/secession/client-members`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 다중 탈퇴
     *
     * @tags [Master] - 평가자 회원 관리 API
     * @name SecessionClientMembersUsingPut1
     * @summary 평가자 다중 탈퇴 처리 api
     * @request PUT:/api/wewill/admin/secession/evaluator-members
     * @secure
     */
    secessionClientMembersUsingPut1: (req: AdminMemberSecessionRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/admin/secession/evaluator-members`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  alarms = {
    /**
     * @description 알림 발송 테스트
     *
     * @tags [의뢰자] - 알림 API
     * @name SendAlarmUsingGet
     * @summary 알림 발송 테스트
     * @request GET:/api/wewill/common/alarms
     * @secure
     */
    sendAlarmUsingGet: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/common/alarms`,
        method: "GET",
        secure: true,
        ...params,
      }),
  };
  check = {
    /**
     * @description 회원 아이디 중복 체크(통합,wewill 확인)
     *
     * @tags [공통] - 아이디 중복체크 API
     * @name SendNewPasswordUsingGet
     * @summary 회원 아이디 중복 체크(통합,wewill 확인)
     * @request GET:/api/wewill/common/check/username
     * @secure
     */
    sendNewPasswordUsingGet: (query: SendNewPasswordUsingGetParams, params: RequestParams = {}) =>
      this.request<CommonUsernameCheckResponse, void>({
        path: `/api/wewill/common/check/username`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  codegroups = {
    /**
     * @description 전체 코드그룹 목록 조회
     *
     * @tags [공통] - 공통코드 API
     * @name SelectCodeGroupListUsingGet
     * @summary 공통코드 > 코드그룹 목록 > 전체 코드그룹 목록 조회 API
     * @request GET:/api/wewill/common/codegroups
     * @secure
     */
    selectCodeGroupListUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoCodeGroupListResponse, void>({
        path: `/api/wewill/common/codegroups`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 전체 코드그룹 + 코드목록 조회
     *
     * @tags [공통] - 공통코드 API
     * @name SelectCodeGroupTreeUsingGet
     * @summary 공통코드 > 코드그룹+코드 목록 조회 > 전체 코드그룹 + 코드 목록 조회 API
     * @request GET:/api/wewill/common/codegroups/codes
     * @secure
     */
    selectCodeGroupTreeUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoCodeGroupTreeResponse, void>({
        path: `/api/wewill/common/codegroups/codes`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 입력받은 코드 id값 제외한 코드 목록조회(복수 코드 ID 지원)
     *
     * @tags [공통] - 공통코드 API
     * @name SelectExceptCodeListUsingGet
     * @summary 공통코드 > 코드목록 조회 > 입력 받은 코드 id 값 제외한 코드 목록조회 API
     * @request GET:/api/wewill/common/codegroups/{codeGroupId}/codes/except/{codeId}
     * @secure
     */
    selectExceptCodeListUsingGet: (codeGroupId: string, codeId: string, params: RequestParams = {}) =>
      this.request<ListVoCodeListResponse, void>({
        path: `/api/wewill/common/codegroups/${codeGroupId}/codes/except/${codeId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 코드그룹 id 값 통한 코드목록 조회(복수 코드그룹 ID 지원)
     *
     * @tags [공통] - 공통코드 API
     * @name SelectCodeListByCodeGroupIdsUsingGet
     * @summary 공통코드 > 코드 목록 조회 > 코드그룹 ID 값 통한 코드 목록 조회 API
     * @request GET:/api/wewill/common/codegroups/{id}/codes
     * @secure
     */
    selectCodeListByCodeGroupIdsUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<ListVoCodeListResponse, void>({
        path: `/api/wewill/common/codegroups/${id}/codes`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  file = {
    /**
     * @description 첨부파일 등록 API
     *
     * @tags [공통] - 파일업로드 API
     * @name UploadAttachFileUsingPost
     * @summary 첨부파일 등록 API
     * @request POST:/api/wewill/common/file/upload
     * @secure
     */
    uploadAttachFileUsingPost: (
      query: UploadAttachFileUsingPostParams,
      data: { file?: File },
      params: RequestParams = {},
    ) =>
      this.request<UploadAttachFileResponse, void>({
        path: `/api/wewill/common/file/upload`,
        method: "POST",
        query: query,
        body: data,
        secure: true,
        type: ContentType.FormData,
        format: "document",
        ...params,
      }),
  };
  samples = {
    /**
     * @description 알림 발송 테스트
     *
     * @tags [공통샘플] - 샘플 API
     * @name SendAlarm2UsingGet
     * @summary 알림 발송 테스트
     * @request GET:/api/wewill/common/samples/alarms
     * @secure
     */
    sendAlarm2UsingGet: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/common/samples/alarms`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 코드매니저 코드명 조회 테스트
     *
     * @tags [공통샘플] - 샘플 API
     * @name GetCodeNameUsingGet
     * @summary 코드매니저 코드명 조회 테스트
     * @request GET:/api/wewill/common/samples/codename
     * @secure
     */
    getCodeNameUsingGet: (query: GetCodeNameUsingGetParams, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/api/wewill/common/samples/codename`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description Exception 테스트2
     *
     * @tags [공통샘플] - 샘플 API
     * @name GetCodeNameExceptionUsingGet
     * @summary Exception 테스트2
     * @request GET:/api/wewill/common/samples/exception
     * @secure
     */
    getCodeNameExceptionUsingGet: (params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/api/wewill/common/samples/exception`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 웹소켓 알림 발송 테스트
     *
     * @tags [공통샘플] - 샘플 API
     * @name SendWebsocketNotificationUsingPost
     * @summary 웹소켓 알림 발송 테스트
     * @request POST:/api/wewill/common/samples/notifications
     * @secure
     */
    sendWebsocketNotificationUsingPost: (request: NotificationMessage, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/common/samples/notifications`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  send = {
    /**
     * @description 임시 비밀번호 변경 및 메일 전송 API
     *
     * @tags [공통] - 비밀변호 변경 API
     * @name SendNewPasswordUsingPost
     * @summary 임시 비밀번호 변경 및 메일 전송 API
     * @request POST:/api/wewill/common/send/new-password
     * @secure
     */
    sendNewPasswordUsingPost: (req: CommonResetPasswordRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/common/send/new-password`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  settlements = {
    /**
     * @description 결제 완료 후 후속 처리 Process API : 결제쿠폰 등록, 사용자 쿠폰 사용 상태로 수정, 포인트 등록 + 사용 처리 등
     *
     * @tags [공통] - 결제 API
     * @name SettlementRequestUsingPost
     * @summary 의뢰 신청 > 신용카드&가상계좌&계좌이체 결제 완료
     * @request POST:/api/wewill/common/settlements
     * @secure
     */
    settlementRequestUsingPost: (request: SettlementInsertRequest, params: RequestParams = {}) =>
      this.request<SettlementInsertResponse, void>({
        path: `/api/wewill/common/settlements`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 취소
     *
     * @tags [공통] - 결제 API
     * @name SettlementCancelRequestUsingPost
     * @summary 결제 취소 API
     * @request POST:/api/wewill/common/settlements/cancel
     * @secure
     */
    settlementCancelRequestUsingPost: (request: SettlementCancelRequest, params: RequestParams = {}) =>
      this.request<InputStream, void>({
        path: `/api/wewill/common/settlements/cancel`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 완료 후 I/F Table 등록
     *
     * @tags [공통] - 결제 API
     * @name PayCompleteUsingPost
     * @summary 의뢰 신청 > 신용카드&가상계좌&계좌이체 결제 완료
     * @request POST:/api/wewill/common/settlements/complete
     * @secure
     */
    payCompleteUsingPost: (query: PayCompleteUsingPostParams, params: RequestParams = {}) =>
      this.request<IfImportPayCompleteResponse, void>({
        path: `/api/wewill/common/settlements/complete`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 시작 시 I/F Table 기본 정보 등록
     *
     * @tags [공통] - 결제 API
     * @name PayGenerateUsingPost
     * @summary 의뢰 신청 > 결제
     * @request POST:/api/wewill/common/settlements/generate
     * @secure
     */
    payGenerateUsingPost: (query: PayGenerateUsingPostParams, params: RequestParams = {}) =>
      this.request<IfImportPayGenerateResponse, void>({
        path: `/api/wewill/common/settlements/generate`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Request parameter를 이용하여 Settlement data 조회
     *
     * @tags [공통] - 결제 API
     * @name SearchSettlementUsingGet
     * @summary 결제 검색 조회 API
     * @request GET:/api/wewill/common/settlements/search
     * @secure
     */
    searchSettlementUsingGet: (query: SearchSettlementUsingGetParams, params: RequestParams = {}) =>
      this.request<SettlementResponse, void>({
        path: `/api/wewill/common/settlements/search`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags [공통] - 결제 API
     * @name PayTokenUsingPost
     * @summary payToken
     * @request POST:/api/wewill/common/settlements/token
     * @secure
     */
    payTokenUsingPost: (params: RequestParams = {}) =>
      this.request<InputStream, void>({
        path: `/api/wewill/common/settlements/token`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description [최근 결제내역 조회] 기간에 따라 조회가능 목록조회 페이징처리
     *
     * @tags [공통] - 결제 API
     * @name PaymentResultListPagingUsingGet
     * @summary [최근 결제내역 조회] 목록조회 페이징처리 API
     * @request GET:/api/wewill/common/settlements/{pageNo}/{pageSize}
     * @secure
     */
    paymentResultListPagingUsingGet: (
      { pageNo, pageSize, ...query }: PaymentResultListPagingUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<PackageResponse, void>({
        path: `/api/wewill/common/settlements/${pageNo}/${pageSize}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제결과 조회
     *
     * @tags [공통] - 결제 API
     * @name SelectSettlementResultUsingGet
     * @summary 결제결과 조회
     * @request GET:/api/wewill/common/settlements/{settlementNo}
     * @secure
     */
    selectSettlementResultUsingGet: (settlementNo: string, params: RequestParams = {}) =>
      this.request<IfImportPayCompleteVbankResponse, void>({
        path: `/api/wewill/common/settlements/${settlementNo}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
}
