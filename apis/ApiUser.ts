/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface AdminPointListResponse {
  /**
   * 성실/불성실
   * @example 불성실
   */
  dishonestEvl?: string;

  /**
   * 이름(아이디)
   * @example 고길동(asdf1342)
   */
  nameId?: string;

  /**
   * 처리자
   * @example 홍길동(asdf1234)
   */
  operator?: string;

  /**
   * 포인트 상태
   * @example 적립 완료
   */
  pointSttusNm?: string;

  /**
   * 포인트
   * @example + 2,500
   */
  pointTxt?: string;

  /**
   * 사유
   * @example 출금 신청 완료
   */
  reason?: string;

  /**
   * 날짜
   * @example 2021.01.01 00:00:00
   */
  regDt?: string;

  /**
   * 영상/제품명
   * @example 포맨 남자 향수
   */
  reqestNm?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;
}

/**
 * 포인트 관리 > 포인트 적립/차감 Insert Request VO
 */
export interface AdminPointManageInsertRequest {
  /**
   * 사용자 id
   * @example 1000
   */
  memberIdArr?: number[];

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 포인트 종류 PAY : 결제, EVL : 평가, EVT : 이벤트, DEF : 출금, ADM : 관리자, DAY: 데일리평가
   * @example ADM
   */
  pointKnd?: string;

  /**
   * 포인트 상태 A: 적립, U: 차감
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;
}

/**
 * 포인트 마스터 Insert Response VO
 */
export interface AdminPointManageResponse {
  /**
   * 출금 완료 포인트
   * @example 30,000
   */
  accmltDefPoint?: string;

  /**
   * 생년월일
   * @format date
   * @example 1985-01-01
   */
  birthday?: string;

  /**
   * 이메일
   * @example example@blocksmith.xyz
   */
  email?: string;

  /**
   * 평가자 고유번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 010-4117-2114
   */
  mbtlnum?: string;

  /**
   * 의뢰자명
   * @example 강하늘
   */
  name?: string;

  /**
   * 최종 접속일
   * @example 2021-04-20 12:00:00
   */
  recentConectDt?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;

  /**
   * 성별명
   * @example M:남, F:여
   */
  sexdstnNm?: string;

  /**
   * 가입일
   * @example 2021-04-20 12:00:00
   */
  subscribeDt?: string;

  /**
   * 오늘 얻은 포인트
   * @example 30,000
   */
  todayAccmlPoint?: string;

  /**
   * 보유 포인트
   * @example 30,000
   */
  usePosblPoint?: string;

  /**
   * 로그인id
   * @example acbd123
   */
  username?: string;
}

export interface AlarmCntResponse {
  /**
   * 미확인 알림
   * @format int32
   * @example 5
   */
  alarmCnt?: number;
  error?: ErrorVo;
}

export interface AlarmSearchResponse {
  /**
   * 알림내용
   * @example 당신의 취미는?
   */
  alarmCn?: string;

  /**
   * 알림제목
   * @example 설문조사
   */
  alarmSubject?: string;

  /**
   * 확인여부
   * @example N
   */
  cnfirmYn?: string;

  /**
   * id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 링크여부
   * @example N
   */
  linkYn?: string;

  /**
   * 페이지url
   * @example www.naver.com
   */
  pageUrl?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;
}

export interface Alarms {
  /**
   * 비밀번호변경일 지남 (지났음:true, 안지났음:false)
   * @example false
   */
  nextPasswordChange?: boolean;

  /**
   * 승급 후 위촉계약서 다시 작성 필요 여부(승급 조건 충족:true, 충족하지 않음:false)
   * @example false
   */
  upgrade?: boolean;
}

/**
 * 질문 답변 정보
 */
export interface Answer {
  /**
   * 답변내용
   * @example 기분좋음
   */
  answerCn: string;

  /**
   * 파일아이디
   * @format int32
   * @example 1000000
   */
  attachFileId?: number;

  /**
   * n문항으로 이동 설정. qestnNo 값 입력
   * @format int32
   */
  skipToQestnNo?: number;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  sn: number;

  /**
   * 함정답변여부 (Y:함정질문, N:일반질문)
   * @example N
   */
  trapAnswerYn?: string;
}

export interface AnswerEntity {
  answerCn?: string;

  /** @format int32 */
  answerId?: number;

  /** @format int32 */
  attachFileId?: number;
  delYn?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnEntity?: QestnEntity;

  /** @format int32 */
  qestnId?: number;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;

  /** @format int32 */
  skipToQestnNo?: number;

  /** @format int32 */
  sn?: number;
  trapAnswerYn?: string;
}

export interface AnswerResponse {
  /**
   * 답 내용
   * @example ["답 보기1","답보기2"]
   */
  answerCn?: string;

  /**
   * 답 id
   * @format int32
   * @example [1,2,3]
   */
  answerId?: number;
}

export interface AttachFileInfResponse {
  /** @format int32 */
  attachFileIdF?: number;
  fileExt?: string;
  fileName?: string;
  fileUrl?: string;
}

/**
 * 첨부파일 상세조회 Response
 */
export interface AttachFileInfoResponse {
  /**
   * 변경 파일명
   * @example 3e09fe25-14c6-4bf4-8e40-3439e828f644.txt
   */
  fileChgName?: string;

  /**
   * 확장자
   * @example txt
   */
  fileExt?: string;

  /**
   * 파일명
   * @example test
   */
  fileName?: string;

  /**
   * 파일 물리경로
   * @example /assets/wewill/dev/upload/board/event/20210312
   */
  filePath?: string;

  /**
   * 파일사이즈
   * @format int32
   * @example 15
   */
  fileSize?: number;

  /**
   * 파일 url
   * @example https://cuspcdn.s3.ap-northeast-2.amazonaws.com/assets/wewill/dev/upload/board/20210312/3e09fe25-14c6-4bf4-8e40-3439e828f644.txt
   */
  fileUrl?: string;

  /**
   * 파일id
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface AttachFileReponse {
  /** @format int32 */
  attachFileIdF?: number;
  fileExt?: string;
  fileName?: string;
  fileSize?: string;
  fileUrl?: string;
}

/**
 * 국세청 홈택스 사업자등록번호 API 조회 Response Vo
 */
export interface BiznumResponse {
  /**
   * 사업자유형
   * @example Y : 등록되어 있는 사업자, N : 등록되어 있지 않은 사업자, C: 폐업자
   */
  businessExist?: string;

  /**
   * 폐업일자
   * @example 2021-02-01
   */
  closeDt?: string;
  error?: ErrorVo;

  /**
   * 홈택스로부터 응답받은 값 전체
   * @example <map id='' ><map id='resultMsg' ><detailMsg></detailMsg><msg></msg><code></code><result>S</result></map><trtEndCd>Y</trtEndCd><smpcBmanEnglTrtCntn>The business registration number is not registered(date of closure: 2018-02-28)</smpcBmanEnglTrtCntn><nrgtTxprYn>N</nrgtTxprYn><smpcBmanTrtCntn>등록되어 있지 않은 사업자등록번호 입니다. (폐업일자: 2018-02-28)</smpcBmanTrtCntn><trtCntn>폐업자 (과세유형: 부가가치세 일반과세자, 폐업일자:2018-02-28) 입니다.</trtCntn></map>
   */
  responseFullText?: string;

  /**
   * 과세유형
   * @example 1
   */
  taxType?: string;
}

export interface BlankResponse {
  error?: ErrorVo;
}

export interface ChartData {
  datasets?: ChartDataSet[];
  labels?: object[];
}

export interface ChartDataSet {
  backgroundColor?: string[];
  data?: number[];
}

/**
 * 욕설체크 Response
 */
export interface CheckBadWordsResponse {
  /**
   * 검출 된 욕설 목록
   * @example ["신발","황세준"]
   */
  badWordsList?: string[];
  error?: ErrorVo;
}

export interface CheckClientMemberResponse {
  error?: ErrorVo;

  /** @format int32 */
  id?: number;
}

/**
 * 의뢰자 쿠폰 Insert Request VO
 */
export interface ClientCouponInsertRequest {
  /**
   * 쿠폰 번호
   * @example ILOVEWEWILL
   */
  couponCode?: string;

  /**
   * 사용자ID
   * @format int32
   * @example 1
   */
  memberId?: number;
}

/**
 * 관리자 - 사용자 쿠폰 Insert Response VO
 */
export interface ClientCouponInsertResponse {
  /**
   * 쿠폰 고유번호
   * @format int32
   * @example 1
   */
  couponNo?: number;
  error?: ErrorVo;
}

/**
 * 사용자 보유 쿠폰 조회 Response VO
 */
export interface ClientCouponResponse {
  /**
   * 쿠폰코드
   * @example ILOVEWEWILL
   */
  couponCode?: string;

  /**
   * 쿠폰 할인 혜택
   * @example 30,000원 할인
   */
  couponDscnt?: string;

  /**
   * 쿠폰 할인 최대 금액
   * @example 최대 50,000원 할인
   */
  couponDscntLmt?: string;

  /**
   * 쿠폰명
   * @example 가입기념 쿠폰
   */
  couponName?: string;

  /**
   * 쿠폰상태(Y:사용 완료, N:사용 가능, E:기간 만료)
   * @example Y
   */
  couponSttus?: string;

  /**
   * 쿠폰 고유 번호(난수)
   * @example ABEL1X3BLEODFPAB10CKLPD
   */
  couponTy?: string;

  /**
   * 쿠폰 사용 가능 기간
   * @example 2021-04-21 12:00 ~ 2021-04-26 23:00
   */
  couponUsePeriod?: string;
  error?: ErrorVo;

  /**
   * 쿠폰 사용 시 최소 결제 금액
   * @example 100,000원 이상 결제 시
   */
  mummSetleAmount?: string;

  /**
   * 사용자 쿠폰 고유번호
   * @format int32
   * @example 1
   */
  no?: number;
}

/**
 * [의뢰자] - 서비스 소개서 관련 등록 Request
 */
export interface ClientIntroInsertRequest {
  /**
   * 활동분야 [CS008] - 001 : TV/방송 002 : 키즈/애니 003 : 음악/댄스 004 : 엔터테인먼트, 005 : 영화, 006 : 교양/정보/다큐, 007 : 예술/공연, 008 : 개인방송, 009 : 뷰티, 010 : 식품, 011 : 유아동, 012 : 펫용품, 013 : 패션잡화, 014 : 도서, 015 : 기타
   * @example 001
   */
  actRealm?: string;

  /**
   * 광고성정보수신약관동의여부
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 회사명
   * @example 블록스미스
   */
  cmpnyNm?: string;

  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;

  /**
   * 부서명
   * @example 마케팅 부서
   */
  fatherSign?: string;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 개인정보처리방침동의여부
   * @example Y
   */
  porsonalInfoAgreYn?: string;
}

/**
 * [의뢰자] - 서비스 소개서 관련 등록 Response
 */
export interface ClientIntroInsertResponse {
  /**
   * 활동분야 [CS008] - 001 : TV/방송 002 : 키즈/애니 003 : 음악/댄스 004 : 엔터테인먼트, 005 : 영화, 006 : 교양/정보/다큐, 007 : 예술/공연, 008 : 개인방송, 009 : 뷰티, 010 : 식품, 011 : 유아동, 012 : 펫용품, 013 : 패션잡화, 014 : 도서, 015 : 기타
   * @example 001
   */
  actRealm?: string;

  /**
   * 광고성정보수신약관동의여부
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /** 첨부 파일 */
  attachFileInfoResponse?: AttachFileInfoResponse;

  /**
   * 회사명
   * @example 블록스미스
   */
  cmpnyNm?: string;

  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 부서명
   * @example 마케팅 부서
   */
  fatherSign?: string;

  /**
   * 의뢰자 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 010-1234-5678
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  nm?: string;

  /**
   * 개인정보처리방침동의여부
   * @example Y
   */
  porsonalInfoAgreYn?: string;
}

export interface CodeGroupListResponse {
  /** 설명 */
  description?: string;

  /**
   * 코드그룹 명
   * @example CM001
   */
  groupNm?: string;

  /**
   * 코드그룹id
   * @example 1
   */
  id?: string;
}

export interface CodeGroupTreeResponse {
  /** 해당코드그룹 하위 코드목록 */
  codeList?: CodeListResponse[];

  /** 설명 */
  description?: string;

  /**
   * 코드그룹 명
   * @example CM001
   */
  groupNm?: string;

  /**
   * 코드그룹id
   * @example 1
   */
  id?: string;
}

export interface CodeListResponse {
  /**
   * 코드이름
   * @example 의뢰자
   */
  label?: string;

  /**
   * 코드ID
   * @example C
   */
  value?: string;
}

export interface CommonResetPasswordRequest {
  /**
   * 회원 아이디(통합회원쪽(accountId) id)
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 전송 이메일
   * @example test@test.com
   */
  to?: string;
}

export interface CommonUsernameCheckResponse {
  error?: ErrorVo;

  /**
   * 아이디 중복 여부(중복:true, 중복아님:false)
   * @example true
   */
  exist?: boolean;
}

/**
 * 콘텐츠 카드 등록 Request
 */
export interface ContentCardRequest {
  /**
   * 엔티티태그
   * @example alwasjkld214jkladklq
   */
  etag: string;

  /**
   * 파일명
   * @example video.mp4
   */
  fileName: string;
}

/**
 * 콘텐츠 카드 등록 Response
 */
export interface ContentCardResponse {
  /**
   * 컨텐츠카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;
  error?: ErrorVo;

  /**
   * 트랜스코딩 예상시간(초)
   * @format int32
   * @example 120
   */
  estimatedSeconds?: number;

  /**
   * 트랜스코딩 예상시간(타임형식)
   * @example 00:02:00
   */
  estimatedTime?: string;

  /**
   * 엔티티태그
   * @example alwasjkld214jkladklq
   */
  etag: string;

  /**
   * 파일명
   * @example video.mp4
   */
  fileName: string;

  /** 미디어워크플로우 정보 수신 웹소켓 정보 */
  websocketInfo?: WebSocketVo;
}

/**
 * 통합컨텐츠 영상 스트리밍정보 상세조회 Response
 */
export interface ContentCardVidoResponse {
  /**
   * content_card_no
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * cmaf엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/f6a738caadd041ee81b956d014cfa67c/27f9d779da5f4b85a5c0732cae6d2a54/index.m3u8
   */
  egressEndpointCmaf?: string;

  /**
   * dash엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/7c033e24f8c54d13833bd902b9d22c33/c916a709ea62459abe2b7b4fb25b9b86/index.mpd
   */
  egressEndpointDash?: string;

  /**
   * hls엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/32a0e4f73a8b402f9e84d3350da33474/a3c862e9dd854f8ea6fb18b7959fe254/index.m3u8
   */
  egressEndpointHls?: string;

  /**
   * mss엔드포인트
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/3bac3d3f4d834406befd4b03a7441bd9/961e172ab1f844209b1a9afd70be9bc1/78623a75eec14c74861a821dd3022395/index.ism/Manifest
   */
  egressEndpointMss?: string;
  error?: ErrorVo;

  /**
   * 파일명
   * @example REPCircle_Presentation17.mp4
   */
  fileNm?: string;

  /**
   * 썸네일S3경로. 콤마구분
   * @example s3://cuspvideoservice-destination-m98dshvs1el8/0f8a927c-594b-4cd6-984e-df6ae7b4b96f/thumbnails/REPCircle_Presentation16_thumb.0000007.jpg
   */
  thumbNails?: string;

  /**
   * 썸네일 웹경로. 콤마구분
   * @example https://d1dopqicmcx111.cloudfront.net/0f8a927c-594b-4cd6-984e-df6ae7b4b96f/thumbnails/REPCircle_Presentation16_thumb.0000007.jpg
   */
  thumbNailsUrls?: string;
}

/**
 * 쿠폰 할인 적용 조회 Response VO
 */
export interface CouponDscntCalcResponse {
  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 금액 텍스트
   * @example 8,000원
   */
  couponDscntAmountTxt?: string;

  /**
   * 쿠폰명
   * @example 네이버 광고 기념 할인 쿠폰
   */
  couponName?: string;
  error?: ErrorVo;

  /**
   * 결제 금액
   * @format int32
   * @example 1400000
   */
  setleAmount?: number;

  /**
   * 결제 금액 텍스트
   * @example 8,000원
   */
  setleAmountTxt?: string;

  /**
   * 공급가액
   * @format int32
   * @example 1280000
   */
  splpcAm?: number;

  /**
   * 부가가치세
   * @format int32
   * @example 128000
   */
  vat?: number;
}

export interface CustomerFileResponse {
  /** @format int32 */
  attachFileId?: number;

  /** @format int32 */
  no?: number;
  se?: string;
}

/**
 * 상세조회 Response Vo
 */
export interface CustomerInfoResponse {
  /**
   * 답변내용
   * @example 답변입니다
   */
  answerCn?: string;

  /**
   * 답변제목
   * @example 답변 제목입니다.
   */
  answerSj?: string;

  /**
   * 문의하기 attach file List
   * @example []
   */
  attachFileList?: AttachFileReponse[];

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 문의하기 file List
   * @example []
   */
  customerFileList?: CustomerFileResponse[];
  error?: ErrorVo;

  /**
   * 자신유무
   * @example Y:자기자신일치,N:자기자신불일치
   */
  flag?: string;

  /** @format int32 */
  id?: number;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 답변완료일시
   * @example 2021-02-02 10:20:30
   */
  processComptDt?: string;

  /**
   * 등록일시
   * @example 2021-02-02 10:20:30
   */
  regDt?: string;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 선택한 의뢰 제목
   * @example 독립영화 평가 의뢰
   */
  reqestSj?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중,  40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 제목입니다
   */
  subject?: string;
}

/**
 * 질문하기 Request Vo
 */
export interface CustomerInsertQuestionRequest {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 내용
   * @example 질문 내용입니다
   */
  cn?: string;

  /**
   * cs카테고리대 ([csoo3] 001:의뢰자, 002:평가자
   * @example 001
   */
  csCategoryLrge?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 제목
   * @example 질문합니다
   */
  subject?: string;
}

/**
 * 질문하기 Request Vo
 */
export interface CustomerInsertQuestionResponse {
  /**
   * 첨부파일 ID
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 내용
   * @example 질문 내용입니다
   */
  cn?: string;

  /**
   * cs카테고리대 ([csoo3] 001:의뢰자, 002:평가자
   * @example 001
   */
  csCategoryLrge?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * cs형태
   * @example 003
   */
  csStle?: string;

  /**
   * 고객센터id
   * @format int32
   * @example 1
   */
  customerServiceId?: number;
  error?: ErrorVo;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 질문합니다
   */
  subject?: string;
}

/**
 * 질문 수정 Request Vo
 */
export interface CustomerModifyQuestionRequest {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 제목
   * @example 제목
   */
  subject?: string;
}

/**
 * 질문 수정 Response Vo
 */
export interface CustomerModifyQuestionResponse {
  /**
   * 첨부파일 ID
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 내용
   * @example 내용입니다
   */
  cn?: string;

  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;
  error?: ErrorVo;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 선택한 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중,  40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 제목
   */
  subject?: string;
}

export interface CustomerSearchResponse {
  /**
   * cs카테고리중 ([cs004] 001:회원정보, 002:출금관련, 003:기타문의)
   * @example 001
   */
  csCategoryMiddl?: string;

  /** @format int32 */
  id?: number;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 접수 일시
   * @example 2021-04-01
   */
  rceptDt?: string;

  /**
   * 상태 10:미확인, 20:확인, 30:처리중, 40:처리완료
   * @example 10
   */
  status?: string;

  /**
   * 제목
   * @example 문의하기 제목
   */
  subject?: string;
}

export interface DailyBadgeResponse {
  /**
   * 데일리 설문 체크(뱃지) Y:설문완료,N:설문미완료
   * @example Y
   */
  check?: string;
  error?: ErrorVo;
}

export interface DailyQuestionInfoResponse {
  /**
   * 답변 List
   * @example [{"answerId":775,"answerCn":"데일리 답변1"},{"answerId":776,"answerCn":"데일리 답변2"}]
   */
  answerResponseList?: AnswerResponse[];
  error?: ErrorVo;

  /**
   * 포인트
   * @example 10
   */
  point?: string;

  /**
   * 질문내용
   * @example 데일리 질문합니다
   */
  qestnCn?: string;
}

export interface DailyQuestionInsertRequest {
  /**
   * 답 id
   * @example [1,2,3]
   */
  answerId?: number[];
}

export interface DashBoardChartDateSetSpec {
  data?: number[];
}

export interface DashBoardChartSpec {
  datasets?: DashBoardChartDateSetSpec[];
  labels?: string[];
}

export interface DeclarationInsertRequest {
  /**
   * 신고내용
   * @example 테러의 의심이 판단되어 신고 합니다.
   */
  declarationCn?: string;

  /**
   * 신고유형 001:선정적인 콘텐츠 002:폭력성 콘텐츠 003:유해하거나 위험한 콘텐츠 004:권리 침해 005:아동학대 006:테러조장
   * @example 006:테러조장
   */
  declarationTy?: string;

  /**
   * 의뢰id
   * @format int32
   * @example 5
   */
  reqestId?: number;

  /**
   * 영상신고시점(분,초)
   * @example 15:00
   */
  vidoDeclarationPnttm?: string;
}

export interface DeclarationInsertResponse {
  /**
   * 신고내용
   * @example 테러의 의심이 판단되어 신고 합니다.
   */
  declarationCn?: string;

  /**
   * 신고유형 001:선정적인 콘텐츠 002:폭력성 콘텐츠 003:유해하거나 위험한 콘텐츠 004:권리 침해 005:아동학대 006:테러조장
   * @example 006
   */
  declarationTy?: string;
  error?: ErrorVo;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /** @format int32 */
  no?: number;

  /**
   * 의뢰id
   * @format int32
   * @example 5
   */
  reqestId?: number;

  /**
   * 영상신고시점(분,초)
   * @example 15:00
   */
  vidoDeclarationPnttm?: string;
}

/**
 * 미디어 컨버터 오류 결과 상세
 */
export interface Detail {
  /**
   * AWS계정ID
   * @example 745109727851
   */
  accountId?: string;

  /**
   * 오류코드
   * @format int32
   * @example 1040
   */
  errorCode?: number;

  /**
   * 오류메세지
   * @example Invalid audio track specified for audio_selector [1]. Audio track [1] not found in input container.
   */
  errorMessage?: string;

  /**
   * 작업ID
   * @example 1622298028173-ccztlo
   */
  jobId?: string;

  /**
   * 큐 ARN정보
   * @example arn:aws:mediaconvert:ap-northeast-2:745109727851:queues/Default
   */
  queue?: string;

  /**
   * 상태
   * @example ERROR
   */
  status?: string;

  /**
   * timestamp
   * @format int64
   * @example 1622298032017
   */
  timestamp?: number;

  /**
   * 사용자메타데이터
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  userMetadata?: UserMetadata;
}

export interface ErrorVo {
  /**
   * 성공/에러코드
   * @example 00
   */
  code?: string;

  /** 에러의 경우 에러메시지 */
  message?: string;

  /**
   * 메세지 코드
   * @example ERROR.SYS.999
   */
  messageCode?: string;
}

export interface EssentialVideoModifyRequest {
  /**
   * 필수영상시청진행률%
   * @format int32
   * @example 30
   */
  essntlVidoCtyhllProgrsRt?: number;
}

export interface EssentialVideoModifyResponse {
  error?: ErrorVo;

  /**
   * 필수영상시청진행률%
   * @format int32
   * @example 30
   */
  essntlVidoCtyhllProgrsRt?: number;

  /**
   * 필수영상시청여부
   * @example N
   */
  essntlVidoCtyhllYn?: string;

  /**
   * 등급
   * @example CR:We_Crowd, AH:We_Ahead, SP:We_SamiPro, PR:We_Pro
   */
  grad?: string;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;
}

export interface EssentialVideoSearchResponse {
  /** 필수영상no */
  contentCardNo?: string;
  error?: ErrorVo;

  /**
   * 필수영상시청진행률%
   * @format int32
   * @example 30
   */
  essntlVidoCtyhllProgrsRt?: number;

  /**
   * 필수영상시청여부
   * @example N
   */
  essntlVidoCtyhllYn?: string;

  /**
   * 회원id
   * @format int32
   * @example 1
   */
  memberId?: number;
}

export interface File {
  absolute?: boolean;
  absoluteFile?: File;
  absolutePath?: string;
  canonicalFile?: File;
  canonicalPath?: string;
  directory?: boolean;
  executable?: boolean;
  file?: boolean;

  /** @format int64 */
  freeSpace?: number;
  hidden?: boolean;

  /** @format int64 */
  lastModified?: number;
  name?: string;
  parent?: string;
  parentFile?: File;
  path?: string;
  readable?: boolean;

  /** @format int64 */
  totalSpace?: number;

  /** @format int64 */
  usableSpace?: number;
  writable?: boolean;
}

export interface FindIdByMobileRequest {
  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 국가코드
   * @example +82
   */
  nationCode?: string;
}

export interface FindPwdByMobileRequest {
  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 국가코드
   * @example +82
   */
  nationCode?: string;

  /**
   * 아이디
   * @example blocksmith
   */
  username?: string;
}

/**
 * Import 결제 완료 Response Vo
 */
export interface IfImportPayCompleteResponse {
  error?: ErrorVo;

  /**
   * 결제반환메시지
   * @example 일반 결제 성공
   */
  message?: string;

  /**
   * 결제반환상태
   * @example success
   */
  status?: string;
}

/**
 * Import 결제 완료 Response Vo
 */
export interface IfImportPayCompleteVbankResponse {
  /**
   * 금액 - 공급가액
   * @format int32
   * @example 1280000
   */
  amount?: number;

  /**
   * 금액 - 공급가액 포멧
   * @example 1,280,000
   */
  amountFormat?: string;

  /**
   * 현금영수증발급 여부
   * @example true
   */
  cashReceiptIssued?: boolean;

  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 금액 포멧
   * @example 8,000
   */
  couponDscntAmountFormat?: string;
  error?: ErrorVo;

  /**
   * 결제반환메시지
   * @example 일반 결제 성공
   */
  message?: string;

  /**
   * 결제(주문)번호
   * @example 20210428-212018-6
   */
  no?: string;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌, memor:수기결제
   * @example vbank
   */
  setleKnd?: string;

  /**
   * 결제종류코드명
   * @example 가상계좌
   */
  setleKndNm?: string;

  /**
   * 결제 금액 합계
   * @format int32
   * @example 1400000
   */
  setleReqestAmount?: number;

  /**
   * 결제 금액 합계 포멧
   * @example 1,400,000
   */
  setleReqestAmountFormat?: string;

  /**
   * 부가세
   * @format int32
   * @example 128000
   */
  vat?: number;

  /**
   * 부가세 포멧
   * @example 12,8000
   */
  vatFormat?: string;

  /**
   * 가상계좌 은행 표준코드 - (금융결제원기준)
   * @example 088
   */
  vbankCode?: string;

  /**
   * 입금받을 가상계좌 마감기한
   * @format date-time
   * @example 1625826618
   */
  vbankDt?: string;

  /**
   * 입금받을 가상계좌 예금주
   * @example （주）케이지이니시
   */
  vbankHolder?: string;

  /**
   * 입금받을 가상계좌 은행명
   * @example 신한(조흥)은행
   */
  vbankName?: string;

  /**
   * 입금받을 가상계좌 계좌번호
   * @example 56211102580756
   */
  vbankNum?: string;

  /**
   * 결제반환상태
   * @example success
   */
  zxc?: string;
}

/**
 * Import 결제 요청 정보 생성 Response Vo
 */
export interface IfImportPayGenerateResponse {
  error?: ErrorVo;

  /**
   * 결제아이디
   * @example f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d
   */
  merchantUid?: string;

  /**
   * 가상계좌입금기한
   * @example 20210712190000
   */
  vbankDue?: string;
}

export interface ImageAttachFileInfoResponse {
  /** @format int32 */
  iattachFileId?: number;
  ifileExt?: string;
  ifileName?: string;
  ifileUrl?: string;
}

export interface ImageFileInfoResponse {
  /** @format int32 */
  attachFileIdIF?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  prductId?: number;
}

export type InputStream = object;

export interface InsertReqestEstimateResponse {
  /**
   * 자동산출여부
   * @example Y/N
   */
  atmcPrdctnYn?: string;
  error?: ErrorVo;

  /**
   * 견적서 id
   * @format int32
   * @example 1
   */
  prqudoId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;
}

export interface Legend {
  /** lebels */
  labels?: string[];
}

export interface LoginRequest {
  /**
   * 비밀번호
   * @example Cusp1234!@
   */
  password?: string;

  /**
   * 서비스[W:wewill, F:Fillin]
   * @example W
   */
  service?: string;

  /**
   * 사용자 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface LoginResponse {
  /**
   * access token
   * @example skjfskldjf-sdf
   */
  accessToken?: string;
  error?: ErrorVo;

  /**
   * refresh token
   * @example skjfskldjf-sdf
   */
  refreshToken?: string;

  /**
   * 서비스 종류(E:평가자 C:의뢰자)
   * @example E
   */
  service?: string;
}

/**
 * 미디어 컨버터 오류 결과
 */
export interface MediaConvertErrorResult {
  /**
   * AWS계정ID
   * @example 745109727851
   */
  account?: string;

  /**
   * 오류 상세
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  detail?: Detail;

  /**
   * 상세타입
   * @example MediaConvert Job State Change
   */
  "detail-type"?: string;

  /**
   * id
   * @example 5a0c3d8c-c9c6-4b18-f796-16fee7540ba4
   */
  id?: string;

  /**
   * 리전
   * @example ap-northeast-2
   */
  region?: string;

  /**
   * 리소스 ARN정보
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  resources?: string[];

  /**
   * 원본
   * @example aws.mediaconvert
   */
  source?: string;

  /**
   * 일시
   * @format date-time
   * @example 2021-05-29T14:20:32Z
   */
  time?: string;

  /**
   * 미디어컨버터 버전
   * @example 0
   */
  version?: string;
}

/**
 * 미디어 워크플로우 결과
 */
export interface MediaWorkflowResultResponse {
  /**
   * acceleratedTranscoding
   * @example PREFERRED
   */
  acceleratedTranscoding?: string;

  /**
   * archiveSource
   * @example GLACIER
   */
  archiveSource?: string;

  /**
   * encodeJobId
   * @format int32
   */
  cardNo?: number;

  /**
   * cloudFront
   * @example d1dopqicmcx111.cloudfront.net
   */
  cloudFront?: string;

  /**
   * customTemplate
   * @example true
   */
  customTemplate?: boolean;

  /**
   * destBucket
   * @example cuspvideoservice-destination-m98dshvs1el8
   */
  destBucket?: string;

  /**
   * egressEndpointCmaf
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/f6a738caadd041ee81b956d014cfa67c/27f9d779da5f4b85a5c0732cae6d2a54/index.m3u8
   */
  egressEndpointCmaf?: string;

  /**
   * egressEndpointDash
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/7c033e24f8c54d13833bd902b9d22c33/c916a709ea62459abe2b7b4fb25b9b86/index.mpd
   */
  egressEndpointDash?: string;

  /**
   * egressEndpointHls
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/32a0e4f73a8b402f9e84d3350da33474/a3c862e9dd854f8ea6fb18b7959fe254/index.m3u8
   */
  egressEndpointHls?: string;

  /**
   * egressEndpointMss
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/961e172ab1f844209b1a9afd70be9bc1/78623a75eec14c74861a821dd3022395/index.ism/Manifest
   */
  egressEndpointMss?: string;

  /** enableMediaPackage */
  enableMediaPackage?: boolean;

  /**
   * enableSns
   * @example true
   */
  enableSns?: boolean;

  /**
   * enableSqs
   * @example true
   */
  enableSqs?: boolean;

  /**
   * encodeJobId
   * @example 1621924386435-648wcn
   */
  encodeJobId?: string;

  /**
   * encodingProfile
   * @format int32
   */
  encodingProfile?: number;

  /**
   * endTime
   * @example 2021-05-25T06:32:55.283Z
   */
  endTime?: string;
  error?: ErrorVo;

  /** 워크플로우 오류 정보 */
  errorResult?: MediaConvertErrorResult;

  /**
   * frameCapture
   * @example true
   */
  frameCapture?: boolean;

  /**
   * frameCaptureHeight
   * @format int32
   */
  frameCaptureHeight?: number;

  /**
   * frameCaptureWidth
   * @format int32
   * @example 1280
   */
  frameCaptureWidth?: number;

  /**
   * inputRotate
   * @example DEGREE_0
   */
  inputRotate?: string;

  /**
   * jobTemplate
   * @example cuspvideoservice_Ott_720p_Avc_Aac_16x9_mvod_no_preset
   */
  jobTemplate?: string;

  /** mediaPackageResourceId */
  mediaPackageResourceId?: string;

  /**
   * srcBucket
   * @example cuspvideoservice-source-z8k6fz4eo0e4
   */
  srcBucket?: string;

  /**
   * srcHeight
   * @format int32
   * @example 420
   */
  srcHeight?: number;

  /**
   * 원본영상 파일명
   * @example test.mp4
   */
  srcVideo?: string;

  /**
   * srcWidth
   * @format int32
   * @example 630
   */
  srcWidth?: number;

  /**
   * startTime
   * @example 2021-05-25T06:32:55.283Z
   */
  startTime?: string;

  /**
   * S3썸네일정보 (,)콤마구분
   * @example a.jpg, b.jpg
   */
  thumbNails?: string;

  /**
   * S3썸네일URL (,)콤마구분
   * @example https://cuspcdn.s3.ap-northeast-2.amazonaws.com/assets/wewill/dev/upload/customer/20210312/37f9d333-58e0-4dbb-b19c-b7a22d455039.jpg, https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/sample_do_not_delete/%EA%B0%95%EC%95%84%EC%A7%80.jpg
   */
  thumbNailsUrls?: string;

  /**
   * workflowName
   * @example cuspvideoservice
   */
  workflowName?: string;

  /**
   * workflowStatus
   * @example Compelete
   */
  workflowStatus?: string;

  /**
   * workflowTrigger
   * @example Video
   */
  workflowTrigger?: string;
}

export interface MemberidPointSumInterface {
  /** @format int32 */
  member_id?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  point?: number;

  /** @format int32 */
  save_no?: number;
}

export interface ModifyPasswordByMobileRequest {
  /**
   * code
   * @example 123532
   */
  code?: string;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * message id
   * @example 1q2w3e
   */
  messageId?: string;

  /**
   * 새 비밀번호
   * @example 1q2w3e
   */
  newPassword?: string;

  /**
   * 사용자 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface NonCustomerInsertRequest {
  /**
   * 회사명
   * @example 블록스미스
   */
  cmpnyNm?: string;

  /**
   * 연락처
   * @example 01033334444
   */
  cttpc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;

  /**
   * 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;

  /**
   * 부서명
   * @example 개발
   */
  fatherSign?: string;

  /**
   * 업종
   * @example 영업
   */
  induty?: string;

  /**
   * 문의내용
   * @example 문의 합니다
   */
  inqryCn?: string;

  /**
   * 이름
   * @example 블록스미스
   */
  nm?: string;

  /**
   * 개인정보처리방침동의여부
   * @example Y
   */
  porsonalInfoAgreYn?: string;

  /**
   * 구분([CM001] C:의뢰자,E:평가자
   * @example C
   */
  section?: string;
}

export interface NonCustomerInsertResponse {
  /**
   * 회사명
   * @example 블록스미스
   */
  cmpnyNm?: string;

  /**
   * 연락처
   * @example 01033334444
   */
  cttpc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;

  /**
   * 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 부서명
   * @example 개발
   */
  fatherSign?: string;

  /** @format int32 */
  id?: number;

  /**
   * 업종
   * @example 영업
   */
  induty?: string;

  /**
   * 문의내용
   * @example 문의 합니다
   */
  inqryCn?: string;

  /**
   * 이름
   * @example 블록스미스
   */
  nm?: string;

  /**
   * 개인정보처리방침동의여부
   * @example Y
   */
  porsonalInfoAgreYn?: string;

  /**
   * 등록자
   * @format int32
   * @example 1
   */
  regId?: number;

  /**
   * 구분([CM001] C:의뢰자,E:평가자
   * @example C
   */
  section?: string;
}

export interface NotificationMessage {
  /** Json 형식의 스트링 */
  data?: string;

  /**
   * 내부,외부 여부(외부 : true , 내부: false)
   * @example false
   */
  isExternal?: boolean;

  /**
   * 새창 여부(새창 : true , 아닐경우: false)
   * @example false
   */
  isNewWindow?: boolean;

  /**
   * Link URL
   * @example http://test.com
   */
  linkUrl?: string;

  /**
   * message 내용 json + uri인코딩
   * @example 메시지 내용
   */
  message?: string;

  /**
   * message Type( N: 일반, L:링크)
   * @example N
   */
  messageType?: "N" | "L";

  /**
   * message Target(A: 전체에게 전송 , U: 개인에게 전송)
   * @example U
   */
  targetType?: "A" | "U";

  /**
   * message 받을 로그인 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface Options {
  /** 차트 옵션 legend */
  legend?: Legend;
}

/**
 * 의뢰 콘텐츠 설정 (Youtube & 영상) 등록 Request
 */
export interface OrderMergeRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (Youtube & 영상) 등록 Response
 */
export interface OrderMergeResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 제품의뢰 사용 전 설문 등록 :: 저장 Request
 */
export interface OrderProductSurveyStep1InsertRequest {
  /** 사용 전 의뢰설문지 정보 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 제품의뢰 사용 중 설문 설문 등록 :: 저장 Request
 */
export interface OrderProductSurveyStep2InsertRequest {
  /** 사용 중 의뢰설문지 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 제품의뢰 사용 후 설문 등록 :: 저장 Request
 */
export interface OrderProductSurveyStep3InsertRequest {
  /** 사용 후 의뢰설문지 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 의뢰반려사유 조회 Response
 */
export interface OrderReturnResnResponse {
  error?: ErrorVo;

  /**
   * return_resn
   * @example 반려사유
   */
  returnResn?: string;
}

/**
 * 의뢰 설정 상세조회 Response
 */
export interface OrderSetInfoResponse {
  /**
   * 연령 (ex. 20,30,40)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트
   */
  comment?: string;
  error?: ErrorVo;

  /**
   * 평가자구성 ([IN005] 001:균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 성별([IN004] A: 전체, M:남성, F:여성)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 의뢰 설문 설정 (평가자설정) 수정 Request
 */
export interface OrderSetModifyRequest {
  /**
   * 연령 (ex. 20,30,40)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트
   */
  comment?: string;

  /**
   * 평가자구성 ([IN005] 001:균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 성별([IN004] A: 전체, M:남성, F:여성)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 의뢰 설문 설정 (평가자설정) 수정 Response
 */
export interface OrderSetModifyResponse {
  /**
   * 연령 (ex. 20,30,40)
   * @example 20,30
   */
  age?: string;

  /**
   * 의뢰코멘트
   * @example 의뢰코멘트
   */
  comment?: string;
  error?: ErrorVo;

  /**
   * 평가자구성 ([IN005] 001:균등분포)
   * @example 001
   */
  evaluatorComposition?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 성별([IN004] A: 전체, M:남성, F:여성)
   * @example M
   */
  sexdstn?: string;
}

/**
 * 의뢰 신청 주문/결제 관련 상세조회 Response
 */
export interface OrderSettlementInfoResponse {
  /**
   * 사업자등록증 등록 여부 ( Y: 등록, N: 미등록)
   * @example N
   */
  bizContractYn?: string;

  /**
   * 개인/사업자구분 (P:개인, C:사업자)
   * @example P
   */
  bizSection?: string;
  error?: ErrorVo;

  /**
   * 견적서id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 의뢰콘텐츠 ([IN002] V:영상, P:제품)
   * @example V
   */
  reqestCntnts?: string;

  /**
   * 의뢰콘텐츠 ([IN002] V:영상, P:제품)
   * @example 영상
   */
  reqestCntntsNm?: string;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example Chanel F/W 2021
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example premium
   */
  reqestTyNm?: string;

  /**
   * 결제 요청 금액
   * @format int32
   * @example 36000
   */
  setleReqestAmount?: number;

  /**
   * 결제 요청 금액
   * @example 36,000원
   */
  setleReqestAmountTxt?: string;

  /**
   * 공급가액
   * @format int32
   * @example 360000
   */
  splpcAm?: number;

  /**
   * 공급가액 텍스트
   * @example 360,000원
   */
  splpcAmTxt?: string;

  /**
   * 부가세
   * @format int32
   * @example 36000
   */
  vat?: number;

  /**
   * 부가세 텍스트
   * @example 36,000원
   */
  vatTxt?: string;
}

/**
 * 견적서 요청 등록 :: 저장 Request
 */
export interface OrderSurveyFinalInsertRequest {
  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId: number;
}

/**
 * 의뢰 설문 일괄 등록 :: 저장 Request
 */
export interface OrderSurveyInsertAllRequest {
  /** 의뢰 설문지 목록 */
  requestSurveyList?: ReqestQestnr[];
}

/**
 * 설문일괄 등록 :: 저장 Response
 */
export interface OrderSurveyInsertAllResponse {
  error?: ErrorVo;

  /** 생성 된 의뢰 설문지 목록 */
  orderSurveyInsertResponseList?: OrderSurveyInsertResponse[];
}

/**
 * 의뢰 사전 설문 등록 :: 저장 Response
 */
export interface OrderSurveyInsertResponse {
  error?: ErrorVo;

  /** 생성 된 설문지 정보 */
  qestnrEntity?: QestnrEntity;

  /** 생성 된 의뢰 설문지 정보 */
  reqestQestnrEntity?: ReqestQestnrEntity;
}

/**
 * 의뢰 설문 설정 (영상) 프리미엄 정책데이터 Response
 */
export interface OrderSurveyPolicyResponse {
  /**
   * 최종평가 영상 출연자 평가의 설문지 ID
   * @format int32
   * @example -2
   */
  actorQestnrId?: number;
  error?: ErrorVo;

  /**
   * 등록 가능한 질문 최대 개수
   * @format int32
   * @example 39
   */
  maxQuestionCnt?: number;

  /**
   * 질문형식 옵션 목록 (single:객관식 질문 (단일 응답), multi:객관식 질문 (복수 응답), short:주관식 질문, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지답변)
   * @example 1
   */
  questionSelectOptions?: SelectOptionsVo[];

  /**
   * 시분초 타임형식 패턴
   * @example ^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$
   */
  timePattern?: string;

  /**
   * 함정답변 목록
   * @example ["꼬부기","이상해씨","파이리"]
   */
  trapAnswerList?: string[];

  /**
   * 최종평가 영상평가의 설문지 ID
   * @format int32
   * @example -1
   */
  videoQestnrId?: number;
}

/**
 * 의뢰 사전 설문 등록 :: 저장 Request
 */
export interface OrderSurveyStep1InsertRequest {
  /** 의뢰설문지 정보 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 의뢰 실시간 설문 등록 :: 저장 Request
 */
export interface OrderSurveyStep2InsertRequest {
  /** 실시간 의뢰설문지 목록 */
  requestSurveyList?: ReqestQestnr[];
}

/**
 * 의뢰 실시간 설문목록 등록 :: 저장 Response
 */
export interface OrderSurveyStep2InsertResponse {
  error?: ErrorVo;

  /** 생성 된 실시간 설문지 목록 */
  orderSurveyInsertResponseList?: OrderSurveyInsertResponse[];
}

/**
 * 의뢰 사후 설문 등록 :: 저장 Request
 */
export interface OrderSurveyStep3InsertRequest {
  /** 의뢰설문지 정보 */
  requestSurvey?: ReqestQestnr;
}

/**
 * 최종평가 설문 등록 :: 저장/임시저장 Request
 */
export interface OrderSurveyStep4InsertRequest {
  /**
   * 설문지id 고정값 (-1:영상평가, -2:영상출연자평가)
   * @format int32
   * @example 1
   */
  qestnrId: number;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId: number;

  /**
   * 영상id - 제품의 경우 입력 불필요
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

/**
 * 의뢰 콘텐츠 설정 상세 Response
 */
export interface OrderVidoInfoResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 업로드 완료여부
   * @example Y
   */
  uploadCompleteYn?: string;

  /** 업로드 파일명 */
  uploadFileNm?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y","fileName":"도넛","fileUrl":"https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/board/event/20210423/59de7024-c5c1-47de-9224-f72b2fa4de2a.png"},{"attachFileId":2,"useYn":"N","fileName":"도넛2","fileUrl":"https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/board/event/20210423/59de7024-c5c1-47de-9224-f72b2fa4de2a.png"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailInfo[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 등록 Request
 */
export interface OrderVidoInsertRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 등록 Response
 */
export interface OrderVidoInsertResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 삭제여부
   * @example N
   */
  delYn?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 수정 Request
 */
export interface OrderVidoModifyRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (영상) 수정 Response
 */
export interface OrderVidoModifyResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 통합컨텐츠 카드번호
   * @format int32
   * @example 1
   */
  contentCardNo?: number;

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example UL
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브) 등록 Request
 */
export interface OrderYoutubeInsertRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브) 등록 Response
 */
export interface OrderYoutubeInsertResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example P
   */
  reqestTy?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브)등록 Request
 */
export interface OrderYoutubeModifyRequest {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 의뢰 콘텐츠 설정 (유투브)수정 Response
 */
export interface OrderYoutubeModifyResponse {
  /**
   * 영상카테고리
   * @example [1,2]
   */
  categoryIdList?: number[];

  /**
   * 설명
   * @example 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 동영상채널url
   * @example https://www.youtube.com/channel/UCFn-bZIsdwS92AMCufEer9Q
   */
  mvpChnnelUrl?: string;

  /**
   * 영상제목
   * @example 영상제목
   */
  mvpSubject?: string;

  /**
   * 동영상url
   * @example https://youtu.be/_sQQpQlzKzo
   */
  mvpUrl?: string;

  /**
   * 영상다시보기유무
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 영상형태 (LK:Youtube, UL:영상 업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상재생시간
   * @format int32
   * @example 100
   */
  vidoRevivTime?: number;

  /**
   * 썸네일정보
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  vidoThumbnailInfoList?: VidoThumbnailMerge[];
}

/**
 * 결제 패키지 등록 Response Vo
 */
export interface PackageResponse {
  /**
   * 서비스기간 종료일
   * @example 2020-07-27
   */
  endDate?: string;
  error?: ErrorVo;

  /**
   * 주문번호
   * @example 20200627-123456-1123
   */
  paymentCode?: string;

  /**
   * 결제일
   * @format date-time
   * @example 2020-06-25 22:15:00
   */
  paymentDate?: string;

  /**
   * 결제일 포멧팅
   * @example 06.25 22:15
   */
  paymentDateStr?: string;

  /**
   * 상점 시퀀스
   * @format int32
   * @example 22
   */
  shopSeq?: number;

  /**
   * 서비스기간 시작일
   * @example 2020-06-27
   */
  startDate?: string;
}

/**
 * 결제결과 Vo
 */
export interface PaymentResult {
  /**
   * 금액
   * @format int32
   * @example 10000
   */
  amount?: number;

  /**
   * 결제아이디
   * @example f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d
   */
  merchantUid?: string;

  /**
   * 결제번호
   * @example 20200627-123456-1123
   */
  paymentCode?: string;

  /**
   * 결제일
   * @format date-time
   * @example 2020-06-25 22:15:00
   */
  paymentDate?: string;

  /**
   * 결제일 포멧팅
   * @example 06.25 22:15
   */
  paymentDateStr?: string;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌
   * @example 1
   */
  paymentMethod?: string;

  /**
   * 결제종류이름
   * @example 신용카드
   */
  paymentMethodName?: string;

  /**
   * 결제상태. ready:미결제, paid:결제완료, cancelled:결제취소, failed:결제실패 = ['ready', 'paid', 'cancelled', 'failed']
   * @example paid
   */
  paymentStatus?: string;

  /**
   * 결제상태명
   * @example 결제완료
   */
  paymentStatusName?: string;
}

/**
 * 포인트 출금 신청 상세 Response VO
 */
export interface PointDefraymentInfoResponse {
  /**
   * 계좌번호
   * @example 092-21-0222-243
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울 강남구 도산대로8길 17-11
   */
  address?: string;

  /**
   * 아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 이름
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 은행명
   * @example 국민은행
   */
  bankNm?: string;

  /**
   * 담당자
   * @example 차명환
   */
  chargerNm?: string;

  /**
   * 상태 (00:출금신청, 10:출금완료, 20:출금반려)
   * @example 00
   */
  defraySttus?: string;

  /**
   * 상태 (00:출금신청, 10:출금완료, 20:출금반려)
   * @example 출금신청
   */
  defraySttusNm?: string;

  /**
   * 상세주소
   * @example 2층 블록스미스
   */
  detailaddress?: string;

  /**
   * 이메일
   * @example hjyoon@blocksmith.xyz
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 전체 주소
   * @example (06039) 서울 강남구 도산대로8길 17-11 2층 블록스미스
   */
  fullAddress?: string;

  /**
   * 회원등급
   * @example W1
   */
  grad?: string;

  /**
   * 보유포인트
   * @example 150,000
   */
  holdPoint?: string;

  /**
   * 포인트 출금 신청 고유 번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 식별자
   * @example 891012-1231231
   */
  identifier?: string;

  /**
   * 연락처
   * @example 010-3333-4444
   */
  mbtlnum?: string;

  /**
   * 처리일
   * @format date
   * @example 1985-01-01
   */
  processDt?: string;

  /**
   * 출금신청금액
   * @example 100,000
   */
  reqstPoint?: string;

  /**
   * 반려사유
   * @example 어머니는 자장면이 싫다고 하셨어
   */
  returnResn?: string;

  /**
   * 우편번호
   * @example 06317
   */
  zipcode?: string;
}

/**
 * 포인트 출금 신청 상세 수정 Request VO
 */
export interface PointDefraymentModifyRequest {
  /**
   * 상태 (00:출금신청, 10:출금완료, 20:출금반려)
   * @example 00
   */
  defraySttus?: string;

  /**
   * 반려사유
   * @example 일일 출금신청 한도 초과
   */
  returnResn?: string;
}

/**
 * 포인트 출금 신청 목록 Response VO
 */
export interface PointDefraymentResponse {
  /**
   * 신청자아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 신청자명
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 담당자
   * @example 차명환
   */
  chargerNm?: string;

  /**
   * 상태 (출금신청, 출금완료, 출금반려)
   * @example 출금신청
   */
  defraySttus?: string;

  /**
   * 출금 신청일
   * @format date
   * @example 2020-12-06
   */
  defraymentReqstDt?: string;

  /**
   * 보유포인트
   * @example 150,000
   */
  holdPoint?: string;

  /**
   * 포인트 출금 신청 고유 번호
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 처리일
   * @example 1985-01-01
   */
  processDt?: string;

  /**
   * 출금신청금액
   * @example 100,000
   */
  reqstPoint?: string;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  rownum?: number;
}

/**
 * 포인트 상세 VO
 */
export interface PointDetail {
  /**
   * 포인트 상세 취소 순번
   * @format int32
   * @example 4
   */
  cancelNo?: number;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /** 수정일시 */
  mod_dt?: string;

  /**
   * 수정자
   * @format int32
   */
  mod_id?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 처리일자
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointProcessDt?: string;

  /**
   * 포인트 요청일자
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointReqDt?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 포인트 사용일자
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointUseDt?: string;

  /** 등록일시 */
  reg_dt?: string;

  /**
   * 등록자
   * @format int32
   */
  reg_id?: number;

  /**
   * 포인트 상세 적립 순번
   * @format int32
   * @example 1
   */
  saveNo?: number;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Insert Request VO
 */
export interface PointMasterInsertRequest {
  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류 PAY : 결제, EVL : 평가, EVT : 이벤트, DEF : 출금, ADM : 관리자, DAY: 데일리평가
   * @example PAY
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 사용 여부('Y':사용, 'N':미사용
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Insert Response VO
 */
export interface PointMasterInsertResponse {
  error?: ErrorVo;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  modDt?: string;

  /**
   * 수정자
   * @format int32
   * @example 1010
   */
  modId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 1010
   */
  regId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Modify Request VO
 */
export interface PointMasterModifyRequest {
  /**
   * 적립 예정일
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  accmlPrarnde?: string;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1000
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Modify Response VO
 */
export interface PointMasterModifyResponse {
  error?: ErrorVo;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  modDt?: string;

  /**
   * 수정자
   * @format int32
   * @example 1010
   */
  modId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 1010
   */
  regId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 VO
 */
export interface PointMasterResponse {
  /**
   * 적립 예정일
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  accmlPrarnde?: string;
  error?: ErrorVo;

  /**
   * 암호화된 Hash값
   * @example DFCD3454BBEA788A751A
   */
  hashDigest?: string;

  /**
   * 사용자 id
   * @format int32
   * @example 1000
   */
  memberId?: number;

  /**
   * 수정일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  modDt?: string;

  /**
   * 수정자
   * @format int32
   * @example 1010
   */
  modId?: number;

  /**
   * 순번
   * @format int32
   */
  no?: number;

  /**
   * 포인트 이력 원본 순번
   * @format int32
   * @example 4
   */
  orginlNo?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 0
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 포인트
   * @format int32
   * @example 50000
   */
  point?: number;

  /**
   * 잔여 포인트 만료일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  pointExpireDt?: string;

  /**
   * 포인트 종류
   * @example CUS
   */
  pointKnd?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 사유
   * @example 유튜브 영상 의뢰 관련 함정 답변으로 인하여 점수차감이 되었으나 컴플레인으로 인하여 차감 금액 및 평가 금액 재지급
   */
  reason?: string;

  /**
   * 등록일시
   * @format date-time
   * @example 2021-02-22T08:30:27.090Z
   */
  regDt?: string;

  /**
   * 등록자
   * @format int32
   * @example 1010
   */
  regId?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 포인트 거래일자
   * @format date-time
   * @example 2021-02-21
   */
  reqstDt?: string;

  /**
   * 사용 여부
   * @example Y
   */
  useYn?: string;
}

/**
 * 포인트 마스터 Insert Response VO
 */
export interface PointMasterSingleGroupByResponse {
  /**
   * 누적 출금 포인트
   * @format int32
   * @example 1000
   */
  accmlt_def_point?: number;
  error?: ErrorVo;

  /**
   * 오늘 적립 예정 포인트
   * @format int32
   * @example 1000
   */
  today_accml_point?: number;

  /**
   * 사용 가능한 포인트
   * @format int32
   * @example 1000
   */
  use_posbl_point?: number;
}

/**
 * [고객센터] - 이벤트 게시글 상세조회 Response
 */
export interface PostUserEventInfoResponse {
  attachFileInfoResponse?: AttachFileInfoResponse[];

  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;
  error?: ErrorVo;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 이벤트 게시글 목록 조회 Response
 */
export interface PostUserEventSearchResponse {
  /**
   * 첨부파일명(이벤트 썸네일용)
   * @example test
   */
  attachFileName?: string;

  /**
   * 첨부파일 URL (이벤트 썸네일용)
   * @example https://.../assets/wewill/dev/upload/board/event/test.txt
   */
  attachFileUrl?: string;

  /**
   * 이벤트 기간 종료요일
   * @example 금
   */
  endDay?: string;

  /**
   * 이벤트 기간 종료일시(이벤트용)
   * @example 2021-05-31 00:00
   */
  endDt?: string;

  /**
   * 이벤트 상태 코드 (W:대기, P:진행, D:마감)
   * @example P
   */
  eventStatus?: string;

  /**
   * 이벤트 상태(대기, 진행, 마감)
   * @example 진행
   */
  eventStatusNm?: string;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록일
   * @example 2021-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 이벤트 기간 시작요일
   * @example 월
   */
  startDay?: string;

  /**
   * 이벤트 기간 시작일시(이벤트용)
   * @example 2021-04-20 00:00
   */
  startDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 공지사항 게시글 상세조회 Response
 */
export interface PostUserNoticeInfoResponse {
  /**
   * 내용
   * @example 게시글 내용
   */
  contents?: string;
  error?: ErrorVo;

  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록일시
   * @example 2021-01-01
   */
  regDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [고객센터] - 공지사항 게시글 목록 조회 Response
 */
export interface PostUserNoticeSearchResponse {
  /**
   * 게시글 ID
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 등록일시
   * @example 2021-01-01
   */
  regDt?: string;

  /**
   * 제목
   * @example 게시글 제목
   */
  subject?: string;
}

/**
 * [사용자페이지] - 미확인된 이벤트 수 Response
 */
export interface PostUserUnCheckEventPostCntResponse {
  error?: ErrorVo;

  /**
   * 미확인된 이벤트 수
   * @format int32
   * @example 99
   */
  unCheckEventPostCnt?: number;
}

export interface ProductCategoryInfoResponse {
  /** @format int32 */
  categoryId?: number;
}

export interface ProductFileInfoResponse {
  /** @format int32 */
  attachFileId?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  prductId?: number;
}

export interface ProductInfoResponse {
  /**
   * 첨부파일 ID(첨부파일aws)
   * @example []
   */
  attachFileList?: AttachFileInfResponse[];

  /**
   * 제품설명
   * @example 질문합니다
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이미지파일 ID(이미지파일aws)
   * @example []
   */
  imageAttachFileList?: ImageAttachFileInfoResponse[];

  /**
   * 제품 이미지파일 ID(제품 이미지)
   * @example []
   */
  imageFileList?: ImageFileInfoResponse[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품카테고리
   * @example [1,2]
   */
  productCategoryList?: ProductCategoryInfoResponse[];

  /**
   * 제품 첨부파일 ID(제품 파일)
   * @example []
   */
  productFileList?: ProductFileInfoResponse[];

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;

  /**
   * 썸네일파일 ID(썸네일파일aws)
   * @example []
   */
  thumbAttachFileList?: ThumbAttachFileInfoResponse[];

  /**
   * 제품 이미지파일 썸네일 ID(제품 썸네일)
   * @example []
   */
  thumbFileList?: ProductThumbInfoResponse[];
}

export interface ProductInsertRequest {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 설명
   * @example 제품 설명
   */
  dc?: string;

  /**
   * 이미지파일 ID(제품 이미지)
   * @example [1,2]
   */
  imageFileIds?: number[];

  /**
   * 제품카테고리
   * @example [1,2]
   */
  prdtCategoryIds?: number[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품 썸네일 ID
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  productThumbnailInfoList?: ProductThumbnailMerge[];

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;
}

export interface ProductInsertResponse {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 제품설명
   * @example 질문합니다
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이미지파일 ID(제품 이미지)
   * @example [1,2]
   */
  imageFileIds?: number[];

  /**
   * 제품카테고리
   * @example [59,60]
   */
  prdtCategoryIds?: number[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품 썸네일 ID
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  productThumbnailInfoList?: ProductThumbnailMerge[];

  /**
   * 의뢰콘텐츠 (V:영상, P:제품)
   * @example P
   */
  reqestCntnts?: string;

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;
}

export interface ProductModifyRequest {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 설명
   * @example 제품 설명
   */
  dc?: string;

  /**
   * 이미지파일 ID(제품 이미지)
   * @example [1,2]
   */
  imageFileIds?: number[];

  /**
   * 제품카테고리
   * @example [1,2]
   */
  prdtCategoryIds?: number[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품 썸네일 ID
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  productThumbnailInfoList?: ProductThumbnailMerge[];

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;
}

export interface ProductModifyResponse {
  /**
   * 첨부파일 ID(첨부파일용)
   * @example [1,2]
   */
  attachFileIds?: number[];

  /**
   * 제품설명
   * @example 질문합니다
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이미지파일 ID(제품 이미지)
   * @example [1,2]
   */
  imageFileIds?: number[];

  /**
   * 제품카테고리
   * @example [59,60]
   */
  prdtCategoryIds?: number[];

  /**
   * 제품 금액
   * @format int32
   * @example 59000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 아기딸랑이
   */
  prductNm?: string;

  /**
   * 제품 썸네일 ID
   * @example [{"attachFileId":1,"useYn":"Y"},{"attachFileId":2,"useYn":"N"}]
   */
  productThumbnailInfoList?: ProductThumbnailMerge[];

  /**
   * 의뢰콘텐츠 (V:영상, P:제품)
   * @example P
   */
  reqestCntnts?: string;

  /**
   * 의뢰명
   * @example 제품의뢰해주세요
   */
  reqestNm?: string;
}

/**
 * 제품 썸네일 등록&수정
 */
export interface ProductThumbnailMerge {
  /**
   * 썸네일파일id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 사용여부
   * @example N
   */
  useYn?: string;
}

export interface QestnEntity {
  answerEntityList?: AnswerEntity[];
  answerTy?: string;
  delYn?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnCn?: string;

  /** @format int32 */
  qestnId?: number;

  /** @format int32 */
  qestnNo?: number;
  qestnrEntity?: QestnrEntity;

  /** @format int32 */
  qestnrId?: number;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;
  rspnsOption?: string;

  /** @format int32 */
  sn?: number;
}

export interface QestnrEntity {
  dc?: string;
  delYn?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnEntityList?: QestnEntity[];

  /** @format int32 */
  qestnrId?: number;
  qestnrTy?: string;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;
}

/**
 * 의뢰 설문지 질문 정보
 */
export interface Question {
  /**
   * 답변유형 (001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지답변)
   * @example 001
   */
  answerTy?: string;

  /** 답변목록 */
  answers?: Answer[];

  /**
   * 질문내용
   * @example 세 사람의 학자가 함께 기차를 타고 가다가 몇 분 걸려 터널을 통과했는데
   */
  qestnCn: string;

  /**
   * 질문 번호
   * @format int32
   * @example 1
   */
  qestnNo: number;

  /**
   * 응답옵션 (S:단일응답, M:중복응답)
   * @example S
   */
  rspnsOption?: string;

  /**
   * 질문 순번 (질문 번호와 동일)
   * @format int32
   * @example 1
   */
  sn: number;

  /**
   * 내부 유지 값(프론트 용)
   * @example aa
   */
  value?: string;
}

/**
 * 캡챠 Response
 */
export interface RecaptchaResponse {
  /**
   * 성공여부
   * @example submit
   */
  action?: string;

  /**
   * 호출시간
   * @example 2021-05-18T05:35:04Z
   */
  challenge_ts?: string;
  error?: ErrorVo;

  /**
   * hostname
   * @example localhost
   */
  hostname?: string;

  /**
   * 점수
   * @example 0.9
   */
  score?: string;

  /**
   * 성공여부
   * @example true
   */
  success?: boolean;
}

export interface ReportAllAnswerRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 모든 답변 질문 타입(H:평가자들이 해당 구간을 가장 인상깊은 장면으로 뽑은 이유 R:영상을 본 소감)
   * @example H
   */
  type?: string;
}

export interface ReportAllAnswerResponse {
  /**
   * 연령대
   * @example 100
   */
  ageList?: number[];

  /**
   * 질문 타입(답변유형 ([CL002] 001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지))
   * @example 001
   */
  answerTy?: string;

  /**
   * 질문 타입명
   * @example 객관식
   */
  answerTyNm?: string;
  error?: ErrorVo;

  /**
   * 평가 수
   * @format int32
   * @example 100
   */
  evlCnt?: number;

  /**
   * 설문 타입
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문 타입명
   * @example 001
   */
  evlMthdNm?: string;

  /** 답변 페이징 데이터 */
  page?: PagingInfoReportAnswerResponse;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 의뢰서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:전체 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 차트 제목
   * @example 차트 제목
   */
  title?: string;
}

export interface ReportAnswerRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;
}

export interface ReportAnswerResponse {
  /**
   * 연령
   * @example 30대
   */
  age?: string;

  /**
   * 답변 내용
   * @example 답변 내용
   */
  answerCn?: string;

  /**
   * 성별
   * @example 남성
   */
  sexdstnNm?: string;

  /**
   * 아이디
   * @example bl***
   */
  username?: string;
}

export interface ReportStarScoreRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 필터( 연령별 :A 성별:S
   * @example A
   */
  filter?: string;

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 별점 설문 구분값(001:전반적 평가 002:기대)
   * @example 1
   */
  starType?: string;
}

export interface ReqestEstimateInfoResponse {
  /**
   * 자동산출여부
   * @example Y/N
   */
  atmcPrdctnYn?: string;

  /**
   * 담당자(견적서 자동산출 불가 , 검수 담당자가 견적서 작성 완료 시 노출)
   * @example 담당자 이름
   */
  charger?: string;
  error?: ErrorVo;

  /**
   * 견적일
   * @example yyyy-MM-dd
   */
  estimatedDate?: string;

  /** 응답수집비용 수량(평가인원) */
  goalEvlCo?: string;

  /** 심사비용 단가 */
  jdgmnCtUntpc?: string;

  /**
   * 견적서 id
   * @format int32
   * @example 1
   */
  prqudoId?: number;

  /**
   * 설문 문항 수
   * @example 10 ~ 19(베이직은 설문 문항 없음)
   */
  qustnrQesitmCo?: string;

  /**
   * 심사비용 수량
   * @format int32
   */
  qy?: number;

  /**
   * 수신자id
   * @format int32
   */
  rcverId?: number;

  /**
   * 의뢰 유형(상품)
   * @example PREMIUM/BASIC
   */
  requestTy?: string;

  /** 응답수집비용 단가 */
  rspnsColctCtUntpc?: string;

  /** 공급가액 */
  splpcAm?: string;

  /** 총 심사비용 */
  totJdgmnCt?: string;

  /** 총 응답수집비용 */
  totRspnsColctCt?: string;

  /** 합계 */
  totSetleExpectAmount?: string;

  /** 수신자(의뢰자명) */
  userNm?: string;

  /** 부가가치세 */
  vat?: string;

  /**
   * 영상 시간
   * @example 10~14분
   */
  vidoRevivTime?: string;
}

/**
 * 의뢰 설문지 정보
 */
export interface ReqestQestnr {
  /**
   * 평가방식 (001:사전설문, 002:실시간설문, 003:사후설문, 004최종평가)
   * @example 001
   */
  evlMthd: string;

  /**
   * 설문지 등장시간 - 사전설문의 경우 00:00:00, 사후설문의 경우 영상재생시간 마지막
   * @example 00:00:00
   */
  qestnTime?: string;

  /**
   * 설문지 설명 - 제품 설문에 사용. 영상은 반드시 null
   * @example 이 제품은 장기 사용 시 납중독 우려가 있음.
   */
  qestnrDc?: string;

  /** 질문목록 */
  questionList: Question[];

  /**
   * 의뢰id
   * @format int32
   * @example 1
   */
  reqestId: number;

  /**
   * 영상id - 제품의 경우 입력 불필요
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

export interface ReqestQestnrEntity {
  evlMthd?: string;

  /** @format date-time */
  modDt?: string;

  /** @format int32 */
  modId?: number;
  qestnTime?: string;

  /** @format int32 */
  qestnrId?: number;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;

  /** @format int32 */
  reqestId?: number;

  /** @format int32 */
  vidoId?: number;
}

/**
 * 의뢰신청 전체 임시저장 Response
 */
export interface ReqestSaveResponse {
  error?: ErrorVo;

  /**
   * 로컬스토리지 데이터 JSON (uri encoding)
   * @example {"a":"aaa"}
   */
  jsonDataUriEncoded?: string;

  /**
   * 의뢰ID
   * @format int32
   * @example 1
   */
  reqestId?: number;
}

export interface Resource {
  description?: string;
  file?: File;
  filename?: string;
  inputStream?: InputStream;
  open?: boolean;
  readable?: boolean;
  uri?: URI;
  url?: URL;
}

export interface SearchPrductCategoryResponse {
  /** 제품 카테고리명 */
  categoryNm?: string;

  /**
   * 제품 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface SearchPrductOrderCategoryResponse {
  /** 제품 카테고리명 */
  categoryNm?: string;

  /**
   * 제품 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface SearchVidoCategoryResponse {
  /** 영상 카테고리명 */
  categoryNm?: string;

  /**
   * 영상 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface SearchVidoOrderCategoryResponse {
  /** 영상 카테고리명 */
  categoryNm?: string;

  /**
   * 영상 카테고리ID
   * @format int32
   * @example 1
   */
  id?: number;
}

/**
 * SelectBox option 생성용 VO
 */
export interface SelectOptionsVo {
  /**
   * 추가정보
   * @example 연세
   */
  extra?: string;

  /**
   * 레이블
   * @example 나이
   */
  label?: string;

  /**
   * 초기선택값 여부
   * @example false
   */
  selected?: boolean;

  /**
   * 순번
   * @format int32
   * @example 0
   */
  seq?: number;

  /**
   * 선택값
   * @example age
   */
  value?: string;
}

/**
 * 결제 취소 Request
 */
export interface SettlementCancelRequest {
  /**
   * 환불 요청 금액
   * @format double
   * @example 10000
   */
  amount?: number;

  /**
   * 서비스 유형
   * @example wewill
   */
  app?: string;

  /**
   * 취소 가능한 잔액
   * @format double
   * @example 10000
   */
  checkSum?: number;

  /**
   * 아임포트결제고유ID
   * @example f87c73b7fe13b724b32836984905f2fd167fc68f
   */
  merchantUid?: string;

  /**
   * 취소사유
   * @example 중복결제로 인한 결제 취소
   */
  reason?: string;

  /**
   * 환불 수령계좌 번호
   * @example 123-45-12345
   */
  refundAccount?: string;

  /**
   * 환불 수령계좌 은행코드
   * @example 031
   */
  refundBank?: string;

  /**
   * 환불 수령계좌 예금주
   * @example 김지만
   */
  refundHolder?: string;
}

/**
 * 결제 등록 Request
 */
export interface SettlementInsertRequest {
  /**
   * 쿠폰 아이디 배열
   * @example [3,4]
   */
  clientCouponNoList?: number[];

  /**
   * 쿠폰할인금액
   * @format int32
   * @example 15000
   */
  couponDscntAmount?: number;

  /**
   * 상품명
   * @example Premium
   */
  goodsName?: string;

  /**
   * 회원 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 아임포트결제고유ID
   * @example f87c73b7fe13b724b32836984905f2fd167fc68f
   */
  merchantUid?: string;

  /**
   * 의뢰 아이디
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰 명
   * @example 의뢰명입니다.
   */
  reqestNm?: string;

  /**
   * 결제 요청 금액
   * @format int32
   * @example 200
   */
  setleReqestAmount?: number;

  /**
   * 공급가액
   * @format int32
   * @example 1280000
   */
  splpcAm?: number;

  /**
   * 부가가치세
   * @format int32
   * @example 128000
   */
  vat?: number;
}

/**
 * 결제 등록 Response
 */
export interface SettlementInsertResponse {
  /**
   * 금액 - 공급가액
   * @format int32
   * @example 1280000
   */
  amount?: number;

  /**
   * 금액 - 공급가액 포멧
   * @example 1,280,000
   */
  amountFormat?: string;

  /**
   * 현금영수증발급 사업자 등록 번호
   * @example 123-45-12345
   */
  cashReceiptBizno?: string;

  /**
   * 현금영수증발급 여부
   * @example true
   */
  cashReceiptIssued?: boolean;

  /**
   * 현금영수증발급 휴대폰 번호
   * @example 010-2323-3232
   */
  cashReceiptMbtlnum?: string;

  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 금액 포멧
   * @example 8,000
   */
  couponDscntAmountFormat?: string;
  error?: ErrorVo;

  /**
   * 상품명
   * @example Premium
   */
  goodsName?: string;

  /**
   * 회원 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 아임포트결제고유ID
   * @example 078831d6b187b2bd23157b3898a142816793fb39
   */
  merchantUid?: string;

  /**
   * 주문번호
   * @example 20210414-215520-1
   */
  no?: string;

  /**
   * 의뢰 아이디
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌, memor:수기결제
   * @example vbank
   */
  setleKnd?: string;

  /**
   * 결제종류코드명
   * @example 가상계좌
   */
  setleKndNm?: string;

  /**
   * 결제 금액 합계
   * @format int32
   * @example 1400000
   */
  setleReqestAmount?: number;

  /**
   * 결제 금액 합계 포멧
   * @example 1,400,000
   */
  setleReqestAmountFormat?: string;

  /**
   * 부가세
   * @format int32
   * @example 128000
   */
  vat?: number;

  /**
   * 부가세 포멧
   * @example 12,8000
   */
  vatFormat?: string;

  /**
   * 가상계좌 은행 표준코드 - (금융결제원기준)
   * @example 088
   */
  vbankCode?: string;

  /**
   * 입금받을 가상계좌 마감기한
   * @example 1625826618
   */
  vbankDt?: string;

  /**
   * 입금받을 가상계좌 예금주
   * @example （주）케이지이니시
   */
  vbankHolder?: string;

  /**
   * 입금받을 가상계좌 은행명
   * @example 신한(조흥)은행
   */
  vbankName?: string;

  /**
   * 입금받을 가상계좌 계좌번호
   * @example 56211102580756
   */
  vbankNum?: string;
}

/**
 * Settlement Response Vo
 */
export interface SettlementResponse {
  /** @format int32 */
  amount?: number;

  /** @format date-time */
  applyDt?: string;

  /** @format date-time */
  cancelDt?: string;
  cardCode?: string;
  cardExpirDt?: string;
  cardName?: string;
  cardNumber?: string;

  /** @format int32 */
  cardQuota?: number;
  cardType?: string;

  /** @format date-time */
  chgDt?: string;

  /** @format int32 */
  chgId?: number;
  chgIp?: string;
  delngNo?: string;

  /** @format int32 */
  memberId?: number;
  pgTid?: string;

  /** @format int32 */
  pg_fee?: number;
  receiptUrl?: string;

  /** @format date-time */
  regDt?: string;

  /** @format int32 */
  regId?: number;
  regIp?: string;

  /** @format int32 */
  reqestId?: number;

  /** @format date-time */
  setleBgnde?: string;

  /** @format int32 */
  setleCode?: number;

  /** @format date-time */
  setleEndde?: string;
  setleKnd?: string;
  setleSttus?: string;

  /** @format int32 */
  vat?: number;
}

export interface StackBarChartResponse {
  /**
   * 질문 타입
   * @example 001
   */
  answerTy?: string;

  /**
   * 질문 타입명
   * @example 객관식 단일
   */
  answerTyNm?: string;

  /** 차트 데이터 셋 */
  chartData?: ChartData;
  error?: ErrorVo;

  /**
   * 평가 수
   * @format int32
   * @example 1000
   */
  evlCnt?: number;

  /**
   * 설문 타입
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문 타입명
   * @example 사전 설문
   */
  evlMthdNm?: string;

  /** 차트 질문 보기목록(Option) */
  options?: Options;

  /**
   * 질문 아이디
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문 노출 시간
   * @example 00:00
   */
  qestnTime?: Time;

  /**
   * 설문지 아이디
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 별점 평균(별점차트)
   * @format float
   * @example 1
   */
  starScoreAverage?: number;

  /**
   * 질문 제목
   * @example 질문 제목
   */
  title?: string;

  /**
   * 이미지 답변 url
   * @example http://test.com
   */
  url?: string[];
}

export interface ThumbAttachFileInfoResponse {
  /** @format int32 */
  tattachFileId?: number;
  tfileExt?: string;
  tfileName?: string;
  tfileUrl?: string;
}

export interface Time {
  /** @format int32 */
  date?: number;

  /** @format int32 */
  day?: number;

  /** @format int32 */
  hours?: number;

  /** @format int32 */
  minutes?: number;

  /** @format int32 */
  month?: number;

  /** @format int32 */
  seconds?: number;

  /** @format int64 */
  time?: number;

  /** @format int32 */
  timezoneOffset?: number;

  /** @format int32 */
  year?: number;
}

export interface TokenRequest {
  /**
   * 토큰
   * @example 토큰값
   */
  token?: string;
}

export interface URI {
  absolute?: boolean;
  authority?: string;
  fragment?: string;
  host?: string;
  opaque?: boolean;
  path?: string;

  /** @format int32 */
  port?: number;
  query?: string;
  rawAuthority?: string;
  rawFragment?: string;
  rawPath?: string;
  rawQuery?: string;
  rawSchemeSpecificPart?: string;
  rawUserInfo?: string;
  scheme?: string;
  schemeSpecificPart?: string;
  userInfo?: string;
}

export interface URL {
  authority?: string;
  content?: object;

  /** @format int32 */
  defaultPort?: number;
  deserializedFields?: URLStreamHandler;
  file?: string;
  host?: string;
  path?: string;

  /** @format int32 */
  port?: number;
  protocol?: string;
  query?: string;
  ref?: string;

  /** @format int32 */
  serializedHashCode?: number;
  userInfo?: string;
}

export type URLStreamHandler = object;

export interface UploadAttachFileResponse {
  error?: ErrorVo;

  /**
   * 파일 확장자
   * @example jpg
   */
  fileExt?: string;

  /**
   * 파일명
   * @example 테스트
   */
  fileName?: string;

  /**
   * 파일 URL
   * @example https://test.com
   */
  fileUrl?: string;

  /**
   * 파일 아이디
   * @format int32
   * @example 1
   */
  id?: number;
}

export interface UserClientChartSearchRequest {
  /**
   * 차트 연령별 조건(전체: 모든 연령대 배열에 넣어주어야 함)
   * @example [10,20,30,40]
   */
  ageList?: number[];

  /**
   * 차트 필터(연령별:A 성별:S)
   * @example A
   */
  filter?: string;

  /**
   * 차트 정렬 조건(A:보기순 E:평가순)
   * @example A
   */
  orderBy?: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 10
   */
  pageSize?: number;

  /**
   * 요청서 설문지 질문
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 요청서 설문지 아이디
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 요청서 아이디
   * @format int32
   * @example 295
   */
  requestId?: number;

  /**
   * 차트 성별 조건(전체:A 남:M 여:F)
   * @example A
   */
  sexdstn?: string;
}

export interface UserClientCheckPwdRequest {
  /**
   * 비밀번호
   * @example Cusp1234!@
   */
  password?: string;

  /**
   * 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserClientCheckPwdResponse {
  error?: ErrorVo;

  /**
   * 입력한 비밀번호가 맞는지 여부 ( 맞으면: true, 틀리면 : false)
   * @example true
   */
  result?: string;
}

export interface UserClientInsertRequest {
  /**
   * 생년월일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 사업자 구분(개인:P 법인:C)
   * @example P
   */
  bizSection?: string;

  /**
   * 이메일
   * @example abc@abc.com
   */
  email?: string;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 메시지 id
   * @example a3a3er
   */
  messageId?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 비밀 번호
   * @example 1q2w3e
   */
  password?: string;

  /**
   * 서비스(wewill:W, fillin:F)
   * @example W
   */
  service?: string;

  /**
   * 성별
   * @example M
   */
  sexdstn?: string;

  /**
   * 이용 약관
   * @example [1,2,3]
   */
  termsIds?: number[];

  /**
   * 사용자 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserClientInsertResponse {
  /**
   * 통합계정 아이디
   * @format int32
   * @example 1
   */
  accountId?: number;
  error?: ErrorVo;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 등록일
   * @example 2020-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 계정 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserClientMemberChartDatasets {
  backgroundColor?: string[];
  data?: number[];
}

export interface UserClientMemberChartResponse {
  datasets?: UserClientMemberChartDatasets[];
  labels?: string[];
}

export interface UserClientMemberDashBoardChartRequest {
  /**
   * 회원 아이디
   * @format int32
   */
  memberId?: number;

  /**
   * 의뢰 아이디
   * @format int32
   */
  requestId?: number;
}

export interface UserClientMemberDashBoardReqCntResponse {
  /**
   * 작성중 갯수
   * @format int32
   * @example 1
   */
  cnt10?: number;

  /**
   * 완료 갯수
   * @format int32
   * @example 1
   */
  cnt100?: number;

  /**
   * 의뢰 중지 갯수
   * @format int32
   * @example 1
   */
  cnt110?: number;

  /**
   * 견적요청 갯수
   * @format int32
   * @example 1
   */
  cnt20?: number;

  /**
   * 견적산출 갯수
   * @format int32
   * @example 1
   */
  cnt30?: number;

  /**
   * 결제대기 갯수
   * @format int32
   * @example 1
   */
  cnt40?: number;

  /**
   * 심사대기 갯수
   * @format int32
   * @example 1
   */
  cnt50?: number;

  /**
   * 심사중 갯수
   * @format int32
   * @example 1
   */
  cnt60?: number;

  /**
   * 심사 반려 갯수
   * @format int32
   * @example 1
   */
  cnt70?: number;

  /**
   * 진행중 갯수
   * @format int32
   * @example 1
   */
  cnt80?: number;

  /**
   * 취소 완료 갯수
   * @format int32
   * @example 1
   */
  cnt90?: number;
  error?: ErrorVo;
}

export interface UserClientMemberDashBoardResponse {
  /**
   * 최초 작성일
   * @example 2020-01-01
   */
  createDate?: string;

  /**
   * 최초 작성일
   * @example 2020-01-01
   */
  date?: string;

  /**
   * 날짜 명
   * @example 의뢰 신청일
   */
  dateNm?: string;

  /**
   * 드롭다운 여부(Y:드롭다운 N:없음)
   * @example Y
   */
  dropYn?: string;

  /**
   * 반려사유 내용
   * @example 반려사유 내용
   */
  message?: string;

  /**
   * 더보기 메뉴
   * @example 결제내역,삭제
   */
  moreView?: string;

  /**
   * new 뱃지(Y:new N:old)
   * @example Y
   */
  newBadge?: string;

  /**
   * productId id(제품일 경우)
   * @format int32
   * @example 1
   */
  prductId?: number;

  /**
   * 진행율(0~100)
   * @format int32
   * @example 10
   */
  progress?: number;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example B
   */
  quality?: string;

  /**
   * 의뢰유형명
   * @example Basic
   */
  qualityNm?: string;

  /**
   * 반려사유존재 유무(Y:반려사유 있음, N: 반려사유 없음)
   * @example N
   */
  rejected?: string;

  /**
   * 다시보기 유무(Y : 다시보기 허용, N: 다시보기 허용안함)
   * @example Y
   */
  replay?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰상태 ([IN001]10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 001
   */
  status?: string;

  /**
   * 의뢰상태명
   * @example 001
   */
  statusNm?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  title?: string;

  /**
   * 의뢰콘텐츠 ([IN002] V:영상, P:제품)
   * @example V
   */
  type?: string;

  /**
   * 의뢰콘텐츠명
   * @example 영상
   */
  typeNm?: string;

  /**
   * vido id(영상일 경우)
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

export interface UserClientMemberDeleteBizRequest {
  /**
   * 첨부 파일 아이디
   * @format int32
   * @example 1
   */
  attachFileId?: number;
}

export interface UserClientMemberDetailChartResponse {
  chartDataSet?: UserClientMemberChartResponse;
  error?: ErrorVo;
}

export interface UserClientMemberInfoResponse {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 12312123
   */
  acnutno?: string;

  /**
   * 광고성정보수신약관 동의여부
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 알림 수신 여부
   * @example Y
   */
  alarmYn?: string;

  /**
   * 사업자 등록 파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 은행명
   * @example XX은행
   */
  bankNm?: string;

  /**
   * 생년월일
   * @example 19900101
   */
  birthday?: string;

  /**
   * 사업자 등록 파일 확장자명
   * @example jpg
   */
  bizFileExt?: string;

  /**
   * 사업자 등록 파일명
   * @example 등록증
   */
  bizFileName?: string;

  /**
   * 계약서 id(사업자 등록증)
   * @format int32
   * @example 1
   */
  contractId?: number;

  /**
   * 계약서명
   * @example test
   */
  contractNm?: string;

  /**
   * 계약서 구분
   * @example 001
   */
  contractSection?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 이메일 수신 동의
   * @example Y
   */
  emailRecptnYn?: string;
  error?: ErrorVo;

  /**
   * 사업자 등록 파일 사이즈(단위 : byte)
   * @format int32
   * @example 10000
   */
  fileSize?: number;

  /**
   * id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 구분 (평가자, 의뢰자)
   * @example C
   */
  section?: string;

  /**
   * 성별
   * @example F
   */
  sexdstn?: string;

  /**
   * sms 수신 동의
   * @example Y
   */
  smsRecptnYn?: string;

  /**
   * 로그인 아이디
   * @example test
   */
  username?: string;
}

export interface UserClientMemberRequestDetailInfoRequest {
  /**
   * 차트 데이터 종류(D:일별 , W:주간별 , M: 월별)
   * @example D
   */
  chartKind?: string;

  /**
   * 의뢰서 id
   * @format int32
   * @example 27
   */
  requestId?: number;
}

export interface UserClientMemberRequestDetailInfoResponse {
  /** 차트 데이터 셋 */
  chart?: DashBoardChartSpec;

  /**
   * 최초 작성일
   * @example 2020-01-01 00:00:00
   */
  createDate?: string;

  /**
   * 최초 작성일
   * @example 2020-01-01 00:00:00
   */
  date?: string;

  /**
   * 날짜 명
   * @example 의뢰 신청일
   */
  dateNm?: string;
  error?: ErrorVo;

  /**
   * 반려사유 내용
   * @example 반려사유 내용
   */
  message?: string;

  /**
   * 진행율(0~100)
   * @format int32
   * @example 10
   */
  progress?: number;

  /**
   * 의뢰유형 ([IN003 ]B:basic, P:premium)
   * @example B
   */
  quality?: string;

  /**
   * 의뢰유형명
   * @example Basic
   */
  qualityNm?: string;

  /**
   * 반려사유존재 유무(Y:반려사유 있음, N: 반려사유 없음)
   * @example N
   */
  rejected?: string;

  /**
   * 다시보기 유무(Y : 다시보기 허용, N: 다시보기 허용안함)
   * @example Y
   */
  replay?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰상태 ([IN001]10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 001
   */
  status?: string;

  /**
   * 의뢰상태명
   * @example 001
   */
  statusNm?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  title?: string;

  /**
   * 의뢰콘텐츠 ([IN002] V:영상, P:제품)
   * @example V
   */
  type?: string;

  /**
   * 의뢰콘텐츠명
   * @example 영상
   */
  typeNm?: string;
}

export interface UserClientModifyAdvrtsRequest {
  /**
   * 광고성 정보 수신동의(Y, N)
   * @example Y
   */
  advrtsProvisionAgreYn?: string;
}

export interface UserClientModifyAlarmRequest {
  /**
   * 알림 설정(Y,N)
   * @example Y
   */
  alarmYn?: string;
}

export interface UserClientModifyBankRequest {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 123123213
   */
  acnutno?: string;

  /**
   * 은행 명
   * @example 신한은행
   */
  bankNm?: string;
}

export interface UserClientModifyBirthdayRequest {
  /**
   * 생년월일
   * @example 19900101
   */
  birthday?: string;
}

export interface UserClientModifyBizContractRequest {
  /**
   * 파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;
}

export interface UserClientModifyBizContractResponse {
  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;
  error?: ErrorVo;

  /**
   * 첨부파일 확장자
   * @example jpg
   */
  fileExt?: string;

  /**
   * 첨부파일 명
   * @example 사업자등록증
   */
  fileName?: string;

  /**
   * 첨부파일 사이즈(byte)
   * @format int32
   * @example 1000000
   */
  fileSize?: number;
}

export interface UserClientModifyEmailRequest {
  /**
   * email
   * @example test@blocksmith.xyz
   */
  email?: string;
}

export interface UserClientModifyNameRequest {
  /**
   * 이름
   * @example 홍길동
   */
  name?: string;
}

export interface UserClientModifyPwdRequest {
  /**
   * 새로운 비밀번호
   * @example 1111
   */
  newPassword?: string;

  /**
   * 기존 비밀번호
   * @example 1111
   */
  password?: string;
}

export interface UserClientModifySexdstnRequest {
  /**
   * 성별(남:M 여:F)
   * @example M
   */
  sexdstn?: string;
}

export interface UserClientReportInfoRequest {
  /** @format int32 */
  requestId?: number;
}

export interface UserClientReportVidoBasicResponse {
  /**
   * 의뢰서에서 설정한 연령대
   * @example [10,20,30]
   */
  ageList?: number[];

  /** 평가자 연령대 차트 */
  ageRangesChart?: StackBarChartResponse;

  /**
   * 카테고리 목록
   * @example ["TV","영화"]
   */
  categoryList?: string[];

  /**
   * 의뢰 완료 기간
   * @example 2021-01-01 ~ 2021-01-02
   */
  completeDate?: string;

  /**
   * 의뢰 콘텐츠명
   * @example 의뢰 명
   */
  contentNm?: string;
  error?: ErrorVo;

  /**
   * 평가인원 수
   * @format int32
   * @example 1000
   */
  evaluatorTotalCnt?: number;

  /** 하이라이트 차트 */
  highlightChart?: StackBarChartResponse;

  /**
   * 영상 제목
   * @example 영상 제목
   */
  mvpSubject?: string;

  /**
   * 설문 수
   * @format int32
   * @example 3
   */
  qestnrTotalCnt?: number;

  /**
   * 영상 다시보기
   * @example 의뢰 명
   */
  replayYn?: string;

  /**
   * 의뢰 명
   * @example 의뢰 명
   */
  reqestNm?: string;

  /**
   * 의뢰 상품(B:베이직 P:프리미엄)
   * @example B
   */
  reqestTy?: string;

  /**
   * 요청서 id(체험하기)
   * @format int32
   */
  requestId?: number;

  /**
   * 의뢰 상품명
   * @example 베이직
   */
  requestTyNm?: string;

  /** 질문 차트(프리미엄만) */
  researchChart?: UserClientResearchResponse;

  /** 평가자 성별 분포 */
  sexdstnChart?: StackBarChartResponse;

  /** 별점 평가(~~어떠신가요) */
  star001Chart?: StackBarChartResponse;

  /** 별점 평가(~~기대되시나요) */
  star002Chart?: StackBarChartResponse;

  /**
   * 별점 평균
   * @format float
   * @example 3.5
   */
  startScoreAverage?: number;

  /**
   * 유튜브 링크
   * @example http://test.com
   */
  url?: string;

  /**
   * 영상 재생 시간(초)
   * @format int32
   * @example 1000
   */
  vidoRevivTime?: number;

  /**
   * 영상 형태
   * @example youtube 링크
   */
  vidoTypeNm?: string;

  /**
   * 조회 수
   * @format int32
   * @example 1000
   */
  viewTotalCnt?: number;

  /** 워드클라우드(장면으로 뽑은 이유) */
  wordCloud001?: WordCloudResponse[];

  /** 워드클라우드(소감) */
  wordCloud002?: WordCloudResponse[];
}

export interface UserClientReportVidoRequest {
  /**
   * 연령대(전체일 경우 모든 나이 다 보내줘야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 필터(A:나이 S:성별)
   * @example A
   */
  filter?: string;

  /**
   * 의뢰서 id
   * @format int32
   * @example 335
   */
  requestId?: number;

  /**
   * 성별(A:전체 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;
}

export interface UserClientRequestDeleteRequest {
  /**
   * 의뢰서 아이디
   * @format int32
   * @example 1
   */
  requestId?: number;
}

export interface UserClientResearchResponse {
  /** 프리미엄 의뢰서일 경우 모든 질문의 대한 차트 리스트 */
  dataList?: object[];
}

export interface UserClientSendSmsRequest {
  /**
   * 휴대폰 번호
   * @example 01012345454
   */
  mbtlnum?: string;
}

/**
 * 결제내역상세 Response Vo
 */
export interface UserClientSettlementInfoResponse {
  /**
   * 취소 환불 시 돌려받을 계좌 정보
   * @example 국민은행 (12-123-******
   */
  accountInfo?: string;

  /**
   * 취소 환불 시 돌려받을 예금주 명
   * @example 신민서
   */
  accountNm?: string;

  /**
   * 공급가액
   * @format int32
   * @example 400000
   */
  amount?: number;

  /**
   * 공급가액 TXT
   * @example 400,000원
   */
  amountTxt?: string;

  /**
   * 승인일시
   * @example 2020-01-01 00:00:00
   */
  applyDt?: string;

  /**
   * 취소완료일시
   * @example 2020-01-01 00:00:00
   */
  cancelDt?: string;

  /**
   * 할부개월 수(0이면 일시불)
   * @example 0
   */
  cardQuota?: string;

  /**
   * 현금영수증 발행여부 - [입금 전 : 미신청, 신청] [입금 후 : 미신청, 발급완료]
   * @example 발급완료
   */
  cashReceiptIssued?: string;

  /**
   * 쿠폰 할인 금액
   * @format int32
   * @example 8000
   */
  couponDscntAmount?: number;

  /**
   * 쿠폰 할인 금액 TXT
   * @example -8,000원
   */
  couponDscntAmountTxt?: string;
  error?: ErrorVo;

  /**
   * 주문 번호
   * @example 20210414-214855-1
   */
  no?: string;

  /**
   * 승인번호
   * @example 2.02104281646036e+23
   */
  pgTid?: string;

  /**
   * 의뢰 번호
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example 유튜브 영상 이단옆차기에 대한 평가
   */
  reqestNm?: string;

  /**
   * 의뢰유형 코드([IN003 ]B:basic, P:premium)
   * @example B
   */
  reqestTy?: string;

  /**
   * 의뢰유형 코드명 ([IN003 ]B:basic, P:premium)
   * @example Basic
   */
  reqestTyNm?: string;

  /**
   * 결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌
   * @example card
   */
  setleKnd?: string;

  /**
   * 결제종류코드명 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌
   * @example 신용카드
   */
  setleKndNm?: string;

  /**
   * 결제수단
   * @example 신용카드 (KB국민 94**********4542)
   */
  setleMn?: string;

  /**
   * 주문일
   * @example 유튜브 영상 이단옆차기에 대한 평가
   */
  setleRegDt?: string;

  /**
   * 결제 금액
   * @format int32
   * @example 432000
   */
  setleReqestAmount?: number;

  /**
   * 결제 금액 TXT
   * @example 432,000원
   */
  setleReqestAmountTxt?: string;

  /**
   * 결제상태. ready:미결제, paid:결제완료, cancelled:결제취소, failed:결제실패 = ['ready', 'paid', 'cancelled', 'failed']
   * @example paid
   */
  setleSttus?: string;

  /**
   * 결제상태명
   * @example 결제완료
   */
  setleSttusNm?: string;

  /**
   * 부가가치세
   * @format int32
   * @example 40000
   */
  vat?: number;

  /**
   * 부가가치세 TXT
   * @example 40,000원
   */
  vatTxt?: string;

  /**
   * 가상계좌 은행 표준코드 - (금융결제원기준) - 기업은행 : 003
   * - 국민은행 : 004
   * - KEB하나은행 : 005
   * - 농협중앙회 : 011 (세틀뱅크 테스트 계정에서만 9999오류 발생)
   * - 우리은행 : 020
   * - SC제일은행 : 023
   * - 한국씨티은행 : 027
   * - 대구은행 : 031
   * - 경남은행 : 039
   * - 부산은행 : 032
   * - 광주은행 : 034
   * - 정보통신부 우체국 : 071
   * - 신한은행 : 088 (세틀뱅크 테스트 계정에서만 9999오류 발생)
   *
   * 지원불가 은행코드표
   * - 수협 : 007
   * - 제주은행 : 035
   * - 전북은행 : 037
   * - 새마을금고 : 045
   * - 신협 : 048
   * @example 003
   */
  vbankCode?: string;

  /**
   * 입금받을 가상계좌 마감기한
   * @example 2021-05-30 01:40:24
   */
  vbankDt?: string;

  /**
   * 입금받을 가상계좌 예금주
   * @example (주)We will
   */
  vbankHolder?: string;

  /**
   * 가상계좌 생성 시각
   * @example 2021-05-30 01:40:24
   */
  vbankIssuedDt?: string;

  /**
   * 입금받을 가상계좌 은행명
   * @example 국민은행
   */
  vbankName?: string;

  /**
   * 입금받을 가상계좌 계좌번호
   * @example 12-123-384902
   */
  vbankNum?: string;
}

/**
 * 결제내역 Response Vo
 */
export interface UserClientSettlementResponse {
  /**
   * 주문 번호
   * @example 20210414-214855-1
   */
  delngNo?: string;

  /**
   * 결제 고유 ID
   * @example 20210414-214855-1
   */
  no?: string;

  /**
   * 의뢰 번호
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example 유튜브 영상 이단옆차기에 대한 평가
   */
  reqestNm?: string;

  /**
   * 의뢰유형 코드([IN003 ]B:basic, P:premium)
   * @example B
   */
  reqestTy?: string;

  /**
   * 의뢰유형 코드명 ([IN003 ]B:basic, P:premium)
   * @example Basic
   */
  reqestTyNm?: string;

  /**
   * 주문일
   * @example 2021-04-03
   */
  setleRegDt?: string;

  /**
   * 결제 금액
   * @format int32
   * @example 432000
   */
  setleReqestAmount?: number;

  /**
   * 결제 금액 TXT
   * @example 432,000원
   */
  setleReqestAmountTxt?: string;

  /**
   * 결제상태. ready:결제대기, paid:결제완료, cancelled:결제취소, expired:결제만료 = ['ready', 'paid', 'cancelled', 'failed']
   * @example 결제대기
   */
  setleSttus?: string;
}

export interface UserDashBoardResponse {
  dashBoardCnt?: UserClientMemberDashBoardReqCntResponse;
  error?: ErrorVo;
  page?: PagingInfoUserClientMemberDashBoardResponse;
}

/**
 * 대시보드 제품 평가 목록 Response
 */
export interface UserDashboardGoodsPageInfoSearchResponse {
  /**
   * 평가 상태  코드명[IN008] 10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈, 50:평가완료(불성실답변))
   * @example 평가중
   */
  codeName?: string;

  /**
   * 불성실평가여부
   * @example N
   */
  dishonestEvlYn?: string;

  /**
   * 평가종료일시
   * @example 2021-05-31 00:00:00
   */
  evlEndDt?: string;

  /**
   * 평가시작일시
   * @example 2021-05-20 00:00:00
   */
  evlStartDt?: string;

  /**
   * 평가상태 (10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   * @example 10
   */
  evlStatus?: string;

  /**
   * 의뢰 등록 회원 id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 이름
   * @example 김블록
   */
  name?: string;

  /**
   * 평가 완료시 지급 포인트
   * @format int32
   * @example 1000
   */
  point?: number;

  /**
   * 제품명
   * @example 제품명
   */
  pruductNm?: string;

  /**
   * 의뢰콘텐츠 (V:영상, P:제품)
   * @example P
   */
  reqestCntnts?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰상태 ([IN001] 10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 80
   */
  reqestStatus?: string;

  /**
   * 로그인 id
   * @example blocksmith
   */
  username?: string;
}

/**
 * 대시보드 제품 평가 목록 Response
 */
export interface UserDashboardGoodsSearchResponse {
  /**
   * 평가완료 목록 개수
   * @format int32
   * @example 10
   */
  completeEvl?: number;

  /**
   * 평가완료(불성실 답변) 목록 개수
   * @format int32
   * @example 10
   */
  dishonestConpleteEvl?: number;
  error?: ErrorVo;

  /** 대시보드 제품 평가 목록 */
  pagingList?: PagingInfoUserDashboardGoodsPageInfoSearchResponse;

  /**
   * 평가중 목록 개수
   * @format int32
   * @example 10
   */
  progressEvl?: number;
}

/**
 * 마이페이지 평가자 해더 정보 Response
 */
export interface UserDashboardHeaderResponse {
  error?: ErrorVo;

  /**
   * 필수영상시청 여부(Y,N)
   * @example Y
   */
  essntlVidoCtyhllYn?: string;

  /**
   * 성실 답변 횟수
   * @format int32
   * @example 10
   */
  honestAnswerCnt?: number;

  /**
   * 성실답변 정책 별 노출 상태값 (PROGRESS : 성실답변 횟수 달성 전, INFO: 성실 답변 횟수 10/10 달성 후 3일 유지, NONE: 성실 답변 횟수 10회 도달 시점부터 3일 후)
   * @example PROGRESS
   */
  honestAnswerCntMode?: string;

  /**
   * 성실 답변률(%)
   * @format int32
   * @example 95
   */
  honestAnswerPercent?: number;

  /**
   * 이름
   * @example 김블록
   */
  name?: string;

  /**
   * 보유 포인트
   * @format int32
   * @example 1000
   */
  point?: number;
}

/**
 * 대시보드 영상/제품 상세 Request
 */
export interface UserDashboardInfoRequest {
  /**
   * 평가상태 (10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈, 50:평가완료(불성실답변))
   * @example 10
   */
  evlStatus?: string;

  /**
   * 의뢰콘텐츠 ([IN002] V:영상, P:제품)
   * @example V
   */
  reqestCntnts?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;
}

/**
 * 대시보드 영상/제품 상세 상태값 Response
 */
export interface UserDashboardInfoResponse {
  error?: ErrorVo;

  /**
   * 평가 영상/제품 상태값(true/false)
   * @example true
   */
  result?: boolean;
}

/**
 * 대시보드 영상 평가 목록 Response
 */
export interface UserDashboardVidoPageInfoSearchResponse {
  /**
   * 평가 상태  코드명[IN007] 10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   * @example 평가중
   */
  codeName?: string;

  /**
   * 불성실평가여부
   * @example N
   */
  dishonestEvlYn?: string;

  /**
   * 필수영상시청 여부(Y,N)
   * @example Y
   */
  essntlVidoCtyhllYn?: string;

  /**
   * 평가종료일시
   * @example 2021-05-31 00:00:00
   */
  evlEndDt?: string;

  /**
   * 평가시작일시
   * @example 2021-05-20 00:00:00
   */
  evlStartDt?: string;

  /**
   * 평가상태 (10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈, 50:평가완료(불성실답변))
   * @example 10
   */
  evlStatus?: string;

  /**
   * 평가자 id
   * @format int32
   * @example 115
   */
  memberId?: number;

  /**
   * 영상 제목
   * @example 동영상 제목
   */
  mvpSubject?: string;

  /**
   * 이름
   * @example 김블록
   */
  name?: string;

  /**
   * 평가 완료시 지급 포인트
   * @format int32
   * @example 1000
   */
  point?: number;

  /**
   * 영상 다시보기 여부
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰콘텐츠 (V:영상, P:제품)
   * @example V
   */
  reqestCntnts?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰상태 ([IN001] 10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 80
   */
  reqestStatus?: string;

  /**
   * 로그인 id
   * @example blocksmith
   */
  username?: string;

  /**
   * 영상 id
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

/**
 * 대시보드 영상 평가 목록 Response
 */
export interface UserDashboardVidoSearchResponse {
  /**
   * 평가완료 목록 개수
   * @format int32
   * @example 10
   */
  completeEvl?: number;

  /**
   * 평가완료(불성실 답변) 목록 개수
   * @format int32
   * @example 10
   */
  dishonestConpleteEvl?: number;
  error?: ErrorVo;

  /** 대시보드 영상 평가 목록 */
  pagingList?: PagingInfoUserDashboardVidoPageInfoSearchResponse;

  /**
   * 평가중 목록 개수
   * @format int32
   * @example 10
   */
  progressEvl?: number;
}

export interface UserEvaluateCheckQestnrRequest {
  /**
   * 이동할 설문지 타입
   * @example 002
   */
  evlMthd?: string;

  /**
   * 이동할 설문지 아이디
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 요청서 아이디
   * @format int32
   * @example 1
   */
  requestId?: number;
}

export interface UserEvaluateCheckQestnrResponse {
  /**
   * 설명
   * @example 설문에 대한 설명
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 평가방식 (001:사전설문, 002:실시간설문, 003:사후설문, 004최종평가)
   * @example 001
   */
  evlMthd?: string;

  /**
   * 평가상태 ([IN007] 10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   * @example 10
   */
  evlStatus?: string;

  /**
   * 설문지 완료 상태(Y:제출 완료 N: 미제출)
   * @example Y
   */
  evlYn?: string;

  /**
   * 설문 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 설문지타입 (001:의뢰, 002:의뢰(커스프), 003:데일리 )
   * @example 001
   */
  qestnrTy?: string;

  /** 질문 리스트 */
  questnList?: UserEvaluateGoodsQuestnResponse[];

  /**
   * 내가 답변한 질문의 갯수
   * @format int32
   * @example 10
   */
  totalMyQestnCnt?: number;

  /**
   * 전체 설문지의 질문 갯수
   * @format int32
   * @example 10
   */
  totalQestnCnt?: number;
}

export interface UserEvaluateCheckResponse {
  /**
   * 평가 인원이 마감되었을 경우 (균등분포 포함),(성별/연령이 맞지 않을 경우) - 영상시청 alert 'N'
   * @example N
   */
  checkAgeAndGenderYn?: string;
  error?: ErrorVo;

  /**
   * 평가 가능 유무
   * @example Y
   */
  evaluateYn?: string;
}

export interface UserEvaluateGoodsAnswerInsertRequest {
  /**
   * 답변 내용
   * @example 답변
   */
  answerCn?: string;

  /**
   * 답변 id
   * @format int32
   * @example 1
   */
  answerId?: number;

  /**
   * 답변 순번
   * @format int32
   * @example 1
   */
  answerSn?: number;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 함정답변 유무
   * @example Y
   */
  trapAnswerYn?: string;
}

export interface UserEvaluateGoodsCategoryResponse {
  /**
   * 카테고리 id
   * @format int32
   * @example 1
   */
  categoryId?: number;

  /**
   * 카테고리 명
   * @example 도서
   */
  categoryNm?: string;
}

export interface UserEvaluateGoodsCheckStatusResponse {
  error?: ErrorVo;

  /**
   * 평가상태 (10:평가중, 20:평가심사전, 30:평가심사중, 40:평가심사반려, 50:완료, 60:평가기간만료)
   * @example 50
   */
  evlStatus?: string;
}

export interface UserEvaluateGoodsFileResponse {
  /**
   * 파일 확장자
   * @example 파일 확장자
   */
  attachFileExt?: string;

  /**
   * 파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 파일 이름
   * @example 파일 명
   */
  attachFileName?: string;
  error?: ErrorVo;

  /**
   * 파일 url
   * @example test@test.com
   */
  fileUrl?: string;
}

export interface UserEvaluateGoodsInfoResponse {
  /**
   * 연령 대
   * @example 10,20
   */
  age?: string;

  /**
   * 제품 카테고리
   * @example ["도서","뷰티"]
   */
  categoryList?: UserEvaluateGoodsCategoryResponse[];

  /**
   * 의뢰자 코멘트
   * @example 의뢰자 코멘트
   */
  comment?: string;

  /**
   * 제품 설명
   * @example 제품설명
   */
  dc?: string;
  error?: ErrorVo;

  /** 평가자구성 (001:균등분포) */
  evaluatorComposition?: string;

  /** 제품 평가 상태 */
  evlStatus?: string;

  /** 제품 파일 리스트 */
  files?: UserEvaluateGoodsFileResponse[];

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 의뢰 등록 회원 id
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 평가 완료시 지급 포인트
   * @format int32
   * @example 1000
   */
  point?: number;

  /**
   * 평가 참여 필요 포인트
   * @format int32
   * @example 1000
   */
  prductAmount?: number;

  /**
   * 제품명
   * @example 제품명
   */
  prductNm?: string;

  /** 제품 평가에 속해 있는 설문지 리스트 */
  qestnrList?: UserEvaluateQestnrInfoResponse[];

  /**
   * 의뢰콘텐츠 (V:영상, P:제품)
   * @example P
   */
  reqestCntnts?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰상태 ([IN001] 10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중 (승인), 90:취소 요청, 100:취소 완료, 110:완료)
   * @example 80
   */
  reqestStatus?: string;

  /**
   * 참여 가능 성별(A: 전체, M:남성, F:여성)
   * @example A
   */
  sexdstn?: string;
}

export interface UserEvaluateGoodsListResponse {
  /**
   * 제품 설정 나이대
   * @example 20,30
   */
  age?: string;

  /**
   * 카테고리 리스트
   * @example {"categoryId":54,"categoryNm":"영화"}
   */
  categoryList?: UserEvaluateGoodsCategoryResponse[];

  /**
   * 평가상태 ([IN007] 10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   * @example 10
   */
  evlStatus?: string;

  /**
   * 의뢰 설정 참여 인원
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 포인트
   * @format int32
   * @example 10
   */
  point?: number;

  /**
   * 제품명
   * @example 제품명
   */
  prductNm?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰 타입(P:프리미엄, B:기본)
   * @example P
   */
  reqestTy?: string;

  /**
   * 제품 설정 성별
   * @example M
   */
  sexdstn?: string;

  /**
   * 썸네일 URL
   * @example test@Test.com
   */
  url?: string;
}

export interface UserEvaluateGoodsQuestAnswerResponse {
  /**
   * 답변 내용
   * @example 답변 내용
   */
  answerCn?: string;

  /**
   * 답변 id
   * @format int32
   * @example 1
   */
  answerId?: number;

  /**
   * 순번
   * @format int32
   * @example 1
   */
  answerSn?: number;

  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /** 내 답변 */
  myAnswer?: string;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 이동할 문항 번호
   * @format int32
   * @example 1
   */
  skipToQestnNo?: number;

  /**
   * 함정 답변 유무
   * @example N
   */
  trapAnswerYn?: string;

  /**
   * 첨부파일 url
   * @example http://test.com
   */
  url?: string;
}

export interface UserEvaluateGoodsQuestnResponse {
  /** 답변 리스트 */
  answerList?: UserEvaluateGoodsQuestAnswerResponse[];

  /**
   * 답변유형 (001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지답변)
   * @example 003
   */
  answerTy?: string;

  /**
   * 질문 내용
   * @example 1
   */
  qestnCn?: string;

  /**
   * 질문id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 질문 번호
   * @format int32
   * @example 1
   */
  qestnNo?: number;

  /**
   * 질문 순번
   * @example 1
   */
  qestnSn?: string;

  /**
   * 질문등장시간
   * @example 00:00:00
   */
  qestnTime?: Time;

  /**
   * 질문등장시간(초)
   * @example 120
   */
  qestnTimeSec?: string;

  /**
   * 응답옵션 (S:단일응답, M:중복응답)
   * @example S
   */
  rspnsOption?: string;
}

export interface UserEvaluateGoodsQuestnrResponse {
  /**
   * 설명
   * @example 설문에 대한 설명
   */
  dc?: string;

  /**
   * 평가방식 (001:사전설문, 002:실시간설문, 003:사후설문, 004최종평가)
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 설문지타입 (001:의뢰, 002:의뢰(커스프), 003:데일리 )
   * @example 001
   */
  qestnrTy?: string;

  /** 질문 리스트 */
  questnList?: UserEvaluateGoodsQuestnResponse[];
}

export interface UserEvaluateInsertAnswerResponse {
  error?: ErrorVo;

  /** 최종 설문지 답변 여부(Y: 최종 설문지 제출 N:참여할 설문지가 더 있음) */
  nextStep?: string;
}

export interface UserEvaluateQestnrInfoResponse {
  /**
   * 설문지 설명
   * @example dc
   */
  dc?: string;

  /**
   * 평가방식 (001:사전설문, 002:실시간설문, 003:사후설문, 004최종평가)
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문지 완료 여부(Y:완료 N:미완료)
   * @example Y
   */
  evlYn?: string;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 평가방식 명
   * @example 사전질문
   */
  qestnrNm?: string;

  /**
   * 설문지 타입
   * @example dc
   */
  qestnrTy?: string;
}

/**
 * 평가신청 다시보기 영상 목록 조회 Response
 */
export interface UserEvaluateReplayVidoPageInfoSearchResponse {
  /** 영상 파일 리스트 */
  attachFileInfoResponse?: AttachFileInfoResponse[];

  /**
   * 영상 카테고리 리스트 (50:TV,방송, 51:키즈,애니, 52:음악,댄스, 53:엔터테인먼트, 54:영화, 55:교양,정보,다큐, 56:예술,공연, 57:개인방송, 64:기타)
   * @example 50
   */
  categoryList?: UserEvaluateVidoCategoryResponse[];

  /**
   * 목록 순서
   * @example 1, 2, 3
   */
  evlSort?: string;

  /**
   * 목록 순서명
   * @example '', 의뢰완료, 평가완료
   */
  evlSortNm?: string;

  /**
   * 평가상태 (10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   * @example 10
   */
  evlStatus?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 영상 제목
   * @example 동영상 제목
   */
  mvpSubject?: string;

  /**
   * 포인트
   * @format int32
   * @example 10
   */
  point?: number;

  /**
   * 영상 다시보기 여부
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰상태 ([IN001] 10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 80
   */
  reqestStatus?: string;

  /**
   * 영상 재생시간
   * @example 120분
   */
  vidoRevivTimeMin?: string;
}

/**
 * 평가신청 다시보기 영상 목록 조회 Response
 */
export interface UserEvaluateReplayVidoSearchResponse {
  error?: ErrorVo;

  /** 평가신청 다시보기 영상 목록 */
  pagingList?: PagingInfoUserEvaluateReplayVidoPageInfoSearchResponse;

  /**
   * 큐레이팅명
   * @example 님에게 딱 맞는 평가 콘텐츠!
   */
  qratingNm?: string;
}

/**
 * 평가 신청 영상 구독 Request
 */
export interface UserEvaluateSubscriptionVidoRequest {
  /**
   * 의뢰 id
   * @format int32
   * @example 27
   */
  reqestId?: number;

  /**
   * 구독타임라인(구독한 영상시점)
   * @example 00:15:27
   */
  subscriptionTimeline?: string;

  /**
   * 구독여부
   * @example Y
   */
  subscriptionYn?: string;
}

/**
 * 평가 신청 영상 구독 Response
 */
export interface UserEvaluateSubscriptionVidoResponse {
  error?: ErrorVo;

  /**
   * 구독일시
   * @example 2021-05-31 00:00:00
   */
  subscriptionDt?: string;

  /**
   * 구독타임라인(구독한 영상시점)
   * @example 00:15:27
   */
  subscriptionTimeline?: string;

  /**
   * 구독여부
   * @example Y
   */
  subscriptionYn?: string;

  /**
   * 영상 id
   * @format int32
   * @example 1
   */
  vidoId?: number;
}

export interface UserEvaluateVidoAnswerInsertRequest {
  /**
   * 불성실평가하이라이트판단여부(불성실답변처리 확인용)
   * @example N
   */
  dishonestEvlHighlightYn?: string;

  /**
   * 불성실평가스티커판단여부(불성실답변처리 확인용)
   * @example N
   */
  dishonestEvlStickerYn?: string;

  /**
   * 불성실평가함정답변판단여부(불성실답변처리 확인용)
   * @example N
   */
  dishonestEvlTrapAnswerYn?: string;

  /**
   * 하이라이트종료구간퍼센트
   * @format int32
   * @example 50
   */
  highlightEndPercent?: number;

  /**
   * 하이라이트종료구간초
   * @format int32
   * @example 160
   */
  highlightEndSecond?: number;

  /**
   * 하이라이트시작구간퍼센트
   * @format int32
   * @example 25
   */
  highlightStartPercent?: number;

  /**
   * 하이라이트시작구간초
   * @format int32
   * @example 30
   */
  highlightStartSecond?: number;

  /** 평가자 답변 등록 */
  qestnrAnswerList?: UserQestnrAnswerInsertRequest[];
}

/**
 * 평가자 영상 카테고리 Response
 */
export interface UserEvaluateVidoCategoryResponse {
  /**
   * 영상카테고리 id (50:TV,방송, 51:키즈,애니, 52:음악,댄스, 53:엔터테인먼트, 54:영화, 55:교양,정보,다큐, 57:개인방송, 64:기타, 65:예술,공연)
   * @format int32
   * @example 55
   */
  categoryId?: number;

  /**
   * 영상카테고리 이름
   * @example ["교양,정보,다큐"]
   */
  categoryNm?: string;
}

/**
 * 평가 신청 상세 조회 Response
 */
export interface UserEvaluateVidoInfoResponse {
  /** 영상 파일 리스트 */
  attachFileInfoResponse?: AttachFileInfoResponse;

  /**
   * 카테고리 리스트
   * @example 영상카테고리 (50:TV,방송, 51:키즈,애니, 52:음악,댄스, 53:엔터테인먼트, 54:영화, 55:교양,정보,다큐, 56:예술,공연, 57:개인방송, 64:기타)
   */
  categoryList?: UserEvaluateVidoCategoryResponse[];

  /**
   * 의뢰자 코멘트
   * @example 의뢰자 코멘트
   */
  comment?: string;

  /**
   * 통합컨텐츠 카드번호(업로드영상 해당)
   * @format int32
   * @example 23658721
   */
  contentCardNo?: number;

  /**
   * 영상설명
   * @example 의뢰자 작성 동영상 설명 영역
   */
  dc?: string;
  error?: ErrorVo;

  /**
   * 평가상태
   * @example 평가상태 (10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   */
  evlStatus?: string;

  /**
   * 평가자 id
   * @format int32
   * @example 115
   */
  memberId?: number;

  /**
   * 동영상 채널 URL
   * @example https://www.youtube.com/watch?v=test
   */
  mvpChnnelUrl?: string;

  /**
   * 영상 제목
   * @example 동영상 제목
   */
  mvpSubject?: string;

  /**
   * 영상 URL(Youtube 해당)
   * @example https://www.youtube.com/watch?v=test
   */
  mvpUrl?: string;

  /**
   * 평가 완료시 지급 포인트
   * @format int32
   * @example 1000
   */
  point?: number;

  /**
   * 질문등장시간
   * @example 00:00:00
   */
  qestnTime?: Time;

  /**
   * 질문등장시간(초)
   * @example 120
   */
  qestnTimeSec?: string;

  /**
   * 영상 다시보기 여부
   * @example Y
   */
  replayYn?: string;

  /**
   * 의뢰명
   * @example 의뢰명
   */
  reqestNm?: string;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /** 설문 리스트 */
  researchList?: UserEvaluateGoodsQuestnrResponse[];

  /**
   * 구분 (LK:LINK, UL:업로드)
   * @example LK
   */
  se?: string;

  /**
   * 영상 id
   * @format int32
   * @example 1
   */
  vidoId?: number;

  /**
   * 영상 재생시간
   * @example 120분
   */
  vidoRevivTimeMin?: string;

  /**
   * 영상 조회수
   * @format int32
   * @example 17000
   */
  vidoViewCnt?: number;
}

/**
 * 평가신청 영상 목록 조회  Response
 */
export interface UserEvaluateVidoPageInfoSearchResponse {
  /** 영상 파일 리스트 */
  attachFileInfoResponse?: AttachFileInfoResponse[];

  /**
   * 영상 카테고리 리스트 (50:TV,방송, 51:키즈,애니, 52:음악,댄스, 53:엔터테인먼트, 54:영화, 55:교양,정보,다큐, 56:예술,공연, 57:개인방송, 64:기타)
   * @example 50
   */
  categoryList?: UserEvaluateVidoCategoryResponse[];

  /**
   * 목록 순서
   * @example 1, 2, 3
   */
  evlSort?: string;

  /**
   * 목록 순서명
   * @example '', 의뢰완료, 평가완료
   */
  evlSortNm?: string;

  /**
   * 평가상태 (10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈)
   * @example 10
   */
  evlStatus?: string;

  /**
   * 목표평가자수
   * @format int32
   * @example 100
   */
  goalEvlManCnt?: number;

  /**
   * 영상 제목
   * @example 동영상 제목
   */
  mvpSubject?: string;

  /**
   * 포인트
   * @format int32
   * @example 10
   */
  point?: number;

  /**
   * 의뢰 id
   * @format int32
   * @example 1
   */
  reqestId?: number;

  /**
   * 의뢰상태 ([IN001] 10:작성 중, 20:견적요청, 30:견적산출 중, 40:결재대기, 50:심사 대기, 60:심사 중, 70:심사 반려, 80:진행중, 90:취소 완료, 100:완료, 110:의뢰중지)
   * @example 80
   */
  reqestStatus?: string;

  /**
   * 영상 재생시간
   * @example 120분
   */
  vidoRevivTimeMin?: string;
}

/**
 * 평가신청 영상 목록 조회  Response
 */
export interface UserEvaluateVidoSearchResponse {
  /** 영상 파일 리스트 */
  attachFileInfoResponse?: AttachFileInfoResponse;
  error?: ErrorVo;

  /** 평가신청 영상 목록 */
  pagingList?: PagingInfoUserEvaluateVidoPageInfoSearchResponse;

  /**
   * 큐레이팅명
   * @example 님에게 딱 맞는 평가 콘텐츠!
   */
  qratingNm?: string;
}

export interface UserEvaluatorBasicInfoResponse {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 환불 받을 계좌번호
   * @example 123456789
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울 강남구
   */
  address?: string;

  /**
   * 마케팅 정보 수신 동의
   * @example Y
   */
  advrtsProvisionAgreYn?: string;

  /**
   * 알람 설정 여부
   * @example Y
   */
  alarmYn?: string;

  /**
   * 환불 받을 은행 코드
   * @example 001
   */
  bankCd?: string;

  /**
   * 환불 받을 은행
   * @example 신한
   */
  bankNm?: string;

  /**
   * 생년월일
   * @example 19000101
   */
  birthday?: string;

  /**
   * 상세 주소
   * @example 12-11
   */
  detailaddress?: string;

  /**
   * 이메일
   * @example test@test.com
   */
  email?: string;

  /**
   * 이메일 수신 동의 여부
   * @example Y
   */
  emailRecptnYn?: string;
  error?: ErrorVo;

  /**
   * 필수영상시청 여부(Y,N)
   * @example Y
   */
  essntlVidoCtyhllYn?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * OTP 가입 여부
   * @example Y
   */
  otpYn?: string;

  /**
   * 성별(M:남자 F:여자)
   * @example M
   */
  sexdstn?: string;

  /**
   * sms 수신 동의 여부
   * @example Y
   */
  smsRecptnYn?: string;

  /**
   * 홍길동
   * @example blocksmith
   */
  username?: string;

  /**
   * 우편번호
   * @example 121111
   */
  zipcode?: string;
}

export interface UserEvaluatorCheckMbtlnumResponse {
  /**
   * 휴대폰 번호 중복 여부(중복이면 true, 아니면 false)
   * @example false
   */
  checkMbtlnum?: boolean;
  error?: ErrorVo;
}

export interface UserEvaluatorCheckPwdRequest {
  /**
   * 비밀번호
   * @example Cusp1234!@
   */
  password?: string;

  /**
   * 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserEvaluatorCheckPwdResponse {
  error?: ErrorVo;

  /**
   * 입력한 비밀번호가 맞는지 여부 ( 맞으면: true, 틀리면 : false)
   * @example true
   */
  result?: string;
}

export interface UserEvaluatorContractInfoResponse {
  /**
   * 첨부 파일 아이디
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 계약서 아이디
   * @format int32
   * @example 1
   */
  contractId?: number;

  /**
   * 계약서구분(001:위촉계약서, 002:비밀유지계약서, 003:윤리서약서, 004:사업자등록증, 005:용역대행계약서)
   * @example 001
   */
  contractSection?: string;
  error?: ErrorVo;

  /**
   * 첨부 파일 url
   * @example http://test.com
   */
  fileUrl?: string;
}

export interface UserEvaluatorContractModifyRequest {
  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 계약석 id
   * @format int32
   * @example 1
   */
  contractId?: number;
}

export interface UserEvaluatorDefraymentReqstInfoResponse {
  /**
   * 예금주
   * @example 신혜선
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 012234142112
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울특별시 강남구 논현동 13-13
   */
  address?: string;

  /**
   * 은행코드
   * @example 032
   */
  bankCd?: string;

  /**
   * 주소상세
   * @example AN빌딩 2층
   */
  detailaddress?: string;
  error?: ErrorVo;

  /**
   * 보유포인트
   * @example 50000
   */
  holdPoint?: string;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 우편번호
   * @example 06317
   */
  zipcode?: string;
}

export interface UserEvaluatorDefraymentReqstInsertRequest {
  /**
   * 예금주
   * @example 신혜선
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 012234142112
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울특별시 강남구 논현동 13-13
   */
  address?: string;

  /**
   * 은행코드
   * @example 032
   */
  bankCd?: string;

  /**
   * 주소상세
   * @example AN빌딩 2층
   */
  detailaddress?: string;

  /**
   * 식별자1
   * @example 820202
   */
  identifier1: string;

  /**
   * 식별자2
   * @example 1231231
   */
  identifier2: string;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 출금신청금액
   * @format int32
   * @example 50000
   */
  reqstPoint: number;

  /**
   * 우편번호
   * @example 06317
   */
  zipcode?: string;
}

export interface UserEvaluatorDefraymentReqstInsertResponse {
  /**
   * 예금주
   * @example 신혜선
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 012234142112
   */
  acnutno?: string;

  /**
   * 주소
   * @example 서울특별시 강남구 논현동 13-13
   */
  address?: string;

  /**
   * 은행코드
   * @example 032
   */
  bankCd?: string;

  /**
   * 처리자ID
   * @format int32
   * @example 1
   */
  chargerId?: number;

  /**
   * 출금상태 - "00":신청, "10":출금완료, "20":출금반려
   * @example 00
   */
  defraySttus?: string;

  /**
   * 주소상세
   * @example AN빌딩 2층
   */
  detailaddress?: string;
  error?: ErrorVo;

  /**
   * 식별자1
   * @example 820202
   */
  identifier1?: string;

  /**
   * 식별자2
   * @example 1231231
   */
  identifier2?: string;

  /**
   * 휴대폰번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 출금신청금액
   * @format int32
   * @example 50000
   */
  reqstPoint?: number;

  /**
   * 반려사유
   * @example 부적절한 포인트 적립 내역이 존재
   */
  returnResn?: string;

  /**
   * 우편번호
   * @example 06317
   */
  zipcode?: string;
}

export interface UserEvaluatorInsertRequest {
  /**
   * 위촉 계약서 파일 아이디
   * @format int32
   * @example 1
   */
  attachFileId001?: number;

  /**
   * 비밀유지 계약서 파일 아이디
   * @format int32
   * @example 1
   */
  attachFileId002?: number;

  /**
   * 윤리 계약서 파일 아이디
   * @format int32
   * @example 1
   */
  attachFileId003?: number;

  /**
   * 생년월일
   * @example 19990101
   */
  birthday?: string;

  /**
   * ci
   * @example 123abc
   */
  ci?: string;

  /**
   * 이메일
   * @example blocksmith@blocksmith.xyz
   */
  email?: string;

  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 비밀번호
   * @example 1234
   */
  password?: string;

  /**
   * 성별
   * @example F or M
   */
  sexdstn?: string;

  /**
   * 약관 아이디 배열
   * @example [1,2]
   */
  termsIds?: number[];

  /**
   * 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserEvaluatorInsertResponse {
  error?: ErrorVo;

  /**
   * id
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 등록일
   * @example 1900-01-01 00:00:00
   */
  regDt?: string;

  /**
   * 서비스
   * @example W
   */
  service?: string;

  /**
   * 계정 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserEvaluatorModifyAddressRequest {
  /**
   * 주소
   * @example 서울시 강남구
   */
  address?: string;

  /**
   * 상세 주소
   * @example 11-1
   */
  detailaddress?: string;

  /**
   * 우편번호
   * @example 12345
   */
  zipcode?: string;
}

export interface UserEvaluatorModifyAdvrtsRequest {
  /**
   * 광고성 정보 수신동의(Y, N)
   * @example Y
   */
  advrtsProvisionAgreYn?: string;
}

export interface UserEvaluatorModifyAlarmRequest {
  /**
   * 알림 설정(Y,N)
   * @example Y
   */
  alarmYn?: string;
}

export interface UserEvaluatorModifyBankRequest {
  /**
   * 예금주
   * @example 홍길동
   */
  acnutnm?: string;

  /**
   * 계좌번호
   * @example 123123213
   */
  acnutno?: string;

  /**
   * 은행 명
   * @example 신한은행
   */
  bankNm?: string;
}

export interface UserEvaluatorModifyEmailRequest {
  /**
   * email
   * @example test@blocksmith.xyz
   */
  email?: string;
}

export interface UserEvaluatorModifyMbtlnumRequest {
  /**
   * 휴대폰 번호
   * @example 01012345454
   */
  mbtlnum?: string;
}

export interface UserEvaluatorPointInfoResponse {
  /**
   * 누적 출금 포인트
   * @example 250,010
   */
  accmltDefPoint?: string;
  error?: ErrorVo;

  /**
   * 오늘 적립 예정 포인트
   * @example 400
   */
  todayAccmlPoint?: string;

  /**
   * 사용 가능한 포인트
   * @example 30,000
   */
  usePosblPoint?: string;
}

export interface UserEvaluatorPointResponse {
  /**
   * 내용(사유)
   * @example 다른 제품 홍보 영상 take2
   */
  contents?: string;

  /**
   * 불성실 평가
   * @example 불성실한 평가가 감지되었습니다.
   */
  dishonestEvl?: string;

  /**
   * 포인트
   * @example + 2,500(5,000)
   */
  point?: string;

  /**
   * 포인트 상태
   * @example A
   */
  pointSttus?: string;

  /**
   * 포인트 상태명
   * @example 적립 완료
   */
  pointSttusNm?: string;

  /**
   * 날짜
   * @example 2021.01.01 00:00:00
   */
  regDt?: string;
}

export interface UserEvaluatorSecessionInsertRequest {
  /**
   * 탈퇴 사유 내용
   * @example 탈퇴 사유 내용
   */
  secessionContents?: string;

  /**
   * 탈퇴 사유(MA004) )
   * @example 001
   */
  secessionReason?: string;
}

export interface UserEvaluatorUpgradeRequest {
  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;
}

export interface UserFindIdResponse {
  error?: ErrorVo;
  username?: string;
}

export interface UserJoinCertificationCheckRequest {
  /**
   * 인증번호
   * @example 123456
   */
  code?: string;

  /**
   * 메시지 아이디
   * @example sdf123
   */
  messageId?: string;
}

export interface UserJoinCertificationRequest {
  /**
   * 휴대폰 번호
   * @example 01012341234
   */
  mbtlnum?: string;
}

export interface UserJoinCertificationResponse {
  /**
   * 인증코드 (개발기에서만 내려옴)
   * @example 180111
   */
  code?: string;
  error?: ErrorVo;

  /**
   * 인증 유효시간(초)
   * @format int32
   * @example 180
   */
  invalidateSeconds?: number;

  /**
   * 휴대폰번호
   * @example 01071178214
   */
  mbtlnum?: string;

  /**
   * 메세지아이디
   * @example 76b1a1c5-d4bf-554e-b027-f96d66cd919d
   */
  messageId?: string;
}

export interface UserMe {
  /**
   * 통합계정 식별자
   * @format int32
   * @example 1
   */
  accountId?: number;

  /** 사용자알림 모음 오브젝트 */
  alarms?: Alarms;

  /**
   * 생년월일
   * @example 19900101
   */
  birthday?: string;

  /**
   * email
   * @example test@test.com
   */
  email?: string;
  error?: ErrorVo;

  /**
   * 평가자 등급
   * @example W
   */
  grad?: string;

  /**
   * 회원식별자
   * @format int32
   * @example 1
   */
  id?: number;

  /**
   * 휴대폰번호
   * @example 01012345678
   */
  mbtlnum?: string;

  /**
   * 이름
   * @example 홍길동
   */
  name?: string;

  /**
   * 사용자 구분(E:평가자 ,C:의뢰자)
   * @example E
   */
  section?: string;

  /**
   * 성별
   * @example M
   */
  sexdstn?: string;

  /**
   * 계정
   * @example blocksmith
   */
  username?: string;
}

export interface UserMemberChangePwdRequest {
  /**
   * 새로운 비밀번호
   * @example Cusp1234!@
   */
  newPassword?: string;
}

export interface UserMemberCheckEmailResponse {
  /**
   * 이메일 중복 여부(있으면 true, 없으면 false)
   * @example true
   */
  checkEmail?: boolean;
  error?: ErrorVo;
}

export interface UserMemberFindIdConfirmRequest {
  /**
   * 인증번호
   * @example 123456
   */
  code?: string;

  /**
   * 휴대 번호
   * @example 01012341234
   */
  mbtlnum?: string;

  /**
   * 메시지 id
   * @example sdfsdf123
   */
  messageId?: string;

  /**
   * 회원명
   * @example 홍길동
   */
  name?: string;
}

export interface UserMemberFindIdSendSmsResponse {
  /**
   * 인증코드 (개발기에서만 내려옴)
   * @example 180111
   */
  code?: string;
  error?: ErrorVo;

  /**
   * 인증 유효시간(초)
   * @format int32
   * @example 180
   */
  invalidateSeconds?: number;

  /**
   * 휴대폰번호
   * @example 01071178214
   */
  mbtlnum?: string;

  /**
   * 메세지아이디
   * @example 76b1a1c5-d4bf-554e-b027-f96d66cd919d
   */
  messageId?: string;

  /**
   * 회원명
   * @example 홍길동
   */
  name?: string;
}

export interface UserMemberFindPasswordSendSmsResponse {
  /**
   * 인증코드 (개발기에서만 내려옴)
   * @example 180111
   */
  code?: string;
  error?: ErrorVo;

  /**
   * 인증 유효시간(초)
   * @format int32
   * @example 180
   */
  invalidateSeconds?: number;

  /**
   * 휴대폰번호
   * @example 01071178214
   */
  mbtlnum?: string;

  /**
   * 메세지아이디
   * @example 76b1a1c5-d4bf-554e-b027-f96d66cd919d
   */
  messageId?: string;

  /**
   * 사용자 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface UserMemberRefreshTokenRequest {
  /** 최초 인증시 발행한 Refresh Token */
  refreshToken?: string;
}

export interface UserMemberRefreshTokenResponse {
  /**
   * access token
   * @example skjfskldjf-sdf
   */
  accessToken?: string;
  error?: ErrorVo;

  /**
   * refresh token
   * @example skjfskldjf-sdf
   */
  refreshToken?: string;
}

export interface UserMemberSendEmailRequest {
  /**
   * 이메일 주소
   * @example jidpara@blocksmith.xyz
   */
  email?: string;

  /**
   * 사용자 계정 아이디
   * @example objgogo041905
   */
  username?: string;
}

/**
 * 미디어 컨버터 오류 유저 메타데이터
 */
export interface UserMetadata {
  /**
   * 고유식별자ID
   * @example 22595b2e-025d-49ca-a2b3-eb35e4b3d605
   */
  guid?: string;

  /**
   * 워크플로우명
   * @example cuspvideoservice
   */
  workflow?: string;
}

/**
 * 평가자 답변 등록 Response
 */
export interface UserQestnrAnswerInsertRequest {
  /**
   * 답변 내용
   * @example 답변 내용(006:하이라이트 일 경우, 형식 통일 필요. 120~320)
   */
  answerCn?: string;

  /**
   * 답변 id
   * @format int32
   * @example 833
   */
  answerId?: number;

  /**
   * 답변 순번
   * @format int32
   * @example 1
   */
  answerSn?: number;

  /**
   * 답변유형 (001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지답변)
   * @example 003
   */
  answerTy?: string;

  /**
   * 질문 id
   * @format int32
   * @example 206
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 60
   */
  qestnrId?: number;
}

/**
 * 후원 대시보드 상세 > 평가자 최종평가 리스트 Response
 */
export interface UserReqestDashboardFinalQnAResponse {
  /**
   * 최종평가 질문 답변
   * @example 답변
   */
  answerCn?: string;

  /**
   * 최종평가 질문
   * @example 해당 구간을 가장 인상깊은 장면으로 뽑은 이유는 무엇인가요?
   */
  qestnCn?: string;
}

/**
 * 의뢰 대시보드 상세 > 평가자 설문 항목 리스트 Response
 */
export interface UserReqestDashboardQnAResponse {
  /**
   * 평가자 답변
   * @example 진돗개
   */
  answerCn?: string;

  /**
   * 첨부파일 id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /** 영상 파일 리스트 */
  attachFileInfoResponse?: AttachFileInfoResponse;

  /**
   * 의뢰자 질문
   * @example 좋아하는 강아지 종은 무엇인가요?
   */
  qestnCn?: string;

  /**
   * 질문 번호
   * @example Q1
   */
  qestnNo?: string;

  /**
   * 설문지타입 (객관식 단일, 객관식 복수, 평가형, 이미지 단일, 이미지 복수, 주관식)
   * @example 객관식 단일
   */
  qestnrType?: string;

  /**
   * 행 순번
   * @format int32
   * @example 1
   */
  rowNum?: number;
}

/**
 * 의뢰 대시보드 목록 Response
 */
export interface UserReqestDashboardSearchResponse {
  /**
   * 연령층
   * @format int32
   * @example 30
   */
  age?: number;

  /**
   * 평가 소요 시간 (일)
   * @example 5
   */
  daydiff?: string;
  error?: ErrorVo;

  /**
   * 총 평가자 수
   * @format int32
   * @example 61
   */
  evlCnt?: number;

  /**
   * 평가완료일시
   * @example 2021-05-31 00:00:00
   */
  evlEndDt?: string;

  /** 최종평가 리스트 */
  evlFinalQnAList?: UserReqestDashboardFinalQnAResponse[];

  /** 설문 리스트 */
  evlResearchList?: UserReqestDashboardQnAResponse[];

  /**
   * 평가시작일시
   * @example 2021-05-31 00:00:00
   */
  evlStartDt?: string;

  /**
   * 평가 소요 시간 (시분초)
   * @example 01:15:00
   */
  hmsdiff?: string;

  /**
   * 평가자 id
   * @format int32
   * @example 115
   */
  memberId?: number;

  /**
   * 평가자
   * @example 평가자
   */
  memberNM?: string;

  /**
   * 평가자 번호
   * @format int32
   * @example 1
   */
  rowNum?: number;

  /**
   * 성별 (M / F)
   * @example M
   */
  sexdstn?: string;
}

export interface UserSecessionInsertRequest {
  /**
   * 탈퇴 사유 내용
   * @example 탈퇴 사유 내용
   */
  secessionContents?: string;

  /**
   * 탈퇴 사유(MA004) )
   * @example 001
   */
  secessionReason?: string;
}

export interface UserSmsCertificationCheckReqest {
  /**
   * 인증번호
   * @example 123456
   */
  code?: string;

  /**
   * 메시지 아이디
   * @example sdf123
   */
  messageId?: string;
}

/**
 * 의뢰 콘텐츠 설정 (영상) 썸네일 등록 Response
 */
export interface VidoThumbnailInfo {
  /**
   * 썸네일파일id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 파일명
   * @example 도넛
   */
  fileName?: string;

  /**
   * 파일url
   * @example https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/board/event/20210423/59de7024-c5c1-47de-9224-f72b2fa4de2a.png
   */
  fileUrl?: string;

  /**
   * 사용여부
   * @example N
   */
  useYn?: string;
}

/**
 * 의뢰 콘텐츠 설정 (영상) 썸네일 등록&수정
 */
export interface VidoThumbnailMerge {
  /**
   * 썸네일파일id
   * @format int32
   * @example 1
   */
  attachFileId?: number;

  /**
   * 사용여부
   * @example N
   */
  useYn?: string;
}

export interface WebSocketVo {
  /**
   * 웹소켓서버 호스트 URL
   * @example https://websocket.we-will.link
   */
  host?: string;

  /**
   * 웹소켓서버 WS-STOMP URL
   * @example https://websocket.we-will.link
   */
  subUrl?: string;

  /**
   * 웹소켓서버 호스트
   * @example https://websocket.we-will.link
   */
  wsUrl?: string;
}

export interface WordCloudChart {
  /**
   * 차트 연령별 조건(전체: 모든 연령대 배열에 넣어주어야 함)
   * @example [10,20,30,40]
   */
  ageList?: number[];

  /**
   * 질문 타입(답변유형 ([CL002] 001:객관식, 002:주관식, 003:평가형, 004:별점, 005:단어입력, 006:하이라이트, 007:이미지))
   * @example 001
   */
  answerTy?: string;

  /**
   * 질문 타입명
   * @example 객관식
   */
  answerTyNm?: string;
  data?: WordCloudResponse[];
  error?: ErrorVo;

  /**
   * 평가 수
   * @format int32
   * @example 100
   */
  evlCnt?: number;

  /**
   * 설문 타입
   * @example 001
   */
  evlMthd?: string;

  /**
   * 설문 타입명
   * @example 001
   */
  evlMthdNm?: string;

  /**
   * 질문 id
   * @format int32
   * @example 1
   */
  qestnId?: number;

  /**
   * 설문지 id
   * @format int32
   * @example 1
   */
  qestnrId?: number;

  /**
   * 차트 성별 조건(전체:A 남:M 여:F)
   * @example A
   */
  sexdstn?: string;

  /**
   * 차트 제목
   * @example 차트 제목
   */
  title?: string;
}

export interface WordCloudRequest {
  /**
   * 연령(전체일 경우 모둔 연령값 넣어야 됨)
   * @example [20,30,40]
   */
  ageList?: number[];

  /**
   * 요청서 id
   * @format int32
   * @example 1
   */
  requestId?: number;

  /**
   * 성별(A:모두 M:남자 F:여자)
   * @example A
   */
  sexdstn?: string;

  /**
   * 워드 크라우드 종류(H:~~뽑은 이유 R:소감)
   * @example H
   */
  type?: string;
}

export interface WordCloudResponse {
  /** 단어 */
  text?: string;

  /**
   * 단어 수
   * @format int32
   */
  value?: number;
}

export interface ProductThumbInfoResponse {
  /** @format int32 */
  attachFileIdtf?: number;

  /** @format int32 */
  no?: number;

  /** @format int32 */
  prductId?: number;
}

export interface ListVoAlarmSearchResponse {
  content?: AlarmSearchResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoCodeGroupListResponse {
  content?: CodeGroupListResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoCodeGroupTreeResponse {
  content?: CodeGroupTreeResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoCodeListResponse {
  content?: CodeListResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchPrductCategoryResponse {
  content?: SearchPrductCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchPrductOrderCategoryResponse {
  content?: SearchPrductOrderCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchVidoCategoryResponse {
  content?: SearchVidoCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoSearchVidoOrderCategoryResponse {
  content?: SearchVidoOrderCategoryResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoUserEvaluatorContractInfoResponse {
  content?: UserEvaluatorContractInfoResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface ListVoWordCloudResponse {
  content?: WordCloudResponse[];
  error?: ErrorVo;

  /** @format int32 */
  size?: number;
}

export interface PageInfoSettlementResponse {
  /** @format int32 */
  endRow?: number;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;
  isFirstPage?: boolean;
  isLastPage?: boolean;

  /** @format int32 */
  lastPage?: number;
  list?: SettlementResponse[];

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int64 */
  total?: number;
}

export interface PagingInfoAdminPointListResponse {
  content?: AdminPointListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAdminPointManageResponse {
  content?: AdminPointManageResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoAlarmSearchResponse {
  content?: AlarmSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoClientCouponResponse {
  content?: ClientCouponResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoCustomerSearchResponse {
  content?: CustomerSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoMemberidPointSumInterface {
  content?: MemberidPointSumInterface[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPaymentResult {
  content?: PaymentResult[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPointDefraymentResponse {
  content?: PointDefraymentResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPointDetail {
  content?: PointDetail[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPointMasterResponse {
  content?: PointMasterResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPostUserEventSearchResponse {
  content?: PostUserEventSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoPostUserNoticeSearchResponse {
  content?: PostUserNoticeSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoReportAnswerResponse {
  content?: ReportAnswerResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserClientMemberDashBoardResponse {
  content?: UserClientMemberDashBoardResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserClientSettlementResponse {
  content?: UserClientSettlementResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserDashboardGoodsPageInfoSearchResponse {
  content?: UserDashboardGoodsPageInfoSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserDashboardVidoPageInfoSearchResponse {
  content?: UserDashboardVidoPageInfoSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserEvaluateGoodsListResponse {
  content?: UserEvaluateGoodsListResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserEvaluateReplayVidoPageInfoSearchResponse {
  content?: UserEvaluateReplayVidoPageInfoSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserEvaluateVidoPageInfoSearchResponse {
  content?: UserEvaluateVidoPageInfoSearchResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface PagingInfoUserEvaluatorPointResponse {
  content?: UserEvaluatorPointResponse[];

  /** @format int32 */
  endRow?: number;
  error?: ErrorVo;

  /** @format int32 */
  firstPage?: number;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;

  /** @format int32 */
  lastPage?: number;

  /** @format int32 */
  navigateFirstPage?: number;

  /** @format int32 */
  navigateLastPage?: number;

  /** @format int32 */
  navigatePages?: number;
  navigatepageNums?: number[];

  /** @format int32 */
  nextPage?: number;

  /** @format int32 */
  pageNum?: number;

  /** @format int32 */
  pageSize?: number;

  /** @format int32 */
  pages?: number;

  /** @format int32 */
  prePage?: number;

  /** @format int32 */
  size?: number;

  /** @format int32 */
  startRow?: number;

  /** @format int32 */
  total?: number;
}

export interface SendNewPasswordUsingGetParams {
  /** username */
  username: string;
}

export interface UploadAttachFileUsingPostParams {
  /**
   * 등록자 아이디(회원가입시 빈값 입력)
   * @format int32
   */
  regId?: number;

  /** 업로드 경로(예시:board/attach) */
  subPath?: string;
}

export interface FindPointDefrayExcelDownUsingGetParams {
  EDate?: string;
  SDate?: string;

  /**
   * 신청자아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 신청자명
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 상태 ("":전체, 00:출금신청, 10:출금완료, 20:출금반려)
   * @example ""
   */
  defraySttus?: string;
}

export interface FindPointDefrayUsingGetParams {
  EDate?: string;
  SDate?: string;

  /**
   * 신청자아이디
   * @example test1
   */
  applcntId?: string;

  /**
   * 신청자명
   * @example 강하늘
   */
  applcntNm?: string;

  /**
   * 상태 ("":전체, 00:출금신청, 10:출금완료, 20:출금반려)
   * @example ""
   */
  defraySttus?: string;

  /**
   * 현재 페이지정보
   * @format int32
   * @example 1
   */
  pageNum: number;

  /**
   * 페이지 크기
   * @format int32
   * @example 10
   */
  pageSize: number;
}

export interface FindAllPointDetailUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId?: number;

  /**
   * no
   * @format int32
   */
  no?: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface FindAllPointMasterUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId?: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface FindGroupAllPointMasterUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId: number;
}

export interface FindGroupPointMasterUsingGetParams {
  /**
   * memberId
   * @format int32
   */
  memberId?: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;
}

export interface FindGroupAdminPointManageUsingGetParams {
  /**
   * email 주소
   * @example example@blocksmith.xyz
   */
  email?: string;

  /**
   * 종료일
   * @example 2021-04-21
   */
  endDt?: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /**
   * 기간검색(등록일:R / 최종접속일:RC)
   * @example R
   */
  periodType?: string;

  /**
   * 검색어 종류값에 따른 검색어 입력
   * @example asdf
   */
  searchKey?: string;

  /**
   * 검색어 종류(USERNAME : 아이디, NAME : 이름, BIRTHDAY : 생년월일
   * @example USERNAME
   */
  searchKnd?: string;

  /**
   * 성별('F':여성, 'M':남성, '':전체)
   * @example ''
   */
  sexdstn?: string;

  /**
   * 시작일
   * @example 2021-04-20
   */
  startDt?: string;
}

export interface FindAdminPointListUsingGetParams {
  /**
   * 성실/불성실("":전체, Y:불성실, N:성실)
   * @example N
   */
  dishonestEvl?: string;

  /**
   * 종료일
   * @example 2021-12-31
   */
  endDt: string;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 4
   */
  pageSize: number;

  /**
   * 포인트 상태 ("":전체, W:적립 예정, A:적립 완료, U:차감, E:소멸, D:출금)
   * @example A
   */
  pointSttus?: string;

  /**
   * 검색어 구분
   * @example 홍길동
   */
  searchKey?: string;

  /**
   * 검색어 종류(NAME : 이름, USERNAME : 아이디, REQUEST_NM : 영상/제품명, POINT : 포인트, OPERATOR_NM : 처리자 이름, OPERATOR_ID : 처리자 아이디)
   * @example NAME
   */
  searchKnd?: string;

  /**
   * 시작일
   * @example 2021-01-01
   */
  startDt: string;

  /**
   * System 여부("":전체, Y:포함, N:불포함
   * @example N
   */
  systemYn?: string;
}

export interface GetCodeNameUsingGetParams {
  /** codeGroupId */
  codeGroupId: string;

  /** codeId */
  codeId?: string;
}

export interface PayCompleteUsingPostParams {
  /** app */
  app?: string;

  /** imp_uid */
  imp_uid?: string;

  /** merchant_uid */
  merchant_uid?: string;
}

export interface PayGenerateUsingPostParams {
  /** amount */
  amount?: string;

  /** app */
  app?: string;

  /** member_id */
  member_id?: string;

  /** order_name */
  order_name?: string;

  /** product_id */
  product_id?: string;
}

export interface SearchSettlementUsingGetParams {
  /** @example 1 */
  setleCode?: string;
}

export interface PaymentResultListPagingUsingGetParams {
  /**
   * 회원시퀀스
   * @format int32
   */
  memSeq: number;

  /** 기간[A:전체 / W:1주일 / 1:1개월 / 3:3개월] */
  period: string;

  /**
   * 페이지번호
   * @format int32
   * @example 1
   */
  pageNo: number;

  /**
   * 페이지당 리스트 수
   * @format int32
   * @example 10
   */
  pageSize: number;
}

export interface SearchAlarmPagingUsingGetParams {
  /**
   * 페이지 번호
   * @format int32
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize?: number;
}

export interface CheckEmailUsingGetParams {
  /**
   * 이메일
   * @example test@blocksmith.xyz
   */
  email: string;
}

export interface CheckClientMemberByEmailUsingGetParams {
  /** email */
  email: string;

  /** username */
  username: string;
}

export interface CheckClientMemberByMobileUsingGetParams {
  /** mbtlnum */
  mbtlnum: string;

  /** username */
  username: string;
}

export interface ExpInfoRequestPrductUsingGetParams {
  /**
   * 의뢰서 아이디
   * @format int32
   */
  requestId: number;
}

export interface InfoClientMemberDashBoardUsingGetParams {
  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;

  /** reqestNm */
  reqestNm?: string;

  /** reqestStatus */
  reqestStatus?: string;
}

export interface InfoRequestPrductUsingGetParams {
  /**
   * 의뢰서 아이디
   * @format int32
   */
  requestId: number;
}

export interface FindClientMemberSetleUsingGetParams {
  /** @example 2021-05-23 */
  endDt?: string;

  /**
   * @format int32
   * @example 4624
   */
  memberId?: number;

  /**
   * 페이지 번호
   * @format int32
   * @example 1
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   * @example 20
   */
  pageSize?: number;

  /** @example paid */
  setleSttus?: string;

  /** @example 2021-05-21 */
  startDt?: string;
}

export interface FindAllClientCouponUsingGetParams {
  /**
   * @format int32
   * @example 1
   */
  memberId: number;

  /**
   * pageNo
   * @format int32
   */
  pageNo: number;

  /**
   * pageSize
   * @format int32
   */
  pageSize: number;

  /**
   * 무시(내부사용용)
   * @format int32
   * @example 1
   */
  startRownum?: number;
}

export interface FindCouponDscntCalcUsingGetParams {
  clientCouponNoList?: number[];

  /**
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * @format int32
   * @example 1280000
   */
  splpcAm?: number;

  /**
   * @format int32
   * @example 128000
   */
  vat?: number;

  /**
   * prqudoId
   * @format int32
   */
  prqudoId: number;
}

export interface SearchCustomerUsingGetParams {
  /**
   * 회원id
   * @format int32
   */
  memberId?: number;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo?: number;

  /**
   * 페이지당 리스트 수
   * @format int32
   */
  pageSize?: number;
}

export interface FindIdByEmailUsingGetParams {
  /**
   * 이메일
   * @example test@test.com
   */
  email: string;

  /**
   * 이름
   * @example 홍길동
   */
  name: string;
}

export interface SearchEssentialVideoUsingGetParams {
  /**
   * 회원id
   * @format int32
   */
  memberId?: number;
}

export interface InsertReqestEstimateUsingPostParams {
  /**
   * reqestId
   * @format int32
   */
  reqestId: number;
}

export interface SearchEvaluateGoodsAllUsingGetParams {
  /**
   * 카테고리 id
   * @format int32
   */
  categoryId?: number;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo?: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize?: number;

  /** 제품명 */
  productNm?: string;
}

export interface InsertMemberStickerClickStatusUsingPostParams {
  /** 스티커클릭여부(Y/N) */
  evlSticker: string;

  /**
   * requestId
   * @format int32
   */
  requestId: number;
}

export interface SearchUserEvaluateVidoUsingGetParams {
  /**
   * 카테고리 ID
   * @format int32
   */
  categoryId?: number;

  /** 영상제목 검색 */
  keyword?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface SelectUserEvaluateReplyVidoUsingGetParams {
  /**
   * 카테고리 ID
   * @format int32
   */
  categoryId?: number;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface ModifyEvaluatorMemberPwdUsingPutParams {
  /**
   * 새로운 비밀번호
   * @example 1111
   */
  newPassword?: string;

  /**
   * 기존 비밀번호
   * @example 1111
   */
  password?: string;
}

export interface CheckDuplicatedMbtlnumUsingGetParams {
  /** mbtlnum */
  mbtlnum?: string;
}

export interface InfoEvaluatorPointsUsingGetParams {
  /**
   * 종료일
   * @example 2021-06-05
   */
  endDt?: string;

  /**
   * 아이디
   * @format int32
   * @example 1
   */
  memberId?: number;

  /**
   * 페이지번호
   * @format int32
   * @example 1
   */
  pageNo: number;

  /**
   * 페이지크기
   * @format int32
   * @example 10
   */
  pageSize: number;

  /**
   * 상태값 (A:적립 완료, W:적립 예정, D:출금, E:소멸
   * @example A
   */
  pointSttus?: string;

  /**
   * 시작일
   * @example 2021-06-02
   */
  startDt?: string;
}

export interface SearchEvaluateDashboardGoodsUsingGetParams {
  /** 평가상태(10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈, 50:평가완료(불성실답변)) */
  evlStatus?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface SearchEvaluateDashboardVidoUsingGetParams {
  /** 평가상태(10:평가중, 20:평가완료, 30:평가기간만료, 40:이탈, 50:평가완료(불성실답변)) */
  evlStatus?: string;

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface ExpSearchReqestDashboardUsingGetParams {
  /**
   * 평가자 번호
   * @format int32
   */
  rowNum: number;
}

export interface SearchReqestDashboardUsingGetParams {
  /**
   * 평가자 번호
   * @format int32
   */
  rowNum: number;

  /**
   * reqestId
   * @format int32
   */
  reqestId: number;
}

export interface SelectReturnResnInReqestCancelUsingGetParams {
  /**
   * 의뢰ID
   * @format int32
   */
  reqestId: number;
}

export interface SearchUserEventPostUsingGetParams {
  /** 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자) */
  bbsLc?: string[];

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface SelectUserEventPostUsingGetParams {
  /**
   * 로그인 한 회원 ID
   * @format int32
   */
  memberId?: number;

  /**
   * id
   * @format int32
   */
  id: number;
}

export interface SearchUserNoticePostUsingGetParams {
  /** 페이지 노출 위치(코드그룹[CM002] -  001 : 게이트페이지(평가자), 002 : 게이트페이지(의뢰자), 003 : 평가자, 004 : 의뢰자) */
  bbsLc: string[];

  /**
   * 페이지 번호
   * @format int32
   */
  pageNo: number;

  /**
   * 페이지 사이즈
   * @format int32
   */
  pageSize: number;
}

export interface SendEmailUsingPostParams {
  /** email */
  email: string;
}

export interface CheckSendSmsUserUsingPostParams {
  /**
   * 인증번호
   * @example 123456
   */
  code: string;

  /**
   * 메세지아이디
   * @example 76b1a1c5-d4bf-554e-b027-f96d66cd919d
   */
  messageId: string;
}

import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, ResponseType } from "axios";
import qs from "qs";

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, "data" | "params" | "url" | "responseType"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, "data" | "cancelToken"> {
  securityWorker?: (
    securityData: SecurityDataType | null,
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || "//dev1.blocksmith.xyz:28091" });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.instance.defaults.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = (format && this.format) || void 0;

    if (type === ContentType.FormData) {
      const formData = new FormData();
      requestParams.headers.common = { Accept: "*/*" };
      requestParams.headers.post = {};
      requestParams.headers.put = {};

      const fromBody = body as any;
      for (const property in fromBody) {
        formData.append(property, fromBody[property]);
      }
      body = formData;
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
      paramsSerializer: (p) => {
        return qs.stringify(p, { arrayFormat: "repeat" });
      },
    });
  };
}

/**
 * @title WEWILL 서비스 API
 * @version 0.1
 * @license
 * @baseUrl //dev1.blocksmith.xyz:28091
 *
 * WEWILL 서비스 API
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  /**
   * @description 사업자번호를 이용하여 홈택스 API 조회
   *
   * @tags [샘플코드] - Biznum API
   * @name FindByBiznumUsingGet
   * @summary 국세청 홈택스 사업자등록번호 조회
   * @request GET:/api/biznum/{code}
   * @secure
   */
  findByBiznumUsingGet = (code: string, params: RequestParams = {}) =>
    this.request<BiznumResponse, void>({
      path: `/api/biznum/${code}`,
      method: "GET",
      secure: true,
      format: "json",
      ...params,
    });

  alarms = {
    /**
     * @description 알림 발송 테스트
     *
     * @tags [의뢰자] - 알림 API
     * @name SendAlarmUsingGet
     * @summary 알림 발송 테스트
     * @request GET:/api/wewill/common/alarms
     * @secure
     */
    sendAlarmUsingGet: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/common/alarms`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 알림 조회
     *
     * @tags [의뢰자] - 알림 API
     * @name SearchAlarmUsingGet
     * @summary 헤더 > 알림
     * @request GET:/api/wewill/user/alarms
     * @deprecated
     * @secure
     */
    searchAlarmUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoAlarmSearchResponse, void>({
        path: `/api/wewill/user/alarms`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 미확인 알림 갯수
     *
     * @tags [의뢰자] - 알림 API
     * @name AlarmCntUsingGet
     * @summary 미확인 알림 갯수 API
     * @request GET:/api/wewill/user/alarms/alarmCnt
     * @secure
     */
    alarmCntUsingGet: (params: RequestParams = {}) =>
      this.request<AlarmCntResponse, void>({
        path: `/api/wewill/user/alarms/alarmCnt`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 알림 조회(페이징처리)
     *
     * @tags [의뢰자] - 알림 API
     * @name SearchAlarmPagingUsingGet
     * @summary 헤더 > 알림(페이징처리)
     * @request GET:/api/wewill/user/alarms/paging
     * @secure
     */
    searchAlarmPagingUsingGet: (query: SearchAlarmPagingUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAlarmSearchResponse, void>({
        path: `/api/wewill/user/alarms/paging`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  check = {
    /**
     * @description 회원 아이디 중복 체크(통합,wewill 확인)
     *
     * @tags [공통] - 아이디 중복체크 API
     * @name SendNewPasswordUsingGet
     * @summary 회원 아이디 중복 체크(통합,wewill 확인)
     * @request GET:/api/wewill/common/check/username
     * @secure
     */
    sendNewPasswordUsingGet: (query: SendNewPasswordUsingGetParams, params: RequestParams = {}) =>
      this.request<CommonUsernameCheckResponse, void>({
        path: `/api/wewill/common/check/username`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 이메일 중복체크
     *
     * @tags [사용자페이지] - 로그인 API
     * @name CheckEmailUsingGet
     * @summary 이메일 중복 체크 API
     * @request GET:/api/wewill/user/check/email
     * @secure
     */
    checkEmailUsingGet: (query: CheckEmailUsingGetParams, params: RequestParams = {}) =>
      this.request<UserMemberCheckEmailResponse, void>({
        path: `/api/wewill/user/check/email`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 로그인 성공 후 비밀변호 변경일이 지났는지 확인
     *
     * @tags [사용자페이지] - 로그인 API
     * @name CheckNextChangeDtUsingGet
     * @summary 로그인 후 비밀번호 변경일 지났는지 확인 API
     * @request GET:/api/wewill/user/check/pwd/date
     * @secure
     */
    checkNextChangeDtUsingGet: (params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/check/pwd/date`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 아이디,이메일로 회원 정보 확인
     *
     * @tags [사용자페이지] - 로그인 API
     * @name CheckClientMemberByEmailUsingGet
     * @summary 아이디,이메일로 회원 정보 확인 API
     * @request GET:/api/wewill/user/check/user/email
     * @secure
     */
    checkClientMemberByEmailUsingGet: (query: CheckClientMemberByEmailUsingGetParams, params: RequestParams = {}) =>
      this.request<CheckClientMemberResponse, void>({
        path: `/api/wewill/user/check/user/email`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 아이디,휴대폰 번호로 회원 정보 확인
     *
     * @tags [사용자페이지] - 로그인 API
     * @name CheckClientMemberByMobileUsingGet
     * @summary 아이디,휴대폰 번호로 회원 정보 확인 API
     * @request GET:/api/wewill/user/check/user/mobile
     * @secure
     */
    checkClientMemberByMobileUsingGet: (query: CheckClientMemberByMobileUsingGetParams, params: RequestParams = {}) =>
      this.request<CheckClientMemberResponse, void>({
        path: `/api/wewill/user/check/user/mobile`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  codegroups = {
    /**
     * @description 전체 코드그룹 목록 조회
     *
     * @tags [공통] - 공통코드 API
     * @name SelectCodeGroupListUsingGet
     * @summary 공통코드 > 코드그룹 목록 > 전체 코드그룹 목록 조회 API
     * @request GET:/api/wewill/common/codegroups
     * @secure
     */
    selectCodeGroupListUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoCodeGroupListResponse, void>({
        path: `/api/wewill/common/codegroups`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 전체 코드그룹 + 코드목록 조회
     *
     * @tags [공통] - 공통코드 API
     * @name SelectCodeGroupTreeUsingGet
     * @summary 공통코드 > 코드그룹+코드 목록 조회 > 전체 코드그룹 + 코드 목록 조회 API
     * @request GET:/api/wewill/common/codegroups/codes
     * @secure
     */
    selectCodeGroupTreeUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoCodeGroupTreeResponse, void>({
        path: `/api/wewill/common/codegroups/codes`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 입력받은 코드 id값 제외한 코드 목록조회(복수 코드 ID 지원)
     *
     * @tags [공통] - 공통코드 API
     * @name SelectExceptCodeListUsingGet
     * @summary 공통코드 > 코드목록 조회 > 입력 받은 코드 id 값 제외한 코드 목록조회 API
     * @request GET:/api/wewill/common/codegroups/{codeGroupId}/codes/except/{codeId}
     * @secure
     */
    selectExceptCodeListUsingGet: (codeGroupId: string, codeId: string, params: RequestParams = {}) =>
      this.request<ListVoCodeListResponse, void>({
        path: `/api/wewill/common/codegroups/${codeGroupId}/codes/except/${codeId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 코드그룹 id 값 통한 코드목록 조회(복수 코드그룹 ID 지원)
     *
     * @tags [공통] - 공통코드 API
     * @name SelectCodeListByCodeGroupIdsUsingGet
     * @summary 공통코드 > 코드 목록 조회 > 코드그룹 ID 값 통한 코드 목록 조회 API
     * @request GET:/api/wewill/common/codegroups/{id}/codes
     * @secure
     */
    selectCodeListByCodeGroupIdsUsingGet: (id: string, params: RequestParams = {}) =>
      this.request<ListVoCodeListResponse, void>({
        path: `/api/wewill/common/codegroups/${id}/codes`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  file = {
    /**
     * @description 첨부파일 등록 API
     *
     * @tags [공통] - 파일업로드 API
     * @name UploadAttachFileUsingPost
     * @summary 첨부파일 등록 API
     * @request POST:/api/wewill/common/file/upload
     * @secure
     */
    uploadAttachFileUsingPost: (
      query: UploadAttachFileUsingPostParams,
      data: { file?: File },
      params: RequestParams = {},
    ) =>
      this.request<UploadAttachFileResponse, void>({
        path: `/api/wewill/common/file/upload`,
        method: "POST",
        query: query,
        body: data,
        secure: true,
        type: ContentType.FormData,
        format: "document",
        ...params,
      }),
  };
  pointdefrayment = {
    /**
     * @description 포인트 출금 신청 목록 ExcelDown
     *
     * @tags [공통] - 포인트 출금 API
     * @name FindPointDefrayExcelDownUsingGet
     * @summary 포인트 출금 > 출금신청 목록 ExcelDown
     * @request GET:/api/wewill/common/pointdefrayment/defray/excel
     * @deprecated
     * @secure
     */
    findPointDefrayExcelDownUsingGet: (query: FindPointDefrayExcelDownUsingGetParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/common/pointdefrayment/defray/excel`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 출금 신청 목록 조회
     *
     * @tags [공통] - 포인트 출금 API
     * @name FindPointDefrayUsingGet
     * @summary 포인트 출금 > 출금신청 목록 조회
     * @request GET:/api/wewill/common/pointdefrayment/defray/findAll
     * @deprecated
     * @secure
     */
    findPointDefrayUsingGet: (query: FindPointDefrayUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPointDefraymentResponse, void>({
        path: `/api/wewill/common/pointdefrayment/defray/findAll`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 출금 신청 상세 조회
     *
     * @tags [공통] - 포인트 출금 API
     * @name FindPointDefrayInfoUsingGet
     * @summary 포인트 출금 > 출금신청 상세 (팝업) > 조회
     * @request GET:/api/wewill/common/pointdefrayment/defray/{id}
     * @deprecated
     * @secure
     */
    findPointDefrayInfoUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<PointDefraymentInfoResponse, void>({
        path: `/api/wewill/common/pointdefrayment/defray/${id}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 출금 신청 상세 수정
     *
     * @tags [공통] - 포인트 출금 API
     * @name UpdatePointDefraymentInfoUsingPut
     * @summary 포인트 출금 > 출금신청 상세 (팝업) > 수정
     * @request PUT:/api/wewill/common/pointdefrayment/defray/{id}
     * @deprecated
     * @secure
     */
    updatePointDefraymentInfoUsingPut: (
      id: number,
      request: PointDefraymentModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/common/pointdefrayment/defray/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  pointdetail = {
    /**
     * @description 포인트목록 조회 페이징
     *
     * @tags [공통] - 포인트 상세 API
     * @name FindAllPointDetailUsingGet
     * @summary 포인트 상세 전체 조회
     * @request GET:/api/wewill/common/pointdetail
     * @deprecated
     * @secure
     */
    findAllPointDetailUsingGet: (query: FindAllPointDetailUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPointDetail, void>({
        path: `/api/wewill/common/pointdetail`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 상세 저장
     *
     * @tags [공통] - 포인트 상세 API
     * @name RegisterPointDetailUsingPost
     * @summary 포인트 상세 저장
     * @request POST:/api/wewill/common/pointdetail
     * @deprecated
     * @secure
     */
    registerPointDetailUsingPost: (pointDetail: PointDetail, params: RequestParams = {}) =>
      this.request<PointDetail, void>({
        path: `/api/wewill/common/pointdetail`,
        method: "POST",
        body: pointDetail,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 포인트 상세 조회
     *
     * @tags [공통] - 포인트 상세 API
     * @name FindPointDetailUsingGet
     * @summary 포인트 상세 조회
     * @request GET:/api/wewill/common/pointdetail/{no}
     * @deprecated
     * @secure
     */
    findPointDetailUsingGet: (no: number, params: RequestParams = {}) =>
      this.request<PointDetail, void>({
        path: `/api/wewill/common/pointdetail/${no}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트목록 수정
     *
     * @tags [공통] - 포인트 상세 API
     * @name UpdatePointDetailUsingPut
     * @summary 포인트 상세 수정
     * @request PUT:/api/wewill/common/pointdetail/{no}
     * @deprecated
     * @secure
     */
    updatePointDetailUsingPut: (no: number, pointDetail: PointDetail, params: RequestParams = {}) =>
      this.request<PointDetail, void>({
        path: `/api/wewill/common/pointdetail/${no}`,
        method: "PUT",
        body: pointDetail,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  pointmaster = {
    /**
     * @description 포인트 마스터 목록 조회 페이징
     *
     * @tags [공통] - 포인트 마스터 API
     * @name FindAllPointMasterUsingGet
     * @summary 포인트 마스터 전체 조회
     * @request GET:/api/wewill/common/pointmaster
     * @deprecated
     * @secure
     */
    findAllPointMasterUsingGet: (query: FindAllPointMasterUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPointMasterResponse, void>({
        path: `/api/wewill/common/pointmaster`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 저장
     *
     * @tags [공통] - 포인트 마스터 API
     * @name RegisterPointMasterUsingPost
     * @summary 포인트 마스터 저장
     * @request POST:/api/wewill/common/pointmaster
     * @deprecated
     * @secure
     */
    registerPointMasterUsingPost: (request: PointMasterInsertRequest, params: RequestParams = {}) =>
      this.request<PointMasterInsertResponse, void>({
        path: `/api/wewill/common/pointmaster`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 포인트 마스터 Group By 목록 조회 페이징
     *
     * @tags [공통] - 포인트 마스터 API
     * @name FindGroupAllPointMasterUsingGet
     * @summary 포인트 마스터 Group By 개별 조회
     * @request GET:/api/wewill/common/pointmaster/find
     * @deprecated
     * @secure
     */
    findGroupAllPointMasterUsingGet: (query: FindGroupAllPointMasterUsingGetParams, params: RequestParams = {}) =>
      this.request<PointMasterSingleGroupByResponse, void>({
        path: `/api/wewill/common/pointmaster/find`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 Group By 목록 조회 페이징
     *
     * @tags [공통] - 포인트 마스터 API
     * @name FindGroupPointMasterUsingGet
     * @summary 포인트 마스터 Group By 전체 조회
     * @request GET:/api/wewill/common/pointmaster/findAll
     * @deprecated
     * @secure
     */
    findGroupPointMasterUsingGet: (query: FindGroupPointMasterUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoMemberidPointSumInterface, void>({
        path: `/api/wewill/common/pointmaster/findAll`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 다건의 포인트 적립 및 차감
     *
     * @tags [공통] - 포인트 마스터 API
     * @name RegisterAdminPointManageUsingPost
     * @summary 포인트 관리 > 포인트 적립/차감
     * @request POST:/api/wewill/common/pointmaster/manage
     * @deprecated
     * @secure
     */
    registerAdminPointManageUsingPost: (request: AdminPointManageInsertRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/common/pointmaster/manage`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 포인트 관리 > 포인트 적립/차감 Group By 목록 조회 페이징
     *
     * @tags [공통] - 포인트 마스터 API
     * @name FindGroupAdminPointManageUsingGet
     * @summary 포인트 관리 > 포인트 적립/차감 Group By 전체 조회
     * @request GET:/api/wewill/common/pointmaster/manage/findAll
     * @deprecated
     * @secure
     */
    findGroupAdminPointManageUsingGet: (query: FindGroupAdminPointManageUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminPointManageResponse, void>({
        path: `/api/wewill/common/pointmaster/manage/findAll`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 관리 > 포인트 내역 > 포인트 내역 리스트
     *
     * @tags [공통] - 포인트 마스터 API
     * @name FindAdminPointListUsingGet
     * @summary 포인트 관리 > 포인트 내역
     * @request GET:/api/wewill/common/pointmaster/manage/list
     * @deprecated
     * @secure
     */
    findAdminPointListUsingGet: (query: FindAdminPointListUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoAdminPointListResponse, void>({
        path: `/api/wewill/common/pointmaster/manage/list`,
        method: "GET",
        query: query,
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 조회
     *
     * @tags [공통] - 포인트 마스터 API
     * @name FindPointMasterUsingGet
     * @summary 포인트 마스터 조회
     * @request GET:/api/wewill/common/pointmaster/{no}
     * @deprecated
     * @secure
     */
    findPointMasterUsingGet: (no: number, params: RequestParams = {}) =>
      this.request<PointMasterResponse, void>({
        path: `/api/wewill/common/pointmaster/${no}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 포인트 마스터 목록 수정
     *
     * @tags [공통] - 포인트 마스터 API
     * @name UpdatePointMasterUsingPut
     * @summary 포인트 마스터 수정
     * @request PUT:/api/wewill/common/pointmaster/{no}
     * @deprecated
     * @secure
     */
    updatePointMasterUsingPut: (no: number, request: PointMasterModifyRequest, params: RequestParams = {}) =>
      this.request<PointMasterModifyResponse, void>({
        path: `/api/wewill/common/pointmaster/${no}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  samples = {
    /**
     * @description 알림 발송 테스트
     *
     * @tags [공통샘플] - 샘플 API
     * @name SendAlarm2UsingGet
     * @summary 알림 발송 테스트
     * @request GET:/api/wewill/common/samples/alarms
     * @secure
     */
    sendAlarm2UsingGet: (params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/common/samples/alarms`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 코드매니저 코드명 조회 테스트
     *
     * @tags [공통샘플] - 샘플 API
     * @name GetCodeNameUsingGet
     * @summary 코드매니저 코드명 조회 테스트
     * @request GET:/api/wewill/common/samples/codename
     * @secure
     */
    getCodeNameUsingGet: (query: GetCodeNameUsingGetParams, params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/api/wewill/common/samples/codename`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description Exception 테스트2
     *
     * @tags [공통샘플] - 샘플 API
     * @name GetCodeNameExceptionUsingGet
     * @summary Exception 테스트2
     * @request GET:/api/wewill/common/samples/exception
     * @secure
     */
    getCodeNameExceptionUsingGet: (params: RequestParams = {}) =>
      this.request<string, void>({
        path: `/api/wewill/common/samples/exception`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 웹소켓 알림 발송 테스트
     *
     * @tags [공통샘플] - 샘플 API
     * @name SendWebsocketNotificationUsingPost
     * @summary 웹소켓 알림 발송 테스트
     * @request POST:/api/wewill/common/samples/notifications
     * @secure
     */
    sendWebsocketNotificationUsingPost: (request: NotificationMessage, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/common/samples/notifications`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  send = {
    /**
     * @description 임시 비밀번호 변경 및 메일 전송 API
     *
     * @tags [공통] - 비밀변호 변경 API
     * @name SendNewPasswordUsingPost
     * @summary 임시 비밀번호 변경 및 메일 전송 API
     * @request POST:/api/wewill/common/send/new-password
     * @secure
     */
    sendNewPasswordUsingPost: (req: CommonResetPasswordRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/common/send/new-password`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 회원등록 인증번호 이메일 발송
     *
     * @tags [사용자페이지] - 로그인 API
     * @name SendEmailUsingPost
     * @summary 회원등록 인증번호 이메일 발송  API
     * @request POST:/api/wewill/user/send/email/certification
     * @secure
     */
    sendEmailUsingPost: (query: SendEmailUsingPostParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/send/email/certification`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 아이디,이메일로 회원 정보 확인 후 임시비밀번호 변경 후 메일 전송
     *
     * @tags [사용자페이지] - 로그인 API
     * @name SendEmailNewPasswordUsingPost
     * @summary 로그인 > 비밀번호 찾기 > 이메일로 찾기 > 이메일 전송 API
     * @request POST:/api/wewill/user/send/email/password
     * @secure
     */
    sendEmailNewPasswordUsingPost: (req: UserMemberSendEmailRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/send/email/password`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  settlements = {
    /**
     * @description 결제 완료 후 후속 처리 Process API : 결제쿠폰 등록, 사용자 쿠폰 사용 상태로 수정, 포인트 등록 + 사용 처리 등
     *
     * @tags [공통] - 결제 API
     * @name SettlementRequestUsingPost
     * @summary 의뢰 신청 > 신용카드&가상계좌&계좌이체 결제 완료
     * @request POST:/api/wewill/common/settlements
     * @secure
     */
    settlementRequestUsingPost: (request: SettlementInsertRequest, params: RequestParams = {}) =>
      this.request<SettlementInsertResponse, void>({
        path: `/api/wewill/common/settlements`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 취소
     *
     * @tags [공통] - 결제 API
     * @name SettlementCancelRequestUsingPost
     * @summary 결제 취소 API
     * @request POST:/api/wewill/common/settlements/cancel
     * @secure
     */
    settlementCancelRequestUsingPost: (request: SettlementCancelRequest, params: RequestParams = {}) =>
      this.request<InputStream, void>({
        path: `/api/wewill/common/settlements/cancel`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 완료 후 I/F Table 등록
     *
     * @tags [공통] - 결제 API
     * @name PayCompleteUsingPost
     * @summary 의뢰 신청 > 신용카드&가상계좌&계좌이체 결제 완료
     * @request POST:/api/wewill/common/settlements/complete
     * @secure
     */
    payCompleteUsingPost: (query: PayCompleteUsingPostParams, params: RequestParams = {}) =>
      this.request<IfImportPayCompleteResponse, void>({
        path: `/api/wewill/common/settlements/complete`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 시작 시 I/F Table 기본 정보 등록
     *
     * @tags [공통] - 결제 API
     * @name PayGenerateUsingPost
     * @summary 의뢰 신청 > 결제
     * @request POST:/api/wewill/common/settlements/generate
     * @secure
     */
    payGenerateUsingPost: (query: PayGenerateUsingPostParams, params: RequestParams = {}) =>
      this.request<IfImportPayGenerateResponse, void>({
        path: `/api/wewill/common/settlements/generate`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description Request parameter를 이용하여 Settlement data 조회
     *
     * @tags [공통] - 결제 API
     * @name SearchSettlementUsingGet
     * @summary 결제 검색 조회 API
     * @request GET:/api/wewill/common/settlements/search
     * @secure
     */
    searchSettlementUsingGet: (query: SearchSettlementUsingGetParams, params: RequestParams = {}) =>
      this.request<SettlementResponse, void>({
        path: `/api/wewill/common/settlements/search`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags [공통] - 결제 API
     * @name PayTokenUsingPost
     * @summary payToken
     * @request POST:/api/wewill/common/settlements/token
     * @secure
     */
    payTokenUsingPost: (params: RequestParams = {}) =>
      this.request<InputStream, void>({
        path: `/api/wewill/common/settlements/token`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description [최근 결제내역 조회] 기간에 따라 조회가능 목록조회 페이징처리
     *
     * @tags [공통] - 결제 API
     * @name PaymentResultListPagingUsingGet
     * @summary [최근 결제내역 조회] 목록조회 페이징처리 API
     * @request GET:/api/wewill/common/settlements/{pageNo}/{pageSize}
     * @secure
     */
    paymentResultListPagingUsingGet: (
      { pageNo, pageSize, ...query }: PaymentResultListPagingUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<PackageResponse, void>({
        path: `/api/wewill/common/settlements/${pageNo}/${pageSize}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제결과 조회
     *
     * @tags [공통] - 결제 API
     * @name SelectSettlementResultUsingGet
     * @summary 결제결과 조회
     * @request GET:/api/wewill/common/settlements/{settlementNo}
     * @secure
     */
    selectSettlementResultUsingGet: (settlementNo: string, params: RequestParams = {}) =>
      this.request<IfImportPayCompleteVbankResponse, void>({
        path: `/api/wewill/common/settlements/${settlementNo}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  categories = {
    /**
     * @description 제품 카테고리 목록 조회 / 평가자 헤더 드롭다운 "제품" 선택시 제품 카테고리 목록 조회
     *
     * @tags [의뢰신청] - 제품,영상 카테고리 API
     * @name SearchRequestPrductCategoriesUsingGet
     * @summary 의뢰신청 > 제품 > 의뢰 콘텐츠 설정 > 카테고리 목록 조회 API
     * @request GET:/api/wewill/user/categories/products
     * @secure
     */
    searchRequestPrductCategoriesUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoSearchPrductOrderCategoryResponse, void>({
        path: `/api/wewill/user/categories/products`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 ID값 통한 매핑된 제품 카테고리 목록 조회
     *
     * @tags [의뢰신청] - 제품,영상 카테고리 API
     * @name SearchPrductCategoriesByProductIdUsingGet
     * @summary 평가신청 > 평가목록 > 2Depth별 목록 형태 > 제품 카테고리 조회 API
     * @request GET:/api/wewill/user/categories/products/{id}
     * @secure
     */
    searchPrductCategoriesByProductIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ListVoSearchPrductCategoryResponse, void>({
        path: `/api/wewill/user/categories/products/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 의뢰신청 / 평가자 헤더 드롭다운 "영상" 선택시 영상 카테고리 목록 조회
     *
     * @tags [의뢰신청] - 제품,영상 카테고리 API
     * @name SearchRequestVideoCategoriesUsingGet
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 카테고리 목록 조회 API
     * @request GET:/api/wewill/user/categories/videos
     * @secure
     */
    searchRequestVideoCategoriesUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoSearchVidoOrderCategoryResponse, void>({
        path: `/api/wewill/user/categories/videos`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 영상 ID값 통한 매핑된 영상 카테고리 목록 조회
     *
     * @tags [의뢰신청] - 제품,영상 카테고리 API
     * @name SearchVidoCategoriesByProductIdUsingGet
     * @summary 평가신청 > 평가목록 > 2Depth별 목록 형태 > 영상 카테고리 조회 API
     * @request GET:/api/wewill/user/categories/videos/{id}
     * @secure
     */
    searchVidoCategoriesByProductIdUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ListVoSearchVidoCategoryResponse, void>({
        path: `/api/wewill/user/categories/videos/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  changePassword = {
    /**
     * @description 비밀번호 재설정(휴대폰)
     *
     * @tags [사용자페이지] - 로그인 API
     * @name ModifyPasswordByMobileUsingPost
     * @summary 비밀번호 재설정(휴대폰) API
     * @request POST:/api/wewill/user/change-password/sms
     * @secure
     */
    modifyPasswordByMobileUsingPost: (req: ModifyPasswordByMobileRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/change-password/sms`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  change = {
    /**
     * @description 비밀번호 변경
     *
     * @tags [사용자페이지] - 로그인 API
     * @name ChangePwdUsingPost
     * @summary 비밀번호 변경 API
     * @request POST:/api/wewill/user/change/pwd
     * @secure
     */
    changePwdUsingPost: (req: UserMemberChangePwdRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/change/pwd`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  clientExperience = {
    /**
     * @description 프리미엄 모든 질문에서 주관식 질문의 모든 답변
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestBasicAnswerUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서(주관식 문항 모든 답변)  API
     * @request POST:/api/wewill/user/client-experience/request/report/basic/vido/answer
     * @secure
     */
    expInfoRequestBasicAnswerUsingPost: (req: ReportAnswerRequest, params: RequestParams = {}) =>
      this.request<ReportAllAnswerResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/basic/vido/answer`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 기본 정보 모든 답변
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestBasicAllAnswerUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서 > 기본정보 > 모든 답변 필터 API
     * @request POST:/api/wewill/user/client-experience/request/report/basic/vido/answer/all
     * @secure
     */
    expInfoRequestBasicAllAnswerUsingPost: (req: ReportAllAnswerRequest, params: RequestParams = {}) =>
      this.request<PagingInfoReportAnswerResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/basic/vido/answer/all`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) 하이라이트 누적 차트 검색
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestBasicChartUsingPost
     * @summary 체험하기 > 기본정보 > 하이라이트 누적 차트 검색  API
     * @request POST:/api/wewill/user/client-experience/request/report/basic/vido/chart/highlight
     * @secure
     */
    expInfoRequestBasicChartUsingPost: (req: UserClientReportVidoRequest, params: RequestParams = {}) =>
      this.request<StackBarChartResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/basic/vido/chart/highlight`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description  기본 정보
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestBasicVidoInfoUsingPost
     * @summary 체험하기 > 보고서 상세화면(기본정보)  API
     * @request POST:/api/wewill/user/client-experience/request/report/basic/vido/info
     * @secure
     */
    expInfoRequestBasicVidoInfoUsingPost: (params: RequestParams = {}) =>
      this.request<UserClientReportVidoBasicResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/basic/vido/info`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) 별점 평가
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestBasicStarScoreUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서(별점 평가)  API
     * @request POST:/api/wewill/user/client-experience/request/report/basic/vido/star
     * @secure
     */
    expInfoRequestBasicStarScoreUsingPost: (req: ReportStarScoreRequest, params: RequestParams = {}) =>
      this.request<StackBarChartResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/basic/vido/star`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) WordCloud - 하이라이트 뽑은 이유 / 소감
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestBasicWordCloudUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서(WordCloud - 하이라이트 뽑은 이유 / 소감) 검색  API
     * @request POST:/api/wewill/user/client-experience/request/report/basic/vido/wordcloud/highlight
     * @secure
     */
    expInfoRequestBasicWordCloudUsingPost: (req: WordCloudRequest, params: RequestParams = {}) =>
      this.request<ListVoWordCloudResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/basic/vido/wordcloud/highlight`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 차트 필터 검색(주관식 제외)
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoChartFilterUsingPost
     * @summary 대시보드 > 보고서(vido) > 차트 필터 검색(주관식 제외)  API
     * @request POST:/api/wewill/user/client-experience/request/report/chart/search
     * @secure
     */
    expInfoChartFilterUsingPost: (req: UserClientChartSearchRequest, params: RequestParams = {}) =>
      this.request<StackBarChartResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/chart/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 차트 필터 검색(주관식 워드클라우드)
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoWordCloudFilterUsingPost
     * @summary 대시보드 > 보고서(vido) > 차트 필터 검색(주관식 워드클라우드)  API
     * @request POST:/api/wewill/user/client-experience/request/report/chart/wordcloud/search
     * @secure
     */
    expInfoWordCloudFilterUsingPost: (req: UserClientChartSearchRequest, params: RequestParams = {}) =>
      this.request<WordCloudChart, void>({
        path: `/api/wewill/user/client-experience/request/report/chart/wordcloud/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 보고서 엑셀 다운로드
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpDownLoadReportExcelUsingGet
     * @summary 대시보드 > 보고서 > 엑셀 다운로드
     * @request GET:/api/wewill/user/client-experience/request/report/excel/{requestId}
     * @secure
     */
    expDownLoadReportExcelUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/user/client-experience/request/report/excel/${requestId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 의뢰 보고서 정보
     *
     * @tags [사용지페이지] - 의뢰자 보고서 체험하기 API
     * @name ExpInfoRequestPrductUsingGet
     * @summary 보고서(Product) API
     * @request GET:/api/wewill/user/client-experience/request/report/product
     * @secure
     */
    expInfoRequestPrductUsingGet: (query: ExpInfoRequestPrductUsingGetParams, params: RequestParams = {}) =>
      this.request<UserClientReportVidoBasicResponse, void>({
        path: `/api/wewill/user/client-experience/request/report/product`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  clientMember = {
    /**
     * @description 의뢰자 회원가입
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InsertClientMemberUsingPost
     * @summary 의뢰자 회원가입 API
     * @request POST:/api/wewill/user/client-member
     * @secure
     */
    insertClientMemberUsingPost: (req: UserClientInsertRequest, params: RequestParams = {}) =>
      this.request<UserClientInsertResponse, void>({
        path: `/api/wewill/user/client-member`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 마이페이지 비밀번호 확인
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name CheckClientPwdUsingPost
     * @summary 마이페이지 > 회원 정보 > 비밀번호 확인 API
     * @request POST:/api/wewill/user/client-member/check/pwd
     * @secure
     */
    checkClientPwdUsingPost: (req: UserClientCheckPwdRequest, params: RequestParams = {}) =>
      this.request<UserClientCheckPwdResponse, void>({
        path: `/api/wewill/user/client-member/check/pwd`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 대시보드
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoClientMemberDashBoardUsingGet
     * @summary 의뢰자 대시보드
     * @request GET:/api/wewill/user/client-member/dashboard
     * @secure
     */
    infoClientMemberDashBoardUsingGet: (query: InfoClientMemberDashBoardUsingGetParams, params: RequestParams = {}) =>
      this.request<UserDashBoardResponse, void>({
        path: `/api/wewill/user/client-member/dashboard`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰서 삭제
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name DeleteRequestUsingDelete
     * @summary 의뢰자 대시보드 > 의뢰 삭제
     * @request DELETE:/api/wewill/user/client-member/dashboard
     * @secure
     */
    deleteRequestUsingDelete: (req: UserClientRequestDeleteRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/dashboard`,
        method: "DELETE",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 일별 차트 데이터
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoDashBoardDayDatasetsUsingPost
     * @summary 의뢰자 대시보드 > 일별 차트 데이터
     * @request POST:/api/wewill/user/client-member/dashboard/data/day
     * @secure
     */
    infoDashBoardDayDatasetsUsingPost: (req: UserClientMemberDashBoardChartRequest, params: RequestParams = {}) =>
      this.request<UserClientMemberDetailChartResponse, void>({
        path: `/api/wewill/user/client-member/dashboard/data/day`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 월별 차트 데이터
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoDashBoardMonthDatasetsUsingPost
     * @summary 의뢰자 대시보드 > 월별 차트 데이터
     * @request POST:/api/wewill/user/client-member/dashboard/data/month
     * @secure
     */
    infoDashBoardMonthDatasetsUsingPost: (req: UserClientMemberDashBoardChartRequest, params: RequestParams = {}) =>
      this.request<UserClientMemberDetailChartResponse, void>({
        path: `/api/wewill/user/client-member/dashboard/data/month`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 주간별 차트 데이터
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoDashBoardWeekDatasetsUsingPost
     * @summary 의뢰자 대시보드 > 주간별 차트 데이터
     * @request POST:/api/wewill/user/client-member/dashboard/data/week
     * @secure
     */
    infoDashBoardWeekDatasetsUsingPost: (req: UserClientMemberDashBoardChartRequest, params: RequestParams = {}) =>
      this.request<UserClientMemberDetailChartResponse, void>({
        path: `/api/wewill/user/client-member/dashboard/data/week`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 상세보기
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoDashBoardReqDetailUsingPost
     * @summary 의뢰자 대시보드 > 의뢰 상세보기
     * @request POST:/api/wewill/user/client-member/dashboard/detail
     * @secure
     */
    infoDashBoardReqDetailUsingPost: (req: UserClientMemberRequestDetailInfoRequest, params: RequestParams = {}) =>
      this.request<UserClientMemberRequestDetailInfoResponse, void>({
        path: `/api/wewill/user/client-member/dashboard/detail`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사업자등록증 삭제 확인
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name DeleteBizFileUsingPost
     * @summary 마이페이지 > 회원 정보 > 사업자등록증 삭제 API
     * @request POST:/api/wewill/user/client-member/delete/bizcontract
     * @secure
     */
    deleteBizFileUsingPost: (req: UserClientMemberDeleteBizRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/delete/bizcontract`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 광고성 정보 수신동의 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyAdvertsUsingPut
     * @summary 마이페이지 > 회원정보 > 광고성 정보 수신동의 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/advrts
     * @secure
     */
    modifyAdvertsUsingPut: (req: UserClientModifyAdvrtsRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/advrts`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 알림설정 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyClientAlarmUsingPut
     * @summary 마이페이지 > 회원정보 > 알림설정 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/alarm
     * @secure
     */
    modifyClientAlarmUsingPut: (req: UserClientModifyAlarmRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/alarm`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 이메일 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyClientMemberBankUsingPut
     * @summary 마이페이지 > 회원정보 > 출금계좌 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/bank
     * @secure
     */
    modifyClientMemberBankUsingPut: (req: UserClientModifyBankRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/bank`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 생년월일 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyClientMemberBirthdayUsingPut
     * @summary 마이페이지 > 회원정보 > 생년월일 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/birthday
     * @secure
     */
    modifyClientMemberBirthdayUsingPut: (req: UserClientModifyBirthdayRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/birthday`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사업자등록증 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyBizContractUsingPut
     * @summary 마이페이지 > 회원정보 > 사업자등록증 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/bizcontract
     * @secure
     */
    modifyBizContractUsingPut: (req: UserClientModifyBizContractRequest, params: RequestParams = {}) =>
      this.request<UserClientModifyBizContractResponse, void>({
        path: `/api/wewill/user/client-member/modify/bizcontract`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 이메일 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyClientMemberEmailUsingPut
     * @summary 마이페이지 > 회원정보 > 이메일 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/email
     * @secure
     */
    modifyClientMemberEmailUsingPut: (req: UserClientModifyEmailRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/email`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 이름 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyClientMemberNameUsingPut
     * @summary 마이페이지 > 회원정보 > 이름 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/name
     * @secure
     */
    modifyClientMemberNameUsingPut: (req: UserClientModifyNameRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/name`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 마이페이지 비밀번호 확인
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyPwdUsingPost
     * @summary 마이페이지 > 회원 정보 > 비밀변경 API
     * @request POST:/api/wewill/user/client-member/modify/pwd
     * @secure
     */
    modifyPwdUsingPost: (req: UserClientModifyPwdRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/pwd`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 성별 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name ModifyClientMemberSexdstnUsingPut
     * @summary 마이페이지 > 회원정보 > 성별 수정 API
     * @request PUT:/api/wewill/user/client-member/modify/sexdstn
     * @secure
     */
    modifyClientMemberSexdstnUsingPut: (req: UserClientModifySexdstnRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/modify/sexdstn`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 상세보기
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoClientMemberUsingGet
     * @summary 마이페이지 > 회원정보 > 기본정보 API
     * @request GET:/api/wewill/user/client-member/mypage
     * @secure
     */
    infoClientMemberUsingGet: (params: RequestParams = {}) =>
      this.request<UserClientMemberInfoResponse, void>({
        path: `/api/wewill/user/client-member/mypage`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 휴대폰 번호로 인증번호 전송
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name SendSmsCertificationMypageUsingPost
     * @summary 마이페이지 > 휴대폰 인증번호 전송
     * @request POST:/api/wewill/user/client-member/mypage/send/sms/certification
     * @secure
     */
    sendSmsCertificationMypageUsingPost: (req: UserClientSendSmsRequest, params: RequestParams = {}) =>
      this.request<UserJoinCertificationResponse, void>({
        path: `/api/wewill/user/client-member/mypage/send/sms/certification`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 전송된 인증번호 확인 후 정보가 맞으면 휴대폰 번호 수정
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name SendSmsCertificationMypageCheckUsingPost
     * @summary 마이페이지 > 휴대폰 인증번호 확인
     * @request POST:/api/wewill/user/client-member/mypage/send/sms/certification/check
     * @secure
     */
    sendSmsCertificationMypageCheckUsingPost: (req: UserSmsCertificationCheckReqest, params: RequestParams = {}) =>
      this.request<UserJoinCertificationResponse, void>({
        path: `/api/wewill/user/client-member/mypage/send/sms/certification/check`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 프리미엄 모든 질문에서 주관식 질문의 모든 답변
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestBasicAnswerUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서(주관식 문항 모든 답변)  API
     * @request POST:/api/wewill/user/client-member/request/report/basic/vido/answer
     * @secure
     */
    infoRequestBasicAnswerUsingPost: (req: ReportAnswerRequest, params: RequestParams = {}) =>
      this.request<ReportAllAnswerResponse, void>({
        path: `/api/wewill/user/client-member/request/report/basic/vido/answer`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 기본 정보 모든 답변
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestBasicAllAnswerUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서 > 기본정보 > 모든 답변 필터 API
     * @request POST:/api/wewill/user/client-member/request/report/basic/vido/answer/all
     * @secure
     */
    infoRequestBasicAllAnswerUsingPost: (req: ReportAllAnswerRequest, params: RequestParams = {}) =>
      this.request<PagingInfoReportAnswerResponse, void>({
        path: `/api/wewill/user/client-member/request/report/basic/vido/answer/all`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) 하이라이트 누적 차트 검색
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestBasicChartUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서 > 기본정보 > 하이라이트 누적 차트 검색  API
     * @request POST:/api/wewill/user/client-member/request/report/basic/vido/chart/highlight
     * @secure
     */
    infoRequestBasicChartUsingPost: (req: UserClientReportVidoRequest, params: RequestParams = {}) =>
      this.request<StackBarChartResponse, void>({
        path: `/api/wewill/user/client-member/request/report/basic/vido/chart/highlight`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) 기본 정보
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestBasicVidoInfoUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서 상세화면(기본정보)  API
     * @request POST:/api/wewill/user/client-member/request/report/basic/vido/info
     * @secure
     */
    infoRequestBasicVidoInfoUsingPost: (req: UserClientReportInfoRequest, params: RequestParams = {}) =>
      this.request<UserClientReportVidoBasicResponse, void>({
        path: `/api/wewill/user/client-member/request/report/basic/vido/info`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) 별점 평가
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestBasicStarScoreUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서(별점 평가)  API
     * @request POST:/api/wewill/user/client-member/request/report/basic/vido/star
     * @secure
     */
    infoRequestBasicStarScoreUsingPost: (req: ReportStarScoreRequest, params: RequestParams = {}) =>
      this.request<StackBarChartResponse, void>({
        path: `/api/wewill/user/client-member/request/report/basic/vido/star`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 보고서(Vido) WordCloud - 하이라이트 뽑은 이유 / 소감
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestBasicWordCloudUsingPost
     * @summary 대시보드 > 보고서 > Vido 보고서(WordCloud - 하이라이트 뽑은 이유 / 소감) 검색  API
     * @request POST:/api/wewill/user/client-member/request/report/basic/vido/wordcloud/highlight
     * @secure
     */
    infoRequestBasicWordCloudUsingPost: (req: WordCloudRequest, params: RequestParams = {}) =>
      this.request<ListVoWordCloudResponse, void>({
        path: `/api/wewill/user/client-member/request/report/basic/vido/wordcloud/highlight`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 차트 필터 검색(주관식 제외)
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoChartFilterUsingPost
     * @summary 대시보드 > 보고서(vido) > 차트 필터 검색(주관식 제외)  API
     * @request POST:/api/wewill/user/client-member/request/report/chart/search
     * @secure
     */
    infoChartFilterUsingPost: (req: UserClientChartSearchRequest, params: RequestParams = {}) =>
      this.request<StackBarChartResponse, void>({
        path: `/api/wewill/user/client-member/request/report/chart/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 차트 필터 검색(주관식 워드클라우드)
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoWordCloudFilterUsingPost
     * @summary 대시보드 > 보고서(vido) > 차트 필터 검색(주관식 워드클라우드)  API
     * @request POST:/api/wewill/user/client-member/request/report/chart/wordcloud/search
     * @secure
     */
    infoWordCloudFilterUsingPost: (req: UserClientChartSearchRequest, params: RequestParams = {}) =>
      this.request<WordCloudChart, void>({
        path: `/api/wewill/user/client-member/request/report/chart/wordcloud/search`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 보고서 엑셀 다운로드
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name DownLoadReportExcelUsingGet
     * @summary 대시보드 > 보고서 > 엑셀 다운로드
     * @request GET:/api/wewill/user/client-member/request/report/excel/{requestId}
     * @secure
     */
    downLoadReportExcelUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/wewill/user/client-member/request/report/excel/${requestId}`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description dummy 데이터에 대한 체험하기
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name ExperienceReportUsingGet
     * @summary 대시보드 > 보고서 > 체험하기
     * @request GET:/api/wewill/user/client-member/request/report/experience
     * @secure
     */
    experienceReportUsingGet: (params: RequestParams = {}) =>
      this.request<UserClientReportVidoBasicResponse, void>({
        path: `/api/wewill/user/client-member/request/report/experience`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 보고서 정보
     *
     * @tags [사용자페이지] - 의뢰자 보고서 API
     * @name InfoRequestPrductUsingGet
     * @summary 보고서(Product) API
     * @request GET:/api/wewill/user/client-member/request/report/product
     * @secure
     */
    infoRequestPrductUsingGet: (query: InfoRequestPrductUsingGetParams, params: RequestParams = {}) =>
      this.request<UserClientReportVidoBasicResponse, void>({
        path: `/api/wewill/user/client-member/request/report/product`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 회원 탈퇴
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name SecessionClientMemberUsingPost
     * @summary 의뢰자 회원 탈퇴 API
     * @request POST:/api/wewill/user/client-member/secession
     * @secure
     */
    secessionClientMemberUsingPost: (req: UserSecessionInsertRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/client-member/secession`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 휴대폰 번호로 인증번호 전송
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name SendSmsCertificationUsingPost
     * @summary 의뢰자 > 회원가입 > 휴대폰 인증번호 전송
     * @request POST:/api/wewill/user/client-member/send/sms/certification
     * @secure
     */
    sendSmsCertificationUsingPost: (req: UserJoinCertificationRequest, params: RequestParams = {}) =>
      this.request<UserJoinCertificationResponse, void>({
        path: `/api/wewill/user/client-member/send/sms/certification`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 전송된 인증번호 확인
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name SendSmsCertificationCheckUsingPost
     * @summary 의뢰자 > 회원가입 > 휴대폰 인증번호 전송 > 인증 번호 확인
     * @request POST:/api/wewill/user/client-member/send/sms/certification/check
     * @secure
     */
    sendSmsCertificationCheckUsingPost: (req: UserJoinCertificationCheckRequest, params: RequestParams = {}) =>
      this.request<UserJoinCertificationResponse, void>({
        path: `/api/wewill/user/client-member/send/sms/certification/check`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 내역 조회 API
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name FindClientMemberSetleUsingGet
     * @summary 마이페이지> 결제내역
     * @request GET:/api/wewill/user/client-member/settlement
     * @secure
     */
    findClientMemberSetleUsingGet: (query: FindClientMemberSetleUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoUserClientSettlementResponse, void>({
        path: `/api/wewill/user/client-member/settlement`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 결제 내역 상세 카드결제 부분 조회 API
     *
     * @tags [사용자페이지] - 의뢰자 회원 API
     * @name InfoClientMemberSetleDtlUsingGet
     * @summary 마이페이지> 결제내역> 결제내역 상세> 가상계좌> 결제완료
     * @request GET:/api/wewill/user/client-member/settlement/{setleNo}
     * @secure
     */
    infoClientMemberSetleDtlUsingGet: (setleNo: string, params: RequestParams = {}) =>
      this.request<UserClientSettlementInfoResponse, void>({
        path: `/api/wewill/user/client-member/settlement/${setleNo}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  clientintro = {
    /**
     * @description 서비스 소개서 요청 등록
     *
     * @tags [GatePage 의뢰자] 서비스 소개서 요청 API
     * @name InsertClientIntroUsingPost
     * @summary 서비스 소개서 요청 등록 API
     * @request POST:/api/wewill/user/clientintro
     * @secure
     */
    insertClientIntroUsingPost: (request: ClientIntroInsertRequest, params: RequestParams = {}) =>
      this.request<ClientIntroInsertResponse, void>({
        path: `/api/wewill/user/clientintro`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  coupons = {
    /**
     * @description 보유 쿠폰 내역의 목록 조회 페이징
     *
     * @tags [공통] - 쿠폰관리 API
     * @name FindAllClientCouponUsingGet
     * @summary 의뢰 신청 > 결제 > 보유 쿠폰 조회 > 내 쿠폰함
     * @request GET:/api/wewill/user/coupons/clientcoupons
     * @secure
     */
    findAllClientCouponUsingGet: (query: FindAllClientCouponUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoClientCouponResponse, void>({
        path: `/api/wewill/user/coupons/clientcoupons`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰자 쿠폰 등록 기능
     *
     * @tags [공통] - 쿠폰관리 API
     * @name RegisterClientCouponUsingPost
     * @summary 의뢰 신청 > 결제 > 보유 쿠폰 조회 > 쿠폰 등록하기
     * @request POST:/api/wewill/user/coupons/clientcoupons
     * @secure
     */
    registerClientCouponUsingPost: (request: ClientCouponInsertRequest, params: RequestParams = {}) =>
      this.request<ClientCouponInsertResponse, void>({
        path: `/api/wewill/user/coupons/clientcoupons`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 쿠폰 할인 금액 조회
     *
     * @tags [공통] - 쿠폰관리 API
     * @name FindCouponDscntCalcUsingGet
     * @summary 의뢰 신청 > 결제 > 보유 쿠폰 조회 > 쿠폰 할인 금액 조회 API
     * @request GET:/api/wewill/user/coupons/clientcoupons/{prqudoId}
     * @secure
     */
    findCouponDscntCalcUsingGet: (
      { prqudoId, ...query }: FindCouponDscntCalcUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<CouponDscntCalcResponse, void>({
        path: `/api/wewill/user/coupons/clientcoupons/${prqudoId}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  customers = {
    /**
     * @description 문의게시판 전체 조회
     *
     * @tags [고객센터] - 사용자 문의 API
     * @name SearchCustomerUsingGet
     * @summary 문의게시판 전체 조회 API
     * @request GET:/api/wewill/user/customers
     * @secure
     */
    searchCustomerUsingGet: (query: SearchCustomerUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoCustomerSearchResponse, void>({
        path: `/api/wewill/user/customers`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 질문 등록(평가자/의뢰자 공통)
     *
     * @tags [고객센터] - 사용자 문의 API
     * @name InsertMemberQuestionUsingPost
     * @summary 고객센터 > 1:1 문의 > 문의하기(평가자/의뢰자 공통) API
     * @request POST:/api/wewill/user/customers
     * @secure
     */
    insertMemberQuestionUsingPost: (request: CustomerInsertQuestionRequest, params: RequestParams = {}) =>
      this.request<CustomerInsertQuestionResponse, void>({
        path: `/api/wewill/user/customers`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 문의게시판 상세페이지 조회
     *
     * @tags [고객센터] - 사용자 문의 API
     * @name SelectCustomerUsingGet
     * @summary 고객센터 > 1:1 문의 > 문의 상세 정보
     * @request GET:/api/wewill/user/customers/{id}
     * @secure
     */
    selectCustomerUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<CustomerInfoResponse, void>({
        path: `/api/wewill/user/customers/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 질문 수정(평가자/의뢰자 공통)
     *
     * @tags [고객센터] - 사용자 문의 API
     * @name ModifyMemberQuestionUsingPut
     * @summary 고객센터 > 1:1 문의 > 문의하기::수정(평가자/의뢰자 공통) API
     * @request PUT:/api/wewill/user/customers/{id}
     * @secure
     */
    modifyMemberQuestionUsingPut: (id: number, request: CustomerModifyQuestionRequest, params: RequestParams = {}) =>
      this.request<CustomerModifyQuestionResponse, void>({
        path: `/api/wewill/user/customers/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 질문 삭제
     *
     * @tags [고객센터] - 사용자 문의 API
     * @name DeleteQuestionUsingDelete
     * @summary 질문 삭제 API
     * @request DELETE:/api/wewill/user/customers/{id}
     * @secure
     */
    deleteQuestionUsingDelete: (id: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/customers/${id}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  dailyquestion = {
    /**
     * @description 데일리 설문(오늘 날짜의 설문지) 설문완료된건 throw 처리
     *
     * @tags [평가자] - 평가신청 daily 설문 API
     * @name SearchDailyUsingGet
     * @summary 데일리 설문 조회
     * @request GET:/api/wewill/user/dailyquestion
     * @secure
     */
    searchDailyUsingGet: (params: RequestParams = {}) =>
      this.request<DailyQuestionInfoResponse, void>({
        path: `/api/wewill/user/dailyquestion`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 데일리 설문에 대한 평가자 답변 등록
     *
     * @tags [평가자] - 평가신청 daily 설문 API
     * @name InsertDailyUsingPost
     * @summary 데일리 설문 답변 등록
     * @request POST:/api/wewill/user/dailyquestion
     * @secure
     */
    insertDailyUsingPost: (request: DailyQuestionInsertRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/dailyquestion`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 데일리 설문(오늘 날짜의 설문지) 뱃지
     *
     * @tags [평가자] - 평가신청 daily 설문 API
     * @name CheckDailyUsingGet
     * @summary 데일리 설문 뱃지
     * @request GET:/api/wewill/user/dailyquestion/badge
     * @secure
     */
    checkDailyUsingGet: (params: RequestParams = {}) =>
      this.request<DailyBadgeResponse, void>({
        path: `/api/wewill/user/dailyquestion/badge`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  declaration = {
    /**
     * @description 신고하기 등록
     *
     * @tags [평가자] - 평가신청 신고하기 API
     * @name InsertDeclarationUsingPost
     * @summary 평가신청 > 평가상세 > 신고하기 API
     * @request POST:/api/wewill/user/declaration
     * @secure
     */
    insertDeclarationUsingPost: (request: DeclarationInsertRequest, params: RequestParams = {}) =>
      this.request<DeclarationInsertResponse, void>({
        path: `/api/wewill/user/declaration`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  email = {
    /**
     * @description 아이디 찾기(이메일)
     *
     * @tags [사용자페이지] - 로그인 API
     * @name FindIdByEmailUsingGet
     * @summary 아이디 찾기(이메일) API
     * @request GET:/api/wewill/user/email/find-id
     * @secure
     */
    findIdByEmailUsingGet: (query: FindIdByEmailUsingGetParams, params: RequestParams = {}) =>
      this.request<UserFindIdResponse, void>({
        path: `/api/wewill/user/email/find-id`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  essentialvideo = {
    /**
     * @description 해당 사용자의 진행율 정보 조회
     *
     * @tags [평가자] - 평가신청 필수 영상 시청 여부 확인 API
     * @name SearchEssentialVideoUsingGet
     * @summary 평가신청 > 평가상세 > 평가 신청 전 > 필수 영상 시청:: 여부 확인 API
     * @request GET:/api/wewill/user/essentialvideo
     * @secure
     */
    searchEssentialVideoUsingGet: (query: SearchEssentialVideoUsingGetParams, params: RequestParams = {}) =>
      this.request<EssentialVideoSearchResponse, void>({
        path: `/api/wewill/user/essentialvideo`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 진행률 업데이트
     *
     * @tags [평가자] - 평가신청 필수 영상 시청 여부 확인 API
     * @name SearchEssentialVideoUsingPut
     * @summary 평가신청 > 평가상세 > 평가 신청 전 > 필수 영상 시청 진행률 업데이트 API
     * @request PUT:/api/wewill/user/essentialvideo
     * @secure
     */
    searchEssentialVideoUsingPut: (request: EssentialVideoModifyRequest, params: RequestParams = {}) =>
      this.request<EssentialVideoModifyResponse, void>({
        path: `/api/wewill/user/essentialvideo`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  estimates = {
    /**
     * @description 의뢰 id 값 통한 의뢰 견적 산출 및 DB Insert
     *
     * @tags [의뢰자] - 견적서 API
     * @name InsertReqestEstimateUsingPost
     * @summary 의뢰 신청 > 견적서 > 견적서 저장 API
     * @request POST:/api/wewill/user/estimates
     * @secure
     */
    insertReqestEstimateUsingPost: (query: InsertReqestEstimateUsingPostParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/estimates`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 id 값 통한 의뢰 견적 조회
     *
     * @tags [의뢰자] - 견적서 API
     * @name SelectRequestEstimateUsingGet
     * @summary 의뢰 신청 > 견적서 > 견적서 조회 API
     * @request GET:/api/wewill/user/estimates/{reqestId}
     * @secure
     */
    selectRequestEstimateUsingGet: (reqestId: number, params: RequestParams = {}) =>
      this.request<ReqestEstimateInfoResponse, void>({
        path: `/api/wewill/user/estimates/${reqestId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  evaluate = {
    /**
     * @description 이전 설문 완료했으면 설문 목록 보여주기, 안했으면 예외처리
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name CheckResearchAnswerUsingPost
     * @summary 평가신청 > 제품 > 평가상세 > 설문 이동
     * @request POST:/api/wewill/user/evaluate/goods/evaluate/next/check
     * @secure
     */
    checkResearchAnswerUsingPost: (req: UserEvaluateCheckQestnrRequest, params: RequestParams = {}) =>
      this.request<UserEvaluateCheckQestnrResponse, void>({
        path: `/api/wewill/user/evaluate/goods/evaluate/next/check`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 제품 답변 완료 후 평가 상태 변경
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name ModifyEvaluateStatusUsingPut
     * @summary 평가자 제품 모든 답변 완료 후 평가 상태 완료 변경 API
     * @request PUT:/api/wewill/user/evaluate/goods/evaluate/{requestId}/complete
     * @secure
     */
    modifyEvaluateStatusUsingPut: (requestId: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/evaluate/goods/evaluate/${requestId}/complete`,
        method: "PUT",
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 설문지 질문에 대한 답변 임시 저장
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name InsertTempMemberEvaluationAnswerUsingPost
     * @summary 평가신청 > 제품 > 평가상세 > 설문 답변 임시저장
     * @request POST:/api/wewill/user/evaluate/goods/evaluate/{requestId}/save/evaluation/answer
     * @secure
     */
    insertTempMemberEvaluationAnswerUsingPost: (
      requestId: number,
      reqList: UserEvaluateGoodsAnswerInsertRequest[],
      params: RequestParams = {},
    ) =>
      this.request<UserEvaluateCheckQestnrResponse, void>({
        path: `/api/wewill/user/evaluate/goods/evaluate/${requestId}/save/evaluation/answer`,
        method: "POST",
        body: reqList,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 설문지 질문에 대한 답변 제출
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name InsertMemberEvaluationAnswerUsingPost
     * @summary 평가신청 > 제품 > 평가상세 > 설문 답변 제출
     * @request POST:/api/wewill/user/evaluate/goods/evaluate/{requestId}/submit/evaluation/answer
     * @secure
     */
    insertMemberEvaluationAnswerUsingPost: (
      requestId: number,
      reqList: UserEvaluateGoodsAnswerInsertRequest[],
      params: RequestParams = {},
    ) =>
      this.request<UserEvaluateInsertAnswerResponse, void>({
        path: `/api/wewill/user/evaluate/goods/evaluate/${requestId}/submit/evaluation/answer`,
        method: "POST",
        body: reqList,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 제품 목록(더보기)
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name SearchEvaluateGoodsAllUsingGet
     * @summary 평가신청 > 제품 > 평가목록
     * @request GET:/api/wewill/user/evaluate/goods/list/all
     * @secure
     */
    searchEvaluateGoodsAllUsingGet: (query: SearchEvaluateGoodsAllUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoUserEvaluateGoodsListResponse, void>({
        path: `/api/wewill/user/evaluate/goods/list/all`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 제품 상세보기
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name InfoEvaluateGoodsUsingGet
     * @summary 평가신청 > 제품 > 평가상세 > 평가신청 전
     * @request GET:/api/wewill/user/evaluate/goods/{requestId}
     * @secure
     */
    infoEvaluateGoodsUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<UserEvaluateGoodsInfoResponse, void>({
        path: `/api/wewill/user/evaluate/goods/${requestId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 제품 신청 가능 유무 확인 및 통과하면 평가 시작
     *
     * @tags [평가자] - 평가신청 제품 API
     * @name CheckEvaluateUsingGet
     * @summary 평가신청 > 제품 > 평가상세 > 평가신청
     * @request GET:/api/wewill/user/evaluate/goods/{requestId}/check
     * @secure
     */
    checkEvaluateUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<UserEvaluateGoodsCheckStatusResponse, void>({
        path: `/api/wewill/user/evaluate/goods/${requestId}/check`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description '구독' 버튼 클릭 시, 등록
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name InsertSubscriptionVidoUsingPost
     * @summary 평가신청 > 평가상세 > 구독 API
     * @request POST:/api/wewill/user/evaluate/video
     * @secure
     */
    insertSubscriptionVidoUsingPost: (request: UserEvaluateSubscriptionVidoRequest, params: RequestParams = {}) =>
      this.request<UserEvaluateSubscriptionVidoResponse, void>({
        path: `/api/wewill/user/evaluate/video`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description '시청 인증하기' 버튼 클릭 시, 불성실평가 여부 'N' 등록
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name InsertMemberStickerClickStatusUsingPost
     * @summary 평가신청 > 평가상세 > 평가 중 중간 스티커
     * @request POST:/api/wewill/user/evaluate/video/sticker/{requestId}
     * @secure
     */
    insertMemberStickerClickStatusUsingPost: (
      { requestId, ...query }: InsertMemberStickerClickStatusUsingPostParams,
      params: RequestParams = {},
    ) =>
      this.request<string, void>({
        path: `/api/wewill/user/evaluate/video/sticker/${requestId}`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description '평가자에게 딱 맞는 평가 콘텐츠' 목록 조회
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name SearchUserEvaluateVidoUsingGet
     * @summary 평가신청 > 영상 > 평가목록 API
     * @request GET:/api/wewill/user/evaluate/video/vidoList
     * @secure
     */
    searchUserEvaluateVidoUsingGet: (query: SearchUserEvaluateVidoUsingGetParams, params: RequestParams = {}) =>
      this.request<UserEvaluateVidoSearchResponse, void>({
        path: `/api/wewill/user/evaluate/video/vidoList`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description '원하는 콘텐츠 다시보기' 목록 조회
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name SelectUserEvaluateReplyVidoUsingGet
     * @summary 평가신청 > 영상 > 다시보기 평가목록 API
     * @request GET:/api/wewill/user/evaluate/video/vidoReplayList
     * @secure
     */
    selectUserEvaluateReplyVidoUsingGet: (
      query: SelectUserEvaluateReplyVidoUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<UserEvaluateReplayVidoSearchResponse, void>({
        path: `/api/wewill/user/evaluate/video/vidoReplayList`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가 신청 시작 전 상세 조회
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name SelectUserEvaluateVidoUsingGet
     * @summary 평가신청 > 평가상세 > 평가 신청 전 API
     * @request GET:/api/wewill/user/evaluate/video/{requestId}
     * @secure
     */
    selectUserEvaluateVidoUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<UserEvaluateVidoInfoResponse, void>({
        path: `/api/wewill/user/evaluate/video/${requestId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description '평가 시작하기' 버튼 클릭 시, 조건 체크
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name CheckUserEvaluateVidoUsingGet
     * @summary 평가신청 > 평가상세 > 평가 신청 전 > 평가 시작하기 버튼 클릭 API
     * @request GET:/api/wewill/user/evaluate/video/{requestId}/check
     * @secure
     */
    checkUserEvaluateVidoUsingGet: (requestId: number, params: RequestParams = {}) =>
      this.request<UserEvaluateCheckResponse, void>({
        path: `/api/wewill/user/evaluate/video/${requestId}/check`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description '답변제출' 버튼 클릭 시, 등록
     *
     * @tags [평가자] - 평가신청 영상 API
     * @name InsertMemberEvaluationVidoAnswerUsingPost
     * @summary 평가신청 > 평가상세 > 평가하기 TAB > 설문 답변 등록 API
     * @request POST:/api/wewill/user/evaluate/video/{requestId}/{memberId}/evaluation/answer
     * @secure
     */
    insertMemberEvaluationVidoAnswerUsingPost: (
      requestId: number,
      memberId: string,
      request: UserEvaluateVidoAnswerInsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/evaluate/video/${requestId}/${memberId}/evaluation/answer`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  findId = {
    /**
     * @description 사용자가 입력한 휴대폰 번호로 인증번호 문자 발송
     *
     * @tags [사용자페이지] - 로그인 API
     * @name SendSmsFindIdUsingPost
     * @summary 로그인 > 아이디 찾기 > 인증번호 발송
     * @request POST:/api/wewill/user/find-id/send/sms/certification
     * @secure
     */
    sendSmsFindIdUsingPost: (req: FindIdByMobileRequest, params: RequestParams = {}) =>
      this.request<UserMemberFindIdSendSmsResponse, void>({
        path: `/api/wewill/user/find-id/send/sms/certification`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  findPassword = {
    /**
     * @description 사용자가 입력한 휴대폰 번호로 인증번호 문자 발송
     *
     * @tags [사용자페이지] - 로그인 API
     * @name SendSmsFindPasswordUsingPost
     * @summary 로그인 > 비밀번호 찾기 > 인증번호 발송
     * @request POST:/api/wewill/user/find-password/send/sms/certification
     * @secure
     */
    sendSmsFindPasswordUsingPost: (req: FindPwdByMobileRequest, params: RequestParams = {}) =>
      this.request<UserMemberFindPasswordSendSmsResponse, void>({
        path: `/api/wewill/user/find-password/send/sms/certification`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  login = {
    /**
     * @description 로그인
     *
     * @tags [사용자페이지] - 로그인 API
     * @name LoginUsingPost
     * @summary Wewill 로그인 API
     * @request POST:/api/wewill/user/login
     * @secure
     */
    loginUsingPost: (req: LoginRequest, params: RequestParams = {}) =>
      this.request<LoginResponse, void>({
        path: `/api/wewill/user/login`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  logout = {
    /**
     * @description 로그아웃
     *
     * @tags [사용자페이지] - 로그인 API
     * @name LogoutUsingGet
     * @summary 로그아웃 API
     * @request GET:/api/wewill/user/logout
     * @secure
     */
    logoutUsingGet: (params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/logout`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  markers = {
    /**
     * @description 평가자 회원가입
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name InsertEvaluatorMemberUsingPost
     * @summary 평가자 회원가입 API
     * @request POST:/api/wewill/user/markers
     * @secure
     */
    insertEvaluatorMemberUsingPost: (req: UserEvaluatorInsertRequest, params: RequestParams = {}) =>
      this.request<UserEvaluatorInsertResponse, void>({
        path: `/api/wewill/user/markers`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 수정(비밀번호 수정)
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyEvaluatorMemberPwdUsingPut
     * @summary 평가자 마이페이지 수정(비밀번호 수정) API
     * @request PUT:/api/wewill/user/markers/change/pwd
     * @secure
     */
    modifyEvaluatorMemberPwdUsingPut: (query: ModifyEvaluatorMemberPwdUsingPutParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/change/pwd`,
        method: "PUT",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 가입시 창작자와 휴대폰 번호가 중복되는지 확인
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name CheckDuplicatedMbtlnumUsingGet
     * @summary 회원가입 > 휴대폰 인증 > 휴대폰 번호 중복체크 API
     * @request GET:/api/wewill/user/markers/check/mbtlnum
     * @secure
     */
    checkDuplicatedMbtlnumUsingGet: (query: CheckDuplicatedMbtlnumUsingGetParams, params: RequestParams = {}) =>
      this.request<UserEvaluatorCheckMbtlnumResponse, void>({
        path: `/api/wewill/user/markers/check/mbtlnum`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 비밀번호 확인
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name CheckUserPwdUsingPost
     * @summary 마이페이지 > 회원 정보 > 비밀번호 확인 API
     * @request POST:/api/wewill/user/markers/check/pwd
     * @secure
     */
    checkUserPwdUsingPost: (req: UserEvaluatorCheckPwdRequest, params: RequestParams = {}) =>
      this.request<UserEvaluatorCheckPwdResponse, void>({
        path: `/api/wewill/user/markers/check/pwd`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 주소 수정
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyAddressUsingPut
     * @summary 마이페이지 > 회원 정보 > 주소 수정 API
     * @request PUT:/api/wewill/user/markers/modify/address
     * @secure
     */
    modifyAddressUsingPut: (req: UserEvaluatorModifyAddressRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/modify/address`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 광고성 정보 수신동의 수정
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyAdvrtsUsingPut
     * @summary 마이페이지 > 회원 정보 > 광고성 정보 수신동의 수정 API
     * @request PUT:/api/wewill/user/markers/modify/advrts
     * @secure
     */
    modifyAdvrtsUsingPut: (req: UserEvaluatorModifyAdvrtsRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/modify/advrts`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 광고성 정보 수신동의 수정
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyAlarmUsingPut
     * @summary 마이페이지 > 회원 정보 > 알림설정 수정 API
     * @request PUT:/api/wewill/user/markers/modify/alarm
     * @secure
     */
    modifyAlarmUsingPut: (req: UserEvaluatorModifyAlarmRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/modify/alarm`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 출금계좌 수정
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyBankUsingPut
     * @summary 마이페이지 > 회원 정보 > 출금계좌 수정 API
     * @request PUT:/api/wewill/user/markers/modify/bank
     * @secure
     */
    modifyBankUsingPut: (req: UserEvaluatorModifyBankRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/modify/bank`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 이메일 수정
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyEmailUsingPut
     * @summary 마이페이지 > 회원 정보 > 이메일 수정 API
     * @request PUT:/api/wewill/user/markers/modify/email
     * @secure
     */
    modifyEmailUsingPut: (req: UserEvaluatorModifyEmailRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/modify/email`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 이름 수정
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyMbtlnumUsingPut
     * @summary 마이페이지 > 회원 정보 > 휴대폰번호 수정 API
     * @request PUT:/api/wewill/user/markers/modify/mbtlnum
     * @secure
     */
    modifyMbtlnumUsingPut: (req: UserEvaluatorModifyMbtlnumRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/modify/mbtlnum`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지(기본정보)
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name InfoEvaluatorMemberUsingGet
     * @summary 평가자 마이페이지(기본정보) API
     * @request GET:/api/wewill/user/markers/mypage/basic
     * @secure
     */
    infoEvaluatorMemberUsingGet: (params: RequestParams = {}) =>
      this.request<UserEvaluatorBasicInfoResponse, void>({
        path: `/api/wewill/user/markers/mypage/basic`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지(계약서 정보)
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name InfoEvaluatorContractsUsingGet
     * @summary 마이페이지 > 회원 정보 > 계약서 조회 API
     * @request GET:/api/wewill/user/markers/mypage/contracts
     * @secure
     */
    infoEvaluatorContractsUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoUserEvaluatorContractInfoResponse, void>({
        path: `/api/wewill/user/markers/mypage/contracts`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 계약서 갱신
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name ModifyEvaluatorContractMemberUsingPut
     * @summary 마이페이지 > 회원 정보 > 계약서 작성 API
     * @request PUT:/api/wewill/user/markers/mypage/contracts
     * @secure
     */
    modifyEvaluatorContractMemberUsingPut: (req: UserEvaluatorContractModifyRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/mypage/contracts`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지(포인트 내역)
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name InfoEvaluatorPointsUsingGet
     * @summary 마이페이지 > 포인트 내역 API
     * @request GET:/api/wewill/user/markers/mypage/points
     * @secure
     */
    infoEvaluatorPointsUsingGet: (query: InfoEvaluatorPointsUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoUserEvaluatorPointResponse, void>({
        path: `/api/wewill/user/markers/mypage/points`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용자 평가자 마이페이지 출금 신청
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name InsertEvaluatorPointDefraymentRequestUsingPost
     * @summary 마이페이지 > 포인트 내역 > 출금 신청 등록 API
     * @request POST:/api/wewill/user/markers/mypage/points/defrayment
     * @secure
     */
    insertEvaluatorPointDefraymentRequestUsingPost: (
      req: UserEvaluatorDefraymentReqstInsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<UserEvaluatorDefraymentReqstInsertResponse, void>({
        path: `/api/wewill/user/markers/mypage/points/defrayment`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용자 평가자 마이페이지 출금 신청 기본정보 조회
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name SelectEvaluatorPointDefraymentBasicInfoUsingGet
     * @summary 마이페이지 > 포인트 내역 > 출금 신청 기본정보 조회 API
     * @request GET:/api/wewill/user/markers/mypage/points/defrayment/{memberId}
     * @secure
     */
    selectEvaluatorPointDefraymentBasicInfoUsingGet: (memberId: number, params: RequestParams = {}) =>
      this.request<UserEvaluatorDefraymentReqstInfoResponse, void>({
        path: `/api/wewill/user/markers/mypage/points/defrayment/${memberId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 오늘 얻은 포인트, 보유 포인트, 출금 완료 포인트 조회
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name InfoEvaluatorPointUsingGet
     * @summary 마이페이지 > 포인트 내역 > 포인트 정보 API
     * @request GET:/api/wewill/user/markers/mypage/points/{memberId}
     * @secure
     */
    infoEvaluatorPointUsingGet: (memberId: number, params: RequestParams = {}) =>
      this.request<UserEvaluatorPointInfoResponse, void>({
        path: `/api/wewill/user/markers/mypage/points/${memberId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 등급업시 위촉계약서 업로드
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name UpgradeEvaluatorUsingPost
     * @summary 마이페이지 > 평가자 등급 업 > 위촉계약서 등록 API
     * @request POST:/api/wewill/user/markers/mypage/upgrade
     * @secure
     */
    upgradeEvaluatorUsingPost: (req: UserEvaluatorUpgradeRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/mypage/upgrade`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가자 마이페이지 회원탈퇴
     *
     * @tags [사용자페이지] - 평가자 회원 API
     * @name SecessionEvaluatorUsingPut
     * @summary 마이페이지 > 회원 정보 > 회원탈퇴 API
     * @request PUT:/api/wewill/user/markers/secession
     * @secure
     */
    secessionEvaluatorUsingPut: (req: UserEvaluatorSecessionInsertRequest, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/markers/secession`,
        method: "PUT",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  media = {
    /**
     * @description 미디어컨텐츠 카드 등록 API
     *
     * @tags [사용자] - 미디어 API
     * @name CardUsingPost
     * @summary 미디어컨텐츠 카드 등록 API
     * @request POST:/api/wewill/user/media/cards
     * @secure
     */
    cardUsingPost: (request: ContentCardRequest, params: RequestParams = {}) =>
      this.request<ContentCardResponse, void>({
        path: `/api/wewill/user/media/cards`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 미디어 워크플로우 상세 조회 API
     *
     * @tags [사용자] - 미디어 API
     * @name SelectCardUsingPost
     * @summary 미디어 워크플로우 상세 조회 API
     * @request POST:/api/wewill/user/media/cards/{contentCardNo}
     * @secure
     */
    selectCardUsingPost: (contentCardNo?: number, params: RequestParams = {}) =>
      this.request<MediaWorkflowResultResponse, void>({
        path: `/api/wewill/user/media/cards/${contentCardNo}`,
        method: "POST",
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  mypage = {
    /**
     * @description 마이페이지 상단 영역 정의
     *
     * @tags [평가자] - 마이페이지 대시보드 API
     * @name SearchEvaluateDashboardHearderUsingGet
     * @summary 마이페이지 > 평가자 > 해더 정보 API
     * @request GET:/api/wewill/user/mypage/dashboard
     * @secure
     */
    searchEvaluateDashboardHearderUsingGet: (params: RequestParams = {}) =>
      this.request<UserDashboardHeaderResponse, void>({
        path: `/api/wewill/user/mypage/dashboard`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 평가 영상/제품 상태값(true일 경우, 평가신청 상세 진입 가능)
     *
     * @tags [평가자] - 마이페이지 대시보드 API
     * @name SelectEvaluateDashboardUsingPost
     * @summary 마이페이지 > 대시보드 > 평가 영상/제품 상세 API
     * @request POST:/api/wewill/user/mypage/dashboard/detailCheck
     * @secure
     */
    selectEvaluateDashboardUsingPost: (request: UserDashboardInfoRequest, params: RequestParams = {}) =>
      this.request<UserDashboardInfoResponse, void>({
        path: `/api/wewill/user/mypage/dashboard/detailCheck`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 평가 목록 조회
     *
     * @tags [평가자] - 마이페이지 대시보드 API
     * @name SearchEvaluateDashboardGoodsUsingGet
     * @summary 마이페이지 > 대시보드 > 제품 평가 목록 API
     * @request GET:/api/wewill/user/mypage/dashboard/goodsList
     * @secure
     */
    searchEvaluateDashboardGoodsUsingGet: (
      query: SearchEvaluateDashboardGoodsUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<UserDashboardGoodsSearchResponse, void>({
        path: `/api/wewill/user/mypage/dashboard/goodsList`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 영상 평가 목록 조회
     *
     * @tags [평가자] - 마이페이지 대시보드 API
     * @name SearchEvaluateDashboardVidoUsingGet
     * @summary 마이페이지 > 대시보드 > 영상 평가 목록 API
     * @request GET:/api/wewill/user/mypage/dashboard/vidoList
     * @secure
     */
    searchEvaluateDashboardVidoUsingGet: (
      query: SearchEvaluateDashboardVidoUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<UserDashboardVidoSearchResponse, void>({
        path: `/api/wewill/user/mypage/dashboard/vidoList`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 대시보드 > 평가자 개별 정보 상세 (로그인 없이)
     *
     * @tags [의뢰자] - 마이페이지 의뢰 대시보드 API
     * @name ExpSearchReqestDashboardUsingGet
     * @summary 마이페이지 > 대시 보드 > 보고서 (프리미엄) > 평가자 개별정보 상세 (로그인 없이) API
     * @request GET:/api/wewill/user/mypage/reqestdashboard
     * @secure
     */
    expSearchReqestDashboardUsingGet: (query: ExpSearchReqestDashboardUsingGetParams, params: RequestParams = {}) =>
      this.request<UserReqestDashboardSearchResponse, void>({
        path: `/api/wewill/user/mypage/reqestdashboard`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 대시보드 > 평가자 개별 정보 상세
     *
     * @tags [의뢰자] - 마이페이지 의뢰 대시보드 API
     * @name SearchReqestDashboardUsingGet
     * @summary 마이페이지 > 대시 보드 > 보고서 (프리미엄) > 평가자 개별정보 상세 API
     * @request GET:/api/wewill/user/mypage/reqestdashboard/{reqestId}
     * @secure
     */
    searchReqestDashboardUsingGet: (
      { reqestId, ...query }: SearchReqestDashboardUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<UserReqestDashboardSearchResponse, void>({
        path: `/api/wewill/user/mypage/reqestdashboard/${reqestId}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  nonCustomers = {
    /**
     * @description 질문 등록
     *
     * @tags [고객센터] - 비회원(GatePage) 문의 API
     * @name InsertNonQuestionUsingPost
     * @summary 질문 등록 API
     * @request POST:/api/wewill/user/nonCustomers
     * @secure
     */
    insertNonQuestionUsingPost: (request: NonCustomerInsertRequest, params: RequestParams = {}) =>
      this.request<NonCustomerInsertResponse, void>({
        path: `/api/wewill/user/nonCustomers`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  order = {
    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상 업로드 일 경우 등록
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderAllUsingPost
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상 업로드 일 경우 등록 API
     * @request POST:/api/wewill/user/order
     * @secure
     */
    insertOrderAllUsingPost: (request: OrderMergeRequest, params: RequestParams = {}) =>
      this.request<OrderMergeResponse, void>({
        path: `/api/wewill/user/order`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 통합컨텐츠 영상 스트리밍정보 상세
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectContentCardVidoUsingGet
     * @summary 통합컨텐츠 영상 스트리밍정보 상세 API
     * @request GET:/api/wewill/user/order/contentCardVido/{contentCardNo}
     * @secure
     */
    selectContentCardVidoUsingGet: (contentCardNo: number, params: RequestParams = {}) =>
      this.request<ContentCardVidoResponse, void>({
        path: `/api/wewill/user/order/contentCardVido/${contentCardNo}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 고객센터 > 1:1 문의 > 문의하기 :: 진입 시 > 의뢰 목록
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderListByMeUsingGet
     * @summary 고객센터 > 1:1 문의 > 문의하기 :: 진입 시 > 의뢰 목록 API
     * @request GET:/api/wewill/user/order/me
     * @secure
     */
    selectOrderListByMeUsingGet: (params: RequestParams = {}) =>
      this.request<ListVoCodeListResponse, void>({
        path: `/api/wewill/user/order/me`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 의뢰 등록
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 제품 API
     * @name InsertProductUsingPost
     * @summary 의뢰 신청 > 제품 > 의뢰 콘텐츠 설정 > 제품 의뢰 등록 API
     * @request POST:/api/wewill/user/order/product
     * @secure
     */
    insertProductUsingPost: (request: ProductInsertRequest, params: RequestParams = {}) =>
      this.request<ProductInsertResponse, void>({
        path: `/api/wewill/user/order/product`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 의뢰 등록 상세
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 제품 API
     * @name SelectProductUsingGet
     * @summary 의뢰 신청 > 제품 > 의뢰 콘텐츠 설정 > 제품 의뢰 등록 상세 API
     * @request GET:/api/wewill/user/order/product/{id}
     * @secure
     */
    selectProductUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<ProductInfoResponse, void>({
        path: `/api/wewill/user/order/product/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 의뢰 수정
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 제품 API
     * @name ModifyProductUsingPut
     * @summary 의뢰 신청 > 제품 > 의뢰 콘텐츠 설정 > 제품 의뢰 수정 API
     * @request PUT:/api/wewill/user/order/product/{id}
     * @secure
     */
    modifyProductUsingPut: (id: number, request: ProductModifyRequest, params: RequestParams = {}) =>
      this.request<ProductModifyResponse, void>({
        path: `/api/wewill/user/order/product/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 제품 설문전체 일괄 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyAllUsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 설문 일괄 등록 :: 저장
     * @request PATCH:/api/wewill/user/order/productsurvey
     * @secure
     */
    insertOrderProductSurveyAllUsingPatch: (
      orderSurveyInsertAllRequest: OrderSurveyInsertAllRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertAllResponse, void>({
        path: `/api/wewill/user/order/productsurvey`,
        method: "PATCH",
        body: orderSurveyInsertAllRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 전 설문 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep1UsingPost
     * @summary 의뢰 신청  > 의뢰 설문 설정 > 제품 > 사용 전 설문 등록 :: 저장
     * @request POST:/api/wewill/user/order/productsurvey/steps/1
     * @secure
     */
    insertOrderProductSurveyStep1UsingPost: (
      orderProductSurveyStep1InsertRequest: OrderProductSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/1`,
        method: "POST",
        body: orderProductSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 전 설문 수정. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderProductSurveyStep1UsingPut
     * @summary 의뢰 신청  > 의뢰 설문 설정 > 제품 > 사용 전 설문 수정 :: 저장
     * @request PUT:/api/wewill/user/order/productsurvey/steps/1
     * @secure
     */
    deleteInsertOrderProductSurveyStep1UsingPut: (
      orderProductSurveyStep1InsertRequest: OrderProductSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/1`,
        method: "PUT",
        body: orderProductSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 중 설문 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep2UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 중 설문 등록 :: 저장
     * @request POST:/api/wewill/user/order/productsurvey/steps/2
     * @secure
     */
    insertOrderProductSurveyStep2UsingPost: (
      orderSurveyProductStep2InsertRequest: OrderProductSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/2`,
        method: "POST",
        body: orderSurveyProductStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 중 설문 수정. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderProductSurveyStep2UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 중 설문 수정 :: 저장
     * @request PUT:/api/wewill/user/order/productsurvey/steps/2
     * @secure
     */
    deleteInsertOrderProductSurveyStep2UsingPut: (
      orderSurveyProductStep2InsertRequest: OrderProductSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/2`,
        method: "PUT",
        body: orderSurveyProductStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 후 설문 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep3UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 후 설문 등록 :: 저장
     * @request POST:/api/wewill/user/order/productsurvey/steps/3
     * @secure
     */
    insertOrderProductSurveyStep3UsingPost: (
      orderProductSurveyStep3InsertRequest: OrderProductSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/3`,
        method: "POST",
        body: orderProductSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용 후 설문 수정. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderProductSurveyStep3UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 후 설문 수정 :: 저장
     * @request PUT:/api/wewill/user/order/productsurvey/steps/3
     * @secure
     */
    deleteInsertOrderProductSurveyStep3UsingPut: (
      orderProductSurveyStep3InsertRequest: OrderProductSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/3`,
        method: "PUT",
        body: orderProductSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 견적 요청 등록.
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderProductSurveyStep4UsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 제품 > 사용 후 설문 :: 견적요청
     * @request PATCH:/api/wewill/user/order/productsurvey/steps/4
     * @secure
     */
    insertOrderProductSurveyStep4UsingPatch: (
      orderSurveyFinalInsertRequest: OrderSurveyFinalInsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<InsertReqestEstimateResponse, void>({
        path: `/api/wewill/user/order/productsurvey/steps/4`,
        method: "PATCH",
        body: orderSurveyFinalInsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 심사반려인 경우 의뢰수정 완료
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name UpdateReqestStatusEvaluateStandbyUsingGet
     * @summary 심사반려인 경우 의뢰수정 완료 API
     * @request GET:/api/wewill/user/order/reqest/{reqestId}/evaluate/standby
     * @secure
     */
    updateReqestStatusEvaluateStandbyUsingGet: (reqestId: number, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/order/reqest/${reqestId}/evaluate/standby`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰반려사유 조회
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectReturnResnInReqestCancelUsingGet
     * @summary 의뢰반려사유 조회 API
     * @request GET:/api/wewill/user/order/reqestCancel/returnResn
     * @secure
     */
    selectReturnResnInReqestCancelUsingGet: (
      query: SelectReturnResnInReqestCancelUsingGetParams,
      params: RequestParams = {},
    ) =>
      this.request<OrderReturnResnResponse, void>({
        path: `/api/wewill/user/order/reqestCancel/returnResn`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 저장한 임시데이터 조회. 디코딩 샘플 const json = JSON.parse(decodeURI(response.jsonDataUriEncoded));
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectSaveTempUsingGet
     * @summary 의뢰신청 임시저장 데이터 조회
     * @request GET:/api/wewill/user/order/save/{reqestId}
     * @secure
     */
    selectSaveTempUsingGet: (reqestId: number, params: RequestParams = {}) =>
      this.request<ReqestSaveResponse, void>({
        path: `/api/wewill/user/order/save/${reqestId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰신청 전체 임시저장. 재호출 시 덮어씀. 인코딩 샘플 const json = {a:1} const jsonStrURIEncoded = encodeURI(JSON.stringify(json));
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SaveTempUsingPut
     * @summary 의뢰신청 전체 임시저장
     * @request PUT:/api/wewill/user/order/save/{reqestId}
     * @secure
     */
    saveTempUsingPut: (reqestId: number, jsonDataUriEncoded: string, params: RequestParams = {}) =>
      this.request<ReqestSaveResponse, void>({
        path: `/api/wewill/user/order/save/${reqestId}`,
        method: "PUT",
        body: jsonDataUriEncoded,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 상세
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderSetUsingGet
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 상세 API
     * @request GET:/api/wewill/user/order/set/{id}
     * @secure
     */
    selectOrderSetUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<OrderSetInfoResponse, void>({
        path: `/api/wewill/user/order/set/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 등록 & 수정
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderSetUsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 공통 > 평가자 설정 등록 & 수정 API
     * @request PUT:/api/wewill/user/order/set/{id}
     * @secure
     */
    modifyOrderSetUsingPut: (id: number, request: OrderSetModifyRequest, params: RequestParams = {}) =>
      this.request<OrderSetModifyResponse, void>({
        path: `/api/wewill/user/order/set/${id}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 결제 > 주문/결제 상세
     *
     * @tags [의뢰자] - 의뢰 신청 > 결제 API
     * @name SelectOrderSettlementInfoUsingGet
     * @summary 의뢰 신청 > 결제 > 주문/결제 상세 API
     * @request GET:/api/wewill/user/order/settlement/{id}
     * @secure
     */
    selectOrderSettlementInfoUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<OrderSettlementInfoResponse, void>({
        path: `/api/wewill/user/order/settlement/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 설문전체 일괄 등록. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyAllUsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 설문 일괄 등록
     * @request PATCH:/api/wewill/user/order/survey
     * @secure
     */
    insertOrderSurveyAllUsingPatch: (
      orderSurveyInsertAllRequest: OrderSurveyInsertAllRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertAllResponse, void>({
        path: `/api/wewill/user/order/survey`,
        method: "PATCH",
        body: orderSurveyInsertAllRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 프리미엄 > 사전 설문 등록용 정책데이터
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderSurveyPolicyUsingGet
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사전 설문 등록 :: 정책데이터
     * @request GET:/api/wewill/user/order/survey/policies
     * @secure
     */
    selectOrderSurveyPolicyUsingGet: (params: RequestParams = {}) =>
      this.request<OrderSurveyPolicyResponse, void>({
        path: `/api/wewill/user/order/survey/policies`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 사전 설문 등록용
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep1UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사전 설문 등록 :: 저장
     * @request POST:/api/wewill/user/order/survey/steps/1
     * @secure
     */
    insertOrderSurveyStep1UsingPost: (
      orderSurveyStep1InsertRequest: OrderSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/survey/steps/1`,
        method: "POST",
        body: orderSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사전 설문 수정용
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderSurveyStep1UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사전 설문 수정 :: 저장
     * @request PUT:/api/wewill/user/order/survey/steps/1
     * @secure
     */
    deleteInsertOrderSurveyStep1UsingPut: (
      orderSurveyStep1InsertRequest: OrderSurveyStep1InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/survey/steps/1`,
        method: "PUT",
        body: orderSurveyStep1InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 실시간 설문 일괄 등록용. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep2UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 실시간 설문 등록 :: 저장
     * @request POST:/api/wewill/user/order/survey/steps/2
     * @secure
     */
    insertOrderSurveyStep2UsingPost: (
      orderSurveyStep2InsertRequest: OrderSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyStep2InsertResponse, void>({
        path: `/api/wewill/user/order/survey/steps/2`,
        method: "POST",
        body: orderSurveyStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 실시간 설문 일괄 수정용. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderSurveyStep2UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 실시간 설문 수정 :: 저장
     * @request PUT:/api/wewill/user/order/survey/steps/2
     * @secure
     */
    deleteInsertOrderSurveyStep2UsingPut: (
      orderSurveyStep2InsertRequest: OrderSurveyStep2InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyStep2InsertResponse, void>({
        path: `/api/wewill/user/order/survey/steps/2`,
        method: "PUT",
        body: orderSurveyStep2InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사후 설문 등록용. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep3UsingPost
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사후 설문 등록 :: 저장
     * @request POST:/api/wewill/user/order/survey/steps/3
     * @secure
     */
    insertOrderSurveyStep3UsingPost: (
      orderSurveyStep3InsertRequest: OrderSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/survey/steps/3`,
        method: "POST",
        body: orderSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 사후 설문 수정용. 등록/수정 동일 - delete / insert
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name DeleteInsertOrderSurveyStep3UsingPut
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 프리미엄 > 사후 설문 수정 :: 저장
     * @request PUT:/api/wewill/user/order/survey/steps/3
     * @secure
     */
    deleteInsertOrderSurveyStep3UsingPut: (
      orderSurveyStep3InsertRequest: OrderSurveyStep3InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderSurveyInsertResponse, void>({
        path: `/api/wewill/user/order/survey/steps/3`,
        method: "PUT",
        body: orderSurveyStep3InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 최종평가 및 견적 요청 등록. 베이직은 step 1~3 없이 4번 즉시 등록
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderSurveyStep4UsingPatch
     * @summary 의뢰 신청 > 의뢰 설문 설정 > 영상 > 베이직/프리미엄 > 최종평가 :: 견적요청
     * @request PATCH:/api/wewill/user/order/survey/steps/4
     * @secure
     */
    insertOrderSurveyStep4UsingPatch: (
      orderSurveyStep4InsertRequest: OrderSurveyStep4InsertRequest,
      params: RequestParams = {},
    ) =>
      this.request<InsertReqestEstimateResponse, void>({
        path: `/api/wewill/user/order/survey/steps/4`,
        method: "PATCH",
        body: orderSurveyStep4InsertRequest,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 등록
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderVidoUsingPost
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 등록 API
     * @request POST:/api/wewill/user/order/vido
     * @secure
     */
    insertOrderVidoUsingPost: (request: OrderVidoInsertRequest, params: RequestParams = {}) =>
      this.request<OrderVidoInsertResponse, void>({
        path: `/api/wewill/user/order/vido`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 등록
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name InsertOrderYoutubeUsingPost
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 등록 API
     * @request POST:/api/wewill/user/order/youtube
     * @secure
     */
    insertOrderYoutubeUsingPost: (request: OrderYoutubeInsertRequest, params: RequestParams = {}) =>
      this.request<OrderYoutubeInsertResponse, void>({
        path: `/api/wewill/user/order/youtube`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > (Youtube 일 경우 & 영상 업로드 일 경우) 상세
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name SelectOrderVidoUsingGet
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > (Youtube 일 경우 & 영상 업로드 일 경우) 상세 API
     * @request GET:/api/wewill/user/order/{id}
     * @secure
     */
    selectOrderVidoUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<OrderVidoInfoResponse, void>({
        path: `/api/wewill/user/order/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 수정
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderVidoUsingPut
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > 영상 업로드 일 경우 수정 API
     * @request PUT:/api/wewill/user/order/{id}/vido/{vidoId}
     * @secure
     */
    modifyOrderVidoUsingPut: (
      id: number,
      vidoId: number,
      request: OrderVidoModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderVidoModifyResponse, void>({
        path: `/api/wewill/user/order/${id}/vido/${vidoId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상업로드 일 경우 수정
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderAllUsingPut
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube & 영상업로드 일 경우 수정 API
     * @request PUT:/api/wewill/user/order/{id}/vido/{vidoId}/common
     * @secure
     */
    modifyOrderAllUsingPut: (id: number, vidoId: number, request: OrderMergeRequest, params: RequestParams = {}) =>
      this.request<OrderMergeResponse, void>({
        path: `/api/wewill/user/order/${id}/vido/${vidoId}/common`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 수정
     *
     * @tags [의뢰자] - 의뢰 콘텐츠 설정 & 의뢰 설문 설정(공통) API
     * @name ModifyOrderYoutubeUsingPut
     * @summary 의뢰 신청 > 영상 > 의뢰 콘텐츠 설정 > Youtube 일 경우 수정 API
     * @request PUT:/api/wewill/user/order/{id}/youtube/{vidoId}
     * @secure
     */
    modifyOrderYoutubeUsingPut: (
      id: number,
      vidoId: number,
      request: OrderYoutubeModifyRequest,
      params: RequestParams = {},
    ) =>
      this.request<OrderYoutubeModifyResponse, void>({
        path: `/api/wewill/user/order/${id}/youtube/${vidoId}`,
        method: "PUT",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  policies = {
    /**
     * @description 욕설목록
     *
     * @tags [사용자] - 정책 API
     * @name GetBadwordsPolicyUsingGet
     * @summary 욕설 정책
     * @request GET:/api/wewill/user/policies/badwords
     * @secure
     */
    getBadwordsPolicyUsingGet: (params: RequestParams = {}) =>
      this.request<string[], void>({
        path: `/api/wewill/user/policies/badwords`,
        method: "GET",
        secure: true,
        ...params,
      }),

    /**
     * @description 욕설 체크
     *
     * @tags [사용자] - 정책 API
     * @name CheckBadwordsUsingPost
     * @summary 욕설 체크
     * @request POST:/api/wewill/user/policies/badwords
     * @secure
     */
    checkBadwordsUsingPost: (wordList: string[], params: RequestParams = {}) =>
      this.request<CheckBadWordsResponse, void>({
        path: `/api/wewill/user/policies/badwords`,
        method: "POST",
        body: wordList,
        secure: true,
        type: ContentType.Json,
        ...params,
      }),
  };
  posts = {
    /**
     * @description 사용자 이벤트 게시글 목록 조회
     *
     * @tags [GatePage 공통][사용자페이지 공통] - 이벤트 게시글 API
     * @name SearchUserEventPostUsingGet
     * @summary 고객센터 > 이벤트 목록 API
     * @request GET:/api/wewill/user/posts/event
     * @secure
     */
    searchUserEventPostUsingGet: (query: SearchUserEventPostUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPostUserEventSearchResponse, void>({
        path: `/api/wewill/user/posts/event`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 미확인된 이벤트 수
     *
     * @tags [GatePage 공통][사용자페이지 공통] - 이벤트 게시글 API
     * @name UserUnCheckEventPostCntUsingGet
     * @summary We will_평가자_사용자페이지 > 미확인된 이벤트 수 조회 API
     * @request GET:/api/wewill/user/posts/event/checkEventCnt
     * @secure
     */
    userUnCheckEventPostCntUsingGet: (params: RequestParams = {}) =>
      this.request<PostUserUnCheckEventPostCntResponse, void>({
        path: `/api/wewill/user/posts/event/checkEventCnt`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용자 이벤트 게시글 상세 조회
     *
     * @tags [GatePage 공통][사용자페이지 공통] - 이벤트 게시글 API
     * @name SelectUserEventPostUsingGet
     * @summary 고객센터 > 이벤트 상세 조회 API
     * @request GET:/api/wewill/user/posts/event/{id}
     * @secure
     */
    selectUserEventPostUsingGet: ({ id, ...query }: SelectUserEventPostUsingGetParams, params: RequestParams = {}) =>
      this.request<PostUserEventInfoResponse, void>({
        path: `/api/wewill/user/posts/event/${id}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용자 공지사항 게시글 목록 조회
     *
     * @tags [GatePage 공통][사용자페이지 공통] - 공지사항 게시글 API
     * @name SearchUserNoticePostUsingGet
     * @summary 고객센터 > 공지사항 목록 API
     * @request GET:/api/wewill/user/posts/notice
     * @secure
     */
    searchUserNoticePostUsingGet: (query: SearchUserNoticePostUsingGetParams, params: RequestParams = {}) =>
      this.request<PagingInfoPostUserNoticeSearchResponse, void>({
        path: `/api/wewill/user/posts/notice`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 사용자 공지사항 게시글 상세 조회
     *
     * @tags [GatePage 공통][사용자페이지 공통] - 공지사항 게시글 API
     * @name SelectUserNoticePostUsingGet
     * @summary 고객센터 > 공지사항 상세 조회 API
     * @request GET:/api/wewill/user/posts/notice/{id}
     * @secure
     */
    selectUserNoticePostUsingGet: (id: number, params: RequestParams = {}) =>
      this.request<PostUserNoticeInfoResponse, void>({
        path: `/api/wewill/user/posts/notice/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  recaptcha = {
    /**
     * @description 리캡챠
     *
     * @tags 리캡챠 API
     * @name RecaptchaUsingPost
     * @summary 리캡챠 API
     * @request POST:/api/wewill/user/recaptcha
     * @secure
     */
    recaptchaUsingPost: (request: TokenRequest, params: RequestParams = {}) =>
      this.request<RecaptchaResponse, void>({
        path: `/api/wewill/user/recaptcha`,
        method: "POST",
        body: request,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  refresh = {
    /**
     * @description refresh token 호출 api
     *
     * @tags [사용자 인증] - 사용자 인증 API
     * @name RefreshTokenUsingPost
     * @summary 사용자 Refresh 토큰 호출 API
     * @request POST:/api/wewill/user/refresh
     * @secure
     */
    refreshTokenUsingPost: (req: UserMemberRefreshTokenRequest, params: RequestParams = {}) =>
      this.request<UserMemberRefreshTokenResponse, void>({
        path: `/api/wewill/user/refresh`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  sms = {
    /**
     * @description 회원등록 문자발송 인증번호 확인
     *
     * @tags [사용자페이지] - 로그인 API
     * @name CheckSendSmsUserUsingPost
     * @summary 로그인 > 아이디 찾기 > 휴대번호로 찾기 > 인증번호 확인(실시간) API
     * @request POST:/api/wewill/user/sms/certification
     * @secure
     */
    checkSendSmsUserUsingPost: (query: CheckSendSmsUserUsingPostParams, params: RequestParams = {}) =>
      this.request<BlankResponse, void>({
        path: `/api/wewill/user/sms/certification`,
        method: "POST",
        query: query,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description 이름, 전화번호, 메시지 아이디, code값으로 재 확인
     *
     * @tags [사용자페이지] - 로그인 API
     * @name FindIdByMobileUsingPost
     * @summary 로그인 > 아이디 찾기 > 휴대전화로 찾기 > 인증번호 확인 후 확인 API
     * @request POST:/api/wewill/user/sms/find-id
     * @secure
     */
    findIdByMobileUsingPost: (req: UserMemberFindIdConfirmRequest, params: RequestParams = {}) =>
      this.request<UserFindIdResponse, void>({
        path: `/api/wewill/user/sms/find-id`,
        method: "POST",
        body: req,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  userme = {
    /**
     * @description user me 호출 api
     *
     * @tags [사용자 인증] - 사용자 인증 API
     * @name UserMeUsingGet
     * @summary UserMe 조회 API
     * @request GET:/api/wewill/user/userme
     * @secure
     */
    userMeUsingGet: (params: RequestParams = {}) =>
      this.request<UserMe, void>({
        path: `/api/wewill/user/userme`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),
  };
}
