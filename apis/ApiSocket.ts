/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/**
 * 미디어 컨버터 오류 결과 상세
 */
export interface Detail {
  /**
   * AWS계정ID
   * @example 745109727851
   */
  accountId?: string;

  /**
   * 오류코드
   * @format int32
   * @example 1040
   */
  errorCode?: number;

  /**
   * 오류메세지
   * @example Invalid audio track specified for audio_selector [1]. Audio track [1] not found in input container.
   */
  errorMessage?: string;

  /**
   * 작업ID
   * @example 1622298028173-ccztlo
   */
  jobId?: string;

  /**
   * 큐 ARN정보
   * @example arn:aws:mediaconvert:ap-northeast-2:745109727851:queues/Default
   */
  queue?: string;

  /**
   * 상태
   * @example ERROR
   */
  status?: string;

  /**
   * timestamp
   * @format int64
   * @example 1622298032017
   */
  timestamp?: number;

  /**
   * 사용자메타데이터
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  userMetadata?: UserMetadata;
}

export interface EgressEndpoints {
  /**
   * cmaf
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/f6a738caadd041ee81b956d014cfa67c/27f9d779da5f4b85a5c0732cae6d2a54/index.m3u8
   */
  CMAF?: string;

  /**
   * dash
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/7c033e24f8c54d13833bd902b9d22c33/c916a709ea62459abe2b7b4fb25b9b86/index.mpd
   */
  DASH?: string;

  /**
   * hls
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/32a0e4f73a8b402f9e84d3350da33474/a3c862e9dd854f8ea6fb18b7959fe254/index.m3u8
   */
  HLS?: string;

  /**
   * mss
   * @example https://d1dopqicmcx111.cloudfront.net/out/v1/661db9313854408290a49ca34b410ac4/961e172ab1f844209b1a9afd70be9bc1/78623a75eec14c74861a821dd3022395/index.ism/Manifest
   */
  MSS?: string;
}

export interface InitResponse {
  /** host url */
  host?: string;

  /** sub url */
  subUrl?: string;

  /** websocket url */
  wsUrl?: string;
}

/**
 * 미디어 컨버터 오류 결과
 */
export interface MediaConvertErrorResult {
  /**
   * AWS계정ID
   * @example 745109727851
   */
  account?: string;

  /**
   * 오류 상세
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  detail?: Detail;

  /**
   * 상세타입
   * @example MediaConvert Job State Change
   */
  "detail-type"?: string;

  /**
   * id
   * @example 5a0c3d8c-c9c6-4b18-f796-16fee7540ba4
   */
  id?: string;

  /**
   * 리전
   * @example ap-northeast-2
   */
  region?: string;

  /**
   * 리소스 ARN정보
   * @example ["arn:aws:mediaconvert:ap-northeast-2:745109727851:jobs/1622298028173-ccztlo"]
   */
  resources?: string[];

  /**
   * 원본
   * @example aws.mediaconvert
   */
  source?: string;

  /**
   * 일시
   * @format date-time
   * @example 2021-05-29T14:20:32Z
   */
  time?: string;

  /**
   * 미디어컨버터 버전
   * @example 0
   */
  version?: string;
}

/**
 * 미디어 컨버터 결과
 */
export interface MediaConvertResult {
  /** acceleratedTranscoding */
  acceleratedTranscoding?: string;

  /** archiveSource */
  archiveSource?: string;

  /** cloudFront */
  cloudFront?: string;
  customTemplate?: boolean;

  /** destBucket */
  destBucket?: string;

  /** egressEndpoints */
  egressEndpoints?: EgressEndpoints;

  /** enableMediaPackage */
  enableMediaPackage?: boolean;

  /** enableSns */
  enableSns?: boolean;

  /** enableSqs */
  enableSqs?: boolean;

  /** encodeJobId */
  encodeJobId?: string;

  /**
   * encodingProfile
   * @format int32
   */
  encodingProfile?: number;

  /** endTime */
  endTime?: string;

  /** frameCapture */
  frameCapture?: boolean;

  /**
   * frameCaptureHeight
   * @format int32
   */
  frameCaptureHeight?: number;

  /**
   * frameCaptureWidth
   * @format int32
   */
  frameCaptureWidth?: number;

  /**
   * guid
   * @example 22595b2e-025d-49ca-a2b3-eb35e4b3d605
   */
  guid?: string;

  /** inputRotate */
  inputRotate?: string;

  /** isCustomTemplate */
  isCustomTemplate?: boolean;

  /** jobTemplate */
  jobTemplate?: string;

  /** mediaPackageResourceId */
  mediaPackageResourceId?: string;

  /** srcBucket */
  srcBucket?: string;

  /**
   * srcHeight
   * @format int32
   */
  srcHeight?: number;

  /** srcVideo */
  srcVideo?: string;

  /**
   * srcWidth
   * @format int32
   */
  srcWidth?: number;

  /** startTime */
  startTime?: string;

  /**
   * thumbNails
   * @example []
   */
  thumbNails?: string[];

  /**
   * thumbNailsUrls
   * @example []
   */
  thumbNailsUrls?: string[];

  /** workflowName */
  workflowName?: string;

  /** workflowStatus */
  workflowStatus?: string;

  /** workflowTrigger */
  workflowTrigger?: string;
}

export interface NotificationMessage {
  /** Json 형식의 스트링 */
  data?: string;
  external?: boolean;

  /**
   * Link URL
   * @example http://test.com
   */
  linkUrl?: string;

  /**
   * message 내용
   * @example 메시지 내용
   */
  message?: string;

  /**
   * message 아이디(메시지 DB 저장시 사용할 목적으로 만듦)
   * @example 1
   */
  messageId?: string;

  /**
   * message Type( N: 일반, L:링크)
   * @example N
   */
  messageType?: "N" | "L";
  newWindow?: boolean;

  /**
   * message Target(A: 전체에게 전송 , U: 개인에게 전송)
   * @example 1
   */
  targetType?: "A" | "U" | "START" | "STOP";

  /**
   * message 받을 계정 아이디
   * @example blocksmith
   */
  username?: string;
}

export interface ResponseVo {
  /** HOST URL */
  host?: string;

  /**
   * publish URL
   * @example /pub/s-notification/abc
   */
  pubUrl?: string;
  response?: string;

  /**
   * subscribe URL
   * @example /sub/s-notification/abc
   */
  subUrl?: string;

  /**
   * WebSocket URL
   * @example ws-stomp
   */
  wsUrl?: string;
}

/**
 * 미디어 컨버터 오류 유저 메타데이터
 */
export interface UserMetadata {
  /**
   * 고유식별자ID
   * @example 22595b2e-025d-49ca-a2b3-eb35e4b3d605
   */
  guid?: string;

  /**
   * 워크플로우명
   * @example cuspvideoservice
   */
  workflow?: string;
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "//dev1.blocksmith.xyz:9000";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
  }

  private addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  private addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  private mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : void 0,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Api Documentation
 * @version 1.0
 * @license Apache 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * @termsOfService urn:tos
 * @baseUrl //dev1.blocksmith.xyz:9000
 * @contact
 *
 * Api Documentation
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  aws = {
    /**
     * @description 워크플로우 정보를 기록하고 웹소켓에 결과를 전달 함
     *
     * @tags receive-aws-media-workflow-controller
     * @name MediaUsingPost1
     * @summary AWS 미디어워크플로우 결과 수신
     * @request POST:/external/aws/workflow/medias
     */
    mediaUsingPost1: (mediaConvertResult: MediaConvertResult, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/external/aws/workflow/medias`,
        method: "POST",
        body: mediaConvertResult,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 워크플로우 결과 정보를 기록하고 웹소켓에 결과를 전달 함
     *
     * @tags receive-aws-media-workflow-controller
     * @name MediaUsingPost
     * @summary AWS 미디어워크플로우 오류 결과 수신
     * @request POST:/external/aws/workflow/medias/{guid}/error
     */
    mediaUsingPost: (guid: string, mediaConvertErrorResult: MediaConvertErrorResult, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/external/aws/workflow/medias/${guid}/error`,
        method: "POST",
        body: mediaConvertErrorResult,
        type: ContentType.Json,
        ...params,
      }),
  };
  kafka = {
    /**
     * @description kafka를통한 알림 메세지 전송
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name InitUsingPost1
     * @summary kafka 알림 메세지 전송
     * @request POST:/s-notification/kafka/notification
     */
    initUsingPost1: (body: NotificationMessage, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/s-notification/kafka/notification`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description kafka를통한 알림 메세지 전송
     *
     * @tags sample-controller
     * @name InitUsingPost
     * @summary kafka 알림 메세지 전송
     * @request POST:/sample/kafka/notification
     */
    initUsingPost: (body: NotificationMessage, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/sample/kafka/notification`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        ...params,
      }),
  };
  topic = {
    /**
     * @description WebSocket 비디오 시청 connect url 호출
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name GetConnectVideoStartUrlUsingGet
     * @summary WebSocket connect url 호출
     * @request GET:/s-notification/topic/create/video/{username}
     */
    getConnectVideoStartUrlUsingGet: (username: string, params: RequestParams = {}) =>
      this.request<ResponseVo, void>({
        path: `/s-notification/topic/create/video/${username}`,
        method: "GET",
        ...params,
      }),

    /**
     * @description WebSocket connect url 호출
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name GetConnectUrlUsingGet
     * @summary WebSocket connect url 호출
     * @request GET:/s-notification/topic/create/{accountId}
     */
    getConnectUrlUsingGet: (accountId: string, params: RequestParams = {}) =>
      this.request<ResponseVo, void>({
        path: `/s-notification/topic/create/${accountId}`,
        method: "GET",
        ...params,
      }),

    /**
     * @description WebSocket 비디오 시청 msg 전송
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name SendVideoStartMsgUsingPost
     * @summary WebSocket connect url 호출
     * @request POST:/s-notification/topic/msg/video/{username}
     */
    sendVideoStartMsgUsingPost: (username: string, body: NotificationMessage, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/s-notification/topic/msg/video/${username}`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        ...params,
      }),

    /**
     * @description 특정 Topic에 메시지 전송
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name SendMessageUsingPost
     * @summary 특정 Topic에 메시지 전송
     * @request POST:/s-notification/topic/{accountId}/msg
     */
    sendMessageUsingPost: (accountId: string, query: { message: string }, params: RequestParams = {}) =>
      this.request<ResponseVo, void>({
        path: `/s-notification/topic/${accountId}/msg`,
        method: "POST",
        query: query,
        type: ContentType.Json,
        ...params,
      }),
  };
  topics = {
    /**
     * @description WebSocket connect url 호출
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name MediaTopicsUsingGet
     * @summary WebSocket connect url 호출
     * @request GET:/s-workflow/topics/media/{contentCardNo}
     */
    mediaTopicsUsingGet: (contentCardNo: number, params: RequestParams = {}) =>
      this.request<ResponseVo, void>({
        path: `/s-workflow/topics/media/${contentCardNo}`,
        method: "GET",
        ...params,
      }),

    /**
     * @description /workflow/media Topic에 메시지 전송
     *
     * @tags [웹소켓] - 웹소켓 API
     * @name SendMessageUsingPost1
     * @summary /workflow/media Topic에 메시지 전송
     * @request POST:/s-workflow/topics/media/{contentCardNo}/msg
     */
    sendMessageUsingPost1: (contentCardNo: number, query: { message: string }, params: RequestParams = {}) =>
      this.request<ResponseVo, void>({
        path: `/s-workflow/topics/media/${contentCardNo}/msg`,
        method: "POST",
        query: query,
        type: ContentType.Json,
        ...params,
      }),
  };
  init = {
    /**
     * @description 진입 시 접속 URL 반환
     *
     * @tags sample-controller
     * @name InitUsingGet
     * @summary 진입 시 접속 URL 반환
     * @request GET:/sample/init/{accountId}
     */
    initUsingGet: (accountId: string, params: RequestParams = {}) =>
      this.request<InitResponse, void>({
        path: `/sample/init/${accountId}`,
        method: "GET",
        ...params,
      }),
  };
}
