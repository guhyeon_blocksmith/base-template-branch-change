import React, { FC } from 'react';
import { Menubar } from 'smith-ui-components/menubar';
import { TabMenu } from 'smith-ui-components/tabmenu';
import { MenuItemType, MenuType } from 'types/index';
import LayoutGNBOneDepthOnly from './LayoutGNBOneDepthOnly';

// import Link from 'next/link';

interface LayoutGNBProps {
  menuItems: MenuItemType[];
  menuType: MenuType;
}

/**
 * GNB (Global Navication Bar)
 * @param menuItems
 * @param menuType
 * @constructor
 */
const LayoutGNB: FC<LayoutGNBProps> = ({ menuItems, menuType }) => {
  const { gnb } = menuType;
  const menuItemsTab = menuItems.map((rowItems: MenuItemType) => {
    // eslint-disable-next-line no-prototype-builtins
    if (rowItems.hasOwnProperty('items')) {
      const menuItem = rowItems.items![0] as MenuItemType;
      const restUrl =
        menuItem.url !== undefined
          ? `${rowItems.url}/${menuItem.url}`
          : rowItems.url;
      return {
        label: rowItems.label,
        icon: rowItems.icon,
        url: restUrl,
        parentUrl: rowItems.url,
      };
    }
    return { label: rowItems.label, icon: rowItems.icon, url: rowItems.url };
  });

  if (gnb === 'bar') {
    return <Menubar model={menuItemsTab} style={{ marginLeft: '0px' }} />;
  } else if (gnb === 'tab') {
    return <TabMenu model={menuItemsTab} />;
  } else if (gnb === 'oneDepthOnly') {
    return <LayoutGNBOneDepthOnly model={menuItemsTab} />;
  }
  return null;
};

export default LayoutGNB;
