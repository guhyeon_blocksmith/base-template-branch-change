import React, { FC } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { MenuItemType } from 'types/index';
import { v4 } from 'uuid';

interface LayoutBreadCrumb {
  model: MenuItemType[];
}

const LayoutBreadCrumb: FC<LayoutBreadCrumb> = ({ model }) => {
  const router = useRouter();
  const pathSplit = router.pathname.split('/');
  const pathDepth = pathSplit.length;
  const currentCategory = model.findIndex(
    (row: MenuItemType) => row.url === pathSplit[1]
  );
  // console.log('pathDepth:', pathDepth);
  // console.log('currentCategory:', currentCategory);
  // console.log('modelCurrentCategory:', model[currentCategory]);
  // console.log('path:', router.pathname);

  const handleRouter = (url: string | undefined, i: number) => {
    if (pathDepth === i) {
      url && router.push(url).then();
    }
  };

  const onBreadDepth = (depthNo: number) => {
    const currentItem: MenuItemType[] | MenuItemType[][] | undefined =
      model[currentCategory].items;
    const findItem: number = currentItem!.findIndex(
      (row: any) => row.url === pathSplit[2]
    );
    if (depthNo === 0) {
      return {
        label: model[currentCategory].label,
        url: model[currentCategory].url,
      };
    } else {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const { url, label, items } = model[currentCategory].items[findItem];
      if (depthNo === 1) {
        return {
          label: label,
          url: url,
        };
      } else if (depthNo === 2) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (currentItem[findItem].items === undefined)
          return { url: '', label: '' };
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const findItem2: number = currentItem[findItem].items.findIndex(
          (row: MenuItemType) => row.url === pathSplit[3]
        );
        return {
          label: items[findItem2].label,
          url: items[findItem2].url,
        };
      } else if (depthNo === 3) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const findItem2: number = currentItem[findItem].items.findIndex(
          (row: MenuItemType) => row.url === pathSplit[3]
        );
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const findItem3: number = currentItem[findItem].items.findIndex(
          (row: MenuItemType) => row.url === pathSplit[4]
        );
        return {
          label: items[findItem2].items[findItem3].label,
          url: items[findItem2].items[findItem3].url,
        };
      }
    }
  };

  const onBreadCrumbRender = () => {
    return (
      <li key={v4()}>
        {[...Array(pathDepth - 1)].map((_e: any, i: number) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const { label, url } = onBreadDepth(i);
          return (
            <React.Fragment key={v4()}>
              <i className="pi pi-chevron-right" />
              <button onClick={() => handleRouter(url, i)}>{label}</button>
            </React.Fragment>
          );
        })}
      </li>
    );
  };

  return currentCategory > 0 ? (
    <div className="smithBreadCrumb">
      <ul>
        <li>
          <Link href="/">
            <a>
              <span className="p-menuitem-icon pi pi-home" />
            </a>
          </Link>
        </li>
        {onBreadCrumbRender()}
      </ul>
    </div>
  ) : null;
};

export default LayoutBreadCrumb;
