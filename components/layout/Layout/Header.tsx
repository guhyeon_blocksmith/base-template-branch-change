import { LayoutConfig } from 'types/index';
import React, { FC } from 'react';
import { layoutMenus } from 'config/layoutSetting';
import LayoutHeader from 'components/layout/LayoutHeader';

interface HeaderProps {
  config: LayoutConfig;
  onMenuButtonClick: () => void;
  useCustomHeader?: boolean;
  CustomHeader?: React.ElementType;
  userInfo?: any | undefined;
  /**
   * 마이페이지 메뉴 사용 여부
   */
  myPage?: boolean;
}

/**
 * Layout Header
 * @param config
 * @param onMenuButtonClick
 * @param useCustomHeader
 * @param CustomHeader
 * @constructor
 */
const Header: FC<HeaderProps> = ({
  config,
  onMenuButtonClick,
  useCustomHeader,
  CustomHeader,
  myPage,
  userInfo,
}) => {
  const { showHeader } = config;
  const { menuItems, myPageMenuItems } = layoutMenus;
  //
  // const onMenuButtonClick = () => {
  //   //blocked-scroll
  //   setLayoutMask(true);
  // };

  if (!showHeader) return null;

  if (useCustomHeader) {
    if (CustomHeader) {
      return <CustomHeader config={config} userInfo={userInfo} />;
    } else {
      console.error('useCustomHeader is true but there is no CustomHeader.');
      return null;
    }
  } else {
    return (
      <LayoutHeader
        config={config}
        onMenuButtonClick={onMenuButtonClick}
        menuItems={myPage ? myPageMenuItems : menuItems}
      />
    );
  }
};

const MemoizedHeader = React.memo(Header);

export default MemoizedHeader;
