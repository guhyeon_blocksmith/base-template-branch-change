import { LayoutConfig } from 'types/index';
import React, { ElementType, FC } from 'react';
import LayoutFooter from 'components/layout/LayoutFooter';

interface FooterProps {
  config: LayoutConfig;
  CustomFooter?: ElementType;
  useCustomFooter?: boolean;
}

/**
 * Layout Footer.
 * @param config
 * @param CustomFooter
 * @param useCustomFooter
 * @constructor
 */
const Footer: FC<FooterProps> = ({ config, CustomFooter, useCustomFooter }) => {
  const { showFooter } = config;

  if (!showFooter) return null;

  if (useCustomFooter) {
    if (CustomFooter) {
      return <CustomFooter />;
    } else {
      console.error('useCustomFooter is true but there is no CustomFooter.');
      return null;
    }
  } else {
    return <LayoutFooter config={config} />;
  }
};

const MemoizedFooter = React.memo(Footer);

export default MemoizedFooter;
