import React, { FC, ReactNode, useEffect, useState } from 'react';
import LayoutContents from '../LayoutContents';
import { CrumbType, LayoutConfig, LayoutCustom } from 'types';
import LayoutExtra from 'components/layout/LayoutExtra';
import {
  config as directConfig,
  custom as directCustom,
  layoutMenus,
} from 'config/layoutSetting';
import Header from 'components/layout/Layout/Header';
import Footer from 'components/layout/Layout/Footer';
import { TabMenuProps } from 'smith-ui-components/tabmenu';
import { useStore } from 'libs/hooks/useStore';
import { PrivateAdminApi, PrivateUserApi } from 'apis/index';
import { useRouter } from 'next/router';
import { openLayoutMode } from 'types/enum';
import { toJS } from 'mobx';
import LoadingModal from 'components/mobxCommon/LoadingModal';
import { AxiosResponse } from 'axios';
import { ManagerMe } from 'apis/ApiAdmin';
import { UserMe } from 'apis/ApiUser';

interface LayoutProps {
  children?: ReactNode;
  crumb?: CrumbType[];
  config?: LayoutConfig;
  custom?: LayoutCustom;
  tabMenuItems?: TabMenuProps;

  tabMenuCallback?(itemName: string): void;

  layoutAuth?: openLayoutMode;

  /**
   * 마이페이지 메뉴 사용 여부
   */
  myPage?: boolean;
}

export const fetchUserInfo = async () => {
  // 내 정보는 한 번만 가져와서 넣어줘도 상관없음. backend에서 api 호출 자체를 token으로 제어하기 때문
  // 처음 온 페이지에 로그인이 안되어있을 때 로그인 페이지로 이동
  let response: AxiosResponse<ManagerMe> | AxiosResponse<UserMe>;

  // 자기 정보 가져오기. private 에서만 작동
  if (process.env.NEXT_PUBLIC_SITE_TYPE === 'ADMIN') {
    response = await PrivateAdminApi.managers.managerMeUsingGet();
  } else {
    response = await PrivateUserApi.userme.userMeUsingGet();
  }

  const { code } = response.data.error!;
  if (code === '00') {
    return response.data;
  } else {
    return undefined;
  }
};

/**
 * Layout 컴포넌트. 커스텀여부에 따라 components/custom에 있는 컴포넌트들을 렌더링합니다.
 * @param children
 * @param config
 * @param custom
 * @param tabMenuItems
 * @param tabMenuCallback
 * @param myPage
 * @param layoutAuth
 * @constructor
 */
const Layout: FC<LayoutProps> = ({
  children,
  config = directConfig,
  custom = directCustom,
  tabMenuItems,
  tabMenuCallback,
  myPage,
  layoutAuth = openLayoutMode.private,
}) => {
  const [layoutMask, setLayoutMask] = useState<boolean>(false);
  const { showMenu, env } = config;
  const { isCustomLayout, useThemeSelector } = env;
  const { menuItemsTree, menuItems, myPageMenuItems } = layoutMenus;
  const { commonStore: commonStore } = useStore();
  const router = useRouter();
  const { pathname } = router;
  const loginPath = '/auth/login';
  const [showLayout, setShowLayout] = useState<boolean>(false);
  const [userInfo, setUserInfo] = useState<any>();

  useEffect(() => {
    // 로그인 필요없는 경우 layout을 보여줍니다.
    if (
      process.env.NEXT_PUBLIC_LOGIN_FREE === 'true' ||
      layoutAuth === openLayoutMode.public
    ) {
      setShowLayout(true);
      return;
    }

    const privateLayoutProcess = async () => {
      // 내 정보는 한 번만 가져와서 넣어줘도 상관없음. backend에서 api 호출 자체를 token으로 제어하기 때문
      // 처음 온 페이지에 로그인이 안되어있을 때 로그인 페이지로 이동

      const userInfo = await fetchUserInfo();
      if (userInfo) {
        commonStore.userInfo = userInfo;
        setUserInfo(toJS(commonStore.userInfo));
        setShowLayout(true);
        return;
      } else {
        commonStore.userInfo = undefined;

        if (layoutAuth === openLayoutMode.private) {
          // 로그인 토큰 인증 실패
          if (router.pathname !== loginPath) {
            await router.push({
              pathname: loginPath,
              query: { returnUrl: router.pathname },
            });
            return;
          }
        }
      }
    };

    privateLayoutProcess().then();
  }, [commonStore, pathname, layoutAuth, router]);

  const {
    CustomHeader,
    CustomContent,
    CustomFooter,
    useCustomHeader,
    useCustomContent,
    useCustomFooter,
    className,
  } = custom;

  const renderContents = () => {
    if (useCustomContent) {
      if (CustomContent) {
        return <CustomContent />;
      } else {
        throw 'useCustomContent is true but there is no CustomContent.';
      }
    } else if (showMenu) {
      return (
        <LayoutContents
          config={config}
          layoutMask={layoutMask}
          menuItemsTree={menuItemsTree}
          menuItems={menuItems}
          tabMenuItems={tabMenuItems}
          tabMenuCallback={tabMenuCallback}
          myPageMenuItems={myPageMenuItems}
          myPage={myPage}
        >
          {children}
        </LayoutContents>
      );
    } else {
      return children;
    }
  };

  return showLayout ? (
    <div className={className}>
      <Header
        config={config}
        onMenuButtonClick={() => setLayoutMask(true)}
        useCustomHeader={useCustomHeader}
        CustomHeader={CustomHeader}
        myPage={myPage}
        userInfo={userInfo}
      />
      {renderContents()}
      <Footer
        config={config}
        CustomFooter={CustomFooter}
        useCustomFooter={useCustomFooter}
      />
      {isCustomLayout === 'false' && useThemeSelector === 'true' && (
        <LayoutExtra
          layoutMask={layoutMask}
          onLayoutMaskClick={() => {
            setLayoutMask(false);
          }}
          showSelector={useThemeSelector === 'true'}
        />
      )}
      <LoadingModal />
    </div>
  ) : null;
};

const MemoizedLayout = React.memo(Layout);

export default MemoizedLayout;
