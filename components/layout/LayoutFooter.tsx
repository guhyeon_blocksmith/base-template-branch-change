import React, { FC } from 'react';
import LayoutMultilingual from 'components/layout/LayoutMultilingual';
import { LayoutConfig } from 'types';
// import { version } from '../../package.json';

interface LayoutFooterProps {
  config: LayoutConfig;
}
/**
 * FNB (Foot Navigation Bar)
 * @param config
 * @constructor
 */
const LayoutFooter: FC<LayoutFooterProps> = ({ config }) => {
  const { showMultilingual, multilingual } = config;
  return (
    <footer>
      <hr />
      <div className="layout-footer">
        {/*<div className="layout-footer-left" style={{ marginLeft: '250px' }}>*/}
        <div className="layout-footer-left">
          {config.footerTextPosition === 'LEFT' && (
            <span>{config.footerText}</span>
          )}
        </div>
        <div className="layout-footer-right">
          {config.footerTextPosition === 'RIGHT' && (
            <span>{config.footerText}</span>
          )}
          {showMultilingual && (
            <LayoutMultilingual multilingual={multilingual.list} />
          )}
        </div>
      </div>
    </footer>
  );
};

export default LayoutFooter;
