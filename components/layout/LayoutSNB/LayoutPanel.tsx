import React, { FC } from 'react';
import { MenuItemType, MyPageMenuItemType } from 'types/index';
import { PanelMenu } from 'smith-ui-components/panelmenu';
import linkNext from 'components/common/link';
import { useRouter } from 'next/router';
import useMobileDetect from 'libs/hooks/useMobileDetect';

interface TreeProps {
  menuItems: MenuItemType[];
  /**
   * 마이페이지 메뉴 사용 여부
   */
  myPage?: boolean;

  /**
   * 마이페이지 메뉴 아이템
   */
  myPageMenuItems?: MyPageMenuItemType[];
}

const LayoutPanel: FC<TreeProps> = ({ menuItems, myPage, myPageMenuItems }) => {
  const router = useRouter();
  const detector = useMobileDetect();

  return (
    <>
      <PanelMenu
        model={myPage ? myPageMenuItems : menuItems}
        style={{ width: '100%' }}
        link={linkNext}
        path={router.pathname}
        expandAll={detector}
        showOnlyCurrentCategory={detector}
        id={'smithLayoutPanelMenu'}
      />
    </>
  );
};

export default LayoutPanel;
