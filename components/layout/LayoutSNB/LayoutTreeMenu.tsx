import React, { FC, useState } from 'react';
import { InputText } from 'smith-ui-components/inputtext';
import CategoryItem from 'components/layout/LayoutSNB/CategoryItem';
import { SNBMenuItemType } from 'types/index';

// import { CSSTransition } from 'react-transition-group';

// SNB(SIde Navigation Bar)
interface Props {
  menuList?: SNBMenuItemType[];
  showMenuSearch?: boolean;
}

const LayoutTreeMenu: FC<Props> = ({ menuList, showMenuSearch = true }) => {
  const [filteredMenu, setFilteredMenu] = useState<SNBMenuItemType[]>(
    menuList || []
  );

  //
  // const isSubmenuActive = (name: string) => {
  //   if (activeSubmenus) {
  //     return activeSubmenus[name];
  //   }
  //   return false;
  // };

  const findFilteredItems = (item: SNBMenuItemType, searchVal: string) => {
    if (item) {
      let matched = false;
      if (item.children) {
        const childItems = [...item.children];
        item.children = [];
        for (const childItem of childItems) {
          const copyChildItem = { ...childItem };
          if (isFilterMatched(copyChildItem, searchVal)) {
            matched = true;
            item.children.push(copyChildItem);
            item.expanded = true;
          }
        }
      }

      if (matched) {
        return true;
      }
    }
  };

  const isFilterMatched = (item: SNBMenuItemType, searchVal: string) => {
    let matched = false;
    if (
      item.name.toLowerCase().indexOf(searchVal) > -1 ||
      onFilterOnMeta(item, searchVal)
    ) {
      matched = true;
    }

    if (!matched || !(item.children && item.children.length)) {
      matched = findFilteredItems(item, searchVal) || matched;
    }

    return matched;
  };

  const onFilterOnMeta = (item: SNBMenuItemType, searchVal: string) => {
    if (item && item.meta) {
      return (
        item.meta.filter((meta) => meta.toLowerCase().indexOf(searchVal) > -1)
          .length > 0
      );
    }

    return false;
  };

  const handleSearchInputChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const { value } = event.target;

    if (!menuList) {
      setFilteredMenu([]);
    } else if (!value) {
      setFilteredMenu(menuList);
    } else if (menuList) {
      const searchVal = value && value.toLowerCase();
      const filteredMenu = [];
      for (const item of menuList) {
        const copyItem = { ...item };
        if (
          findFilteredItems(copyItem, searchVal) ||
          isFilterMatched(copyItem, searchVal)
        ) {
          filteredMenu.push(copyItem);
        }
      }
      setFilteredMenu(filteredMenu);
    }
  };

  return (
    <>
      {showMenuSearch && (
        <span className="layout-sidebar-filter p-input-icon-left p-fluid p-my-2">
          <i className="pi pi-search" />
          <InputText
            type="text"
            onChange={handleSearchInputChange}
            placeholder="Search..."
            aria-label="Search input"
            autoComplete="off"
          />
        </span>
      )}

      <div className="layout-menu" role="menubar">
        <>
          {filteredMenu &&
            filteredMenu.map((menuitem: SNBMenuItemType, index: number) => {
              const { name, children } = menuitem;
              return (
                <CategoryItem
                  key={index}
                  menuName={name}
                  menuItems={children}
                />
              );
            })}
        </>
      </div>
    </>
  );
};

const MemoizedLayoutTreeMenu = React.memo(LayoutTreeMenu);

export default MemoizedLayoutTreeMenu;
