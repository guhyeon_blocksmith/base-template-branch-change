import classnames from 'classnames';
import React, { FC } from 'react';
import { SNBMenuItemType } from 'types/index';
import { useRouter } from 'next/router';

interface MenuLinkProps {
  item: SNBMenuItemType;
}

const MenuLink: FC<MenuLinkProps> = ({ item }) => {
  const { name, to, href, badge } = item;
  const router = useRouter();

  const handleRouter = (to: any) => {
    router.push(to).then();
  };

  const content = (
    <>
      {name}
      {badge && (
        <span
          className={classnames(
            'layout-menu-badge p-tag p-tag-rounded p-ml-2 p-text-uppercase',
            {
              [`${badge}`]: true,
              'p-tag-success': badge === 'new',
              'p-tag-info': badge === 'updated',
            }
          )}
        >
          {badge}
        </span>
      )}
    </>
  );

  if (href === undefined) {
    return (
      <button
        style={{ border: '0', background: 'none' }}
        onClick={() => handleRouter(to)}
      >
        {content}
      </button>
    );
  } else {
    return (
      <a href={href} role="menuitem" rel="noopener noreferrer" target="_blank">
        {content}
      </a>
    );
  }
};

const MemoizedMenuLink = React.memo(MenuLink);

export default MemoizedMenuLink;
