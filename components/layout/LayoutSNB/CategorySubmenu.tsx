import React, { FC } from 'react';
import MenuLink from 'components/layout/LayoutSNB/MenuLink';
import { SNBMenuItemType } from 'types/index';

interface CategorySubmenuProps {
  item: SNBMenuItemType;
}

/**
 * 카테고리 아이템 서브메뉴
 * @param item
 * @constructor
 */
const CategorySubmenu: FC<CategorySubmenuProps> = ({ item }) => {
  if (item.children === undefined || item.children.length === 0) return null;
  return (
    <div className="p-toggleable-content">
      <div className="p-toggleable-content">
        <ul role="menu">
          {item.children.map((menuItem: SNBMenuItemType, index: number) => {
            return (
              <li role="menuitem" key={`${index}`}>
                <MenuLink item={menuItem} />
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

const MemoizedCategorySubmenu = React.memo(CategorySubmenu);

export default MemoizedCategorySubmenu;
