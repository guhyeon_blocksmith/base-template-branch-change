import React, { FC } from 'react';
import MenuLink from 'components/layout/LayoutSNB/MenuLink';
import CategorySubmenu from 'components/layout/LayoutSNB/CategorySubmenu';
import { SNBMenuItemType } from 'types/index';

interface CategoryItemProps {
  menuName: string;
  menuItems?: SNBMenuItemType[];
}

/**
 * SNB의 카테고리 아이템
 * @param menuName
 * @param menuItems
 * @constructor
 */
const CategoryItem: FC<CategoryItemProps> = ({ menuName, menuItems }) => {
  return (
    <>
      <div className="menu-category">{menuName}</div>
      {menuItems && (
        <div className="menu-items">
          {menuItems.map((item: SNBMenuItemType, index: number) => {
            return (
              <React.Fragment key={index}>
                <MenuLink item={item} />
                <CategorySubmenu item={item} />
              </React.Fragment>
            );
          })}
        </div>
      )}
    </>
  );
};

const MemoizedCategoryItem = React.memo(CategoryItem);

export default MemoizedCategoryItem;
