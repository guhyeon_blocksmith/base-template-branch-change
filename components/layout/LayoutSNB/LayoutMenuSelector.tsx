import React, { FC } from 'react';
import {
  MenuItemType,
  LayoutConfig,
  SNBMenuItemType,
  MyPageMenuItemType,
} from 'types/index';
import LayoutTree from 'components/layout/LayoutSNB/LayoutTree';
import LayoutPanel from 'components/layout/LayoutSNB/LayoutPanel';
import Image from 'next/image';
import Link from 'next/link';
// import UserCard from 'components/usercard';

interface TreeProps {
  layoutMask: boolean;
  config: LayoutConfig;
  menuItemsTree: SNBMenuItemType[];
  menuItems: MenuItemType[];

  /**
   * 마이페이지 메뉴 사용 여부
   */
  myPage?: boolean;

  /**
   * 마이페이지 메뉴 아이템
   */
  myPageMenuItems: MyPageMenuItemType[];
}

const LayoutMenuSelector: FC<TreeProps> = ({
  config,
  menuItemsTree,
  menuItems,
  myPageMenuItems,
  myPage,
}) => {
  const { header, layout } = config;

  const rendererMenu = () => {
    if (config.menuType.snb === 'tree') {
      return <LayoutTree menuItemsTree={menuItemsTree} config={config} />;
    } else if (config.menuType.snb === 'panel') {
      return <LayoutPanel menuItems={myPage ? myPageMenuItems : menuItems} />;
    }
  };

  const rendererLogo = () => {
    if (header?.logo === undefined) {
      return (
        <Image src="/images/logo.png" alt="logo" width="180" height="40" />
      );
    } else {
      const { logo } = header;
      return (
        <Image
          src={logo?.src}
          alt={logo?.alt}
          width={logo?.width}
          height={logo?.height}
        />
      );
    }
  };

  return (
    <>
      {layout.logoPosition === 'snb' && (
        <Link href="/">
          <span style={{ cursor: 'pointer' }}>{rendererLogo()}</span>
        </Link>
      )}

      {/*<UserCard />*/}
      {rendererMenu()}
    </>
  );
};

export default LayoutMenuSelector;
