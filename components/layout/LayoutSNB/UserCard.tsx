import React, { FC } from 'react';
import styles from './UserCard.module.css';
import Image from 'next/image';

const UserCard: FC = () => {
  return (
    <div className={styles.userCard}>
      <div className={styles.pic}>
        <Image
          src="/images/no-image.png"
          width={'210'}
          height={'210'}
          alt="user picture"
        />
      </div>
      <p className={styles.hello}>
        <strong>admin</strong>님 <br /> 안녕하세요!
      </p>
      <div className={styles.botBtn}>
        <button>
          <i className="pi pi-user" />
        </button>
        <button>
          <i className="pi pi-sign-out" />
        </button>
      </div>
    </div>
  );
};

export default UserCard;
