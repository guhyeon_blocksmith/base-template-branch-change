import React, { FC, useCallback, useEffect, useState } from 'react';
import { LayoutConfig, SNBMenuItemType } from 'types/index';
import Axios from 'axios';
import LayoutTreeMenu from 'components/layout/LayoutSNB/LayoutTreeMenu';

interface TreeProps {
  config: LayoutConfig;
  menuItemsTree: SNBMenuItemType[];
}

const LayoutTree: FC<TreeProps> = ({ config, menuItemsTree }) => {
  const { showMenuSearch, env } = config;
  const { baseUrl, menuUrl, menuMode } = env;
  const [menuList, setMenuList] = useState<SNBMenuItemType[]>();
  const [isReady, setIsReady] = useState<boolean>(false);

  const getMenu = useCallback(() => {
    Axios.get(`${baseUrl}${menuUrl}`, {
      headers: { 'Cache-Control': 'no-cache' },
    }).then((res) => {
      setMenuList(res.data);
      setIsReady(true);
    });
  }, [baseUrl, menuUrl]);

  useEffect(() => {
    if (menuMode === 'static') {
      setMenuList(menuItemsTree);
      setIsReady(true);
    } else {
      getMenu();
    }
  }, [menuItemsTree, getMenu, menuMode]);

  if (isReady) {
    return (
      <LayoutTreeMenu menuList={menuList} showMenuSearch={showMenuSearch} />
    );
  }
  return null;
};

export default LayoutTree;
