import React, { FC } from 'react';
import { useRouter } from 'next/router';
import { v4 } from 'uuid';

export interface MenuItem {
  label?: string;
  icon?: string;
  url?: string;
  items?: MenuItem[] | MenuItem[][];
  disabled?: boolean;
  target?: string;
  separator?: boolean;
  style?: any;
  className?: string;

  command?(e: { originalEvent: Event; item: MenuItem }): void;

  path?: string;
  parentUrl?: string;
}

interface MenubarProps {
  model: MenuItem[];
}

const LayoutGNBOneDepthOnly: FC<MenubarProps> = ({ model }) => {
  const router = useRouter();
  const pathDepth = router.pathname.split('/');
  const handleRouter = (url: string | undefined, idx: number) => {
    // console.log('url:', url);
    // console.log('handleRouter:', typeof url);
    // console.log('handle idx:', idx);

    if (url && url.indexOf('http') > -1) {
      window.location.assign(url);
      return;
    }

    if (idx === 0 && url === '') {
      router.push('/').then();
    } else {
      if (url === '/') {
        url && router.push(url).then();
      } else {
        if (url) {
          if (url.charAt(0) === '/') {
            router.push(url).then();
          } else {
            router.push(`/${url}`).then();
          }
        }
      }
    }
  };

  return (
    <div className="customGnb">
      <ul role="menubar">
        {model.map((rowItem: MenuItem, idx: number) => {
          const { label, url, parentUrl } = rowItem;

          let isActive = false;
          if (pathDepth[1] === parentUrl) {
            isActive = true;
          }

          if (parentUrl === undefined && pathDepth[1] === url) {
            isActive = true;
          }

          if (parentUrl === undefined && url === '/' && pathDepth[1] === '') {
            isActive = true;
          }

          return (
            <li role="none" key={v4()}>
              <button
                role="menuitem"
                className={`p-menuitem-link ${isActive ? 'current' : ''}`}
                aria-haspopup="false"
                onClick={() => {
                  handleRouter(url, idx);
                  // console.log('url:', url);
                }}
                key={v4()}
              >
                <span className="p-menuitem-text" key={v4()}>
                  {label}
                </span>
              </button>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default LayoutGNBOneDepthOnly;
