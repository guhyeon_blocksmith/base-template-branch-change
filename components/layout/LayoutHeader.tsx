import React, { FC } from 'react';
import LayoutGNB from './LayoutGNB';
import Image from 'next/image';
import Link from 'next/link';
import MetaHeader from 'components/common/MetaHeader';
import { LayoutConfig, MenuItemType } from 'types';
import LayoutHeaderUser from './LayoutHeaderUser';

interface LayoutHeaderProps {
  config: LayoutConfig;
  onMenuButtonClick: any;
  menuItems: MenuItemType[];
}

/**
 * Layout Header
 * @param config
 * @param onMenuButtonClick
 * @param menuItems
 * @constructor
 */
const LayoutHeader: FC<LayoutHeaderProps> = ({
  config,
  onMenuButtonClick,
  menuItems,
}) => {
  const { header, layout, menuType } = config;
  const rendererLogo = () => {
    if (header?.logo === undefined) {
      return (
        <Image src="/images/logo.png" alt="logo" width="180" height="40" />
      );
    } else {
      const { logo } = header;
      return (
        <Image
          src={logo?.src}
          alt={logo?.alt}
          width={logo?.width}
          height={logo?.height}
        />
      );
    }
  };

  const handleMenuClick = () => {
    if (onMenuButtonClick) {
      onMenuButtonClick(true);
    }
  };
  return (
    <>
      <MetaHeader config={config} />
      <header>
        <div className="layout-topbar">
          <button
            type="button"
            className="p-link menu-button"
            aria-haspopup
            aria-label="Menu"
            onClick={handleMenuClick}
          >
            <i className="pi pi-bars" />
          </button>
          {layout.logoPosition === 'gnb' && (
            <Link href="/">
              <span style={{ cursor: 'pointer' }}>{rendererLogo()}</span>
            </Link>
          )}

          <ul className="topbar-menu p-unselectable-text" role="menubar">
            <LayoutGNB menuItems={menuItems} menuType={menuType} />
          </ul>

          <LayoutHeaderUser />
        </div>
      </header>
    </>
  );
};

const MemoizedLayoutHeader = React.memo(LayoutHeader);

export default MemoizedLayoutHeader;
