import React, { FC } from 'react';
import { Dropdown } from 'smith-ui-components/dropdown';
import useTheme, { themeNames } from 'libs/hooks/useTheme';
import themeLayer from './LayoutExtra.module.css';
import { useState } from 'react';
import Image from 'next/image';

interface LayoutExtraProps {
  layoutMask: boolean;
  onLayoutMaskClick: () => void;
  showSelector: boolean;
}

/**
 * 레이아웃 Footer 추가 컴포넌트
 * @param layoutMask
 * @param onLayoutMaskClick
 * @constructor
 */
const LayoutExtra: FC<LayoutExtraProps> = ({
  layoutMask,
  onLayoutMaskClick,
  showSelector = true,
}) => {
  const { selectedTheme, setSelectedTheme } = useTheme();

  const [themeLayerState, setThemeLayerState] = useState(false);
  const themeLayerTrigger = () => setThemeLayerState(!themeLayerState);

  return (
    <div
      className={
        themeLayerState === true
          ? `${themeLayer.on} ${themeLayer.themeLayer}`
          : themeLayer.themeLayer
      }
      style={{ textAlign: 'right', marginRight: '0' }}
    >
      <h1>
        <Image
          width={'150px'}
          height={'22px'}
          src="/sample/components/logoBlocksmithBlack.png"
          alt="blocksmith"
        />
      </h1>
      {showSelector && (
        <button className={themeLayer.trigger} onClick={themeLayerTrigger}>
          <i className="pi pi-times"></i>
        </button>
      )}
      <Dropdown
        id={'dropdown_theme'}
        value={selectedTheme}
        options={themeNames}
        onChange={(e) => {
          setSelectedTheme(e.value.code);
        }}
        optionLabel="name"
        placeholder="테마를 선택하세요"
      />
      <div
        className={`layout-mask layout-mask-${layoutMask ? 'active' : ''}`}
        onClick={onLayoutMaskClick}
      />
    </div>
  );
};
const MemoizedLayoutExtra = React.memo(LayoutExtra);

export default MemoizedLayoutExtra;
