import React, { FC, ReactNode, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  MenuItemType,
  LayoutConfig,
  SNBMenuItemType,
  MyPageMenuItemType,
} from 'types';
import LayoutMenuSelector from './LayoutSNB';
import { TabMenu, TabMenuProps } from 'smith-ui-components/tabmenu';
import LayoutBreadCrumb from './LayoutBreadCrumb';

interface LayoutContentsProps {
  config: LayoutConfig;
  children?: ReactNode;
  layoutMask: boolean;
  menuItemsTree: SNBMenuItemType[];
  menuItems: MenuItemType[];
  tabMenuItems?: TabMenuProps;
  tabMenuCallback?(itemName: string): void;

  /**
   * 마이페이지 메뉴 사용 여부
   */
  myPage?: boolean;

  /**
   * 마이페이지 메뉴 아이템
   */
  myPageMenuItems: MyPageMenuItemType[];
}

/**
 * 레이아웃 내용
 * @param config
 * @param children
 * @param layoutMask
 * @param menuItemsTree
 * @param menuItems
 * @param tabMenuItems
 * @param tabMenuCallback
 * @constructor
 */
const LayoutContents: FC<LayoutContentsProps> = ({
  config,
  children,
  layoutMask,
  menuItemsTree,
  menuItems,
  tabMenuItems,
  tabMenuCallback,
  myPageMenuItems,
  myPage,
}) => {
  const { showBreadCrumb } = config;
  const router = useRouter();
  const asPath: string = router.asPath;
  const isHashUrl: boolean =
    tabMenuItems?.model !== undefined && asPath.indexOf('#') !== -1;
  const [currentTab, setCurrentTab] = useState<number>(-1);
  const currentTabItems =
    tabMenuItems?.model !== undefined
      ? Number(
          tabMenuItems?.model!.findIndex(
            (row) => row.label === asPath.split('#')[1]
          )
        )
      : null;

  useEffect(() => {
    if (tabMenuItems?.model !== undefined && currentTabItems !== null) {
      setCurrentTab(currentTabItems);
    }
    if (tabMenuCallback && isHashUrl && currentTabItems !== null) {
      tabMenuCallback(tabMenuItems!.model![currentTabItems]!.label!);
    }
  }, [currentTabItems]);

  const cloneTabMenuItems = {
    ...tabMenuItems,
    activeItem:
      tabMenuItems?.activeItem !== undefined
        ? tabMenuItems?.activeItem
        : tabMenuItems?.model !== undefined && tabMenuItems?.model![currentTab],
    onTabChange: (item: any) => {
      if (tabMenuItems?.model !== undefined && currentTabItems !== null) {
        setCurrentTab(
          Number(
            tabMenuItems?.model!.findIndex(
              (row) => row.label === item.value.label
            )
          )
        );
      }
      router
        .push(`${router.pathname}#${item.value.label}`, undefined, {
          shallow: true,
        })
        .then();
    },
  };

  return (
    <>
      <div
        className={`layout-sidebar ${layoutMask ? 'active' : ''}`}
        style={{ width: '250px' }}
      >
        <LayoutMenuSelector
          layoutMask={layoutMask}
          config={config}
          menuItemsTree={menuItemsTree}
          menuItems={menuItems}
          myPageMenuItems={myPageMenuItems}
          myPage={myPage}
        />
      </div>
      <div className="layout-content">
        {showBreadCrumb && <LayoutBreadCrumb model={menuItems} />}
        {cloneTabMenuItems?.model && (
          <div className="tabType01">
            <TabMenu
              id={cloneTabMenuItems?.id}
              model={cloneTabMenuItems?.model}
              activeItem={cloneTabMenuItems?.activeItem}
              style={cloneTabMenuItems?.style}
              className={cloneTabMenuItems?.className}
              onTabChange={cloneTabMenuItems?.onTabChange}
            />
          </div>
        )}
        <div className={'content-section'}>{children}</div>
      </div>
    </>
  );
};

const MemoizedLayoutContents = React.memo(LayoutContents);

export default MemoizedLayoutContents;
