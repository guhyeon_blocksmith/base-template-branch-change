import React, { FC, useState } from 'react';
import { Dropdown } from 'smith-ui-components/dropdown';
import { Regions } from 'types/index';
import useTranslation from 'libs/hooks/useTranslation';

interface LayoutMultilingualProps {
  multilingual: Regions[];
}

/**
 * 다국어 선택 컴포넌트
 * @param multilingual
 * @constructor
 */
const LayoutMultilingual: FC<LayoutMultilingualProps> = ({ multilingual }) => {
  const { setLocale } = useTranslation();

  const [selectedCountry, setSelectedCountry] = useState<Regions>();

  const handleCountryChange = (e: {
    originalEvent: Event;
    value: any;
    target: { name: string; id: string; value: any };
  }) => {
    setSelectedCountry(e.value);
    setLocale(e.value.code);
  };

  return (
    <Dropdown
      id={'dropdown_language'}
      value={selectedCountry}
      options={multilingual}
      optionValue="code"
      optionLabel="name"
      onChange={handleCountryChange}
      placeholder="언어선택"
    />
  );
};

const MemoizedLayoutMultilingual = React.memo(LayoutMultilingual);

export default MemoizedLayoutMultilingual;
