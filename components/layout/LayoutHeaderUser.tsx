import React, { FC, useRef } from 'react';
import userCss from './LayoutHeaderUser.module.css';
import { useRouter } from 'next/router';
import Link from 'next/link';

/**
 * Layout Header User. 사용자 컴포넌트
 * @constructor
 */
const LayoutHeaderUser: FC = () => {
  const router = useRouter();
  // const [how, setHow] = useState(false);
  // const clicked = () => setHow(!how);

  // function useOutsideAlerter(ref: any) {
  //   useEffect(() => {
  //     /**
  //      * Alert if clicked on outside of element
  //      */
  //     function handleClickOutside(event: Event) {
  //       if (ref.current && !ref.current.contains(event.target)) {
  //         setHow(false);
  //       }
  //     }
  //
  //     // Bind the event listener
  //     document.addEventListener('mousedown', handleClickOutside);
  //     return () => {
  //       // Unbind the event listener on clean up
  //       document.removeEventListener('mousedown', handleClickOutside);
  //     };
  //   }, [ref]);
  // }

  const wrapperRef = useRef(null);
  // useOutsideAlerter(wrapperRef);

  return (
    <div className={userCss.rightSet} ref={wrapperRef}>
      <div className={userCss.userUtil}>
        <Link href="/my-page/staff-info">
          <button className={userCss.userPic} /*onClick={clicked}*/></button>
        </Link>
        {/*<div className={how === true ? userCss.on : ''}>*/}
        {/*  <ul>*/}
        {/*    <li>*/}
        {/*      <Link href="/my-page/staff-info">*/}
        {/*        <button>*/}
        {/*          <i className="pi pi-user"></i> 마이페이지*/}
        {/*        </button>*/}
        {/*      </Link>*/}
        {/*    </li>*/}
        {/*    <li className={userCss.logOut}>*/}
        {/*      <button*/}
        {/*        onClick={async () => {*/}
        {/*          await router.push('/sample/auth/logout');*/}
        {/*        }}*/}
        {/*      >*/}
        {/*        <i className="pi pi-sign-out"></i> 로그아웃*/}
        {/*      </button>*/}
        {/*    </li>*/}
        {/*  </ul>*/}
        {/*</div>*/}
      </div>
      <button
        className={userCss.signOut}
        onClick={async () => {
          await router.push('/auth/logout');
        }}
      >
        <img src="/images/sign_out.svg" alt="" />
      </button>
    </div>
  );
};

export default LayoutHeaderUser;
