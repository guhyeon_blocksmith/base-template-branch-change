import React, { FC } from 'react';
import styles from './ApprovalStatusTable.module.css';
import GridTable from 'components/common/GridTable';
import GridTableItem from 'components/common/GridTableItem';

/**
 * 처리상태 표.
 */
export interface ApprovalStatusTableProps {
  /**
   * 신청상태
   */
  status: string;

  /**
   * 처리일
   */
  date: string;

  /**
   * 담당자
   */
  inCharge: string;
}

/**
 * 신청 처리 상태 표
 * @constructor
 */
export const ApprovalStatusTable: FC<ApprovalStatusTableProps> = ({
  status,
  date,
  inCharge,
}) => {
  return (
    <div className={styles.wrapper}>
      <GridTable className={styles.smithGridTable}>
        <GridTableItem title={'신청상태'} className={'p-col-12'}>
          {status}
        </GridTableItem>
        <GridTableItem title={'처리일'} className={'p-col-12'}>
          {date}
        </GridTableItem>
        <GridTableItem title={'담당자'} className={'p-col-12'}>
          {inCharge}
        </GridTableItem>
      </GridTable>
    </div>
  );
};
