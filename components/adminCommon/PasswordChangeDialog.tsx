import React, { FC, useEffect } from 'react';
import { Dialog } from 'smith-ui-components/dialog';
import { Button } from 'smith-ui-components/button';
import { observer } from 'mobx-react';
import { Password } from 'smith-ui-components/password';
import styles from './PasswordChangeDialog.module.css';
import useAppendRef from 'libs/hooks/useAppendRef';
import { useStore } from 'libs/hooks/useStore';

/**
 * 비밀번호 변경 다이얼로그
 */
export const PasswordChangeDialog: FC = observer(() => {
  const { passwordChangeDialogStore: store } = useStore();
  const [currentPasswordRef] = useAppendRef(store.inputRefs, 'currentPassword');
  const [newPasswordRef] = useAppendRef(store.inputRefs, 'newPassword');
  const [newConfirmPasswordRef] = useAppendRef(
    store.inputRefs,
    'newConfirmPassword'
  );

  useEffect(() => {
    store.init();
  }, [store]);

  return (
    <Dialog
      visible={store.visible}
      className={styles.wrapper}
      modal
      onHide={store.handleCancel}
      header="비밀번호 변경"
      footer={
        <>
          <Button
            label="취소"
            onClick={store.handleCancel}
            className="p-button-outlined"
          />
          <Button
            label="변경하기"
            disabled={store.disabledOkButton}
            onClick={store.handleOK}
            className="p-button-primary"
          />
        </>
      }
    >
      <div className="p-fluid">
        <div className="p-field p-grid">
          <div className={`p-col-12 ${styles.info}`}>
            <span>변경할 비밀번호를 입력해주세요.</span>
          </div>
          <label htmlFor="firstname4" className="p-col-12 p-md-3">
            현재 비밀번호
          </label>
          <div className="p-col-12 p-md-9">
            <Password
              ref={currentPasswordRef}
              className={`${
                store.validErrors.has('currentPassword') ? 'p-invalid' : ''
              }`}
              value={store.model.currentPassword}
              feedback={false}
              onChange={store.handleCurrentChange}
            />
            {store.validErrors
              .get('currentPassword')
              ?.map((message: string) => (
                <small key={message} className="p-invalid">
                  {message}
                </small>
              ))}
          </div>
        </div>
        <div className="p-field p-grid">
          <label htmlFor="lastname4" className="p-col-12 p-md-3">
            새 비밀번호
          </label>
          <div className="p-col-12 p-md-9">
            <Password
              ref={newPasswordRef}
              className={`${
                store.validErrors.has('newPassword') ? 'p-invalid' : ''
              }`}
              value={store.model.newPassword}
              feedback={false}
              onChange={store.handleNewChange}
            />
            {store.validErrors.get('newPassword')?.map((message: string) => (
              <small key={message} className="p-invalid">
                {message}
              </small>
            ))}
            {/*<small className={'p-invalid'}>*/}
            {/*  최소 8자 이상 영문 대소문자, 숫자 및 특수문자를 모두*/}
            {/*  포함해주세요*/}
            {/*  <br />*/}
            {/*  기존 비밀번호와 동일하게 설정할 수 없습니다.*/}
            {/*</small>*/}
          </div>
        </div>
        <div className="p-field p-grid">
          <label htmlFor="lastname4" className="p-col-12 p-md-3">
            새 비밀번호 확인
          </label>
          <div className="p-col-12 p-md-9">
            <Password
              ref={newConfirmPasswordRef}
              className={`${
                store.validErrors.has('newConfirmPassword') ? 'p-invalid' : ''
              }`}
              value={store.model.newConfirmPassword}
              feedback={false}
              onChange={store.handleNewConfirmChange}
            />
            {store.validErrors
              .get('newConfirmPassword')
              ?.map((message: string) => (
                <small key={message} className="p-invalid">
                  {message}
                </small>
              ))}
          </div>
        </div>
      </div>
    </Dialog>
  );
});
