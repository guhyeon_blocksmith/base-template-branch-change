import React, { FC, useMemo } from 'react';

/**
 * 관리자 테이블 푸터 Props
 */
export interface AdminDataTableFooterContainerProps {
  /**
   * align default : right
   */
  align?: 'left' | 'center' | 'right';
}

/**
 * 관리자 테이블 푸터
 * @constructor
 */
export const AdminDataTableFooterContainer: FC<AdminDataTableFooterContainerProps> = ({
  align = 'right',
  children,
}) => {
  const _align = useMemo(() => {
    switch (align) {
      case 'left':
        return 'taL';
      case 'center':
        return 'taC';
      case 'right':
        return 'taR';
    }
  }, []);

  return <div className={_align}>{children}</div>;
};
