import React, { FC, useEffect } from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'libs/hooks/useStore';
import { Button } from 'smith-ui-components/button';
import styles from './PlayerList.module.scss';
import { toJS } from 'mobx';
import { useRouter } from 'next/router';
import { VideoType } from 'stores/video/VideoListStore';
import { v4 } from 'uuid';

/**
 * Video List
 * @constructor
 * sample videos from https://www.videezy.com/
 */
interface PlayerListType {
  onUpdate: (obj?: any) => void | undefined;
  setTabName: (obj?: any) => void | undefined;
}

const PlayerList: FC<PlayerListType> = observer(({ onUpdate, setTabName }) => {
  const router = useRouter();
  const { videoListStore: store } = useStore();

  useEffect(() => {
    store.loadList('Complete').then();
  }, []);

  return (
    <React.Fragment>
      <table className="defTable">
        <colgroup>
          <col style={{ width: '20px' }} />
          <col style={{ width: '160px' }} />
          <col />
          <col style={{ width: 'calc(50% - 120px)' }} />
        </colgroup>
        <tbody>
          {!store.isLoading &&
            store.list &&
            store.list.map((rowItem: VideoType, idx: number) => {
              const {
                egressEndpoints,
                srcVideo,
                workflowStatus,
                thumbNailsUrls,
                guid,
              } = rowItem;

              return (
                <tr key={v4()}>
                  <th>{idx + 1}</th>
                  <th>
                    {thumbNailsUrls && (
                      <img
                        src={toJS(thumbNailsUrls![0])}
                        alt={srcVideo}
                        style={{ width: '150px', height: '84px' }}
                      />
                    )}
                  </th>
                  <td>
                    <p>{guid}</p>
                    <p>{workflowStatus}</p>
                  </td>
                  <td>
                    <Button
                      style={{
                        background: '#ffffff',
                        color: '#000000',
                        margin: '0px',
                        border: '0px solid #ffffff',
                        padding: '0',
                      }}
                      onClick={() => {
                        setTabName('MetaInfo');
                        router
                          .push(`${router.pathname}#MetaInfo`, undefined, {
                            shallow: true,
                          })
                          .then();
                        onUpdate({
                          url: egressEndpoints!.DASH,
                          playing: true,
                          metaInfo: toJS(rowItem),
                        });
                      }}
                    >
                      <span className={styles['product-category']}>SHOW</span>
                    </Button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </React.Fragment>
  );
});

export default PlayerList;
