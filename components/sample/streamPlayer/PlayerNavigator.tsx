import React, { FC, useState } from 'react';
import { observer } from 'mobx-react';
import { InputSwitch } from 'smith-ui-components/inputswitch';
import { Button } from 'smith-ui-components/button';
import { v4 as uuid4 } from 'uuid';

/**
 * Video Navigator
 * @constructor
 */
interface PlayerNavigatorType {
  config: any;
  onUpdate: (obj?: any) => void | undefined;
  setConfig: (obj?: any) => void;
  channels?: any;
  controls: boolean;
}
const PlayerNavigator: FC<PlayerNavigatorType> = observer(
  ({ config, onUpdate, controls, setConfig }) => {
    const [selectedButton, setSelectedButton] = useState<number>(0);
    const {
      url,
      pip,
      playing,
      volume,
      muted,
      played,
      loaded,
      loop,
      fullscreen,
      loadedSeconds,
      playedSeconds,
    } = config;

    const disabled = url === null;

    const renderButton = (
      state: any,
      label: string,
      disable: boolean = disabled,
      idx?: number
    ) => {
      return (
        <Button
          disabled={disable}
          onClick={() => {
            setConfig({ ...config, ...state });
            idx !== undefined && setSelectedButton(idx);
          }}
          style={{ cursor: 'pointer' }}
          className={`p-button-outlined ${
            Number(selectedButton) === Number(idx)
              ? 'p-button-danger '
              : disable
              ? 'p-button-help'
              : 'p-button-warning'
          }`}
          key={uuid4()}
        >
          {label}
        </Button>
      );
    };
    return (
      <>
        <table className="defTable">
          <colgroup>
            <col style={{ width: '180px' }} />
            <col style={{ width: 'calc(50% - 120px)' }} />
          </colgroup>
          <tbody>
            {/*<tr>*/}
            {/*  <th>Channel</th>*/}
            {/*  <td>*/}
            {/*    {channels.map((rowItems: any, idx: number) => {*/}
            {/*      return renderButton(*/}
            {/*        {*/}
            {/*          url: rowItems.egressEndpoints.DASH,*/}
            {/*        },*/}
            {/*        rowItems.title,*/}
            {/*        false,*/}
            {/*        idx*/}
            {/*      );*/}
            {/*    })}*/}
            {/*  </td>*/}
            {/*</tr>*/}
            <tr>
              <th>Controls</th>
              <td>
                {renderButton(
                  { muted: !muted },
                  `Mute(${muted ? 'on' : 'off'})`
                )}
                {renderButton({ url: null, playing: false }, 'Stop')}
                {renderButton(
                  { playing: !playing },
                  playing ? 'Pause' : 'Play'
                )}
                {renderButton({ fullscreen: !fullscreen }, 'FullScreen')}
                {renderButton(
                  { pip: !pip },
                  pip ? 'Disable PiP' : 'Enable PiP'
                )}
              </td>
            </tr>

            <tr>
              <th>Speed</th>
              <td>
                {renderButton({ playbackRate: 1.0 }, '1x')}
                {renderButton({ playbackRate: 1.5 }, '1.5x')}
                {renderButton({ playbackRate: 2.0 }, '2x')}
              </td>
            </tr>

            <tr>
              <th>Volume</th>
              <td>
                <input
                  disabled={disabled}
                  type="range"
                  min={0}
                  max={1}
                  step="any"
                  value={volume}
                  onChange={(e) =>
                    onUpdate({ volume: parseFloat(e.target.value) })
                  }
                />
                {volume?.toFixed(3)}
              </td>
            </tr>
            <tr>
              <th>Seek</th>
              <td>
                <input
                  type="range"
                  min={0}
                  max={1}
                  width={'100%'}
                  step="any"
                  value={played}
                  onMouseDown={() => onUpdate({ seeking: true })}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    onUpdate({ played: parseFloat(e.target.value) });
                  }}
                  onMouseUp={(e: any) => {
                    onUpdate({
                      seeking: false,
                      seekTo: parseFloat(e.target.value),
                    });
                  }}
                />{' '}
                {played}
              </td>
            </tr>
            <tr>
              <th>Show Control Bar</th>
              <td>
                <InputSwitch
                  checked={controls}
                  onChange={() => onUpdate({ controls: !controls })}
                />
              </td>
            </tr>
            <tr>
              <th>Loop</th>
              <td>
                <InputSwitch
                  checked={loop}
                  onChange={() => onUpdate({ loop: !loop })}
                />
              </td>
            </tr>
            <tr>
              <th>Played</th>
              <td>
                <progress max={1} value={played} />
              </td>
            </tr>
            <tr>
              <th>Played</th>
              <td>{played}</td>
            </tr>
            <tr>
              <th>PlayedSeconds</th>
              <td>{playedSeconds}</td>
            </tr>
            <tr>
              <th>Loaded</th>
              <td>
                <progress max={1} value={loaded} />
              </td>
            </tr>
            <tr>
              <th>LoadedSeconds</th>
              <td>{loadedSeconds}</td>
            </tr>
          </tbody>
        </table>
      </>
    );
  }
);

export default PlayerNavigator;
