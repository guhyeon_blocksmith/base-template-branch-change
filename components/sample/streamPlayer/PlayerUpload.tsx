import React, { FC, useState } from 'react';
import { TabMenu } from 'smith-ui-components/tabmenu';
import PlayerUploadByBlob from 'components/sample/streamPlayer/PlayerUploadByBlob';
import PlayerUploadByUrl from 'components/sample/streamPlayer/PlayerUploadByUrl';

/**
 * Video Upload
 * videoId: video pk
 * https://www.npmjs.com/package/video-metadata-thumbnails
 * @constructor
 */

interface UploadPlayerType {
  videoId: string;
}

const PlayerUpload: FC<UploadPlayerType> = ({ videoId }) => {
  const items = [
    { label: 'URL', icon: 'pi pi-fw pi-calendar' },
    { label: '업로드', icon: 'pi pi-fw pi-home' },
  ];
  const [activeItem, setActiveItem] = useState<number>(0);
  const tabRender: { [key: string]: any } = {
    screen0: <PlayerUploadByUrl />,
    screen1: <PlayerUploadByBlob videoId={videoId} />,
  };

  return (
    <>
      <TabMenu
        model={items}
        activeItem={items[activeItem]}
        onTabChange={(e) => {
          const filterCurrentItem = items.findIndex(
            (row: { icon: string; label: string }) =>
              row.label === e.value.label
          );
          setActiveItem(filterCurrentItem);
        }}
      />
      <div style={{ marginTop: '30px' }}>
        {tabRender[`screen${activeItem}`]}
      </div>
    </>
  );
};

export default PlayerUpload;
