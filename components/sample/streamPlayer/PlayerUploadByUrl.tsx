import React, { FC, useRef, useState } from 'react';
import { Button } from 'smith-ui-components/button';
import YouTubeValidator, {
  CallbackType,
} from 'components/video/YouTubeValidator';
import { Dropdown } from 'smith-ui-components/dropdown';
import { InputTextarea } from 'smith-ui-components/inputtextarea';

/**
 * Video Upload
 * videoId: video pk
 * https://www.npmjs.com/package/video-metadata-thumbnails
 * @constructor
 */

const PlayerUploadByUrl: FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isValid, setIsValid] = useState<boolean>();
  const [showThumbNail, setShowThumbNail] = useState<boolean>(false);
  const [videoUrlString, setVideoUrlString] = useState<string>();
  const [status, setStatus] = useState<string>('READY');
  const [selectedCity1, setSelectedCity1] = useState(null);
  const [value1, setValue1] = useState<string>('');

  const handleClick = () => {
    if (inputRef.current !== null) {
      setVideoUrlString(inputRef.current.value);
    }
  };

  const cities = [
    { name: 'New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'Istanbul', code: 'IST' },
    { name: 'Paris', code: 'PRS' },
  ];

  const onCityChange = (e: { value: React.SetStateAction<null> }) => {
    setSelectedCity1(e.value);
  };

  return (
    <>
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 제목</div>
        <div className="p-col-12 p-md-9">
          <div className="p-inputgroup">
            <input
              placeholder="동영상 제목을 입력해 주세요."
              id={'title'}
              className="p-inputtext p-component"
              type="text"
            />
          </div>
        </div>
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 URL</div>
        <div className="p-col-12 p-md-9">
          <div className="p-inputgroup">
            <input
              placeholder="동영상 URL을 입력해 주세요."
              id={'videoUrl'}
              ref={inputRef}
              className="p-inputtext p-component"
              type="text"
            />
            <Button
              label="확인"
              onClick={() => {
                handleClick();
              }}
            />
          </div>
          <p
            style={{
              marginTop: '3px',
              marginBottom: '-6px',
              color: isValid ? 'blue' : 'red',
              fontSize: '8px',
            }}
          >
            {status}
          </p>
        </div>
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 카테고리</div>
        <div className="p-col-12 p-md-6">
          <Dropdown
            value={selectedCity1}
            options={cities}
            onChange={onCityChange}
            optionLabel="name"
            placeholder="Select a City"
          />
        </div>
        <div className="p-col-12 p-md-3" />
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 썸네일</div>
        <div className="p-col-12 p-md-9">
          <div className="p-inputgroup">
            <YouTubeValidator
              videoUrl={videoUrlString}
              mediaConfig={{
                apiKey: 'AIzaSyCiJ20U5MiaGB8n_DArvcZ8Lpb3plKShxQ',
                showVideo: false,
              }}
              callback={(res: CallbackType) => {
                setIsValid(res.isValid);
                setStatus(res.status);
                setShowThumbNail(res.isValid);
              }}
              showThumbNail={showThumbNail}
            />
          </div>
        </div>
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 설명</div>
        <div className="p-col-12 p-md-9">
          <InputTextarea
            value={value1}
            onChange={(e) => setValue1(e.target.value)}
            rows={5}
            cols={30}
          />
        </div>
      </div>

      <div style={{ textAlign: 'center' }}>
        <Button label={'다음'} onClick={() => alert('준비중')} />
      </div>
    </>
  );
};

export default PlayerUploadByUrl;
