import React, { FC, useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import SurveyRegister from 'components/survey/SurveyRegister';
import TooltipTimeline from 'components/video/TooltipTimeline';
import { Button } from 'smith-ui-components/button';
import { StreamType } from 'smith-ivs/dist/types';
import { InputSwitch } from 'smith-ui-components/inputswitch';
import { surveyRegistrationMode, surveyShowMode } from 'types/enum';
import { SurveySecondsCallBack } from 'types/index';
import { toJS } from 'mobx';
import { useStore } from 'libs/hooks/useStore';
import SmithPlayer from 'smith-ivs';
import SurveyShow from 'components/survey/SurveyShow';
import { CallbackStatusType } from 'smith-ivs/dist/components/reactplayer/ReactPlayerControls';

/**
 * Video MetaInfo
 * @constructor
 * todo SurveyRegister move to smith-ivs
 */

interface PlayerNavigatorType {
  config: StreamType;
  onUpdate: (obj?: any) => void | undefined;
  isShowSurvey: boolean;
  setIsShowSurvey: (obj?: any) => void | undefined;
}

const PlayerMetaInfo: FC<PlayerNavigatorType> = observer(
  ({ config, onUpdate, isShowSurvey, setIsShowSurvey }) => {
    const { playedSeconds, playing, played, metaInfo, duration } = config;
    const convertPlayedSec = Math.floor(playedSeconds!);
    const { guid } = metaInfo;
    const { videoListStore: store } = useStore();
    const [isRegister, setIsRegister] = useState<boolean>(false);
    const [surveySecArray, setSurveySecArray] = useState<{ sec: number }[]>([]);
    const [isReady, setIsReady] = useState<boolean>(false);
    console.log('surveySec00---Array', surveySecArray);

    useEffect(() => {
      store.loadSurveyList(guid).then(() => {
        const getColumn = (anArray: any) => {
          return anArray.map(function (row: any) {
            return { sec: row.playedSec };
          });
        };
        setSurveySecArray(getColumn(toJS(store.surveyList)));
        setIsReady(true);
      });
      return () => {
        setSurveySecArray([]);
        setIsReady(false);
      };
    }, []);

    return (
      <>
        <SmithPlayer
          callback={(res: StreamType) => {
            const params = { ...config, ...res };
            onUpdate(params);
          }}
          playSpec={config}
          callbackStatus={(res: CallbackStatusType) =>
            // todo onBuffer 에 대한 loading 화면 구현
            console.log('video play status:', res)
          }
        />
        <SurveyShow
          callback={(val) => {
            onUpdate({ playing: val });
          }}
          currentPlayingSec={Math.floor(playedSeconds!)}
          surveyList={store.surveyList}
          config={{
            viewMode: surveyShowMode.normal,
            closable: false,
            isShowSurvey,
            guid: config.metaInfo.guid!,
          }}
        />

        <strong className="contTitle">시간 선택</strong>
        <table className="defTable">
          <colgroup>
            <col style={{ width: '180px' }} />
            <col style={{ width: 'calc(50% - 120px)' }} />
          </colgroup>
          <tbody>
            <tr>
              <th>전체 시간(초)</th>
              <td>{duration}</td>
            </tr>
            <tr>
              <th>현재 진행 시간(초)</th>
              <td>{playedSeconds}</td>
            </tr>
            <tr>
              <th>Seek</th>
              <td>
                {isReady && (
                  <TooltipTimeline
                    currentPlayingSec={playedSeconds!}
                    // todo 문제 해결
                    surveySecArray={[]}
                    isShowSurvey={isShowSurvey}
                    played={played!}
                    callbackSelected={(val: SurveySecondsCallBack) => {
                      console.log('val.secArray:', val.secArray);
                      onUpdate({
                        playing: false,
                        seeking: false,
                        seekTo: Math.floor(Number(val.secArray[val.secIndex])),
                      });
                    }}
                    onMouseUp={(val: number) => {
                      onUpdate({
                        seeking: false,
                        seekTo: val,
                      });
                    }}
                    onChange={(val: number) => onUpdate({ played: val })}
                    onMouseDown={(val: boolean) => onUpdate({ seeking: val })}
                    maxTime={metaInfo.runtime!}
                  />
                )}
              </td>
            </tr>
            <tr>
              <th>제어</th>
              <td>
                <Button
                  label={playing ? 'Pause' : 'Play'}
                  onClick={() => {
                    onUpdate({
                      playing: !playing,
                    });
                  }}
                />
              </td>
            </tr>
            <tr>
              <th>설문 보기</th>
              <td>
                <InputSwitch
                  checked={isShowSurvey}
                  onChange={() => setIsShowSurvey(!isShowSurvey)}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <SurveyRegister
          callCallback={(res) => {
            onUpdate({ playing: res.playing });
            setIsRegister(!res.playing);
          }}
          config={config}
          surveySecArray={surveySecArray!}
          playedSeconds={convertPlayedSec}
          surveyMode={
            isRegister
              ? surveyRegistrationMode.add
              : surveyRegistrationMode.ready
          }
        />
      </>
    );
  }
);

export default PlayerMetaInfo;
