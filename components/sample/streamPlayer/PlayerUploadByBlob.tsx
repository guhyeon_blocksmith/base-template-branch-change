import React, { FC, useState } from 'react';
import { ProgressBar } from 'smith-ui-components/progressbar';
import UploadMedia from 'components/video/UploadMedia';
import { Dropdown } from 'smith-ui-components/dropdown';
import { InputTextarea } from 'smith-ui-components/inputtextarea';
import { Button } from 'smith-ui-components/button';

/**
 * Video Upload
 * videoId: video pk
 * https://www.npmjs.com/package/video-metadata-thumbnails
 * @constructor
 */

interface UploadType {
  videoId: string;
}

interface MetaType {
  duration: number;
  height: number;
  width: number;
}

interface FileMetaInfoType {
  lastModified: number;
  lastModifiedDate: any | undefined;
  name: string;
  size: number;
  type: string;
  webkitRelativePath: string;
}

const PlayerUploadByBlob: FC<UploadType> = () => {
  const [progressStep, setProgressStep] = useState<number>(0);
  const [duration, setDuration] = useState<number>(0);
  const [metaData, setMetaData] = useState<MetaType>();
  const [fileInfo, setFileInfo] = useState<FileMetaInfoType>();
  const [selectedCity1, setSelectedCity1] = useState(null);
  const [blobImage, setBlobImage] = useState<any>(null);
  const [value1, setValue1] = useState<string>('');

  const cities = [
    { name: 'New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'Istanbul', code: 'IST' },
    { name: 'Paris', code: 'PRS' },
  ];

  const onCityChange = (e: { value: React.SetStateAction<null> }) => {
    setSelectedCity1(e.value);
  };

  const checkRender = () => {
    return (
      <i
        className="pi pi-check"
        style={{ marginLeft: '5px', fontSize: '1em', color: 'red' }}
      />
    );
  };

  return (
    <>
      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 업로드</div>
        <div className="p-col-12 p-md-9">
          <UploadMedia
            callbackStep={(res: number) => setProgressStep(res)}
            callbackDuration={(res: number) => setDuration(res)}
            callbackMetaData={(res: MetaType | undefined) =>
              res !== undefined && setMetaData(res)
            }
            callbackFileInfo={(res: FileMetaInfoType | undefined) =>
              setFileInfo(res)
            }
            callbackThumbnail={(res) => setBlobImage(res)}
          />
        </div>
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 제목</div>
        <div className="p-col-12 p-md-9">
          <div className="p-inputgroup">
            <input
              placeholder="동영상 제목을 입력해 주세요."
              id={'title'}
              className="p-inputtext p-component"
              type="text"
            />
          </div>
        </div>
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 카테고리</div>
        <div className="p-col-12 p-md-6">
          <Dropdown
            value={selectedCity1}
            options={cities}
            onChange={onCityChange}
            optionLabel="name"
            placeholder="Select a City"
          />
        </div>
        <div className="p-col-12 p-md-3" />
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 썸네일</div>
        <div className="p-col-12 p-md-6">
          <img
            src={blobImage}
            alt={''}
            style={{ width: '320px', height: '180px' }}
          />
        </div>
        <div className="p-col-12 p-md-3" />
      </div>

      <div className="p-grid p-fluid">
        <div className="p-col-12 p-md-3">동영상 설명</div>
        <div className="p-col-12 p-md-9">
          <InputTextarea
            value={value1}
            onChange={(e) => setValue1(e.target.value)}
            rows={5}
            cols={30}
          />
        </div>
      </div>

      <h4>
        1. Get Meta Info
        {progressStep === 1 && checkRender()}
      </h4>
      {metaData !== undefined && (
        <>
          <strong className="contTitle">Meta Info</strong>
          <table className="defTable">
            <colgroup>
              <col style={{ width: '180px' }} />
              <col style={{ width: 'calc(50% - 120px)' }} />
            </colgroup>
            <tbody>
              <tr>
                <th>duration</th>
                <td>{duration}</td>
              </tr>
              <tr>
                <th>name</th>
                <td>{fileInfo?.name}</td>
              </tr>
              <tr>
                <th>type</th>
                <td>{fileInfo?.type}</td>
              </tr>
              <tr>
                <th>size</th>
                <td>{fileInfo?.size}</td>
              </tr>
            </tbody>
          </table>
        </>
      )}
      <h4>
        2. Upload Media
        {progressStep === 2 && (
          <>
            {checkRender()}
            <ProgressBar mode="indeterminate" style={{ marginTop: '15px' }} />
          </>
        )}
      </h4>
      <h4>
        3. Uploaded Media on Storage
        {progressStep === 3 && checkRender()}
      </h4>
      <h4>
        4. Media Converting
        {progressStep === 4 && (
          <>
            {checkRender()}
            <ProgressBar mode="indeterminate" style={{ marginTop: '15px' }} />
          </>
        )}
      </h4>

      <div style={{ textAlign: 'center' }}>
        <Button label={'다음'} onClick={() => alert('준비중')} />
      </div>
    </>
  );
};

export default PlayerUploadByBlob;
