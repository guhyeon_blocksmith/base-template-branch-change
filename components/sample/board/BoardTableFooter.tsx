import React, { FC } from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'libs/hooks/useStore';
import Link from 'next/link';
import { Button } from 'smith-ui-components/button';

/**
 * 게시판 footer. 등록, 삭제, 수정 버튼
 */
const BoardTableFooter: FC = observer(() => {
  const { boardListStore: store } = useStore();

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#f8f9fa',
      }}
    >
      <Link href={`/sample/board/new`}>
        <Button label="등록" icon="pi pi-plus" className="p-mr-2 col-md-2" />
      </Link>
      <Button
        label="삭제"
        onClick={store.handleDeleteClick}
        icon="pi pi-trash"
        className="p-button-outlined p-button-danger p-mr-2"
      />
      <Button
        label="수정"
        className="p-button-outlined p-button-edit p-mr-2"
        icon={'pi pi-pencil'}
        onClick={store.handleEditClick}
      />
    </div>
  );
});

export default BoardTableFooter;
