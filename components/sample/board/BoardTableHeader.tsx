import React, { FC } from 'react';
import { DataTable } from 'smith-ui-components/datatable';
import { observer } from 'mobx-react';
import { useStore } from 'libs/hooks/useStore';
import { Dropdown } from 'smith-ui-components/dropdown';
import { Button } from 'smith-ui-components/button';

interface BoardTableHeaderProps {
  dataTableRef: React.RefObject<DataTable>;
}

/**
 * 게시판 Header, 페이지당 글 수, 엑셀 내보내기
 */
const BoardTableHeader: FC<BoardTableHeaderProps> = observer(
  ({ dataTableRef }) => {
    const { boardListStore: store } = useStore();
    const exportCSV = () => {
      dataTableRef.current?.exportCSV();
    };

    const rowPerPage = [
      { label: '10개씩 보기', value: 10 },
      { label: '20개씩 보기', value: 20 },
      { label: '30개씩 보기', value: 30 },
      { label: '50개씩 보기', value: 50 },
      { label: '100개씩 보기', value: 100 },
    ];

    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#f8f9fa',
        }}
      >
        <Dropdown
          options={rowPerPage}
          value={store.rowCount}
          optionLabel="label"
          placeholder="10개씩 보기"
          onChange={store.handleRowPerPageChange}
        />
        <Button
          label="엑셀로 보내기"
          className="p-button-outlined p-button-secondary"
          icon={'pi pi-file-excel'}
          onClick={exportCSV}
        />
      </div>
    );
  }
);

export default BoardTableHeader;
