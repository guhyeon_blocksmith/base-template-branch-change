import React, { FC, useState } from 'react';
import { observer } from 'mobx-react';
import { Button } from 'smith-ui-components/button';
import { Dropdown } from 'smith-ui-components/dropdown';
import { InputText } from 'smith-ui-components/inputtext';
import { DropdownType } from 'types/sample';
import { useStore } from 'libs/hooks/useStore';

const BoardListSearch: FC = observer(() => {
  const { boardListStore: store } = useStore();

  const [dropdownValue, setDropdownValue] = useState<DropdownType>();

  const dropdownValues: DropdownType[] = [
    { name: '아이디', code: '1' },
    { name: '제목', code: '2' },
  ];

  return (
    <>
      <strong className="contTitle">회원 정보 검색</strong>
      <table className="defTable">
        <colgroup>
          <col style={{ width: '120px' }} />
          <col style={{ width: 'calc(50% - 120px)' }} />
          <col style={{ width: '120px' }} />
        </colgroup>
        <tbody>
          <tr>
            <th>
              <i className="clrRed">*</i> 검색어
            </th>
            <td>
              <Dropdown
                value={dropdownValue}
                options={dropdownValues}
                onChange={(e) => setDropdownValue(e.value)}
                optionLabel="name"
                placeholder="항목"
              />
              <InputText
                type="search"
                value={store.searchKeyword}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    store.loadList().then();
                  }
                }}
                onChange={store.handleSearchKeywordChange}
                placeholder="검색어를 입력하세요"
              />
            </td>
          </tr>
        </tbody>
      </table>
      <div className="botBtns">
        <Button
          label="검색"
          onClick={() => {
            store.loadList().then();
          }}
        />
        <Button
          label="초기화"
          className="p-button-outlined gray"
          onClick={store.handleSearchClear}
        />
      </div>
    </>
  );
});

export default BoardListSearch;
