import React, { FC } from 'react';
import { InputText } from 'smith-ui-components/inputtext';
import { Editor } from 'smith-ui-components/editor';
import Axios from 'axios';
import { FileUpload } from 'smith-ui-components/fileupload';
import { Button } from 'smith-ui-components/button';
import { useRouter } from 'next/router';
import { observer } from 'mobx-react';
import FileList from 'components/common/FileList';
import useAppendRef from 'libs/hooks/useAppendRef';
import { toJS } from 'mobx';
import { useStore } from 'libs/hooks/useStore';

/**
 * 게시판 수정 폼
 * @param mode
 * @constructor
 */
const BoardEditForm: FC = observer(() => {
  const { boardEditStore: store } = useStore();
  const router = useRouter();
  const [titleRef] = useAppendRef(store.inputRefs, 'title');
  const [contentRef] = useAppendRef(store.inputRefs, 'content');

  // item 구조분해 할당
  const { item } = store;

  // item이 없으면 null render - 리액트에서는 null, boolean, undefined 리턴시 빈 화면을 뿌려줍니다.
  if (item === undefined) return null;

  // content,title 구조분해 할당 (mobx store는 렌더링으로만 쓰고 값 대입에는 item.content=값 이런식으로 써주세요.)
  const { content, title, id } = item;

  // TODO gridTable
  return (
    <>
      <ul className="fakeTable">
        <li className="row">
          <dl>
            <dt>ID</dt>
            <dd>
              <InputText id="id" type="text" value={id} disabled={true} />
            </dd>
          </dl>
        </li>
        {/*        2단 테이블 샘플*/}
        {/*        <li>
          <dl>
            <dt>1단</dt>
            <dd>
              <InputText id="id" type="text" value={id} disabled={true} />
            </dd>
          </dl>
          <dl>
            <dt>2단</dt>
            <dd>
              <InputText id="id" type="text" value={id} disabled={true} />
            </dd>
          </dl>
        </li>*/}
        <li className="row">
          <dl>
            <dt>제목</dt>
            <dd>
              <InputText
                ref={titleRef}
                id="title"
                type="text"
                className={`${
                  store.validErrors.has('title') ? 'p-invalid' : ''
                }`}
                value={title}
                onChange={store.handleChangeTitle}
              />
              {store.validErrors.get('title')?.map((message: string) => (
                <small key={message} className="p-invalid p-d-block">
                  {message}
                </small>
              ))}
            </dd>
          </dl>
        </li>
        <li className="row">
          <dl>
            <dt>내용</dt>
            <dd>
              <Editor
                ref={contentRef}
                className={`${
                  store.validErrors.has('content') ? 'p-invalid' : ''
                }`}
                style={{ height: '320px' }}
                value={content}
                onTextChange={store.handleChangeContent}
                uploadImage={async (file) => {
                  const formData = new FormData();
                  formData.append('file', file);
                  const response = await Axios.post(
                    process.env.NEXT_PUBLIC_BOARD_FILE_IMAGE_API!,
                    formData
                  );
                  return response.data.fileUrl;
                }}
              />
              {store.validErrors.get('content')?.map((message: string) => (
                <small key={message} className="p-invalid p-d-block">
                  {message}
                </small>
              ))}
            </dd>
          </dl>
        </li>
        <li className="row">
          <dl>
            <dt>파일첨부</dt>
            <dd>
              <FileUpload
                name="files"
                url={`${process.env.NEXT_PUBLIC_BOARD_FILE_API}_${item.id}`}
                onUpload={store.handleFileUpload}
                multiple
                maxFileSize={1000000}
                emptyTemplate={
                  <p className="p-m-0">
                    Drag and drop files to here to upload.
                  </p>
                }
                auto={true}
              />
              <FileList
                attachedFiles={toJS(item.attachedFiles)}
                onFileDeleteClick={(file) => {
                  store.handleFileRemove(file).then();
                }}
              />
            </dd>
          </dl>
        </li>
      </ul>
      <div className="p-fluid">
        <div className="p-formgroup-inline">
          <div className="p-field">
            <Button label="저장" onClick={store.handleClickSave} />
          </div>
          <div className="p-field">
            <Button
              label="뒤로가기"
              onClick={async () => {
                router.back();
              }}
            />
          </div>
          <div className="p-field">
            <Button label="Clear" onClick={store.handleClear} />
          </div>
        </div>
      </div>
    </>
  );
});

export default BoardEditForm;
