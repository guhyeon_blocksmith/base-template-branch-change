import React, { FC, useRef } from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'libs/hooks/useStore';
import { DataTable } from 'smith-ui-components/datatable';
import { Column } from 'smith-ui-components/column';
import { BoardDetailResponse } from 'types/sample';
import BoardTableHeader from 'components/sample/board/BoardTableHeader';
import BoardTableFooter from 'components/sample/board/BoardTableFooter';
import { Button } from 'smith-ui-components/button';
import { windowPopup } from 'libs/common';

/**
 * 게시판 목록 테이블
 * @constructor
 */
const BoardTable: FC = observer(() => {
  const { boardListStore: store } = useStore();
  // const router = useRouter();
  const dataTableRef = useRef<DataTable>(null);

  return (
    <>
      <strong className="contTitle">검색 결과</strong>
      <strong className="smallTitle">검색 건수: {store.totalCount}건</strong>
      <DataTable
        ref={dataTableRef} // excel 내보내기 때문에 사용함
        header={<BoardTableHeader dataTableRef={dataTableRef} />}
        footer={<BoardTableFooter />}
        lazy={true} // 비동기로 데이터를 가져옴
        paginator // 페이지 사용
        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink" // 페이지에 표시될 템플릿
        value={store.items} // 목록 아이템
        rows={store.rowCount} // 보여줄 열 수
        totalRecords={store.totalCount} // 전체 열 수
        first={store.firstIndex} // index
        selection={store.selectedItems} // 선택된 아이템
        onPage={store.handlePageChange}
        selectionMode="multiple" // 여러 행 선택
        metaKeySelection={false} // 여러 행 선택시 각자 클릭으로 선택
        onSelectionChange={store.handleSelectionChange} // 선택된 행 변경 이벤트
        removableSort // 오름차순, 내림차순, 취소 가능한 정렬
        sortField={store.sortField} // 정렬 필드
        sortOrder={store.sortOrder} // 정렬 순서(1,0,-1)
        onSort={store.handleSort} // 정렬 이벤트
      >
        <Column selectionMode="multiple" headerStyle={{ width: '3em' }} />

        <Column field={'id'} header={'ID'} sortable={true} />
        <Column field={'title'} header={'제목'} sortable={true} />
        <Column
          header={'생성일'}
          field={'createdAt'}
          body={(item: BoardDetailResponse) => {
            const { createdAt } = item;
            if (createdAt) {
              // TODO date함수로 포맷 변경
              return new Date(createdAt).toLocaleString('ko-KR', {
                timeZone: 'Asia/Seoul',
              });
            } else {
              return '';
            }
          }}
          sortable={true}
        />
        <Column
          field={'detail'}
          body={(item: BoardDetailResponse) => (
            <Button
              label="상세"
              onClick={() => {
                windowPopup(`/sample/board/${item.id}`);
              }}
              className="p-button-outlined"
            />
          )}
          header={'상세 내용'}
        />
      </DataTable>
    </>
  );
});
export default BoardTable;
