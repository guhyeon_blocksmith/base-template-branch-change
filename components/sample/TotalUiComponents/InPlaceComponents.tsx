import React, { FC, useRef, useState } from 'react';
import {
  Inplace,
  InplaceContent,
  InplaceDisplay,
} from 'smith-ui-components/inplace';
import { InputText } from 'smith-ui-components/inputtext';
import { ProgressBar } from 'smith-ui-components/progressbar';
import { ProgressSpinner } from 'smith-ui-components/progressspinner';
import { Captcha } from 'smith-ui-components/captcha';
import { Toolbar } from 'smith-ui-components/toolbar';
import { DeferredContent } from 'smith-ui-components/deferredcontent';
import { Button } from 'smith-ui-components/button';
import { Toast } from 'smith-ui-components/toast';

const InPlaceComponents: FC = () => {
  const toastRef = useRef<Toast>(null);
  const [inPlaceText, setInPlaceText] = useState<string>();

  const showResponse = () => {
    toastRef.current?.show({
      severity: 'info',
      summary: 'Success',
      detail: 'User Responded',
    });
  };

  const rightContents = () => {
    return (
      <>
        <Button icon="pi pi-search" className="p-mr-2" />
        <Button icon="pi pi-calendar" className="p-button-success p-mr-2" />
        <Button icon="pi pi-times" className="p-button-danger" />
      </>
    );
  };

  const onImageLoad = () => {
    toastRef.current?.show({
      severity: 'success',
      summary: 'Image Initialized',
      detail: 'Scroll down to load the datatable',
    });
  };
  return (
    <>
      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>InPlace</h5>
          <Inplace closable>
            <InplaceDisplay>{inPlaceText || 'Click to Edit'}</InplaceDisplay>
            <InplaceContent>
              <InputText
                value={inPlaceText}
                onChange={(e) => setInPlaceText(e.target.value)}
              />
            </InplaceContent>
          </Inplace>
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>ProgressBar</h5>
          <ProgressBar mode="indeterminate" style={{ height: '6px' }} />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>ProgressSpinner</h5>
          <ProgressSpinner />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Captcha</h5>
          <Captcha siteKey="YOUR_SITE_KEY" onResponse={showResponse} />
        </div>
      </div>

      <div className="p-col-12 p-md-8">
        <div className="card">
          <h5>Toolbar</h5>
          <Toolbar right={rightContents} />
        </div>
      </div>

      <div className="p-col-12 p-md-8">
        <div className="card">
          <h5>Deferred</h5>
          <Toast ref={toastRef} />
          <DeferredContent onLoad={onImageLoad}>
            <img
              src="/sample/totaluicomponents/galleria/galleria1.jpg"
              onError={(e) =>
                (e.currentTarget.src =
                  'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
              }
              alt="Prime"
            />
          </DeferredContent>
        </div>
      </div>
    </>
  );
};

export default InPlaceComponents;
