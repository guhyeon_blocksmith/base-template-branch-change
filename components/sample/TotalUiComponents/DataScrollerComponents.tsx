import React, { FC, useEffect, useRef, useState } from 'react';
import { DataScroller } from 'smith-ui-components/datascroller';
import { Rating } from 'smith-ui-components/rating';
import { Button } from 'smith-ui-components/button';
import axios from 'axios';
import { ProductsType } from 'types/sample';
import styles from './DataScrollerComponents.module.scss';

const DataScrollerComponents: FC = () => {
  const dataScrollerRef = useRef<Button>(null);
  const [dataViewProduct, setDataViewProduct] = useState<ProductsType[]>([]);

  useEffect(() => {
    const dataViewData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/products.json')
        .then((res) => res.data.data);
    };
    dataViewData().then((data) => setDataViewProduct(data));
  }, []);

  const dataScrollerItemTemplate = (data: any) => {
    return (
      <div className={styles['product-item']}>
        <img
          src={`/sample/totaluicomponents/product/${data.image}`}
          onError={(e) =>
            (e.currentTarget.src =
              'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
          }
          alt={data.name}
        />
        <div className={styles['product-detail']}>
          <div className={styles['product-name']}>{data.name}</div>
          <div className={styles['product-description']}>
            {data.description}
          </div>
          <Rating value={data.rating} readOnly={true} cancel={false} />
          <i className={`pi pi-tag ${styles['product-category-icon']}`} />
          <span className={styles['product-category']}>{data.category}</span>
        </div>
        <div className={styles['product-action']}>
          <span className={styles['product-price']}>${data.price}</span>
          <Button
            icon="pi pi-shopping-cart"
            label="Add to Cart"
            disabled={data.inventoryStatus === 'OUTOFSTOCK'}
          />
          <span
            className={`${styles['product-badge']} ${
              styles['status-' + data.inventoryStatus.toLowerCase()]
            }`}
          >
            {data.inventoryStatus}
          </span>
        </div>
      </div>
    );
  };

  const dataScrollerFooter = (
    <Button ref={dataScrollerRef} type="text" icon="pi pi-plus" label="Load" />
  );
  return (
    <div className={`card ${styles['p-data-scroller']}`}>
      <h5>Data Scroller</h5>
      <DataScroller
        value={dataViewProduct}
        itemTemplate={dataScrollerItemTemplate}
        rows={5}
        loader={dataScrollerRef.current}
        footer={dataScrollerFooter}
        header="Click Load Button at Footer to Load More"
      />
    </div>
  );
};

export default DataScrollerComponents;
