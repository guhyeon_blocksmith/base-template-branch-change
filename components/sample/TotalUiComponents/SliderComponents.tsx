import React, { FC, useState } from 'react';
import { InputText } from 'smith-ui-components/inputtext';
import { Slider } from 'smith-ui-components/slider';
import { Rating } from 'smith-ui-components/rating';
import { InputSwitch } from 'smith-ui-components/inputswitch';

const SliderComponents: FC = () => {
  const [sliderValue, setSliderValue] = useState<number>(0);
  const [ratingValue, setRatingValue] = useState<number>(0);
  const [switchValue, setSwitchValue] = useState<boolean>(false);

  return (
    <div className="card">
      <h5>Slider</h5>
      <InputText
        value={sliderValue}
        onChange={(e) => setSliderValue(Number(e.target.value))}
        className="p-mb-1"
      />
      <Slider
        value={sliderValue}
        onChange={(e) => setSliderValue(Number(e.value))}
      />

      <h5>Rating</h5>
      <Rating
        value={ratingValue}
        onChange={(e) => setRatingValue(Number(e.value))}
      />

      <h5>Input Switch</h5>
      <InputSwitch
        checked={switchValue}
        onChange={(e) => setSwitchValue(e.value)}
      />
    </div>
  );
};

export default SliderComponents;
