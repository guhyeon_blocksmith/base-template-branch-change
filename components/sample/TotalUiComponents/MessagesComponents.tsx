import React, { FC, useRef } from 'react';
import { Button } from 'smith-ui-components/button';
import { Messages } from 'smith-ui-components/messages';
import { Toast } from 'smith-ui-components/toast';
import { InputText } from 'smith-ui-components/inputtext';
import { Message } from 'smith-ui-components/message';

const MessagesComponents: FC = () => {
  const msgRef = useRef<Messages>(null);
  const toastRef = useRef<Toast>(null);
  const capitalize = (s: string) => {
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  const addMessage = (
    severity: 'success' | 'info' | 'warn' | 'error' | undefined
  ) => {
    msgRef.current?.show([
      {
        severity,
        summary: capitalize(severity || ''),
        detail: 'Message Content',
      },
    ]);
  };

  const showToast = (
    severity: 'success' | 'info' | 'warn' | 'error' | undefined
  ) => {
    toastRef.current?.show({
      severity,
      summary: capitalize(severity || ''),
      detail: 'Message Content',
      life: 3000,
    });
  };

  return (
    <>
      <div className="p-col-12 p-lg-6">
        <div className="card">
          <h5>Messages</h5>
          <Button
            onClick={() => addMessage('info')}
            label="Info"
            className="p-button-info p-mr-2"
          />
          <Button
            onClick={() => addMessage('success')}
            label="Success"
            className="p-button-success p-mr-2"
          />
          <Button
            onClick={() => addMessage('warn')}
            label="Warn"
            className="p-button-warning p-mr-2"
          />
          <Button
            onClick={() => addMessage('error')}
            label="Error"
            className="p-button-danger"
          />

          <Messages ref={msgRef} />
        </div>
      </div>

      <div className="p-col-12 p-lg-6">
        <div className="card">
          <h5>Toast</h5>

          <Toast ref={toastRef} />
          <Button
            onClick={() => showToast('info')}
            label="Info"
            className="p-button-info p-mr-2"
          />
          <Button
            onClick={() => showToast('success')}
            label="Success"
            className="p-button-success p-mr-2"
          />
          <Button
            onClick={() => showToast('warn')}
            label="Warn"
            className="p-button-warning p-mr-2"
          />
          <Button
            onClick={() => showToast('error')}
            label="Error"
            className="p-button-danger"
          />
        </div>
      </div>

      <div className="p-col-12 p-lg-8">
        <div className="card">
          <h5>Inline Message</h5>
          <div className="p-formgroup-inline" style={{ marginBottom: '.5rem' }}>
            <label htmlFor="username1" className="p-sr-only">
              Username
            </label>
            <InputText
              id="username1"
              type="text"
              placeholder="Username"
              className="p-invalid p-mr-2"
            />
            <Message severity="error" text="Username is required" />
          </div>
          <div className="p-formgroup-inline">
            <label htmlFor="email" className="p-sr-only">
              email
            </label>
            <InputText
              id="email"
              placeholder="Email"
              className="p-invalid p-mr-2"
            />
            <Message severity="error" />
          </div>
        </div>
      </div>

      <div className="p-col-12 p-lg-4">
        <div className="card">
          <h5>Helper Text</h5>
          <div className="p-field p-fluid">
            <label htmlFor="username2">Username</label>
            <InputText
              id="username2"
              type="text"
              className="p-error"
              aria-describedby="username-help"
            />
            <small id="username-help" className="p-error">
              Enter your username to reset your password.
            </small>
          </div>
        </div>
      </div>
    </>
  );
};

export default MessagesComponents;
