import React, { FC, useEffect, useState } from 'react';
import { DataView, DataViewLayoutOptions } from 'smith-ui-components/dataview';
import { Dropdown } from 'smith-ui-components/dropdown';
import { Rating } from 'smith-ui-components/rating';
import { Button } from 'smith-ui-components/button';
import axios from 'axios';
import { ProductsType } from 'types/sample';
import styles from './DataViewComponents.module.scss';

const DataViewComponents: FC = () => {
  const [dataViewSortOrder, setDataViewSortOrder] = useState<number>();
  const [dataViewSortField, setDataViewSortField] = useState();
  const [dataViewLayout, setDataViewLayout] = useState('grid');
  const [dataViewSortKey, setDataViewSortKey] = useState();
  const [dataViewProduct, setDataViewProduct] = useState<ProductsType[]>([]);

  useEffect(() => {
    const dataViewData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/products.json')
        .then((res) => res.data.data);
    };
    dataViewData().then((data) => setDataViewProduct(data));
  }, []);

  const dataViewHeader = () => {
    return (
      <div className="p-grid p-nogutter">
        <div className="p-col-6" style={{ textAlign: 'left' }}>
          <Dropdown
            options={sortOptions}
            value={dataViewSortKey}
            optionLabel="label"
            placeholder="Sort By Price"
            onChange={onSortChange}
          />
        </div>
        <div className="p-col-6" style={{ textAlign: 'right' }}>
          <DataViewLayoutOptions
            layout={dataViewLayout}
            onChange={(e) => setDataViewLayout(e.value)}
          />
        </div>
      </div>
    );
  };

  const sortOptions = [
    { label: 'Price High to Low', value: '!price' },
    { label: 'Price Low to High', value: 'price' },
  ];

  const onSortChange = (event: {
    originalEvent: Event;
    value: any;
    target: { name: string; id: string; value: any };
  }) => {
    const value = event.value;

    if (value.indexOf('!') === 0) {
      setDataViewSortOrder(-1);
      setDataViewSortField(value.substring(1, value.length));
      setDataViewSortKey(value);
    } else {
      setDataViewSortOrder(1);
      setDataViewSortField(value);
      setDataViewSortKey(value);
    }
  };

  const dataViewItemTemplate = (product: any, layout: 'grid' | 'list') => {
    if (!product) {
      return;
    }

    if (layout === 'list') {
      return renderListItem(product);
    } else if (layout === 'grid') {
      return renderGridItem(product);
    }
  };

  const renderListItem = (data: any) => {
    return (
      <div className="p-col-12">
        <div className={styles['product-list-item']}>
          <img
            src={`/sample/totaluicomponents/product/${data.image}`}
            onError={(e) =>
              (e.currentTarget.src =
                'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
            }
            alt={data.name}
          />
          <div className={styles['product-list-detail']}>
            <div className={styles['product-name']}>{data.name}</div>
            <div className={styles['product-description']}>
              {data.description}
            </div>
            <Rating value={data.rating} readOnly cancel={false} />
            <i className={`pi pi-tag ${styles['product-category-icon']}`} />
            <span className={styles['product-category']}>{data.category}</span>
          </div>
          <div className={styles['product-list-action']}>
            <span className={styles['product-price']}>${data.price}</span>
            <Button
              icon="pi pi-shopping-cart"
              label="Add to Cart"
              disabled={data.inventoryStatus === 'OUTOFSTOCK'}
            />
            <span
              // className={`product-badge status-${data.inventoryStatus.toLowerCase()}`}
              className={`${styles['product-badge']} ${
                styles['status-' + data.inventoryStatus.toLowerCase()]
              }`}
            >
              {data.inventoryStatus}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const renderGridItem = (data: any) => {
    return (
      <div className="p-col-12 p-md-4">
        <div className={`${styles['product-grid-item']} card`}>
          <div className={styles['product-grid-item-top']}>
            <div>
              <i className={`pi pi-tag ${styles['product-category-icon']}`} />
              <span className={styles['product-category']}>
                {data.category}
              </span>
            </div>
            <span
              //className={`product-badge status-${data.inventoryStatus.toLowerCase()}`}
              className={`${styles['product-badge']} ${
                styles['status-' + data.inventoryStatus.toLowerCase()]
              }`}
            >
              {data.inventoryStatus}
            </span>
          </div>
          <div className={styles['product-grid-item-content']}>
            <img
              src={`/sample/totaluicomponents/product/${data.image}`}
              onError={(e) =>
                (e.currentTarget.src =
                  'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
              }
              alt={data.name}
            />
            <div className={styles['product-name']}>{data.name}</div>
            <div className={styles['product-description']}>
              {data.description}
            </div>
            <Rating value={data.rating} readOnly cancel={false} />
          </div>
          <div className={styles['product-grid-item-bottom']}>
            <span className={styles['product-price']}>${data.price}</span>
            <Button
              icon="pi pi-shopping-cart"
              label="Add to Cart"
              disabled={data.inventoryStatus === 'OUTOFSTOCK'}
            />
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className={`card ${styles['data-view']}`}>
      <h5>DataView</h5>
      <DataView
        value={dataViewProduct}
        layout={dataViewLayout}
        header={dataViewHeader()}
        itemTemplate={dataViewItemTemplate}
        paginator
        rows={9}
        sortOrder={dataViewSortOrder}
        sortField={dataViewSortField}
      />
    </div>
  );
};

export default DataViewComponents;
