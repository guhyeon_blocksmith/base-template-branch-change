import React, { FC, useRef } from 'react';
import { Menubar } from 'smith-ui-components/menubar';
import { InputText } from 'smith-ui-components/inputtext';
import { TieredMenu } from 'smith-ui-components/tieredmenu';
import { Menu } from 'smith-ui-components/menu';
import { Button } from 'smith-ui-components/button';
import { ContextMenu } from 'smith-ui-components/contextmenu';

const MenubarComponents: FC = () => {
  const menuRef = useRef<Menu>(null);
  const contextMenuRef = useRef<ContextMenu>(null);
  const menubarEndTemplate = () => {
    return (
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText type="text" placeholder="Search" />
      </span>
    );
  };

  const tieredMenuItems = [
    {
      label: 'Customers',
      icon: 'pi pi-fw pi-table',
      items: [
        {
          label: 'New',
          icon: 'pi pi-fw pi-user-plus',
          items: [
            {
              label: 'Customer',
              icon: 'pi pi-fw pi-plus',
            },
            {
              label: 'Duplicate',
              icon: 'pi pi-fw pi-copy',
            },
          ],
        },
        {
          label: 'Edit',
          icon: 'pi pi-fw pi-user-edit',
        },
      ],
    },
    {
      label: 'Orders',
      icon: 'pi pi-fw pi-shopping-cart',
      items: [
        {
          label: 'View',
          icon: 'pi pi-fw pi-list',
        },
        {
          label: 'Search',
          icon: 'pi pi-fw pi-search',
        },
      ],
    },
    {
      label: 'Shipments',
      icon: 'pi pi-fw pi-envelope',
      items: [
        {
          label: 'Tracker',
          icon: 'pi pi-fw pi-compass',
        },
        {
          label: 'Map',
          icon: 'pi pi-fw pi-map-marker',
        },
        {
          label: 'Manage',
          icon: 'pi pi-fw pi-pencil',
        },
      ],
    },
    {
      label: 'Profile',
      icon: 'pi pi-fw pi-user',
      items: [
        {
          label: 'Settings',
          icon: 'pi pi-fw pi-cog',
        },
        {
          label: 'Billing',
          icon: 'pi pi-fw pi-file',
        },
      ],
    },
    {
      separator: true,
    },
    {
      label: 'Quit',
      icon: 'pi pi-fw pi-sign-out',
    },
  ];

  const toggleMenu = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    menuRef.current?.toggle(event);
  };

  const onContextRightClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    contextMenuRef.current?.show(event);
  };

  const menuItems = [
    {
      label: 'Customers',
      items: [
        {
          label: 'New',
          icon: 'pi pi-fw pi-plus',
        },
        {
          label: 'Edit',
          icon: 'pi pi-fw pi-user-edit',
        },
      ],
    },
    {
      label: 'Orders',
      items: [
        {
          label: 'View',
          icon: 'pi pi-fw pi-list',
        },
        {
          label: 'Search',
          icon: 'pi pi-fw pi-search',
        },
      ],
    },
  ];

  const contextMenuItems = [
    {
      label: 'Save',
      icon: 'pi pi-save',
    },
    {
      label: 'Update',
      icon: 'pi pi-refresh',
    },
    {
      label: 'Delete',
      icon: 'pi pi-trash',
    },
    {
      separator: true,
    },
    {
      label: 'Options',
      icon: 'pi pi-cog',
    },
  ];
  return (
    <>
      <div className="p-col-12">
        <div className="card card-w-title">
          <h5>Menubar</h5>
          <Menubar model={tieredMenuItems} end={menubarEndTemplate} />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Tiered Menu</h5>
          <TieredMenu model={tieredMenuItems} />
        </div>
      </div>
      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Plain Menu</h5>
          <Menu model={menuItems} />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Overlay Menu</h5>

          <Menu ref={menuRef} model={menuItems} popup />
          <Button
            type="button"
            label="Options"
            icon="pi pi-angle-down"
            onClick={toggleMenu}
            style={{ width: 'auto' }}
          />
        </div>

        <div className="card" onContextMenu={onContextRightClick}>
          <h5>ContextMenu</h5>
          Right click to display.
          <ContextMenu model={contextMenuItems} ref={contextMenuRef} />
        </div>
      </div>
    </>
  );
};

export default MenubarComponents;
