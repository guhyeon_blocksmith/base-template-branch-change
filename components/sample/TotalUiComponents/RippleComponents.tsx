import React from 'react';
import PrimeReact from 'smith-ui-components/api';
import { Ripple } from 'smith-ui-components/ripple';
import styles from './RippleComponents.module.scss';

const RippleComponents = () => {
  PrimeReact.ripple = true;

  return (
    <div className="p-col-12 p-md-6">
      <div className={`${styles.rippleComponent}`}>
        <div className={`${styles.container} p-d-flex`}>
          <div className={`${styles.card} ${styles.primaryBox} p-ripple`}>
            Default
            <Ripple />
          </div>
          <div
            className={`${styles.card} ${styles['styled-box-green']} p-ripple`}
          >
            Green
            <Ripple />
          </div>
          <div
            className={`${styles.card} ${styles['styled-box-orange']} p-ripple`}
          >
            Orange
            <Ripple />
          </div>
          <div
            className={`${styles.card} ${styles['styled-box-purple']} p-ripple`}
          >
            Purple
            <Ripple />
          </div>
        </div>
      </div>
    </div>
  );
};

export default RippleComponents;
