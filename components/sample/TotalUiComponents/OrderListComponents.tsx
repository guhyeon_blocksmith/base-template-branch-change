import React, { FC, useEffect, useState } from 'react';
import { OrderList } from 'smith-ui-components/orderlist';
import axios from 'axios';
import { ProductsType } from 'types/sample';
import styles from './OrderListComponents.module.scss';

const OrderListComponents: FC = () => {
  const [orderListProducts, setOrderListProducts] = useState();

  useEffect(() => {
    const orderListData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/products-small.json')
        .then((res) => res.data.data);
    };
    orderListData().then((data) => setOrderListProducts(data));
  }, []);

  const orderListItemTemplate = (item: ProductsType) => {
    return (
      <div className={styles['product-item']}>
        <div className={styles['image-container']}>
          <img
            src={`/sample/totaluicomponents/product/${item.image}`}
            onError={(e) =>
              (e.currentTarget.src =
                'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
            }
            alt={item.name}
          />
        </div>
        <div className={styles['product-list-detail']}>
          <h5 className="p-mb-2">{item.name}</h5>
          <i className={`pi pi-tag ${styles['product-category-icon']}`} />
          <span className={styles['product-category']}>{item.category}</span>
        </div>
        <div className={styles['product-list-action']}>
          <h6 className="p-mb-2">${item.price}</h6>
          <span
            className={`${styles['product-badge']} ${
              styles['status-' + item.inventoryStatus.toLowerCase()]
            } `}
          >
            {item.inventoryStatus}
          </span>
        </div>
      </div>
    );
  };
  return (
    <div className={`card ${styles['p-order-list']}`}>
      <h5>Order List</h5>
      <OrderList
        value={orderListProducts}
        itemTemplate={orderListItemTemplate}
        header="Products"
        onChange={(e) => setOrderListProducts(e.value)}
      />
    </div>
  );
};

export default OrderListComponents;
