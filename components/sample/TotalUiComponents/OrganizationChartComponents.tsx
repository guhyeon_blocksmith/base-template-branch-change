import React, { FC } from 'react';
import { OrganizationChart } from 'smith-ui-components/organizationchart';

const OrganizationChartComponents: FC = () => {
  const OrganizationChartValue = {
    data: [
      {
        label: 'F.C Barcelona',
        expanded: true,
        children: [
          {
            label: 'F.C Barcelona',
            expanded: true,
            children: [
              {
                label: 'Chelsea FC',
              },
              {
                label: 'F.C. Barcelona',
              },
            ],
          },
          {
            label: 'Real Madrid',
            expanded: true,
            children: [
              {
                label: 'Bayern Munich',
              },
              {
                label: 'Real Madrid',
              },
            ],
          },
        ],
      },
    ],
  };

  return (
    <div className="card">
      <h5>OrganizationChart</h5>
      <OrganizationChart value={OrganizationChartValue.data} />
    </div>
  );
};

export default OrganizationChartComponents;
