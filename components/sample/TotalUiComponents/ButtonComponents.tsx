import React, { FC } from 'react';
import { Button } from 'smith-ui-components/button';
import { Badge } from 'smith-ui-components/badge';
import { Tag } from 'smith-ui-components/tag';
import styles from './ButtonComponents.module.scss';

const ButtonComponents: FC = () => {
  return (
    <div className={`card ${styles.buttonDemo}`}>
      <h5>Basic</h5>
      <Button label="Submit" />
      <Button label="Disabled" disabled />
      <Button label="Link" className="p-button-link" />
      <h5>Icons</h5>
      <Button icon="pi pi-check" />
      <Button label="Submit" icon="pi pi-check" />
      <Button label="Submit" icon="pi pi-check" iconPos="right" />
      <h5>Severities</h5>
      <Button label="Primary" />
      <Button label="Secondary" className="p-button-secondary" />
      <Button label="Success" className="p-button-success" />
      <Button label="Info" className="p-button-info" />
      <Button label="Warning" className="p-button-warning" />
      <Button label="Help" className="p-button-help" />
      <Button label="Danger" className="p-button-danger" />
      <h5>Raised Buttons</h5>
      <Button label="Primary" className="p-button-raised" />
      <Button
        label="Secondary"
        className="p-button-raised p-button-secondary"
      />
      <Button label="Success" className="p-button-raised p-button-success" />
      <Button label="Info" className="p-button-raised p-button-info" />
      <Button label="Warning" className="p-button-raised p-button-warning" />
      <Button label="Help" className="p-button-raised p-button-help" />
      <Button label="Danger" className="p-button-raised p-button-danger" />
      <h5>Rounded Buttons</h5>
      <Button label="Primary" className="p-button-rounded" />
      <Button
        label="Secondary"
        className="p-button-rounded p-button-secondary"
      />
      <Button label="Success" className="p-button-rounded p-button-success" />
      <Button label="Info" className="p-button-rounded p-button-info" />
      <Button label="Warning" className="p-button-rounded p-button-warning" />
      <Button label="Help" className="p-button-rounded p-button-help" />
      <Button label="Danger" className="p-button-rounded p-button-danger" />
      <h5>Text Buttons</h5>
      <Button label="Primary" className="p-button-text" />
      <Button label="Secondary" className="p-button-secondary p-button-text" />
      <Button label="Success" className="p-button-success p-button-text" />
      <Button label="Info" className="p-button-info p-button-text" />
      <Button label="Warning" className="p-button-warning p-button-text" />
      <Button label="Help" className="p-button-help p-button-text" />
      <Button label="Danger" className="p-button-danger p-button-text" />
      <Button label="Plain" className="p-button-text p-button-plain" />
      <h5>Raised Text Buttons</h5>
      <Button label="Primary" className="p-button-raised p-button-text" />
      <Button
        label="Secondary"
        className="p-button-raised p-button-secondary p-button-text"
      />
      <Button
        label="Success"
        className="p-button-raised p-button-success p-button-text"
      />
      <Button
        label="Info"
        className="p-button-raised p-button-info p-button-text"
      />
      <Button
        label="Warning"
        className="p-button-raised p-button-warning p-button-text"
      />
      <Button
        label="Help"
        className="p-button-raised p-button-help p-button-text"
      />
      <Button
        label="Danger"
        className="p-button-raised p-button-danger p-button-text"
      />
      <Button
        label="Plain"
        className="p-button-raised p-button-text p-button-plain"
      />
      <h5>Outlined Buttons</h5>
      <Button label="Primary" className="p-button-outlined" />
      <Button
        label="Secondary"
        className="p-button-outlined p-button-secondary"
      />
      <Button label="Success" className="p-button-outlined p-button-success" />
      <Button label="Info" className="p-button-outlined p-button-info" />
      <Button label="Warning" className="p-button-outlined p-button-warning" />
      <Button label="Help" className="p-button-outlined p-button-help" />
      <Button label="Danger" className="p-button-outlined p-button-danger" />
      <h5>Rounded Icon Buttons</h5>
      <Button
        icon="pi pi-bookmark"
        className="p-button-rounded p-button-secondary"
      />
      <Button
        icon="pi pi-search"
        className="p-button-rounded p-button-success"
      />
      <Button icon="pi pi-user" className="p-button-rounded p-button-info" />
      <Button icon="pi pi-bell" className="p-button-rounded p-button-warning" />
      <Button icon="pi pi-heart" className="p-button-rounded p-button-help" />
      <Button icon="pi pi-times" className="p-button-rounded p-button-danger" />
      <Button icon="pi pi-check" className="p-button-rounded" />
      <h5>Rounded Text Icon Buttons</h5>
      <Button icon="pi pi-check" className="p-button-rounded p-button-text" />
      <Button
        icon="pi pi-bookmark"
        className="p-button-rounded p-button-secondary p-button-text"
      />
      <Button
        icon="pi pi-search"
        className="p-button-rounded p-button-success p-button-text"
      />
      <Button
        icon="pi pi-user"
        className="p-button-rounded p-button-info p-button-text"
      />
      <Button
        icon="pi pi-bell"
        className="p-button-rounded p-button-warning p-button-text"
      />
      <Button
        icon="pi pi-heart"
        className="p-button-rounded p-button-help p-button-text"
      />
      <Button
        icon="pi pi-times"
        className="p-button-rounded p-button-danger p-button-text"
      />
      <Button
        icon="pi pi-filter"
        className="p-button-rounded p-button-text p-button-plain"
      />
      <h5>Rounded and Outlined Icon Buttons</h5>
      <Button
        icon="pi pi-check"
        className="p-button-rounded p-button-outlined"
      />
      <Button
        icon="pi pi-bookmark"
        className="p-button-rounded p-button-secondary p-button-outlined"
      />
      <Button
        icon="pi pi-search"
        className="p-button-rounded p-button-success p-button-outlined"
      />
      <Button
        icon="pi pi-user"
        className="p-button-rounded p-button-info p-button-outlined"
      />
      <Button
        icon="pi pi-bell"
        className="p-button-rounded p-button-warning p-button-outlined"
      />
      <Button
        icon="pi pi-heart"
        className="p-button-rounded p-button-help p-button-outlined"
      />
      <Button
        icon="pi pi-times"
        className="p-button-rounded p-button-danger p-button-outlined"
      />
      <h5>Custom SVG Icon Buttons With Colors</h5>
      <Button icon="si-exel" label="엑셀" iconColor={'white'} />
      <Button
        icon="si-exel"
        label="엑셀"
        iconColor={'black'}
        className="p-button-outlined"
      />
      <Button
        icon="si-exel"
        label="엑셀"
        iconColor={'red'}
        className="p-button-danger p-button-outlined"
      />
      <h5>Badges</h5>
      <Button type="button" label="Emails" badge="8" />
      <Button
        type="button"
        label="Messages"
        icon="pi pi-users"
        className="p-button-warning"
        badge="8"
        badgeClassName="p-badge-danger"
      />
      <h5>Button Set</h5>
      <span className="p-buttonset">
        <Button label="Save" icon="pi pi-check" />
        <Button label="Delete" icon="pi pi-trash" />
        <Button label="Cancel" icon="pi pi-times" />
      </span>
      <h5>Sizes</h5>
      <div className="sizes">
        <Button label="Small" icon="pi pi-check" className="p-button-sm" />
        <Button label="Normal" icon="pi pi-check" className="p-button" />
        <Button label="Large" icon="pi pi-check" className="p-button-lg" />
      </div>
      <h5>Numbers</h5>
      <Badge value="2" className="p-mr-2" />
      <Badge value="8" severity="success" className="p-mr-2" />
      <Badge value="4" severity="info" className="p-mr-2" />
      <Badge value="12" severity="warning" className="p-mr-2" />
      <Badge value="3" severity="danger" />
      <h5 className="p-mb-4">Positioned Badge</h5>
      <i
        className="pi pi-bell p-mr-4 p-text-secondary p-overlay-badge"
        style={{ fontSize: '2rem' }}
      >
        <Badge value="2" />
      </i>
      <i
        className="pi pi-calendar p-mr-4 p-text-secondary p-overlay-badge"
        style={{ fontSize: '2rem' }}
      >
        <Badge value="10+" severity="danger" />
      </i>
      <i
        className="pi pi-envelope p-text-secondary p-overlay-badge"
        style={{ fontSize: '2rem' }}
      >
        <Badge severity="danger" />
      </i>
      <h5>Button Badge</h5>
      <Button type="button" label="Emails" className="p-mr-2">
        <Badge value="8" />
      </Button>
      <Button
        type="button"
        label="Messages"
        icon="pi pi-users"
        className="p-button-warning"
      >
        <Badge value="8" severity="danger" />
      </Button>
      <h5>Sizes</h5>
      <Badge value="2" className="p-mr-2" />
      <Badge value="4" className="p-mr-2" size="large" severity="warning" />
      <Badge value="6" size="xlarge" severity="success" />
      <h5>Tags</h5>
      <Tag className="p-mr-2" value="Primary" />
      <Tag className="p-mr-2" severity="success" value="Success" />
      <Tag className="p-mr-2" severity="info" value="Info" />
      <Tag className="p-mr-2" severity="warning" value="Warning" />
      <Tag severity="danger" value="Danger" />
      <h5>Pills</h5>
      <Tag className="p-mr-2" value="Primary" rounded />
      <Tag className="p-mr-2" severity="success" value="Success" rounded />
      <Tag className="p-mr-2" severity="info" value="Info" rounded />
      <Tag className="p-mr-2" severity="warning" value="Warning" rounded />
      <Tag severity="danger" value="Danger" rounded />
      <h5>Icons</h5>
      <Tag className="p-mr-2" icon="pi pi-user" value="Primary" />
      <Tag
        className="p-mr-2"
        icon="pi pi-check"
        severity="success"
        value="Success"
      />
      <Tag
        className="p-mr-2"
        icon="pi pi-info-circle"
        severity="info"
        value="Info"
      />
      <Tag
        className="p-mr-2"
        icon="pi pi-exclamation-triangle"
        severity="warning"
        value="Warning"
      />
      <Tag icon="pi pi-times" severity="danger" value="Danger" />
    </div>
  );
};

export default ButtonComponents;
