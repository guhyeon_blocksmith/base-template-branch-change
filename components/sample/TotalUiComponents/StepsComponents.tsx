import React, { FC, useState } from 'react';
import { Steps } from 'smith-ui-components/steps';

const StepsComponents: FC = () => {
  const stepItems = [
    { label: 'Personal' },
    { label: 'Seat' },
    { label: 'Payment' },
    { label: 'Confirmation' },
  ];

  const [activeIndex, setActiveIndex] = useState<number>();
  return (
    <div className="card">
      <h5>Steps</h5>
      <Steps
        model={stepItems}
        activeIndex={activeIndex}
        onSelect={(e) => setActiveIndex(e.index)}
        readOnly={false}
      />
    </div>
  );
};

export default StepsComponents;
