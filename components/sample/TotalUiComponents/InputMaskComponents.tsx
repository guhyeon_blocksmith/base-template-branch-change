import React, { FC, useState } from 'react';
import { InputMask } from 'smith-ui-components/inputmask';
import { InputText } from 'smith-ui-components/inputtext';
import { Password } from 'smith-ui-components/password';
import { TriStateCheckbox } from 'smith-ui-components/tristatecheckbox';

const InputMaskComponents: FC = () => {
  const [inputMaskValue, setInputMaskValue] = useState<string>();
  const [pwdValue, setPwdValue] = useState<string>();
  const [triStateValue, setTriStateValue] = useState<
    boolean | null | undefined
  >();
  return (
    <>
      <div className="p-col-12 p-lg-4">
        <div className="card">
          <h5>Input Mask</h5>
          <InputMask
            mask="99-999999"
            value={inputMaskValue}
            onChange={(e) => setInputMaskValue(e.value)}
          />
        </div>
      </div>
      <div className="p-col-12 p-md-8">
        <div className="card">
          <h5>KeyFilter</h5>
          <div className="p-grid p-fluid">
            <div className="p-field p-col-12 p-md-4">
              <label htmlFor="numbers">Numbers</label>
              <InputText id="numbers" keyfilter="num" />
            </div>
            <div className="p-field p-col-12 p-md-4">
              <label htmlFor="alpha">Alphabetic</label>
              <InputText id="alpha" keyfilter="alpha" />
            </div>
            <div className="p-field p-col-12 p-md-4">
              <label htmlFor="block">Block {`< > * !`}</label>
              <InputText id="block" keyfilter={/^[^<>*!]+$/} />
            </div>
          </div>
        </div>
      </div>
      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>PassWord</h5>
          <Password
            value={pwdValue}
            onChange={(e) => setPwdValue(e.target.value)}
          />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>TriStateCheckbox</h5>
          <TriStateCheckbox
            value={triStateValue}
            onChange={(e) => setTriStateValue(e.value)}
          />
          <label>{String(triStateValue)}</label>
        </div>
      </div>
    </>
  );
};

export default InputMaskComponents;
