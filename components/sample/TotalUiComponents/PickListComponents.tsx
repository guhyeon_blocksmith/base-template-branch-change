import React, { FC, useEffect, useState } from 'react';
import { PickList } from 'smith-ui-components/picklist';
import axios from 'axios';
import { ProductsType } from 'types/sample';
import styles from './PickListComponents.module.scss';

const PickListComponents: FC = () => {
  const [pickListSource, setPickListSource] = useState([]);
  const [pickListTarget, setPickListTarget] = useState([]);

  useEffect(() => {
    const pickListData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/products-small.json')
        .then((res) => res.data.data);
    };
    pickListData().then((data) => setPickListSource(data));
  }, []);

  const pickListItemTemplate = (item: ProductsType) => {
    return (
      <div className={styles['product-item']}>
        <div className={styles['image-container']}>
          <img
            src={`/sample/totaluicomponents/product/${item.image}`}
            onError={(e) =>
              (e.currentTarget.src =
                'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
            }
            alt={item.name}
          />
        </div>
        <div className={styles['product-list-detail']}>
          <h5 className="p-mb-2">{item.name}</h5>
          <i className={`pi pi-tag ${styles['product-category-icon']}`} />
          <span className={styles['product-category']}>{item.category}</span>
        </div>
        <div className={styles['product-list-action']}>
          <h6 className="p-mb-2">${item.price}</h6>
          <span
            className={`product-badge ${
              styles['status-' + item.inventoryStatus.toLowerCase()]
            }`}
          >
            {item.inventoryStatus}
          </span>
        </div>
      </div>
    );
  };

  const pickListonChange = (event: any) => {
    setPickListSource(event.source);
    setPickListTarget(event.target);
  };

  return (
    <div className={`card ${styles['p-pick-list']}`}>
      <h5>PickList</h5>
      <PickList
        source={pickListSource}
        target={pickListTarget}
        itemTemplate={pickListItemTemplate}
        onChange={pickListonChange}
      />
    </div>
  );
};

export default PickListComponents;
