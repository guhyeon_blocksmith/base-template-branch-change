import React, { FC, useState } from 'react';
import { Timeline } from 'smith-ui-components/timeline';
import { Card } from 'smith-ui-components/card';
import { ScrollPanel } from 'smith-ui-components/scrollpanel';
import { Sidebar } from 'smith-ui-components/sidebar';
import { Button } from 'smith-ui-components/button';
import { InputText } from 'smith-ui-components/inputtext';
import { FileUpload } from 'smith-ui-components/fileupload';

const TimeLineComponents: FC = () => {
  const [visible, setVisible] = useState<boolean>();
  const timeLineEvent = [
    {
      status: 'Ordered',
      date: '15/10/2020 10:30',
      icon: 'pi pi-shopping-cart',
      color: '#9C27B0',
      image: 'game-controller.jpg',
    },
    {
      status: 'Processing',
      date: '15/10/2020 14:00',
      icon: 'pi pi-cog',
      color: '#673AB7',
    },
    {
      status: 'Shipped',
      date: '15/10/2020 16:15',
      icon: 'pi pi-shopping-cart',
      color: '#FF9800',
    },
    {
      status: 'Delivered',
      date: '16/10/2020 10:00',
      icon: 'pi pi-check',
      color: '#607D8B',
    },
  ];
  return (
    <>
      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>TimeLine</h5>
          <Timeline value={timeLineEvent} content={(item) => item.status} />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <h5>Card</h5>
        <Card
          title="Simple Card"
          style={{ width: '25rem', marginBottom: '2em' }}
        >
          <p className="p-m-0" style={{ lineHeight: '1.5' }}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore
            sed consequuntur error repudiandae numquam deserunt quisquam
            repellat libero asperiores earum nam nobis, culpa ratione quam
            perferendis esse, cupiditate neque quas!
          </p>
        </Card>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Scroll Panel</h5>
          <ScrollPanel style={{ width: '100%', height: '200px' }}>
            The story begins as Don Vito Corleone, the head of a New York Mafia
            family, oversees his daughter's wedding. His beloved son Michael has
            just come home from the war, but does not intend to become part of
            his father's business. Through Michael's life the nature of the
            family business becomes clear. The business of the family is just
            like the head of the family, kind and benevolent to those who give
            respect, but given to ruthless violence whenever anything stands
            against the good of the family.
          </ScrollPanel>
        </div>
      </div>

      <div className="p-col-12 p-md-2">
        <div className="card">
          <h5>Sidebar</h5>
          <Sidebar onHide={() => setVisible(false)} visible={visible} />
          <Button
            icon="pi pi-arrow-right"
            onClick={() => {
              setVisible(true);
            }}
          />
        </div>
      </div>

      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Tooltip</h5>
          <InputText
            type="text"
            placeholder="Right"
            tooltip="Enter your username"
          />
        </div>
      </div>

      <div className="p-col-12 p-md-6">
        <div className="card">
          <h5>FileUpload</h5>
          <FileUpload name={'demo'} url={'./upload'} />
        </div>
      </div>
    </>
  );
};

export default TimeLineComponents;
