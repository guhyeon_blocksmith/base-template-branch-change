import React, { FC, useRef } from 'react';
import { Toast } from 'smith-ui-components/toast';
import { SplitButton } from 'smith-ui-components/splitbutton';

const SplitButtonComponents: FC = () => {
  const toastRef = useRef<Toast>(null);
  const splitBtnSave = () => {
    toastRef.current?.show({
      severity: 'success',
      summary: 'Success',
      detail: 'Data Saved',
    });
  };

  const splitBtnItems = [
    {
      label: 'Update',
      icon: 'pi pi-refresh',
      command: () => {
        toastRef.current?.show({
          severity: 'success',
          summary: 'Updated',
          detail: 'Data Updated',
        });
      },
    },
    {
      label: 'Delete',
      icon: 'pi pi-times',
      command: () => {
        toastRef.current?.show({
          severity: 'success',
          summary: 'Delete',
          detail: 'Data Deleted',
        });
      },
    },
    {
      label: 'React Website',
      icon: 'pi pi-external-link',
      command: () => {
        window.location.href = 'https://facebook.github.io/react/';
      },
    },
    {
      label: 'Upload',
      icon: 'pi pi-upload',
      command: () => {
        window.location.hash = '/fileupload';
      },
    },
  ];

  return (
    <div className="card">
      <Toast ref={toastRef} />

      <h5>Split Button</h5>
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        onClick={splitBtnSave}
        model={splitBtnItems}
      />

      <h5>Severities</h5>
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-mr-2"
      />
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-button-secondary p-mr-2"
      />
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-button-success p-mr-2"
      />
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-button-info p-mr-2"
      />
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-button-warning p-mr-2"
      />
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-button-help p-mr-2"
      />
      <SplitButton
        label="Save"
        icon="pi pi-plus"
        model={splitBtnItems}
        className="p-button-danger p-mr-2"
      />
    </div>
  );
};

export default SplitButtonComponents;
