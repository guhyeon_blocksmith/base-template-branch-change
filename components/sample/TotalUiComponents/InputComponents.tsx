import { InputText } from 'smith-ui-components/inputtext';
import { InputTextarea } from 'smith-ui-components/inputtextarea';
import { AutoComplete } from 'smith-ui-components/autocomplete';
import { Calendar } from 'smith-ui-components/calendar';
import { InputNumber } from 'smith-ui-components/inputnumber';
import { Chips } from 'smith-ui-components/chips';
import React, { FC, useEffect, useState } from 'react';
import axios from 'axios';
import { DropdownType } from 'types/sample';

const InputComponents: FC = () => {
  const [autoValue, setAutoValue] = useState<DropdownType[]>([]);

  const [floatValue, setFloatValue] = useState('');
  const [selectedAutoValue, setSelectedAutoValue] = useState('');
  const [autoFilteredValue, setAutoFilteredValue] = useState<DropdownType[]>(
    []
  );

  const [calendarValue, setCalendarValue] = useState<Date | Date[]>();
  const [inputNumberValue, setInputNumberValue] = useState();
  const [chipsValue, setChipsValue] = useState([]);

  useEffect(() => {
    const autoCompleteData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/countries.json')
        .then((res) => res.data.data);
    };
    autoCompleteData().then((data) => setAutoValue(data));
  }, []);

  const searchCountry = (event: { originalEvent: Event; query: string }) => {
    setTimeout(() => {
      let autoFilteredValues: DropdownType[];
      if (!event.query.trim().length) {
        autoFilteredValues = [...autoValue];
      } else {
        autoFilteredValues = autoValue.filter((country) => {
          return country.name
            .toLowerCase()
            .startsWith(event.query.toLowerCase());
        });
      }
      setAutoFilteredValue(autoFilteredValues);
    }, 250);
  };

  return (
    <div className="card">
      <h5>InputText</h5>
      <div className="p-grid p-formgrid">
        <div className="p-col-12 p-mb-2 p-lg-4 p-mb-lg-0">
          <InputText type="text" placeholder="Default" />
        </div>
        <div className="p-col-12 p-mb-2 p-lg-4 p-mb-lg-0">
          <InputText type="text" placeholder="Disabled" disabled />
        </div>
        <div className="p-col-12 p-mb-2 p-lg-4 p-mb-lg-0">
          <InputText type="text" placeholder="Invalid" className="p-invalid" />
          <small id="username2-help" className="p-error p-d-block">
            Username is not available.
          </small>
        </div>
      </div>

      <h5>Icons</h5>
      <div className="p-grid p-formgrid">
        <div className="p-col-12 p-mb-2 p-lg-4 p-mb-lg-0">
          <span className="p-input-icon-left">
            <i className="pi pi-user" />
            <InputText type="text" placeholder="Username" />
          </span>
        </div>
        <div className="p-col-12 p-mb-2 p-lg-4 p-mb-lg-0">
          <span className="p-input-icon-right">
            <InputText type="text" placeholder="Search" />
            <i className="pi pi-search" />
          </span>
        </div>
        <div className="p-col-12 p-mb-2 p-lg-4 p-mb-lg-0">
          <span className="p-input-icon-left p-input-icon-right">
            <i className="pi pi-user" />
            <InputText type="text" placeholder="Search" />
            <i className="pi pi-search" />
          </span>
        </div>
      </div>

      <h5>Float Label</h5>
      <span className="p-float-label">
        <InputText
          id="username"
          type="text"
          value={floatValue}
          onChange={(e) => setFloatValue(e.target.value)}
        />
        <label htmlFor="username">Username</label>
      </span>

      <h5>Textarea</h5>
      <InputTextarea placeholder="Your Message" autoResize rows="3" cols="30" />

      <h5>AutoComplete</h5>
      <AutoComplete
        placeholder="Search"
        dropdown
        multiple
        value={selectedAutoValue}
        suggestions={autoFilteredValue}
        completeMethod={searchCountry}
        field="name"
        onChange={(e) => setSelectedAutoValue(e.target.value)}
      />

      <h5>Calendar</h5>
      <Calendar
        showIcon
        showButtonBar
        value={calendarValue}
        onChange={(e) => setCalendarValue(e.value)}
      />

      <h5>InputNumber</h5>
      <InputNumber
        value={inputNumberValue}
        onValueChange={(e) => setInputNumberValue(e.target.value)}
        showButtons
        mode="decimal"
      />

      <h5>Chips</h5>
      <Chips value={chipsValue} onChange={(e) => setChipsValue(e.value)} />
    </div>
  );
};
export default InputComponents;
