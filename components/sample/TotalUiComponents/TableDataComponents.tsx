import React, { FC, useEffect, useState } from 'react';
import { DataTable } from 'smith-ui-components/datatable';
import { Column } from 'smith-ui-components/column';
import { InputText } from 'smith-ui-components/inputtext';
import { ProgressBar } from 'smith-ui-components/progressbar';
import { Button } from 'smith-ui-components/button';
import axios from 'axios';
import styles from './TableDataComponents.module.scss';
import { CustomerType } from 'types/sample';

const TableDataComponents: FC = () => {
  const [selectedCustomers, setSelectedCustomers] = useState();
  const [globalFilter, setGlobalFilter] = useState<string>();
  const [customers, setCustomers] = useState<CustomerType[]>([]);

  useEffect(() => {
    const customerData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/customers-large.json')
        .then((res) => res.data.data);
    };
    customerData().then((data) => setCustomers(data));
  }, []);

  const customerTableHeader = () => {
    return (
      <div className="p-d-flex p-ai-center p-jc-between">
        DataTable
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            type="search"
            onInput={(e) => setGlobalFilter(e.currentTarget.value)}
            placeholder="Global Search"
          />
        </span>
      </div>
    );
  };

  const nameBodyTemplate = (data: CustomerType) => {
    return (
      <>
        <span className={styles['p-column-title']}>Name</span>
        {data.name}
      </>
    );
  };

  const countryBodyTemplate = (data: CustomerType) => {
    return (
      <>
        <span className={styles['p-column-title']}>Country</span>
        <img
          alt={data.country.name}
          src={'/sample/totaluicomponents/flag_placeholder.png'}
          className={`flag flag-${data.country.code} p-mr-2`}
          width="30"
        />
        <span className="image-text">{data.country.name}</span>
      </>
    );
  };

  const dateBodyTemplate = (data: CustomerType) => {
    return (
      <>
        <span className={styles['p-column-title']}>Date</span>
        <span>{data.date}</span>
      </>
    );
  };

  const representativeBodyTemplate = (data: CustomerType) => {
    return (
      <>
        <span className={styles['p-column-title']}>Representative</span>
        <img
          alt={data.representative.name}
          src={`/sample/totaluicomponents/avatar/${data.representative.image}`}
          width="32"
          style={{ verticalAlign: 'middle' }}
        />
        <span className="image-text p-ml-2">{data.representative.name}</span>
      </>
    );
  };

  const statusBodyTemplate = (data: CustomerType) => {
    return (
      <>
        <span className={styles['p-column-title']}>Status</span>
        <span className={`customer-badge status-${data.status}`}>
          {data.status}
        </span>
      </>
    );
  };

  const activityBodyTemplate = (data: CustomerType) => {
    return (
      <>
        <span className="p-column-title">Activity</span>
        <ProgressBar value={data.activity} showValue={false} />
      </>
    );
  };

  const actionBodyTemplate = () => {
    return (
      <Button type="button" icon="pi pi-cog" className="p-button-secondary" />
    );
  };

  return (
    <div className="card">
      <h5>TableData</h5>
      <DataTable
        value={customers}
        paginator
        className={styles['p-datatable-customers']}
        rows={10}
        dataKey="id"
        rowHover
        selection={selectedCustomers}
        onSelectionChange={(e) => setSelectedCustomers(e.value)}
        globalFilter={globalFilter}
        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
        rowsPerPageOptions={[10, 25, 50]}
        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
        header={customerTableHeader}
        emptyMessage="No customers found."
      >
        <Column selectionMode="multiple" headerStyle={{ width: '3em' }} />
        <Column
          field="name"
          header="Name"
          body={nameBodyTemplate}
          sortable
          filter
        />
        <Column
          header="Country"
          body={countryBodyTemplate}
          sortable
          sortField="country.name"
          filter
          filterField="country.name"
          filterMatchMode="contains"
        />
        <Column
          header="Representative"
          body={representativeBodyTemplate}
          sortable
          sortField="representative.name"
          filter
          filterField="representative.name"
          filterMatchMode="in"
        />
        <Column
          field="date"
          header="Date"
          body={dateBodyTemplate}
          sortable
          filter
          filterMatchMode="equals"
        />
        <Column
          field="status"
          header="Status"
          body={statusBodyTemplate}
          sortable
          filter
          filterMatchMode="equals"
        />
        <Column
          field="activity"
          header="Activity"
          body={activityBodyTemplate}
          sortable
          filter
          filterMatchMode="gte"
        />
        <Column
          body={actionBodyTemplate}
          headerStyle={{ width: '8rem', textAlign: 'center' }}
          bodyStyle={{ textAlign: 'center', overflow: 'visible' }}
        />
      </DataTable>
    </div>
  );
};

export default TableDataComponents;
