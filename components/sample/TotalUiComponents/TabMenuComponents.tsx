import React, { FC } from 'react';
import { TabMenu } from 'smith-ui-components/tabmenu';
import { BreadCrumb } from 'smith-ui-components/breadcrumb';
import { PanelMenu } from 'smith-ui-components/panelmenu';
import { SlideMenu } from 'smith-ui-components/slidemenu';

const TabMenuComponents: FC = () => {
  const tabMenuItems = [
    { label: 'Home', icon: 'pi pi-fw pi-home' },
    { label: 'Calendar', icon: 'pi pi-fw pi-calendar' },
    { label: 'Edit', icon: 'pi pi-fw pi-pencil' },
    { label: 'Documentation', icon: 'pi pi-fw pi-file' },
    { label: 'Settings', icon: 'pi pi-fw pi-cog' },
  ];

  const breadCrumbItems = [
    { label: 'Categories' },
    { label: 'Sports' },
    { label: 'Football' },
    { label: 'Countries' },
    { label: 'Spain' },
    { label: 'F.C. Barcelona' },
    { label: 'Squad' },
    {
      label: 'Lionel Messi',
      url: 'https://en.wikipedia.org/wiki/Lionel_Messi',
    },
  ];
  const breadCrumbHome = {
    icon: 'pi pi-home',
    url: 'https://www.primefaces.org/primereact',
  };

  const panelMenuItems = [
    {
      label: 'File',
      icon: 'pi pi-fw pi-file',
      items: [
        {
          label: 'New',
          icon: 'pi pi-fw pi-plus',
          items: [
            {
              label: 'Bookmark',
              icon: 'pi pi-fw pi-bookmark',
            },
            {
              label: 'Video',
              icon: 'pi pi-fw pi-video',
            },
          ],
        },
        {
          label: 'Delete',
          icon: 'pi pi-fw pi-trash',
        },
        {
          label: 'Export',
          icon: 'pi pi-fw pi-external-link',
        },
      ],
    },
    {
      label: 'Edit',
      icon: 'pi pi-fw pi-pencil',
      items: [
        {
          label: 'Left',
          icon: 'pi pi-fw pi-align-left',
        },
        {
          label: 'Right',
          icon: 'pi pi-fw pi-align-right',
        },
        {
          label: 'Center',
          icon: 'pi pi-fw pi-align-center',
        },
        {
          label: 'Justify',
          icon: 'pi pi-fw pi-align-justify',
        },
      ],
    },
    {
      label: 'Users',
      icon: 'pi pi-fw pi-user',
      items: [
        {
          label: 'New',
          icon: 'pi pi-fw pi-user-plus',
        },
        {
          label: 'Delete',
          icon: 'pi pi-fw pi-user-minus',
        },
        {
          label: 'Search',
          icon: 'pi pi-fw pi-users',
          items: [
            {
              label: 'Filter',
              icon: 'pi pi-fw pi-filter',
              items: [
                {
                  label: 'Print',
                  icon: 'pi pi-fw pi-print',
                },
              ],
            },
            {
              icon: 'pi pi-fw pi-bars',
              label: 'List',
            },
          ],
        },
      ],
    },
    {
      label: 'Events',
      icon: 'pi pi-fw pi-calendar',
      items: [
        {
          label: 'Edit',
          icon: 'pi pi-fw pi-pencil',
          items: [
            {
              label: 'Save',
              icon: 'pi pi-fw pi-calendar-plus',
            },
            {
              label: 'Delete',
              icon: 'pi pi-fw pi-calendar-minus',
            },
          ],
        },
        {
          label: 'Archieve',
          icon: 'pi pi-fw pi-calendar-times',
          items: [
            {
              label: 'Remove',
              icon: 'pi pi-fw pi-calendar-minus',
            },
          ],
        },
      ],
    },
  ];

  return (
    <>
      <div className="p-col-12">
        <div className="card">
          <h5>TabMenu</h5>
          <TabMenu model={tabMenuItems} />
        </div>
      </div>

      <div className="p-col-12">
        <div className="card">
          <h5>Breadcrumb</h5>
          <BreadCrumb model={breadCrumbItems} home={breadCrumbHome} />
        </div>
      </div>

      <div className="p-col-12 p-md-6">
        <div className="card">
          <h5>PanelMenu</h5>
          <PanelMenu model={panelMenuItems} style={{ width: '300px' }} />
        </div>
      </div>

      <div className="p-col-12 p-md-6">
        <div className="card">
          <h5>Slide Menu</h5>
          <SlideMenu model={panelMenuItems} />
        </div>
      </div>
    </>
  );
};

export default TabMenuComponents;
