import React, { FC, useState } from 'react';
import { Dialog } from 'smith-ui-components/dialog';
import { Button } from 'smith-ui-components/button';

const DialogComponents: FC = () => {
  const [display, setDisplay] = useState<boolean>(false);
  const open = () => {
    setDisplay(true);
  };
  const close = () => {
    setDisplay(false);
  };
  return (
    <div className="card">
      <h5>Dialog</h5>
      <Dialog
        visible={display}
        style={{ width: '30vw' }}
        modal
        onHide={close}
        header="Dialog"
        footer={
          <Button
            label="Dismiss"
            onClick={close}
            icon="pi pi-check"
            className="p-button-secondary"
          />
        }
      >
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </Dialog>
      <Button label="Show" icon="pi pi-external-link" onClick={open} />
    </div>
  );
};

export default DialogComponents;
