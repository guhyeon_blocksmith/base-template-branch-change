import React, { FC, useEffect, useState } from 'react';
import { Tree } from 'smith-ui-components/tree';
import { TreeTable } from 'smith-ui-components/treetable';
import { Column } from 'smith-ui-components/column';
import TreeNode from 'smith-ui-components/components/treenode/TreeNode';
import axios from 'axios';

const TreeComponents: FC = () => {
  const [treeTableValue, setTreeTableValue] = useState();

  useEffect(() => {
    const treeTableData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/treeTable.json')
        .then((res) => res.data.data);
    };
    treeTableData().then((data) => setTreeTableValue(data));
  }, []);

  const treeData: TreeNode[] = [
    {
      key: '0',
      label: 'Documents',
      data: 'Documents Folder',
      icon: 'pi pi-fw pi-inbox',
      children: [
        {
          key: '0-0',
          label: 'Work',
          data: 'Work Folder',
          icon: 'pi pi-fw pi-cog',
          children: [
            {
              key: '0-0-0',
              label: 'Expenses.doc',
              icon: 'pi pi-fw pi-file',
              data: 'Expenses Document',
            },
            {
              key: '0-0-1',
              label: 'Resume.doc',
              icon: 'pi pi-fw pi-file',
              data: 'Resume Document',
            },
          ],
        },
        {
          key: '0-1',
          label: 'Home',
          data: 'Home Folder',
          icon: 'pi pi-fw pi-home',
          children: [
            {
              key: '0-1-0',
              label: 'Invoices.txt',
              icon: 'pi pi-fw pi-file',
              data: 'Invoices for this month',
            },
          ],
        },
      ],
    },
    {
      key: '1',
      label: 'Events',
      data: 'Events Folder',
      icon: 'pi pi-fw pi-calendar',
      children: [
        {
          key: '1-0',
          label: 'Meeting',
          icon: 'pi pi-fw pi-calendar-plus',
          data: 'Meeting',
        },
        {
          key: '1-1',
          label: 'Product Launch',
          icon: 'pi pi-fw pi-calendar-plus',
          data: 'Product Launch',
        },
        {
          key: '1-2',
          label: 'Report Review',
          icon: 'pi pi-fw pi-calendar-plus',
          data: 'Report Review',
        },
      ],
    },
    {
      key: '2',
      label: 'Movies',
      data: 'Movies Folder',
      icon: 'pi pi-fw pi-star',
      children: [
        {
          key: '2-0',
          icon: 'pi pi-fw pi-star',
          label: 'Al Pacino',
          data: 'Pacino Movies',
          children: [
            {
              key: '2-0-0',
              label: 'Scarface',
              icon: 'pi pi-fw pi-video',
              data: 'Scarface Movie',
            },
            {
              key: '2-0-1',
              label: 'Serpico',
              icon: 'pi pi-fw pi-video',
              data: 'Serpico Movie',
            },
          ],
        },
        {
          key: '2-1',
          label: 'Robert De Niro',
          icon: 'pi pi-fw pi-star',
          data: 'De Niro Movies',
          children: [
            {
              key: '2-1-0',
              label: 'Goodfellas',
              icon: 'pi pi-fw pi-video',
              data: 'Goodfellas Movie',
            },
            {
              key: '2-1-1',
              label: 'Untouchables',
              icon: 'pi pi-fw pi-video',
              data: 'Untouchables Movie',
            },
          ],
        },
      ],
    },
  ];
  return (
    <>
      <div className="p-col-12 p-md-4">
        <div className="card">
          <h5>Tree</h5>
          <Tree value={treeData} />
        </div>
      </div>

      <div className="p-col-12 p-md-8">
        <div className="card">
          <h5>Tree Table</h5>
          <TreeTable value={treeTableValue}>
            <Column field="name" header="Name" expander />
            <Column field="size" header="Size" />
            <Column field="type" header="Type" />
          </TreeTable>
        </div>
      </div>
    </>
  );
};

export default TreeComponents;
