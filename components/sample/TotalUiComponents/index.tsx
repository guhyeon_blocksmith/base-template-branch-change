import React from 'react';
import InputComponents from 'components/sample/TotalUiComponents/InputComponents';
import SliderComponents from 'components/sample/TotalUiComponents/SliderComponents';
import RadioButtonComponents from 'components/sample/TotalUiComponents/RadioButtonComponents';
import ListBoxComponents from 'components/sample/TotalUiComponents/ListBoxComponents';
import ButtonComponents from 'components/sample/TotalUiComponents/ButtonComponents';
import TableDataComponents from 'components/sample/TotalUiComponents/TableDataComponents';
import AccordionPanelComponents from 'components/sample/TotalUiComponents/AccordionPanelComponents';
import PanelComponents from 'components/sample/TotalUiComponents/PanelComponents';
import OverlayPanelComponents from 'components/sample/TotalUiComponents/OverlayPanelComponents';
import DialogComponents from 'components/sample/TotalUiComponents/DialogComponents';
import MenubarComponents from 'components/sample/TotalUiComponents/MenubarComponents';
import MessagesComponents from 'components/sample/TotalUiComponents/MessagesComponents';
import EditorComponents from 'components/sample/TotalUiComponents/EditorComponents';
import InputMaskComponents from 'components/sample/TotalUiComponents/InputMaskComponents';
import SplitButtonComponents from 'components/sample/TotalUiComponents/SplitButtonComponents';
import DataViewComponents from 'components/sample/TotalUiComponents/DataViewComponents';
import DataScrollerComponents from 'components/sample/TotalUiComponents/DataScrollerComponents';
import OrderListComponents from 'components/sample/TotalUiComponents/OrderListComponents';
import OrganizationChartComponents from 'components/sample/TotalUiComponents/OrganizationChartComponents';
import PickListComponents from 'components/sample/TotalUiComponents/PickListComponents';
import TreeComponents from 'components/sample/TotalUiComponents/TreeComponents';
import TimeLineComponents from 'components/sample/TotalUiComponents/TimeLineComponents';
import TabMenuComponents from 'components/sample/TotalUiComponents/TabMenuComponents';
import StepsComponents from 'components/sample/TotalUiComponents/StepsComponents';
import MediaComponents from 'components/sample/TotalUiComponents/MediaComponents';
import InPlaceComponents from 'components/sample/TotalUiComponents/InPlaceComponents';
import ChartComponents from 'components/sample/TotalUiComponents/ChartComponents';
import RippleComponents from 'components/sample/TotalUiComponents/RippleComponents';

/**
 * 전체 UI 컴포넌트
 * @constructor
 */
const TotalUiComponents = () => {
  return (
    <>
      <div className="p-grid">
        <div className="p-col-12 p-md-6 p-fluid">
          <InputComponents />
          <SliderComponents />
        </div>

        <div className="p-col-12 p-md-6">
          <RadioButtonComponents />
          <ListBoxComponents />
        </div>

        <div className="p-col-12">
          <ButtonComponents />
        </div>

        <div className="p-col-12">
          <TableDataComponents />
        </div>

        <div className="p-col-12 p-md-6">
          <AccordionPanelComponents />
        </div>
        <div className="p-col-12 p-md-6">
          <PanelComponents />
        </div>
        <div className="p-col-6">
          <OverlayPanelComponents />
        </div>

        <div className="p-col-6 p-fluid">
          <DialogComponents />
        </div>

        <MenubarComponents />
        <MessagesComponents />
        <EditorComponents />
        <InputMaskComponents />

        <div className="p-col-12">
          <SplitButtonComponents />
        </div>

        <div className="p-col-12">
          <DataViewComponents />
        </div>

        <div className="p-col-12">
          <DataScrollerComponents />
        </div>

        <div className="p-col-12">
          <OrderListComponents />
        </div>

        <div className="p-col-12">
          <OrganizationChartComponents />
        </div>

        <div className="p-col-12">
          <PickListComponents />
        </div>

        <TreeComponents />
        <TimeLineComponents />
        <TabMenuComponents />

        <div className="p-col-12">
          <StepsComponents />
        </div>

        <MediaComponents />
        <InPlaceComponents />
        <ChartComponents />
        <RippleComponents />
      </div>
    </>
  );
};

export default TotalUiComponents;
