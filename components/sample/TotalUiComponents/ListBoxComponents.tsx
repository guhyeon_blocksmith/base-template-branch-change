import React, { FC, useState } from 'react';
import { ListBox } from 'smith-ui-components/listbox';
import { Dropdown } from 'smith-ui-components/dropdown';
import { MultiSelect } from 'smith-ui-components/multiselect';
import { ToggleButton } from 'smith-ui-components/togglebutton';
import { SelectButton } from 'smith-ui-components/selectbutton';
import styles from './ListBoxComponents.module.css';
import { DropdownType } from 'types/sample';

const ListBoxComponents: FC = () => {
  const [listBoxValue, setListBoxValue] = useState<DropdownType>();
  const listBoxValues: DropdownType[] = [
    { name: 'New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'Istanbul', code: 'IST' },
    { name: 'Paris', code: 'PRS' },
  ];
  const [dropdownValue, setDropdownValue] = useState<DropdownType>();

  const dropdownValues: DropdownType[] = [
    { name: 'New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'Istanbul', code: 'IST' },
    { name: 'Paris', code: 'PRS' },
  ];
  const [multiselectValue, setMultiselectValue] = useState<DropdownType>();

  const multiselectValues: DropdownType[] = [
    { name: 'Australia', code: 'AU' },
    { name: 'Brazil', code: 'BR' },
    { name: 'China', code: 'CN' },
    { name: 'Egypt', code: 'EG' },
    { name: 'France', code: 'FR' },
    { name: 'Germany', code: 'DE' },
    { name: 'India', code: 'IN' },
    { name: 'Japan', code: 'JP' },
    { name: 'Spain', code: 'ES' },
    { name: 'United States', code: 'US' },
  ];

  const [toggleValue, setToggleValue] = useState<boolean>(false);
  const [selectButtonValue1, setSelectButtonValue1] = useState<DropdownType>();
  const selectButtonValues1: DropdownType[] = [
    { name: 'Option 1', code: 'O1' },
    { name: 'Option 2', code: 'O2' },
    { name: 'Option 3', code: 'O3' },
  ];
  const [selectButtonValue2, setSelectButtonValue2] = useState<DropdownType>();
  const selectButtonValues2: DropdownType[] = [
    { name: 'Option 1', code: 'O1' },
    { name: 'Option 2', code: 'O2' },
    { name: 'Option 3', code: 'O3' },
  ];

  const countryTemplate = (option: DropdownType) => {
    return (
      <div className="country-item p-d-flex p-ai-center">
        <span
          className={`p-mr-2 ${styles.flag} ${
            styles['flag-' + option.code.toLowerCase()]
          }`}
          style={{ width: '18px', height: '12px' }}
        />
        <div>{option.name}</div>
      </div>
    );
  };

  return (
    <>
      <div className="card p-fluid">
        <h5>ListBox</h5>
        <ListBox
          value={listBoxValue}
          options={listBoxValues}
          onChange={(e) => setListBoxValue(e.value)}
          optionLabel="name"
          filter
        />

        <h5>Dropdown</h5>
        <Dropdown
          value={dropdownValue}
          options={dropdownValues}
          onChange={(e) => setDropdownValue(e.value)}
          optionLabel="name"
          placeholder="Select"
        />

        <h5>MultiSelect</h5>
        <MultiSelect
          value={multiselectValue}
          options={multiselectValues}
          onChange={(e) => setMultiselectValue(e.value)}
          optionLabel="name"
          placeholder="Select Countries"
          filter
          className={'multiselect-custom'}
          itemTemplate={countryTemplate}
        />
      </div>
      <div className="card p-fluid">
        <h5>ToggleButton</h5>
        <ToggleButton
          checked={toggleValue}
          onChange={(e) => setToggleValue(e.value)}
          onLabel="Yes"
          offLabel="No"
        />

        <h5>SelectButton</h5>
        <SelectButton
          value={selectButtonValue1}
          options={selectButtonValues1}
          onChange={(e) => setSelectButtonValue1(e.value)}
          optionLabel="name"
        />

        <h5>SelectButton - Multiple</h5>
        <SelectButton
          value={selectButtonValue2}
          options={selectButtonValues2}
          onChange={(e) => setSelectButtonValue2(e.value)}
          optionLabel="name"
          multiple
        />
      </div>{' '}
    </>
  );
};

export default ListBoxComponents;
