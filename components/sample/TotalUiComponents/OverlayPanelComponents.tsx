import React, { FC, useRef } from 'react';
import { Button } from 'smith-ui-components/button';
import { isServer } from 'libs/common';
import { OverlayPanel } from 'smith-ui-components/overlaypanel';

const OverlayPanelComponents: FC = () => {
  const opRef = useRef<OverlayPanel>(null);
  const toggle = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    opRef.current?.toggle(event);
  };

  return (
    <div className="card p-fluid">
      <h5>Overlay Panel</h5>
      <Button
        type="button"
        label="Image"
        onClick={toggle}
        className="p-button-success"
      />
      <OverlayPanel
        ref={opRef}
        appendTo={isServer ? null : document.body}
        showCloseIcon
      >
        <img src={'/sample/components/logo.svg'} alt="Logo" />
      </OverlayPanel>
    </div>
  );
};

export default OverlayPanelComponents;
