import React, { FC, useEffect, useState } from 'react';
import { Carousel } from 'smith-ui-components/carousel';
import { Galleria } from 'smith-ui-components/galleria';
import { Button } from 'smith-ui-components/button';
import axios from 'axios';
import { GalleriaType, ProductsType } from 'types/sample';
import styles from './MediaComponents.module.scss';

const MediaComponents: FC = () => {
  const [galleriaImages, setGalleriaImages] = useState();
  const [mediaValue, setMediaValue] = useState([]);

  useEffect(() => {
    const mediaData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/products-small.json')
        .then((res) => res.data.data);
    };
    mediaData().then((data) => setMediaValue(data.slice(0, 9)));

    const galleriaData = async () => {
      return await axios
        .get('/sample/totaluicomponents/data/photos.json')
        .then((res) => res.data.data);
    };
    galleriaData().then((data) => setGalleriaImages(data));
  }, []);

  const responsiveOptions: {
    breakpoint: string;
    numVisible: number;
    numScroll: number;
  }[] = [
    {
      breakpoint: '1024px',
      numVisible: 3,
      numScroll: 3,
    },
    {
      breakpoint: '600px',
      numVisible: 2,
      numScroll: 2,
    },
    {
      breakpoint: '480px',
      numVisible: 1,
      numScroll: 1,
    },
  ];

  const productTemplate = (product: ProductsType) => {
    return (
      <div className={styles['product-item']}>
        <div className={styles['product-item-content']}>
          <div className="p-mb-3">
            <img
              src={`/sample/totaluicomponents/product/${product.image}`}
              onError={(e) =>
                (e.currentTarget.src =
                  'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
              }
              alt={product.name}
              className={styles['product-image']}
            />
          </div>
          <div>
            <h4 className="p-mb-1">{product.name}</h4>
            <h6 className="p-mt-0 p-mb-3">${product.price}</h6>
            <span
              className={`product-badge ${
                styles['status-' + product.inventoryStatus.toLowerCase()]
              }`}
            >
              {product.inventoryStatus}
            </span>
            <div className="car-buttons p-mt-5">
              <Button
                icon="pi pi-search"
                className="p-button p-button-rounded p-mr-2"
              />
              <Button
                icon="pi pi-star"
                className="p-button-success p-button-rounded p-mr-2"
              />
              <Button
                icon="pi pi-cog"
                className="p-button-help p-button-rounded"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  const galleriaItemTemplate = (item: GalleriaType) => {
    return (
      <img src={item.itemImageSrc} alt={item.alt} style={{ width: '100%' }} />
    );
  };

  const galleriaThumbnailTemplate = (item: GalleriaType) => {
    return <img src={item.thumbnailImageSrc} alt={item.alt} />;
  };

  return (
    <>
      <div className="p-col-12 p-md-6">
        <div className={`card ${styles['p-carousel']}`}>
          <h5>MEDIA - Carousel</h5>
          <Carousel
            value={mediaValue}
            numVisible={3}
            numScroll={3}
            responsiveOptions={responsiveOptions}
            itemTemplate={productTemplate}
          />
        </div>
      </div>

      <div className="p-col-12 p-md-6">
        <div className={`card ${styles['p-galleria']}`}>
          <h5>MEDIA - Galleria</h5>
          <Galleria
            value={galleriaImages}
            numVisible={5}
            style={{ maxWidth: '640px' }}
            item={galleriaItemTemplate}
            thumbnail={galleriaThumbnailTemplate}
          />
        </div>
      </div>
    </>
  );
};

export default MediaComponents;
