import React, { FC, useState } from 'react';
import { RadioButton } from 'smith-ui-components/radiobutton';
import { Checkbox } from 'smith-ui-components/checkbox';

const RadioButtonComponents: FC = () => {
  const [radioValue, setRadioValue] = useState();
  const [checkboxValues, setCheckboxValues] = useState<string[]>([]);

  const onCheckboxOptionChange = (e: {
    originalEvent: Event;
    value: any;
    checked: boolean;
    target: {
      type: string;
      name: string;
      id: string;
      value: any;
      checked: boolean;
    };
  }) => {
    const checkboxValue = [...checkboxValues];
    if (e.checked) {
      checkboxValue.push(e.value);
    } else {
      checkboxValue.splice(checkboxValues.indexOf(e.value), 1);
    }

    setCheckboxValues(checkboxValue);
  };
  return (
    <div className="card">
      <h5>RadioButton</h5>
      <div className="p-grid">
        <div className="p-col-12 p-md-4">
          <div className="p-field-radiobutton">
            <RadioButton
              inputId="option1"
              name="option"
              value="Option 1"
              onChange={(e) => setRadioValue(e.value)}
              checked={radioValue === 'Option 1'}
            />
            <label htmlFor="option1">Option 1</label>
          </div>
        </div>
        <div className="p-col-12 p-md-4">
          <div className="p-field-radiobutton">
            <RadioButton
              inputId="option2"
              name="option"
              value="Option 2"
              onChange={(e) => setRadioValue(e.value)}
              checked={radioValue === 'Option 2'}
            />
            <label htmlFor="option2">Option 2</label>
          </div>
        </div>
        <div className="p-col-12 p-md-4">
          <div className="p-field-radiobutton">
            <RadioButton
              inputId="option3"
              name="option"
              value="Option 3"
              onChange={(e) => setRadioValue(e.value)}
              checked={radioValue === 'Option 3'}
            />
            <label htmlFor="option3">Option 3</label>
          </div>
        </div>
      </div>

      <h5>Checkbox</h5>
      <div className="p-grid">
        <div className="p-col-12 p-md-4">
          <div className="p-field-checkbox">
            <Checkbox
              inputId="checkOption1"
              name="option"
              value="Option 1"
              onChange={onCheckboxOptionChange}
              checked={checkboxValues.indexOf('Option 1') !== -1}
            />
            <label htmlFor="checkOption1">Option 1</label>
          </div>
        </div>
        <div className="p-col-12 p-md-4">
          <div className="p-field-checkbox">
            <Checkbox
              inputId="checkOption2"
              name="option"
              value="Option 2"
              onChange={onCheckboxOptionChange}
              checked={checkboxValues.indexOf('Option 2') !== -1}
            />
            <label htmlFor="checkOption2">Option 2</label>
          </div>
        </div>
        <div className="p-col-12 p-md-4">
          <div className="p-field-checkbox">
            <Checkbox
              inputId="checkOption3"
              name="option"
              value="Option 3"
              onChange={onCheckboxOptionChange}
              checked={checkboxValues.indexOf('Option 3') !== -1}
            />
            <label htmlFor="checkOption3">Option 3</label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RadioButtonComponents;
