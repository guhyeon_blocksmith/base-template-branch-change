import React, { FC, useState } from 'react';
import { Editor } from 'smith-ui-components/editor';
import { ColorPicker } from 'smith-ui-components/colorpicker';

const EditorComponents: FC = () => {
  const [color1, setColor1] = useState();
  const [color2, setColor2] = useState();
  const [editorText, setEditorText] = useState<string>();

  return (
    <>
      <div className="p-col-12 p-lg-8">
        <div className="card">
          <h5>Editor</h5>
          <Editor
            style={{ height: '320px' }}
            value={editorText}
            onTextChange={(e) => setEditorText(e.htmlValue || '')}
          />
        </div>
      </div>
      <div className="p-col-12 p-lg-4">
        <div className="card">
          <h5>ColorPicker Inline</h5>
          <ColorPicker
            value={color1}
            onChange={(e) => setColor1(e.value)}
            inline
          />

          <h5>ColorPicker Overlay</h5>
          <ColorPicker value={color2} onChange={(e) => setColor2(e.value)} />
        </div>
      </div>
    </>
  );
};

export default EditorComponents;
