import Link from 'next/link';
import React from 'react';

/**
 * mobx 샘플 navigation
 * @constructor
 */
const Navigations = () => {
  return (
    <ul>
      <li>
        <Link href="/sample/mobx">index</Link>
      </li>
      <li>
        <Link href="/sample/mobx/ssg">ssg</Link>
      </li>
      <li>
        <Link href="/sample/mobx/ssr">ssr</Link>
      </li>
    </ul>
  );
};

export default Navigations;
