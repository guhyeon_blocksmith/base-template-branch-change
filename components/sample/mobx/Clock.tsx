import React from 'react';
import { observer } from 'mobx-react';

interface ClockProps {
  light: boolean;
  timeString: string;
}

/**
 * 시계 컴포넌트. 상태관리 mobx 예시
 * @param props
 * @constructor
 */
const Clock = observer((props: ClockProps) => {
  return (
    <div className={props.light ? 'light' : ''}>
      {props.timeString}
      <style jsx>{`
        div {
          padding: 15px;
          color: #82fa58;
          display: inline-block;
          font: 50px menlo, monaco, monospace;
          background-color: #000;
        }

        .light {
          background-color: #999;
        }
      `}</style>
    </div>
  );
});

export default Clock;
