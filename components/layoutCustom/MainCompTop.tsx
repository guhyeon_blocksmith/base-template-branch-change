import React, { FC } from 'react';
import { Button } from 'smith-ui-components/button';

/**
 * 커스텀 페이지 더미 컴포넌트
 * @constructor
 */
const MainCompTop: FC = () => {
  return (
    <div className="hero-full-container background-image-container white-text-container">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h1>Boilerplates</h1>
            <p>WELCOME TO MOUNTAIN WEBSITE. AGENCY IN BERLIN.</p>
            <br />
            <Button className="btn btn-default btn-lg">Discover</Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainCompTop;
