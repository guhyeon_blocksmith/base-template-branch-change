import React, { FC } from 'react';

// FNB (Foot Navigation Bar)
/**
 * 커스텀 Footer<br/>
 * .env에 NEXT_PUBLIC_IS_CUSTOM_LAYOUT이 true인 경우 렌더링되는 커스텀 컴포넌트입니다.<br/>
 * 마음대로 수정 가능
 * @constructor
 */
const CustomFooter: FC = () => {
  return (
    <footer className="footer-container white-text-container">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h3>BlockSmith</h3>

            <div className="row">
              <div className="col-xs-12 col-sm-7">
                <p>
                  <small>
                    Website created with{' '}
                    <a
                      href="http://www.mashup-template.com/"
                      title="Create website with free html template"
                    >
                      BlockSmith Template
                    </a>
                    /
                    <a
                      href="https://www.unsplash.com/"
                      title="Beautiful Free Images"
                    >
                      Unsplash
                    </a>
                  </small>
                </p>
              </div>
              <div className="col-xs-12 col-sm-5">
                <p className="text-right">
                  <a
                    href="https://facebook.com/"
                    className="social-round-icon white-round-icon fa-icon"
                    title=""
                  >
                    <i className="fa fa-facebook" aria-hidden="true" />
                  </a>
                  <a
                    href="https://twitter.com/"
                    className="social-round-icon white-round-icon fa-icon"
                    title=""
                  >
                    <i className="fa fa-twitter" aria-hidden="true" />
                  </a>
                  <a
                    href="https://www.linkedin.com/"
                    className="social-round-icon white-round-icon fa-icon"
                    title=""
                  >
                    <i className="fa fa-linkedin" aria-hidden="true" />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default CustomFooter;
