import React, { FC } from 'react';
import MainCompTop from 'components/layoutCustom/MainCompTop';
import MainCompMid01 from 'components/layoutCustom/MainCompMid01';
import MainCompMid02 from 'components/layoutCustom/MainCompMid02';

/**
 * 커스텀 컨텐츠<br/>
 * .env에 NEXT_PUBLIC_IS_CUSTOM_LAYOUT이 true인 경우 렌더링되는 커스텀 컨텐츠입니다.<br/>
 * 마음대로 수정 가능
 * @constructor
 */
const CustomContents: FC = () => {
  return (
    <>
      <MainCompTop />

      <MainCompMid01 />

      <MainCompMid02 />
    </>
  );
};

export default CustomContents;
