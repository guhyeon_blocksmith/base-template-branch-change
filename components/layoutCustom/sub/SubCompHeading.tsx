import { FC } from 'react';

const SubCompHeading: FC = () => {
  return (
    <div className="template-example">
      <h2 className="template-title-example">Headings</h2>

      <table className="table table-bordered">
        <tr>
          <td>
            <h1>H1: Heading 1</h1>
          </td>
        </tr>
        <tr>
          <td>
            <h2>H2: Heading 2</h2>
          </td>
        </tr>
        <tr>
          <td>
            <h3>H3: Heading 3</h3>
          </td>
        </tr>
        <tr>
          <td>
            <h4>H4: Heading 4</h4>
          </td>
        </tr>
      </table>
    </div>
  );
};

export default SubCompHeading;
