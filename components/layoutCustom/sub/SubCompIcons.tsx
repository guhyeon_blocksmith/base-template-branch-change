import React, { FC } from 'react';
import { Button } from 'smith-ui-components/button';

const SubCompIcons: FC = () => {
  return (
    <div className="template-example">
      <h2 className="template-title-example">Icons</h2>

      <div className="row">
        <div className="col-md-6">
          <h3 className="template-title-example">Social Round</h3>
          <ul className="list-icons">
            <li>
              <Button className="social-round-icon fa-icon">
                <i className="fa fa-facebook" />
              </Button>
            </li>
            <li>
              <Button className="social-round-icon fa-icon">
                <i className="fa fa-twitter" />
              </Button>
            </li>
            <li>
              <Button className="social-round-icon fa-icon">
                <i className="fa fa-linkedin" />
              </Button>
            </li>
            <li>
              <Button className="social-round-icon fa-icon">
                <i className="fa fa-instagram" />
              </Button>
            </li>
          </ul>
        </div>
        <div className="col-md-6">
          <h3 className="template-title-example">Basic Icons</h3>
          <ul className="list-icons">
            <li>
              <span className="fa-icon">
                <i className="fa fa-facebook" />
              </span>
            </li>
            <li>
              <span className="fa-icon">
                <i className="fa fa-twitter" />
              </span>
            </li>
            <li>
              <span className="fa-icon">
                <i className="fa fa-linkedin" />
              </span>
            </li>
            <li>
              <span className="fa-icon">
                <i className="fa fa-instagram" />
              </span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default SubCompIcons;
