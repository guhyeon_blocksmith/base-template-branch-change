import React, { FC } from 'react';
import { Dropdown } from 'smith-ui-components/dropdown';

const SubCompInputsDropdown: FC = () => {
  const selSet = [
    { label: '바', value: '1' },
    { label: '꿨', value: '2' },
    { label: '습', value: '3' },
    { label: '니', value: '4' },
    { label: '다', value: '5' },
  ];

  return (
    <div className="form-group">
      <Dropdown options={selSet} placeholder="Custom dropdown" />
    </div>
  );
};

export default SubCompInputsDropdown;
