import React from 'react';
import { useState } from 'react';
import { FC } from 'react';
import { Checkbox } from 'smith-ui-components/checkbox';

const SubCompInputsCheckbox: FC = () => {
  const [checkMe, setCheckMe] = useState({
    chk1: false,
    chk2: false,
    chk3: false,
  });
  const checkBoom = (val: string) => {
    switch (val) {
      case 'chk1':
        setCheckMe({ ...checkMe, chk1: !checkMe.chk1 });
        break;
      case 'chk2':
        setCheckMe({ ...checkMe, chk2: !checkMe.chk2 });
        break;
      case 'chk3':
        setCheckMe({ ...checkMe, chk3: !checkMe.chk3 });
        break;
    }
  };

  return (
    <div className="form-group">
      <label htmlFor="inlineCheckbox1" className="checkbox-inline">
        <Checkbox
          inputId="inlineCheckbox1"
          value="option1"
          onChange={() => checkBoom('chk1')}
          checked={checkMe.chk1}
        />
        Email me a copy
      </label>
      <label htmlFor="inlineCheckbox2" className="checkbox-inline">
        <Checkbox
          inputId="inlineCheckbox2"
          value="option2"
          onChange={() => checkBoom('chk2')}
          checked={checkMe.chk2}
        />
        I am a human
      </label>
      <label htmlFor="inlineCheckbox3" className="checkbox-inline">
        <Checkbox
          inputId="inlineCheckbox3"
          value="option3"
          onChange={() => checkBoom('chk3')}
          checked={checkMe.chk3}
        />
        Copy
      </label>
    </div>
  );
};

export default SubCompInputsCheckbox;
