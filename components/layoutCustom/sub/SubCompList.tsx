import { FC } from 'react';

const SubCompList: FC = () => {
  return (
    <div className="template-example">
      <h2 className="template-title-example">List</h2>
      <div className="row">
        <div className="col-md-6">
          <h3 className="template-title-example">Ordered</h3>
          <ul>
            <li>Consectetur adipiscing elit</li>
            <li>Integer molestie lorem at massa</li>
            <li>Facilisis in pretium nisl aliquet</li>
            <li>Nulla volutpat aliquam velit</li>
          </ul>
        </div>
        <div className="col-md-6">
          <h3 className="template-title-example">Number</h3>
          <ol>
            <li>Consectetur adipiscing elit</li>
            <li>Integer molestie lorem at massa</li>
            <li>Facilisis in pretium nisl aliquet</li>
            <li>Nulla volutpat aliquam velit</li>
          </ol>
        </div>
      </div>
    </div>
  );
};

export default SubCompList;
