import React from 'react';
import { FC } from 'react';
import { InputText } from 'smith-ui-components/inputtext';
import { InputTextarea } from 'smith-ui-components/inputtextarea';
import SubCompInputsCheckbox from './SubCompInputsCheckbox';
import SubCompInputsRadio from './SubCompInputsRadio';
import SubCompInputsDropdown from './SubCompInputsDropdown';
import { PanelMenu } from 'smith-ui-components/panelmenu';

const SubCompInputs: FC = () => {
  return (
    <div className="template-example">
      <h2 className="template-title-example">Inputs</h2>

      <div className="form-group">
        <label htmlFor="exampleInputEmail1">Email address</label>
        <InputText
          type="email"
          className="form-control"
          id="exampleInputEmail1"
          placeholder="Custom InputText"
        />
      </div>

      <SubCompInputsDropdown />

      <SubCompInputsCheckbox />

      <div className="form-group">
        <InputTextarea
          className="form-control"
          placeholder="Custom textarea"
          style={{ height: '150px' }}
        />
      </div>

      <SubCompInputsRadio />

      <div className="alert alert-success" role="alert">
        Your message was successfully sent
      </div>
      <div className="alert alert-danger" role="alert">
        Your message has not been sent, restart
      </div>
      <PanelMenu />
    </div>
  );
};

export default SubCompInputs;
