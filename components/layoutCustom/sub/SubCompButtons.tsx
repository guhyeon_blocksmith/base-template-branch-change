import React, { FC } from 'react';
import { Button } from 'smith-ui-components/button';

const SubCompButtons: FC = () => {
  return (
    <div className="template-example">
      <h2 className="template-title-example">Buttons</h2>
      <div className="row">
        <div className="col-md-6">
          <h3 className="template-title-example">Primary</h3>
          <div className="row">
            <div className="col-md-6">
              <p>
                <Button className="btn btn-primary btn-lg">
                  Primary Large
                </Button>
              </p>
              <p>
                <Button className="btn btn-primary">Primary </Button>
              </p>
              <p>
                <Button className="btn btn-primary btn-sm">
                  Primary Small
                </Button>
              </p>
            </div>
          </div>
        </div>

        <div className="col-md-6">
          <h3 className="template-title-example">Secondary</h3>
          <div className="row">
            <div className="col-md-6">
              <p>
                <Button className="btn btn-info btn-lg">Secondary Large</Button>
              </p>
              <p>
                <Button className="btn btn-info">Secondary </Button>
              </p>
              <p>
                <Button className="btn btn-info btn-sm">Secondary Small</Button>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubCompButtons;
