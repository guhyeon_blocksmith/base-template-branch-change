import React from 'react';
import { useState } from 'react';
import { FC } from 'react';
import { RadioButton } from 'smith-ui-components/radiobutton';

const SubCompInputsRadio: FC = () => {
  const rdoFalse = {
    rdo1: false,
    rdo2: false,
    rdo3: false,
    rdo4: false,
    rdo5: false,
  };
  const [rdoChk, setRdoChk] = useState({
    rdo1: true,
    rdo2: false,
    rdo3: false,
    rdo4: false,
    rdo5: false,
  });
  const rdoUpdate = (value: string) => {
    switch (value) {
      case 'rdo1':
        setRdoChk({
          ...rdoFalse,
          rdo1: !rdoChk.rdo1,
        });
        break;
      case 'rdo2':
        setRdoChk({
          ...rdoFalse,
          rdo2: !rdoChk.rdo2,
        });
        break;
      case 'rdo3':
        setRdoChk({
          ...rdoFalse,
          rdo3: !rdoChk.rdo3,
        });
        break;
      case 'rdo4':
        setRdoChk({
          ...rdoFalse,
          rdo4: !rdoChk.rdo4,
        });
        break;
      case 'rdo5':
        setRdoChk({
          ...rdoFalse,
          rdo5: !rdoChk.rdo5,
        });
        break;
    }
  };

  return (
    <ul>
      <li>
        <RadioButton
          inputId="rdo1"
          onChange={() => rdoUpdate('rdo1')}
          checked={rdoChk.rdo1}
        />
        <label htmlFor="rdo1">라디오1</label>
      </li>
      <li>
        <RadioButton
          inputId="rdo2"
          onChange={() => rdoUpdate('rdo2')}
          checked={rdoChk.rdo2}
        />
        <label htmlFor="rdo2">라디오2</label>
      </li>
      <li>
        <RadioButton
          inputId="rdo3"
          onChange={() => rdoUpdate('rdo3')}
          checked={rdoChk.rdo3}
        />
        <label htmlFor="rdo3">라디오3</label>
      </li>
      <li>
        <RadioButton
          inputId="rdo4"
          onChange={() => rdoUpdate('rdo4')}
          checked={rdoChk.rdo4}
        />
        <label htmlFor="rdo4">라디오4</label>
      </li>
      <li>
        <RadioButton
          inputId="rdo5"
          onChange={() => rdoUpdate('rdo5')}
          checked={rdoChk.rdo5}
        />
        <label htmlFor="rdo5">라디오5</label>
      </li>
    </ul>
  );
};

export default SubCompInputsRadio;
