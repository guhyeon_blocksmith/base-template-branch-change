import React, { FC } from 'react';
import { TabMenu } from 'smith-ui-components/tabmenu';
import tabCustom from './TabCustom.module.css';

const TabType01: FC = () => {
  const items = [
    { label: '페이지명1' },
    { label: '페이지명2' },
    { label: '페이지명3' },
    { label: '페이지명4' },
  ];
  return (
    <div className={tabCustom.tabType01}>
      <TabMenu model={items} />
    </div>
  );
};

export default TabType01;
