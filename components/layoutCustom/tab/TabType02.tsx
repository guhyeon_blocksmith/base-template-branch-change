import React, { FC } from 'react';
import { TabMenu } from 'smith-ui-components/tabmenu';
import tabCustom from 'components/layoutCustom/tab/TabCustom.module.css';

const TabType02: FC = () => {
  const items = [
    { label: '탭1' },
    { label: '탭2' },
    { label: '탭3' },
    { label: '탭4' },
    { label: '탭5' },
  ];
  return (
    <div className={tabCustom.tabType02}>
      <TabMenu model={items} />
    </div>
  );
};

export default TabType02;
