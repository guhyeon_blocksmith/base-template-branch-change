import React, { FC, useEffect, useRef, useState } from 'react';
import MetaHeader from 'components/common/MetaHeader';
import { LayoutConfig } from 'types';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Button } from 'smith-ui-components/button';

/**
 * 커스텀 Header<br/>
 * .env에 NEXT_PUBLIC_IS_CUSTOM_LAYOUT이 true인 경우 렌더링되는 커스텀 컴포넌트입니다.<br/>
 * 마음대로 수정 가능
 * @constructor
 */

interface LayoutHeaderProps {
  config: LayoutConfig;
}
const CustomHeader: FC<LayoutHeaderProps> = ({ config }) => {
  const cts = useRef(null);
  const [gapToTop, setGapToTop] = useState<number>(100);
  const router = useRouter();

  const handleScroll = () => {
    const lastScrollY = window.scrollY; //현재 scroll의 위치값
    if (router.pathname === '/') {
      setGapToTop(lastScrollY);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  return (
    <>
      <MetaHeader config={config} />
      <header style={{ zIndex: 1 }}>
        <nav className={`navbar navbar-default ${gapToTop > 0 && 'active'}`}>
          <div className="container" id={'cts'} ref={cts}>
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle collapsed"
                data-toggle="collapse"
                data-target="#navbar-collapse"
                aria-expanded="false"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>

              <div className="navbar-brand">
                <h1>
                  <Link href="/">
                    <Button>
                      <img
                        src="/images/custom/mashup-icon.svg"
                        className="navbar-logo-img"
                        alt=""
                      />
                      Smith Template
                    </Button>
                  </Link>
                </h1>
              </div>
            </div>

            <div className="collapse navbar-collapse" id="navbar-collapse">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  <Link href="/">Home</Link>
                </li>
                <li>
                  <Link href="/">Project</Link>
                </li>
                <li>
                  <p>
                    <Link href="/sample/sub">
                      <Button className="btn btn-default navbar-btn">
                        Components
                      </Button>
                    </Link>
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    </>
  );
};

export default CustomHeader;
