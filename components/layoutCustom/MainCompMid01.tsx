import React, { FC } from 'react';
import { Button } from 'smith-ui-components/button';

/**
 * 커스텀 페이지 더미 컴포넌트
 * @constructor
 */
const MainCompMid01: FC = () => {
  return (
    <>
      <div className="section-container">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-8 col-md-offset-2">
              <div className="text-center">
                <>
                  <h2>About Us</h2>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Maecenas luctus at sem quis varius.
                    <br />
                    Class aptent taciti sociosqu ad litora torquent per conubia
                    nostra, per inceptos himenaeos. Phasellus iaculis magna
                    sagittis elit sagittis, at hendrerit lorem venenatis. Morbi
                    accumsan iaculis blandit. Cras ultrices hendrerit nisl.
                  </p>
                </>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="section-container">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div
                id="carousel-example-generic"
                className="carousel carousel-fade slide"
                data-ride="carousel"
              >
                <div className="carousel-inner" role="listbox">
                  <div className="item active">
                    <>
                      <img
                        className="img-responsive"
                        src="/images/custom/img-06.jpg"
                        alt="First slide"
                      />
                      <div className="carousel-caption card-shadow reveal">
                        <>
                          <h3>Boilerplates</h3>
                          <Button
                            className="left carousel-control"
                            //href="#carousel-example-generic"
                            role="button"
                            data-slide="prev"
                          >
                            <i
                              className="fa fa-chevron-left"
                              aria-hidden="true"
                            />
                            <span className="sr-only">Previous</span>
                          </Button>
                          <Button
                            className="right carousel-control"
                            //href="#carousel-example-generic"
                            role="button"
                            data-slide="next"
                          >
                            <i
                              className="fa fa-chevron-right"
                              aria-hidden="true"
                            />
                            <span className="sr-only">Next</span>
                          </Button>
                          <p>
                            Sed id tellus in risus pre tium imperdiet eu
                            lobortis dolor. Sed pellentesque, urna ac viverra
                            lacinia, erat mauris venenatis purus, mollis egestas
                            urna purus ac ex. Aenean nunc sem, lobortis at elit
                            non, lobortis laoreet nibh. Maecenas at mi ipsum.
                          </p>

                          <p>
                            Quisque tempor, ligula pharetra luctus elementum,
                            arcu nisl suscipit ante, pharetra commodo dui est et
                            enim. Sed eu vestibulum elit. Donec ut libero non.
                          </p>
                          <Button
                            //href="./project.html"
                            className="btn btn-primary"
                            title=""
                          >
                            Discover
                          </Button>
                        </>
                      </div>
                    </>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainCompMid01;
