import React, { FC } from 'react';
import { DataTable } from 'smith-ui-components/datatable';
import { Column } from 'smith-ui-components/column';
import { Button } from 'smith-ui-components/button';

import { saveAs } from 'file-saver';
import { AttachFileType } from 'types/AttachFileType';

export interface FileListProps {
  deletable?: boolean;
  attachedFiles?: AttachFileType[];
  onFileDeleteClick?: (file: AttachFileType) => void;
}

/**
 * 파일 리스트
 * @param deletable
 * @constructor
 */
const FileList: FC<FileListProps> = ({
  deletable = true,
  attachedFiles,
  onFileDeleteClick,
}) => {
  const actionBodyTemplate = (rowData: AttachFileType) => {
    if (!deletable) {
      return null;
    }

    return (
      <div>
        <Button
          icon="pi pi-times"
          className="p-button-rounded p-button-danger"
          // TODO 버튼 한 가운데를 누르면 이 이벤트는 발생안하고 onRowClick이벤트가 발생한다.
          // TODO 버튼 위치 이동 필요
          onClick={() => onFileDeleteClick && onFileDeleteClick(rowData)}
        />
      </div>
    );
  };

  return (
    <>
      <DataTable
        value={attachedFiles}
        dataKey="name"
        onRowClick={async (rowData) => {
          saveAs(rowData.data.url, rowData.data.name);
        }}
        selectionMode={'single'}
      >
        <Column field="name" header="파일명" />
        <Column body={actionBodyTemplate} />
      </DataTable>
    </>
  );
};

export default FileList;
