import React, { FC, useRef, useState } from 'react';
import SignatureCanvas from 'react-signature-canvas';
import { Button } from 'smith-ui-components/button';
import ReactSignatureCanvas from 'react-signature-canvas';
import { Dialog } from 'smith-ui-components/dialog';

export interface SignatureProps {
  onClose: (base64?: string) => void;
  userName: string;
}

const Signature: FC<SignatureProps> = ({ onClose, userName = '홍길동' }) => {
  const [imageURL, setImageURL] = useState<string>();

  const signatureRef = useRef<ReactSignatureCanvas>(null);
  const clear = () => {
    signatureRef.current?.clear();
  };

  const save = () => {
    setImageURL(
      signatureRef.current?.getTrimmedCanvas().toDataURL('image/png')
    );
  };

  const [displaySign, setDisplaySign] = useState<boolean>(false);
  const open = () => {
    setDisplaySign(true);
  };
  const close = () => {
    setDisplaySign(false);
    onClose(imageURL);
  };

  return (
    <div
      style={{
        position: 'relative',
        width: '100px',
        height: '100px',
        margin: '0 auto',
      }}
    >
      <Dialog
        visible={displaySign}
        modal
        onHide={close}
        header="서명하기"
        footer={
          <div>
            <Button label={'clear'} onClick={clear} />
            <Button label={'save'} onClick={save} />
            <Button label={'close'} onClick={close} />
          </div>
        }
      >
        <SignatureCanvas
          ref={signatureRef}
          canvasProps={{ width: 600, height: 400, className: 'sigCanvas' }}
        />
      </Dialog>
      <div
        style={{
          display: 'block',
          width: '150px',
          position: 'absolute',
          left: '35%',
          top: '50%',
          margin: '-9px 0 0 -28px',
        }}
        onClick={open}
      >
        성명 : {userName} (인)
      </div>
      {imageURL ? (
        <img
          src={imageURL}
          style={{
            display: 'block',
            width: '120px',
            height: '70px',
          }}
        />
      ) : null}
    </div>
  );
};

export default Signature;
