import React, { forwardRef, ReactNode, useImperativeHandle } from 'react';
import useHtmlToPdf from 'libs/hooks/useHtmlToPdf';

export interface HtmlToPDFElement {
  printToPdf: () => void;
  printToPdfBlob: () => void;
  printToPdfFile: (fileName: any) => Promise<File | undefined>;
}

export interface HtmlToPDFProps {
  children: ReactNode;
  pdfName?: string;
}

// eslint-disable-next-line react/display-name
const HtmlToPDF = forwardRef<HtmlToPDFElement, HtmlToPDFProps>((props, ref) => {
  const { pdfName } = props;

  const { htmlRef, printToPdf, printToPdfBlob, printToPdfFile } = useHtmlToPdf(
    pdfName
  );

  useImperativeHandle(ref, () => ({
    printToPdf,
    printToPdfBlob,
    printToPdfFile,
  }));

  // HtmlToPdfWrapper 아이디는 pdf로 변환시 상요합니다.
  return (
    <div ref={htmlRef} id={'HtmlToPdfWrapper'}>
      {/*width a4 용지 사이즈와 맞춤*/}
      {props.children}
    </div>
  );
});

export default HtmlToPDF;
