import React, { FC, useEffect, useRef, useState } from 'react';
import { InputText, InputTextProps } from 'smith-ui-components/inputtext';
import { regexKor } from 'libs/regex';

interface InputTextUserNameProps extends InputTextProps {
  onTriedKoreanInput?: () => void;
}

/**
 * 로그인 사용자 id
 * @param props
 * @constructor
 */
const InputTextUserName: FC<InputTextUserNameProps> = (props) => {
  const inputRef = useRef<InputText>(null);

  const [innerValue, setInnerValue] = useState<
    string | number | readonly string[] | undefined
  >(props.value || '');

  useEffect(() => {
    if (props.ref) {
      props.ref = inputRef;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setInnerValue(props.value || '');
  }, [props.value]);

  const newProps = { ...props };
  delete newProps.onTriedKoreanInput;

  return (
    <InputText
      {...props}
      value={innerValue}
      style={{ imeMode: 'disabled' }}
      type={'email'}
      onChange={(e) => {
        if (regexKor.test(e.target.value)) {
          inputRef.current?.element?.blur();
          inputRef.current?.element?.focus();
          props.onTriedKoreanInput && props.onTriedKoreanInput();
          return;
        }

        const newValue = e.target.value;
        const regName = /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,20}$/;
        const checkValid = regName.test(newValue);

        if (checkValid || newValue === '') {
          setInnerValue(newValue.toLocaleLowerCase());
          props.onChange && props.onChange(e);
        }
      }}
    />
  );
};

export default InputTextUserName;
