import React, { FC, useEffect, useState } from 'react';
import { Chart } from 'smith-ui-components/chart';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import _ from 'lodash';
import styles from './MultiChart.module.scss';
import { SelectButton } from 'smith-ui-components/selectbutton';

export interface DatasetType {
  data: number[];
  borderColor?: string;
  backgroundColor?: string[];
  fill: boolean;
}

export interface MultiChartDataType {
  labels: string[];
  datasets: DatasetType[];
}

export type ChartType =
  | 'bar'
  | 'pie'
  | 'line'
  | 'doughnut'
  | 'horizontalBar'
  | 'polarArea';

export interface MultiChartsProps {
  /**
   * 차트 데이터
   */
  chartData: MultiChartDataType;
  /**
   * 차트 제목
   */
  title: string;
  /**
   * 기본 차트 타입   'bar'| 'pie'| 'line'| 'doughnut'| 'horizontalBar'| 'polarArea'
   */
  defaultChartType: ChartType;
  /**
   * 차트 크기 (width)
   */
  width?: string;
}

/**
 * 멀티차트 옵션
 */
export interface MultiChartOptions {
  legend: { labels?: { fontColor: string }; display?: boolean };
  scales?: {
    xAxes: { ticks: { min: number; fontColor: string } }[];
    yAxes: { ticks: { min: number; fontColor: string } }[];
  };
}

const basicOptions = {
  plugins: {
    plugins: [ChartDataLabels],
    sort: {
      enable: true,
    },
    datalabels: {
      weight: 'bold',
      color: 'white',
      font: {
        size: 15,
      },
    },
  },
  legend: {
    labels: {
      display: true,
      fontColor: '#495057',
    },
  },
};

const barOptions = {
  plugins: {
    plugins: [ChartDataLabels],
    datalabels: {
      align: 'end',
      anchor: 'end',
      color: 'black',
      font: {
        weight: 'bold',
        size: 15,
      },
    },
  },
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        ticks: {
          min: 0,
          fontColor: '#495057',
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          min: 0,
          fontColor: '#495057',
        },
      },
    ],
  },
};

const MultiChart: FC<MultiChartsProps> = ({
  chartData,
  title,
  defaultChartType,
  width = '700px',
}) => {
  const [data, setData] = useState<MultiChartDataType>();
  const [chartType, setChartType] = useState<ChartType>(defaultChartType);
  const [chartOption, setChartOption] = useState<MultiChartOptions>();

  useEffect(() => {
    setData(chartData);
    setTotalValue(chartData.datasets[0].data.reduce((a, b) => a + b));
  }, [chartData]);

  const [percent, setPercent] = useState<boolean>(false);
  const [perValue, setPerValue] = useState<number[]>([]);
  const [totalValue, setTotalValue] = useState<number>();
  const [valueType, setValueType] = useState('응답수');
  const options = ['응답수', '응답률'];

  useEffect(() => {
    if (valueType === '응답수') {
      const handleScore = () => {
        if (data === undefined) return;
        if (!percent) return;

        setData(chartData);
        setPercent(false);
      };

      handleScore();
    } else if (valueType === '응답률') {
      const handlePercent = () => {
        if (data === undefined) return;
        if (totalValue === undefined) return;
        if (percent) return;

        const arrayData = data.datasets[0].data;

        const percentValue = arrayData.map((v) => {
          return Math.round((v * 100) / totalValue);
        });

        setPerValue(percentValue);

        const newData = _.cloneDeep(data);

        newData.datasets[0].data = percentValue;
        setData(newData);
        setPercent(true);
      };

      handlePercent();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueType]);

  useEffect(() => {
    if (
      chartType === 'bar' ||
      chartType === 'horizontalBar' ||
      chartType === 'line'
    ) {
      setChartOption(barOptions);
    } else {
      setChartOption(basicOptions);
    }

    const newData = _.cloneDeep(chartData);
    if (percent) {
      newData.datasets[0].data = perValue!;
      setData(newData);
    }

    if (chartType === 'line') {
      const dataSet = newData!.datasets[0];
      delete dataSet.backgroundColor;
    } else {
      const dataSet = newData!.datasets[0];
      delete dataSet.borderColor;
    }
    setData(newData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartType]);

  const handleSortChart = (type: string) => {
    if (data === undefined) return;

    const arrayLabel = data.labels;
    const arrayData = data.datasets[0].data;

    const arrayObj = arrayLabel.map((v, i) => {
      return {
        label: v,
        data: arrayData[i],
      };
    });
    const sortValue = arrayObj.sort((a, b) => {
      let result: number;

      if (type === 'asc') {
        result = a.data - b.data;
      } else {
        result = b.data - a.data;
      }
      return result;
    });

    const newData: MultiChartDataType = _.cloneDeep(data);
    newData.labels = sortValue.map((v) => {
      return v.label;
    });

    newData.datasets[0].data = sortValue.map((v) => v.data);
    setData(newData);
  };

  const handleValueType = (e: {
    originalEvent: Event;
    value: any;
    target: { name: string; id: string; value: any };
  }) => {
    setValueType(e.value);
  };

  return (
    <div style={{ width: `${width}` }}>
      <div className={styles.chartHeader}>
        <div style={{ float: 'left' }}>
          <h3>{title}</h3>
        </div>
        <div className={`${styles.chartIcon}`}>
          <SelectButton
            className={`${styles['p-selectbutton']}`}
            value={valueType}
            options={options}
            onChange={(e) => {
              handleValueType(e);
            }}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.line}`}
            onClick={() => setChartType('line')}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.bar}`}
            onClick={() => setChartType('bar')}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.horizontalBar}`}
            onClick={() => setChartType('horizontalBar')}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.pie}`}
            onClick={() => setChartType('pie')}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.doughnut}`}
            onClick={() => setChartType('doughnut')}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.asc}`}
            onClick={() => handleSortChart('asc')}
          />
          <button
            className={`${styles['button']} ${styles.opt} ${styles.desc}`}
            onClick={() => handleSortChart('desc')}
          />
          <span style={{ display: 'inline-block', paddingBottom: '2px' }}>
            {' '}
            응답수 : {totalValue}{' '}
          </span>
        </div>
      </div>

      <Chart type={chartType} data={data} options={chartOption} />
    </div>
  );
};

export default MultiChart;
