import React, { FC, Fragment } from 'react';

/**
 * React Compound Slider
 * https://sghall.github.io/react-compound-slider/#/slider-demos/horizontal
 * @constructor
 */

interface HandleObjectsType {
  id: string;
  value: number;
  percent: number;
}

interface CommonSliderType {
  domain: number[];
  handle: HandleObjectsType;
  disabled?: boolean;
  getHandleProps: (props: any) => void;
}

// *******************************************************
// RAIL
// *******************************************************

export const SliderRail: FC<any> = ({ getRailProps }) => {
  return (
    <Fragment>
      <div
        style={{
          position: 'absolute',
          width: '100%',
          height: 42,
          transform: 'translate(0%, -50%)',
          borderRadius: 7,
          cursor: 'pointer',
          // border: '1px solid white',
        }}
        {...getRailProps()}
      />
      <div
        style={{
          position: 'absolute',
          width: '100%',
          height: 14,
          transform: 'translate(0%, -50%)',
          borderRadius: 7,
          pointerEvents: 'none',
          backgroundColor: 'rgb(198,198,198)',
        }}
      />
    </Fragment>
  );
};

// *******************************************************
// HANDLE COMPONENT
// *******************************************************
export const Handle: FC<CommonSliderType> = ({
  domain: [min, max],
  handle: { id, value, percent },
  disabled = false,
  getHandleProps,
}) => {
  return (
    <Fragment>
      <div
        style={{
          left: `${percent}%`,
          position: 'absolute',
          transform: 'translate(-50%, -50%)',
          WebkitTapHighlightColor: 'rgba(0,0,0,0)',
          zIndex: 5,
          width: 28,
          height: 42,
          cursor: 'pointer',
          // border: '1px solid white',
          backgroundColor: 'none',
        }}
        {...getHandleProps(id)}
      />
      <div
        role="slider"
        aria-valuemin={min}
        aria-valuemax={max}
        aria-valuenow={value}
        style={{
          left: `${percent}%`,
          position: 'absolute',
          transform: 'translate(-50%, -50%)',
          zIndex: 2,
          width: 24,
          height: 24,
          borderRadius: '50%',
          boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.3)',
          backgroundColor: disabled ? '#d2d2d2' : '#656574',
        }}
      />
    </Fragment>
  );
};

// *******************************************************
// KEYBOARD HANDLE COMPONENT
// Uses a button to allow keyboard events
// *******************************************************
export const KeyboardHandle: FC<CommonSliderType> = ({
  domain: [min, max],
  handle: { id, value, percent },
  disabled = false,
  getHandleProps,
}) => {
  return (
    <button
      role="slider"
      aria-valuemin={min}
      aria-valuemax={max}
      aria-valuenow={value}
      style={{
        left: `${percent}%`,
        position: 'absolute',
        transform: 'translate(-50%, -50%)',
        zIndex: 2,
        width: 24,
        height: 24,
        borderRadius: '50%',
        boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.3)',
        backgroundColor: disabled ? '#666' : '#ffc400',
      }}
      {...getHandleProps(id)}
    />
  );
};

// *******************************************************
// TRACK COMPONENT
// *******************************************************
interface TracksType {
  source: HandleObjectsType;
  target: HandleObjectsType;
  getTrackProps: () => void;
  disabled?: boolean;
}

export const Track: FC<TracksType> = ({
  source,
  target,
  getTrackProps,
  disabled = false,
}) => {
  return (
    <div
      style={{
        position: 'absolute',
        transform: 'translate(0%, -50%)',
        height: 14,
        zIndex: 1,
        backgroundColor: disabled ? '#999' : '#ffeeb6',
        borderRadius: 7,
        cursor: 'pointer',
        left: `${source.percent}%`,
        width: `${target.percent - source.percent}%`,
      }}
      {...getTrackProps()}
    />
  );
};

// *******************************************************
// TICK COMPONENT
// *******************************************************
interface SliderTickTypes {
  tick: HandleObjectsType;
  count: number;
  format?: any;
}

export const Tick: FC<SliderTickTypes> = ({ tick, count }) => {
  return (
    <div>
      <div
        style={{
          position: 'absolute',
          marginTop: 14,
          width: 1,
          height: 5,
          backgroundColor: 'rgb(200,200,200)',
          left: `${tick.percent}%`,
        }}
      />
      <div
        style={{
          position: 'absolute',
          marginTop: 22,
          fontSize: 10,
          textAlign: 'center',
          marginLeft: `${-(100 / count) / 2}%`,
          width: `${100 / count}%`,
          left: `${tick.percent}%`,
        }}
      >
        {tick.value}
      </div>
    </div>
  );
};
