import React, { FC, useEffect, useRef, useState } from 'react';
import { Handles, Rail, Slider, Ticks, Tracks } from 'react-compound-slider';
import { Handle, SliderRail, Tick, Track } from './RangeSliderCommon';

export interface RangeSliderProps {
  sliderStyle?: any;
  domain: number[];
  value: readonly number[];
  reversed?: boolean;
  disabled?: boolean;
  callback: (props: number[]) => void;
  maxGap?: number;
  minGap?: number;
}

const RangeSlider: FC<RangeSliderProps> = ({
  sliderStyle,
  domain,
  reversed = false,
  disabled = false,
  value = [1, 5],
  callback,
  maxGap,
  minGap,
}) => {
  const [valueState, setValueState] = useState<readonly number[]>(value);
  useEffect(() => {
    setValueState(value);
  }, [value]);

  const [isReady, setIsReady] = useState<boolean>(false);

  const selectedHandleId = useRef<string>();

  const limitRange = (v: any, handleId?: string) => {
    let newValue0 = v[0];
    let newValue1 = v[1];

    let _handleId;
    if (handleId === undefined) {
      _handleId = selectedHandleId.current;
    } else {
      _handleId = handleId;
    }

    // console.log('-> minGap, maxGap', minGap, maxGap);
    if (maxGap !== undefined) {
      const graterThanMaxGap = v[1] - v[0] >= maxGap;
      // console.log('-> graterThanMaxGap', graterThanMaxGap, _handleId);
      if (graterThanMaxGap) {
        if (_handleId === '$$-0') {
          // 앞 부분 핸들 선택시 조절
          newValue0 = v[0];
          newValue1 = v[0] + maxGap;
        } else if (_handleId === '$$-1') {
          // 뒷 부분 핸들 선택시 조절
          newValue0 = v[1] - maxGap;
          newValue1 = v[1];
        }
      }
    }
    if (minGap !== undefined) {
      const lessThanMinGap = v[1] - v[0] <= minGap;
      // console.log('-> lessThanMinGap', lessThanMinGap);
      if (lessThanMinGap) {
        if (_handleId === '$$-0') {
          // 앞 부분 핸들 선택시 조절
          newValue0 = v[0];
          newValue1 = v[0] + minGap;
        } else if (_handleId === '$$-1') {
          // 뒷 부분 핸들 선택시 조절
          newValue0 = v[1] - minGap;
          newValue1 = v[1];
        }
      }
    }

    const newValues = [newValue0, newValue1];
    // console.log('-> newValue0, newValue1', newValue0, newValue1);

    setValueState(newValues);
    return newValues;
  };

  useEffect(() => {
    // domain 이 0이면 준비되지 않음
    if (domain[1] !== 0) {
      setIsReady(true);
    }
  }, [isReady, domain]);

  const handleUpdate = (v: ReadonlyArray<number>) => {
    limitRange(v);
  };

  const handleChange = (values: any) => {
    fireCallbackEvent(values);
  };

  const handleSlideStart = (
    _values: ReadonlyArray<number>,
    data: { activeHandleID: string }
  ) => {
    selectedHandleId.current = data.activeHandleID;
  };

  const handleSlideEnd = (
    _values: ReadonlyArray<number>,
    data: { activeHandleID: string }
  ) => {
    fireCallbackEvent(_values, data.activeHandleID);
  };

  const fireCallbackEvent = (
    _values: ReadonlyArray<number>,
    activeHandleID?: string
  ) => {
    const newValues = limitRange(_values, activeHandleID);
    if (callback) {
      callback(newValues);
    }
  };

  return isReady ? (
    <Slider
      mode={2}
      step={1}
      domain={domain || [0, 10]}
      reversed={reversed}
      rootStyle={sliderStyle}
      onUpdate={handleUpdate}
      onSlideStart={handleSlideStart}
      onSlideEnd={handleSlideEnd}
      onChange={handleChange}
      values={valueState}
      disabled={disabled}
    >
      <Rail>
        {({ getRailProps }) => <SliderRail getRailProps={getRailProps} />}
      </Rail>
      <Handles>
        {({ handles, getHandleProps }) => (
          <div className="slider-handles">
            {handles.map((handle) => (
              <Handle
                key={handle.id}
                handle={handle}
                domain={domain}
                getHandleProps={getHandleProps}
              />
            ))}
          </div>
        )}
      </Handles>
      <Tracks left={false} right={false}>
        {({ tracks, getTrackProps }) => (
          <div className="slider-tracks">
            {tracks.map(({ id, source, target }) => (
              <Track
                key={id}
                source={source}
                target={target}
                getTrackProps={getTrackProps}
              />
            ))}
          </div>
        )}
      </Tracks>
      <Ticks count={10}>
        {({ ticks }) => (
          <div className="slider-ticks">
            {ticks.map((tick) => (
              <Tick key={tick.id} tick={tick} count={ticks.length} />
            ))}
          </div>
        )}
      </Ticks>
    </Slider>
  ) : null;
};

export default RangeSlider;
