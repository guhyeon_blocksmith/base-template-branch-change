import React, { FC } from 'react';

import Head from 'next/head';
import { LayoutConfig } from 'types/index';
// import useTranslation from 'hooks/useTranslation';

interface MetaHeaderProps {
  config: LayoutConfig;
}

/**
 * 사이트 메타 정보 헤더
 * @param config
 * @constructor
 */
const MetaHeader: FC<MetaHeaderProps> = ({ config }) => {
  const { header, kr } = config;
  // const { t } = useTranslation();

  return (
    <Head>
      <title>{kr?.title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="shortcut icon" href={header?.icon} />
      <meta name="google" content="notranslate" />
      <meta content={kr?.author} name="author" />
      <meta property="og:url" content={header?.url} />
      <meta property="og:title" content={kr?.title} />
      <meta property="og:type" content={header?.type} />
      <meta property="og:image" content={header?.ogImage} />
      <meta property="og:description" content={kr?.description} />
    </Head>
  );
};

export default MetaHeader;
