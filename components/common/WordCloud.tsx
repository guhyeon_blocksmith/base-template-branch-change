import React, { FC } from 'react';
import ReactWordcloud, { OptionsProp, Word } from 'react-wordcloud';
import styles from './WordCloud.module.css';

/**
 * wordCloud Data Props
 */
export interface WordsCloudProps {
  /**
   * Type은 ReactWordcloud 에 있는  word[] 타입
   */
  wordData?: Word[];
}

/**
 * 워드 클라우드 공통 컴포넌트
 * @param wordData
 * @constructor
 */
export const WordCloud: FC<WordsCloudProps> = ({ wordData }) => {
  const options: OptionsProp = {
    colors: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b'],
    enableTooltip: true, //마우스 오버 시 툴팁 사용 여부
    deterministic: false,
    fontFamily: 'impact', //폰트이름
    fontSizes: [5, 60], // 폰트사이즈 최소, 최대
    fontStyle: 'normal', // 폰트 italic
    fontWeight: 'normal', // 폰트 굵기
    padding: 1, // 단어 간격
    rotations: 3, // 배치 옵션
    rotationAngles: [0, 90], // 기울기 최소, 최대
    scale: 'sqrt',
    spiral: 'archimedean',
    transitionDuration: 1000,
  };

  if (wordData === undefined) return null;

  return (
    <div className={styles.wrapper}>
      <ReactWordcloud options={options} words={wordData} />
    </div>
  );
};
