import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
// import testPDF from 'public/sample/test.pdf';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;

const PDFReader = () => {
  const [url, setUrl] = useState<string>();
  const fileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files) {
      files.length > 0 && setUrl(URL.createObjectURL(files[0]));
    }
  };

  return (
    <div>
      <input type={'file'} accept={'.pdf'} onChange={fileUpload} />
      <Document file={url}>
        <Page pageNumber={1} />
      </Document>
    </div>
  );
};

export default PDFReader;
