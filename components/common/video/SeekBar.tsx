import * as React from 'react';
import { FC, useEffect, useRef, useState } from 'react';
import styles from 'components/common/video/SeekBar.module.scss';
import { isTouchEvent } from 'types/index';
import { ProgressState } from 'components/common/video/VideoPlayer';

export interface SeekBarProps {
  progressState?: ProgressState;
  duration?: number;
  points?: number[];
  onPoint?: (seconds: number) => void;
  className?: string;
  movable?: boolean;
  onChange?: (event: {
    originalEvent: React.MouseEvent | React.TouchEvent;
    value: ProgressState;
  }) => void;
  tabIndex?: number;
  onDragStart?: (event: React.MouseEvent | React.TouchEvent) => void;
  onDragEnd?: (event: React.MouseEvent | React.TouchEvent) => void;

  /**
   * 최대 움직일수있는 값
   */
  maxMovable?: number;
  onBarClick?: () => void;
}

const getWindowScrollLeft = () => {
  const doc = document.documentElement;
  return (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
};

const getWindowScrollTop = () => {
  const doc = document.documentElement;
  return (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
};

export const SeekBar: FC<SeekBarProps> = ({
  className,
  duration = 0,
  progressState = { played: 0, playedSeconds: 0, loaded: 0, loadedSeconds: 0 },
  points,
  onPoint,
  movable = true,
  onChange,
  tabIndex,
  onDragStart,
  onDragEnd,
  maxMovable,
  onBarClick,
}) => {
  const { played, playedSeconds, loaded } = progressState;

  const barRef = useRef<HTMLDivElement>(null);

  const [valuePercent, setValuePercent] = useState<number>(0);

  useEffect(() => {
    if (played !== undefined && played !== null) {
      setValuePercent(played * 100);
    }
  }, [played]);

  useEffect(() => {
    if (onPoint && playedSeconds !== undefined && playedSeconds !== null) {
      const playedSecondsTruncated = Math.trunc(playedSeconds);
      if (points && points.some((p) => p === playedSecondsTruncated)) {
        onPoint(playedSecondsTruncated);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playedSeconds]);

  const sliderHandleClick = useRef<boolean>(false);

  const handleBarClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    if (!movable) {
      return;
    }

    if (!sliderHandleClick.current) {
      updateDomData();
      setValue(event);
    }
    sliderHandleClick.current = false;
    if (onBarClick) {
      // setTimeout(() => {
      onBarClick();
      // }, 0);
    }
  };

  const domData = useRef<{
    initX: number;
    initY: number;
    barWidth: number;
    barHeight: number;
  }>();

  const max = duration;
  const min = 0;

  const updateDomData = () => {
    if (barRef.current) {
      const rect = barRef.current.getBoundingClientRect();
      if (rect) {
        const initX = rect.left + getWindowScrollLeft();
        const initY = rect.top + getWindowScrollTop();
        const barWidth = barRef.current.offsetWidth;
        const barHeight = barRef.current.offsetHeight;
        domData.current = { initX, initY, barWidth, barHeight };
      }
    }
  };

  const setValue = (event: React.MouseEvent | React.TouchEvent) => {
    if (domData.current === undefined) {
      console.warn('domData가 없습니다.');
      return;
    }
    let pageX;
    if (isTouchEvent(event)) {
      pageX = event.touches[0].pageX;
    } else {
      pageX = event.pageX;
    }

    const handleValue =
      ((pageX - domData.current.initX) * 100) / domData.current.barWidth;

    const newValue = (max - min) * (handleValue / 100) + min;

    if (maxMovable && maxMovable <= newValue) {
      fireOnChangeEvent(event, {
        played: maxMovable / duration,
        playedSeconds: maxMovable,
      });
      return;
    }

    fireOnChangeEvent(event, {
      played: handleValue / 100,
      playedSeconds: newValue,
    });
  };

  const getInRangeValue = (value: number, min: number, max: number) => {
    let newValue = value;

    if (newValue < min) newValue = min;
    else if (newValue > max) newValue = max;

    return newValue;
  };

  const fireOnChangeEvent = (
    event: React.MouseEvent | React.TouchEvent,
    value: ProgressState
  ) => {
    if (onChange) {
      onChange({
        originalEvent: event,
        value: {
          playedSeconds: Math.floor(
            getInRangeValue(value.playedSeconds!, min, max)
          ),
          played: getInRangeValue(value.played!, 0, 1),
        },
      });
    }
  };

  const handleMouseDown = (event: React.MouseEvent) => {
    bindDragListeners(true);
    handleDragStart(event);
  };

  const handleTouchStart = (event: React.TouchEvent) => {
    bindDragListeners(false);
    handleDragStart(event);
  };

  const dragging = useRef<boolean>(false);

  const handleDragStart = (event: React.MouseEvent | React.TouchEvent) => {
    if (!movable) {
      return;
    }

    dragging.current = true;
    if (onDragStart) {
      onDragStart(event);
    }
    updateDomData();
    sliderHandleClick.current = true;
    event.preventDefault();
  };

  const dragStartListener = (event: React.MouseEvent | React.TouchEvent) => {
    if (dragging.current) {
      if (handleDragStart) {
        handleDragStart(event);
      }
      setValue(event);
      event.preventDefault();
    }
  };

  const dragEndListener = (event: React.MouseEvent | React.TouchEvent) => {
    // const onDragEnd = (event: React.MouseEvent) => {
    if (dragging.current) {
      dragging.current = false;
      if (onDragEnd) {
        onDragEnd(event);
      }
      // if (onSlideEnd) {
      //   this.props.onSlideEnd({
      //     originalEvent: event,
      //     value: this.props.value,
      //   });
      // }

      unbindDragListeners(true);
      unbindDragListeners(false);
    }
  };

  useEffect(() => {
    return () => {
      unbindDragListeners(true);
      unbindDragListeners(false);
    };
  }, []);

  const onDragListener = useRef<
    (event: React.MouseEvent | React.TouchEvent) => void
  >();

  const onDragEndListener = useRef<
    (event: React.MouseEvent | React.TouchEvent) => void
  >();

  const bindDragListeners = (isMouse: boolean) => {
    const dragType = isMouse ? 'mousemove' : 'touchmove';
    const dragEndType = isMouse ? 'mouseup' : 'touchend';
    if (!onDragListener.current) {
      onDragListener.current = dragStartListener;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      document.addEventListener(dragType, onDragListener.current);
    }
    if (!onDragEndListener.current) {
      onDragEndListener.current = dragEndListener;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      document.addEventListener(dragEndType, onDragEndListener.current);
    }
  };

  const unbindDragListeners = (isMouse: boolean) => {
    const dragType = isMouse ? 'mousemove' : 'touchmove';
    const dragEndType = isMouse ? 'mouseup' : 'touchend';
    if (onDragListener.current) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      document.removeEventListener(dragType, onDragListener.current);
      onDragListener.current = undefined;
    }
    if (onDragEndListener.current) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      document.removeEventListener(dragEndType, onDragEndListener.current);
      onDragEndListener.current = undefined;
    }
  };

  return (
    <div
      className={`${styles.seekArea} seekArea ${className ? className : ''}`}
    >
      {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
      <div
        className={`${styles.stickWrap} stickWrap`}
        ref={barRef}
        onClick={handleBarClick}
      >
        <div className={styles.loaded} style={{ width: `${loaded ?? 0}%` }} />
        <div className={styles.pointsWrap}>
          {points &&
            points.map((v: number, index: number) => {
              return (
                <div
                  style={{ left: `${(v / duration!) * 100}%` }}
                  key={index}
                />
              );
            })}
        </div>
        <div className={styles.stick} style={{ width: `${valuePercent}%` }}>
          <button
            className={styles.handle}
            onMouseDown={handleMouseDown}
            onTouchStart={handleTouchStart}
            tabIndex={tabIndex}
            role="slider"
            aria-valuemin={min}
            aria-valuemax={max}
            aria-valuenow={valuePercent}
          />
        </div>
      </div>
    </div>
  );
};
