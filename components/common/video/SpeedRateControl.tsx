import * as React from 'react';
import { FC, useState } from 'react';
import styles from 'components/common/video/SpeedRateControl.module.scss';

export interface SpeedRateControlProps {
  speedRates?: number[];
  onChange?: (speedRate: number) => void;
}

export const SpeedRateControl: FC<SpeedRateControlProps> = ({
  speedRates,
  onChange,
}) => {
  // const speedRates = [1.0, 1.25, 1.5, 1.75, 2.0];
  const [visibleOptions, setVisibleOptions] = useState<boolean>();
  const [selectedSpeedRate, setSelectedSpeedRate] = useState<number>(1.0);

  if (speedRates === undefined) {
    return null;
  }

  return (
    <div className={`${styles.option} ${visibleOptions && styles.on}`}>
      <button
        onClick={() => {
          setVisibleOptions((prevState) => !prevState);
        }}
      >
        {selectedSpeedRate + 'x'}
      </button>
      <div
        onMouseLeave={() => {
          setVisibleOptions(false);
        }}
      >
        <ul>
          {speedRates.map((speedRate: number) => {
            return (
              <li key={speedRate}>
                <button
                  onClick={() => {
                    if (onChange) {
                      onChange(speedRate);
                    }
                    setSelectedSpeedRate(speedRate);
                    setVisibleOptions(!visibleOptions);
                  }}
                >
                  {speedRate}x
                </button>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};
