import * as React from 'react';
import { FC, useEffect } from 'react';
import styles from 'components/common/video/FullScreenControl.module.scss';
import screenfull from 'screenfull';

export interface FullScreenControlProps {
  fullScreenEnabled?: boolean;
  videoClassName?: string;
}

/**
 * 전체화면 컨트롤
 * @param fullScreenEnabled
 * @param videoClassName
 * @constructor
 */
export const FullScreenControl: FC<FullScreenControlProps> = ({
  fullScreenEnabled,
  videoClassName,
}) => {
  //// 전체화면 대상 div
  const getReactPlayerDiv = () => {
    return document.getElementsByClassName(videoClassName ?? 'smith-ivs')[0];
  };
  // 전체화면 Prevent
  useEffect(() => {
    const handleScreenChange = () => {
      // TODO IOS는 fullscreen API 지원 X.
      // IOS 대응 필요.
      if (
        screenfull.isEnabled &&
        !fullScreenEnabled &&
        screenfull.isFullscreen
      ) {
        // 전체화면 막기
        screenfull.exit().then(); //.toggle(getReactPlayerDiv()).then();
      }
    };
    if (screenfull.isEnabled) {
      screenfull.onchange(handleScreenChange);
    }

    return () => {
      if (screenfull.isEnabled) {
        screenfull.off('change', handleScreenChange);
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fullScreenEnabled]);

  const handleFullScreenClick = () => {
    if (fullScreenEnabled) {
      if (screenfull.isEnabled) {
        screenfull.toggle(getReactPlayerDiv()).then();
      }
    }
  };

  if (fullScreenEnabled) {
    return (
      <button
        className={`button ${styles.fullscreen}`}
        onClick={handleFullScreenClick}
      />
    );
  } else {
    return null;
  }
};
