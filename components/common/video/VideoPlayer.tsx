import * as React from 'react';
import {
  CSSProperties,
  forwardRef,
  ReactElement,
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import ReactPlayer, { Config } from 'react-player';
import RandomStickerButton from 'components/common/video/RandomStickerButton';
import styles from './VideoPlayer.module.scss';
import { VolumeControl } from 'components/common/video/VolumeControl';
import { FullScreenControl } from 'components/common/video/FullScreenControl';
import { SeekBar } from 'components/common/video/SeekBar';
import { formatSecondsToTime } from 'libs/common';
import { SpeedRateControl } from 'components/common/video/SpeedRateControl';
import { VideoQualityControl } from 'components/common/video/VideoQualityControl';

/**
 * 영상 랜덤스티커 Props
 */
export interface RandomStickerProps {
  /**
   * 랜덤 시작 구간
   */
  start?: number;
  /**
   * 랜덤 종료 구간
   */
  end?: number;
  showStickySeconds?: number;

  /**
   * 랜덤스티커 사용여부.
   */
  enabled?: boolean;

  /**
   * 랜덤으로 나오는 시간이 정해졌을 때 이벤트
   * @param seconds
   */
  onRandomSeconds?: (seconds: number) => void;
}

interface SourceProps {
  src: string;
  type: string;
}

export interface VideoPlayerElement extends ReactPlayer {
  clearUserVideoLog: () => void;
  getTrackingLog: () => TrackingLogItem | undefined;
  getRandomStickerResult: () => boolean | undefined;
  // getProgressState: () => ProgressState | undefined;
}

/**
 * VideoPlayerProps
 */
export interface VideoPlayerProps {
  /**
   * 영상 고유 id
   */
  guid?: string;
  /**
   * 사용자 id. trackingLog에서 고유 로그 식별을 위함
   */
  userId?: string;
  /**
   * 트래킹 사용여부. 트래킹 사용할 경우. localstorage에 trackingLog 에 로그가 쌓입니다.
   */
  tracking?: boolean;

  /**
   * SeekBar Props
   */
  seekbar?: SeekbarProps;

  /**
   * 랜덤 스티커
   */
  randomSticker?: RandomStickerProps;

  /**
   * 전체화면 여부
   */
  fullScreenEnabled?: boolean;

  maxPlayedSeconds?: number;

  /**
   * 이어보기. 기본값 true. guid, userId를 기준으로한 trackingLog를 참고하여 이전에 보던 부분부터 이어봄
   */
  continuePlay?: boolean;

  /**
   * 배속 목록. ex) [1.0, 1.25, 1.5, 1.75, 2.0]
   */
  speedRates?: number[];

  /**
   * 화질 목록.
   */
  qualities?: string[];

  config?: Config;
  url?: string | string[] | SourceProps[] | MediaStream;
  playing?: boolean;
  loop?: boolean;
  controls?: boolean;
  volume?: number;
  muted?: boolean;
  playbackRate?: number;
  width?: string | number;
  height?: string | number;
  style?: CSSProperties;
  className?: string;
  progressInterval?: number;
  playsinline?: boolean;
  playIcon?: ReactElement;
  previewTabIndex?: number;
  pip?: boolean;
  stopOnUnmount?: boolean;
  light?: boolean | string;
  fallback?: ReactElement;
  wrapper?: any;
  onReady?: (player: ReactPlayer) => void;
  onStart?: () => void;
  onPlay?: () => void;
  onPause?: () => void;
  onBuffer?: () => void;
  onBufferEnd?: () => void;
  onEnded?: () => void;
  onClickPreview?: (event: any) => void;
  onEnablePIP?: () => void;
  onDisablePIP?: () => void;
  onError?: (
    error: any,
    data?: any,
    hlsInstance?: any,
    hlsGlobal?: any
  ) => void;
  onDuration?: (duration: number) => void;
  onSeek?: (seconds: number) => void;
  onProgress?: (state: {
    played: number;
    playedSeconds: number;
    loaded: number;
    loadedSeconds: number;
  }) => void;

  noTouch?: boolean;
}

/**
 * 영상 SeekBar Props
 */
export interface SeekbarProps {
  /**
   * 기본값 true. true이면 이동가능, 보고있는 구간에 실시간 설문 위치 이후로는 탐색 못함.
   **/
  movable?: boolean;
  /**
   * seekbar에 실시간 설문 위치 표시. 첫번째 포인트 이후로는 탐색 불가
   **/
  points?: number[];
  /**
   * seekbar 포인터에 도달했을 때 이벤트
   * @param seekbarPoint
   */
  onPoint?: (seekbarPoint: number) => void;

  /**
   * seeking
   * @param isSeeking
   */
  onSeekChange?: (isSeeking: boolean) => void;
}

/**
 * Tracking ItemType
 */
export interface TrackType {
  /**
   * 플레이한 시간 초 ( 영상에서 4초를 본 경우 4 )
   */
  played: number;
  /**
   * 플레이한 일시
   */
  date: string;
}

/**
 * 트래킹 로그 아이템
 */
export interface TrackingLogItem {
  guid: string;
  userId?: string;
  track: TrackType[];
}

/**
 * 트래킹 로그
 */
export type TrackingLog = TrackingLogItem[];

export interface ProgressState {
  played?: number;
  playedSeconds?: number;
  loaded?: number;
  loadedSeconds?: number;
}

export const getTrackingLogs = () => {
  const str = localStorage.getItem('trackingLog');
  if (str === null) return undefined;

  return JSON.parse(str) as TrackingLog;
};
const RANDOM_STICKER_START_PERCENT = 0.5;
const RANDOM_STICKER_END_PERCENT = 0.9;
/**
 * 비디오 플레이어 컴포넌트
 */
// eslint-disable-next-line react/display-name
const VideoPlayer = forwardRef<VideoPlayerElement, VideoPlayerProps>(
  (props, ref) => {
    const videoWrapperRef = useRef<HTMLDivElement>(null); // 비디오 모듈
    const reactPlayerRef = useRef<VideoPlayerElement>(null); // 비디오 모듈

    const [playing, setPlaying] = useState<boolean>(false);

    const [progressState, setProgressState] = useState<ProgressState>({
      played: 0,
      playedSeconds: 0,
      loadedSeconds: 0,
      loaded: 0,
    });

    const [isEnd, setIsEnd] = useState<boolean>(false);

    useEffect(() => {
      if (props.onProgress && !isEnd) {
        props.onProgress({
          played: progressState.played ?? 0,
          playedSeconds: progressState.playedSeconds ?? 0,
          loadedSeconds: progressState.loadedSeconds ?? 0,
          loaded: progressState.loaded ?? 0,
        });
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [progressState, isEnd]);

    //// seek 슬라이더 변경시 play 중지
    const lastPlaying = useRef<boolean>(false);

    const [duration, setDuration] = useState<number>();

    /////////////////// 랜덤 스티커
    // const [randomStickerSeconds, setRandomStickerSeconds] = useState<number>();
    const [randomStickerProps, setRandomStickerProps] = useState<
      RandomStickerProps | undefined
    >(props.randomSticker);

    useEffect(() => {
      if (duration) {
        if (duration < 90) {
          setRandomStickerProps((prev) => ({
            ...prev,
            enabled: false,
          }));
        } else {
          setRandomStickerProps((prev) => ({
            ...prev,
            end: duration * RANDOM_STICKER_END_PERCENT, // 영상의 90%이후 에는 나오지 않는다.
            start: duration * RANDOM_STICKER_START_PERCENT, // 영상 50%~90%사이에 스티커가 나온다
          }));
        }
      }
    }, [duration]);

    /**
     * 랜덤 스티커 결과 조회
     */
    const getRandomStickerResult = useCallback(() => {
      const result = localStorage.getItem(`sr_${props.guid}_${props.userId}`);
      if (result === 'Y') {
        return true;
      } else if (result === 'N') {
        return false;
      }

      return undefined;
    }, [props.guid, props.userId]);

    /**
     * 랜덤 스티커 결과 초기화
     */
    const removeRandomStickerResult = useCallback(() => {
      localStorage.removeItem(`sr_${props.guid}_${props.userId}`);
    }, [props.guid, props.userId]);

    useEffect(() => {
      setRandomStickerProps((prev) => ({ ...prev, ...props.randomSticker }));
    }, [props.randomSticker]);

    const [
      alreadyDoneRandomSticker,
      setAlreadyDoneRandomSticker,
    ] = useState<boolean>(false);

    // 이미 랜덤 스티커가 처리되었을 경우 다시 안보이도록 처리
    useEffect(() => {
      const result = getRandomStickerResult();
      if (result !== undefined) {
        setAlreadyDoneRandomSticker(true);
      }
    }, [getRandomStickerResult]);

    const handleRandomSeconds = (seconds: number) => {
      // setRandomStickerSeconds(seconds);
      if (randomStickerProps?.onRandomSeconds) {
        randomStickerProps.onRandomSeconds(seconds);
      }
    };

    // const [
    //   fireResetRandomStickerStartRange,
    //   setFireResetRandomStickerStartRange,
    // ] = useState<boolean>();

    // // 랜덤 스티커가 플레이된 시간보다 이전이면 그 이후로 랜덤 스티커가 나오도록 수정
    // useEffect(() => {
    //   // const result = getRandomStickerResult();
    //   //
    //   // if (result !== undefined) {
    //   //   setAlreadyDoneRandomSticker(true);
    //   //   return;
    //   // }
    //
    //   if (fireResetRandomStickerStartRange === undefined) return;
    //   setFireResetRandomStickerStartRange(undefined);
    //
    //   if (randomStickerSeconds !== undefined) {
    //     if (randomStickerSeconds <= progressState.playedSeconds!) {
    //       const newStart = progressState.playedSeconds;
    //       setRandomStickerProps((prevState) => {
    //         return {
    //           ...prevState,
    //           start: newStart,
    //         };
    //       });
    //     }
    //   }
    //   // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [progressState, fireResetRandomStickerStartRange]);

    // /**
    //  * 드래그 종료 후 랜덤 스티커 시작 시간 초기화
    //  */
    // const resetRandomStickerStartRange = () => {
    //   setFireResetRandomStickerStartRange((p) => !p);
    // };

    // 랜덤 스티커 영역
    const [randomStickerArea, setRandomStickerArea] = useState<{
      width: number;
      height: number;
    }>({
      width: 0,
      height: 0,
    });

    // 랜덤 스티커 영역 설정
    useEffect(() => {
      const resizeListener = () => {
        const controller = videoWrapperRef.current;
        if (controller) {
          setRandomStickerArea({
            width: controller['offsetWidth'],
            height: controller['offsetHeight'] - 68, // 하단 컨트롤 부분 제외
          });
        }
      };

      resizeListener();
      window.addEventListener('resize', resizeListener);

      return () => {
        window.removeEventListener('resize', resizeListener);
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    /**
     * 영상 trakingLog 제거. 평가 완료시 호출
     */
    const removeTrackingLog = useCallback(() => {
      // userId: string, guid: string
      const oldTrackingLogs = getTrackingLogs();
      let newTrackingLogs;
      if (oldTrackingLogs) {
        newTrackingLogs = oldTrackingLogs.filter(
          (t) => !(t.userId === props.userId && t.guid === props.guid)
        );
      }

      if (newTrackingLogs !== undefined) {
        localStorage.setItem('trackingLog', JSON.stringify(newTrackingLogs));
      }
    }, [props.guid, props.userId]);

    /**
     * 사용자 관련 영상로그 초기화
     */
    const clearUserVideoLog = useCallback(() => {
      removeTrackingLog();
      removeRandomStickerResult();
    }, [removeTrackingLog, removeRandomStickerResult]);

    //// 트래킹 로그 기록
    const [trackingLog, setTrackingLog] = useState<TrackingLog>();

    const getTrackingLog = useCallback(() => {
      return getTrackingLogs()?.find(
        (t) => t.guid === props.guid && t.userId === props.userId
      );
    }, [props.guid, props.userId]);

    // const getProgressState = useCallback(() => {
    //   console.log('-> progressState', progressState);
    //   return progressState;
    // }, [progressState]);

    useImperativeHandle(
      ref,
      () => {
        reactPlayerRef.current!.getTrackingLog = getTrackingLog;
        reactPlayerRef.current!.getRandomStickerResult = getRandomStickerResult;

        reactPlayerRef.current!.clearUserVideoLog = clearUserVideoLog;
        // reactPlayerRef.current!.getProgressState = getProgressState;
        // console.log(
        //   '-> reactPlayerRef.current!.getProgressState',
        //   reactPlayerRef.current!.getProgressState
        // );
        return reactPlayerRef.current!;
      },
      [
        getTrackingLog,
        getRandomStickerResult,
        clearUserVideoLog,
        // getProgressState,
      ]
    );

    useEffect(() => {
      if (props.tracking) {
        setTrackingLog(getTrackingLogs() ?? []);
      }
    }, [props.tracking]);

    useEffect(() => {
      if (props.tracking && trackingLog) {
        localStorage.setItem('trackingLog', JSON.stringify(trackingLog));
      }
    }, [trackingLog, props.tracking]);

    //// 이어보기 기능
    useEffect(() => {
      if (props.continuePlay && duration) {
        const trackingLog = getTrackingLog();
        if (trackingLog) {
          const lastPlayed =
            trackingLog.track[trackingLog.track.length - 1].played;
          if (lastPlayed) {
            reactPlayerRef.current?.seekTo(lastPlayed, 'seconds');
            const playedPercent = lastPlayed / duration!;
            setPlaying(false);
            setProgressState((prev) => ({
              ...prev,
              playedSeconds: lastPlayed,
              played: playedPercent,
            }));
          }
        }
      }
    }, [props.continuePlay, duration, getTrackingLog]);

    //////////// 비디오 컨트롤러
    //// 플레이버튼
    useEffect(() => {
      setPlaying(props.playing ?? false);
    }, [props.playing]);

    const handlePlayPauseButton = () => {
      if (playing) {
        setPlaying(false);
        if (props.onPause) {
          props.onPause();
        }
      } else {
        setPlaying(true);
        if (props.onPlay) {
          props.onPlay();
        }
      }
    };

    //// 재생 시간
    // Play Time 00:00/00:00
    const [playedTime, setPlayedTime] = useState<string>('0:00');
    const [totalTime, setTotalTime] = useState<string>('');

    useEffect(() => {
      const playedSeconds =
        isEnd || progressState.played === 1
          ? duration
          : progressState.playedSeconds;

      setPlayedTime(formatSecondsToTime(playedSeconds!) ?? '0:00');
    }, [progressState, duration, isEnd]);

    //// 음량
    const [volume, setVolume] = useState<number>(props.volume!);
    useEffect(() => {
      setVolume(props.volume!);
    }, [props.volume]);
    // 음소거
    const [muted, setMuted] = useState<boolean>(props.muted!);
    useEffect(() => {
      setMuted(props.muted!);
    }, [props.muted]);

    const [isSeeking, setIsSeeking] = useState<boolean>(false);

    const handleProgress = (state: ProgressState) => {
      console.log('>>>handleProgress state', state);
      setIsEnd(false);
      if (!playing) return;

      if (
        props.maxPlayedSeconds &&
        props.maxPlayedSeconds < state.playedSeconds!
      ) {
        const maxValue = props.maxPlayedSeconds;
        const maxPlayedPercent = maxValue / duration!;
        reactPlayerRef.current?.seekTo(props.maxPlayedSeconds, 'seconds');
        setPlaying(false);
        setProgressState((prev) => ({
          ...prev,
          playedSeconds: maxValue,
          played: maxPlayedPercent,
        }));
        return;
      }

      setProgressState(state);

      if (!isSeeking && props.tracking && state.played! > 0) {
        if (props.guid && trackingLog) {
          const log = {
            played: Math.floor(state.playedSeconds!),
            date: Date(),
          };
          const itemIndex = trackingLog.findIndex(
            (res: {
              guid: string;
              userId?: string;
              track: { played: number; date: any }[];
            }) => res.guid === props.guid && res.userId === props.userId
          );

          const newArr = [...trackingLog];
          if (itemIndex > -1) {
            newArr[itemIndex]['track'].push(log);
            setTrackingLog(newArr);
          } else {
            setTrackingLog((oldArray) => [
              ...(oldArray ?? []),
              { guid: props.guid!, userId: props.userId, track: [log] },
            ]);
          }
        }
      }
    };

    // 배속
    const [speedRate, setSpeedRate] = useState<number>(1.0);

    // 확장 props 제거. ReactPlayerProps 를 구해준다
    const reactPlayerProps = { ...props };
    delete reactPlayerProps.guid;
    delete reactPlayerProps.userId;
    delete reactPlayerProps.tracking;
    delete reactPlayerProps.seekbar;
    delete reactPlayerProps.randomSticker;
    delete reactPlayerProps.fullScreenEnabled;
    delete reactPlayerProps.maxPlayedSeconds;
    delete reactPlayerProps.continuePlay;
    delete reactPlayerProps.speedRates;
    // delete reactPlayerProps.onEnded;

    return (
      <>
        <div
          ref={videoWrapperRef}
          className={`${styles.videoModule} ${
            props.noTouch ? styles.noTouch : ''
          }`}
        >
          <ReactPlayer
            {...reactPlayerProps}
            playsinline={!props.fullScreenEnabled}
            volume={volume}
            muted={muted}
            className={props.className}
            playing={playing}
            ref={reactPlayerRef}
            onProgress={handleProgress}
            onDuration={(duration) => {
              setTotalTime(formatSecondsToTime(duration));
              setDuration(duration);
              if (props.onDuration) {
                props.onDuration(duration);
              }
            }}
            onStart={() => {
              setPlaying(true);
              if (props.onStart) {
                props.onStart();
              }
            }}
            onPause={() => {
              setPlaying(false);
              if (props.onPause) {
                props.onPause();
              }
            }}
            onPlay={() => {
              setPlaying(true);
              if (props.onPlay) {
                props.onPlay();
              }
            }}
            onEnded={() => {
              setTimeout(() => {
                if (props.onEnded) {
                  setIsEnd(true);
                  props.onEnded();
                }
              }, 800);
            }}
            playbackRate={speedRate}
          />
          <button
            className={`pi ${styles.playLarge} ${
              playing ? styles.pause : styles.play
            }`}
            onClick={handlePlayPauseButton}
          />
          <div className={styles.controls}>
            <div className={styles.upArea}>
              <div className={styles.lefts}>
                <button
                  className={`pi ${styles.button} ${
                    playing ? 'pi-pause' : 'pi-play'
                  }`}
                  onClick={handlePlayPauseButton}
                />
                <span className={styles.times}>
                  {playedTime}
                  <i style={{ margin: '0 7px' }}>/</i>
                  {totalTime}
                </span>
              </div>
              <div className={styles.rights}>
                <VolumeControl
                  value={volume}
                  onChange={(volume) => {
                    setVolume(volume);
                  }}
                  muted={muted}
                  onMutedChange={(muted) => {
                    setMuted(muted);
                  }}
                />
                <SpeedRateControl
                  speedRates={props.speedRates}
                  onChange={(speedRate) => {
                    setSpeedRate(speedRate);
                  }}
                />
                <VideoQualityControl
                  qualities={props.qualities}
                  onChange={(quality) => {
                    console.log('-> quality', quality);
                    // setSpeedRate(speedRate);
                  }}
                />
                <FullScreenControl
                  fullScreenEnabled={props.fullScreenEnabled}
                  videoClassName={props.className}
                />
              </div>
            </div>
            <SeekBar
              duration={duration}
              progressState={progressState}
              movable={props.seekbar?.movable}
              points={props.seekbar?.points}
              onPoint={props.seekbar?.onPoint}
              maxMovable={props.maxPlayedSeconds}
              onChange={(event) => {
                const newValue = event.value;
                setProgressState((prev) => ({ ...prev, ...newValue }));
                reactPlayerRef.current?.seekTo(newValue.played!, 'fraction');
              }}
              onDragStart={() => {
                setIsSeeking(true);
                if (props.seekbar?.onSeekChange) {
                  props.seekbar?.onSeekChange(true);
                }

                lastPlaying.current = playing;
                setPlaying(false);
              }}
              onBarClick={() => {
                // resetRandomStickerStartRange();
              }}
              onDragEnd={() => {
                setIsSeeking(false);
                if (props.seekbar?.onSeekChange) {
                  props.seekbar?.onSeekChange(false);
                }
                // resetRandomStickerStartRange();
                setPlaying(lastPlaying.current);
              }}
            />
          </div>
          {randomStickerProps &&
            randomStickerProps.enabled &&
            // !isSeeking &&
            !alreadyDoneRandomSticker && (
              <RandomStickerButton
                area={randomStickerArea!}
                end={randomStickerProps?.end}
                start={randomStickerProps?.start}
                playedSeconds={progressState.playedSeconds!}
                deadlineSeconds={randomStickerProps?.showStickySeconds}
                onResult={(res) => {
                  // 랜덤 스티커 결과를 로컬스토리지에 저장한다.
                  const result = getRandomStickerResult();
                  if (result === undefined) {
                    localStorage.setItem(
                      `sr_${props.guid}_${props.userId}`,
                      res === 'Success' ? 'Y' : 'N'
                    );
                  }
                  // randomStickerProps.onStickerResult(res === 'Success');
                }}
                onRandomSeconds={handleRandomSeconds}
              />
            )}
        </div>
      </>
    );
  }
);

VideoPlayer.defaultProps = {
  userId: 'anonymous',
  width: '100%',
  height: '550px',
  playing: false,
  muted: false,
  pip: false,
  controls: false,
  light: false,
  volume: 0.8,
  loop: false,
  className: 'smith-ivs',
  fullScreenEnabled: true,
};

export default VideoPlayer;
