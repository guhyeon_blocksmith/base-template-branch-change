import React, { FC, useRef, useState, useEffect } from 'react';
import useMobileDetect from 'libs/hooks/useMobileDetect';
// import styles from './RandomStickerButton.module.scss';

export interface RandomStickerButtonProps {
  /**
   * 스티커 팝업 나오는 범위. parent Width, Height
   */
  area: { width: number; height: number };

  /**
   * 랜덤 시작 구간
   */
  start?: number;
  /**
   * 랜덤 종료 구간
   */
  end?: number;
  /**
   * 플레이 시간
   */
  playedSeconds: number;
  /**
   * 스티커 결과 이벤트
   * @param state
   */
  onResult?: (state: string) => void;
  /**
   * 스티커 클릭 실패 시간
   */
  deadlineSeconds?: number;

  /**
   * 랜덤으로 나오는 시간이 정해졌을 때 이벤트
   * @param seconds
   */
  onRandomSeconds?: (seconds: number) => void;

  /**
   * 스티커 이벤트 초기화
   */
  resetToggle?: boolean;

  /**
   * 스티커 보임 여부 강제
   */
  visible?: boolean;
}

/**
 * 랜덤 스티커 팝업. start와 end 구간 사이에 랜덤 스티커가 나타납니다.
 * @param controllerInfo 스티커 팝업 나오는 범위
 * @param start 랜덤 시작 구간
 * @param end 랜덤 종료 구간
 * @param playedSeconds 플레이 시간
 * @param callback 스티커 결과
 * @param showStickySeconds 스티커 노출 시간
 * @param onRandomSeconds 랜덤으로 나오는 시간이 정해졌을 때 이벤트
 * @param resetToggle 초기화
 * @param visible 보임 여부 ( UI 테스트를 위한 props ). default false
 * @constructor
 */
const RandomStickerButton: FC<RandomStickerButtonProps> = ({
  area,
  start,
  end,
  playedSeconds,
  onResult,
  deadlineSeconds = 7,
  onRandomSeconds,
  resetToggle,
  visible,
}) => {
  const isDesktop = useMobileDetect();
  const flooredPlayedSeconds = Math.floor(playedSeconds);
  const [buttonInfo, setButtonInfo] = useState<{
    width: number;
  }>({ width: 200 });
  const [buttonPosition, setButtonPosition] = useState<{
    left: number;
    top: number;
  }>();
  const [visiblePopup, setVisiblePopup] = useState<boolean>(visible ?? false);
  useEffect(() => {
    // if (visible !== undefined) {
    setVisiblePopup(visible ?? false);
    // }
  }, [visible]);
  const [randomNum, setRandomNum] = useState<number>(0);
  const doneClick = useRef<boolean>();

  // 버튼 모바일 대응
  useEffect(() => {
    if (isDesktop) {
      setButtonInfo({ width: 200 });
    } else {
      setButtonInfo({ width: 80 });
    }
  }, [isDesktop]);

  // 초기화
  useEffect(() => {
    doneClick.current = false;
  }, [resetToggle]);

  // // 실패 이벤트
  // useEffect(() => {
  //   if (isFiredEvent.current) return;
  //   if (
  //     randomNum < flooredPlayedSeconds &&
  //     randomNum + showStickySeconds <= flooredPlayedSeconds
  //   ) {
  //     if (visiblePopup) {
  //       isFiredEvent.current = true;
  //       // setIsFiredEvent(true);
  //       // 클릭 전까지는 스티커가 계속 보여야함
  //       // setVisiblePopup(false);
  //       callback && callback('Fail');
  //     }
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [flooredPlayedSeconds]);

  // 팝업 보이기
  useEffect(() => {
    if (doneClick.current) return;
    // console.log('-> randomNum', randomNum, flooredPlayedSeconds, end);
    if (
      end !== undefined &&
      flooredPlayedSeconds > 0 &&
      randomNum > 0 &&
      randomNum <= flooredPlayedSeconds &&
      flooredPlayedSeconds <= end
      // &&
      // randomNum + deadlineSeconds <= end
    ) {
      setVisiblePopup(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [flooredPlayedSeconds, end]);

  const timeOutId = useRef<NodeJS.Timeout>();

  // 버튼이 보인 후에 데드라인이 지나면, 실패
  useEffect(() => {
    if (visiblePopup) {
      // console.log('deadLineStart');
      // 버튼이 보인 후에 데드라인이 지나면, 실패
      timeOutId.current = setTimeout(() => {
        // failed
        // isFiredEvent.current = true;
        // console.log('deadLineEnd');

        // 클릭 전까지는 스티커가 계속 보여야함
        // setVisiblePopup(false);
        onResult && onResult('Fail');
      }, deadlineSeconds * 1000);
    } else {
      // 팝업이 닫히면 실패 타임아웃 종료
      if (timeOutId.current) {
        clearTimeout(timeOutId.current);
      }
    }
  }, [visiblePopup, deadlineSeconds]);

  // 랜덤 시간
  useEffect(() => {
    if (start !== undefined && end !== undefined) {
      let rangeStart = start;

      // 랜덤 시간 끝은 스티커 보여줄 수 있는 시간보다 빨라야한다.
      const rangeEnd = end - deadlineSeconds + 1;

      if (rangeStart === 0) {
        rangeStart = 1;
      }

      // 랜덤 정수
      const randomNumber =
        Math.floor(Math.random() * Math.floor(rangeEnd - rangeStart)) +
        rangeStart;
      // console.log('-> randomNumber', randomNumber);

      if (onRandomSeconds) {
        onRandomSeconds(randomNumber);
      }
      setRandomNum(randomNumber);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [start, end, resetToggle]);

  // 랜덤 위치
  useEffect(() => {
    // controller의 x에서 width, y에서 height 만큼의 범위중에
    // 스티커가 벗어나지 않는 랜덤 위치
    const containerWidth = area.width;
    const buttonWidth = buttonInfo.width;
    const containerHeight = area.height;
    const buttonHeight = buttonInfo.width * 1.425; //(285 / 200)

    if (containerWidth && containerHeight && buttonWidth && buttonHeight) {
      const leftMax = containerWidth - buttonWidth;
      const leftMin = 0;
      const left = Math.floor(Math.random() * (leftMax - leftMin)) + leftMin;
      const topMax = containerHeight - buttonHeight;
      const topMin = 0;
      const top =
        Math.floor(Math.random() * Math.abs(topMax - topMin)) + topMin;

      // console.log('-> left, top', left, top);
      // console.log(
      //   '-> area, buttonInfo, resetToggle',
      //   area,
      //   buttonInfo,
      //   resetToggle
      // );

      setButtonPosition({ left, top });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [area, buttonInfo, resetToggle]);

  if ((visible || visiblePopup) && buttonPosition) {
    return (
      // <div className={styles.stickerPopupWrap}>
      //   {buttonPosition && (
      <button
        // className={styles.button}
        onClick={() => {
          setVisiblePopup(false);
          doneClick.current = true;
          onResult && onResult('Success');
        }}
        style={{
          left: `${buttonPosition.left}px`,
          top: `${buttonPosition.top}px`,
          width: buttonInfo.width,
          position: 'absolute',
          cursor: 'pointer',
          zIndex: 99999,
        }}
      >
        {/*{buttonPosition.left}*/}
        <img
          src="/images/confirmWatch.png"
          alt=""
          style={{ width: '100%', height: '100%' }}
        />
      </button>
      // )}
      // </div>
    );
  } else {
    return null;
  }
};

export default RandomStickerButton;
