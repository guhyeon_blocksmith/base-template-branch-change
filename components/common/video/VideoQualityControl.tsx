import * as React from 'react';
import { FC, useState } from 'react';
import styles from 'components/common/video/VideoQualityControl.module.scss';

export interface VideoQualityControlProps {
  qualities?: string[];
  onChange?: (quality: string) => void;
}

export const VideoQualityControl: FC<VideoQualityControlProps> = ({
  qualities,
  onChange,
}) => {
  // const speedRates = [1.0, 1.25, 1.5, 1.75, 2.0];
  const [visibleOptions, setVisibleOptions] = useState<boolean>();
  const [quality, setSelectedQuality] = useState<string>('자동');

  if (qualities === undefined) {
    return null;
  }

  return (
    <div className={`${styles.option} ${visibleOptions && styles.on}`}>
      <button
        onClick={() => {
          setVisibleOptions((prevState) => !prevState);
        }}
      >
        {quality}
      </button>
      <div
        onMouseLeave={() => {
          setVisibleOptions(false);
        }}
      >
        <ul>
          {qualities.map((quality: string) => {
            return (
              <li key={quality}>
                <button
                  onClick={() => {
                    if (onChange) {
                      onChange(quality);
                    }
                    setSelectedQuality(quality);
                    setVisibleOptions(!visibleOptions);
                  }}
                >
                  {quality}
                </button>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};
