import * as React from 'react';
import { FC, useEffect, useMemo, useState } from 'react';
import styles from 'components/common/video/VolumeControl.module.scss';
import _ from 'lodash';
import { Slider } from 'smith-ui-components/slider';

export interface VolumeControlProps {
  value?: number;
  onChange?: (volume: number) => void;
  muted?: boolean;
  onMutedChange?: (muted: boolean) => void;
  debounceMilliSeconds?: number;
  defaultValue?: number;
}

/**
 * 음량 조절 컨트롤
 * @param muted 음소거 여부
 * @param onChange
 * @param throttleMilliSeconds
 * @param defaultValue
 * @constructor
 */
export const VolumeControl: FC<VolumeControlProps> = ({
  value,
  onChange,
  muted = false,
  onMutedChange,
  debounceMilliSeconds = 200,
  defaultValue = 0.8,
}) => {
  const normalizedDefaultValue = useMemo(() => defaultValue * 100, [
    defaultValue,
  ]);

  // 음소거
  const [mutedState, setMutedState] = useState<boolean>(muted);

  useEffect(() => {
    setMutedState(muted ?? false);
  }, [muted]);

  useEffect(() => {
    if (mutedState) {
      setSliderValue(0);
    } else {
      setSliderValue(normalizedDefaultValue);
    }
  }, [mutedState]);

  // volume
  const fireOnChange = useMemo(
    () =>
      _.debounce(
        (value: number) => {
          // volume이 0~1사이이고 slider의 value는 0~100이므로 100으로 나눠준다.
          onChange!(value / 100);
        },
        debounceMilliSeconds,
        { trailing: true }
      ),
    [onChange, debounceMilliSeconds]
  );

  const [visibleSlider, setVisibleSlider] = useState<boolean>(false);
  const [sliderValue, setSliderValue] = useState<number>();
  useEffect(() => {
    if (value !== undefined && value !== null) {
      setSliderValue(value * 100);
    }
  }, [value]);

  return (
    <div className={`${styles.volume} ${visibleSlider && styles.on} volume`}>
      <button
        className={`pi ${styles.button} ${
          mutedState ? styles.mute : 'pi-volume-up'
        }`}
        onMouseEnter={() => {
          setVisibleSlider(true);
        }}
        onClick={(event) => {
          event.preventDefault();
          // toggleMutedState
          setMutedState((prev) => {
            const newValue = !prev;
            if (prev) {
              setVisibleSlider(true);
            }
            if (onMutedChange) {
              onMutedChange(newValue);
            }

            if (onChange) {
              fireOnChange(newValue ? 0 : normalizedDefaultValue);
            }

            return newValue;
          });
        }}
      />
      <div
        className={`${styles.level} level`}
        onMouseLeave={() => {
          setVisibleSlider(false);
        }}
      >
        <Slider
          step={1}
          min={0}
          max={100}
          value={sliderValue}
          onChange={(e) => {
            setSliderValue(e.value as number);
            if (!muted && e.value === 0) {
              if (onMutedChange) {
                onMutedChange(true);
              }
            }

            if (muted && e.value > 0) {
              if (onMutedChange) {
                onMutedChange(false);
              }
            }

            if (onChange) {
              fireOnChange(e.value as number);
            }
          }}
          className={`${styles.stickWrap} stickWrap`}
        />
      </div>
    </div>
  );
};
