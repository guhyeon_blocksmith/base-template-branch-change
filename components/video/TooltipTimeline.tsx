import React, { FC, useEffect, useState } from 'react';
import TooltipTimelineDraggable from 'components/video/TooltipTimelineDraggable';
import { v4 } from 'uuid';
import { SurveySecondsArrayType, SurveySecondsCallBack } from 'types/index';

export interface TooltipTimelineProps {
  text?: string;
  currentPlayingSec: number;
  played: number;
  isShowSurvey: boolean;
  callbackSelected?: (state: SurveySecondsCallBack) => void;
  onMouseUp?: (state: any) => void | undefined;
  onMouseDown?: (state: any) => void | undefined;
  onChange?: (state: any) => void | undefined;
  maxTime: number;
  pointerDown?: (state: any) => void | undefined;
  pointerUp?: (state: any) => void | undefined;
  surveySecArray: SurveySecondsArrayType[];
}

const TooltipTimeline: FC<TooltipTimelineProps> = ({
  currentPlayingSec,
  played,
  isShowSurvey,
  callbackSelected,
  onMouseDown,
  onChange,
  onMouseUp,
  maxTime,
  surveySecArray,
}) => {
  console.log('surveySecArray::::::::', surveySecArray);
  /**
   * 드래그 상태 체크
   */
  const [isDragging, setIsDragging] = useState(false);
  const [baseWidth, setBaseWidth] = useState<number>();
  /**
   * 드래그 시작점
   */
  const [standard, setStandard] = useState<number>();
  const [selectedIndex, setSelectedIndex] = useState<number>();
  console.log('currentPlayingSec:', currentPlayingSec);
  console.log('surveySecArray------------:', surveySecArray);

  useEffect(() => {
    //
  }, [surveySecArray]);
  const handlePointerUp = () => {
    /**
     * 드래그 완료 후 dragging 값을 false로 바꿔준다.
     */
    setIsDragging(false);
  };

  /**
   * 드래그 대상에 마우스를 누를 때
   * @param e
   * @param index
   */
  const handlePointerDown = (e: any, index: number) => {
    /**
     * dragging을 true로 바꾼다.
     */
    setIsDragging(true);

    /**
     * 대상이 움직일 영역의 가로값을 구한다.
     */
    setBaseWidth(e.currentTarget.closest('.tooltipLine').clientWidth);

    /**
     * 드래그 시작점 입력(가로)
     */
    setStandard(e.pageX);

    /**
     * 대상의 번호
     */
    setSelectedIndex(index);
    e.preventDefault();
  };

  /**
   * 드래그 대상 마우스 이동
   * @param e
   */
  const handlePointerMove = (e: React.PointerEvent<HTMLDivElement>) => {
    /**
     * 드래그 중 일때만 작동 *** dragging 이 true 일때만 ***
     */
    if (isDragging) {
      /**
       * 이동중의 마우스 위치값 (가로)
       */
      const position = e.pageX;

      /**
       * 마우스 이동 거리를 드래그영역 내에서 백분율로 계산
       */
      const deltaPercent = ((position! - standard!) / baseWidth!) * 100;

      /**
       * 이동 거리를 계산 후 시작점을 새로운 값으로 입력
       */
      setStandard(position);
      const newState = [...surveySecArray];
      const newItem = newState[selectedIndex!];
      const deltaSec = (deltaPercent * maxTime!) / 100;
      if (newItem.sec + deltaSec >= maxTime!) {
        newItem.sec = maxTime!;
      } else if (newItem.sec + deltaSec <= 0) {
        newItem.sec = 0;
      } else {
        newItem.sec = newItem.sec + deltaSec;
      }
      surveySecArray[selectedIndex!] = newItem;
      callbackSelected &&
        callbackSelected({
          secArray: surveySecArray,
          secIndex: selectedIndex!,
        });
      e.stopPropagation();
    }
  };

  return (
    <>
      <div
        className="tooltipLine"
        style={{
          width: `100%`,
          // border: '1px solid red',
        }}
        onPointerMove={handlePointerMove}
        onPointerLeave={handlePointerUp}
        onPointerUp={handlePointerUp}
      >
        <input
          type="range"
          min={0}
          max={1}
          step="any"
          value={played}
          onMouseDown={() => onMouseDown && onMouseDown(true)}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            console.log('onChange e.target.value', e.target.value);
            onChange && onChange(parseFloat(e.target.value));
          }}
          onMouseUp={(e: any) => {
            console.log('onMouseUp e.target.value', e.target.value);
            onMouseUp && onMouseUp(parseFloat(e.target.value));
          }}
          style={{ width: '100%', border: '1px solid red' }}
        />
        {surveySecArray &&
          surveySecArray.map((v: SurveySecondsArrayType, index: number) => {
            return (
              <TooltipTimelineDraggable
                currentPlayingSec={currentPlayingSec}
                isShowSurvey={isShowSurvey}
                runningTime={maxTime!}
                sec={v.sec}
                key={v4()}
                onPointerDown={handlePointerDown}
                onPointerUp={handlePointerUp}
                idx={index}
              />
            );
          })}
      </div>
    </>
  );
};

export default TooltipTimeline;
