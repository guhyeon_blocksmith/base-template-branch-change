import React, { FC } from 'react';
import { observer } from 'mobx-react';

export interface TooltipTimelineProps {
  text?: string;
  currentPlayingSec: number;
  onClick?: (value: number) => void | undefined;
  isShowSurvey: boolean;
  runningTime: number;
  sec: number;
  onPointerDown?: (e: React.PointerEvent<HTMLDivElement>, idx: number) => void;
  onPointerUp?: (e: React.PointerEvent<HTMLDivElement>, idx: number) => void;
  idx: number;
}

/**
 * Tooltip Seek Navigator
 * isShowSurvey
 * runtime : total video running time
 * sec : current second from array
 * playingSeconds : current second from video
 */

const TooltipTimelineDraggable: FC<TooltipTimelineProps> = observer(
  ({
    text = '설문',
    isShowSurvey,
    runningTime,
    sec,
    currentPlayingSec,
    onClick,
    onPointerDown,
    onPointerUp,
    idx,
  }) => {
    const showSurvey = isShowSurvey ? '#1c1c1c' : '#dee2e6';
    console.log('sec-----------draggable:', sec);

    return (
      // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
      <div
        className="p-tooltip p-component p-tooltip-top p-tooltip-active"
        style={{
          left: `${(sec / runningTime!) * 100}%`,
        }}
        onClick={() => onClick && onClick(sec)}
        onPointerDown={(e: React.PointerEvent<HTMLDivElement>) =>
          onPointerDown && onPointerDown(e, idx)
        }
        onPointerUp={(e: React.PointerEvent<HTMLDivElement>) =>
          onPointerUp && onPointerUp(e, idx)
        }
      >
        <div
          className="p-tooltip-arrow"
          style={{
            borderTopColor:
              Math.floor(sec) === Math.floor(currentPlayingSec)
                ? '#BB1818'
                : showSurvey,
            cursor: 'pointer',
          }}
        />
        <button
          className="p-tooltip-text"
          style={{
            background:
              Math.floor(sec) === Math.floor(currentPlayingSec)
                ? '#BB1818'
                : showSurvey,
          }}
        >
          <span
            style={{
              color: isShowSurvey
                ? '#ffffff'
                : Math.ceil(sec) === Math.ceil(currentPlayingSec)
                ? '#ffffff'
                : '#1c1c1c',
              cursor: 'pointer',
            }}
          >
            {text}[{sec}][{currentPlayingSec}]
          </span>
        </button>
      </div>
    );
  }
);

export default TooltipTimelineDraggable;
