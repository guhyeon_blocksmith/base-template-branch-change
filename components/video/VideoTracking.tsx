import React, { FC, useEffect, useState } from 'react';
import styles from './VideoTracking.module.scss';
import { observer } from 'mobx-react';
import { v4 } from 'uuid';
import _ from 'lodash';

export interface videoTrackingProps {
  duration: number;
  playedSeconds: number;
  guid: string;
  userId: string;
  callback?: (props: boolean) => void;
  showProgress?: boolean;
}

interface trackingLogType {
  guid: string;
  userId: string;
  track: { played: number; date: any }[];
}

const VideoTracking: FC<videoTrackingProps> = observer(
  ({ duration, playedSeconds, guid, userId, callback, showProgress }) => {
    const [watchData, setWatchData] = useState<trackingLogType[]>();
    const [isReady, setIsReady] = useState<boolean>(false);
    const [watchAll, setWatchAll] = useState<boolean>(false);
    const flooredDuration = Math.floor(duration);
    const flooredPlayedSeconds = Math.floor(playedSeconds);

    const onValidationAllWatchedByLog = () => {
      // 영상 전체 track 을 봤을 때 true
      const guidIndex = watchData!.findIndex(
        (res: trackingLogType) => res.guid === guid && res.userId === userId
      );
      if (guidIndex !== -1) {
        const uniqLog = _.uniqBy(watchData![guidIndex].track, function (e) {
          return e.played;
        });
        const uniqSortBy = _.sortBy(uniqLog, 'played');

        // 모든 초에 대한 모든 데이터가 있으면 모두 보았음
        // console.log('uniqSortBy.length:', uniqSortBy.length);
        // console.log('flooredDuration:', flooredDuration);
        // duration 은 0을 포함하므로 length 는 duration 보다 하나가 많다
        if (uniqSortBy.length === flooredDuration + 1) {
          setWatchAll(true);
          callback && callback(true);
        } else {
          // 모든 초에 대한 모든 데이터가 없으면 %로 보았는지 판단
          const result = (uniqSortBy.length / flooredDuration) * 100;
          // 95% 시청한 경우 모두 본것으로 판단
          if (Math.floor(result) > 94) {
            setWatchAll(true);
            callback && callback(true);
          } else {
            setWatchAll(false);
            callback && callback(false);
          }
        }
      }
    };

    useEffect(() => {
      setWatchData(JSON.parse(localStorage.getItem('trackingLog') as any));
      flooredPlayedSeconds === 0 && setIsReady(true);
      // 영상의 마지막에 완료 여부 판단
      if (
        (isReady && flooredPlayedSeconds === 0) ||
        (isReady && flooredPlayedSeconds === flooredDuration)
      ) {
        onValidationAllWatchedByLog();
      }
    }, [isReady, playedSeconds, watchAll, flooredDuration]);

    const onPlayedProgressRenderer = () => {
      // 재생에 대한 Progress
      return [...Array(flooredDuration)].map((_, i) => {
        const guidIndex = watchData!.findIndex(
          (res: trackingLogType) => res.guid === guid && res.userId === userId
        );
        const seenBefore = () => {
          if (guidIndex !== -1) {
            const uniqLog = watchData![guidIndex].track;
            const seen = uniqLog.findIndex(
              (res: { played: number; date: any }) => res.played === i
            );
            return seen !== -1;
          } else {
            if (flooredPlayedSeconds === i || flooredPlayedSeconds < i) {
              return true;
            } else {
              return false;
            }
          }
        };
        return (
          <button
            className={`${i < flooredPlayedSeconds ? styles.checked : ''} ${
              seenBefore() ? styles.played : ''
            }`}
            key={v4()}
          />
        );
      });
    };

    return (
      <>
        {showProgress && (
          <div className={styles.piecesWrap}>
            {isReady && onPlayedProgressRenderer()}
          </div>
        )}
      </>
    );
  }
);

export default VideoTracking;
