import React, { FC, ReactNode, useEffect, useState } from 'react';
import { v4 } from 'uuid';
import axios from 'axios';

/**
 * videoId: youtube thumbnail, validator
 * youtube Data API https://developers.google.com/youtube/v3/docs/videos/list?hl=ko
 * @constructor
 * part 매개변수는 API 응답이 포함하는 video 리소스 속성 하나 이상의 쉼표로 구분된 목록을 지정합니다.
 * id, snippet, contentDetails, fileDetails, liveStreamingDetails, player, processingDetails, recordingDetails, statistics, status, suggestions, topicDetails
 * todo move to smith-ivs
 * */

export interface Default {
  url: string;
  width: number;
  height: number;
}

export interface Medium {
  url: string;
  width: number;
  height: number;
}

export interface High {
  url: string;
  width: number;
  height: number;
}

export interface Standard {
  url: string;
  width: number;
  height: number;
}

export interface Maxres {
  url: string;
  width: number;
  height: number;
}

export interface Thumbnails {
  default: Default;
  medium: Medium;
  high: High;
  standard: Standard;
  maxres: Maxres;
}

export interface Localized {
  title: string;
  description: string;
}

export interface Snippet {
  publishedAt: Date;
  channelId: string;
  title: string;
  description: string;
  thumbnails: Thumbnails;
  channelTitle: string;
  tags: string[];
  categoryId: string;
  liveBroadcastContent: string;
  defaultLanguage: string;
  localized: Localized;
  defaultAudioLanguage: string;
}

export interface Item {
  kind: string;
  etag: string;
  id: string;
  snippet: Snippet;
  statistics: Statistics;
}

export interface Statistics {
  viewCount: number;
  likeCount: number;
  dislikeCount: number;
  favoriteCount: number;
  commentCount: number;
}

export interface PageInfo {
  totalResults: number;
  resultsPerPage: number;
}

export interface RootObject {
  kind: string;
  etag: string;
  items: Item[];
  pageInfo: PageInfo;
}

export interface CallbackType {
  isValid: boolean;
  status: string;
  urlParse: UrlType | undefined;
  snippet?: any | undefined;
  blob?: any;
}

interface YoutubeType {
  children?: ReactNode;
  videoUrl?: string;
  callback?: (state: CallbackType) => void;
  mediaConfig?: ShowVideoType;
  showThumbNail?: boolean;
}

interface ShowVideoType {
  apiKey?: string;
  showVideo?: boolean;
}

export interface UrlType {
  href: string;
  protocol: string;
  username?: string;
  password?: string;
  host: string;
  hostname: string;
  port: string;
  pathname: string;
  search?: string;
  hash?: string;
  origin: string;
  searchParams: any;
}

// export interface BlobType {
//   readonly size: number;
//   readonly type: string;
//   slice(start?: number, end?: number, contentType?: string): Blob;
// }

const YouTubeValidator: FC<YoutubeType> = ({
  children,
  videoUrl,
  callback,
  mediaConfig,
  showThumbNail,
}) => {
  const [localThumbNail, setLocalThumbNail] = useState<string>();

  useEffect(() => {
    if (videoUrl && validURL(videoUrl)) {
      if (videoUrl !== undefined) {
        onSnippetInfo(videoUrl).then();
      }
    } else {
      if (videoUrl) {
        callback &&
          callback({
            isValid: false,
            status: 'FAIL',
            urlParse: undefined,
            snippet: undefined,
          });
      }
    }
  }, [videoUrl]);

  const onSnippetInfo = async (url: string) => {
    const parseUrl = new URL(url);
    const parseData = {
      href: parseUrl.href,
      protocol: parseUrl.protocol,
      username: parseUrl.username,
      password: parseUrl.password,
      host: parseUrl.host,
      hostname: parseUrl.hostname,
      port: parseUrl.port,
      pathname: parseUrl.pathname,
      search: parseUrl.search,
      hash: parseUrl.hash,
      origin: parseUrl.origin,
      searchParams: parseUrl.searchParams,
    };
    const videoId =
      parseUrl.pathname === '/watch'
        ? parseUrl.search.replace('?v=', '')
        : parseUrl.pathname.substr(1);

    await axios(
      `https://www.googleapis.com/youtube/v3/videos?id=${videoId}&part=snippet,contentDetails,statistics&key=${mediaConfig?.apiKey}`
    )
      .then((r) => {
        if (r.status === 200) {
          onParseUrl(parseData, r.data);
        }
      })
      .catch((error: any) => error);

    await axios(
      `https://www.googleapis.com/youtube/v3/videos?id=${videoId}&part=status&key=${mediaConfig?.apiKey}`
    )
      .then((r) => {
        if (r.status === 200) {
          console.log(r);
        }
      })
      .catch((error: any) => error);
  };

  // const extractBlob = async (url: string) => {
  //   await fetch(url)
  //     .then(function (response) {
  //       return response.blob();
  //     })
  //     .then(function (blob) {
  //       // here the image is a blob
  //       const fileBlob: BlobType = blob;
  //       return fileBlob;
  //     })
  //     .catch((error) => console.log('error:', error));
  // };

  const onParseUrl = async (parseData: any, snippet: RootObject) => {
    if (onYoutubeUrlValidator(parseData)) {
      if (snippet.items[0] === undefined) {
        // 올바른 url 이 아님
        callback &&
          callback({
            isValid: false,
            status: 'FAIL',
            urlParse: parseData,
            snippet: undefined,
          });
      } else {
        setLocalThumbNail(snippet.items[0].snippet.thumbnails.medium.url);
        callback &&
          callback({
            isValid: true,
            status: 'SUCCESS',
            urlParse: parseData,
            snippet: snippet,
          });
      }
    } else {
      callback &&
        callback({
          isValid: false,
          status: 'FAIL',
          urlParse: parseData,
          snippet: undefined,
        });
    }
  };

  // const onThumbNailBlob = () => {
  //   if (youtubeSnippet) {
  //     if (videoId !== undefined && isValid === 'SUCCESS') {
  //       const youtubeUrlFromApi =
  //         youtubeSnippet?.items[0].snippet.thumbnails.default.url;
  //       fetch(youtubeUrlFromApi)
  //         .then(function (response) {
  //           return response.blob();
  //         })
  //         .then(function (blob) {
  //           // here the image is a blob
  //           const fileBlob: BlobType = blob;
  //           return fileBlob;
  //         })
  //         .catch((error) => console.log('error:', error));
  //     }
  //   }
  // };

  const onYoutubeUrlValidator = (parseData: UrlType) => {
    console.log('parseData:', parseData);
    const { host, hostname, pathname, origin } = parseData;
    // url type A
    if (host === 'www.youtube.com' || hostname === 'www.youtube.com') {
      if (pathname === '/watch') {
        return true;
      } else {
        return false;
      }
    } else {
      // url type B
      if (
        hostname === 'youtu.be' &&
        pathname !== '' &&
        origin === 'https://youtu.be'
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  const validURL = (str: string) => {
    const pattern = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i'
    ); // fragment locator
    return !!pattern.test(str);
  };

  return (
    <React.Fragment key={v4()}>
      {showThumbNail && <img src={localThumbNail} alt={''} />}
      {children && children}
    </React.Fragment>
  );
};

export default YouTubeValidator;
