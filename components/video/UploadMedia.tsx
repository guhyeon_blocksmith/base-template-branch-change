import React, { FC, useRef } from 'react';
import { alert } from 'smith-ui-components/dialog';
import { getMetadata, getThumbnails } from 'video-metadata-thumbnails';

/**
 * videoId: youtube thumbnail
 * @constructor
 */

interface UploadType {
  callbackStep?: (state: number) => void;
  callbackDuration?: (state: number) => void;
  callbackFileInfo?: (state: FileMetaInfoType | undefined) => void;
  callbackMetaData?: (state: MetaType | undefined) => void;
  callbackThumbnail?: (state: string | undefined) => void;
}

interface MetaType {
  duration: number;
  height: number;
  width: number;
}

interface FileMetaInfoType {
  lastModified: number;
  lastModifiedDate: any | undefined;
  name: string;
  size: number;
  type: string;
  webkitRelativePath: string;
}

const UploadMedia: FC<UploadType> = ({
  callbackStep,
  callbackFileInfo,
  callbackMetaData,
  callbackDuration,
  callbackThumbnail,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const uploadVideo = async (e: any) => {
    callbackStep && callbackStep(1);
    const file =
      e.target.files[0] !== undefined ? e.target.files[0] : 'not support/';
    // video type check
    const checkVideoType = file.type.split('/');
    if (checkVideoType[0] !== 'video') {
      await alert('video type only support');
      callbackStep && callbackStep(-1);
      inputRef.current!.value = '';
    } else {
      // 파일 기본 정보
      callbackFileInfo && callbackFileInfo(file);
      // 파일 duration 정보 얻기
      await getMetadata(file)
        .then((res) => {
          callbackStep && callbackStep(2);
          callbackMetaData && callbackMetaData(res);
          callbackDuration && callbackDuration(res.duration);
          uploadS3(file);
        })
        .catch((error) => {
          // it dosen't matter getMetadata support or not.
          callbackStep && callbackStep(2);
          callbackMetaData && callbackMetaData(error);
          uploadS3(file);
          // console.log('error:', error);
        });
      await getThumbnails(file, {
        base64: false,
        end: 0,
        interval: 0,
        scale: 0,
        sprite: false,
        start: 0,
        quality: 6,
      })
        .then((res) => {
          blobToImage(res);
        })
        .catch(() => {
          alert("Uploader doesn't support this media format");
          inputRef.current!.value = '';
        });
    }
  };

  const blobToImage = (blob: any) => {
    const fileMeta = URL.createObjectURL(blob[0].blob);
    callbackThumbnail && callbackThumbnail(fileMeta);
  };

  const uploadS3 = async (file: any) => {
    const filename = encodeURIComponent(file.name);
    const res = await fetch(`/api/s3-upload?file=${filename}`);
    if (res.status === 200) {
      const { url, fields } = await res.json();
      const formData = new FormData();

      Object.entries({ ...fields, file }).forEach(([key, value]) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        formData.append(key, value);
      });
      callbackStep && callbackStep(3);
      await fetch(url, {
        method: 'POST',
        body: formData,
      })
        .then(() => {
          callbackStep && callbackStep(4);
          // console.log('uploadS3:', res);
        })
        .catch((error) => {
          console.warn('error from S3:', error);
          alert('Media Converter does not support media format');
          callbackMetaData && callbackMetaData(undefined);
          callbackStep && callbackStep(-1);
        });
    } else {
      console.warn('s3-upload api is dead', res);
    }
  };

  return (
    <>
      <input
        onChange={uploadVideo}
        type="file"
        id={'videoInput'}
        ref={inputRef}
      />
    </>
  );
};

export default UploadMedia;
