import React, { FC, useState } from 'react';
import { Button } from 'smith-ui-components/button';
import { alert } from 'smith-ui-components/dialog';
import { Slider } from 'smith-ui-components/slider';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';

interface SurveyShowContentsType {
  data: any;
  callbackClose?: (state: any) => void | undefined;
}

const SurveyShowPoint: FC<SurveyShowContentsType> = ({
  data,
  callbackClose,
}) => {
  const [slidePoint, setSlidePoint] = useState<number>(0);
  const [currentPoint, setCurrentPoint] = useState<number>(0);
  const { questionnaire } = data;
  const { answers } = questionnaire;
  const maxPoint = _.maxBy(answers);

  const handleValidations = () => {
    if (slidePoint === 0) {
      alert('답변해 주세요.').then();
    } else {
      callbackClose && callbackClose(true);
    }
  };

  const pointRenderer = () => {
    return answers.map((row: number) => {
      return (
        <div className="p-col" key={uuidv4()}>
          <Button
            onClick={() => {
              setSlidePoint(row);
              setCurrentPoint(row);
            }}
            style={{
              background: '#ffffff',
              color:
                Number(currentPoint) === Number(row) ? '#ba3737' : '#000000',
              margin: '2px',
              border: '0px solid #ffffff',
              fontSize: '2rem',
              padding: '0',
            }}
          >
            {row}
          </Button>
        </div>
      );
    });
  };

  return (
    <>
      <Slider
        value={slidePoint}
        onChange={(e) => {
          setSlidePoint(Number(e.value));
          setCurrentPoint(Number(e.value));
        }}
        step={0}
        max={Number(maxPoint)}
      />
      <br />
      <div className="card" key={uuidv4()}>
        <div className="p-grid">{pointRenderer()}</div>
      </div>
      <br />
      <div style={{ textAlign: 'center' }}>
        <Button label={'확인'} onClick={handleValidations} />
      </div>
    </>
  );
};

export default SurveyShowPoint;
