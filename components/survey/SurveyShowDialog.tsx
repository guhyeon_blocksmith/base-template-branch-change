import React, { FC, useEffect, useState } from 'react';
import { Dialog } from 'smith-ui-components/dialog';
import { Button } from 'smith-ui-components/button';
import SurveyShowContents from 'components/survey/SurveyShowContents';
import { surveyShowMode } from 'types/enum';

interface SurveyType {
  callback?: (obj?: any) => void | undefined;
  closable?: boolean;
  isShowSurvey?: boolean;
  currentPlayingSec: number;
  videoId: string;
  config: SurveyConfigType;
  surveyData: SurveyDataType;
}

interface SurveyConfigType {
  viewMode: surveyShowMode;
  closable: boolean;
  isShowSurvey: boolean;
}

interface SurveyQuestionType {
  type: string;
  question: string;
  answers: Array<string>;
}

export interface SurveyDataType {
  guid: string;
  playedSec: number;
  questionnaire: SurveyQuestionType;
}

/**
 *  callback
 *  currentPlayingSec 현재 진행 시간
 *  isShowSurvey 모달 노출 여부
 *  videoId 영상 pk
 *  todo move to smith-ivs
 *  기획에 Dialog 형식 없으므로 분리 후 개발 보류
 * */

const SurveyShowDialog: FC<SurveyType> = ({
  callback,
  currentPlayingSec,
  videoId,
  config,
  surveyData,
}) => {
  const { viewMode, closable } = config;
  const [displayBasic, setDisplayBasic] = useState<boolean>(false);
  const [answered, setAnswered] = useState<boolean>(false);
  const [position, setPosition] = useState<string>('center');

  // todo api call (sec, videoId)
  useEffect(() => {
    setAnswered(false);
    surveyShowMode.modal && onDialog('displayBasic');
    onDialog('displayBasic');
  }, [currentPlayingSec, videoId]);

  const dialogFuncMap = {
    displayBasic: setDisplayBasic,
  };

  const onDialog = (name: string) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    dialogFuncMap[`${name}`](true);
    callback && callback(false);
    if (position) {
      setPosition(position);
    }
  };

  const onHide = (name: string) => {
    if (viewMode === surveyShowMode.modal) {
      //
    } else {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      dialogFuncMap[`${name}`](false);
      //console.log('onHide');
      callback && callback(true);
    }
  };

  const renderFooter = (name: string) => {
    return (
      <>
        {closable && (
          <Button
            label="Close"
            icon="pi pi-check"
            onClick={() => onHide(name)}
            className="p-button-text"
          />
        )}
        {/*<Button label="Yes" icon="pi pi-check" onClick={() => onHide(name)} />*/}
      </>
    );
  };

  return (
    <Dialog
      header={surveyData && surveyData.questionnaire.question!}
      closable={closable}
      visible={displayBasic}
      style={{ width: '50vw' }}
      footer={renderFooter('displayBasic')}
      onHide={() => onHide('displayBasic')}
    >
      <SurveyShowContents
        data={surveyData}
        callbackClose={(close) => {
          if (close) {
            onHide('displayBasic');
            setAnswered(true);
          }
        }}
        viewMode={viewMode}
        answered={answered}
      />
    </Dialog>
  );
};

export default SurveyShowDialog;
