import React, { FC, useState } from 'react';
import { Button } from 'smith-ui-components/button';
import { StreamType } from 'smith-ivs/dist/types';
import SurveyRegisterEditor from 'components/survey/SurveyRegisterEditor';
import { surveyRegistrationMode } from 'types/enum';
import { SurveySecondsArrayType } from 'types/index';

interface SurveyType {
  playedSeconds: number;
  callCallback: (obj?: any) => void | undefined;
  config: StreamType;
  surveyMode: surveyRegistrationMode;
  surveySecArray: SurveySecondsArrayType[];
}

const SurveyRegister: FC<SurveyType> = ({
  playedSeconds,
  callCallback,
  config,
  surveyMode,
  surveySecArray,
}) => {
  const convertPlayedSec = Math.floor(playedSeconds);
  const [surveyShowSecArrayIntoRegister] = useState<SurveySecondsArrayType[]>(
    surveySecArray
  );

  const onRemovePlaySecOnArray = () => {
    const removeIdx = surveyShowSecArrayIntoRegister!.findIndex(
      (row) => row.sec === convertPlayedSec
    );
    surveyShowSecArrayIntoRegister.splice(removeIdx, 1);
    callCallback({
      playing: true,
      surveyShowSecArray: surveyShowSecArrayIntoRegister,
    });
  };

  const onAddPlaySecOnArray = () => {
    const addSurvey = surveyShowSecArrayIntoRegister.push({
      sec: playedSeconds,
    });
    callCallback({
      playing: false,
      surveyShowSecArray: addSurvey,
    });
  };

  return (
    <>
      {surveyMode === 'READY' ? (
        <>
          <Button
            label={`영상의 ${playedSeconds} 초에 설문 추가`}
            onClick={onAddPlaySecOnArray}
          />
        </>
      ) : (
        <>
          <SurveyRegisterEditor
            playedSeconds={playedSeconds}
            surveyMode={surveyMode}
            guid={config.metaInfo.guid}
            onRemovePlaySecOnArray={onRemovePlaySecOnArray}
          />
        </>
      )}
    </>
  );
};

export default SurveyRegister;
