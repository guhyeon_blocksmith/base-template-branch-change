import React, { FC, useEffect, useState } from 'react';
import SurveyShowContents from 'components/survey/SurveyShowContents';
import { surveyShowMode } from 'types/enum';
import SurveyShowDialog from 'components/survey/SurveyShowDialog';
import { SurveyItemType } from 'types/index';
import { toJS } from 'mobx';

interface SurveyType {
  callback?: (obj?: any) => void | undefined;
  closable?: boolean;
  isShowSurvey?: boolean;
  currentPlayingSec: number;
  config: SurveyConfigType;
  surveyList: SurveyItemType[];
}

interface SurveyConfigType {
  viewMode: surveyShowMode;
  closable: boolean;
  isShowSurvey: boolean;
  guid: string;
}

interface SurveyQuestionType {
  type: string;
  question: string;
  answers: Array<string>;
}

export interface SurveyDataType {
  guid: string;
  playedSec: number;
  questionnaire: SurveyQuestionType;
}

/**
 *  callback
 *  currentPlayingSec 현재 진행 시간
 *  dialogSec 설문 노출 시간
 *  isShowSurvey 모달 노출 여부
 *  videoId 영상 pk
 *  todo move to smith-ivs module
 * */

const SurveyShow: FC<SurveyType> = ({
  callback,
  currentPlayingSec,
  config,
  surveyList,
}) => {
  const { viewMode, isShowSurvey, guid } = config;
  const [shown, setShown] = useState<boolean>(false);
  const [answered, setAnswered] = useState<boolean>(false);
  const [surveyData, setSurveyData] = useState<SurveyDataType>({
    guid: '',
    playedSec: 0,
    questionnaire: {
      type: '',
      question: '',
      answers: [],
    },
  });
  console.log('SurveyShow currentPlayingSec:', currentPlayingSec);
  const surveyListToJs = toJS(surveyList);
  console.log('SurveyShow surveyList:', surveyListToJs);
  useEffect(() => {
    setAnswered(false);

    const showSurveyScreen = surveyListToJs.findIndex(
      (row: SurveyItemType) =>
        Math.floor(Number(row.playedSec)) === currentPlayingSec
    );
    if (showSurveyScreen !== -1) {
      if (!shown) {
        if (isShowSurvey) {
          setAnswered(false);
          // 자료 찾아 가져오면 설문 일시정지
          callback && callback(false);
          setSurveyData(surveyListToJs[showSurveyScreen]);
        }
      }
    } else {
      setShown(false);
    }

    return () => {
      setAnswered(false);
      setSurveyData({
        guid: '',
        playedSec: 0,
        questionnaire: {
          type: '',
          question: '',
          answers: [],
        },
      });
    };
  }, [currentPlayingSec, guid]);

  const SurveyShowPageRenderer = () => {
    return surveyData ? (
      <SurveyShowContents
        data={surveyData}
        callbackClose={(close) => {
          if (close) {
            setAnswered(true);
            callback && callback(true);
          }
        }}
        viewMode={viewMode}
        answered={answered}
      />
    ) : null;
  };
  return viewMode === surveyShowMode.modal ? (
    <SurveyShowDialog
      surveyData={surveyData}
      videoId={guid}
      isShowSurvey={isShowSurvey}
      currentPlayingSec={currentPlayingSec}
      config={config}
    />
  ) : (
    <SurveyShowPageRenderer />
  );
};

export default SurveyShow;
