import React, { FC, useState } from 'react';
import { InputText } from 'smith-ui-components/inputtext';
import { InputTextarea } from 'smith-ui-components/inputtextarea';
import { surveyRegistrationMode } from 'types/enum';
import { Button } from 'smith-ui-components/button';
import { alert } from 'smith-ui-components/dialog';

interface SurveyType {
  guid: string;
  playedSeconds: number;
  surveyMode: surveyRegistrationMode;
  onRemovePlaySecOnArray: (obj: any) => void;
}

const SurveyRegisterEditor: FC<SurveyType> = ({
  guid,
  playedSeconds,
  surveyMode,
  onRemovePlaySecOnArray,
}) => {
  console.log('surveyMode:', surveyMode);
  const [editorText, setEditorText] = useState<string>('');

  const handleSubmit = async () => {
    if (editorText === '') {
      alert('내용을 넣어주세요.').then();
    } else {
      await fetch(`/api/vod-node`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          guid,
          playedSec: playedSeconds,
          questionnaire: {
            type: 'short',
            question: editorText,
            answers: [],
          },
        }),
      })
        .then(() => {
          alert('저장되었습니다.').then();
        })
        .catch((error: any) => console.log('error:', error));
    }
  };

  return (
    <>
      <strong className="contTitle">설문 등록</strong>
      <table className="defTable" style={{ marginTop: '30px' }}>
        <colgroup>
          <col style={{ width: '180px' }} />
          <col style={{ width: 'calc(50% - 120px)' }} />
        </colgroup>
        <tbody>
          <tr>
            <th>설문 아이디</th>
            <td>신규</td>
          </tr>
          <tr>
            <th>영상 아이디</th>
            <td>{guid}</td>
          </tr>
          <tr>
            <th>등록 시간(초) </th>
            <td>
              <InputText
                disabled={true}
                value={playedSeconds}
                onChange={() => null}
              />
            </td>
          </tr>
          <tr>
            <th>설문유형</th>
            <td>주관식/객관식/포인트 (준비중)</td>
          </tr>
          <tr>
            <th>질문</th>
            <td>
              <InputTextarea
                style={{ width: '290px', height: '180px' }}
                onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
                  setEditorText(e.target.value)
                }
                value={editorText}
              />
            </td>
          </tr>
        </tbody>
      </table>
      <div style={{ width: '100%', textAlign: 'center' }}>
        <Button label={'등록'} onClick={handleSubmit} />{' '}
        <Button label={'닫기'} onClick={onRemovePlaySecOnArray} />
      </div>
    </>
  );
};

export default SurveyRegisterEditor;
