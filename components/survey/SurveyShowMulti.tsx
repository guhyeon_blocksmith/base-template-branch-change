import React, { FC, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Button } from 'smith-ui-components/button';
import { Checkbox } from 'smith-ui-components/checkbox';
import { alert } from 'smith-ui-components/dialog';
interface SurveyShowContentsType {
  data: any;
  callbackClose?: (state: any) => void | undefined;
}

const SurveyShowMulti: FC<SurveyShowContentsType> = ({
  data,
  callbackClose,
}) => {
  const [checkBox, setCheckBox] = useState<number[]>([]);
  const [forceUpdate, setForceUpdate] = useState<boolean>(false);
  const { questionnaire } = data;
  const { answers } = questionnaire;

  useEffect(() => {
    //
  }, [forceUpdate]);

  const handleValidations = () => {
    if (checkBox.length === 0) {
      alert('답변해 주세요.').then();
    } else {
      callbackClose && callbackClose(true);
    }
  };

  const render = answers.map((answer: number, idx: number) => {
    return (
      <div style={{ marginTop: '5px' }} key={uuidv4()}>
        <span className="p-ml-2">
          <Checkbox
            inputId={idx.toString()}
            name={answer.toString()}
            value={idx}
            onChange={(e) => {
              if (checkBox.indexOf(idx) === -1) {
                checkBox.push(Number(e.target.value));
              } else {
                const filtered = checkBox.filter((res: number) => res !== idx);
                setCheckBox(filtered);
              }
              setForceUpdate(!forceUpdate);
            }}
            checked={checkBox.indexOf(idx) !== -1}
          />{' '}
          <label htmlFor={idx.toString()}>{answer}</label>
        </span>
        <br />
      </div>
    );
  });

  return (
    <>
      <div className="card" key={uuidv4()}>
        {render}
      </div>
      <br />
      <div style={{ textAlign: 'center' }}>
        <Button label={'확인'} onClick={handleValidations} />
      </div>
    </>
  );
};

export default SurveyShowMulti;
