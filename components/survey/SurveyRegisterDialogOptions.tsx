import React, { FC } from 'react';
import { InputSwitch } from 'smith-ui-components/inputswitch';
import { Button } from 'smith-ui-components/button';

const SurveyRegisterDialogOptions: FC = () => {
  return (
    <>
      <strong className="contTitle">모달 설정</strong>
      <table className="defTable" style={{ marginTop: '30px' }}>
        <colgroup>
          <col style={{ width: '180px' }} />
          <col style={{ width: 'calc(50% - 120px)' }} />
        </colgroup>
        <tbody>
          <tr>
            <th>닫기 버튼 사용</th>
            <td>
              <InputSwitch
                checked={true}
                // onChange={() => setClosable(!closable)}
              />
            </td>
          </tr>
          <tr>
            <th>위치</th>
            <td>
              <div className="p-grid p-dir-col">
                <div className="p-col">
                  <Button
                    label="Left"
                    icon="pi pi-arrow-right"
                    className="p-button-sm p-m-2"
                  />
                  <Button
                    label="Right"
                    icon="pi pi-arrow-left"
                    className="p-button-sm p-m-2"
                  />
                  <Button
                    label="Top"
                    icon="pi pi-arrow-down"
                    className="p-button-sm p-m-2"
                  />
                  <Button
                    label="TopLeft"
                    icon="pi pi-arrow-down"
                    className="p-button-sm p-m-2"
                  />
                  <Button
                    label="TopRight"
                    icon="pi pi-arrow-down"
                    className="p-button-sm  p-m-2"
                  />
                </div>
                <div className="p-col">
                  <Button
                    label="Bottom"
                    icon="pi pi-arrow-up"
                    className="p-button-sm p-m-2"
                  />
                  <Button
                    label="BottomLeft"
                    icon="pi pi-arrow-up"
                    className="p-button-sm p-m-2"
                  />
                  <Button
                    label="BottomRight"
                    icon="pi pi-arrow-up"
                    className="p-button-sm p-m-2"
                  />
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
};

export default SurveyRegisterDialogOptions;
