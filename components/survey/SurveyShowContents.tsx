import React, { FC } from 'react';
import SurveyShowMulti from 'components/survey/SurveyShowMulti';
import SurveyShowSingle from 'components/survey/SurveyShowSingle';
import SurveyShowShort from 'components/survey/SurveyShowShort';
import SurveyShowPoint from 'components/survey/SurveyShowPoint';
import { surveyShowMode } from 'types/enum';
import { v4 } from 'uuid';

interface SurveyShowContentsType {
  data: any;
  callbackClose?: (state: any) => void | undefined;
  viewMode: surveyShowMode;
  answered: boolean;
}

const SurveyShowContents: FC<SurveyShowContentsType> = ({
  data,
  callbackClose,
  viewMode,
  answered,
}) => {
  const { questionnaire } = data;
  const { type } = questionnaire;

  const baseRenderer = () => {
    switch (type) {
      case 'multi':
        return (
          <SurveyShowMulti
            data={data}
            callbackClose={callbackClose}
            key={v4()}
          />
        );
        break;
      case 'single':
        return (
          <SurveyShowSingle
            data={data}
            callbackClose={callbackClose}
            key={v4()}
          />
        );
        break;
      case 'short':
        return <SurveyShowShort callbackClose={callbackClose} key={v4()} />;
        break;
      case 'point':
        return (
          <SurveyShowPoint
            data={data}
            callbackClose={callbackClose}
            key={v4()}
          />
        );
        break;
      default:
        return null;
        break;
    }
  };

  const normalRenderer = () => {
    return (
      data && (
        <React.Fragment>
          {data.guid}
          <h4>{data.questionnaire.question!}</h4>
          {baseRenderer()}
        </React.Fragment>
      )
    );
  };

  return answered
    ? null
    : viewMode === surveyShowMode.modal
    ? baseRenderer()
    : normalRenderer();
};

export default SurveyShowContents;
