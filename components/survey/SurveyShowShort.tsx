import React, { FC, useState } from 'react';
import { Button } from 'smith-ui-components/button';
import { alert } from 'smith-ui-components/dialog';
import { InputTextarea } from 'smith-ui-components/inputtextarea';

interface SurveyShowContentsType {
  callbackClose?: (state: any) => void | undefined;
}

const SurveyShowShort: FC<SurveyShowContentsType> = ({ callbackClose }) => {
  const [inputText, setInputText] = useState<string>('');

  const handleValidations = () => {
    if (inputText === '') {
      alert('답변해 주세요.').then();
    } else {
      callbackClose && callbackClose(true);
    }
  };

  return (
    <React.Fragment>
      <div className="card">
        <InputTextarea
          value={inputText}
          onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
            e.preventDefault();
            setInputText(e.target.value);
          }}
          rows={5}
          style={{ width: '100%' }}
        />
        <br />
        <div style={{ textAlign: 'center' }}>
          <Button label={'확인'} onClick={handleValidations} />
        </div>
      </div>
    </React.Fragment>
  );
};

export default SurveyShowShort;
