import React, { FC, useState } from 'react';
import { RadioButton } from 'smith-ui-components/radiobutton';
import { v4 as uuidv4 } from 'uuid';
import { Button } from 'smith-ui-components/button';
import { alert } from 'smith-ui-components/dialog';

interface SurveyShowContentsType {
  data: any;
  callbackClose?: (state: any) => void | undefined;
}

const SurveyShowSingle: FC<SurveyShowContentsType> = ({
  data,
  callbackClose,
}) => {
  const [radio, setRadio] = useState<number>(-1);
  const { questionnaire } = data;
  const { answers } = questionnaire;

  const handleValidations = () => {
    if (radio === -1) {
      alert('답변해 주세요.').then();
    } else {
      callbackClose && callbackClose(true);
    }
  };

  const render = answers.map((answer: string, idx: number) => {
    return (
      <div style={{ marginTop: '5px' }} key={uuidv4()}>
        <span className="p-ml-2">
          <RadioButton
            onChange={(e) => {
              setRadio(e.target.value);
            }}
            checked={radio === idx}
            value={idx}
          />{' '}
          {answer}
        </span>
        <br />
      </div>
    );
  });

  return (
    <>
      <div className="card">{render}</div>
      <br />
      <div style={{ textAlign: 'center' }}>
        <Button label={'확인'} onClick={handleValidations} />
      </div>
    </>
  );
};

export default SurveyShowSingle;
