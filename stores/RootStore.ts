import { action, isObservableMap, isObservableSet } from 'mobx';
import MobxStore from './sample/MobxStore';

import SampleLoginStore from 'stores/sample/auth/SampleLoginStore';
import BoardListStore from 'stores/sample/board/BoardListStore';
import BoardDetailStore from 'stores/sample/board/BoardDetailStore';
import BoardNewStore from 'stores/sample/board/BoardNewStore';
import BoardEditStore from 'stores/sample/board/BoardEditStore';
import VideoListStore from 'stores/video/VideoListStore';
import TemplateMobxPageStore from 'stores/TemplateMobxPageStore';
import MyPageStore from 'stores/adminCommon/MyPageStore';
import PasswordChangeDialogStore from 'stores/adminCommon/PasswordChangeDialogStore';
import LoginStore from 'stores/auth/LoginStore';
import CommonStore from 'stores/CommonStore';

/**
 * Mobx Root Store. 모든 스토어는 rootStore를 통해 서로 참조할 수 있습니다.
 */
export default class RootStore {
  // Mobx Template 2. 스토어를 rootStore에 선언합니다.
  templateMobxPageStore: TemplateMobxPageStore;
  // Sample Stores
  sampleLoginStore: SampleLoginStore;
  mobxStore: MobxStore;
  boardListStore: BoardListStore;
  boardDetailStore: BoardDetailStore;
  boardNewStore: BoardNewStore;
  boardEditStore: BoardEditStore;
  videoListStore: VideoListStore;

  // 공통 스토어
  commonStore: CommonStore;

  // 프로젝트별 스토어
  loginStore: LoginStore;
  myPageStore: MyPageStore;
  passwordChangeDialogStore: PasswordChangeDialogStore;

  constructor() {
    // Mobx Template 3. 스토어에 rootStore를 주입하여 생성합니다.
    this.templateMobxPageStore = new TemplateMobxPageStore(this);
    // Sample Stores
    this.sampleLoginStore = new SampleLoginStore(this);
    this.boardListStore = new BoardListStore(this);
    this.boardDetailStore = new BoardDetailStore(this);
    this.boardNewStore = new BoardNewStore(this);
    this.boardEditStore = new BoardEditStore(this);
    this.mobxStore = new MobxStore(this);
    this.videoListStore = new VideoListStore(this);

    // 공통 스토어
    this.commonStore = new CommonStore(this);

    // 프로젝트별 스토어
    this.loginStore = new LoginStore(this);
    this.myPageStore = new MyPageStore(this);
    this.passwordChangeDialogStore = new PasswordChangeDialogStore(this);
  }

  @action
  showLoading = (visible: boolean) => {
    this.commonStore.showLoading = visible;
  };

  /**
   * getServerSideProps(SSR), getStaticProps(SSG) 에서 주입한 store를 화면에 녹여준다
   * @param initialStore
   */
  @action hydrate = (initialStore: RootStore) => {
    if (!initialStore) return;
    // 각 스토어 별로 대입
    Object.keys(initialStore).forEach((key: string) => {
      const newStore = (initialStore as any)[key];
      const prevStore = (this as any)[key];

      if (prevStore === undefined) {
        throw `RootStore에 ${key}가 없습니다.`;
      }

      // 스토어의 각 항목에 대입
      Object.keys(newStore).forEach((propertyKey: string) => {
        // Map, Set의 경우 JSON.stringify를 통해 Array로 변환된다.
        // 이 항목들을 다시 Map, Set으로 변환해준다.
        if (
          isObservableMap(prevStore[propertyKey]) ||
          prevStore[propertyKey] instanceof Map
        ) {
          prevStore[propertyKey] = new Map(newStore[propertyKey]);
        } else if (
          isObservableSet(prevStore[propertyKey]) ||
          prevStore[propertyKey] instanceof Set
        ) {
          prevStore[propertyKey] = new Set(newStore[propertyKey]);
        } else {
          prevStore[propertyKey] = newStore[propertyKey];
        }
      });
    });
  };
}
