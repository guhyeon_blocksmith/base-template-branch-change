import { action, makeAutoObservable, runInAction } from 'mobx';
import RootStore from '../RootStore';

import Router from 'next/router';
// import { setValidationErrorMessages } from 'libs/formUtil';
import { LoginModel } from 'types/formModels/adminCommon/LoginModel';
import { PublicAdminApi } from 'apis/index';

/**
 * Login. AC_LOGIN_001
 */
export default class LoginStore {
  rootStore?: RootStore;

  loginItem: LoginModel = new LoginModel();

  /**
   * 유효성 검사 에러 메시지 맵
   */
  validErrors: Map<string, string[]> = new Map();

  /**
   * 입력 Refs
   */
  inputRefs = new Map<string, any>();

  /**
   * 아이디 저장 여부
   */
  isSaveId = false;

  /**
   * 로그인 시도 실패 메시지 보임 여부
   */
  visibleLoginFailedMessage = false;

  constructor(rootStore?: RootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  @action
  init = () => {
    this.loginItem = new LoginModel();
    this.validErrors.clear();
    this.visibleLoginFailedMessage = false;

    const id = localStorage.getItem('savedId');
    if (id) {
      this.loginItem.loginId = id;
      this.isSaveId = true;
    }
  };

  /**
   * 로그인
   */
  @action
  submitLogin = async (backTo: string | string[] | undefined) => {
    // 유효성 검사
    // await setValidationErrorMessages(this.loginItem, this.validErrors);
    // if (this.validErrors.size > 0) {
    //   // alert로 유효성검사 내용 경고 띄우기
    //   // const message = [...this.validErrors.values()].join('\r\n');
    //   // await alert(message);
    //   // focusFirstInvalidElement(this.validErrors, this.inputRefs);
    //   throw 'loginFailed';
    // }

    // 로그인을하면 pages/api/adminProxy/api/wewill/admin/login을 호출 하고
    // 해당 로직에서 token값을 server쪽에 저장한다.
    const response = await PublicAdminApi.login.loginManagerUsingPost(
      this.loginItem
    );

    if (response.data?.error?.code === '00') {
      // 성공 했을 때
      if (this.isSaveId) {
        // 아이디 저장
        localStorage.setItem('savedId', this.loginItem.loginId);
      } else {
        localStorage.removeItem('savedId');
      }
      const returnUrl = Router.query.returnUrl as string;
      backTo !== undefined && backTo === typeof 'string'
        ? Router.replace(backTo).then()
        : Router.replace(returnUrl || '/').then();
    } else {
      runInAction(() => {
        this.loginItem.password = '';
        this.visibleLoginFailedMessage = true;
      });
    }
  };

  @action
  handleChangeSaveId = (e: {
    originalEvent: Event;
    value: any;
    checked: boolean;
    target: {
      type: string;
      name: string;
      id: string;
      value: any;
      checked: boolean;
    };
  }) => {
    this.isSaveId = e.checked;
    if (!e.checked) {
      localStorage.removeItem('savedId');
    }
  };

  @action
  handleIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.loginItem.loginId = e.target.value.replace(/[ㄱ-ㅎㅏ-ㅣ가-힣\s]/g, '');
  };

  @action
  handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.loginItem.password = e.target.value;
  };
}
