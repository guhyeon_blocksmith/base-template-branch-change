import RootStore from 'stores/RootStore';
import { makeAutoObservable } from 'mobx';
import { ManagerMe } from 'apis/ApiAdmin';
import { UserMe } from 'apis/ApiUser';

// type UserInfoType<T extends ManagerMe | UserMe> = T extends ManagerMe
//   ? ManagerMe
//   : UserMe;

/**
 * 공통 스토어
 */
export default class CommonStore {
  // 다른 스토어로 접근 할 수 있도록, 생성시 주입해준 rootStore 객체입니다.
  rootStore?: RootStore;
  userInfo?: ManagerMe | UserMe;

  showLoading = false;

  constructor(rootStore?: RootStore) {
    // rootStore를 제외하고 observable로 만듭니다.
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }
}
