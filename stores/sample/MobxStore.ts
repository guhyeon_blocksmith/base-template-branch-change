import { action, computed, makeAutoObservable, runInAction } from 'mobx';
import RootStore from '../RootStore';

/**
 * Mobx Page Store
 */
export default class MobxStore {
  rootStore?: RootStore;
  timer?: NodeJS.Timeout;

  lastUpdate = 0;
  title?: string;

  constructor(rootStore?: RootStore) {
    makeAutoObservable(this, { rootStore: false, timer: false });
    this.rootStore = rootStore;
  }

  @computed get timeString() {
    const pad = (n: number) => (n < 10 ? `0${n}` : n);
    const format = (t: Date) =>
      `${pad(t.getUTCHours())}:${pad(t.getUTCMinutes())}:${pad(
        t.getUTCSeconds()
      )}`;
    return format(new Date(this.lastUpdate));
  }
  light = false;

  @action start = () => {
    this.timer = setInterval(() => {
      runInAction(() => {
        const date = new Date(this.lastUpdate);
        this.lastUpdate = date.setSeconds(date.getSeconds() + 1);
        this.light = true;
      });
    }, 1000);
  };

  stop = () => this.timer && clearInterval(this.timer);
}
