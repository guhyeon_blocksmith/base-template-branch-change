import Axios, { AxiosResponse } from 'axios';
import { action, makeAutoObservable, runInAction } from 'mobx';
import RootStore from 'stores/RootStore';
import { alert } from 'smith-ui-components/dialog';
import { BoardEditFormModel } from 'types/formModels/sample/BoardEditFormModel';

import Router from 'next/router';

import _ from 'lodash';
import { AttachFileType } from 'types/AttachFileType';
import { ChangeEvent } from 'react';
import {
  bindFromTo,
  focusFirstInvalidElement,
  refreshValidationErrorMessage,
  setValidationErrorMessages,
} from 'libs/formUtil';

const baseApi = process.env.NEXT_PUBLIC_APP_FAKE_API_BASE_URL;

/**
 * Board 수정 페이지 스토어
 */
export default class BoardEditStore {
  rootStore?: RootStore;

  /**
   * 폼 아이템
   */
  item?: BoardEditFormModel;

  /**
   * 유효성 검사 에러 메시지 맵
   */
  validErrors: Map<string, string[]> = new Map();

  /**
   * 입력 Refs
   */
  inputRefs = new Map<string, any>();

  constructor(rootStore?: RootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  @action
  handleClear = () => {
    this.item!.title = '';
    this.item!.content = '';
  };

  @action
  init = async (id: string) => {
    this.validErrors.clear();
    this.inputRefs.clear();
    await this.loadItem(id);
  };

  @action
  loadItem = async (id: string) => {
    this.rootStore?.showLoading(true);
    try {
      // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
      const response = await Axios.get(`${baseApi}/board/${id}`);
      runInAction(() => {
        this.item = new BoardEditFormModel();
        bindFromTo(response.data, this.item);
      });
    } catch (e) {
      const { response } = e;
      if (response) {
        // csr 404
        const { status } = response as AxiosResponse;
        if (status === 404) {
          Router.replace('/404').then();
        }
      }
    } finally {
      this.rootStore?.showLoading(false);
    }
  };

  /**
   * 수정 저장
   */
  @action
  saveEditItem = async () => {
    if (this.item === undefined) {
      console.error('아이템 없음');
      return;
    }

    this.item.editedAt = Date.now();

    // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
    const response = await Axios.put(
      `${baseApi}/board/${this.item.id}`,
      this.item
    );
    if (response.status === 200) {
      await alert('저장 성공');
      Router.push(`/sample/board/list-csr`).then();
    } else {
      await alert('저장 실패');
    }
  };

  /**
   * 저장버튼 클릭 이벤트 처리기. 수정페이지
   */
  @action
  handleClickSave = async () => {
    await setValidationErrorMessages(this.item, this.validErrors);
    // 유효성검사 내용 경고 띄우기
    if (this.validErrors.size > 0) {
      const message = [...this.validErrors.values()].join('\r\n');

      await alert(message);
      focusFirstInvalidElement(this.validErrors, this.inputRefs);
      return;
    }

    await this.saveEditItem().then();
  };

  /**
   * 제목 변경 이벤트 처리기
   * @param e
   */
  @action
  handleChangeTitle = async (e: ChangeEvent<HTMLInputElement>) => {
    this.item!.title! = e.currentTarget.value;

    await refreshValidationErrorMessage(this.item, this.validErrors, 'title');
  };

  /**
   * 내용 변경 이벤트 처리기
   * @param e
   */
  @action
  handleChangeContent = async (e: {
    htmlValue: string | null;
    textValue: string;
    delta: any;
    source: string;
  }) => {
    this.item!.content! = e.htmlValue || '';
    await refreshValidationErrorMessage(this.item, this.validErrors, 'content');
  };

  /**
   * 파일 업로드 완료 이벤
   */
  @action
  handleFileUpload = async (event: { xhr: XMLHttpRequest; files: File[] }) => {
    const { xhr } = event;
    // xhr.response sample
    // "[{"seq":6098,"fileName":"스크린샷 2020-12-30 오후 6.09.19.png","fileChgName":"31b5abc0-ac9a-4af6-b0b3-f2f8b3470d43.png","filePath":"/assets/dev/uploadImages/origin/base-template-board_69a8aacd-210b-4c4f-b936-72f138175339/20210105","ext":"png","fileUrl":"http://d1n8fbd6e2ut61.cloudfront.net/assets/dev/uploadImages/origin/base-template-board_69a8aacd-210b-4c4f-b936-72f138175339/20210105/31b5abc0-ac9a-4af6-b0b3-f2f8b3470d43.png","error":{"code":"00","message":""}}]"

    const uploadedFiles: AttachFileType[] = JSON.parse(xhr.response).map(
      (responseItem: any) => ({
        name: responseItem.fileName,
        url: responseItem.fileUrl,
      })
    );

    this.item!.attachedFiles!.push(...uploadedFiles);
  };

  handleFileRemove = async (file: AttachFileType) => {
    _.remove(this.item!.attachedFiles!, (f) => f.name === file.name);
  };
}
