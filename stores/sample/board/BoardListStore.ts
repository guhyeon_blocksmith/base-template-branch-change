import Axios from 'axios';
import { action, makeAutoObservable, runInAction } from 'mobx';
import RootStore from 'stores/RootStore';
import { ChangeEvent } from 'react';
import { alert, confirm } from 'smith-ui-components/dialog';
import { BoardDetailResponse } from 'types/sample';
import Router from 'next/router';

const baseApi = process.env.NEXT_PUBLIC_APP_FAKE_API_BASE_URL;

/**
 * Board 페이지 스토어.
 */
export default class BoardListStore {
  rootStore?: RootStore;

  /**
   * 아이템 목록
   */
  items?: BoardDetailResponse[] = [];
  /**
   * 아이템 목록 검색어
   */
  searchKeyword = '';
  /**
   * 아이템 목록 전체 카운트
   */
  totalCount?: number = 0;
  /**
   * 목록 페이지 열 수
   */
  rowCount = 10;

  /**
   * 페이지 글 시작 index
   */
  firstIndex = 0;

  /**
   * 선택된 아이템 목록
   */
  selectedItems?: BoardDetailResponse[];

  /**
   * 정렬 필드
   */
  sortField?: string;

  /**
   * 정렬 순서
   */
  sortOrder?: number;

  constructor(rootStore?: RootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  /**
   * api에서 목록을 불러와 items에 바인딩해준다.
   */
  @action
  loadList = async () => {
    this.rootStore?.showLoading(true);

    let url = `${baseApi}/board?_start=${this.firstIndex}&_limit=${this.rowCount}&q=${this.searchKeyword}`;
    if (this.sortOrder !== 0) {
      const sortOrderString = this.sortOrder === 1 ? 'asc' : 'desc';
      url = url + `&_sort=${this.sortField}&_order=${sortOrderString}`;
    }

    // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
    const response = await Axios.get(url);

    runInAction(() => {
      this.items = response.data;
      this.totalCount = Number(response.headers['x-total-count']);
      this.rootStore?.showLoading(false);
    });
  };

  /**
   * 페이지 변경 이벤트 처리기
   * @param e
   */
  handlePageChange = async (e: {
    first: number;
    rows: number;
    page: number;
    pageCount: number;
  }) => {
    this.rowCount = e.rows;
    this.firstIndex = e.first;
    await this.loadList().then();
  };

  @action
  handleSearchKeywordChange = (event: ChangeEvent<HTMLInputElement>) => {
    this.searchKeyword = event.currentTarget.value;
  };

  @action
  bindList(data: any, totalCount: number) {
    this.items = data;
    this.totalCount = totalCount;
    this.rootStore?.showLoading(false);
  }

  @action
  handleRowPerPageChange = async (e: {
    originalEvent: Event;
    value: any;
    target: { name: string; id: string; value: any };
  }) => {
    this.rowCount = e.target.value;
    this.firstIndex = 0;
    await this.loadList().then();
  };

  /**
   *  수정 클릭 이벤트 처리
   */
  handleEditClick = async () => {
    if (this.selectedItems === undefined || this.selectedItems.length === 0) {
      await alert('선택된 아이템이 없습니다.');
      return;
    }

    const selectedIds = this.selectedItems?.map((el) => {
      return el.id;
    });

    if (selectedIds.length > 1) {
      await alert('한개의 아이템을 선택해주세요.');
    } else {
      await Router.push(`/sample/board/edit/${selectedIds[0]}`);
    }
  };

  /**
   * 삭제 버튼 클릭 이벤트 처리
   */
  @action
  handleDeleteClick = async () => {
    if (this.selectedItems === undefined || this.selectedItems.length === 0) {
      await alert('선택된 회원이 없습니다.');
      return;
    }

    const selectedIds = this.selectedItems.map((el) => {
      return el.id!;
    });

    if (selectedIds.length === 1) {
      const result = await confirm(
        `${selectedIds.length}명의 회원을 삭제 하시겠습니까?`
      );
      if (result !== 'OK') return;
      const isSuccess = await this.delete(selectedIds[0]).then();
      if (isSuccess) {
        await this.loadList().then();
        await alert('삭제하였습니다.');
      } else {
        await alert('삭제에 실패하였습니다.');
      }
    } else {
      const multiResult = await confirm(
        `${selectedIds.length}명의 회원을 삭제 하시겠습니까?`
      );
      if (multiResult !== 'OK') return;

      const isSuccess = await this.deleteMulti(selectedIds);
      if (isSuccess) {
        await alert('삭제하였습니다.');
      }
    }
    // 선택된 목록에서 삭제된 목록을 제외한다.
    this.selectedItems = this.selectedItems.filter(
      (item) => !selectedIds.some((deletedId) => deletedId === item.id)
    );
  };

  @action
  handleSelectionChange = (e: {
    originalEvent: Event;
    value: BoardDetailResponse[];
  }) => {
    this.selectedItems = e.value;
  };

  /**
   * 삭제
   */
  @action
  delete = async (id: string) => {
    let isSuccess: boolean;

    try {
      // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
      const response = await Axios.delete(`${baseApi}/board/${id}`);
      isSuccess = response.status === 200;
    } catch {
      isSuccess = false;
    }

    return isSuccess;
  };

  /**
   * 삭제 multi
   */
  @action
  deleteMulti = async (ids: string[]) => {
    // 가급적 multi로 지울 수 있는 backend API를 요청하시길 바랍니다.
    // 아래 방법은 여러개를 지울 방법이 없을 때 사용하세요.

    // id별로 삭제를 하는 비동기 함수를 호출합니다.
    const deletePromises = ids.map(async (id) => {
      const isSuccess = await this.delete(id);
      return { id, isSuccess };
    });

    // 삭제비동기 함수들이 모두 끝날 때까지 기다립니다.
    const responses = await Promise.all(deletePromises);

    // 리스트를 새로 불러옵니다. - 부분삭제나, 전체삭제를 고려해 항상 새로 불러옵니다.
    await this.loadList().then();

    // 부분 실패한 경우 경고처리
    if (responses.some((r) => !r.isSuccess)) {
      const failedIds = responses
        .filter((r) => !r.isSuccess)
        .map((r) => r.id)
        .join(', ');

      await alert(`${failedIds}을 지우는데 실패했습니다`);

      return false;
    }
    return true;
  };

  /**
   * 검색 초기화
   */
  @action
  handleSearchClear = () => {
    this.searchKeyword = '';
    this.loadList().then();
  };

  /**
   * 정렬이벤트 처리기 (상단 헤더 정렬 클릭)
   * @param e
   */
  @action
  handleSort = (e: {
    sortField: string;
    sortOrder: number;
    multiSortMeta: any;
  }) => {
    const { sortField, sortOrder } = e;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.loadList().then();
  };
}
