import Axios, { AxiosResponse } from 'axios';
import { action, makeAutoObservable, runInAction } from 'mobx';
import RootStore from 'stores/RootStore';
import { BoardDetailResponse } from 'types/sample';
import Router from 'next/router';

const baseApi = process.env.NEXT_PUBLIC_APP_FAKE_API_BASE_URL;

/**
 * Board 상세 페이지 스토어
 */
export default class BoardDetailStore {
  rootStore?: RootStore;

  /**
   * 상세보기 아이템
   */
  item?: BoardDetailResponse;

  constructor(rootStore?: RootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  @action
  loadItem = async (id: string) => {
    this.rootStore?.showLoading(true);
    try {
      // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
      const response = await Axios.get(`${baseApi}/board/${id}`);

      runInAction(() => {
        this.item = response.data;
      });
    } catch (e) {
      const { response } = e;
      if (response) {
        // csr 404
        const { status } = response as AxiosResponse;
        if (status === 404) {
          Router.replace('/404').then();
        }
      }
    } finally {
      this.rootStore?.showLoading(false);
    }
  };

  @action
  delete = async (id: string) => {
    this.rootStore?.showLoading(true);
    // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
    await Axios.delete(`${baseApi}/board/${id}`);
    this.rootStore?.showLoading(false);
  };
}
