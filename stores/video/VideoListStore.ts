import { action, makeAutoObservable, runInAction } from 'mobx';
import RootStore from '../RootStore';
import { SurveyItemType } from 'types/index';
import axios from 'axios';

/**
 * Video 페이지 스토어
 */
export interface EgressEndpoints {
  CMAF: string;
  DASH: string;
  HLS: string;
  MSS: string;
}

export interface AccelerationSettings {
  Mode: string;
}

export interface AudioSelector1 {
  DefaultSelection: string;
  Offset: number;
  ProgramSelection: number;
  SelectorType: string;
  Tracks: number[];
}

export interface VideoSelector {
  ColorSpace: string;
  Rotate: string;
}

export interface Input {
  AudioSelectors: AudioSelector1;
  DeblockFilter: string;
  DenoiseFilter: string;
  FileInput: string;
  FilterEnable: string;
  FilterStrength: number;
  PsiControl: string;
  TimecodeSource: string;
  VideoSelector: VideoSelector;
}

export interface FileGroupSettings {
  Destination: string;
}

export interface HlsGroupSettings {
  Destination: string;
  MinSegmentLength: number;
  SegmentLength: number;
}

export interface OutputGroupSettings {
  FileGroupSettings: FileGroupSettings;
  Type: string;
  HlsGroupSettings: HlsGroupSettings;
}

export interface ContainerSettings {
  Container: string;
}

export interface FrameCaptureSettings {
  FramerateDenominator: number;
  FramerateNumerator: number;
  MaxCaptures: number;
  Quality: number;
}

export interface CodecSettings {
  Codec: string;
  FrameCaptureSettings: FrameCaptureSettings;
}

export interface VideoDescription {
  AfdSignaling: string;
  AntiAlias: string;
  CodecSettings: CodecSettings;
  ColorMetadata: string;
  DropFrameTimecode: string;
  RespondToAfd: string;
  ScalingBehavior: string;
  Sharpness: number;
  TimecodeInsertion: string;
}

export interface Output {
  ContainerSettings: ContainerSettings;
  NameModifier: string;
  VideoDescription: VideoDescription;
}

export interface OutputGroup {
  Name: string;
  OutputGroupSettings: OutputGroupSettings;
  Outputs: Output[];
  CustomName: string;
}

export interface TimecodeConfig {
  Source: string;
}

export interface Settings {
  Inputs: Input[];
  OutputGroups: OutputGroup[];
  TimecodeConfig: TimecodeConfig;
}

export interface UserMetadata {
  guid: string;
  workflow: string;
}

export interface EncodingJob {
  AccelerationSettings: AccelerationSettings;
  JobTemplate: string;
  Role: string;
  Settings: Settings;
  UserMetadata: UserMetadata;
}

export interface VideoDetails {
  heightInPx: number;
  widthInPx: number;
}

export interface OutputDetail {
  durationInMs: number;
  outputFilePaths: string[];
  videoDetails: VideoDetails;
}

export interface OutputGroupDetail {
  outputDetails: OutputDetail[];
  type: string;
  playlistFilePaths: string[];
}

export interface UserMetadata2 {
  guid: string;
  workflow: string;
}

export interface Detail {
  accountId: string;
  jobId: string;
  outputGroupDetails: OutputGroupDetail[];
  queue: string;
  status: string;
  timestamp: number;
  userMetadata: UserMetadata2;
}

export interface EncodingOutput {
  account: string;
  detail: Detail;
  detailType: string;
  id: string;
  region: string;
  resources: string[];
  source: string;
  time: Date;
  version: string;
}

export interface VideoType {
  acceleratedTranscoding?: string;
  archiveSource?: string;
  cloudFront?: string;
  destBucket?: string;
  egressEndpoints?: EgressEndpoints;
  enableMediaPackage?: boolean;
  enableSns?: boolean;
  enableSqs?: boolean;
  encodeJobId?: string;
  encodingJob?: EncodingJob;
  encodingOutput?: EncodingOutput;
  encodingProfile?: number;
  endTime?: Date;
  frameCapture?: boolean;
  frameCaptureHeight?: number;
  frameCaptureWidth?: number;
  guid?: string;
  hlsPlaylist?: string;
  hlsUrl?: string;
  inputRotate?: string;
  isCustomTemplate?: boolean;
  jobTemplate?: string;
  jobTemplate1080p?: string;
  jobTemplate2160p?: string;
  jobTemplate720p?: string;
  mediaPackageResourceId?: string;
  mp4Outputs?: string[];
  mp4Urls?: string[];
  srcBucket?: string;
  srcHeight?: number;
  srcMediainfo?: string;
  srcVideo?: string;
  srcWidth?: number;
  startTime?: Date;
  thumbNails?: string[];
  thumbNailsUrls?: string[];
  workflowName?: string;
  workflowStatus?: string;
  workflowTrigger?: string;
}

export default class VideoListStore {
  rootStore?: RootStore;
  /**
   * 리스트 불러오는 중 여부
   */
  isLoading = false;
  isSurveyReady = false;
  /**
   * 영상 리스트
   */
  list?: VideoType[] = [];

  /**
   * 설문 리스트
   */
  surveyList: SurveyItemType[] = [];

  constructor(rootStore?: RootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  /**
   * 스토어 초기화
   */
  @action
  initialize = () => {
    this.list = [];
    this.surveyList = [];
  };

  /**
   * 영상 목록 자료 가져오기.
   */
  @action
  loadList = async (condition: string) => {
    this.isLoading = true;
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_VIDEO_PRIVATE_PROXY_URL}/video?workflowStatus=${condition}`
    );
    //console.log('response:', response.data.body);

    runInAction(() => {
      this.list = response.data.body.Items;
      this.isLoading = false;
    });
  };

  /**
   * guid 를 근거로 설문 목록을 가져오기.
   */
  @action
  loadSurveyList = async (condition: string) => {
    const response = await fetch(`/api/vod-node?guid=${condition}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'access-control-allow-origin': '*',
      },
    });

    const { body } = await response.json();

    runInAction(() => {
      this.surveyList = body.Items;
      this.isSurveyReady = true;
    });
  };
}
