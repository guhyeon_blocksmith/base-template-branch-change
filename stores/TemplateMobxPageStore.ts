import RootStore from 'stores/RootStore';
import { action, makeAutoObservable } from 'mobx';

/**
 * Mobx Template 1. 아래 소스를 복사합니다.
 */
export default class TemplateMobxPageStore {
  // 다른 스토어로 접근 할 수 있도록, 생성시 주입해준 rootStore 객체입니다.
  rootStore?: RootStore;

  /**
   * !!! TODO 타입은 api generte 후 알맞게 넣어주세요.
   */
  items: any = [];

  constructor(rootStore?: RootStore) {
    // rootStore를 제외하고 observable로 만듭니다.
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  /**
   * 데이터 불러오기
   */
  @action // store에 있는 상태값을 세팅하는 함수에 붙여주세요.
  load = async () => {
    this.rootStore?.showLoading(true);
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/');
    this.items = await response.json();
    this.rootStore?.showLoading(false);
  };
}
