import RootStore from 'stores/RootStore';
import { action, makeAutoObservable } from 'mobx';
import { PrivateAdminApi } from 'apis/index';
import { MyPageInfoResponse } from 'apis/ApiAdmin';

/**
 * 마이페이지. MS_MYPAGE_001
 */
export default class MyPageStore {
  // 다른 스토어로 접근 할 수 있도록, 생성시 주입해준 rootStore 객체입니다.
  rootStore?: RootStore;

  /**
   * 관리자 정보 타입
   */
  item?: MyPageInfoResponse;

  constructor(rootStore?: RootStore) {
    // rootStore를 제외하고 observable로 만듭니다.
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  /**
   * 데이터 불러오기
   */
  @action // store에 있는 상태값을 세팅하는 함수에 붙여주세요.
  load = async () => {
    this.rootStore?.showLoading(true);
    if (this.rootStore?.commonStore?.userInfo?.id) {
      const response = await PrivateAdminApi.mypages.selectMypageUsingGet(
        this.rootStore.commonStore.userInfo.id
      );

      this.item = response.data;
    }
    this.rootStore?.showLoading(false);
  };

  handleChangePassword = () => {
    this.rootStore!.passwordChangeDialogStore.visible = true;
  };
  handleClose = () => {
    this.rootStore!.passwordChangeDialogStore.visible = false;
  };
}
