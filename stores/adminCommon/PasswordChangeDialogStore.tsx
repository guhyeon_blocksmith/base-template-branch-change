import { PasswordChangeModel } from 'types/formModels/adminCommon/PasswordChangeModel';
import { action, computed, makeAutoObservable, runInAction } from 'mobx';
import React from 'react';
import { setValidationErrorMessage } from 'libs/formUtil';
import { alert } from 'smith-ui-components/dialog';
import RootStore from 'stores/RootStore';
import { PrivateAdminApi } from 'apis/index';

export default class PasswordChangeDialogStore {
  // 다른 스토어로 접근 할 수 있도록, 생성시 주입해준 rootStore 객체입니다.
  rootStore?: RootStore;

  model = new PasswordChangeModel();
  /**
   * 다이얼로그 보임여부. 비밀번호 변경은 재사용 다이얼로그가 아니므로, visible을 스토어에서 관리
   */
  visible = false;

  /**
   * 유효성 검사 에러 메시지 맵
   */
  validErrors: Map<string, string[]> = new Map();

  /**
   * 입력 Refs
   */
  inputRefs = new Map<string, any>();

  /**
   * 변경하기 버튼 활성화
   */
  @computed get disabledOkButton() {
    if (
      this.model.newConfirmPassword === '' ||
      this.model.newPassword === '' ||
      this.model.currentPassword === ''
    ) {
      return true;
    }
    return this.validErrors.size !== 0;
  }

  constructor(rootStore?: RootStore) {
    // rootStore를 제외하고 observable로 만듭니다.
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  @action
  init = () => {
    this.model = new PasswordChangeModel();
  };

  @action
  handleCurrentChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    this.model.currentPassword = event.target.value;
    await setValidationErrorMessage(
      this.model,
      this.validErrors,
      'currentPassword'
    );
  };

  @action
  handleNewChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    this.model.newPassword = e.target.value;
    await setValidationErrorMessage(
      this.model,
      this.validErrors,
      'newPassword'
    );

    // 새 비밀번호 확인에 값을 입력 후 비밀번호가 바뀌었을 때 새 비밀번호 확인에 비밀번호 일치 유효성
    if (this.model.newConfirmPassword) {
      await setValidationErrorMessage(
        this.model,
        this.validErrors,
        'newConfirmPassword'
      );
    }
  };

  @action
  handleNewConfirmChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    this.model.newConfirmPassword = e.target.value;
    setValidationErrorMessage(
      this.model,
      this.validErrors,
      'newConfirmPassword'
    );
  };

  @action
  handleOK = async () => {
    const response = await PrivateAdminApi.mypages.resetPwUsingPost({
      password: this.model.currentPassword,
      pwdChange: this.model.newPassword,
    });
    await runInAction(async () => {
      const { code } = response.data!.error!;
      if (code === '00') {
        // 성공
        this.visible = false;
        await alert('비밀번호 변경이 완료되었습니다.', '비밀번호 변경 완료');
        this.init();
      }
      // 실패
      // 각 필드별 유효성 결과를 내려줘야함.
      else if (code === 'ERROR.USER.0007') {
        this.validErrors.set('currentPassword', ['잘못된 비밀번호입니다.']);
      } else if (code === 'ERROR.USER.0008') {
        this.validErrors.set('newPassword', [
          '기존 비밀번호와 동일하게 설정할 수 없습니다.',
        ]);
      }
    });
  };

  @action
  handleCancel = () => {
    this.visible = false;
  };
}
