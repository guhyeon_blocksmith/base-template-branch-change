import React, { useEffect } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

import AppProvider from 'libs/providers/AppProvider';
import RootStore from 'stores/RootStore';
import styles from './TemplateMobx.module.css';
import { useStore } from 'libs/hooks/useStore';
import Layout from 'components/layout/Layout/Layout';
import { observer } from 'mobx-react-lite';
import { useRouter } from 'next/router';

/**
 * !!! TODO 페이지 컴포넌트 템플릿. 작성 후 url에 맞추어 pages로 코드 이동해주세요
 * @constructor
 */
const TemplateMobxPage = observer(() => {
  // Mobx Template 3. useStore를 통해서 store에 접근합니다.
  const { templateMobxPageStore: store } = useStore(); // store로 별칭합니다.
  useEffect(() => {
    store.load();
  }, [store]);

  // router 파라미터를 받아 처리
  const router = useRouter();
  useEffect(() => {
    const { id } = router.query;
    console.log(id);
  }, [router]);

  return (
    <Layout>
      {/* 관리자 상세(새 창) 인 경우 Layout에 우측 추가 config={detailLayoutConfig} custom={detailPageCustom} */}
      <div className={styles.wrapper}>
        {/* !!! TODO :any 제거 */}
        {store.items.map(({ id, title, userId }: any) => {
          return (
            <div key={id}>
              {id}, {title}, {userId}
            </div>
          );
        })}
      </div>
    </Layout>
  );
});

// 스토리에서 사용할 스토어를 생성합니다.
const rootStore = new RootStore();

export default {
  title: 'Template/TemplateMobxPage',
  component: TemplateMobxPage,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  return (
    <AppProvider rootStore={rootStore}>
      <TemplateMobxPage {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
// Mobx Template 4. 스토리북 mobx 애드온에 스토어를 연결합니다.
Default.parameters = {
  mobxStore: rootStore.templateMobxPageStore, // mobxStore Addon에 표시될 스토어를 지정해줍니다.
  nextRouter: {
    // page url 라우트 목업
    path: '/templatemobxpage/[id]',
    asPath: '/templatemobxpage/111222',
    query: {
      id: '111222',
    },
  },
};
