import React, { useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'libs/providers/AppProvider';
import BoardEditForm from 'components/sample/board/BoardEditForm';

import RootStore from 'stores/RootStore';

const rootStore = new RootStore();

export default {
  title: 'Pages/Sample/Board/BoardEditForm',
  component: BoardEditForm,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  const { boardEditStore } = rootStore;
  useEffect(() => {
    console.debug('boardEidtStore InitStart');
    boardEditStore.init('92527e8f-f603-426e-b197-12590832fb1f').then(() => {
      console.debug('boardEidtStore InitEndout');
    });
  }, [boardEditStore]);

  return (
    <AppProvider rootStore={rootStore}>
      <BoardEditForm {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
Default.parameters = { mobxStore: rootStore.boardEditStore };
