import React, { useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'libs/providers/AppProvider';
import BoardNewForm from 'components/sample/board/BoardNewForm';
import RootStore from 'stores/RootStore';

const rootStore = new RootStore();

export default {
  title: 'Pages/Sample/Board/BoardNewForm',
  component: BoardNewForm,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  const { boardNewStore } = rootStore;
  useEffect(() => {
    boardNewStore.init();
  }, [boardNewStore]);

  return (
    <AppProvider rootStore={rootStore}>
      <BoardNewForm {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
Default.parameters = { mobxStore: rootStore.boardNewStore };
