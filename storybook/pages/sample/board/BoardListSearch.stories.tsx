import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'libs/providers/AppProvider';
import BoardListSearch from 'components/sample/board/BoardListSearch';
import RootStore from 'stores/RootStore';

const rootStore = new RootStore();

export default {
  title: 'Pages/Sample/Board/BoardListSearch',
  component: BoardListSearch,
  argTypes: {},
} as Meta;

/**
 * BoardListSearch 스토리.
 * @param args
 * @constructor
 */
const Template: Story = (args) => {
  return (
    <AppProvider rootStore={rootStore}>
      <BoardListSearch {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
Default.parameters = { mobxStore: rootStore.boardListStore };
