import React, { useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'libs/providers/AppProvider';
import BoardTable from 'components/sample/board/BoardTable';
import RootStore from 'stores/RootStore';

const rootStore = new RootStore();

export default {
  title: 'Pages/Sample/Board/BoardTable',
  component: BoardTable,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  useEffect(() => {
    rootStore.boardListStore.loadList().then();
  }, []);

  return (
    <AppProvider rootStore={rootStore}>
      <BoardTable {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
Default.parameters = { mobxStore: rootStore.boardListStore };
