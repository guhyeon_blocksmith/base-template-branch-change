import React, { useEffect } from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

// import BoardDetail from 'components/sample/board/BoardDetail';
import AppProvider from 'libs/providers/AppProvider';
import RootStore from 'stores/RootStore';
import BoardDetail from '../../../../pages/sample/board/[id]';
const rootStore = new RootStore();

export default {
  title: 'Pages/Sample/Board/BoardDetail',
  component: BoardDetail,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  useEffect(() => {
    rootStore.boardDetailStore
      .loadItem('4b9ca746-b215-417c-a3bb-d0fe90fcb42b')
      .then();
  }, []);

  return (
    <AppProvider rootStore={rootStore}>
      <BoardDetail {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
Default.parameters = { mobxStore: rootStore.boardDetailStore };
