import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { LanguageProvider } from 'libs/providers/LanguageProvider';
import { ThemeProvider } from 'libs/providers/ThemeProvider';
import TotalUiComponents from 'components/sample/TotalUiComponents';

export default {
  title: 'Pages/Sample/TotalUiComponents',
  component: TotalUiComponents,
  argTypes: {},
} as Meta;

const Template: Story = () => {
  return (
    <ThemeProvider>
      <LanguageProvider>
        <TotalUiComponents />
      </LanguageProvider>
    </ThemeProvider>
  );
};
export const Default = Template.bind({});
Default.args = {};
