import React, { FC } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import styles from './TemplateCommon.module.css';

/**
 * !!! TODO 샘플 컴포넌트 Props Type. 작성 후 components로 코드 이동해주세요
 */
interface TemplateCommonProps {
  /**
   * 제목
   */
  title: string;
}
/**
 * !!! TODO 샘플 공통 컴포넌트. 작성 후 components로 코드 이동해주세요
 * @constructor
 */
const TemplateCommon: FC<TemplateCommonProps> = ({ title = '기본값' }) => {
  return <div className={styles.wrapper}>{title}</div>;
};

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'Template/TemplateCommon', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: TemplateCommon, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<TemplateCommonProps> = (args) => {
  return <TemplateCommon {...args} />;
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});

/**
 * 제목 변경 스토리
 */
export const Title = Template.bind({});
Title.args = { title: '제목변경' };
