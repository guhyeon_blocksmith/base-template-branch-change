import React, { FC, useEffect } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppProvider from 'libs/providers/AppProvider';
import RootStore from 'stores/RootStore';
import styles from './TemplateMobx.module.css';
import { useStore } from 'libs/hooks/useStore';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { observer } from 'mobx-react-lite';

/**
 * !!! TODO 샘플 mobx 컴포넌트. 페이지 하위 컴포넌트. 작성 후 components/{페이지명}폴더로 코드 이동해주세요
 * @constructor
 */
const TemplateMobx: FC = observer(() => {
  // Mobx Template 3. useStore를 통해서 store에 접근합니다.
  const { templateMobxPageStore: store } = useStore(); // store로 별칭합니다.
  useEffect(() => {
    store.load();
  }, [store]);

  return (
    <Layout config={config} custom={custom}>
      <div className={styles.wrapper}>
        {/* !!! TODO :any 제거 */}
        {store.items.map(({ id, title, userId }: any) => {
          return (
            <div key={id}>
              {id}, {title}, {userId}
            </div>
          );
        })}
      </div>
    </Layout>
  );
});

// 스토리에서 사용할 스토어를 생성합니다.
const rootStore = new RootStore();

export default {
  title: 'Template/TemplateMobx',
  component: TemplateMobx,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  return (
    <AppProvider rootStore={rootStore}>
      <TemplateMobx {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
// Mobx Template 4. 스토리북 mobx 애드온에 스토어를 연결합니다.
Default.parameters = { mobxStore: rootStore.templateMobxPageStore }; // mobxStore Addon에 표시될 스토어를 지정해줍니다.
