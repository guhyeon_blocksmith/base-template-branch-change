import React, { useEffect, useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import {
  AdminDataTable,
  AdminDataTableProps,
  PageInfoType,
} from 'components/adminCommon/AdminDataTable';
import NegativeAmountHeader from 'components/adminCommon/NegativeAmountHeader';
import { AdminDataTableFooterContainer } from 'components/adminCommon/AdminDataTableFooterContainer';
import { Button } from 'smith-ui-components/button';
import { ColumnGroup } from 'smith-ui-components/columngroup';
import { Row } from 'smith-ui-components/row';
import { Column } from 'smith-ui-components/column';

export default {
  title: 'AdminCommon/AdminDataTable',
  component: AdminDataTable,
  argTypes: {},
} as Meta;

interface ItemType {
  date: string;
  amount: string;
  couponAmount: string;
  saleAmount: string;
}

const Template: Story<AdminDataTableProps> = (args) => {
  const [value, setValue] = useState<any>([]);
  const [pageInfo, setPageInfo] = useState<PageInfoType>({ page: 1, rows: 10 });

  const loadData = async (pageInfo: PageInfoType) => {
    const data = await fetchData(pageInfo);
    setValue(data);
  };

  useEffect(() => {
    loadData(pageInfo);
  }, [pageInfo]);

  return (
    <div>
      <button
        onClick={async () => {
          setPageInfo((prevState) => {
            const newState = { ...prevState };
            newState.page = 1;
            return newState;
          });
        }}
      >
        go to page1
      </button>
      <AdminDataTable
        {...args}
        value={value}
        pageInfo={pageInfo}
        onPageInfoChange={async (e: PageInfoType) => {
          console.log('onPageInfoChange', e);
          setPageInfo(e);
        }}
      />
    </div>
  );
};
const items: ItemType[] = [
  {
    date: '2020-12-06',
    amount: '1',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-05',
    amount: '2',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-04',
    amount: '3',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-03',
    amount: '4',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-02',
    amount: '5',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '6',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '7',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '8',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '9',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '10',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '11',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
  {
    date: '2020-12-01',
    amount: '12',
    couponAmount: '30,000',
    saleAmount: '970,000',
  },
];

const fetchData = ({ page, rows }: PageInfoType) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      const temp = [...items];

      const start = (page - 1) * rows;
      resolve(temp.splice(start, rows));
    }, 0.5);
  });
};

const commonArgs: AdminDataTableProps = {
  columns: [
    { field: 'date', header: '결제일' },
    { field: 'amount', header: '결제금액' },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'쿠폰사용 금액'} />,
    },
    { field: 'saleAmount', header: '매출' },
  ],
  value: items,
  totalRecords: items.length,
};

export const Default = Template.bind({});
Default.args = { ...commonArgs };

export const DefaultRowsPage: Story<AdminDataTableProps> = (args) => {
  const [value, setValue] = useState<any>([]);
  const [pageInfo, setPageInfo] = useState<PageInfoType>({ page: 1, rows: 5 });

  const loadData = async (pageInfo: PageInfoType) => {
    const data = await fetchData(pageInfo);
    setValue(data);
  };

  useEffect(() => {
    loadData(pageInfo);
  }, [pageInfo]);

  return (
    <div>
      <button
        onClick={async () => {
          setPageInfo((prevState) => {
            const newState = { ...prevState };
            newState.page = 1;
            return newState;
          });
        }}
      >
        go to page1
      </button>
      <AdminDataTable
        {...args}
        pageInfo={pageInfo}
        value={value}
        onPageInfoChange={async (e: PageInfoType) => {
          console.log('onPageInfoChange', e);
          setPageInfo(e);
        }}
      />
    </div>
  );
};
DefaultRowsPage.args = { ...commonArgs };

export const ShowDetailRowsPage = Template.bind({});
ShowDetailRowsPage.args = {
  ...commonArgs,
  visibleDetail: true,
  onDetailClick: (pageInfo) => {
    alert(JSON.stringify(pageInfo));
  },
};

/**
 * 상세보기 커스텀 헤더는 field:'detail' 로 하여 나머지 컬럼 propery를 입력하면됩니다.
 */
export const CustomDetailColumn = Template.bind({});
CustomDetailColumn.args = {
  ...commonArgs,
  columns: [
    { field: 'date', header: '결제일' },
    { field: 'amount', header: '결제금액' },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'쿠폰사용 금액'} />,
    },
    { field: 'saleAmount', header: '매출' },
    { field: 'detail', header: '상세보기헤더' },
  ],
  visibleDetail: true,
  onDetailClick: (pageInfo) => {
    alert(JSON.stringify(pageInfo));
  },
};

export const TemplateColumn = Template.bind({});
TemplateColumn.args = {
  columns: [
    {
      field: 'date',
      header: '결제일',
      // eslint-disable-next-line react/display-name
      body: (rowData: ItemType) => <b>{rowData.date}</b>,
    },
    { field: 'amount', header: '결제금액' },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'쿠폰사용 금액'} />,
    },
    {
      field: 'saleAmount',
      header: '매출',
      // eslint-disable-next-line react/display-name
      body: (rowData: ItemType) => rowData.saleAmount + '0000000',
    },
  ],
  value: items,
  totalRecords: items.length,
};

export const NoPage = Template.bind({});
NoPage.args = {
  ...commonArgs,
  visiblePage: false,
  onDetailClick: (pageInfo) => {
    alert(JSON.stringify(pageInfo));
  },
};

export const UseFooter = Template.bind({});
UseFooter.args = {
  columns: [
    {
      field: 'date',
      header: '결제일',
      // eslint-disable-next-line react/display-name
      body: (rowData: ItemType) => <b>{rowData.date}</b>,
    },
    { field: 'amount', header: '결제금액' },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'쿠폰사용 금액'} />,
    },
    {
      field: 'saleAmount',
      header: '매출',
      // eslint-disable-next-line react/display-name
      body: (rowData: ItemType) => rowData.saleAmount + '0000000',
    },
  ],
  value: items,
  totalRecords: items.length,
  footer: (
    <AdminDataTableFooterContainer>
      <Button label={'취소'} />
      <Button label={'확인'} />
    </AdminDataTableFooterContainer>
  ),
};

/**
 * 기본적으로 가로 스크롤 적용 되어있음
 */
export const HorizontalScrollIsDefault = Template.bind({});
HorizontalScrollIsDefault.args = {
  ...commonArgs,
  columns: [
    { field: 'date', header: '결제일' },
    { field: 'amount', header: '결제금액' },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'쿠폰사용 금액'} />,
    },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    { field: 'saleAmount', header: '매출' },
    {
      field: 'detail',
      header: '상세보기헤더',
    },
  ],
  style: { width: '100%' },
  visibleDetail: true,
};
//
// export const HorizontalScroll = Template.bind({});
// HorizontalScroll.args = {
//   ...commonArgs,
//   columns: [
//     { field: 'date', header: '결제일', headerStyle: { width: '200px' } },
//     { field: 'amount', header: '결제금액', headerStyle: { width: '200px' } },
//     {
//       field: 'couponAmount',
//       header: <NegativeAmountHeader title={'쿠폰사용 금액'} />,
//       headerStyle: { width: '200px' },
//     },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     { field: 'saleAmount', header: '매출', headerStyle: { width: '200px' } },
//     {
//       field: 'detail',
//       header: '상세보기헤더',
//       headerStyle: { width: '120px' },
//     },
//   ],
//   style: { width: '100%' },
//   visibleDetail: true,
//   scrollable: true,
// };

export const AlignStyle = Template.bind({});
AlignStyle.args = {
  ...commonArgs,
  columns: [
    {
      field: 'date',
      header: 'HeaderCenter',
      headerStyle: { textAlign: 'center' },
    },
    {
      field: 'amount',
      header: 'BodyCenter',
      bodyStyle: { textAlign: 'center' },
    },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'Both Center'} />,
      headerStyle: { textAlign: 'center' },
      bodyStyle: { textAlign: 'center' },
    },
    {
      field: 'saleAmount',
      header: 'Right',
      headerStyle: { textAlign: 'right' },
    },
  ],
};

/**
 * 테이블 헤더 그룹. headerColumnGroup에 있는 hedaer 명이 보여집니다.
 */
export const HeaderColumnGroup = Template.bind({});
HeaderColumnGroup.args = {
  columns: [
    { field: 'date' },
    { field: 'amount' },
    { field: 'couponAmount' },
    { field: 'saleAmount' },
  ],
  value: items,
  totalRecords: items.length,
  headerColumnGroup: (
    <ColumnGroup>
      {/* date, amount, couponAmount, saleAmount */}
      <Row>
        <Column header="날짜" rowSpan={2} />
        <Column
          header="가격"
          colSpan={3}
          headerStyle={{
            // center align
            textAlign: 'center',
          }}
        />
      </Row>
      <Row>
        <Column field="amount" header={'수량'} />
        <Column field="couponAmount" header={'쿠폰가격'} />
        <Column field="saleAmount" header={'판매가격'} />
      </Row>
    </ColumnGroup>
  ),
};

export const SortHeader: Story<AdminDataTableProps> = (args) => {
  const [value, setValue] = useState<any>([]);
  const [pageInfo, setPageInfo] = useState<PageInfoType>({
    page: 1,
    rows: 10,
    // sort: 'date&order=desc',// sort 기본값
    sort: undefined,
  });

  const loadData = async (pageInfo: PageInfoType) => {
    const data = await fetchData(pageInfo);
    setValue(data);
  };

  useEffect(() => {
    loadData(pageInfo);
  }, [pageInfo]);

  return (
    <div>
      <button
        onClick={async () => {
          setPageInfo((prevState) => {
            const newState = { ...prevState };
            newState.page = 1;
            return newState;
          });
        }}
      >
        go to page1
      </button>
      <AdminDataTable
        {...args}
        value={value}
        pageInfo={pageInfo}
        onPageInfoChange={async (e: PageInfoType) => {
          console.log('onPageInfoChange', e);
          setPageInfo(e);
        }}
      />
    </div>
  );
};

SortHeader.args = {
  ...commonArgs,
  sortOptions: [
    { label: '결제일↑', value: 'date&order=desc' },
    { label: '결제일↓', value: 'date&order=asc' },
    { label: '결제금액↑', value: 'amount&order=desc' },
    { label: '결제금액↓', value: 'amount&order=asc' },
  ],
  onPageInfoChange: (e) => {
    window.alert(JSON.stringify(e));
  },
};

export const CustomRowPerPage = Template.bind({});
CustomRowPerPage.args = {
  ...commonArgs,
  rowPerPage: [
    { label: '5개씩 보기', value: 5 },
    { label: '10개씩 보기', value: 10 },
    { label: '20개씩 보기', value: 20 },
    { label: '50개씩 보기', value: 50 },
    { label: '500개씩 보기', value: 500 },
    { label: '1000개씩 보기', value: 1000 },
  ],
  columns: [
    {
      field: 'date',
      header: 'HeaderCenter',
      headerStyle: { textAlign: 'center' },
    },
    {
      field: 'amount',
      header: 'BodyCenter',
      bodyStyle: { textAlign: 'center' },
    },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'Both Center'} />,
      headerStyle: { textAlign: 'center' },
      bodyStyle: { textAlign: 'center' },
    },
    {
      field: 'saleAmount',
      header: 'Right',
      headerStyle: { textAlign: 'right' },
    },
  ],
};

export const SelectHeader: Story<AdminDataTableProps> = (args) => {
  const [value, setValue] = useState<any>([]);
  const [pageInfo, setPageInfo] = useState<PageInfoType>({ page: 1, rows: 10 });

  const [selectedItems, setSelectedItems] = useState();

  const loadData = async (pageInfo: PageInfoType) => {
    const data = await fetchData(pageInfo);
    setValue(data);
  };

  useEffect(() => {
    loadData(pageInfo);
  }, [pageInfo]);

  return (
    <div>
      <button
        onClick={async () => {
          setPageInfo((prevState) => {
            const newState = { ...prevState };
            newState.page = 1;
            return newState;
          });
        }}
      >
        go to page1
      </button>
      <AdminDataTable
        {...args}
        value={value}
        pageInfo={pageInfo}
        onPageInfoChange={async (e: PageInfoType) => {
          console.log('onPageInfoChange', e);
          setPageInfo(e);
        }}
        selection={selectedItems}
        onSelectionChange={(e) => setSelectedItems(e.value)}
      />
    </div>
  );
};
SelectHeader.args = {
  ...commonArgs,
  columns: [
    {
      selectionMode: 'multiple',
    },
    {
      field: 'date',
      header: 'HeaderCenter',
      headerStyle: { textAlign: 'center' },
    },
    {
      field: 'amount',
      header: 'BodyCenter',
      bodyStyle: { textAlign: 'center' },
    },
    {
      field: 'couponAmount',
      header: <NegativeAmountHeader title={'Both Center'} />,
      headerStyle: { textAlign: 'center' },
      bodyStyle: { textAlign: 'center' },
    },
    {
      field: 'saleAmount',
      header: 'Right',
      headerStyle: { textAlign: 'right' },
    },
  ],
};
