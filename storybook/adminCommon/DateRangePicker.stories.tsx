import React, { useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import DateRangePicker, {
  DateRangePickerProps,
} from 'components/adminCommon/DateRangePicker';
import { Button } from 'smith-ui-components/button';
import dayjs from 'dayjs';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'AdminCommon/DateRangePicker', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: DateRangePicker, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<DateRangePickerProps> = (args) => {
  // <DateRangePicker
  //   startDate={searchCondition[id]?.startDate}
  //   endDate={searchCondition[id]?.endDate}
  //   onChange={(e) => {
  //     setSearchCondition((prevState: any) => {
  //       const newState = {
  //         ...prevState,
  //       };
  //       newState[id] = {
  //         startDate: e.startDate,
  //         endDate: e.endDate,
  //       };
  //
  //       // onChange && onChange(id, newState);
  //       return newState;
  //     });
  //   }}
  //   {...controlProps}
  // />

  // return <DateRangePicker {...args} onChange={(e) => console.log(e)} />;

  return (
    <DateRangePicker
      {...args}
      onChange={(e) => {
        console.log(e);
      }}
      // startDate={startDate}
      // endDate={endDate}
    />
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = {};

/**
 * 기본 값 입력
 */
export const DefaultValue: Story<DateRangePickerProps> = () => {
  const [startDate, setStartDate] = useState<string>('2021-04-01');
  const [endDate, setEndDate] = useState<string>('2021-04-28');

  return (
    <>
      <DateRangePicker
        onChange={(e) => {
          setStartDate(e.startDate);
          setEndDate(e.endDate);
          console.log(e);
        }}
        startDate={startDate}
        endDate={endDate}
      />
    </>
  );
};

/**
 * 값 변경 스토리 입력
 */
export const ValueChange: Story<DateRangePickerProps> = () => {
  const [startDate, setStartDate] = useState<string>('2021-04-01');
  const [endDate, setEndDate] = useState<string>('2021-04-28');

  return (
    <>
      <DateRangePicker
        onChange={(e) => {
          setStartDate(e.startDate);
          setEndDate(e.endDate);
          console.log(e);
        }}
        startDate={startDate}
        endDate={endDate}
      />
      <Button
        label={'start day+1'}
        onClick={() => {
          setStartDate((prevState) => {
            // dayjs add 스토리북에서 사용시 amount는 변수로 빼서 더해줘야함.
            const amount = 1;
            return dayjs(prevState).add(amount, 'day').format('YYYY-MM-DD');
          });
        }}
      />
      <Button
        label={'end day+1'}
        onClick={() => {
          setEndDate((prevState) => {
            // dayjs add 스토리북에서 사용시 amount는 변수로 빼서 더해줘야함.
            const amount = 1;
            return dayjs(prevState).add(amount, 'day').format('YYYY-MM-DD');
          });
        }}
      />
    </>
  );
};

/**
 * 단축 버튼 커스텀
 */
export const CustomShortCuts = Template.bind({});
CustomShortCuts.args = {
  shortcuts: [
    { id: 0, amount: 7, type: 'day', labelName: '7일' },
    { id: 1, amount: 1, type: 'month', labelName: '1개월' },
  ],
};

/**
 * 단축 버튼 없애기
 */
export const NoShortCuts = Template.bind({});
NoShortCuts.args = {
  shortcuts: [],
};

/**
 * 기준값 시작일
 */
export const StandardWithStartDate = Template.bind({});
StandardWithStartDate.args = {
  standard: 'startDate',
};
