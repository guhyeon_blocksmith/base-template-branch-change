import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import {
  ApprovalStatusTable,
  ApprovalStatusTableProps,
} from 'components/adminCommon/ApprovalStatusTable';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'AdminCommon/ApprovalStatusTable', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: ApprovalStatusTable, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<ApprovalStatusTableProps> = (args) => {
  return <ApprovalStatusTable {...args} />;
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});

/**
 * HasValue
 */
export const HasValue = Template.bind({});
HasValue.args = {
  status: '환불신청',
  date: '2020-01-02',
  inCharge: '차명환',
};
