import React, { useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import {
  AdminDataTableHeader,
  AdminTableHeaderProps,
} from 'components/adminCommon/AdminDataTableHeader';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'AdminCommon/AdminDataTableHeader', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: AdminDataTableHeader, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<AdminTableHeaderProps> = (args) => {
  const [rowCount, setRowCount] = useState<number>(args.rowCount || 10);
  return (
    <AdminDataTableHeader
      {...args}
      rowCount={rowCount}
      onChangeRowCount={(e) => {
        setRowCount(e.target.value);
      }}
    />
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = { excelUrl: 'as' };

export const RowCount20 = Template.bind({});
RowCount20.args = { rowCount: 20, excelUrl: 'as' };

export const NoRowDropdown = Template.bind({});
NoRowDropdown.args = { visibleRowDropDown: false, excelUrl: 'as' };

export const NoExcelButton = Template.bind({});
NoExcelButton.args = { excelUrl: undefined };
