import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import { PasswordChangeDialog } from 'components/adminCommon/PasswordChangeDialog';
import AppProvider from 'libs/providers/AppProvider';
import RootStore from 'stores/RootStore';

const rootStore = new RootStore();

export default {
  title: 'AdminCommon/PasswordChangeDialog',
  component: PasswordChangeDialog,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  return (
    <AppProvider rootStore={rootStore}>
      <PasswordChangeDialog {...args} />
    </AppProvider>
  );
};

export const Default = Template.bind({});
// Default.args = { store };
Default.parameters = { mobxStore: rootStore.passwordChangeDialogStore };
