import React, { useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import {
  SearchKeyword,
  SearchKeywordProps,
} from 'components/adminCommon/SearchKeyword';

export default {
  title: 'AdminCommon/SearchKeyword', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: SearchKeyword, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<SearchKeywordProps> = (args) => {
  const [dropdownValue, setDropdownValue] = useState<string>();
  const [inputValue, setInputValue] = useState<string>('');
  return (
    <SearchKeyword
      {...args}
      dropdownValue={dropdownValue}
      inputValue={inputValue}
      onDropdownChange={(e) => {
        setDropdownValue(e.value);
      }}
      onInputChange={(e) => {
        setInputValue(e.target.value);
      }}
    />
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = {
  dropdownOptions: [
    { name: '제목', key: 'title' },
    { name: '등록인', key: 'regUser' },
  ],
};
