import React, { FC, useEffect, useState } from 'react';

import { Meta, Story } from '@storybook/react';
import {
  SearchControlItem,
  SearchCustomControlProps,
  SearchPanel,
  SearchPanelProps,
} from 'components/adminCommon/SearchPanel';
import {
  defaultSearchConditionRange,
  defaultSearchConditionSalesUnit,
  dateRangeControl,
  salesUnitControl,
} from 'libs/const';
import { today } from 'libs/common';
import { InputText } from 'smith-ui-components/inputtext';

export default {
  title: 'AdminCommon/SearchPanel',
  component: SearchPanel,
  argTypes: {},
} as Meta;

const Template: Story<SearchPanelProps> = (args: SearchPanelProps) => {
  return (
    <SearchPanel
      {...args}
      onReset={() => {
        console.log('Reset');
      }}
      onSearchClick={(searchCondition: any) => {
        console.log('Search', searchCondition);
      }}
    />
  );
};

export const WithSearchInfoComponentMessage = Template.bind({});
WithSearchInfoComponentMessage.args = {
  infoMessage: (
    <>
      매출은 <b>결제완료</b> 상태만 집계됩니다
    </>
  ),
  controls: [dateRangeControl],
  defaultSearchCondition: {
    ...defaultSearchConditionRange,
  },
};

export const WithSearchInfoStringMessage = Template.bind({});
WithSearchInfoStringMessage.args = {
  infoMessage: '매출은 결제완료 상태만 집계됩니다',
  controls: [dateRangeControl],
  defaultSearchCondition: {
    ...defaultSearchConditionRange,
  },
};

export const DateRangeControl = Template.bind({});
DateRangeControl.args = {
  controls: [dateRangeControl],
  defaultSearchCondition: {
    ...defaultSearchConditionRange,
  },
};

export const DateRangeNoShortcutsControl = Template.bind({});
DateRangeNoShortcutsControl.args = {
  controls: [
    {
      label: '기간',
      id: 'range',
      control: 'dateRangePicker',
      className: 'p-col-12',
      controlProps: { shortcuts: [] },
    },
  ],
  defaultSearchCondition: {
    ...defaultSearchConditionRange,
  },
};

// storybook에 add라는 이름의 메소드사용시 에러 예외를 위해 따로 변수로 뺌
const endDateAddAmount = 10;
/**
 * 드롭다운 + 기간 검색
 */
export const DropdownDateRangeControl = Template.bind({});
DropdownDateRangeControl.args = {
  controls: [
    {
      label: '기간 검색',
      id: 'dropdownDateRange',
      control: 'dropdownDateRangePicker',
      className: 'p-col-12',
      items: [
        { name: '등록일', key: 'regDate' },
        { name: '최종 접속일', key: 'latestDate' },
      ],
    },
  ],
  defaultSearchCondition: {
    dropdownDateRange: {
      dropdown: 'regDate',
      range: {
        startDate: today.format('YYYY-MM-DD'),
        endDate: today.add(endDateAddAmount, 'day').format('YYYY-MM-DD'),
      },
    },
  },
};

export const RadioControl = Template.bind({});
RadioControl.args = {
  controls: [salesUnitControl],
  defaultSearchCondition: {
    ...defaultSearchConditionSalesUnit,
  },
};

export const DropDownControl = Template.bind({});
DropDownControl.args = {
  controls: [
    {
      label: '의뢰 카테고리',
      id: 'requestCategory',
      control: 'dropdown',
      className: 'p-col-6',
      items: [
        { name: '전체', key: 'all' },
        { name: '에듀', key: 'education' },
        { name: '키즈', key: 'kids' },
        { name: '영화', key: 'movie' },
      ],
      //     - Dropdown으로 의뢰카테고리 선택 ⎿ 요소 : 전체, 에듀, 키즈, 영화
      // ⎿ Default : 전체
    },
  ],
  defaultSearchCondition: {
    requestCategory: 'all',
  },
};

/**
 * CheckBox 검색
 */
export const CheckboxControl = Template.bind({});
CheckboxControl.args = {
  controls: [
    {
      label: '페이지 노출 위치',
      id: 'pageLocation',
      control: 'checkbox',
      className: 'p-col-12',
      items: [
        { name: '일별', key: 'day' },
        { name: '주별', key: 'week' },
        { name: '월별', key: 'month' },
        { name: '년별', key: 'year' },
      ],
    },
  ],
  defaultSearchCondition: {
    pageLocation: ['day', 'week'],
  },
};

/**
 * 의뢰별 검색
 */
export const InputControl = Template.bind({});
InputControl.args = {
  controls: [
    {
      label: '신청자명',
      id: 'userName',
      control: 'input',
      className: 'p-col-12',
    },
  ],
  defaultSearchCondition: {
    userName: '',
  },
};

/**
 * InputCheckbox 검색
 */
export const InputCheckboxControl = Template.bind({});
InputCheckboxControl.args = {
  controls: [
    {
      label: 'CS 담당자명',
      id: 'csManager',
      control: 'inputAndCheckbox',
      className: 'p-col-12',
      items: [{ name: '내 문의만 보기', key: 'onlyMine' }],
    },
  ],
  defaultSearchCondition: {
    csManager: { inputValue: 'asf', checkValues: ['onlyMine'] },
  },
};

/**
 * dropdown + input 검색
 */
export const SearchKeywordControl = Template.bind({});
SearchKeywordControl.args = {
  controls: [
    {
      label: '검색어',
      id: 'searchKeyword',
      control: 'searchKeyword',
      className: 'p-col-12',
      items: [
        { name: '제목', key: 'title' },
        { name: '등록인', key: 'register' },
      ],
    },
  ],
  defaultSearchCondition: {
    searchKeyword: {
      dropdownValue: 'title',
      inputValue: 'asf',
    },
  },
};

/**
 * dropdown + input +check 검색
 */
export const SearchKeywordCheckControl = Template.bind({});
SearchKeywordCheckControl.args = {
  controls: [
    {
      label: '검색어',
      id: 'searchKeyword',
      control: 'searchKeyword',
      className: 'p-col-12',
      items: [
        { name: '제목', key: 'title' },
        { name: '등록인', key: 'register' },
      ],
      checkItems: [
        { name: '내 문의만 보기', key: 'onlyMine' },
        { name: '다른체크', key: 'check1' },
      ],
    },
  ],
  defaultSearchCondition: {
    searchKeyword: {
      dropdownValue: 'title',
      inputValue: 'asf',
      checkValues: ['onlyMine', 'check1'],
    },
  },
};

/**
 * Email
 */
export const EmailControl = Template.bind({});
EmailControl.args = {
  controls: [
    {
      label: '이메일',
      id: 'email',
      control: 'email',
      className: 'p-col-12',
    },
  ],
  defaultSearchCondition: {
    email: {
      part1: 'guhyeon',
      part2: 'naver.com',
    },
  },
};

/**
 * 복합
 */
export const Complicate = Template.bind({});
Complicate.args = {
  controls: [
    dateRangeControl,
    {
      label: '신청자명',
      id: 'userName',
      control: 'input',
      className: 'p-col-6',
    },
    {
      label: '의뢰 카테고리',
      id: 'requestCategory',
      control: 'dropdown',
      className: 'p-col-6',
      items: [
        { name: '전체', key: 'all' },
        { name: '에듀', key: 'education' },
        { name: '키즈', key: 'kids' },
        { name: '영화', key: 'movie' },
      ],
    },
  ],
  defaultSearchCondition: {
    ...defaultSearchConditionRange,
    userName: '',
    requestCategory: 'all',
  },
};

export const Controlled: Story<SearchPanelProps> = () => {
  const defaultSearchCondition = {
    ...defaultSearchConditionRange,
    userName: '',
    requestCategory: 'all',
    requestName: 'all',
  };
  const [searchCondition, setSearchCondition] = useState<any>();
  const [requestNames, setRequestNames] = useState<SearchControlItem[]>([]);
  useEffect(() => {
    if (requestNames.length === 0) return;
    console.log('requestNames', requestNames);
  }, [requestNames]);

  useEffect(() => {
    console.log(searchCondition);
  }, [searchCondition]);
  return (
    <SearchPanel
      searchCondition={searchCondition}
      onChange={(eventSearchCondition: any) => {
        setTimeout(() => {
          if (eventSearchCondition.requestCategory === 'education') {
            setRequestNames([
              { name: 'education1', key: 'requestId1' },
              { name: 'education2', key: 'requestId2' },
              { name: 'education3', key: 'requestId3' },
              { name: 'education4', key: 'requestId4' },
            ]);
          } else if (eventSearchCondition.requestCategory === 'kids') {
            setRequestNames([
              { name: '어린이의뢰1', key: 'requestId1' },
              { name: '어린이의뢰2', key: 'requestId2' },
              { name: '어린이의뢰3', key: 'requestId3' },
              { name: '어린이의뢰4', key: 'requestId4' },
            ]);
          } else {
            setRequestNames([]);
          }
        }, 200);
        setSearchCondition(eventSearchCondition);
      }}
      controls={[
        dateRangeControl,
        {
          label: '신청자명',
          id: 'userName',
          control: 'input',
          className: 'p-col-6',
        },
        {
          label: '의뢰 카테고리',
          id: 'requestCategory',
          control: 'dropdown',
          className: 'p-col-6',
          items: [
            { name: '전체', key: 'all' },
            { name: '에듀', key: 'education' },
            { name: '키즈', key: 'kids' },
            { name: '영화', key: 'movie' },
          ],
        },
        {
          label: '의뢰명',
          id: 'requestName',
          control: 'dropdown',
          className: 'p-col-6',
          items: [{ name: '전체', key: 'all' }, ...requestNames],
        },
      ]}
      defaultSearchCondition={defaultSearchCondition}
      onReset={() => {
        console.log('Reset');
        setSearchCondition(defaultSearchCondition);
      }}
      onSearchClick={(searchCondition: any) => {
        console.log('Search', searchCondition);
      }}
    />
  );
};

export const SampleCustomControl: FC<
  SearchCustomControlProps<{ start: string; end: string }>
> = ({ value, onChange }) => {
  return (
    <>
      start:
      <InputText
        value={value?.start}
        onChange={(e) => {
          const start = e.target.value;
          onChange && onChange({ ...value, start });
        }}
      />
      ~ end:
      <InputText
        value={value?.end}
        onChange={(e) => {
          const end = e.target.value;
          onChange && onChange({ ...value, end });
        }}
      />
    </>
  );
};

export const UsingCustomControl = Template.bind({});
UsingCustomControl.args = {
  controls: [
    dateRangeControl,
    {
      label: '커스텀',
      id: 'custom',
      control: SampleCustomControl,
      className: 'p-col-12',
    },
  ],
  onChange: (searchCondition) => console.log(JSON.stringify(searchCondition)),
  defaultSearchCondition: {
    ...defaultSearchConditionRange,
    custom: { start: '12', end: 'aa' },
  },
};
