import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
// import { useNotify } from 'libs/providers/NotifyWrapper';
import AppProvider from 'libs/providers/AppProvider';
import RootStore from 'stores/RootStore';
import { Button } from 'smith-ui-components/button';
import { SocketApi } from 'apis/index';

export default {
  title: 'Pages/SampleCode/WebSocketAlertResponse',
  argTypes: {},
} as Meta;

/**
 * docs 패널에서 확인
 * @param args
 * @constructor
 * 개발서버 swagger http://dev1.blocksmith.xyz:9000/swagger-ui.html
 */
const Template: Story = (args) => {
  return (
    <AppProvider rootStore={new RootStore()} {...args}>
      <p>
        서버 에서 메세지 발송하는 기능 구현 /** 실제로는 사용하지 않을 예정
        (클라이언트에서 쓰지 않을 코드) **/
      </p>
      <Button
        label={'NotifyToAccount1'}
        onClick={() => {
          SocketApi?.kafka.initUsingPost({
            linkUrl: 'www.naver.com',
            message: '1메시지',
            messageId: '1',
            messageType: 'N',
            targetType: 'A',
            username: '1',
          });
        }}
      />
      <p>
        서버 에서 메세지 발송하는 기능 구현 /** 실제로는 사용하지 않을 예정
        (클라이언트에서 쓰지 않을 코드) **/
      </p>
      <Button
        label={'NotifyToGuest'}
        onClick={() => {
          SocketApi?.kafka.initUsingPost({
            linkUrl: 'www.naver.com',
            message: '메세지당당당숭구리당당',
            messageId: '2',
            messageType: 'N',
            targetType: 'A',
            username: 'guest',
          });
        }}
      />
    </AppProvider>
  );
};

/**
 * 게스트 로그인
 */
export const Guest = Template.bind({});

/**
 * 1 로그인
 */
export const Account1 = Template.bind({});
Account1.args = { accountId: '1' };
// export const Account2 = Template.bind({});
// Account2.args = { accountId: '2' };
