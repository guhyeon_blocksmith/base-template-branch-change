import React, { useRef } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import HtmlToPDF, { HtmlToPDFElement } from 'components/common/HtmlToPDF';
import Signature from 'components/common/Signature';

export default {
  title: 'Common/PDF/HtmlToPDF',
  component: HtmlToPDF,
  argTypes: {},
} as Meta;

const Template: Story = (args) => {
  const ref = useRef<HtmlToPDFElement>(null);

  return (
    <>
      <HtmlToPDF ref={ref}>
        {args.contents}
        <Signature
          userName={args.userName}
          onClose={(base64?: string) => {
            console.log(base64);
          }}
        />
      </HtmlToPDF>
      <button onClick={() => ref.current?.printToPdf()}>pdf 저장</button>
      <button onClick={() => ref.current?.printToPdfBlob()}>blob pdf</button>
    </>
  );
};

export const Default = Template.bind({});
Default.args = {
  userName: '김철수',
  contents: (
    <>
      <h2>안녕</h2>
      <h3>hi hello</h3>
    </>
  ),
};
