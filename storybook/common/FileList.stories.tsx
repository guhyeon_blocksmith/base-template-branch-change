import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import FileList, { FileListProps } from 'components/common/FileList';

export default {
  title: 'Common/FileList',
  component: FileList,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<FileListProps> = (args) => (
  <FileList
    {...args}
    attachedFiles={[
      { name: 'file1.txt', url: '' },
      { name: 'file2.txt', url: '' },
      { name: 'file3.txt', url: '' },
    ]}
  />
);

export const Default = Template.bind({});
Default.args = {};

export const NotDeletable = Template.bind({});
NotDeletable.args = {
  deletable: false,
};
