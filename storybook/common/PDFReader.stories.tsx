import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import PDFReader from 'components/common/PDFReader';

export default {
  title: 'Common/PDF/PDFReader',
  component: PDFReader,
  argTypes: {},
} as Meta;

const Template: Story = () => <PDFReader />;

export const Default = Template.bind({});
Default.args = {
  fileName: 'asdf',
};
