import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import GridTable, { GridTableProps } from 'components/common/GridTable';
import GridTableItem from 'components/common/GridTableItem';
import { InputText } from 'smith-ui-components/inputtext';
import { Password } from 'smith-ui-components/password';
import { Button } from 'smith-ui-components/button';
import GridTableRowSpan from 'components/common/GridTableRowSpan';

export default {
  title: 'Common/GridTable/GridTable',
  component: GridTable,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

export const Default: Story<GridTableProps> = () => (
  <GridTable>
    <GridTableItem title={'제목2'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'}>
      <InputText />
    </GridTableItem>
  </GridTable>
);
Default.args = {};

export const GridTitle: Story<GridTableProps> = () => (
  <GridTable gridTitle="그리드 타이틀">
    <GridTableItem title={'제목2'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'}>
      <InputText />
    </GridTableItem>
  </GridTable>
);
GridTitle.args = {};

export const GridTitleWithRight: Story<GridTableProps> = () => (
  <GridTable
    gridTitle="그리드 타이틀"
    gridTitleWithRight={
      <>
        <InputText />
      </>
    }
  >
    <GridTableItem title={'제목2'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'}>
      <InputText />
    </GridTableItem>
  </GridTable>
);
GridTitleWithRight.args = {};

export const MultiRow: Story<GridTableProps> = () => (
  <GridTable>
    <GridTableItem
      title={
        <>
          제목
          <input
            type="checkbox"
            style={{
              position: 'relative',
              top: '2px',
              marginLeft: '4px',
            }}
          />
        </>
      }
      className={'p-col-4 p-md-6 p-lg-4'}
    >
      <InputText />
    </GridTableItem>
    <GridTableItem title={'제목2'} className={'p-col-4 p-md-6 p-lg-4'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'} className={'p-col-4 p-md-6 p-lg-4'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'제목'} className={'p-col-4 p-md-6 p-lg-4'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'성공!'} className={'p-col-4 p-md-6 p-lg-4'}>
      <Password />
    </GridTableItem>
    <GridTableItem title={'성공!'} className={'p-col-4 p-md-6 p-lg-4'}>
      <InputText />
      <Button label={'테스트'} />
    </GridTableItem>
  </GridTable>
);
MultiRow.args = {};

export const RowSpan: Story<GridTableProps> = () => (
  <GridTable>
    <GridTableItem
      title={
        <>
          제목
          <input
            type="checkbox"
            style={{
              position: 'relative',
              top: '2px',
              marginLeft: '4px',
            }}
          />
        </>
      }
      thWidth={130}
      className={'p-col-4'}
    >
      <InputText />
    </GridTableItem>
    <GridTableItem title={'제목2'} thWidth={130} className={'p-col-4'}>
      <InputText />
    </GridTableItem>
    <GridTableItem title={'버튼'} thWidth={130} className={'p-col-4'}>
      <InputText />
    </GridTableItem>
    <GridTableRowSpan
      rowTitle={
        <>
          <div style={{ textAlign: 'center' }}>
            <span>row</span>
            <br />
            <strong>span</strong>
          </div>
        </>
      }
      rowTitleWidth={80}
    >
      <GridTableItem title={'제목'} thWidth={50}>
        <InputText />
      </GridTableItem>
      <GridTableItem title={'성공!'} thWidth={50}>
        <Password />
      </GridTableItem>
      <GridTableItem title={'성공!'} thWidth={50}>
        <InputText />
        <Button label={'테스트'} />
      </GridTableItem>
    </GridTableRowSpan>
  </GridTable>
);
RowSpan.args = {};
