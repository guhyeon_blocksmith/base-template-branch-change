import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import GridTable from 'components/common/GridTable';
import GridTableItem, {
  GridTableItemProps,
} from 'components/common/GridTableItem';
import { InputText } from 'smith-ui-components/inputtext';

export default {
  title: 'Common/GridTable/GridTableItem',
  component: GridTableItem,
  argTypes: {},
} as Meta;

const Template: Story<GridTableItemProps> = (args) => (
  <GridTable>
    <GridTableItem title={'컬럼명'} {...args}>
      <InputText />
      <InputText />
    </GridTableItem>
    <GridTableItem title={'컬럼명'} {...args}>
      text
    </GridTableItem>
  </GridTable>
);

export const Default = Template.bind({});
Default.args = {};
