import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import GridTableItem from 'components/common/GridTableItem';
import { InputText } from 'smith-ui-components/inputtext';
import GridTableRowSpan, {
  GridTableRowSpanProps,
} from 'components/common/GridTableRowSpan';
import { Password } from 'smith-ui-components/password';
import { Button } from 'smith-ui-components/button';
import GridTable from 'components/common/GridTable';

export default {
  title: 'Common/GridTable/GridTableRowSpan',
  component: GridTableRowSpan,
  argTypes: {},
} as Meta;

const Template: Story<GridTableRowSpanProps> = () => (
  <GridTable>
    <GridTableRowSpan
      rowTitle={
        <>
          <div style={{ textAlign: 'center' }}>
            <span>row</span>
            <br />
            <strong>span</strong>
          </div>
        </>
      }
      rowTitleWidth={80}
    >
      <GridTableItem title={'제목'} thWidth={70}>
        <InputText />
      </GridTableItem>
      <GridTableItem title={'성공!'} thWidth={70}>
        <Password />
      </GridTableItem>
      <GridTableItem title={'성공!'} thWidth={70}>
        <InputText />
        <Button label={'테스트'} />
      </GridTableItem>
    </GridTableRowSpan>
  </GridTable>
);

export const Default = Template.bind({});
Default.args = {};
