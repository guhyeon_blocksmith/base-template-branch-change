import React, { useEffect, useState } from 'react';
import PDFWriter from 'components/common/PDFWriter';
import { Meta, Story } from '@storybook/react/types-6-0';
import { PDFViewer } from '@react-pdf/renderer';

export default {
  title: 'Common/PDF/PDFWriter',
  component: PDFWriter,
  argTypes: {},
} as Meta;

const Template: Story = () => {
  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    setIsClient(true);
  }, []);

  return (
    <div>
      {isClient && (
        <PDFViewer style={{ width: '500px', height: '400px' }}>
          <PDFWriter />
        </PDFViewer>
      )}
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {};
