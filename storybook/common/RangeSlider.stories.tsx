/* eslint-disable @typescript-eslint/ban-ts-comment */
import React, { useState } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import RangeSlider, {
  RangeSliderProps,
} from 'components/common/rangeSlider/RangeSlider';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'Common/RangeSlider', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: RangeSlider, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<RangeSliderProps> = (args) => {
  const domain = [0, 112];
  const [range, setRange] = useState<number[]>([5, 10]);

  return (
    <div style={{ width: 200 }}>
      <RangeSlider
        sliderStyle={{
          position: 'relative',
          width: '100%',
        }}
        {...args}
        domain={domain}
        value={range}
        callback={(res: number[]) => {
          console.log('-> res', ...res);
          setRange(res);
        }}
      />
    </div>
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = { maxGap: 20, minGap: 10 };
