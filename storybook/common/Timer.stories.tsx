import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Timer from 'components/common/Timer';

export default {
  title: 'common/Timer',
  component: Timer,
  argTypes: {},
} as Meta;

const Template: Story = (args) => <Timer {...args} />;

export const Default = Template.bind({});
Default.args = {
  flag: true,
  remainSeconds: 180,
};
