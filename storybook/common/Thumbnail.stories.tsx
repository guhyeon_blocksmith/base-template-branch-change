import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Thumbnail, { ThumbnailProps } from 'components/common/Thumbnail';

export default {
  title: 'common/Thumbnail',
  component: Thumbnail,
  argTypes: {},
} as Meta;

const Template: Story<ThumbnailProps> = (args) => {
  return <Thumbnail {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  imgSrc:
    'https://upload.wikimedia.org/wikipedia/commons/3/3b/20181013_%EC%9D%B4%EB%8B%AC%EC%9D%98_%EC%86%8C%EB%85%80_%EC%9D%8C%EC%95%85%EC%A4%91%EC%8B%AC_%EB%AF%B8%EB%8B%88_%ED%8C%AC%EB%AF%B8%ED%8C%85_%287%29.jpg',
};

export const S3Img = Template.bind({});
S3Img.args = {
  imgSrc: 'Front-Test2.jpg',
  s3: true,
};

export const HasThumbnailSrc = Template.bind({});
HasThumbnailSrc.args = {
  thumbSrc:
    'https://upload.wikimedia.org/wikipedia/commons/3/3b/20181013_%EC%9D%B4%EB%8B%AC%EC%9D%98_%EC%86%8C%EB%85%80_%EC%9D%8C%EC%95%85%EC%A4%91%EC%8B%AC_%EB%AF%B8%EB%8B%88_%ED%8C%AC%EB%AF%B8%ED%8C%85_%287%29.jpg',
  imgSrc:
    'https://meaningandmanipulation.files.wordpress.com/2015/07/c3316-unclesam.jpg',
};

export const CustomThumb = Template.bind({});
CustomThumb.args = {
  thumbSrc:
    'https://upload.wikimedia.org/wikipedia/commons/3/3b/20181013_%EC%9D%B4%EB%8B%AC%EC%9D%98_%EC%86%8C%EB%85%80_%EC%9D%8C%EC%95%85%EC%A4%91%EC%8B%AC_%EB%AF%B8%EB%8B%88_%ED%8C%AC%EB%AF%B8%ED%8C%85_%287%29.jpg',
  imgSrc:
    'https://meaningandmanipulation.files.wordpress.com/2015/07/c3316-unclesam.jpg',
  thumbStyle: { height: '100px' },
};

export const CustomPopup = Template.bind({});
CustomPopup.args = {
  thumbSrc:
    'https://upload.wikimedia.org/wikipedia/commons/3/3b/20181013_%EC%9D%B4%EB%8B%AC%EC%9D%98_%EC%86%8C%EB%85%80_%EC%9D%8C%EC%95%85%EC%A4%91%EC%8B%AC_%EB%AF%B8%EB%8B%88_%ED%8C%AC%EB%AF%B8%ED%8C%85_%287%29.jpg',
  imgSrc:
    'https://meaningandmanipulation.files.wordpress.com/2015/07/c3316-unclesam.jpg',
  popupStyle: { width: '700px' },
};
