import React, { useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import PassButton, {
  CompleteDataType,
  PassButtonProps,
} from 'components/common/PassButton';

export default {
  title: 'Common/PassButton',
  component: PassButton,
  argTypes: {},
} as Meta;

const Template: Story<PassButtonProps> = (args) => {
  const [testName, setTestName] = useState<string>('');
  return (
    <>
      <PassButton
        {...args}
        onComplete={(completeDataType: CompleteDataType) => {
          setTestName(completeDataType.RSLT_NAME);
        }}
      />

      <h1>{testName}</h1>
    </>
  );
};

export const Default = Template.bind({});
Default.args = {
  popupUrl: 'https://kcb.we-will.link/phone_popup2.jsp',
};

export const Red = Template.bind({});
Red.args = {
  style: { backgroundColor: 'red' },
  popupUrl: 'https://kcb.we-will.link/phone_popup2.jsp',
};
