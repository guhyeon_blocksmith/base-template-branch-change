import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { InputTextProps } from 'smith-ui-components/inputtext';
import InputTextOnlyKor from 'components/common/inputText/InputTextOnlyKor';

export default {
  title: 'common/InputText/InputTextOnlyKor',
  component: InputTextOnlyKor,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<InputTextProps> = (args) => (
  <InputTextOnlyKor {...args} />
);

export const Default = Template.bind({});
Default.args = {};
