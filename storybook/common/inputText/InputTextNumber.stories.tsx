import React, { useState } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { InputTextProps } from 'smith-ui-components/inputtext';
import InputTextNumber from 'components/common/inputText/InputTextNumber';

export default {
  title: 'common/InputText/InputTextNumber',
  component: InputTextNumber,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<InputTextProps> = (args) => (
  <InputTextNumber
    {...args}
    onChange={({ event, value }) => {
      console.log(typeof event.target.value, event.target.value);
      console.log(typeof value, value);
    }}
  />
);

export const Default = Template.bind({});
Default.args = {};

export const Positive = Template.bind({});
Positive.args = { min: 0 };

export const MinMinus1 = Template.bind({});
MinMinus1.args = { min: -1 };

export const Max1000 = Template.bind({});
Max1000.args = { max: 1000 };

export const Controlled: Story<InputTextProps> = () => {
  const [num, setNum] = useState<number | undefined>(5000);
  return (
    <>
      <input
        value={num}
        onChange={(e) => {
          if (e.target.value !== undefined || e.target.value !== '') {
            if (Number(e.target.value) > 1000) {
              setNum(1000);
            } else {
              setNum(Number(e.target.value));
            }
          }
        }}
      />
      <InputTextNumber
        value={num}
        onChange={({ event, value }) => {
          console.log(typeof event.target.value, event.target.value);
          console.log(typeof value, value);
          setNum(value);
        }}
      />
    </>
  );
};

export const NoThousandMark = Template.bind({});
NoThousandMark.args = { thousandComma: false };
