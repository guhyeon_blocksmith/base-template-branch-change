import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { InputTextProps } from 'smith-ui-components/inputtext';
import InputTextUserName from 'components/common/inputText/InputTextUserName';

export default {
  title: 'common/InputText/InputTextUserName',
  component: InputTextUserName,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<InputTextProps> = (args) => (
  <InputTextUserName {...args} />
);

export const Default = Template.bind({});
Default.args = {};
