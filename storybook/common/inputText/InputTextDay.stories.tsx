import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import InputTextDay from 'components/common/inputText/InputTextDay';
import { InputTextProps } from 'smith-ui-components/inputtext';

export default {
  title: 'common/InputText/InputTextDay',
  component: InputTextDay,
  argTypes: {
    // backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<InputTextProps> = (args) => (
  <InputTextDay {...args} onChange={(e) => console.log(e.target.value)} />
);

export const Default = Template.bind({});
Default.args = {};
