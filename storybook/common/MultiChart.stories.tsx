import React, { useEffect, useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import MultiChart, {
  MultiChartDataType,
  MultiChartsProps,
} from 'components/common/MultiChart';
import { clearTimeout } from 'timers';

export default {
  title: 'Common/MultiChart',
  component: MultiChart,
} as Meta;

const Template: Story<MultiChartsProps> = (args) => (
  <MultiChart
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    chartData={{
      labels: ['10대', '20대', '30대', '40대', '50대', '60대', '기타'], //x축 값(horizontal bar에서만 y축 값)
      datasets: [
        {
          data: [65, 59, 80, 81, 56, 55, 30],
          borderColor: '#42A5F5',
          fill: false,
          backgroundColor: [
            '#42A5F5',
            '#ea3719',
            '#6ecd20',
            '#ae3bae',
            '#018263',
            '#017292',
            '#f5a0da',
          ],
        },
      ],
    }}
    {...args}
  />
);

export const Bar = Template.bind({});
Bar.args = {
  title: '1.귀하의 나이는??',
  defaultChartType: 'bar',
};

export const Line = Template.bind({});
Line.args = {
  title: '2. 귀하의 연령대는?',
  defaultChartType: 'line',
};

export const Pie = Template.bind({});
Pie.args = { title: '3. 귀하의 나이는?', defaultChartType: 'pie' };

export const Doughnut = Template.bind({});
Doughnut.args = { title: '4. 귀하의 연령대는?', defaultChartType: 'doughnut' };

export const HorizontalBar = Template.bind({});
HorizontalBar.args = {
  title: '5. 귀하의 나이는?',
  defaultChartType: 'horizontalBar',
};

export const ChartDataChange: Story<MultiChartsProps> = (args) => {
  const [chartData, setChartData] = useState<MultiChartDataType>({
    labels: ['10대', '20대', '30대', '40대', '50대', '60대', '기타'], //x축 값(horizontal bar에서만 y축 값)
    datasets: [
      {
        data: [65, 59, 80, 81, 56, 55, 30],
        borderColor: '#42A5F5',
        fill: false,
        backgroundColor: [
          '#42A5F5',
          '#ea3719',
          '#6ecd20',
          '#ae3bae',
          '#018263',
          '#017292',
          '#f5a0da',
        ],
      },
    ],
  });

  useEffect(() => {
    const timeOut = setTimeout(() => {
      console.log('>>>>>');
      setChartData({
        labels: ['1대', '2대', '3대', '4대', '5대', '기타'], //x축 값(horizontal bar에서만 y축 값)
        datasets: [
          {
            data: [215, 519, 580, 281, 356, 455],
            borderColor: '#42A5F5',
            fill: false,
            backgroundColor: [
              '#ea3719',
              '#6ecd20',
              '#ae3bae',
              '#018263',
              '#017292',
              '#f5a0da',
            ],
          },
        ],
      });
    }, 2000);

    return () => {
      clearTimeout(timeOut);
    };
  }, []);

  return <MultiChart {...args} chartData={chartData} />;
};
ChartDataChange.args = {
  title: '5. 귀하의 나이는?',
  defaultChartType: 'horizontalBar',
};

export const MultiDataHorizontalBar = Template.bind({});
MultiDataHorizontalBar.args = {
  title: '5. 귀하의 나이는?',
  defaultChartType: 'horizontalBar',
  chartData: {
    labels: ['10대', '20대', '30대', '40대', '50대', '60대', '기타'], //x축 값(horizontal bar에서만 y축 값)
    datasets: [
      {
        data: [65, 59, 80, 81, 56, 55, 30],
        borderColor: '#42A5F5',
        fill: false,
        backgroundColor: [
          '#42A5F5',
          '#ea3719',
          '#6ecd20',
          '#ae3bae',
          '#018263',
          '#017292',
          '#f5a0da',
        ],
      },
      {
        data: [1, 2, 3, 4, 5, 6, 7],
        borderColor: '#42A5F5',
        fill: false,
        backgroundColor: [
          '#ff0000',
          '#cd00ff',
          '#ffb400',
          'rgba(0,227,255,0.54)',
          '#3aff00',
          '#bed4ff',
          '#ff00ae',
        ],
      },
    ],
  },
};
