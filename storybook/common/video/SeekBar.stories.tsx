/* eslint-disable @typescript-eslint/ban-ts-comment */
import React, { useRef, useState } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import playerStyles from 'components/common/video/VideoPlayer.module.scss';
import styles from 'components/common/video/VideoPlayer.module.scss';
import { SeekBar, SeekBarProps } from 'components/common/video/SeekBar';
import { ProgressState } from 'components/common/video/VideoPlayer';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'Common/Video/SeekBar', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: SeekBar, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<SeekBarProps> = (args) => {
  const [progressState, setProgressState] = useState<ProgressState>({
    played: 0,
    playedSeconds: 0,
  });

  const interval = useRef<NodeJS.Timeout>();

  return (
    <>
      <button
        onClick={() => {
          interval.current = setInterval(() => {
            setProgressState((p) => ({
              ...p,
              playedSeconds: p.playedSeconds! + 1,
              played: p.playedSeconds! / args.duration!,
            }));
          }, 100);
        }}
      >
        play
      </button>
      <button
        onClick={() => {
          if (interval.current) {
            clearInterval(interval.current);
          }
        }}
      >
        stop
      </button>
      <div className={styles.videoModule} style={{ height: 200 }}>
        <div className={playerStyles.controls}>
          <SeekBar
            {...args}
            progressState={progressState}
            onChange={(event) => {
              console.log('event', event.value);
              setProgressState((prev) => {
                return {
                  ...prev,
                  ...event.value,
                };
              });
            }}
            onPoint={(pointSeconds) => {
              console.log('onPoint', pointSeconds);
            }}
          />
        </div>
      </div>
    </>
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = { duration: 2500, points: [20, 60, 90] };

/**
 * 기본 스토리
 */
export const MaxMovable = Template.bind({});
MaxMovable.args = { duration: 2500, points: [20, 60, 90], maxMovable: 100 };
