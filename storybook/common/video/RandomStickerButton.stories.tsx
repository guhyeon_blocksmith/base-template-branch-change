/* eslint-disable @typescript-eslint/ban-ts-comment */
import React, { useEffect, useRef, useState } from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import RandomStickerButton, {
  RandomStickerButtonProps,
} from 'components/common/video/RandomStickerButton';
import { ProgressState } from 'components/common/video/VideoPlayer';
import { Button } from 'smith-ui-components/button';
import _ from 'lodash';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'Common/video/RandomStickerButton', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: RandomStickerButton, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<RandomStickerButtonProps> = (args) => {
  const [progressState, setProgressState] = useState<ProgressState>({
    // played: 0,
    playedSeconds: 0,
  });
  const [resetToggle, setResetToggle] = useState<boolean>();

  const interval = useRef<NodeJS.Timeout>();

  const [randomSeconds, setRandomSeconds] = useState<number>();

  const stop = () => {
    if (interval.current) {
      clearInterval(interval.current);
    }
  };
  const reset = () => {
    stop();
    setProgressState((p) => ({ ...p, playedSeconds: 0 }));
    setResetToggle((p) => !p);
  };

  // 랜덤 스티커 영역
  const [randomStickerArea, setRandomStickerArea] = useState<{
    width: number;
    height: number;
  }>({
    width: 0,
    height: 0,
  });

  useEffect(() => {
    if (
      progressState.playedSeconds !== undefined &&
      args.end !== undefined &&
      progressState.playedSeconds >= args.end
    ) {
      reset();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [progressState.playedSeconds, args.end]);

  const stickerAreaRef = useRef<HTMLDivElement>(null);

  // 랜덤 스티커 영역 설정
  useEffect(() => {
    const resizeListener = _.throttle(() => {
      const controller = stickerAreaRef.current;
      if (controller) {
        console.log(
          '>>>>>> controller',
          controller['offsetWidth'],
          controller['offsetHeight']
        );
        setRandomStickerArea({
          width: controller['offsetWidth'],
          height: controller['offsetHeight'],
        });
      }
    }, 500);
    window.addEventListener('resize', resizeListener);
    // 최초 위치 설정
    resizeListener();
    return () => {
      window.removeEventListener('resize', resizeListener);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div style={{ display: 'flex' }}>
        <Button
          style={{ flex: 'auto' }}
          onClick={() => {
            interval.current = setInterval(() => {
              setProgressState((p) => ({
                ...p,
                playedSeconds: p.playedSeconds! + 1,
                // played: p.playedSeconds! / args.duration!,
              }));
            }, 1000);
          }}
          label={'Play'}
        />
        <Button
          style={{ flex: 'auto' }}
          onClick={() => {
            stop();
          }}
          label={'stop'}
        />
        <Button
          style={{ flex: 'auto' }}
          onClick={() => {
            reset();
          }}
          label={'reset'}
        />
      </div>
      <span>{`${progressState.playedSeconds}/ ${args.end}`}</span>
      <br />
      <span>randomSeconds : {`${randomSeconds}`}</span>
      <div
        ref={stickerAreaRef}
        style={{
          width: '100%',
          height: '80%',
          left: 0,
          top: 150,
          background: 'gray',
          position: 'absolute',
        }}
      >
        스티커 노출 범위
        <RandomStickerButton
          // @ts-ignore
          area={randomStickerArea}
          onResult={(result) => {
            console.log('-> result', result);
          }}
          {...args}
          onRandomSeconds={(seconds) => {
            setRandomSeconds(seconds);
          }}
          resetToggle={resetToggle}
          playedSeconds={progressState.playedSeconds ?? 0}
        />
      </div>
    </>
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = { start: 0, end: 10, deadlineSeconds: 3 };

/**
 * 보임 여부
 */
export const Visible = Template.bind({});
Visible.args = { start: 0, end: 10, deadlineSeconds: 3, visible: true };
