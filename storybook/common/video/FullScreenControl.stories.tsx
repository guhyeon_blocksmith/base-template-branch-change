/* eslint-disable @typescript-eslint/ban-ts-comment */
import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import playerStyles from 'components/common/video/VideoPlayer.module.scss';
import styles from 'components/common/video/VideoPlayer.module.scss';
import {
  FullScreenControl,
  FullScreenControlProps,
} from 'components/common/video/FullScreenControl';

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'Common/Video/FullScreenControl', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: FullScreenControl, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<FullScreenControlProps> = (args) => {
  return (
    <div className={styles.videoModule} style={{ height: 200 }}>
      <div className={playerStyles.controls}>
        <div className={playerStyles.upArea}>
          <div className={playerStyles.rights}>
            <FullScreenControl {...args} />
          </div>
        </div>
      </div>
    </div>
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});
Default.args = {};
