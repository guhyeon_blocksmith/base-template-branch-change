/* eslint-disable @typescript-eslint/ban-ts-comment */
import React, { useRef, useState } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import VideoPlayer, {
  VideoPlayerElement,
  VideoPlayerProps,
} from 'components/common/video/VideoPlayer';
import { Button } from 'smith-ui-components/button';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';

dayjs.extend(duration);

// 스토리에 대한 정보. 스토리북에서 제목, doc애드온, control 애드온에 대한 정보를 생성합니다.
export default {
  title: 'Common/Video/VideoPlayer', // 제목은 좌측 스토리 목록에 나오는 제목으로 고유해야합니다.
  component: VideoPlayer, // 스토리 컴포넌트입니다. 컨트롤 및 docs에서 사용됩니다.
  argTypes: {}, // 컨트롤을 커스텀할 때 사용합니다.
} as Meta;

const Template: Story<VideoPlayerProps> = (args) => {
  return (
    <VideoPlayer
      guid={'sample-local-video-controller'}
      userId={'anonymous'}
      url={'https://youtu.be/XA2YEHn-A8Q'}
      {...args}
    />
  );
};

/**
 * 기본 스토리
 */
export const Default = Template.bind({});

export const FullScreen = Template.bind({});
FullScreen.args = { fullScreenEnabled: true };

export const NoFullScreen = Template.bind({});
NoFullScreen.args = { fullScreenEnabled: false };

/**
 * 이어보기. 이전 trackingLog를 참조하여 이전에 본 이후로 이어봄
 */
export const ContinuePlay = Template.bind({});
ContinuePlay.args = { continuePlay: true, tracking: true };

export const Playing = Template.bind({});
Playing.args = { playing: true };

export const ChangePlaying: Story<VideoPlayerProps> = (args) => {
  const [playing, setPlaying] = useState<boolean>(false);
  return (
    <>
      <button
        style={{ background: 'white', width: 100, height: 20 }}
        onClick={() => {
          setPlaying((p) => !p);
        }}
      >
        {playing ? 'stop' : 'play'}
      </button>
      <VideoPlayer
        guid={'sample-local-video-controller'}
        userId={'anonymous'}
        url={'https://youtu.be/XA2YEHn-A8Q'}
        {...args}
        onPlay={() => setPlaying(true)}
        onPause={() => setPlaying(false)}
        playing={playing}
      />
    </>
  );
};

/**
 * Remove UserVideoLog. 평가완료시 호출
 * @param args
 * @constructor
 */
export const RemoveUserVideoLog: Story<VideoPlayerProps> = (args) => {
  const ref = useRef<VideoPlayerElement>(null);

  return (
    <>
      <Button
        label={'사용자 비디오로그 제거'}
        style={{ background: 'white', height: 80, color: 'black' }}
        onClick={() => {
          ref.current?.clearUserVideoLog();
        }}
      />
      <span>{`개발자도구 > Application > Local Storage 참고`}</span>
      <VideoPlayer
        ref={ref}
        url={'https://m.youtube.com/watch?v=_OvP9YXm2rE'}
        guid={'sample-local-video-controller'}
        userId={'anonymous'}
        {...args}
      />
    </>
  );
};

RemoveUserVideoLog.args = {
  tracking: true,
};

/**
 * RandomSticker.
 */
export const RandomSticker: Story<VideoPlayerProps> = (args) => {
  const ref = useRef<VideoPlayerElement>(null);

  const [randomSeconds, setRandomSeconds] = useState<number>();
  const [playState, setPlayState] = useState<{
    played: number;
    playedSeconds: number;
    loaded: number;
    loadedSeconds: number;
  }>();

  if (
    args &&
    args.randomSticker &&
    args.randomSticker.onRandomSeconds === undefined
  ) {
    args.randomSticker.onRandomSeconds = (randomSeconds) => {
      setRandomSeconds(randomSeconds);
    };
  }

  return (
    <div>
      <span>
        randomSeconds :{' '}
        {`${dayjs.duration(randomSeconds ?? 0, 'seconds').format('HH:mm:ss')}`}
      </span>
      ,&nbsp;
      <span>
        played :
        {`${dayjs
          .duration(playState?.playedSeconds ?? 0, 'seconds')
          .format('HH:mm:ss')}`}
      </span>
      <Button
        label={'사용자 비디오 로그 제거'}
        style={{ background: 'white', height: 80, color: 'black' }}
        onClick={() => {
          ref.current?.clearUserVideoLog();
        }}
      />
      <pre>
        localstorage에 sr_{`{guid}_{userId}`}로 결과를 저장하고, 이미 결과가
        있으면 랜덤스티커를 보여주지 않는다.
      </pre>
      <VideoPlayer
        ref={ref}
        guid={'sample-local-video-controller'}
        userId={'anonymous'}
        url={'https://youtu.be/XA2YEHn-A8Q'}
        {...args}
        onProgress={(p) => {
          setPlayState(p);
        }}
      />
      <div style={{ height: 1000 }} />
    </div>
  );
};

RandomSticker.args = {
  randomSticker: {
    // onStickerResult: (result) => {
    //   console.log('onStickerReuslt', result);
    // },
    enabled: true,
  },
  url: 'https://www.youtube.com/watch?v=bqxby7GvAJc', // 1분 45초
  // url: 'https://www.youtube.com/watch?v=4ir7xgcpzg0', // 1분 영상
};

/**
 * TrackingLog 관련.
 */
export const TrackingLog: Story<VideoPlayerProps> = (args) => {
  const ref = useRef<VideoPlayerElement>(null);
  return (
    <div>
      <Button
        label={'트래킹로그 가져오기'}
        style={{ background: 'white', height: 80, color: 'black' }}
        onClick={() => {
          const trackingLog = ref.current?.getTrackingLog();
          console.log(trackingLog);
        }}
      />
      <Button
        label={'사용자 비디오 로그 제거'}
        style={{ background: 'white', height: 80, color: 'black' }}
        onClick={() => {
          ref.current?.clearUserVideoLog();
        }}
      />

      <p>
        <span>{`개발자도구 > Application > Local Storage 참고`}</span>
      </p>
      <p>
        <span>{`개발자도구 > console log 참고`}</span>
      </p>
      <VideoPlayer
        ref={ref}
        guid={'sample-local-video-controller'}
        userId={'anonymous'}
        url={'https://youtu.be/XA2YEHn-A8Q'}
        {...args}
      />
    </div>
  );
};

TrackingLog.args = {
  tracking: true,
};

/**
 * 배속 기능
 */
export const PlaySpeed = Template.bind({});
PlaySpeed.args = {
  speedRates: [1.0, 1.25, 1.5, 1.75, 2.0],
};
/**
 * 화질 조절 기능
 */
export const VideoQuality = Template.bind({});
VideoQuality.args = {
  qualities: ['자동', '144p', '240p', '360p', '480p', '720p', '1080p'],
};
