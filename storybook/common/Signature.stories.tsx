import React from 'react';
import Signature, { SignatureProps } from 'components/common/Signature';
import { Meta, Story } from '@storybook/react/types-6-0';

export default {
  title: 'Common/Signature',
  component: Signature,
  argTypes: {},
} as Meta;

const Template: Story<SignatureProps> = (args) => {
  return (
    <Signature
      {...args}
      onClose={(base64?: string) => {
        console.log(base64);
      }}
    />
  );
};

export const Default = Template.bind({});
