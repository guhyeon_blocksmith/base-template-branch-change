import React from 'react';
import Img, { ImgProps } from 'components/common/Img';
import { Meta, Story } from '@storybook/react/types-6-0';

export default {
  title: 'Common/Img',
  component: Img,
  argTypes: {},
} as Meta;

const Template: Story<ImgProps> = (args) => {
  return <Img {...args} />;
};

/**
 * Default 이미지. S3에 이미지가 없는 경우
 */
export const Default = Template.bind({});
Default.args = { src: '/images/logo.png' };

/**
 * S3 small 이미지
 */
export const Small = Template.bind({});
Small.args = { size: 's', fileName: 'Front-Test2.jpg' };

export const Medium = Template.bind({});
Medium.args = { size: 'm', fileName: 'Front-Test2.jpg' };

export const Large = Template.bind({});
Large.args = { size: 'l', fileName: 'Front-Test2.jpg' };

/**
 * S3 origin 이미지만 존재하는 경우
 */
export const OnlyOrigin = Template.bind({});
OnlyOrigin.args = { size: 's', fileName: 'Front-Test.jpg' };

/**
 * S3에 존재하지않는 이미지인 경우
 */
export const NoExist = Template.bind({});
NoExist.args = { size: 's', fileName: 'FrontTest.jpg' };
