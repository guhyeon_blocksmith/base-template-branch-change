import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import ImageLabelChart, {
  ImageLabelChartProps,
} from 'components/common/ImageLabelChart';

export default {
  title: 'Common/ImageLabelChart',
  component: ImageLabelChart,
  argTypes: {},
} as Meta;

const Template: Story<ImageLabelChartProps> = () => (
  <ImageLabelChart
    data={{
      datasets: [
        {
          backgroundColor: ['#fff', '#f00', '#fff', '#f00'],
          data: [1456, 467, 134, 9113],
        },
        {
          backgroundColor: '#cd4c77',
          data: [123, 1164, 174, 1234],
        },
        {
          backgroundColor: '#5aece6',
          data: [134, 1423, 122, 163],
        },
        {
          backgroundColor: '#ac52ca',
          data: [156, 112, 121, 1119],
        },
        {
          backgroundColor: '#2257ff',
          data: [131, 146, 1113, 123],
        },
      ],
      labels: [
        {
          label:
            '저먼 셰퍼드 입니다 잘 부탁드립니다. 문장이 길 경우 줄 바꿈 처리되어 텍스트가 노출 됩니다. 문장이 길 경우 줄 바꿈 처리되어 텍스트가 노출 됩니다.',
          url:
            'https://img.huffingtonpost.com/asset/5ec601832500000f1eeb1d4f.jpeg?cache=WuaLzjSHpx&ops=1778_1000',
        },
        {
          label: '래브라도 리트리버',
          url:
            'https://www.sciencetimes.co.kr/wp-content/uploads/2019/11/dog-3.jpg',
        },
        {
          // label: '시바견',
          label: '',
          url:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoNgG00qO10GDR2J4mo3fdg1oL0fhMU-EkDQ&usqp=CAU',
        },
        {
          label: '말티즈',
          url: '',
        },
        // {
        //   label: '동네개',
        //   url:
        //     'https://newsimg.hankookilbo.com/cms/articlerelease/2015/12/17/201512171111528240_2.jpg',
        // },
      ],
    }}
    options={{ legend: { labels: ['20대', '30대', '40대', '50대', '60대'] } }}
  />
);

export const Default = Template.bind({});
Default.args = {};
