import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import SliderCustom, {
  SliderCustomProps,
} from 'components/common/SliderCustom';

export default {
  title: 'common/SliderCustom',
  component: SliderCustom,
  argTypes: {},
} as Meta;

export const Default: Story<SliderCustomProps> = (args) => (
  <SliderCustom {...args} />
);
Default.args = {};

export const MinimumRange: Story<SliderCustomProps> = (args) => (
  <SliderCustom {...args} />
);
MinimumRange.args = {
  step: 100,
  min: 100,
  max: 60000,
  visibleInput: true,
  visibleSection: true,
};
