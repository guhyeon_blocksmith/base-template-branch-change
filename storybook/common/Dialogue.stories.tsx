import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import Dialogue, { dialogueProps } from 'components/adminCommon/Dialogue';

export default {
  title: 'common/Dialogue',
  component: Dialogue,
  argTypes: {},
} as Meta;

const Template: Story<dialogueProps> = (args) => <Dialogue {...args} />;

export const Default = Template.bind({});
Default.args = {
  style: { position: 'relative' },
};

export const TextOnly = Template.bind({});
TextOnly.args = {
  visibleTextarea: false,
  style: { position: 'relative' },
};
