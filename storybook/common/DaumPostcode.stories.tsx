import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import DaumPostcode, {
  AddressData,
  DaumPostcodeProps,
} from 'react-daum-postcode';

export default {
  title: 'Common/DaumPostcode',
  component: DaumPostcode,
  argTypes: {},
} as Meta;

/**
 * 다음 카카오 스토리북 샘플
 * 속성 설명은 아래 2가지 url 참조
 * https://www.npmjs.com/package/react-daum-postcode#props
 * http://postcode.map.daum.net/guide#attributes
 * @param args
 * @constructor
 */
const Template: Story<DaumPostcodeProps> = (args) => {
  const handleComplete = (data: AddressData) => {
    let fullAddress = data.address;
    let extraAddress = '';

    if (data.addressType === 'R') {
      if (data.bname !== '') {
        extraAddress += data.bname;
      }
      if (data.buildingName !== '') {
        extraAddress +=
          extraAddress !== '' ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== '' ? ` (${extraAddress})` : '';
    }

    console.log(fullAddress); // e.g. '서울 성동구 왕십리로2길 20 (성수동1가)'
  };

  return <DaumPostcode {...args} onComplete={handleComplete} />;
};

export const Default = Template.bind({});
Default.args = {
  alwaysShowEngAddr: false,
  animation: false,
  autoClose: false,
  autoMapping: false,
  autoResize: false,
  defaultQuery: '',
  errorMessage: (
    <p>
      현재 Daum 우편번호 서비스를 이용할 수 없습니다. 잠시 후 다시 시도해주세요.
    </p>
  ),
  height: '400',
  hideEngBtn: false,
  hideMapBtn: false,
  maxSuggestItems: 10,
  pleaseReadGuide: 0,
  pleaseReadGuideTimer: 1.5,
  scriptUrl:
    'https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js',
  shorthand: false,
  showMoreHName: false,
  style: undefined,
  theme: undefined,
  useSuggest: false,
  width: '500px',
  submitMode: false,
  focusInput: false,
};
