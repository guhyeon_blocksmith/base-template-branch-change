import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import ProgressCircle, {
  ProgressCircleProps,
} from 'components/common/ProgressCircle';

export default {
  title: 'common/ProgressCircle',
  component: ProgressCircle,
  argTypes: {},
} as Meta;

/**
 * docs 패널에서 확인
 * @param args
 * @constructor
 */
const Template: Story<ProgressCircleProps> = (args) => {
  return <ProgressCircle {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  value: 10,
};

export const FixedSize = Template.bind({});
FixedSize.args = {
  value: 33,
  width: 100,
  height: 100,
};
