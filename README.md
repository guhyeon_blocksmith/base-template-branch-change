##프로젝트 설명
베이스 템플릿 프로젝트입니다. 이 프로젝트를 포크하여, 베이스 템플릿의 변경사항을 가져가면서 각자 프로젝트를 유지할 수 있습니다.
### 기술 스택
1. nextjs
2. typescript
3. classvalidator
4. prime-react
###새 프로젝트 시작하기
https://blocksmith.atlassian.net/wiki/spaces/smith/pages/281182221/base-template+-
###레이아웃 설정가이드
#### 레이아웃을 사용할지 Custom인지 여부에 따라 
####_app.tsx에 상단 import중 main.css를 주석 풀고 App.scss를 주석처리하세요
https://blocksmith.atlassian.net/wiki/spaces/smith/pages/266404239/base-template
###테마 스타일 css 생성 가이드
https://blocksmith.atlassian.net/wiki/spaces/smith/pages/241205249/UI
###UI Kit( smith-ui-components ) theme 적용 방법
1. public/themes에 원하는 theme.css의 폴더 명을 NEXT_PUBLIC_DEFAULT_THEME에 입력하세요.
 
###Layout theme 가이드
.env에 NEXT_PUBLIC_USE_THEME_SELECTOR를 true로 하면 하단에 테마를 선택할 수 있습니다.
테마 선택기가 true인 경우 기본 테마명은 NEXT_PUBLIC_DEFAULT_THEME 에 명시하면됩니다.

layout 관련 테마는 커스텀 테마로 theme.css 하단에 작성
###UI Kit( smith-ui-components ) Document
http://dev1.blocksmith.xyz:8090/#/
###폼 사용방법
https://github.com/typestack/class-validator 참고
1. types/formModels에 모델 파일 추가
2. class validator의 valid함수로 검증

###이미지 리소스 안내
https://blocksmith.atlassian.net/wiki/spaces/smith/pages/525467656/base-template **필독**

###주의사항
> **WARNING**: 프로젝트에 있는 파일들은 가급적 삭제하지 말아주세요.
###폴더 설명
    .
    ├── .ebextensions           # 빈스톡 사용시 업로드 용량 설정
    ├── .next                   # next 빌드 캐시파일. next build시 생성됩니다.
    ├── .platform               # 빈스톡 사용시 업로드 용량 설정
    ├── .storybook              # 스토리북 설정
    ├── apis                    # backend api 타입 및 호출 함수 모음
    │   └── fakeServer          # 개발용 로컬 api server
    ├── assets                  # 소스에서 import로 사용하는 글로벌 assets
    │   ├── layout              # layout assets
    │   │   ├── app             # Layout 스타일 (레이아웃 사용시 사용하는 스타일)  ( 삭제 X, 수정 O)
    │   └── └── custom          # Layout Custom assets
    │                           #  .env에 NEXT_PUBLIC_IS_CUSTOM_LAYOUT= true 인 경우 사용하는 스타일
    │                           #  .env에 NEXT_PUBLIC_PROJECT_CUSTOM_CSS 경로에서 커스텀 가능.
    ├── components              # 리액트 컴포넌트 디렉토리
    │   ├── common              # 공통 컴포넌트( 삭제 X, 수정 X)
    │   ├── layoutCustom        # 커스텀 레이아웃 컴포넌트
    │   └── └── sub             # 커스텀 서브페이지
    │   ├── layout              # 레이아웃 컴포넌트( 삭제 X, 수정 X)
    │   ├── sample              # 샘플 컴포넌트( 삭제 X, 수정 X)
    │   │   ├── board           # 게시판 CRUD 컴포넌트
    │   │   ├── components      # UI Kit(smith ui components) 데모 컴포넌트
    │   │   ├── member          # 회원 정보 CRUD 컴포넌트
    │   └── └── mobx            # Mobx 샘플 컴포넌트
    ├── libs                    # uitlity library
    │   ├── hooks               # React Custom Hooks
    │   └── providers           # React Providers
    ├── node_modules            # npm package module
    ├── pages                   # next route page. 폴더 경로가 라우트가 됩니다.
    │   ├── api                 # next api 라우트. next 에서 백엔드 api 개발시 사용.
    │   └── └── proxy           # next proxy api. ( 삭제 X, 수정 X)
    │   ├── auth                # 인증 관련 페이지 ( 삭제 X, 수정 O)
    │   ├── sample              # 샘플 페이지 ( 삭제 X, 수정 X )
    │   │   ├── board           # 게시판 페이지 ( 삭제 X, 수정 X)
    │   │   ├── member          # 회원 페이지 ( 삭제 X, 수정 X)
    │   └── └── mobx            # Mobx 샘플 컴포넌트 ( 삭제 X, 수정 X)
    │   └── sub                 # 커스텀 레이아웃 서브페이지
    ├── public                  # next static assets. url로 접근하는 assets 
    │   ├── images              # 이미지 assets
    │   ├── sample              # sample 페이지 관련 static assets
    │   └── themes              # smith-ui-components theme( 삭제 X, 수정 X)
    ├── store                   # mobx store. 상태 관리 클래스들 
    │   └── sample              # 샘플 페이지 관련 상태( 삭제 X, 수정 X)
    ├── storybook               # 스토리북 
    ├── types                   # 타입 
    │   ├── enum                # enum 타입
    │   ├── formModels          # form 모델. validation rule도 같이 정의
    │   └── sample              # 샘플 관련 타입( 삭제 X, 수정 X)
    └── uml                     # UML 시퀀스 다이어그램
- - - -

##프로젝트 실행방법
###프로젝트 설치
> yarn
####
###개발
개발 테스트 명령어는 상황에 알맞게 사용해주세요.
####개발 - 일반
fakeServer, .env.development 사용
>yarn dev
####개발 - 인증
fakeServer with auth, .env.development 사용
>yarn dev-auth
####개발 - fakeServer 없이
>yarn dev-no-db
###검증 ( 운영에서 빌드하는 프로세스 사용. 개발 태스크 처리시마다 돌려서 확인 할 것 )
상황에 알맞게 일반, 인증으로 나눠 사용해주세요.
####검증 - 일반
fakeServer, .env.production 사용
>yarn stg
####검증 - 인증
fakeServer with auth, .env.production 사용
>yarn stg-auth

###스토리북
fakeServer사용
>yarn storybook
####
fakeServer 없이
>yarn storybook-no-db
