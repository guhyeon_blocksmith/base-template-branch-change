import { FC } from 'react';
import { GetServerSideProps } from 'next';
import { deleteStatelessSession } from 'libs/AuthUtil';

/**
 *  SSR 방식 초기 데이터 가져오기
 */
export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  deleteStatelessSession(req, res, 'adminSession');

  return {
    redirect: {
      permanent: false,
      destination: '/',
    },
  };
};

/**
 * 로그아웃 페이지
 * @constructor
 */
const LogoutPage: FC = () => {
  return null;
};

export default LogoutPage;
