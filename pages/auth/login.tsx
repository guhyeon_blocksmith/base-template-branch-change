import React, { FC, useEffect, useState } from 'react';
import Layout from 'components/layout/Layout';
import { InputText } from 'smith-ui-components/inputtext';
import { Button } from 'smith-ui-components/button';
import { config, custom } from 'config/layoutSetting';
import useAppendRef from 'libs/hooks/useAppendRef';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';

import styles from './login.module.css';
import { Password } from 'smith-ui-components/password';
import { Checkbox } from 'smith-ui-components/checkbox';
import { useRouter } from 'next/router';
import { openLayoutMode } from 'types/enum';
import { fetchUserInfo } from 'components/layout/Layout/Layout';
import Image from 'next/image';

const pageConfig: any = {
  ...config,
  showHeader: false,
  showMenu: false,
};

const pageCustom = {
  ...custom,
  useCustomHeader: false,
  useCustomContent: false,
  useCustomFooter: false,
};

//로그인 페이지
// - Admin 접근시 노출되는 페이지 (비로그인 기준)
//
// /**
//  *  SSR 방식 초기 데이터 가져오기
//  */
// export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
//   console.log('4 getServerSideProps');

// try {
//   const accessToken = await getOrRefreshAccessToken(
//     req!,
//     res!,
//     'adminSession'
//   );
//   if (accessToken) {
//     return {
//       redirect: {
//         permanent: false,
//         destination: '/',
//       },
//     };
//   }
//   return { props: {} };
// } catch {
//   return { props: {} };
//   // }
// };

/**
 * Login. AC_LOGIN_001
 * @constructor
 */
const LoginPage: FC = observer(() => {
  // const inputRef = useRef<InputText>(null);
  const router = useRouter();
  const { returnUrl } = router.query;
  const { loginStore: store } = useStore();

  const [loginIdRef] = useAppendRef(store.inputRefs, 'loginId');
  const [passwordRef] = useAppendRef(store.inputRefs, 'password');

  const [isReady, setIsReady] = useState<boolean>();

  useEffect(() => {
    const preventShowLogin = async () => {
      if (router.pathname === '/auth/login') {
        // 로그인 페이지인 경우 userInfo가 있을 경우 home으로 이동한다.
        if (store.rootStore?.commonStore.userInfo) {
          // 로그인후 로그인페이지인 경우 home으로 이동
          await router.replace('/');
        } else if (store.rootStore?.commonStore.userInfo === undefined) {
          // layout을 통하지 않아 userInfo가 없는 경우
          const userInfo = await fetchUserInfo();
          if (userInfo) {
            await router.replace('/');
          }
        }
        setIsReady(true);
      }
    };
    preventShowLogin().then();
  }, [router, router.pathname, store.rootStore?.commonStore.userInfo]);

  useEffect(() => {
    store.init();
  }, [store]);

  if (!isReady) return null;

  return (
    <Layout
      config={pageConfig}
      custom={pageCustom}
      layoutAuth={openLayoutMode.public}
    >
      <>
        <form>
          <div className={styles.wrapper}>
            <div>
              <div className="content-section p-grid">
                <div className="p-col-12">
                  <h1>
                    <Image
                      src={pageConfig.header.logo?.src}
                      alt={pageConfig.header.logo?.alt}
                      width={pageConfig.header.logo?.width}
                      height={pageConfig.header.logo?.height}
                    />
                  </h1>
                  {/* 로고 - 클릭 요소 없음 */}
                </div>
                <div className="p-col-12">
                  <InputText
                    id="ID"
                    placeholder={'아이디'}
                    ref={loginIdRef}
                    maxLength={20}
                    value={store.loginItem.loginId}
                    onKeyDown={(e) => {
                      if (e.key === 'Enter') {
                        store.inputRefs.get('password').ref.inputEl.focus();
                      }
                    }}
                    onChange={store.handleIdChange}
                    className={`${styles.input}`}
                  />
                </div>
                <div className="p-col-12">
                  <Password
                    ref={passwordRef}
                    id="password"
                    placeholder="비밀번호"
                    feedback={false}
                    value={store.loginItem.password}
                    onChange={store.handlePasswordChange}
                    onKeyDown={(e) => {
                      if (e.key === 'Enter') {
                        store.submitLogin(returnUrl).then();
                      }
                    }}
                    className={`${styles.input}`}
                  />
                </div>
                <div className={'p-col-12'}>
                  {/*{store.validErrors.get('loginId') && (*/}
                  {/*  <small className="p-invalid p-d-block">*/}
                  {/*    최소 5자 이상 영문 소문자 및 숫자를 사용하세요*/}
                  {/*  </small>*/}
                  {/*)}*/}
                  {/*{store.validErrors.get('password') && (*/}
                  {/*  <small className="p-invalid p-d-block">*/}
                  {/*    최소 8자이상 영문 대 소문자, 숫자 및 특수문자를 함께*/}
                  {/*    사용하세요.*/}
                  {/*  </small>*/}
                  {/*)}*/}
                  {store.visibleLoginFailedMessage && (
                    <small className="p-invalid p-d-block">
                      가입하지 않은 아이디거나, 잘못된 비밀번호입니다.
                    </small>
                  )}
                </div>
                <div
                  className={`p-col-12 p-grid p-justify-between p-align-center ${styles.buttonWrapper}`}
                >
                  <div className="p-field-checkbox">
                    <Checkbox
                      inputId="saveId"
                      checked={store.isSaveId}
                      onChange={store.handleChangeSaveId}
                    />
                    <label htmlFor="saveId">아이디 저장</label>
                  </div>
                  <Button
                    type="button"
                    label="로그인"
                    onClick={() => store.submitLogin(returnUrl)}
                  />
                </div>
              </div>
            </div>
          </div>
        </form>
      </>
    </Layout>
  );
});

export default LoginPage;
