import React from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';

const World = () => {
  return (
    <Layout config={config} custom={custom}>
      <>
        <h1>World</h1>
        <p>Menu Test</p>
      </>
    </Layout>
  );
};

export default World;
