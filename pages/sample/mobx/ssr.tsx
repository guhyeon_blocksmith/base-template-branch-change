import React, { useEffect } from 'react';

import Clock from 'components/sample/mobx/Clock';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';
import MobxStore from 'stores/sample/MobxStore';
import Navigations from 'components/sample/mobx/Navigations';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { GetServerSideProps } from 'next';

/**
 * SSR 초기화 방식. 매 렌더링마다 서버에서 해당 로직을 호출합니다.
 */
export const getServerSideProps: GetServerSideProps = async () => {
  const mobxStore = new MobxStore();
  mobxStore.light = false;
  mobxStore.lastUpdate = Date.now();
  mobxStore.title =
    'SSR - server side rendering. 매 호출 시마다 서버에서 렌더링한 값이 들어갑니다.1111';

  return { props: { initialStore: JSON.stringify({ mobxStore }) } };
};

/**
 * Mobx 샘플 페이지. ssr
 * @constructor
 */
const MobxSsr = observer(() => {
  const { mobxStore } = useStore();

  useEffect(() => {
    mobxStore.start();
    return () => {
      mobxStore.stop();
    };
  }, [mobxStore]);

  if (mobxStore.timeString === undefined || mobxStore.light === undefined)
    return null;
  return (
    <Layout config={config} custom={custom}>
      <div>
        <div>{mobxStore.title}</div>
        <Clock timeString={mobxStore.timeString} light={mobxStore.light} />
        <Navigations />
      </div>
    </Layout>
  );
});

export default MobxSsr;
