import React, { useEffect } from 'react';

import Clock from 'components/sample/mobx/Clock';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';
import MobxStore from 'stores/sample/MobxStore';
import Navigations from 'components/sample/mobx/Navigations';
import { config, custom } from 'config/layoutSetting';
import Layout from 'components/layout/Layout';
import { GetStaticProps } from 'next';

/**
 * SSG init 방식. 빌드시 있는 값으로 고정됨.
 */
export const getStaticProps: GetStaticProps = async () => {
  const mobxStore = new MobxStore();
  mobxStore.light = false;
  mobxStore.lastUpdate = Date.now();
  mobxStore.title =
    'SSG - static site generate. 빌드시 초기화 한 값이 들어갑니다.';

  return { props: { initialStore: JSON.stringify({ mobxStore }) } };
};

/**
 * Mobx 샘플 페이지. ssg
 * @constructor
 */
const MobxSsg = observer(() => {
  const { mobxStore } = useStore();

  useEffect(() => {
    mobxStore.start();
    return () => {
      mobxStore.stop();
    };
  }, [mobxStore]);

  if (mobxStore.timeString === undefined || mobxStore.light === undefined)
    return null;
  return (
    <Layout config={config} custom={custom}>
      <div>
        <div>{mobxStore.title}</div>
        <Clock timeString={mobxStore.timeString} light={mobxStore.light} />
        <Navigations />
      </div>
    </Layout>
  );
});

export default MobxSsg;
