import React, { useEffect } from 'react';

import Clock from 'components/sample/mobx/Clock';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';
import Navigations from 'components/sample/mobx/Navigations';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';

/**
 * Mobx 샘플 페이지. csr init 방식
 * @constructor
 */
const MobxIndex = observer(() => {
  const { mobxStore } = useStore();

  useEffect(() => {
    mobxStore.lastUpdate = Date.now();
    mobxStore.title = `Static html. CSR방식입니다.\r\n 
    렌더링 확인하려면 
    디버깅모드에서 Ctrl+Shift+P desable javascript로 확인 가능합니다. 
    
    CSR이므로 javascript 사용안하면 이 메시지는 안나옵니다.`;
    mobxStore.start();
    return () => {
      mobxStore.stop();
    };
  }, [mobxStore]);

  if (mobxStore.timeString === undefined || mobxStore.light === undefined)
    return null;
  return (
    <Layout config={config} custom={custom}>
      <div>
        <div>{mobxStore.title}</div>
        <Clock timeString={mobxStore.timeString} light={mobxStore.light} />
        <Navigations />
      </div>
    </Layout>
  );
});

export default MobxIndex;
