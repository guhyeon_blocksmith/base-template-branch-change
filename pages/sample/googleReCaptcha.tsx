import React, { useState } from 'react';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { Button } from 'smith-ui-components/button';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import { InputTextarea } from 'smith-ui-components/inputtextarea';
import { PrivateAdminApi } from 'apis/index';
import { RecaptchaResponse } from 'apis/ApiAdmin';

const GoogleReCapCha = () => {
  const { executeRecaptcha } = useGoogleReCaptcha();

  const recaptchaActionName = '/sample/googleReCaptcha';

  const [reCapCahResponse, setReCapCahResponse] = useState<RecaptchaResponse>();
  return (
    <Layout config={config} custom={custom}>
      <>
        <ul>
          <li>
            <Button
              label="테스트"
              onClick={async () => {
                const reCapChatoken = await executeRecaptcha(
                  recaptchaActionName
                );
                if (reCapChatoken) {
                  const response = await PrivateAdminApi.recaptcha.recaptchaUsingPost(
                    { token: reCapChatoken }
                  );
                  setReCapCahResponse(response.data);
                }
              }}
            />
          </li>
          <li>
            <InputTextarea
              value={JSON.stringify(reCapCahResponse)}
              autoResize
            />
          </li>
        </ul>
      </>
    </Layout>
  );
};

export default GoogleReCapCha;
