import React, { FC, useEffect, useState } from 'react';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { ProgressSpinner } from 'smith-ui-components/progressspinner';

const Loading: FC = () => {
  const [showIndicator, setShowIndicator] = useState<boolean>(true);

  useEffect(() => {
    const timer1 = setTimeout(() => setShowIndicator(false), 2000);

    return () => {
      clearTimeout(timer1);
    };
  }, []);
  return (
    <Layout config={config} custom={custom}>
      <h1>Show Indicator [{showIndicator ? 'true' : 'false'}]</h1>
      {!showIndicator && <ProgressSpinner />}
    </Layout>
  );
};

export default Loading;
