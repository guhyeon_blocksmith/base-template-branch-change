import { FC, useEffect } from 'react';
import { GetServerSideProps } from 'next';
import { deleteStatelessSession } from 'libs/AuthUtil';
import Router, { useRouter } from 'next/router';
/**
 *  SSR 방식 초기 데이터 가져오기
 */
export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  deleteStatelessSession(req, res);

  return {
    redirect: {
      permanent: false,
      destination: '/',
    },
  };
};

/**
 * 로그아웃 페이지
 * @constructor
 */
const LogoutPage: FC = () => {
  const router = useRouter();
  const { returnUrl } = router.query;
  useEffect(() => {
    if (returnUrl !== undefined && returnUrl === typeof 'string') {
      Router.replace(returnUrl.toString()).then();
    } else {
      router
        .push({
          pathname: '/auth/login',
          query: { returnUrl: router.pathname },
        })
        .then();
    }
  }, []);

  return null;
};

export default LogoutPage;
