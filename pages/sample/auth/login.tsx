import React, { FC, useEffect, useRef } from 'react';
import Layout from 'components/layout/Layout';
import { InputText } from 'smith-ui-components/inputtext';
import { Button } from 'smith-ui-components/button';
import Link from 'next/link';
import { config, custom } from 'config/layoutSetting';
import useAppendRef from 'libs/hooks/useAppendRef';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';

import styles from './login.module.css';
import { Password } from 'smith-ui-components/password';

const pageConfig: any = {
  ...config,
  showHeader: false,
  showMenu: false,
};

const pageCustom = {
  ...custom,
  useCustomHeader: false,
  useCustomContent: false,
  useCustomFooter: false,
};

/**
 * 로그인 페이지 통합인증
 * @constructor
 */
const LoginPage: FC = observer(() => {
  const inputRef = useRef<InputText>(null);

  const { sampleLoginStore: store } = useStore();

  const [usernameRef] = useAppendRef(store.inputRefs, 'username');
  const [passwordRef] = useAppendRef(store.inputRefs, 'password');

  useEffect(() => {
    inputRef.current?.element?.focus();
  }, []);

  return (
    <Layout config={pageConfig} custom={pageCustom}>
      <>
        <form>
          <div className={styles.wrapper}>
            <div>
              <div className="content-section implementation p-fluid">
                <div className="card">
                  <div className="card">
                    <div className="p-fluid">
                      <div className="p-field p-grid p-justify-end">
                        <div className="p-col-10 p-md-10">
                          <h1>LOGIN</h1>
                        </div>
                      </div>
                      <div className="p-field p-grid">
                        <label
                          htmlFor="firstname4"
                          className="p-col-12 p-mb-2 p-md-2 p-mb-md-0"
                        >
                          ID
                        </label>
                        <div className="p-col-12 p-md-10">
                          <InputText
                            id="ID"
                            ref={usernameRef}
                            value={store.loginItem.loginId}
                            onChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ) => (store.loginItem.loginId = e.target.value)}
                            className={`${
                              store.validErrors.has('username')
                                ? 'p-invalid'
                                : ''
                            }`}
                          />
                          {store.validErrors
                            .get('username')
                            ?.map((message: string) => (
                              <small
                                key={message}
                                className="p-invalid p-d-block"
                              >
                                {message}
                              </small>
                            ))}
                        </div>
                      </div>
                      <div className="p-field p-grid">
                        <label
                          htmlFor="password"
                          className="p-col-12 p-mb-2 p-md-2 p-mb-md-0"
                        >
                          Password
                        </label>
                        <div className="p-col-12 p-md-10">
                          <Password
                            ref={passwordRef}
                            id="password"
                            feedback={false}
                            value={store.loginItem.password}
                            onChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ) => (store.loginItem.password = e.target.value)}
                            className={`${
                              store.validErrors.has('password')
                                ? 'p-invalid'
                                : ''
                            }`}
                          />
                          {store.validErrors
                            .get('password')
                            ?.map((message: string) => (
                              <small
                                key={message}
                                className="p-invalid p-d-block"
                              >
                                {message}
                              </small>
                            ))}
                        </div>
                      </div>
                      <div className="p-field p-grid p-justify-end">
                        <div className="p-col-10 p-md-10">
                          <Button
                            type="button"
                            label="LOGIN"
                            onClick={() => store.submitLogin('/')}
                          />
                        </div>
                      </div>
                      <div className="p-field p-grid p-justify-end">
                        <div className="p-col-3 p-md-3">
                          <Link href={'/sample/member/join'}>
                            <a>회원가입</a>
                          </Link>
                          <Link href={'/'}>
                            <a>dashboard</a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </>
    </Layout>
  );
});

export default LoginPage;
