import React, { FC, useState } from 'react';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { Button } from 'smith-ui-components/button';
import RangeSlider from 'components/common/rangeSlider/RangeSlider';

const DoubleRangePages: FC = () => {
  const sliderStyle = {
    position: 'relative',
    width: '100%',
  };
  const [range, setRange] = useState<number[]>([450, 600]);
  const [, setUpdate] = useState<number[]>(range.slice());
  const [domain] = useState<number[]>([1, 1200]);
  const [reversed, setReversed] = useState<boolean>(false);
  const [disabled, setDisabled] = useState<boolean>(false);

  const onUpdate = (update: any) => {
    setUpdate(update);
  };

  const onChange = (values: any) => {
    setRange(values);
  };

  const toggleReverse = () => {
    setReversed(!reversed);
  };

  return (
    <Layout config={config} custom={custom}>
      <div style={{ height: 450, width: '100%' }}>
        <Button
          onClick={() => {
            onChange([200, 300]);
            onUpdate([200, 300]);
          }}
          style={{ marginRight: '20px' }}
        >
          SET VALUES [200, 300]
        </Button>
        <Button
          onClick={() => {
            onChange([350, 450]);
            onUpdate([350, 450]);
          }}
          style={{ marginRight: '20px' }}
        >
          SET VALUES [350, 450]
        </Button>
        <Button onClick={() => toggleReverse()} style={{ marginRight: '20px' }}>
          {reversed ? 'DISPLAY LOW TO HIGH' : 'DISPLAY HIGH TO LOW'}
        </Button>
        <Button
          onClick={() => setDisabled(!disabled)}
          style={{ marginRight: '20px' }}
        >
          {disabled ? 'RELEASE' : 'DISABLE'}
        </Button>
        <br />
        <br />
        <table className="defTable">
          <colgroup>
            <col style={{ width: '180px' }} />
            <col style={{ width: 'calc(50% - 120px)' }} />
          </colgroup>
          <tbody>
            <tr>
              <th>시작/최종 값</th>
              <td>
                {domain[0]}/{domain[1]}
              </td>
            </tr>
            <tr>
              <th>Start Point</th>
              <td>{range[0]}</td>
            </tr>
            <tr>
              <th>End Point</th>
              <td>{range[1]}</td>
            </tr>
          </tbody>
        </table>
        <RangeSlider
          sliderStyle={sliderStyle}
          domain={domain}
          value={range}
          disabled={disabled}
          callback={(res: number[]) => {
            setRange(res);
          }}
          reversed={reversed}
        />
      </div>
    </Layout>
  );
};

export default DoubleRangePages;
