import React, { FC, useEffect, useRef, useState } from 'react';
import Cropper, { ReactCropperElement } from 'react-cropper';
import { Button } from 'smith-ui-components/button';
import styles from './imageEditor.module.css';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
/**
 * 이미지 수정 샘플소스
 * ####### 참조 사이트 #######
 * http://jsfiddle.net/franknight/kvzz0LzL/
 * https://fengyuanchen.github.io/cropperjs/
 * @constructor
 */
export const ImageEditor: FC = () => {
  const defaultSrc = 'https://picsum.photos/1200/900';
  const [image, setImage] = useState(defaultSrc);
  const [files, setFiles] = useState<any>();
  const [cropData, setCropData] = useState('#');
  const [cropper, setCropper] = useState<any>();
  // const [fileRef, setFileRef] = useState<any>();
  const fileRef = useRef<HTMLInputElement>(null);
  /**
   * CropperRef 값
   * */
  const cropperRef = useRef<ReactCropperElement>(null);

  useEffect(() => {
    if (files === undefined) return;

    const reader = new FileReader();
    reader.onload = () => {
      setImage(reader.result as any);
    };

    if (files.length === 1) {
      reader.readAsDataURL(files[0]);
    }
  }, [files]);

  const onChange = (e: any) => {
    e.preventDefault();

    if (e.dataTransfer) {
      setFiles(e.dataTransfer.files);
    } else if (e.target) {
      setFiles(e.target.files);
    }
  };

  const getCropData = () => {
    if (typeof cropper !== 'undefined') {
      setCropData(cropper.getCroppedCanvas().toDataURL());
    }
  };

  return (
    <Layout config={config} custom={custom}>
      <div>
        <div style={{ width: '100%' }}>
          <input ref={fileRef} type="file" onChange={onChange} />
          <Button
            onClick={() => {
              if (fileRef.current) {
                // fileRef.current.files = null;
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                fileRef.current.value = null;
              }
              setImage(defaultSrc);
            }}
          >
            Use default img
          </Button>
          <br />
          <br />
          <div>
            <Button
              icon={'pi pi-refresh'}
              // label={'+45'}
              onClick={() => {
                cropperRef.current?.cropper.rotate(45);
              }}
            />
            <Button
              icon={'pi pi-replay'}
              // label={'-45'}
              onClick={() => {
                cropperRef.current?.cropper.rotate(-45);
              }}
            />
            <Button
              // icon={'pi pi-pencil'}
              icon={'pi pi-search-plus'}
              // label={'+0.1'}
              onClick={() => {
                cropperRef.current?.cropper.zoom(0.1);
              }}
            />
            <Button
              icon={'pi pi-search-minus'}
              // label={'-0.1'}
              onClick={() => {
                cropperRef.current?.cropper.zoom(-0.1);
              }}
            />
            <Button
              icon={'pi pi-arrow-left'}
              // label={'-0.1'}
              onClick={() => {
                cropperRef.current?.cropper.move(-10, 0);
              }}
            />
            <Button
              icon={'pi pi-arrow-right'}
              // label={'-0.1'}
              onClick={() => {
                cropperRef.current?.cropper.move(10, 0);
              }}
            />
            <Button
              icon={'pi pi-arrow-up'}
              // label={'-0.1'}
              onClick={() => {
                cropperRef.current?.cropper.move(0, 10);
              }}
            />
            <Button
              icon={'pi pi-arrow-down'}
              // label={'-0.1'}
              onClick={() => {
                cropperRef.current?.cropper.move(0, -10);
              }}
            />
            <Button
              icon={'pi pi-refresh'}
              // label={'-0.1'}
              onClick={() => {
                cropperRef.current?.cropper.reset();
              }}
            />
          </div>
          <br />
          <br />
          <Cropper
            style={{ height: 400, width: '100%' }}
            ref={cropperRef}
            zoomTo={1}
            initialAspectRatio={1}
            preview=".img-preview"
            src={image}
            viewMode={1}
            guides={true}
            minCropBoxHeight={10}
            minCropBoxWidth={10}
            background={false}
            responsive={true}
            autoCropArea={1}
            checkOrientation={false} // https://github.com/fengyuanchen/cropperjs/issues/671
            onInitialized={(instance) => {
              setCropper(instance);
            }}
          />
        </div>
        <div>
          <div className={styles.box} style={{ width: '50%', float: 'right' }}>
            <h1>Preview</h1>
            <div
              className="img-preview"
              style={{ width: '100%', float: 'left', height: '300px' }}
            />
          </div>
          <div
            className={styles.box}
            style={{ width: '50%', float: 'right', height: '300px' }}
          >
            <h1>
              <span>Crop</span>
              <Button
                icon={'pi pi-check'}
                label={' Crop Image'}
                style={{ float: 'right' }}
                onClick={getCropData}
              />
              <Button
                icon={'pi pi-times'}
                label={' Crop off'}
                style={{ float: 'right' }}
                onClick={() => {
                  cropperRef.current?.cropper.clear();
                }}
              />
            </h1>
            <div>
              <img
                style={{ width: 'auto', height: '100%' }}
                src={cropData}
                alt="cropped"
              />
            </div>
          </div>
        </div>
        <br style={{ clear: 'both' }} />
      </div>
    </Layout>
  );
};
export default ImageEditor;
