import React from 'react';
import Layout from 'components/layout/Layout';
import { config as layoutConfig, custom } from 'config/layoutSetting';

const VideoController = () => {
  return (
    <Layout config={layoutConfig} custom={custom}>
      스토리북에 VideoPlayer 참고
    </Layout>
  );
};

export default VideoController;
