import React, { useState } from 'react';
import Layout from 'components/layout/Layout';
import { config as layoutConfig, custom } from 'config/layoutSetting';
import SmithPlayer from 'smith-ivs';
import { StreamType } from 'smith-ivs/dist/types';
import dummy from 'components/survey/dummy.json';
import VideoTracking from 'components/video/VideoTracking';

const Tracking = () => {
  const init: StreamType = {
    url: dummy[0].egressEndpoints.DASH!,
    metaInfo: dummy[0]!,
    pip: false,
    playing: true,
    light: false,
    volume: 0.8,
    muted: false,
    played: 0,
    loaded: 0,
    duration: 0,
    playbackRate: 1.0,
    loop: false,
    fullscreen: false,
    playerSeeking: 0,
    seeking: false,
    playedSeconds: 0,
    loadedSeconds: 0,
  };

  const [config, setConfig] = useState<StreamType>(init);
  const [watchAll, setWatchAll] = useState<boolean>(false);
  const { duration, playedSeconds, metaInfo } = config;
  const { guid } = metaInfo;

  return (
    <Layout config={layoutConfig} custom={custom}>
      <SmithPlayer
        callback={(res: StreamType) => setConfig({ ...config, ...res })}
        playSpec={config}
      />
      <VideoTracking
        duration={duration!}
        playedSeconds={playedSeconds!}
        guid={guid}
        userId={'anonymous'}
        callback={(res: boolean) => setWatchAll(res)}
        showProgress={true}
      />
      <p>
        Message:{' '}
        {watchAll ? (
          <b style={{ color: 'red' }}>Completely Watched</b>
        ) : (
          <b>Not Completely Watched</b>
        )}
      </p>
    </Layout>
  );
};

export default Tracking;
