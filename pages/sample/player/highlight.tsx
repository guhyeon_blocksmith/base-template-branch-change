import React, { useState } from 'react';
import Layout from 'components/layout/Layout';
import { config as layoutConfig, custom } from 'config/layoutSetting';
import SmithPlayer from 'smith-ivs';
import { StreamType } from 'smith-ivs/dist/types';
import RangeSlider from 'components/common/rangeSlider/RangeSlider';

const HighlightOnTrack = () => {
  const init: StreamType = {
    url:
      'https://d1dopqicmcx111.cloudfront.net/out/v1/4a32dcd39bed4333962e0d1b5f5d1c7a/7c033e24f8c54d13833bd902b9d22c33/c916a709ea62459abe2b7b4fb25b9b86/index.mpd',
    metaInfo: { guid: 'sample-local-video-id-range-picker' },
    played: 0,
    playing: true,
    controls: true,
    playedSeconds: 0,
    volume: 0.8,
    duration: 0,
  };

  const [config, setConfig] = useState<StreamType>(init);
  const { duration } = config;
  const flooredDuration = Math.floor(duration!);
  const [range, setRange] = useState<number[]>([5, 10]);
  const domain = [0, flooredDuration];

  return (
    <Layout config={layoutConfig} custom={custom}>
      <SmithPlayer
        callback={(res: StreamType) =>
          setConfig({ ...config, ...res, playing: true })
        }
        playSpec={config}
      />
      <div style={{ height: '150px' }}>
        <div style={{ margin: '50px' }}>
          <h3>구간 하일라이트 지정</h3>
        </div>
        <RangeSlider
          sliderStyle={{
            position: 'relative',
            width: '100%',
          }}
          domain={domain}
          value={range}
          callback={(res: number[]) => {
            setRange(res);
          }}
        />
      </div>
      <table className="defTable">
        <colgroup>
          <col style={{ width: '180px' }} />
          <col style={{ width: 'calc(50% - 120px)' }} />
        </colgroup>
        <tbody>
          <tr>
            <th>시작</th>
            <td>{range[0]}</td>
          </tr>
          <tr>
            <th>끝</th>
            <td>{range[1]}</td>
          </tr>
        </tbody>
      </table>
    </Layout>
  );
};

export default HighlightOnTrack;
