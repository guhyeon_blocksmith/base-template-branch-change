import React, { useState } from 'react';
import Layout from 'components/layout/Layout';
import PlayerMetaInfo from 'components/sample/streamPlayer/PlayerMetaInfo';
import PlayerList from 'components/sample/streamPlayer/PlayerList';
import PlayerNavigator from 'components/sample/streamPlayer/PlayerNavigator';
import { useStore } from 'libs/hooks/useStore';
import PlayerUpload from 'components/sample/streamPlayer/PlayerUpload';
import styles from 'components/sample/TotalUiComponents/ButtonComponents.module.scss';
import { Card } from 'smith-ui-components/card';
import { StreamType } from 'smith-ivs/dist/types';
import { openLayoutMode } from 'types/enum';

const Vod = () => {
  const { videoListStore: store } = useStore();
  const [tabName, setTabName] = useState<string>('List');
  const model = [
    { label: 'List', icon: 'pi pi-fw pi-video' },
    { label: 'MetaInfo', icon: 'pi pi-fw pi-flag' },
    { label: 'Controls', icon: 'pi pi-fw pi-cog' },
    { label: 'Upload', icon: 'pi pi-fw pi-upload' },
  ];
  const tabMenuItems = {
    activeItem: model[model.findIndex((row: any) => row.label === tabName)],
    model,
  };

  const init: StreamType = {
    url: '',
    metaInfo: { guid: '' },
    played: 0,
    playing: false,
    controls: false,
    playedSeconds: 0,
    volume: 0.8,
    duration: 0,
  };

  const [config, setConfig] = useState<StreamType>(init);
  const [isShowSurvey, setIsShowSurvey] = useState<boolean>(true);

  const { metaInfo, controls } = config;
  const { guid } = metaInfo;

  const onUpdate = (update: any) => setConfig({ ...config, ...update });

  return (
    <Layout
      tabMenuItems={tabMenuItems}
      tabMenuCallback={(tabName: string) => {
        setTabName(tabName);
      }}
      //
      layoutAuth={openLayoutMode.private}
    >
      <Card
        title="Current Video ID"
        style={{ width: '100%', marginBottom: '2em' }}
      >
        {guid}
      </Card>

      {tabName === 'List' && (
        <>
          <Card
            title="Video List"
            style={{ width: '100%', marginBottom: '2em' }}
          >
            {store && (
              <PlayerList onUpdate={onUpdate} setTabName={setTabName} />
            )}
          </Card>
        </>
      )}
      {tabName === 'Controls' && (
        <Card title="Controls" style={{ width: '100%', marginBottom: '2em' }}>
          <div className={`card ${styles.buttonDemo}`}>
            <PlayerNavigator
              config={config}
              setConfig={setConfig}
              onUpdate={onUpdate}
              controls={controls!}
            />
            <p>smith-ivs 0.2.1</p>
          </div>
        </Card>
      )}
      {tabName === 'MetaInfo' && (
        <>
          <Card title="MetaInfo" style={{ width: '100%', marginBottom: '2em' }}>
            <PlayerMetaInfo
              onUpdate={onUpdate}
              config={config}
              isShowSurvey={isShowSurvey}
              setIsShowSurvey={setIsShowSurvey}
            />
          </Card>
        </>
      )}
      {tabName === 'Upload' && (
        <>
          <Card
            title="Video Upload"
            style={{ width: '100%', marginBottom: '2em' }}
          >
            <PlayerUpload videoId={init.metaInfo.guid} />
          </Card>
        </>
      )}
    </Layout>
  );
};

export default Vod;
