import React, { useRef, useState } from 'react';
import Layout from 'components/layout/Layout';
import { config as layoutConfig, custom } from 'config/layoutSetting';
import YouTubeValidator, {
  CallbackType,
  RootObject,
  UrlType,
} from 'components/video/YouTubeValidator';
import { Button } from 'smith-ui-components/button';
import { v4 } from 'uuid';

/**
 * youtube validator
 * @constructor
 * todo YouTubeValidator move to smith-ivs
 */
const YoutubeValidatorComponent = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [parseInfo, setYoutubeUrl] = useState<UrlType | null>();
  const [isValid, setIsValid] = useState<boolean>(false);
  const [status, setStatus] = useState<string>('READY');
  const [videoUrl, setVideoUrl] = useState<string>('');
  const [snippetInfo, setSnippetInfo] = useState<RootObject | null>();

  const handleClick = () => {
    inputRef.current && setVideoUrl(inputRef.current.value);
  };

  const rendererValidator = () => {
    if (isValid) {
      return (
        <>
          <h4>Youtube Url Parse</h4>
          <table className="defTable">
            <colgroup>
              <col style={{ width: '180px' }} />
              <col style={{ width: 'calc(50% - 120px)' }} />
            </colgroup>
            <tbody>
              <tr>
                <th>Pasted Url</th>
                <td>{videoUrl}</td>
              </tr>

              <tr>
                <th>Protocol</th>
                <td>{parseInfo?.protocol}</td>
              </tr>

              <tr>
                <th>Hostname</th>
                <td>{parseInfo?.hostname}</td>
              </tr>

              <tr>
                <th>Pathname</th>
                <td>{parseInfo?.pathname}</td>
              </tr>

              <tr>
                <th>Port</th>
                <td>{parseInfo?.port}</td>
              </tr>

              <tr>
                <th>Search</th>
                <td>{parseInfo?.search}</td>
              </tr>

              <tr>
                <th>Hash</th>
                <td>{parseInfo?.hash}</td>
              </tr>

              <tr>
                <th>Username</th>
                <td>{parseInfo?.username}</td>
              </tr>

              <tr>
                <th>Password</th>
                <td>{parseInfo?.password}</td>
              </tr>
              <tr>
                <th>Search Params</th>
                <td>{parseInfo?.searchParams.entries()}</td>
              </tr>

              <tr>
                <th>Origin</th>
                <td>{parseInfo?.origin}</td>
              </tr>
            </tbody>
          </table>
        </>
      );
    }
  };

  const rendererSnippets = () => {
    if (isValid && snippetInfo) {
      const { items } = snippetInfo;
      const { snippet } = items[0];
      const {
        categoryId,
        channelId,
        channelTitle,
        description,
        title,
        tags,
      } = snippet;
      return (
        <>
          <h4>Media Snippets</h4>
          <table className="defTable">
            <colgroup>
              <col style={{ width: '180px' }} />
              <col style={{ width: 'calc(50% - 120px)' }} />
            </colgroup>
            <tbody>
              <tr>
                <th>channelTitle</th>
                <td>{channelTitle} </td>
              </tr>
              <tr>
                <th>title</th>
                <td>{title} </td>
              </tr>
              <tr>
                <th>categoryId</th>
                <td>{categoryId} </td>
              </tr>
              <tr>
                <th>channelId</th>
                <td>{channelId} </td>
              </tr>
              <tr>
                <th>description</th>
                <td>{description} </td>
              </tr>
              <tr>
                <th>tags</th>
                <td>
                  {tags &&
                    tags.map((res) => {
                      return <div key={v4()}>#{res}&nbsp;</div>;
                    })}{' '}
                </td>
              </tr>
            </tbody>
          </table>
        </>
      );
    }
  };

  const rendererThumbnails = () => {
    if (isValid) {
      return (
        <>
          <h4>Thumbnails</h4>
          <table className="defTable">
            <colgroup>
              <col style={{ width: '180px' }} />
              <col style={{ width: 'calc(50% - 120px)' }} />
            </colgroup>
            <tbody>
              <tr>
                <th>default</th>
                <td>
                  {snippetInfo && (
                    <img
                      src={snippetInfo.items[0].snippet.thumbnails.default.url}
                      alt={''}
                    />
                  )}
                </td>
              </tr>
              <tr>
                <th>high</th>
                <td>
                  {snippetInfo && (
                    <img
                      src={snippetInfo.items[0].snippet.thumbnails.high.url}
                      alt={''}
                    />
                  )}
                </td>
              </tr>
            </tbody>
          </table>
        </>
      );
    }
  };

  const rendererStatics = () => {
    if (isValid && snippetInfo) {
      const { items } = snippetInfo;
      const { statistics } = items[0];
      const {
        commentCount,
        dislikeCount,
        favoriteCount,
        likeCount,
        viewCount,
      } = statistics;
      return (
        <>
          <h4>Statistics</h4>
          <table className="defTable">
            <colgroup>
              <col style={{ width: '180px' }} />
              <col style={{ width: 'calc(50% - 120px)' }} />
            </colgroup>
            <tbody>
              <tr>
                <th>comment</th>
                <td>{commentCount}</td>
              </tr>
              <tr>
                <th>like</th>
                <td>{likeCount}</td>
              </tr>
              <tr>
                <th>dislike</th>
                <td>{dislikeCount}</td>
              </tr>
              <tr>
                <th>favorite</th>
                <td>{favoriteCount}</td>
              </tr>
              <tr>
                <th>view</th>
                <td>{viewCount}</td>
              </tr>
            </tbody>
          </table>
        </>
      );
    }
  };

  return (
    <Layout config={layoutConfig} custom={custom}>
      <strong className="contTitle">Youtube Validator</strong>
      <div style={{ textAlign: 'center', height: '120px' }}>
        <br />
        <h1>{status}</h1>
      </div>
      <YouTubeValidator
        videoUrl={videoUrl}
        mediaConfig={{
          apiKey: 'AIzaSyCiJ20U5MiaGB8n_DArvcZ8Lpb3plKShxQ',
          showVideo: false,
        }}
        callback={(res: CallbackType) => {
          setStatus(res.status);
          if (res.isValid) {
            setIsValid(res.isValid);
            setYoutubeUrl(res.urlParse);
            setSnippetInfo(res.snippet);
          } else {
            setIsValid(false);
            setYoutubeUrl(null);
            setSnippetInfo(null);
          }
        }}
      >
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-3">동영상 URL</div>
          <div className="p-col-12 p-md-9">
            <div className="p-inputgroup">
              <input
                placeholder="Youtube URL"
                id={'videoURL'}
                className="p-inputtext p-component"
                type="text"
                ref={inputRef}
              />
              <Button
                label="확인"
                onClick={() => {
                  handleClick();
                }}
              />
              <div style={{ width: '20px' }} />
              <Button
                label="초기화"
                onClick={() => {
                  setIsValid(false);
                  setYoutubeUrl(null);
                  setSnippetInfo(null);
                  setStatus('READY');
                }}
              />
            </div>
          </div>
        </div>
      </YouTubeValidator>
      {rendererStatics()}
      {rendererThumbnails()}
      {rendererSnippets()}
      {rendererValidator()}
    </Layout>
  );
};

export default YoutubeValidatorComponent;
