import React, { useState } from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { Card } from 'smith-ui-components/card';

const TabMenu = () => {
  const [tabName, setTabName] = useState<string>('Home');
  const tabMenuItems = {
    model: [
      { label: 'Home', icon: 'pi pi-fw pi-home' },
      { label: 'Calendar', icon: 'pi pi-fw pi-calendar' },
      { label: 'Edit', icon: 'pi pi-fw pi-pencil' },
      { label: 'Documentation', icon: 'pi pi-fw pi-file' },
      { label: 'Settings', icon: 'pi pi-fw pi-cog' },
    ],
  };

  return (
    <Layout
      config={config}
      custom={custom}
      tabMenuItems={tabMenuItems}
      tabMenuCallback={(tabNo: string) => setTabName(tabNo)}
    >
      <>
        <h1>Tab Menu Sample</h1>
        {tabName === 'Home' && (
          <Card title="Home" style={{ width: '25rem', marginBottom: '2em' }}>
            <p className="p-m-0" style={{ lineHeight: '1.5' }}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Inventore sed consequuntur error repudiandae numquam deserunt
              quisquam repellat libero asperiores earum nam nobis, culpa ratione
              quam perferendis esse, cupiditate neque quas!
            </p>
          </Card>
        )}
        {tabName === 'Calendar' && (
          <Card
            title="Calendar"
            style={{ width: '25rem', marginBottom: '2em', color: 'blue' }}
          >
            <p className="p-m-0" style={{ lineHeight: '1.5' }}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Inventore sed consequuntur error repudiandae numquam deserunt
              quisquam repellat libero asperiores earum nam nobis, culpa ratione
              quam perferendis esse, cupiditate neque quas!
            </p>
          </Card>
        )}{' '}
        {tabName === 'Edit' && (
          <Card
            title="Edit"
            style={{ width: '25rem', marginBottom: '2em', color: 'gray' }}
          >
            <p className="p-m-0" style={{ lineHeight: '1.5' }}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Inventore sed consequuntur error repudiandae numquam deserunt
              quisquam repellat libero asperiores earum nam nobis, culpa ratione
              quam perferendis esse, cupiditate neque quas!
            </p>
          </Card>
        )}{' '}
        {tabName === 'Documentation' && (
          <Card
            title="Documentation"
            style={{ width: '25rem', marginBottom: '2em', color: 'green' }}
          >
            <p className="p-m-0" style={{ lineHeight: '1.5' }}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Inventore sed consequuntur error repudiandae numquam deserunt
              quisquam repellat libero asperiores earum nam nobis, culpa ratione
              quam perferendis esse, cupiditate neque quas!
            </p>
          </Card>
        )}{' '}
        {tabName === 'Settings' && (
          <Card
            title="Settings"
            style={{ width: '25rem', marginBottom: '2em', color: 'red' }}
          >
            <p className="p-m-0" style={{ lineHeight: '1.5' }}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Inventore sed consequuntur error repudiandae numquam deserunt
              quisquam repellat libero asperiores earum nam nobis, culpa ratione
              quam perferendis esse, cupiditate neque quas!
            </p>
          </Card>
        )}
      </>
    </Layout>
  );
};

export default TabMenu;
