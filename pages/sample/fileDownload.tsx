import React, { FC, useEffect, useState } from 'react';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { Button } from 'smith-ui-components/button';
import { InputText } from 'smith-ui-components/inputtext';
import { download, fetchBlob } from 'libs/common';

const FileDownload: FC = () => {
  const [imageName, setImageName] = useState<string>('default.jpg');
  const [url, setUrl] = useState<string>(
    'https://i.ytimg.com/vi/WvSZta4zsWg/default.jpg'
  );

  useEffect(() => {
    const parts = decodeURIComponent(url).split('/');

    setImageName(parts[parts.length - 1]);
  }, [url]);

  const [blob, setBlob] = useState<Blob>();

  return (
    <Layout config={config} custom={custom}>
      url:
      <InputText value={url} onChange={(e) => setUrl(e.target.value)} />
      <Button
        label="BLOB Load"
        onClick={async () => {
          const blob = await fetchBlob(url);
          setBlob(blob);
          //
          // //// 파일 업로드시 참고
          // const f: File = new File([blob], 'you.png');
          // const response = await PrivateAdminApi.file.uploadAttachFileUsingPost(
          //   {
          //     regId: 0,
          //     subPath: 'youtube',
          //   },
          //   { file: f }
          // );
          // console.warn(response);
        }}
      />
      <Button
        label={'url 다운로드'}
        onClick={() => {
          download(url, imageName);
        }}
      />
      {blob && (
        <a download={imageName} href={window.URL.createObjectURL(blob)}>
          download
        </a>
      )}
      <div>
        <h5>sampleUrls</h5>
        <ul>
          <li>
            https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/sample_do_not_delete/%ED%98%B8%EB%9E%91%EC%9D%B4.jpg
          </li>
        </ul>
      </div>
    </Layout>
  );
};

export default FileDownload;
