import React, { FC, useState } from 'react';
import { ImageItem, ImageUploader } from 'components/bizCommon/ImageUploader';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { Panel } from 'smith-ui-components/panel';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';

const ImageUpload: FC = observer(() => {
  const { commonStore } = useStore();
  const [imagesMulti, setImagesMulti] = useState<ImageItem[]>([
    {
      src:
        'https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/temp//20210422/24ca6b03-ffbd-43e7-a700-d5e1a55357c2.jpg',
      alt: 'asdf',
      fileId: 1,
    },
  ]);
  const [images, setImages] = useState<ImageItem[]>([
    {
      src:
        'https://cuspcdn.s3.ap-northeast-2.amazonaws.com/wewill/dev/upload/images/o/temp//20210422/24ca6b03-ffbd-43e7-a700-d5e1a55357c2.jpg',
      alt: 'asdf',
      fileId: 2,
    },
  ]);
  return (
    <Layout config={config} custom={custom}>
      <Panel header={'이미지 멀티 업로드'}>
        <ImageUploader
          imgArray={imagesMulti}
          subPath={'temp/'}
          isMulti
          onChange={(imgSrcArray) => {
            setImagesMulti(imgSrcArray);
            console.log('-> imgSrcArray', imgSrcArray);
          }}
          regId={commonStore.userInfo?.id}
        />
      </Panel>

      <Panel header={'이미지 단일 업로드'}>
        <ImageUploader
          imgArray={images}
          subPath={'temp/'}
          onChange={(imgSrcArray) => {
            setImages(imgSrcArray);
          }}
          regId={commonStore.userInfo?.id}
        />
      </Panel>
    </Layout>
  );
});

export default ImageUpload;
