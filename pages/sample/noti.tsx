import React from 'react';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';

const Noti = () => {
  const handleNotiConfirm = () => {
    Notification.requestPermission().then();
  };
  const handleNoti = () => {
    new Notification('알림', { body: '테스트알림입니다.' });
  };
  return (
    <Layout config={config} custom={custom}>
      <ul>
        <li>
          <button
            onClick={handleNotiConfirm}
            style={{ border: '1px solid #000', padding: '5px' }}
          >
            브라우저 알림 허용
          </button>
        </li>
        <li style={{ marginTop: '20px' }}>
          <button
            onClick={handleNoti}
            style={{ border: '1px solid #000', padding: '5px' }}
          >
            브라우저 알림
          </button>
        </li>
      </ul>
    </Layout>
  );
};

export default Noti;
