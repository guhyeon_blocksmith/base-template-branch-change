import React, { FC, useEffect } from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { useStore } from 'libs/hooks/useStore';
import { useRouter } from 'next/router';
import BoardEditForm from 'components/sample/board/BoardEditForm';

/**
 * 게시판 수정 페이지
 * @constructor
 */
const BoardEdit: FC = () => {
  // CSR 방식 초기 데이터 가져오기
  const { boardEditStore: store } = useStore();
  const router = useRouter();

  useEffect(() => {
    if (store === undefined || router.query.id === undefined) return;
    const { id } = router.query;
    store.init(id as string).then();
  }, [router.query, store]);

  return (
    <Layout config={config} custom={custom}>
      <BoardEditForm />
    </Layout>
  );
};

export default BoardEdit;
