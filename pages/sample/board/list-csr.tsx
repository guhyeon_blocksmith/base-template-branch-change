import { observer } from 'mobx-react';
import React, { useEffect } from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { useStore } from 'libs/hooks/useStore';
import Axios from 'axios';
import BoardTable from 'components/sample/board/BoardTable';
import BoardListSearch from 'components/sample/board/BoardListSearch';

/**
 * 게시판 목록. CSR 호출
 * @constructor
 */
const BoardListCSR = observer(() => {
  const { boardListStore: store, showLoading } = useStore();
  useEffect(() => {
    showLoading(true);
    const fetchData = async () => {
      // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
      const response = await Axios.get(
        `${process.env.NEXT_PUBLIC_APP_FAKE_API_BASE_URL}/board?_start=${store.firstIndex}&_limit=${store.rowCount}&q=${store.searchKeyword}&_sort=createdAt&_order=desc`
      );

      store.bindList(response.data, Number(response.headers['x-total-count']));
    };

    fetchData().then();
  }, [store, showLoading]);
  return (
    <Layout config={config} custom={custom}>
      <BoardListSearch />
      <BoardTable />
    </Layout>
  );
});

export default BoardListCSR;
