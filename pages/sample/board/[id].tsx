import React, { FC } from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { GetServerSideProps } from 'next';
import BoardDetailStore from 'stores/sample/board/BoardDetailStore';
import { useStore } from 'libs/hooks/useStore';
import { observer } from 'mobx-react';
import { Panel } from 'smith-ui-components/panel';
import FileList from 'components/common/FileList';
import { toJS } from 'mobx';
import { Button } from 'smith-ui-components/button';
import { confirm } from 'smith-ui-components/dialog';
import Link from 'next/link';
import { windowClose } from 'libs/common';

/**
 * SSR 방식 초기 데이터 가져오기
 */
export const getServerSideProps: GetServerSideProps = async (context) => {
  const boardDetailStore = new BoardDetailStore();
  try {
    await boardDetailStore.loadItem(context.query.id as string);
  } catch (e) {
    // ssr 404
    return {
      notFound: true,
    };
  }

  return { props: { initialStore: JSON.stringify({ boardDetailStore }) } };
};

/**
 * 게시판 상세보기 (수정페이지 이동, 삭제기능)
 * @constructor
 */
const BoardDetail: FC = observer(() => {
  //// CSR 방식 초기 데이터 가져오기
  // const { boardDetailStore: store } = useStore();
  // const router = useRouter();
  // useEffect(() => {
  //   const init = async () => {
  //     await store.loadItem(router.query.id as string);
  //   };
  //   init().then();
  // }, [router]);

  const { boardDetailStore: store } = useStore();
  if (store.item === undefined) return null;
  const { title, content, id, createdAt } = store.item;

  return (
    <Layout config={config} custom={custom}>
      <>
        <Panel header={`${id}-${title}`}>
          <p dangerouslySetInnerHTML={{ __html: content || '' }} />
          <p>{new Date(createdAt!).toLocaleString()}</p>
        </Panel>
        <FileList
          deletable={false}
          attachedFiles={toJS(store.item.attachedFiles)}
        />
        <Button onClick={windowClose}>셀프팝업닫기</Button>
        <Button
          label="지우기"
          onClick={async () => {
            if (id) {
              const result = await confirm(`${title}를 지우시겠습니까?`);
              if (result !== 'OK') return;
              store.delete(id).then();
            }
          }}
        />
        <Link href={`/sample/board/edit/${id}`}>
          <Button label="수정하기" />
        </Link>
      </>
    </Layout>
  );
});

export default BoardDetail;
