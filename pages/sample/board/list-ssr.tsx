import { observer } from 'mobx-react';
import React from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { GetServerSideProps } from 'next';
import Axios from 'axios';
import { withAuthTokenSSR } from 'libs/AuthUtil';
import BoardListStore from 'stores/sample/board/BoardListStore';
import BoardTable from 'components/sample/board/BoardTable';
import BoardListSearch from 'components/sample/board/BoardListSearch';

/**
 *  SSR 방식 초기 데이터 가져오기
 */
export const getServerSideProps: GetServerSideProps = withAuthTokenSSR(
  async ({ req }) => {
    try {
      const boardListStore = new BoardListStore();
      // 토큰을 불러옵니다.

      if (req.accessToken === undefined) {
        throw new Error('no access-token');
      }

      // 맨 처음 첫 페이지에 렌더링 될 데이터를 불러옵니다.
      // backendApi 연동시 apis/index.tsx에 있는 알맞은 api객체를 사용해주세요
      const response = await Axios.get(
        `${process.env.NEXT_PUBLIC_APP_FAKE_API_BASE_URL}/board?_start=${boardListStore.firstIndex}&_limit=${boardListStore.rowCount}&q=${boardListStore.searchKeyword}&_sort=createdAt&_order=desc`,
        {
          headers: { Authorization: 'Bearer ' + req.accessToken },
        }
      );

      boardListStore.bindList(
        response.data,
        Number(response.headers['x-total-count'])
      );
      return { props: { initialStore: JSON.stringify({ boardListStore }) } };
    } catch (err) {
      return {
        redirect: {
          permanent: false,
          destination: '/auth/login',
        },
      };
    }
  }
);

/**
 * 게시판 목록.  SSR
 * @constructor
 */
const BoardListSSR = observer(() => {
  return (
    <Layout config={config} custom={custom}>
      <BoardListSearch />
      <BoardTable />
    </Layout>
  );
});

export default BoardListSSR;
