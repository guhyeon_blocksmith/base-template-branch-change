import React, { FC, useEffect } from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { useStore } from 'libs/hooks/useStore';
import BoardNewForm from 'components/sample/board/BoardNewForm';

/**
 * 게시판 만들기 페이지
 * @constructor
 */
const BoardNew: FC = () => {
  const { boardNewStore: store } = useStore();

  useEffect(() => {
    store.init();
  }, [store]);

  return (
    <Layout config={config} custom={custom}>
      <BoardNewForm />
    </Layout>
  );
};

export default BoardNew;
