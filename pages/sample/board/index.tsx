import React from 'react';
import Link from 'next/link';
import Layout from 'components/layout/Layout/Layout';
import { config, custom } from 'config/layoutSetting';

/**
 * 게시판 목록 페이지.
 * @constructor
 */
const BoardIndex = () => {
  return (
    <Layout config={config} custom={custom}>
      <ul>
        <li>
          <Link href={'/sample/board/list-csr'}>
            {` CSR List - 목록이 없는 html 렌더링 후 브라우저에서 목록을 가져옵니다.`}
          </Link>
        </li>
        <li>
          <Link href={'/sample/board/list-ssr'}>
            {`SSR List - 서버에서 첫 목록을 포함한 html을 렌더링해줍니다.=> 이후 CSR에서 작동`}
          </Link>
        </li>
      </ul>
    </Layout>
  );
};

export default BoardIndex;
