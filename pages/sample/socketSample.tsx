import React, { FC, useEffect, useRef, useState } from 'react';

import { observer } from 'mobx-react-lite';
import { SocketApi } from 'apis/index';
import Layout from 'components/layout/Layout/Layout';

import { CompatClient, IMessage, Stomp } from '@stomp/stompjs';
import SocketJs from 'sockjs-client';
import _ from 'lodash';
import { Button } from 'smith-ui-components/button';

const SocketSample: FC = observer(() => {
  return (
    <Layout>
      <SocketSampleComponent />
    </Layout>
  );
});
const SocketSampleComponent = () => {
  const socketClient = useRef<CompatClient>();

  const [pubUrl, setPubUrl] = useState<string>();
  const connectClient = (client: any) => {
    return new Promise((resolve, reject) => {
      if (client) {
        if (!client.connected) {
          client.connect(
            {},
            () => {
              resolve(true);
            },
            (error: any) => {
              reject(error);
            }
          );
        } else {
          resolve(true);
        }
      } else {
        reject('client is null or undefined');
      }
    });
  };

  // 사용자 소켓 URL 가져오기
  useEffect(() => {
    // let socketClient: CompatClient | undefined = undefined;
    // let interval: NodeJS.Timeout;
    const connectSocket = async () => {
      if (SocketApi === undefined) {
        return;
      }

      const username = 'blocksmith'; //commonStore.userInfo?.username;
      if (_.isEmpty(username)) {
        return;
      }

      // 소켓 접속 정보 가져오기
      const response = await SocketApi.topic.getConnectVideoStartUrlUsingGet(
        username!
      );

      const data = response.data;
      const { wsUrl, subUrl, pubUrl } = data;
      setPubUrl(pubUrl);

      // 소켓 연결
      const socket = new SocketJs(wsUrl!);
      socketClient.current = Stomp.over(socket);
      await connectClient(socketClient);
      // 기존에 보고있던 영상을 튕겨내기위해 새로 접속했다고 알림.
      socketClient.current.send(
        pubUrl!,
        {},
        JSON.stringify({
          accountId: username!,
        })
      );

      socketClient.current.subscribe(subUrl!, (message: IMessage) => {
        if (message.body) {
          console.log('-> 수신 메시지 message.body', message.body);
        }
      });
    };
    connectSocket().then();

    return () => {
      // 소켓 dispose
      socketClient.current?.deactivate();
    };
  }, []);

  return (
    <div>
      소켓 샘플{' '}
      <Button
        label={'다른 창에 알림'}
        onClick={() => {
          if (pubUrl) {
            socketClient.current?.send(pubUrl, {}, '전송메시지');
          }
        }}
      />
    </div>
  );
};

export default SocketSample;
