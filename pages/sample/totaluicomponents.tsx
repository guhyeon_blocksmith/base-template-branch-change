import React, { FC } from 'react';
import { config, custom } from 'config/layoutSetting';
import Layout from 'components/layout/Layout';

import TotalUiComponents from 'components/sample/TotalUiComponents';

const TotaluicomponentsPage: FC = () => {
  return (
    <Layout config={config} custom={custom}>
      <TotalUiComponents />
    </Layout>
  );
};

export default TotaluicomponentsPage;
