import React from 'react';
import Link from 'next/link';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import useTranslation from 'libs/hooks/useTranslation';

const Multilingual = () => {
  const { t } = useTranslation();
  return (
    <Layout config={config} custom={custom}>
      <>
        <h1>Multilingual Page</h1>
        <p>This is the Multilingual page</p>
        <p>{t('slogan')}</p>
        <p>
          <Link href="/">
            <a>Go home</a>
          </Link>
        </p>
      </>
    </Layout>
  );
};

export default Multilingual;
