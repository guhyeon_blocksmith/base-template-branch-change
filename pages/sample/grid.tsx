import React from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { Button } from 'smith-ui-components/button';
import { InputText } from 'smith-ui-components/inputtext';
import { InputTextarea } from 'smith-ui-components/inputtextarea';
import GridTable from 'components/common/GridTable';
import GridTableItem from 'components/common/GridTableItem';
import { Password } from 'smith-ui-components/password';

const Grid = () => {
  return (
    <Layout config={config} custom={custom}>
      <>
        <strong className="contTitle">회원 정보 검색</strong>

        <div>
          <Button icon="si-exel" label="엑셀" iconColor={'black'} />
          <Button icon="si-exel" label="엑셀" className="p-button-outlined" />
        </div>

        <Password />

        <table className="defTable">
          <colgroup>
            <col style={{ width: '120px' }} />
            <col style={{ width: 'calc(50% - 120px)' }} />
            <col style={{ width: '120px' }} />
          </colgroup>
          <tbody>
            <tr>
              <th>
                <i className="must">*</i>
                <span>검색어</span>
              </th>
              <td></td>
              <th>
                이메일
                <i className="must">*</i>
              </th>
              <td></td>
            </tr>
            <tr>
              <th>성별</th>
              <td></td>
              <th>
                <i className="must">*</i>
                <span>거주지</span>
              </th>
              <td></td>
            </tr>
            <tr>
              <th>연령대</th>
              <td></td>
              <th>알게된 경로</th>
              <td></td>
            </tr>
            <tr>
              <th>날짜</th>
              <td></td>
              <th>최종접속일</th>
              <td></td>
            </tr>
          </tbody>
        </table>
        <div className="botBtns">
          <Button label="검색" />
          <Button label="초기화" className="p-button-outlined gray" />
        </div>

        <strong className="contTitle">
          반응형 테이블(p-grid smithGridTable)
        </strong>

        <GridTable
          gridTitle="그리드 타이틀"
          style={{ marginBottom: '40px' }}
          //className="blah"
        >
          <GridTableItem
            className={'p-col-3 p-md-6 p-lg-4'}
            title={
              <>
                제목
                <input
                  type="checkbox"
                  style={{
                    position: 'relative',
                    top: '2px',
                    marginLeft: '4px',
                  }}
                />
              </>
            }
          >
            텍스트
          </GridTableItem>
          <GridTableItem
            title={'제목2'}
            className={'p-col-3 p-md-6 p-lg-4'}
            thAlign="right"
            tdVAlign="bottom"
          >
            text
          </GridTableItem>
          <GridTableItem title={'버튼'} className={'p-col-3 p-md-6 p-lg-4'}>
            <InputText />
          </GridTableItem>
          <GridTableItem title={'제목'} className={'p-col-3 p-md-6 p-lg-4'}>
            <InputText />
          </GridTableItem>
          <GridTableItem title={'성공!'} className={'p-col-3 p-md-6 p-lg-4'}>
            <Password />
          </GridTableItem>
          <GridTableItem
            title={'성공!'}
            className={'p-col-3 p-md-6 p-lg-4'}
            thAlign={'right'}
            thVAlign={'top'}
            tdVAlign={'bottom'}
            tdAlign={'right'}
          >
            text
          </GridTableItem>
        </GridTable>

        <strong className="contTitle">반응형 테이블</strong>
        <ul className="fakeTable">
          <li>
            <dl>
              <dt>연령대</dt>
              <dd>첫번째</dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd>
                <InputText />
              </dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd>
                <InputTextarea />
              </dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li className="row">
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
          <li>
            <dl>
              <dt>연령대</dt>
              <dd></dd>
            </dl>
            <dl>
              <dt>알게된 경로</dt>
              <dd></dd>
            </dl>
          </li>
        </ul>
        <strong className="contTitle">기본정보</strong>
        <strong className="smallTitle">필수정보</strong>
        <table className="defTable">
          <colgroup>
            <col style={{ width: '120px' }} />
            <col style={{ width: 'calc(50% - 120px)' }} />
            <col style={{ width: '120px' }} />
          </colgroup>
          <tbody>
            <tr>
              <th>검색어</th>
              <td></td>
              <th>검색어</th>
              <td></td>
            </tr>
          </tbody>
        </table>
      </>
    </Layout>
  );
};

export default Grid;
