import { NextApiRequest, NextApiResponse } from 'next';
import { setStatelessSession } from 'libs/AuthUtil';
import { Api as _ApiUser } from 'apis/ApiUser';
import { cors } from 'libs/proxyUtil';

// 사용자사이트 로그인용 api
const loginApi = new _ApiUser({
  baseURL: process.env.NEXT_PUBLIC_API_USER_URL,
});

/**
 * 로그인 프록시 실제 로그인 호출 후, 토큰을 가져와 헤더에 심는다.
 * @param req
 * @param res
 */
export default async (req: NextApiRequest, res: NextApiResponse) => {
  // 스토리북, 개발모드인 경우 cors해제
  if (process.env.NODE_ENV !== 'production') {
    await cors(req, res);
  }
  // 로그인 서버 호출
  // 로그인 완료 후 access-token, refresh-token cookie에 저장
  try {
    const { username, password, service } = req.body;
    console.log('[user auth login] username', username);

    // 원래 api로 호출
    const response = await loginApi.login.loginUsingPost({
      username,
      password,
      service,
    });

    const { accessToken, refreshToken } = response.data;

    // 로그인 실패인 경우
    if (accessToken === undefined || refreshToken === undefined) {
      res.status(401).json({ isSuccess: false, error: { code: '401' } });
      res.end();
      return;
    }

    setStatelessSession(req, res, {
      accessToken,
      refreshToken,
    });

    res.status(200).json({ isSuccess: true, error: { code: '00' } });
    res.end();
    return;
  } catch (e) {
    console.log('[user auth login] failed', e);
    res.status(401).json({ isSuccess: false, error: { code: '401' } });
    res.end();
  }
};
