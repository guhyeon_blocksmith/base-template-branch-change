import * as uuid from 'uuid';
import dynamoDb from '../../libs/dynamo-db';
import { toJS } from 'mobx';

export default async function handler(req: any, res: any) {
  if (req.method === 'PUT') {
    const item = {
      sid: uuid.v4(),
      ...req.body,
      createdAt: Date.now(),
    };

    await dynamoDb.put({
      Item: item,
    });

    res.status(201).json(item);
  }

  if (req.method === 'GET') {
    const params = {
      FilterExpression: 'guid = :guid',
      ExpressionAttributeValues: {
        ':guid': req.query.guid,
      },
    };

    let responseBody: any = '';
    let statusCode: any = 0;

    try {
      const data = await dynamoDb.scan(params);
      responseBody = data;
      statusCode = 200;
    } catch (err) {
      responseBody = `Unable to scan product: ${err}`;
      statusCode = 403;
    }

    const result = {
      statusCode: statusCode,
      headers: {
        'Content-Type': 'application/json',
        'access-control-allow-origin': '*',
      },
      body: toJS(responseBody),
    };

    res.status(200).json(result);
  }

  if (req.method === 'POST') {
    const { Attributes } = await dynamoDb.update({
      Key: {
        sid: req.body.sid,
        title: req.body.title,
      },
      UpdateExpression: 'SET content = :content',
      ExpressionAttributeValues: {
        ':content': req.body.content || null,
      },
      ReturnValues: 'ALL_NEW',
    });

    res.status(200).json(Attributes);
  }

  if (req.method === 'DELETE') {
    await dynamoDb.delete({
      Key: {
        id: req.query.id,
      },
    });

    res.status(204).json({});
  }
}
