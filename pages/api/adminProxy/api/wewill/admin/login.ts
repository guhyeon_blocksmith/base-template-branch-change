import { NextApiRequest, NextApiResponse } from 'next';
import { setStatelessSession } from 'libs/AuthUtil';
import { Api as _ApiAdmin } from 'apis/ApiAdmin';
import { cors } from 'libs/proxyUtil';

// 관리자 로그인용 api
const adminLoginApi = new _ApiAdmin({
  baseURL: process.env.NEXT_PUBLIC_API_ADMIN_URL,
});

/**
 * 로그인 프록시 실제 로그인 호출 후, 토큰을 가져와 헤더에 심는다.
 * @param req
 * @param res
 */
export default async (req: NextApiRequest, res: NextApiResponse) => {
  // 스토리북, 개발모드인 경우 cors해제
  if (process.env.NODE_ENV !== 'production') {
    await cors(req, res);
  }
  // 로그인 서버 호출
  // 로그인 완료 후 access-token, refresh-token cookie에 저장
  try {
    const { loginId, password } = req.body;
    console.log('[admin auth login] loginId', loginId);

    // 원래 api로 호출
    const response = await adminLoginApi.login.loginManagerUsingPost({
      loginId,
      password,
    });

    const { accessToken } = response.data;

    // 로그인 실패인 경우
    if (accessToken === undefined) {
      res.status(401).json({ isSuccess: false, error: { code: '401' } });
      res.end();
      return;
    }

    setStatelessSession(
      req,
      res,
      {
        accessToken,
      },
      'adminSession'
    );

    res.status(200).json({ isSuccess: true, error: { code: '00' } });
    res.end();
    return;
  } catch (e) {
    console.log('[admin auth login] failed', e);
    res.status(401).json({ isSuccess: false, error: { code: '401' } });
    res.end();
  }
};
