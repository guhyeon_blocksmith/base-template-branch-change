import { privateProxyApiHandler } from 'libs/proxyUtil';

/**
 * 실제 API URL
 */
const API_URL = process.env.NEXT_PUBLIC_API_ADMIN_URL;

export const config = {
  api: {
    bodyParser: false,
  },
};

export default privateProxyApiHandler(
  process.env.NEXT_PUBLIC_API_ADMIN_PRIVATE_PROXY_URL!,
  API_URL,
  'adminSession'
);
