import React from 'react';
import Layout from 'components/layout/Layout';
import { config, custom } from 'config/layoutSetting';
import { LayoutCustom } from 'types/index';
// import SubCompText from 'components/sub/SubCompText';
// import SubCompHeading from 'components/sub/SubCompHeading';
// import SubCompList from 'components/sub/SubCompList';
// import SubCompIcons from 'components/sub/SubCompIcons';
// import SubCompButtons from 'components/sub/SubCompButtons';
// import SubCompInputs from 'components/sub/SubCompInputs';

const SubPage: React.FC = () => {
  const layoutsSettings: LayoutCustom = {
    ...custom,
    useCustomContent: false,
  };
  const configSettings = {
    ...config,
    showMenu: false,
  };

  return (
    <Layout config={configSettings} custom={layoutsSettings}>
      <div className="margin">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-6">
              {/*<SubCompText />*/}

              {/*<SubCompHeading />*/}

              {/*<SubCompList />*/}
            </div>

            <div className="col-xs-12 col-md-6">
              {/*<SubCompIcons />*/}

              {/*<SubCompButtons />*/}

              {/*<SubCompInputs />*/}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default SubPage;
