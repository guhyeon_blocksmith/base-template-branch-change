import React, { FC, useEffect } from 'react';
import Layout from 'components/layout/Layout';
import GridTable from 'components/common/GridTable';
import GridTableItem from 'components/common/GridTableItem';
import { Button } from 'smith-ui-components/button';
import { observer } from 'mobx-react';
import { useStore } from 'libs/hooks/useStore';
import { PasswordChangeDialog } from 'components/adminCommon/PasswordChangeDialog';
import MyPageStore from 'stores/adminCommon/MyPageStore';

const StaffInfoComponent = observer((props: { store: MyPageStore }) => {
  if (props.store.item === undefined) {
    return null;
  }
  return (
    <>
      <strong className="contTitle">마이페이지</strong>
      <GridTable>
        <GridTableItem title="아이디" className={'p-col-6'}>
          {props.store.item.loginId}
        </GridTableItem>
        <GridTableItem title="이름" className={'p-col-6'}>
          {props.store.item.name}
        </GridTableItem>
        <GridTableItem title="이메일" className={'p-col-6'}>
          {props.store.item.email}
        </GridTableItem>
        <GridTableItem title="핸드폰 번호" className={'p-col-6'}>
          {props.store.item.mobileNo}
        </GridTableItem>
        {/*<GridTableItem title="부서명" className={'p-col-12'}>*/}
        {/*  {props.store.item.roleDc}*/}
        {/*</GridTableItem>*/}
        {/*<GridTableItem title="권한" className={'p-col-12'}>*/}
        {/*  {store.item.auth}*/}
        {/*</GridTableItem>*/}
        <GridTableItem title="비밀번호 변경" className={'p-col-12'}>
          <Button
            label="비밀번호 변경"
            onClick={props.store.handleChangePassword}
          />
        </GridTableItem>
      </GridTable>
      <PasswordChangeDialog />
    </>
  );
});

/**
 * 마이페이지. MS_MYPAGE_001
 */
const MyPage: FC = observer(() => {
  const { myPageStore: store, commonStore } = useStore();

  useEffect(() => {
    if (commonStore.userInfo) {
      store.load().then();
    }
  }, [commonStore, commonStore.userInfo, store]);

  return (
    <Layout myPage>
      <StaffInfoComponent store={store} />
    </Layout>
  );
});

export default MyPage;
