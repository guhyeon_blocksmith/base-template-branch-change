import React from 'react';
import { config, custom } from 'config/layoutSetting';
import Layout from 'components/layout/Layout';
import Link from 'next/link';
import { Button } from 'smith-ui-components/button';
import { alert, confirm } from 'smith-ui-components/dialog';
// import { GetServerSideProps } from 'next';
// import { getOrRefreshAccessToken } from 'libs/AuthUtil';

// // /**
// //  *  SSR 방식 초기 데이터 가져오기 redirect to login
// //  */
// export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
//   try {
//     await getOrRefreshAccessToken(req!, res!, 'adminSession');
//     return { props: {} };
//   } catch {
//     return {
//       redirect: {
//         permanent: false,
//         destination: '/auth/login?returnUrl=/',
//       },
//     };
//   }
// };

const IndexPage: React.FC = () => {
  return (
    <Layout config={config} custom={custom}>
      <>
        <Button
          label={'alert'}
          onClick={() => {
            alert('asf');
            confirm('bbb');
          }}
        />
        <Link href={'/auth/login'}>
          <a>로그인</a>
        </Link>
        <h1>Layout Dashboard</h1>
        <p>The Ultimate UI Component Library</p>
      </>
    </Layout>
  );
};

export default IndexPage;
