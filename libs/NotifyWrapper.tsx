import React, { FC, useEffect, useRef, useState } from 'react';
import { SocketApi } from 'apis/index';
// import useSocket from 'libs/hooks/useSocket';
import { Toast } from 'smith-ui-components/toast';
// import { NotificationMessage } from 'apis/ApiSocket';
import { observer } from 'mobx-react';
import { useStore } from 'libs/hooks/useStore';
// import { ActivationState } from '@stomp/stompjs';
// import Layout from 'components/layout/Layout/Layout';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// // @ts-ignore
// import SockJsClient from 'react-stomp';
import { NotificationMessage } from 'apis/ApiSocket';
import { alertUser } from 'libs/userCommon';
import { useRouter } from 'next/router';
import { ActivationState } from '@stomp/stompjs';
import useSocket from 'libs/hooks/useSocket';

export interface NotifyProviderProps {
  /**
   * 로그인 사용자 아이디
   */
  accountId?: string;
}

/**
 * 알림 Wrapper 컴포넌트.
 * @param children
 * @param accountId
 * @constructor
 */
export const NotifyWrapper: FC<NotifyProviderProps> = observer(
  ({ children }) => {
    const { commonStore } = useStore();
    const router = useRouter();

    const refToast = useRef<Toast>(null);

    // const socketRef = useRef<SockJsClient>(null);

    const showNotify = async (socketBody: NotificationMessage) => {
      if (socketBody) {
        // 소켓 알림 메시지 오면, alert처리. 다른 메시지(confirm)등의 타입은 추후 확장필요.
        await alertUser(socketBody.message!);
        // redirect to link
        // 링크가 있을경우 링크로 보낸다.
        if (socketBody.linkUrl) {
          await router.push(socketBody.linkUrl!);
        }

        // refToast.current?.show({
        //   severity: 'warn',
        //   sticky: true,
        //   content: (
        //     <div className="p-flex p-flex-column" style={{ flex: '1' }}>
        //       {JSON.stringify(socketBody)}
        //     </div>
        //   ),
        // });
      }
    };

    // 소켓 주소
    const [connectApiUrl, setConnectApiUrl] = useState<string>();

    // 소켓 Unique 구독 URL
    const [destination, setDestination] = useState<string>();

    // 사용자 소켓 URL 가져오기
    useEffect(() => {
      if (
        commonStore.userInfo === undefined ||
        commonStore.userInfo?.id === undefined
      )
        return;

      const fetchData = async () => {
        if (SocketApi) {
          const response = await SocketApi.topic.getConnectUrlUsingGet(
            commonStore.userInfo!.id!.toString()
          ); // accountId  <-- 고유 소켓방 생성
          const data = response.data;
          const { subUrl, wsUrl } = data;
          // host: "http://dev1.blocksmith.xyz:9000"
          // subUrl: "/sub/s-notification/2"
          // wsUrl: "http://dev1.blocksmith.xyz:9000/ws-stomp"
          setConnectApiUrl(wsUrl);
          setDestination(subUrl);
        }
      };
      fetchData().then();
    }, [commonStore.userInfo, commonStore.userInfo?.id]);

    // 사용자 소켓 연결
    const {
      socketBody,
      close,
      activationState,
    } = useSocket<NotificationMessage>({
      url: connectApiUrl,
      destination,
    });

    useEffect(() => {
      if (
        activationState === ActivationState.ACTIVE &&
        commonStore.userInfo === undefined
      ) {
        close();
      }
    }, [commonStore.userInfo, activationState, close]);

    useEffect(() => {
      if (socketBody) {
        showNotify(socketBody).then();
      }
    }, [socketBody]);

    return (
      // <NotifyContext.Provider value={{ socketBody }}>
      <>
        {/*{connectApiUrl && (*/}
        {/*  <SockJsClient*/}
        {/*    url={connectApiUrl}*/}
        {/*    topics={[destination]}*/}
        {/*    onMessage={showNotify}*/}
        {/*    ref={socketRef}*/}
        {/*  />*/}
        {/*)}*/}
        {children}
        <Toast ref={refToast} />
        {/*</NotifyContext.Provider>*/}
      </>
    );
  }
);
