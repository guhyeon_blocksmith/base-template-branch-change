import { today } from 'libs/common';
import { SearchControl } from 'components/adminCommon/SearchPanel';

/**
 * 기간 컨트롤. id : range.
 */
export const dateRangeControl: SearchControl = {
  label: '기간',
  id: 'range',
  control: 'dateRangePicker',
  className: 'p-col-12',
};

/**
 * 매출단위 컨트롤. id : salesUnit.
 */
export const salesUnitControl: SearchControl = {
  label: '매출 단위',
  id: 'salesUnit',
  control: 'radio',
  className: 'p-col-12',
  items: [
    { name: '일별', key: 'day' },
    { name: '주별', key: 'week' },
    { name: '월별', key: 'month' },
    { name: '년별', key: 'year' },
  ],
};

/**
 * 기본 조건, 기간.
 */
export const defaultSearchConditionRange = {
  range: {
    startDate: today.subtract(1, 'month').format('YYYY-MM-DD'),
    endDate: today.format('YYYY-MM-DD'),
  },
};
/**
 * 기본 조건, 매출단위.
 */
export const defaultSearchConditionSalesUnit = {
  salesUnit: 'day',
};
