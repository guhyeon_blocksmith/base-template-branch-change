import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { isServer } from 'libs/common';

export const GA_TRACKING_ID = process.env.NEXT_PUBLIC_GA_ID;

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
/**
 * 페이지뷰 GA 호출
 * @param url
 */
export const pageview = (url: string) => {
  if (GA_TRACKING_ID && !isServer) {
    (window as any).gtag('config', GA_TRACKING_ID, {
      page_path: url,
    });
  }
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
/**
 * Event GA 호출
 * @param action
 * @param category
 * @param label
 * @param value
 */
export const event = ({
  action,
  category,
  label,
  value,
}: {
  action: string;
  category: string;
  label: string;
  value: number;
}) => {
  if (GA_TRACKING_ID && !isServer) {
    (window as any).gtag('event', action, {
      event_category: category,
      event_label: label,
      value: value,
    });
  }
};

/**
 * GA(GoogleAnalytics) 페이지 뷰 hooks. 최상위에 한 번만 사용하면
 * GA pageview 이벤트를 호출합니다.
 * env에 GA_ID가 없을 경우 작동하지 않습니다.
 */
export const useGaPageView = () => {
  // [GA] google analytics 페이지 이동 기록
  const router = useRouter();
  useEffect(() => {
    if (
      GA_TRACKING_ID === '' ||
      GA_TRACKING_ID === undefined ||
      GA_TRACKING_ID === null
    ) {
      return;
    }

    const handleRouteChange = (url: string) => {
      pageview(url);
    };
    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);
};
