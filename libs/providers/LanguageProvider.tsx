import React, { createContext, FC, useState } from 'react';
import { config } from 'config/layoutSetting';

interface LanguageContextType {
  locale: string;
  setLocale: (locale: string) => void;
}

export const LanguageContext = createContext<LanguageContextType | undefined>(
  undefined
);

/**
 * LanguageProvider. TODO i18n으로 변경 필요 next-js sample repository 참고
 * @param children
 * @constructor
 */
export const LanguageProvider: FC = ({ children }) => {
  const { multilingual } = config;
  const { defaultLocale } = multilingual;
  const [locale, setLocale] = useState<string>(defaultLocale || 'kr');

  return (
    <LanguageContext.Provider value={{ locale: locale, setLocale: setLocale }}>
      {children}
    </LanguageContext.Provider>
  );
};
