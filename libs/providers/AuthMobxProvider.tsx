import React, { FC, useEffect } from 'react';
import { useRouter } from 'next/router';
import { PrivateAdminApi } from 'apis/index';
import RootStore from 'stores/RootStore';
import { observer } from 'mobx-react';
import Loading from 'components/common/Loading';

interface AuthMobxProviderProps {
  rootStore: RootStore;
}

const loginPath = '/auth/login';
const logoutPath = '/auth/logout';

/**
 * rootStore에 commonStore에 userInfo를 채워준다. 로그인을 안한 경우 로그인페이지로 이동시킨다.
 * @param rootStore
 * @param children
 * @constructor
 */
export const AuthMobxProvider: FC<AuthMobxProviderProps> = observer(
  ({ rootStore, children }) => {
    // const rootStore = useStore();
    const router = useRouter();

    // 로그인 사용자 정보를 가져와 공통 store에 넣어준다.
    // 매 페이지이동시마다, userInfo가 없으면 실행
    useEffect(() => {
      const fetchMe = async () => {
        try {
          // 로딩 지연 테스트
          // await (() => {
          //   return new Promise((resolve) => {
          //     setTimeout(() => {
          //       console.log('asf');
          //       resolve(0);
          //     }, 3000);
          //   });
          // })();

          // 내 정보는 한 번만 가져와서 넣어줘도 상관없음. backend에서 api 호출 자체를 token으로 제어하기 때문
          // 처음 온 페이지에 로그인이 안되어있을 때 로그인 페이지로 이동
          const response = await PrivateAdminApi.managers.managerMeUsingGet();

          rootStore.commonStore.userInfo = response.data;
          // console.log('fetchMe', response.data);
        } catch {
          rootStore.commonStore.userInfo = undefined;

          // 로그인 토큰 인증 실패
          if (router.pathname !== loginPath) {
            router
              .push({
                pathname: loginPath,
                query: { returnUrl: router.pathname },
              })
              .then();
          }
        }
      };
      fetchMe().then();
    }, [rootStore, router, router.pathname]);

    if (
      rootStore.commonStore.userInfo ||
      router.pathname === loginPath ||
      router.pathname === logoutPath
    ) {
      return <>{children}</>;
    } else {
      return <Loading />;
    }
  }
);
