import React, { createContext, FC, useState } from 'react';
import useStylesheet from 'libs/hooks/useStylesheet';

interface ThemeContextType {
  selectedTheme: string;
  setSelectedTheme: (value: string) => void;
}

const defaultThemeCode = process.env.NEXT_PUBLIC_DEFAULT_THEME || ''; // 'base';
// const useThemeSelector =
//   process.env.NEXT_PUBLIC_USE_THEME_SELECTOR === 'true' || false;

/**
 * 테마 컨텍스트.
 */
export const ThemeContext = createContext<ThemeContextType>({
  selectedTheme: defaultThemeCode,
  setSelectedTheme: (value: string) => {
    throw `${value}를 적용할 수 없습니다. ThemeProvider생성 전에 호출되었습니다.`;
  },
});

/**
 * 테마 프로바이더
 * @param children
 * @constructor
 */
export const ThemeProvider: FC = ({ children }) => {
  const [selectedTheme, setSelectedTheme] = useState<string>(defaultThemeCode);

  useStylesheet(
    selectedTheme ? `/themes/${selectedTheme}/theme.css` : undefined
  );

  return (
    <ThemeContext.Provider value={{ selectedTheme, setSelectedTheme }}>
      {children}
    </ThemeContext.Provider>
  );
};
