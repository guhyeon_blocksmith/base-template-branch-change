import React, { FC } from 'react';
import { ThemeProvider } from 'libs/providers/ThemeProvider';
import { LanguageProvider } from 'libs/providers/LanguageProvider';
import { RootStoreProvider } from 'libs/providers/RootStoreProivder';
import RootStore from 'stores/RootStore';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import { NotifyWrapper } from 'libs/NotifyWrapper';

interface AppProviderProps {
  rootStore: RootStore;
}

/**
 * RootWrapper. AppProvider. 앱에 제공하는 프로바이더들을 모아 제공.
 * @param rootStore
 * @param accountId
 * @param children
 * @constructor
 */
const AppProvider: FC<AppProviderProps> = ({ rootStore, children }) => {
  const RECAPTCHA_API_KEY = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;
  const RECAPTCHA_LANG = process.env.NEXT_PUBLIC_RECAPTCHA_LANG || 'kr';

  return (
    <GoogleReCaptchaProvider
      reCaptchaKey={RECAPTCHA_API_KEY}
      language={RECAPTCHA_LANG}
    >
      <ThemeProvider>
        <LanguageProvider>
          <RootStoreProvider rootStore={rootStore}>
            <NotifyWrapper>{children}</NotifyWrapper>
          </RootStoreProvider>
        </LanguageProvider>
      </ThemeProvider>
    </GoogleReCaptchaProvider>
  );
};

export default AppProvider;
