import { NextApiRequest, NextApiResponse } from 'next';
import httpProxy from 'http-proxy';
import { getOrRefreshAccessToken } from 'libs/AuthUtil';
import Cors from 'cors';
import initMiddleware from 'libs/init-middleware';

// Initialize the cors middleware
export const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  // 모든 cors 허용
  Cors()
);

/**
 * publicProxyApiHandler
 * @param fromPath /api/proxy 등 next api를 호출 할때 붙는 proxy주소
 * @param toUrl 실제 호출해야하는 api base url
 * @constructor
 */
export const publicProxyApiHandler = (
  fromPath: string,
  toUrl?: string
) => async (req: NextApiRequest, res: NextApiResponse) => {
  // 스토리북, 개발모드인 경우 cors해제
  if (process.env.NODE_ENV !== 'production') {
    await cors(req, res);
  }

  const proxy = httpProxy.createProxyServer();

  if (toUrl === undefined) {
    throw Error('proxy target url이 없습니다.');
  }
  //console.log('proxy', req.url);

  if (req.url === undefined) {
    // throw Error('url is undefined');
    res.status(404);
    res.end();
    return;
  }

  // Rewrite URL, strip out leading '/api'
  // '/api/sample/proxy/*' becomes '${API_URL}/*'
  // !Info proxy prefix 변경 필요
  req.url = req.url.replace(fromPath, '');
  console.log(`privateProxyApiHandler reqUrl ${toUrl}${req.url}`);

  // Api에 쿠키 전달 X
  req.headers.cookie = '';

  // 프록시
  return new Promise((resolve, reject) => {
    try {
      proxy
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .once('proxyRes', () => {
          resolve();
        })
        .once('error', (e) => {
          console.error(`Proxy Error : ${toUrl}${req.url}`, e);
          reject(e);
        })
        .web(req, res, {
          target: toUrl,
          autoRewrite: false,
        });
    } catch (e) {
      console.error(e);
    }
  });
};

/**
 * privateProxyApiHandler
 * @param fromPath /api/proxy 등 next api를 호출 할때 붙는 proxy주소
 * @param toUrl 실제 호출해야하는 api base url
 * @param cookieName cookieName. session정보를 암호화하여 저장할 cookie명. 기본값 session
 * @constructor
 */
export const privateProxyApiHandler = (
  fromPath: string,
  toUrl?: string,
  cookieName = 'session'
) => async (req: NextApiRequest, res: NextApiResponse) => {
  // 스토리북, 개발모드인 경우 cors해제
  if (process.env.NODE_ENV !== 'production') {
    await cors(req, res);
  }

  const proxy = httpProxy.createProxyServer();

  if (toUrl === undefined) {
    throw Error('proxy target url이 없습니다.');
  }
  if (req.url === undefined) {
    // throw Error('url is undefined');
    res.status(404);
    res.end();
    return;
  }

  try {
    const accessToken = await getOrRefreshAccessToken(req, res, cookieName);
    req.headers['Authorization'] = `Bearer ${accessToken}`;
  } catch {
    console.log('No Auth', req.url);
    // 401 에러
    res.status(200).json({ error: { code: '401' } });
    res.end();
    return;
  }

  // Rewrite URL, strip out leading '/api'
  // '/api/sample/proxy/*' becomes '${API_URL}/*'
  // !Info proxy prefix 변경 필요
  req.url = req.url.replace(fromPath, '');
  console.log(`privateProxyApiHandler reqUrl ${toUrl}${req.url}`);
  // Api에 쿠키 전달 X
  req.headers.cookie = '';

  // 프록시
  return new Promise((resolve, reject) => {
    try {
      proxy
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .once('proxyRes', () => {
          resolve();
        })
        .once('error', (e) => {
          console.error(`Proxy Error : ${toUrl}${req.url}`, e);
          reject(e);
        })
        .web(req, res, {
          target: toUrl,
          autoRewrite: false,
        });
    } catch (e) {
      console.error(e);
    }
  });
};
