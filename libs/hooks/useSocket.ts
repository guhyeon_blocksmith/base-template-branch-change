import { useCallback, useEffect, useState } from 'react';
import { ActivationState, CompatClient, IMessage, Stomp } from '@stomp/stompjs';
import SocketJs from 'sockjs-client';

export interface useSocketProps {
  /**
   * 소켓 url. http
   */
  url?: string;

  /**
   * 소켓 subscribe destination
   */
  destination?: string;
}

/**
 * 소켓 hook. 글로벌로 사용하는걸 추천합니다. 소켓 연결이 많아질 수 있음.
 * @param url
 * @param destination
 */
const useSocket = <T>({ url, destination }: useSocketProps) => {
  const [socketBody, setSocketBody] = useState<T>();
  const [client, setClient] = useState<CompatClient>();

  // 소켓 활성화 상태
  const [activationState, setActivationState] = useState<ActivationState>();

  // 소켓 client 생성
  useEffect(() => {
    if (url === undefined) {
      return;
    }

    const createClient = async () => {
      const webSocket = await new SocketJs(url);
      const client = await Stomp.over(webSocket);
      setClient(client);
    };
    createClient().then();
  }, [url]);

  const callbackSubscribe = useCallback(
    (msg: IMessage) => {
      const msgBody = JSON.parse(msg.body);
      setSocketBody(msgBody);
    },
    [setSocketBody]
  );

  // 소켓 연결
  useEffect(() => {
    if (client === undefined) return;
    if (destination === undefined) return;

    client.connect({}, function () {
      client.subscribe(destination, callbackSubscribe);
    });

    return () => {
      try {
        client?.unsubscribe(destination);
        client?.disconnect();
      } catch (e) {
        console.log(e);
      }
    };
  }, [client, destination, callbackSubscribe]);

  useEffect(() => {
    setActivationState(client?.state);
  }, [client?.state]);

  /**
   * 소켓 종료
   */
  const close = useCallback(() => {
    return client?.deactivate();
  }, [client]);

  return { socketBody, close, activationState };
};

export default useSocket;
