import { useRef } from 'react';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

const useHtmlToPdf = (pdfName?: string) => {
  const htmlRef = useRef<HTMLDivElement | null>(null);
  const printToPdf = async () => {
    if (htmlRef.current) {
      const canvas = await html2canvas(htmlRef.current, {
        backgroundColor: '#252525',
      });

      const imgData = canvas.toDataURL('image/png');
      const imgWidth = 210;
      const pageHeight = 297;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;
      let position = 0;

      const pdf = new jsPDF('p', 'mm', 'a4');
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      pdf.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight); // 이미지 형식 png로 통일하지말고 jpeg로 해야 안깨짐
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      pdf.save(`${pdfName ? `${pdfName}.pdf` : 'download.pdf'}`);

      // const blobPDF = new Blob([pdf.output('blob')], {
      //   type: 'application/pdf',
      // });
      // const blobUrl = URL.createObjectURL(blobPDF);
      //
      // window.open(blobUrl);
    }
  };

  const printToPdfBlob = async () => {
    if (htmlRef.current) {
      const height1 =
        (htmlRef.current?.clientHeight * htmlRef.current?.clientWidth) / 795;
      const height2 = Math.round(height1 / 1123 + 1);

      const canvas = await html2canvas(htmlRef.current, {
        onclone: (doc) => {
          const hiddenDiv = doc.getElementById('HtmlToPdfWrapper');
          hiddenDiv!.style.width = '21cm';
        },
        scrollX: 0,
        scrollY: -window.scrollY,
        scale: 3,
        width: 795,
        height: height2 * 1123,
      });

      // const imgData = canvas.toDataURL('image/png');
      // const pdf = new jsPDF('p', 'mm', 'a4');
      // // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // // @ts-ignore
      // pdf.addImage(imgData, 'JPEG', 0, 0); // 이미지 형식 png로 통일하지말고 jpeg로 해야 안깨짐

      const imgData = canvas.toDataURL('image/jpeg');

      const imgWidth = 210;
      // const pageHeight = 295;
      const pageHeight = imgWidth * 1.414;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;
      const pdf = new jsPDF('p', 'mm', 'a4');
      let position = 0;

      pdf.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      const blobPDF = new Blob([pdf.output('blob')], {
        type: 'application/pdf',
      });

      const blobUrl = URL.createObjectURL(blobPDF);
      window.open(blobUrl);
    }
  };

  const printToPdfFile = async (fileName: any) => {
    if (htmlRef.current) {
      const htmlHeight =
        (htmlRef.current?.clientHeight * htmlRef.current?.clientWidth) / 795;
      const pdfHeight = Math.round(htmlHeight / 1123 + 1);

      const canvas = await html2canvas(htmlRef.current, {
        onclone: (doc) => {
          const hiddenDiv = doc.getElementById('HtmlToPdfWrapper');
          const test = doc.querySelector('.pledge-content')! as HTMLElement;
          hiddenDiv!.style.width = '21cm';
          test.style.background = 'white';
        },
        scrollX: 0,
        scrollY: -window.scrollY,
        scale: 3,
        width: 795,
        height: pdfHeight * 1123,
      });

      const imgData = canvas.toDataURL('image/jpeg');
      const imgWidth = 210;
      const pageHeight = imgWidth * 1.414;
      const imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;
      const pdf = new jsPDF('p', 'mm');
      let position = 0;

      pdf.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }

      const blobPDF = new Blob([pdf.output('blob')], {
        type: 'application/pdf',
      });

      return new File([blobPDF], `${fileName}`);
    }
  };
  return { htmlRef, printToPdf, printToPdfBlob, printToPdfFile };
};

export default useHtmlToPdf;
