import { useContext } from 'react';

import { LanguageContext } from 'libs/providers/LanguageProvider';
import { config } from 'config/layoutSetting';

/**
 * 다국어 사용 hooks
 */
export default function useTranslation() {
  const languageContext = useContext(LanguageContext);
  if (languageContext === undefined) {
    throw 'languageContext is undefined';
  }

  const { locale, setLocale } = languageContext;
  // console.log('locale:', locale);

  function t(key: string) {
    //console.log('key:', key);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!config[locale][key]) {
      throw `No string '${key}' for locale '${locale}'`;
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return config[locale][key];
  }

  return { t, locale, setLocale };
}
