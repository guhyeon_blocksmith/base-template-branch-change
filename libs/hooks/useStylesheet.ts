import { useEffect } from 'react';

/**
 * 스타일 태그 추가. head의 link에 href를 넣어줍니다.
 * @param href stylesheet 경로
 */
const useStylesheet = (href?: string) => {
  useEffect(() => {
    if (href) {
      const linkTag = document.createElement('link');
      linkTag.setAttribute('rel', 'stylesheet');
      linkTag.setAttribute('href', href);

      document.getElementsByTagName('head')[0].appendChild(linkTag);

      return () => {
        linkTag.remove();
      };
    }
  }, [href]);
};

export default useStylesheet;
