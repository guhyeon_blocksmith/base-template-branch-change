import { ParsedUrlQuery } from 'querystring';
import {
  GetServerSidePropsContext,
  GetServerSidePropsResult,
  NextApiRequest,
  NextApiResponse,
} from 'next';
import { IncomingMessage, ServerResponse } from 'http';
import Cookies from 'cookies';
import jwt_decode from 'jwt-decode';
import aes from 'crypto-js/aes';
import encUtf8 from 'crypto-js/enc-utf8';
import { Api as _ApiUser } from 'apis/ApiUser';

/**
 * GetServerSideProps에 req에 accessToken 주입.
 */
export type GetServerSidePropsAuth<
  P extends { [key: string]: any } = { [key: string]: any },
  Q extends ParsedUrlQuery = ParsedUrlQuery
> = (
  context: GetServerSidePropsContextAuth<Q>
) => Promise<GetServerSidePropsResult<P>>;

/**
 * GetServerSidePropsContext 의 req에 accessToken 추가
 */
export type GetServerSidePropsContextAuth<
  Q extends ParsedUrlQuery = ParsedUrlQuery
> = GetServerSidePropsContext<Q> & {
  req: IncomingMessage & {
    cookies?: { [key: string]: any };
    accessToken?: string;
  };
};

/**
 * getServerSideProps에서(SSR) req에 accessToken을 주입
 * @param handler 핸들러파라미터
 */
export function withAuthTokenSSR(handler: GetServerSidePropsAuth) {
  const withAuthTokenHandler: GetServerSidePropsAuth = async (context) => {
    const req = context.req;
    const res = context.res;

    // 토큰을 불러옵니다.
    const cookies = new Cookies(req, res);

    req.accessToken = cookies.get('access-token');

    return handler(context);
  };
  return withAuthTokenHandler;
}

/**
 * NextApiRequest 에 accessToken 주입
 */
export interface NextApiRequestAuth extends NextApiRequest {
  accessToken?: string;
}

/**
 * NextApiHandler. accessToken 주입
 */
export type NextApiHandlerAuth<T = any> = (
  req: NextApiRequestAuth,
  res: NextApiResponse<T>
) => void | Promise<void>;

interface JwtType {
  exp: number;
}

/**
 * 기간에 맞는 JWT 토큰이 있는지 체크
 */
export function hasJwtToken(jwtToken: JwtType | string) {
  if (jwtToken === undefined) return false;

  if (typeof jwtToken === 'string') {
    jwtToken = jwt_decode<JwtType>(jwtToken);
  }
  let isValid = false;
  // 토큰 유무 확인
  if (jwtToken) {
    // 토큰에 기간 있는지 확인. 토큰 기간은 필수 (권장사항 강제)
    if (jwtToken.exp) {
      // 토큰 기간 만료 확인
      let currentTime;
      // exp가 epoch seconds 인 경우
      if (jwtToken.exp.toString().length === 10) {
        currentTime = new Date().getTime() / 1000;
      } else {
        // exp가 milliseconds 인 경우
        currentTime = new Date().getTime();
      }

      // 토큰이 현재시간 이후인 경우 유효함
      if (jwtToken.exp > currentTime) {
        isValid = true;
      }
    } else {
      throw new Error('jwtToken에 exp가 없습니다.');
    }
  }

  return isValid;
}

/**
 * 쿠키를 이용한 statelessSession에 data를 넣어준다
 * @param req
 * @param res
 * @param data
 * @param cookieName
 */
export function setStatelessSession(
  req: IncomingMessage,
  res: ServerResponse,
  data: any,
  cookieName = 'session'
) {
  // set stateless session
  const cookies = new Cookies(req, res);

  const sessionValue = JSON.stringify(data);
  const cipherText = aes
    .encrypt(sessionValue, process.env.NEXT_PUBLIC_API_AUTH_SESSION_SECRET!)
    .toString();

  cookies.set(cookieName, cipherText, {
    httpOnly: true,
    sameSite: 'strict', // CSRF protection. proxy
    // https 에서만 쿠키를 사용할 지 여부
    secure: process.env.NEXT_PUBLIC_IS_HTTPS === 'true',
  });
}

/**
 * 쿠키를 이용한 statelessSession을 가져온다.
 * @param req
 * @param res
 * @param cookieName
 */
export function getStatelessSession(
  req: IncomingMessage,
  res: ServerResponse,
  cookieName = 'session'
) {
  try {
    const cookies = new Cookies(req, res);
    const sessionValue = cookies.get(cookieName);
    // console.log('getStatelessSession', sessionValue);

    if (sessionValue === undefined) return undefined;

    const decryptedBytes = aes.decrypt(
      sessionValue,
      process.env.NEXT_PUBLIC_API_AUTH_SESSION_SECRET!
    );
    // console.log('getStatelessSession decryptedBytes', decryptedBytes);

    const sessionText = decryptedBytes.toString(encUtf8);
    // console.log('getStatelessSession sessionText', sessionText);

    return JSON.parse(sessionText);
  } catch (e) {
    console.log(e);
    return undefined;
  }
}

/**
 * 쿠키를 이용한 statelessSession을 제거한다.
 * @param req
 * @param res
 * @param cookieName
 */
export function deleteStatelessSession(
  req: IncomingMessage,
  res: ServerResponse,
  cookieName = 'session'
) {
  // set stateless session
  const cookies = new Cookies(req, res);
  cookies.set(cookieName);
}

/**
 * 로그인 여부 확인 ssr
 * @param req
 * @param res
 * @param cookieName
 */
export async function isLogin(
  req: NextApiRequest | IncomingMessage,
  res: NextApiResponse | ServerResponse,
  cookieName = 'session'
) {
  try {
    const accessToken = getOrRefreshAccessToken(req, res, cookieName);
    if (accessToken) {
      return true;
    }
  } catch {
    return false;
  }
}

/**
 * accessToken을 가져온다. accessToken이 만료된 경우 refreshToken으로 재발급하여 가져온다.
 * accessToken이 없으면 401에러
 * @param req
 * @param res
 * @param cookieName
 */
export async function getOrRefreshAccessToken(
  req: NextApiRequest | IncomingMessage,
  res: NextApiResponse | ServerResponse,
  cookieName = 'session'
) {
  console.log('getOrRefreshAccessToken req.url', req.url);
  // get stateless session
  const session = getStatelessSession(req, res, cookieName);

  // 재발급한 토큰을 포함한 새로운 sessionData
  if (session === undefined) {
    throw Error('401');
  }

  const { accessToken, refreshToken } = session;
  // console.log('refreshToken', refreshToken);
  if (hasJwtToken(accessToken)) {
    return accessToken;
  } else if (
    //  관리자는 refresh Token 안함
    process.env.NEXT_PUBLIC_SITE_TYPE === 'USER' &&
    hasJwtToken(refreshToken)
  ) {
    // 토큰 재발급 요청
    try {
      // 관리자 로그인용 api
      const AuthApi = new _ApiUser({
        baseURL: process.env.NEXT_PUBLIC_API_USER_URL,
      });

      // Api Admin에는 refresh가 없기 때문.
      const response = await AuthApi.refresh.refreshTokenUsingPost({
        refreshToken,
      });
      // console.log('refreshTokenUsingPost response', response.data);

      const { accessToken, refreshToken: newRefreshToken } = response.data;
      const newSessionData = {
        accessToken,
        refreshToken: newRefreshToken,
      };
      // token Refresh를 통해 새로운 응답이 있을 경우, 새 쿠키를 구워줍니다.
      if (newSessionData) {
        setStatelessSession(req, res, newSessionData);
      }
      //console.log('>>>4>>');

      return accessToken;
    } catch (e) {
      // console.log('getOrRefreshAccessToken Error Headers', JSON.stringify(e));
      console.log('getOrRefreshAccessToken Error', e);
      throw Error('401');
    }
  } else {
    //console.log('>>>6>>');
    throw Error('401');
  }
}
