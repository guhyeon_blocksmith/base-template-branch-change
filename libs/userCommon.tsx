import {
  AlertOptions,
  ConfirmOptions,
} from 'smith-ui-components/components/dialog/Dialog';
import React from 'react';
import { alert, confirm } from 'smith-ui-components/dialog';

/**
 * 사용자 사이트용 공통 confirm
 * @param content
 * @param title
 * @param options
 */
export const confirmUser = (
  content: string | JSX.Element,
  title?: string | JSX.Element,
  options?: ConfirmOptions
) => {
  return confirm(
    <>
      <h2 className="title">{title}</h2>
      <p className="text">{content}</p>
    </>,
    '',
    {
      noButtonLabel: '아니오',
      okButtonLabel: '삭제하기',
      ...options,
    }
  );
};

/**
 * 사용자 사이트용 공통 alert
 * @param content
 * @param title
 * @param options
 */
export const alertUser = (
  content: string | JSX.Element,
  title?: string | JSX.Element,
  options?: AlertOptions
) => {
  return alert(
    <>
      <h2 className="title">{title}</h2>
      <p className="text">{content}</p>
    </>,
    '',
    {
      buttonLabel: '확인',
      ...options,
    }
  );
};

/**
 * input 하단 active, warning 표시 클래스명 가져오기.
 * 유효성 에러가 없고, 필드에 값이 있으면 active.
 * 유효성 에러가 있으면 warning
 * @param model
 * @param validErrors
 * @param field
 */
export const getStatusClassNameForInput = (
  model: any,
  validErrors: Map<string, string[]>,
  field: string
) => {
  if (validErrors.get(field) && validErrors.get(field)!.length > 0) {
    return 'warning';
  } else {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (model[field].length > 0) {
      return 'active';
    }
  }

  return '';
};
