import CustomFooter from 'components/layoutCustom/CustomFooter';
import CustomHeader from 'components/layoutCustom/CustomHeader';
import CustomContents from 'components/layoutCustom/CustomContents';
import {
  LayoutConfig,
  LayoutCustom,
  MenuItemType,
  MyPageMenuItemType,
  SNBMenuItemType,
} from 'types/index';
import {
  languageCode,
  logoPosition,
  menuTypeGnb,
  menuTypeSnb,
  position,
} from 'types/enum';

export const custom: LayoutCustom = {
  CustomHeader: CustomHeader,
  useCustomHeader: false,
  CustomContent: CustomContents,
  useCustomContent: false,
  CustomFooter: CustomFooter,
  useCustomFooter: false,
  className: 'layout-wrapper',
};

export const config: LayoutConfig = {
  showHeader: true,
  showMenu: true,
  showMenuSearch: true,
  showFooter: true,
  showBreadCrumb: false,
  footerText: 'ⓒ Cusp Fillin : Corp All Rights Reserved. (ver : 1.0.0)',
  footerTextPosition: position.left,
  menuType: {
    snb: menuTypeSnb.panel,
    gnb: menuTypeGnb.oneDepthOnly,
  },
  showMultilingual: true,
  env: {
    isCustomLayout: process.env.NEXT_PUBLIC_IS_CUSTOM_LAYOUT!,
    menuMode: process.env.NEXT_PUBLIC_APP_MENU_MODE!,
    baseUrl: process.env.NEXT_PUBLIC_APP_SITE_URL!,
    menuUrl: process.env.NEXT_PUBLIC_APP_MENU_URL!,
    projectCustomCss: process.env.NEXT_PUBLIC_PROJECT_CUSTOM_CSS!,
    useThemeSelector: process.env.NEXT_PUBLIC_USE_THEME_SELECTOR || 'false',
  },
  multilingual: {
    defaultLocale: languageCode.korean,
    list: [
      { name: 'Korean', code: languageCode.korean },
      { name: 'English', code: languageCode.english },
      { name: 'China', code: languageCode.chinese },
    ],
  },
  layout: {
    logoPosition: logoPosition.snb,
  },
  header: {
    logo: {
      src: '/images/fillin_logo.png',
      width: 180,
      height: 40,
      alt: 'fill in',
    },
    ogImage: `${process.env.NEXT_PUBLIC_APP_SITE_URL!}/images/cusp_thum.jpg`,
    url: process.env.NEXT_PUBLIC_APP_SITE_URL!,
    type: 'website',
    icon: '/favicon.ico',
  },
  kr: {
    slogan: '111Smith Boilerplates가 있는 이중 언어 사이트를 보여주는 예제',
    title: '[KR]Boilerplate Title',
    author: 'Smith templates have been developped by BlockSmith Team',
    description:
      '커스프 커뮤니티센터는 시니어들의 평생교육 활성화, 자아실현, 삶의 질 향상, 나아가 사회 통합을 위하여 설립된 직업능력개발 교육 플랫폼입니다.',
  },
  en: {
    slogan:
      '222An example site showcasing a bilingual site with Smith Boilerplates.',
    title: '[EN]Boilerplate Title',
    author: 'Smith templates have been developped by BlockSmith Team',
    description:
      '커스프 커뮤니티센터는 시니어들의 평생교육 활성화, 자아실현, 삶의 질 향상, 나아가 사회 통합을 위하여 설립된 직업능력개발 교육 플랫폼입니다.',
  },
  cn: {
    slogan: '333一個示例網站，顯示帶有Smith Boilerplates的雙語網站',
    title: '[CN]樣板標題',
    author: 'Smith模板由BlockSmith團隊開發',
    description:
      'Cusp 社區中心是一個職業能力發展教育平台，旨在激發老年人的終身教育，自我實現，生活質量和社會融合.',
  },
};

const depthSample: any = [
  {
    label: 'Folder1',
    icon: 'no-icon',
    url: 'hello',
  },
  {
    label: 'Folder2',
    icon: 'no-icon',
    url: 'love',
  },
  {
    label: 'Depth3',
    icon: 'pi pi-angle-right',
    url: 'korea',
    items: [
      {
        label: 'Folder1',
        icon: 'no-icon',
      },
      {
        label: 'Folder2',
        icon: 'no-icon',
      },
      {
        label: 'Folder3',
        icon: 'no-icon',
      },
    ],
  },
];

export const menuItems: MenuItemType[] = [
  {
    label: 'Get Started',
    icon: 'no-icon',
    url: '',
    items: [
      {
        label: 'Depth2',
        icon: 'pi pi-angle-right',
        items: depthSample,
      },
      {
        label: 'Depth2',
        icon: 'pi pi-angle-right',
        url: 'depth2',
        items: depthSample,
      },
      {
        label: 'Depth2',
        icon: 'pi pi-angle-right',
        url: 'depth2',
        items: depthSample,
      },
    ],
  },
  {
    label: 'Sample',
    className: 'sample',
    icon: 'no-icon',
    url: 'sample',
    items: [
      {
        label: 'Live Stream Player',
        icon: 'pi pi-angle-right',
        url: 'player',
        items: [
          {
            label: 'Play List',
            icon: 'no-icon',
            url: 'vod',
          },
          {
            label: 'Youtube Validator',
            icon: 'no-icon',
            url: 'youtube',
          },
          {
            label: 'Video Controller',
            icon: 'no-icon',
            url: 'controller',
          },
          {
            label: 'Video Tracking',
            icon: 'no-icon',
            url: 'tracking',
          },
          {
            label: 'Video Highlighter',
            icon: 'no-icon',
            url: 'highlight',
          },
        ],
      },
      {
        label: 'Board',
        icon: 'no-icon',
        url: 'board',
      },
      {
        label: 'TotalUiComponents',
        icon: 'no-icon',
        url: 'totaluicomponents',
      },
      {
        label: 'Mobx',
        icon: 'no-icon',
        url: 'mobx',
      },
      {
        label: 'Tab Menu',
        icon: 'no-icon',
        url: 'tabmenu',
      },
      {
        label: 'Grid',
        icon: 'no-icon',
        url: 'grid',
      },
      {
        label: 'GoogleReCaptcha',
        icon: 'no-icon',
        url: 'googleReCaptcha',
      },
      {
        label: 'ImageEditor',
        icon: 'no-icon',
        url: 'imageEditor',
      },
      {
        label: 'ImageUploader',
        icon: 'no-icon',
        url: 'image-upload',
      },
      {
        label: 'FileDownload',
        icon: 'no-icon',
        url: 'fileDownload',
      },
      {
        label: 'Noti',
        icon: 'no-icon',
        url: 'noti',
      },
      {
        label: 'Loading',
        icon: 'no-icon',
        url: 'loading',
      },
      {
        label: 'Double Range',
        icon: 'no-icon',
        url: 'doubleRange',
      },
      {
        separator: true,
      },
      {
        label: 'Depth2',
        icon: 'pi pi-angle-right',
        url: 'practice',
        items: depthSample,
      },
    ],
  },
  {
    label: 'Category',
    icon: 'no-icon',
    url: 'category',
    items: [
      {
        label: 'Hello',
        icon: 'no-icon',
        url: 'world',
      },
    ],
  },
];

/**
 * SNB가 tree인 경우 작동하는 메뉴. menuType의 snb의 타입이 menuTypeSnb.tree일 때 나오는 snb메뉴.
 * menuType: {
    snb: menuTypeSnb.tree,
    gnb: menuTypeGnb.oneDepthOnly,
  },
 */
export const menuItemsTree: SNBMenuItemType[] = [
  {
    name: 'General',
    meta: ['general'],
    children: [
      {
        name: 'Get Started',
        to: '/',
        meta: ['get started'],
      },
      {
        name: 'smith-ui-components',
        href: 'http://192.168.1.36:8090/',
        meta: ['smith ui components'],
      },
      {
        name: 'theme-creator',
        href: 'http://192.168.1.36:8089/',
        meta: ['theme creator'],
      },
    ],
  },
  {
    name: 'Sample Pages',
    meta: ['sample'],
    children: [
      {
        name: 'Board',
        to: '/sample/board',
        meta: ['board'],
        badge: 'updated',
      },
      {
        name: 'Mobx',
        to: '/sample/mobx',
        meta: ['mobx'],
        badge: 'updated',
      },
      {
        name: 'Login',
        to: '/login',
        meta: ['login'],
      },
      {
        name: 'MemberList',
        to: '/sample/member',
        meta: ['MemberList'],
      },
      {
        name: 'TotalUiComponents',
        to: '/sample/TotalUiComponents',
        meta: ['TotalUiComponents'],
      },
    ],
  },
];

/**
 * 마이페이지 메뉴
 */
export const myPageMenuItems: MyPageMenuItemType[] = [
  {
    label: '마이페이지',
    icon: 'no-icon',
    url: 'myPage',
    // items: [
    //   {
    //     label: 'MyPage0101',
    //     icon: 'pi pi-angle-right',
    //     items: depthSample,
    //   },
    //   {
    //     label: 'MyPage0102',
    //     icon: 'pi pi-angle-right',
    //     url: 'depth2',
    //     items: depthSample,
    //   },
    // ],
  },
  // {
  //   label: 'MyPage02',
  //   icon: 'no-icon',
  //   url: 'myPage',
  //   items: [
  //     {
  //       label: 'MyPage0201',
  //       icon: 'pi pi-angle-right',
  //       items: depthSample,
  //     },
  //     {
  //       label: 'MyPage0202',
  //       icon: 'pi pi-angle-right',
  //       url: 'depth2',
  //       items: depthSample,
  //     },
  //   ],
  // },
];
export const layoutMenus = {
  menuItems,
  menuItemsTree,
  myPageMenuItems,
};
